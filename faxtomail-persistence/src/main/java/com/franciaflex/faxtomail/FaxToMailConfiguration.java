package com.franciaflex.faxtomail;

/*
 * #%L
 * Extranet ENC-AHI :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ApplicationConfigHelper;
import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.jaxx.application.ApplicationConfiguration;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.version.Version;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

public class FaxToMailConfiguration extends ApplicationConfiguration {

    private static final Log log = LogFactory.getLog(FaxToMailConfiguration.class);

    protected final String[] optionKeyToNotSave;

    protected File configFile;

    public FaxToMailConfiguration(ApplicationConfig applicationConfig) {
        super(applicationConfig);
        optionKeyToNotSave = null;
    }

    public FaxToMailConfiguration(String file, String... args) {
        super(new ApplicationConfig());
        applicationConfig.setEncoding(StandardCharsets.UTF_8.name());

        // get all config providers
        Set<ApplicationConfigProvider> providers =
                ApplicationConfigHelper.getProviders(null,
                                                     null,
                                                     null,
                                                     true);

        // load all default options
        ApplicationConfigHelper.loadAllDefaultOption(applicationConfig,
                                                     providers);

        // get all transient and final option keys
        Set<String> optionToSkip =
                ApplicationConfigHelper.getTransientOptionKeys(providers);

        if (log.isDebugEnabled()) {
            log.debug("Option that won't be saved: " + optionToSkip);
        }
        optionKeyToNotSave = optionToSkip.toArray(new String[optionToSkip.size()]);

        applicationConfig.setConfigFileName(file);

        try {
            applicationConfig.parse(args);

        } catch (ArgumentsParserException e) {
            throw new ApplicationTechnicalException(t("faxtomail.config.parse.error"), e);
        }

        //TODO Review this, this is very dirty to do this...
        File faxToMailBasedir = applicationConfig.getOptionAsFile(
                FaxToMailConfigurationOption.BASEDIR.getKey());

        if (faxToMailBasedir == null) {
            faxToMailBasedir = new File("");
        }
        if (!faxToMailBasedir.isAbsolute()) {
            faxToMailBasedir = new File(faxToMailBasedir.getAbsolutePath());
        }
        if (faxToMailBasedir.getName().equals("..")) {
            faxToMailBasedir = faxToMailBasedir.getParentFile().getParentFile();
        }
        if (faxToMailBasedir.getName().equals(".")) {
            faxToMailBasedir = faxToMailBasedir.getParentFile();
        }
        if (log.isInfoEnabled()) {
            log.info("Application basedir: " + faxToMailBasedir);
        }
        applicationConfig.setOption(
                FaxToMailConfigurationOption.BASEDIR.getKey(),
                faxToMailBasedir.getAbsolutePath());
    }

    public void prepareDirectories() {

        File dataDirectory = getDataDirectory();
        ApplicationIOUtil.forceMkdir(
                dataDirectory,
                t("faxtomail.service.mkDir.error", dataDirectory));

    }

    public File getConfigFile() {
        if (configFile == null) {
            File dir = getBasedir();
            if (dir == null || !dir.exists() || !isFullLaunchMode()) {
                dir = new File(applicationConfig.getUserConfigDirectory());
            }
            configFile = new File(dir, applicationConfig.getConfigFileName());
        }
        return configFile;
    }

    public void save() {

        File file = getConfigFile();
        if (log.isInfoEnabled()) {
            log.info("Save configuration at: " + file);
        }
        try {
            applicationConfig.save(file, false, optionKeyToNotSave);
        } catch (IOException e) {
            throw new ApplicationTechnicalException(
                    t("faxtomail.config.save.error", file), e);
        }
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    // OPTION SETTERS
    public void setCsvSeparator(char c) {
        applicationConfig.setOption(FaxToMailConfigurationOption.CSV_SEPARATOR.getKey(), c + "");
    }

    // OPTION GETTERS
    public boolean isDevMode() {
        return applicationConfig.getOptionAsBoolean(FaxToMailConfigurationOption.DEV_MODE.getKey());
    }

    public boolean isUseFolderCache() {
        return applicationConfig.getOptionAsBoolean(FaxToMailConfigurationOption.USE_FOLDER_CACHE.getKey());
    }

    /**
     * Get sub properties containing all options starting with "hibernate" and "topia".
     * 
     * @return new sub map
     */
    public Map<String, String> getTopiaProperties() {
        Map<String, String> jpaParameters = Maps.newHashMap();
        Properties hibernateProperties = applicationConfig.getOptionStartsWith("hibernate");
        jpaParameters.putAll((Map) hibernateProperties);
        Properties topiaProperties = applicationConfig.getOptionStartsWith("topia");
        jpaParameters.putAll((Map) topiaProperties);
        return jpaParameters;
    }

    public String getImportFileEncoding() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.IMPORT_FILE_ENCODING.getKey());
    }

    public String getSmtpHost() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.SMTP_HOST.getKey());
    }

    public int getSmtpPort() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.SMTP_PORT.getKey());
    }
    
    public String getSmtpUser() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.SMTP_USER.getKey());
    }
    
    public String getSmtpPassword() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.SMTP_PASSWORD.getKey());
    }
    
    public boolean isSmtpUseSsl() {
        return applicationConfig.getOptionAsBoolean(FaxToMailConfigurationOption.SMTP_SSL.getKey());
    }

    public boolean isMailDelete() {
        return applicationConfig.getOptionAsBoolean(FaxToMailConfigurationOption.MAIL_DELETE.getKey());
    }
    
    public boolean isMailExpunge() {
        return applicationConfig.getOptionAsBoolean(FaxToMailConfigurationOption.MAIL_EXPUNGE.getKey());
    }

    public boolean isLdapMock() {
        return applicationConfig.getOptionAsBoolean(FaxToMailConfigurationOption.LDAP_MOCK.getKey());
    }

    public String getLdapHost() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.LDAP_HOST.getKey());
    }
    
    public int getLdapPort() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.LDAP_PORT.getKey());
    }

    public String getLdapUser() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.LDAP_USER.getKey());
    }

    public String getLdapPassword() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.LDAP_PASSWORD.getKey());
    }

    public String getLdapBaseDn() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.LDAP_BASEDN.getKey());
    }

    public List<String> getLdapAdminGroups() {
        return applicationConfig.getOptionAsList(FaxToMailConfigurationOption.LDAP_ADMIN_GROUPS.getKey()).getOption();
    }

    public String getLdapTestPrincipal() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.LDAP_TEST_PRINCIPAL.getKey());
    }

    public String getJobEdiExpression() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.JOB_EDI_EXPRESSION.getKey());
    }

    public String getJobMailExpression() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.JOB_MAIL_EXPRESSION.getKey());
    }

    public String getJobCientExpression() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.JOB_CLIENT_EXPRESSION.getKey());
    }

    public String getImageCleanupExpression() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.JOB_IMAGE_CLEANUP_EXPRESSION.getKey());
    }

    public int getArchiveImportCommitTreshold() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.ARCHIVE_IMPORT_COMMIT_TRESHOLD.getKey());
    }

    public boolean isLogConfigurationProvided() {
        return StringUtils.isNotBlank(applicationConfig.getOption(FaxToMailConfigurationOption.LOG_CONFIGURATION_FILE.getKey()));
    }

    public File getLogConfigurationFile() {
        return applicationConfig.getOptionAsFile(FaxToMailConfigurationOption.LOG_CONFIGURATION_FILE.getKey());
    }

    @Override
    public String getApplicationName() {
        return "FaxToMail";
    }

    /** @return {@link FaxToMailConfigurationOption#VERSION} value */
    @Override
    public Version getVersion() {
        return applicationConfig.getOptionAsVersion(FaxToMailConfigurationOption.VERSION.getKey());
    }

    /** @return {@link FaxToMailConfigurationOption#SITE_URL} value */
    public URL getSiteUrl() {
        return applicationConfig.getOptionAsURL(FaxToMailConfigurationOption.SITE_URL.getKey());
    }

    /** @return {@link FaxToMailConfigurationOption#ORGANIZATION_NAME} value */
    public String getOrganizationName() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.ORGANIZATION_NAME.getKey());
    }

    /** @return {@link FaxToMailConfigurationOption#INCEPTION_YEAR} value */
    public int getInceptionYear() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.INCEPTION_YEAR.getKey());
    }

    /** @return {@link FaxToMailConfigurationOption#BASEDIR} value */
    public File getBasedir() {
        return applicationConfig.getOptionAsFile(FaxToMailConfigurationOption.BASEDIR.getKey());
    }
    
    /** @return {@link FaxToMailConfigurationOption#DEMO_DIRECTORY} value */
    public File getDemoDirectory() {
        return applicationConfig.getOptionAsFile(FaxToMailConfigurationOption.DEMO_DIRECTORY.getKey());
    }

    /** @return {@link FaxToMailConfigurationOption#DATA_DIRECTORY} value */
    public File getDataDirectory() {
        return applicationConfig.getOptionAsFile(FaxToMailConfigurationOption.DATA_DIRECTORY.getKey());
    }

    public char getCsvSeparator() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.CSV_SEPARATOR.getKey()).charAt(0);
    }

    public String getDefaultImageIfMalformedUrl() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.DEFAULT_IMAGE_IF_MALFORMED_URL.getKey());
    }

    public boolean isFullLaunchMode() {
        return "full".equals(getFaxToMailLaunchMode());
    }

    public String getFaxToMailLaunchMode() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.FAXTOMAIL_LAUNCH_MODE.getKey());
    }

    /** @return {@link FaxToMailConfigurationOption#UI_CONFIG_FILE} value */
    public File getUIConfigFile() {
        return applicationConfig.getOptionAsFile(FaxToMailConfigurationOption.UI_CONFIG_FILE.getKey());
    }

    /** @return {@link FaxToMailConfigurationOption#START_ACTION_FILE} value */
    public File getStartActionFile() {
        return applicationConfig.getOptionAsFile(FaxToMailConfigurationOption.START_ACTION_FILE.getKey());
    }

    public KeyStroke getShortCut(String actionName) {
        return applicationConfig.getOptionAsKeyStroke("faxtomail.ui." + actionName);
    }

    public boolean isAutoPopupNumberEditor() {
        return applicationConfig.getOptionAsBoolean(FaxToMailConfigurationOption.AUTO_POPUP_NUMBER_EDITOR.getKey());
    }

    public boolean isShowNumberEditorButton() {
        return applicationConfig.getOptionAsBoolean(FaxToMailConfigurationOption.SHOW_NUMBER_EDITOR_BUTTON.getKey());
    }

    public Color getColorBlockingLayer() {
        return applicationConfig.getOptionAsColor(FaxToMailConfigurationOption.COLOR_BLOCKING_LAYER.getKey());
    }

    public Color getColorAlternateRow() {
        return applicationConfig.getOptionAsColor(FaxToMailConfigurationOption.COLOR_ALTERNATE_ROW.getKey());
    }

    public Color getColorSelectedRow() {
        return applicationConfig.getOptionAsColor(FaxToMailConfigurationOption.COLOR_SELECTED_ROW.getKey());
    }

    public Color getColorGroupedDemandWarningDialog() {
        return applicationConfig.getOptionAsColor(FaxToMailConfigurationOption.COLOR_GROUPED_DEMAND_WARNING_DIALOG.getKey());
    }

    public float getFontSizeGroupedDemandWarningDialog() {
        return applicationConfig.getOptionAsFloat(FaxToMailConfigurationOption.FONT_SIZE_GROUPED_DEMAND_WARNING_DIALOG.getKey());
    }

    public int getDelayGroupedDemandWarningDialog() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.DELAY_GROUPED_DEMAND_WARNING_DIALOG.getKey());
    }

    public int getResultPerPage() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.RESULT_PER_PAGE.getKey());
    }

    public void setResultPerPage(int resultPerPage) {
        applicationConfig.setOption(FaxToMailConfigurationOption.RESULT_PER_PAGE.getKey(), String.valueOf(resultPerPage));
    }

    public int getRefreshListInterval() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.REFRESH_LIST_INTERVAL.getKey());
    }

    public void setRefreshListInterval(int refreshListInterval) {
        applicationConfig.setOption(FaxToMailConfigurationOption.REFRESH_LIST_INTERVAL.getKey(),
                                    String.valueOf(refreshListInterval));
    }

    @Override
    public KeyStroke getShortcutClosePopup() {
        return applicationConfig.getOptionAsKeyStroke(FaxToMailConfigurationOption.SHORTCUT_CLOSE_POPUP.getKey());
    }

    public String getDateFormat() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.DATE_FORMAT.getKey());
    }

    public File getI18nDirectory() {
        return applicationConfig.getOptionAsFile(
                FaxToMailConfigurationOption.FAXTOMAIL_I18N_DIRECTORY.getKey());
    }

    public Locale getI18nLocale() {
        return applicationConfig.getOptionAsLocale(
                FaxToMailConfigurationOption.FAXTOMAIL_I18N_LOCALE.getKey());
    }

    public void setI18nLocale(Locale locale) {
        applicationConfig.setOption(FaxToMailConfigurationOption.FAXTOMAIL_I18N_LOCALE.getKey(), locale.toString());
    }

    public float getDefaultZoomValue() {
        return applicationConfig.getOptionAsFloat(FaxToMailConfigurationOption.DEFAULT_ZOOM_VALUE.getKey());
    }

    public float getDefaultZoomStepSize() {
        return applicationConfig.getOptionAsFloat(FaxToMailConfigurationOption.DEFAULT_ZOOM_STEP_SIZE.getKey());
    }

    public int getDefaultPrintMargin() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.DEFAULT_PRINT_MARGIN.getKey());
    }

    public boolean getPrintDetailPage() {
        return applicationConfig.getOptionAsBoolean(FaxToMailConfigurationOption.PRINT_DETAIL_PAGE.getKey());
    }

    public int getClientComboBoxFilterStartChars() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.CLIENT_COMBOBOX_FILTER_START_CHARS.getKey());
    }

    public int getMaxWidthImageInTextPane() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.MAX_WIDTH_IMAGE_IN_TEXT_PANE.getKey());
    }

    public String getImageMagickLocation() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.IMAGEMAGICK_LOCATION.getKey());
    }

    public String getConvertLocation() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.CONVERT_LOCATION.getKey());
    }

    public int getConvertMaxNumber() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.CONVERT_MAX_NUMBER.getKey());
    }

    public int getConvertMaxSize() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.CONVERT_MAX_SIZE.getKey());
    }

    public int getConvertQuality() {
        return applicationConfig.getOptionAsInt(FaxToMailConfigurationOption.CONVERT_QUALITY.getKey());
    }

    public String getProtectedEmailDomains() {
        return applicationConfig.getOption(FaxToMailConfigurationOption.PROTECTED_EMAIL_DOMAINS.getKey());
    }
}
