package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ClientTopiaDao extends AbstractClientTopiaDao<Client> {

    public static final String PROP_VALUE = "propValue";

    protected List<Client> forPropertyJsonLike(String property, String value) {
        Preconditions.checkArgument(StringUtils.isNotBlank(value), "Empty value can produce unexcepted results");

        String query = MessageFormat.format("FROM {0} WHERE {1} LIKE :propValue", Client.class.getName(), property);

        Map<String, Object> args = new HashMap<>();
        args.put(PROP_VALUE, "%\"" + value + "\"%");
        List<Client> result = findAll(query, args);
        return result;
    }

    public List<Client> forEmailAddressesJsonLike(String emailAddress) {
        List<Client> result = forPropertyJsonLike(Client.PROPERTY_EMAIL_ADDRESSES_JSON, emailAddress);
        return result;
    }

    public List<Client> forFaxNumbersJsonLike(String faxNumber) {
        List<Client> result = forPropertyJsonLike(Client.PROPERTY_FAX_NUMBERS_JSON, faxNumber);
        return result;
    }

    public List<Client> forNameStartsWith(String nameQuery) {

        String query = MessageFormat.format("FROM {0} WHERE UPPER({1}) LIKE :propValue", Client.class.getName(), Client.PROPERTY_NAME);

        Map<String, Object> args = new HashMap<>();
        args.put(PROP_VALUE, nameQuery.toUpperCase() + "%");
        return findAll(query, args);
    }

    public List<Client> forCompanyFiltered(String company, String filter) {
        String query = MessageFormat.format(
                "FROM {0} WHERE {1}= :companyValue AND (UPPER({2}) LIKE :propValue OR UPPER({3}) LIKE :propValue )",
                Client.class.getName(),
                Client.PROPERTY_COMPANY,
                Client.PROPERTY_NAME,
                Client.PROPERTY_CODE);

        Map<String, Object> args = new HashMap<>();
        args.put(PROP_VALUE, "%" + filter.toUpperCase() + "%");
        args.put("companyValue", company);
        return findAll(query, args);
    }

    public List<Client> forCompanyInFiltered(Set<String> companies, String filter) {
        String query = MessageFormat.format(
                "FROM {0} WHERE {1} IN ( :companyValues ) AND (UPPER({2}) LIKE :propValue OR UPPER({3}) LIKE :propValue )",
                Client.class.getName(),
                Client.PROPERTY_COMPANY,
                Client.PROPERTY_NAME,
                Client.PROPERTY_CODE);

        StringBuilder companyValuesBuilder = new StringBuilder();

        for (String company:companies){
            companyValuesBuilder.append(company).append(",");
        }

        if (companyValuesBuilder.length()>0) {
            companyValuesBuilder.deleteCharAt(companyValuesBuilder.length() - 1);
        }


        Map<String, Object> args = new HashMap<>();
        args.put(PROP_VALUE, "%" + filter.toUpperCase() + "%");
        args.put("companyValues", companyValuesBuilder.toString());
        return findAll(query, args);
    }


    public List<Client> forEmailAddressOrCodeLike(String searchQuery) {
        Preconditions.checkArgument(StringUtils.isNotBlank(searchQuery), "Empty query can produce unexcepted results");

        String query = MessageFormat.format("FROM {0} WHERE LOWER({1}) LIKE :searchEmail OR UPPER({2}) LIKE :searchCode",
                Client.class.getName(),
                Client.PROPERTY_EMAIL_ADDRESSES_JSON,
                Client.PROPERTY_CODE);

        Map<String, Object> args = new HashMap<>();
        args.put("searchEmail", "%" + searchQuery.toLowerCase() + "%");
        args.put("searchCode", searchQuery.toUpperCase() + "%");
        return findAll(query, args);
    }

} //ClientTopiaDao
