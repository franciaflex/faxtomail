package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;

import java.util.Collection;
import java.util.HashMap;

public class MailFolderTopiaDao extends AbstractMailFolderTopiaDao<MailFolder> {
    
    @Override
    public void delete(MailFolder entity) {

        // code from AbstractTopiaDao
        topiaJpaSupport.delete(entity);
        entity.notifyDeleted();
        topiaFiresSupport.notifyEntityDeleted(entity);

//      do nothing just to disable strange topia code
//        org.nuiton.topia.persistence.TopiaException: An error occurs during query operation: from com.franciaflex.faxtomail.persistence.entities.Email topiaEntity_ where topiaEntity_.mailFolder = :mailFolder0 : not-null property references a null or transient value : com.franciaflex.faxtomail.persistence.entities.MailFilterImpl.mailFolder
//        org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport.findAll(HibernateTopiaJpaSupport.java:116)
//        org.nuiton.topia.persistence.internal.AbstractTopiaDao.findAll(AbstractTopiaDao.java:540)
//        org.nuiton.topia.persistence.internal.AbstractTopiaDao$InnerTopiaQueryBuilderRunQueryStep.findAll(AbstractTopiaDao.java:1084)
//        org.nuiton.topia.persistence.internal.AbstractTopiaDao$InnerTopiaQueryBuilderAddCriteriaOrRunQueryStep.findAll(AbstractTopiaDao.java:944)
//        com.franciaflex.faxtomail.persistence.entities.GeneratedMailFolderTopiaDao.delete(GeneratedMailFolderTopiaDao.java:59)
//        com.franciaflex.faxtomail.persistence.entities.GeneratedMailFolderTopiaDao.delete(GeneratedMailFolderTopiaDao.java:1)
//        org.nuiton.topia.persistence.internal.AbstractTopiaDao.deleteAll(AbstractTopiaDao.java:343)
//        com.franciaflex.faxtomail.services.service.ConfigurationService.saveMailFolders(ConfigurationService.java:239)
//        com.franciaflex.faxtomail.services.service.ConfigurationService.save(ConfigurationService.java:139)
//        com.franciaflex.faxtomail.web.action.admin.ConfigurationAction.execute(ConfigurationAction.java:126)

    }

    public Collection<MailFolder> getReadableFolders(FaxToMailUser user) {
        String query = newFromClause("mf") + " where (:user in elements(mf.readRightUsers) or exists (from mf.readRightGroups g where g in (:groups))) and mf.archiveFolder = false";
        HashMap<String, Object> map = Maps.newHashMap();
        map.put("user", user);
        map.put("groups", user.getUserGroups());
        Collection<MailFolder> folders = findAll(query, map);
        return folders;
    }
    
    public Collection<MailFolder> getMoveableFolders(FaxToMailUser user) {
        String query = newFromClause("mf") + " where (:user in elements(mf.moveRightUsers) or exists (from mf.moveRightGroups g where g in (:groups))) and mf.archiveFolder = false";
        HashMap<String, Object> map = Maps.newHashMap();
        map.put("user", user);
        map.put("groups", user.getUserGroups());
        Collection<MailFolder> folders = findAll(query, map);
        return folders;
    }

} //MailFolderTopiaDao
