package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Random;

public class AttachmentFileImpl extends AttachmentFileAbstract {

    private static final Log log = LogFactory.getLog(AttachmentFileImpl.class);

    /** Transient file instance with blob content on local file system. */
    protected transient File tmpLocalFile;

    @Override
    public File getFile() {

        byte[] content = getContent();

        if (tmpLocalFile == null && content != null) {
            try {
                // create temp file
                File tempFolder = new File(FileUtils.getTempDirectory(), "faxtomail");
                tempFolder.mkdir();

                //generate a random subfolder
                int leftLimit = 97; // letter 'a'
                int rightLimit = 122; // letter 'z'
                int targetStringLength = 10;
                Random random = new Random();

                String generatedString = random.ints(leftLimit, rightLimit + 1)
                        .limit(targetStringLength)
                        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                        .toString();

                tempFolder = new File(tempFolder, generatedString);
                tempFolder.deleteOnExit();
                tempFolder.mkdir();

                String fileName = URLEncoder.encode(getFilename(), "UTF-8");
                tmpLocalFile = new File(tempFolder, fileName);

                // if the file already exists, then add a number to the name
                int i = 1;
                String extension = "." + FileUtil.extension(fileName);
                String name = FileUtil.basename(fileName, extension);
                while (tmpLocalFile.exists()) {
                    tmpLocalFile = new File(tempFolder, name + "-" + i++ + extension);
                }

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Copy blob content to file : %s", tmpLocalFile.getAbsolutePath()));
                }

                tmpLocalFile.deleteOnExit();

                // copy blob content
                // be sure that "is" is not closed because is can be a local fileinputstream
                // that hibernate must read to put in database
                InputStream is = new ByteArrayInputStream(content);
                OutputStream fos = new FileOutputStream(tmpLocalFile);
                IOUtils.copy(is, fos);
                fos.close();

            } catch (IOException ex) {
                throw new RuntimeException("Can't create file on local file system", ex);
            }
        }
        return tmpLocalFile;
    }

    @Override
    public long getLength() {
        long result = 0;
        if (getFile() != null) {
            result = getFile().length();
        }
        return result;
    }

    /**
     * Overwrite finalize to delete local tmp file if necessary.
     */
    @Override
    protected void finalize() throws Throwable {
        if (log.isDebugEnabled()) {
            log.debug(String.format("Deleting file : %s", tmpLocalFile.getAbsolutePath()));
        }
        Files.delete(tmpLocalFile.toPath());
        super.finalize();
    }
}
