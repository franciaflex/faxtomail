package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ClientImpl extends ClientAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 645048064844251689L;

    private static final Type LIST_STRING_GSON_TYPE = new TypeToken<List<String>>() {}.getType();

    @Override
    public void setEmailAddresses(List<String> emails) {
        String json = AbstractFaxToMailTopiaDao.GSON_INSTANCE.toJson(emails);
        setEmailAddressesJson(json);
    }

    @Override
    public List<String> getEmailAddresses() {
        String json = getEmailAddressesJson();
        List<String> emailAddress = AbstractFaxToMailTopiaDao.GSON_INSTANCE.fromJson(json, LIST_STRING_GSON_TYPE);
        return emailAddress;
    }

    @Override
    public void setFaxNumbers(List<String> faxNumbers) {
        String json = AbstractFaxToMailTopiaDao.GSON_INSTANCE.toJson(faxNumbers);
        setFaxNumbersJson(json);
    }

    @Override
    public List<String> getFaxNumbers() {
        String json = getFaxNumbersJson();
        List<String> faxNumbers = AbstractFaxToMailTopiaDao.GSON_INSTANCE.fromJson(json, LIST_STRING_GSON_TYPE);
        return faxNumbers;
    }

}
