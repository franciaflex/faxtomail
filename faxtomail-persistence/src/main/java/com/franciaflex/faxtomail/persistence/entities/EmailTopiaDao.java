package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.beans.Quantities;
import com.franciaflex.faxtomail.beans.QuantitiesByRange;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class EmailTopiaDao extends AbstractEmailTopiaDao<Email> {

    private static final Log log = LogFactory.getLog(EmailTopiaDao.class);

    public static final String EMAIL_IDENTIFIER = "E";
    public static final String CLIENT_IDENTIFIER = "C";
    public static final String PRIORITY_IDENTIFIER = "P";
    public static final String WAITING_STATE_IDENTIFIER = "WS";
    public static final String DEMAND_TYPE_IDENTIFIER = "DT";
    public static final String TAKEN_BY_IDENTIFIER = "TB";
    public static final String LAST_ATTACHMENT_OPENER_IDENTIFIER = "LAO";
    public static final String LAST_PRINTING_USER_IDENTIFIER = "LPU";
    public static final String GROUP_IDENTIFIER = "G";
    public static final String RANGE_ROW_IDENTIFIER = "RR";

    /**
     * Search for email using filter.
     * 
     * @param searchFilter email filter
     * @param readMailFolders readable mail folders (for rights compute)
     * @param pagination pagination
     * @return email list matching query filters
     */
    public PaginationResult<Email> search(SearchFilter searchFilter, Set<MailFolder> readMailFolders, PaginationParameter pagination) {

        //StringBuilder query = new StringBuilder("FROM " + Email.class.getName() + " E");
        StringBuilder query = new StringBuilder();
        Map<String, Object> args = new HashMap<>();

        // apply security
        query.append(" WHERE " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_MAIL_FOLDER + " in (:readMailFolders)");
        args.put("readMailFolders", readMailFolders);

        // email minReceptionDate
        if (searchFilter.getMinReceptionDate() != null) {
            Date date = DateUtils.truncate(searchFilter.getMinReceptionDate(), Calendar.DAY_OF_MONTH);
            query.append(" AND " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RECEPTION_DATE + " >= :" + SearchFilter.PROPERTY_MIN_RECEPTION_DATE);
            args.put(SearchFilter.PROPERTY_MIN_RECEPTION_DATE, date);
        }

        // email maxReceptionDate
        if (searchFilter.getMaxReceptionDate() != null) {
            Date date = DateUtils.ceiling(searchFilter.getMaxReceptionDate(), Calendar.DAY_OF_MONTH);
            query.append(" AND " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RECEPTION_DATE + " <= :" + SearchFilter.PROPERTY_MAX_RECEPTION_DATE);
            args.put(SearchFilter.PROPERTY_MAX_RECEPTION_DATE, date);
        }

        // email taken by
        if (searchFilter.getTakenBy() != null) {
            query.append(" AND " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_TAKEN_BY + " = :" + SearchFilter.PROPERTY_TAKEN_BY);
            args.put(SearchFilter.PROPERTY_TAKEN_BY, searchFilter.getTakenBy());
        }

        // history minModificationDate && modifiedBy
        if (searchFilter.getMinModificationDate() != null || searchFilter.getModifiedBy() != null) {
            query.append(" AND EXISTS (FROM " + History.class.getName() + " H WHERE H in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_HISTORY + ")");
            query.append("   AND H." + History.PROPERTY_TYPE + " = :modificationType");
            args.put("modificationType", HistoryType.MODIFICATION);
            if (searchFilter.getMinModificationDate() != null) {
                Date date = DateUtils.truncate(searchFilter.getMinModificationDate(), Calendar.DAY_OF_MONTH);
                query.append("   AND H." + History.PROPERTY_MODIFICATION_DATE + " >= :" + SearchFilter.PROPERTY_MIN_MODIFICATION_DATE);
                args.put(SearchFilter.PROPERTY_MIN_MODIFICATION_DATE, date);
            }
            if (searchFilter.getModifiedBy() != null) {
                query.append("   AND H." + History.PROPERTY_FAX_TO_MAIL_USER + " = :" + SearchFilter.PROPERTY_MODIFIED_BY);
                args.put(SearchFilter.PROPERTY_MODIFIED_BY, searchFilter.getModifiedBy());
            }
            query.append(")");
        }

        // history maxModificationDate && modifiedBy
        if (searchFilter.getMaxModificationDate() != null || searchFilter.getModifiedBy() != null) {
            query.append(" AND EXISTS (FROM " + History.class.getName() + " H WHERE H in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_HISTORY + ")");
            query.append("   AND H." + History.PROPERTY_TYPE + " = :modificationType");
            args.put("modificationType", HistoryType.MODIFICATION);
            if (searchFilter.getMaxModificationDate() != null) {
                Date date = DateUtils.ceiling(searchFilter.getMaxModificationDate(), Calendar.DAY_OF_MONTH);
                query.append("   AND H." + History.PROPERTY_MODIFICATION_DATE + " <= :" + SearchFilter.PROPERTY_MAX_MODIFICATION_DATE);
                args.put(SearchFilter.PROPERTY_MAX_MODIFICATION_DATE, date);
            }
            if (searchFilter.getModifiedBy() != null) {
                query.append("   AND H." + History.PROPERTY_FAX_TO_MAIL_USER + " = :" + SearchFilter.PROPERTY_MODIFIED_BY);
                args.put(SearchFilter.PROPERTY_MODIFIED_BY, searchFilter.getModifiedBy());
            }
            query.append(")");
        }

        // history minTransferDate && transferBy
        if (searchFilter.getMinTransferDate() != null || searchFilter.getTransferBy() != null) {
            query.append(" AND EXISTS (FROM " + History.class.getName() + " H WHERE H in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_HISTORY + ")");
            query.append("   AND H." + History.PROPERTY_TYPE + " = :transmissionType");
            args.put("transmissionType", HistoryType.TRANSMISSION);
            if (searchFilter.getMinTransferDate() != null) {
                Date date = DateUtils.truncate(searchFilter.getMinTransferDate(), Calendar.DAY_OF_MONTH);
                query.append("   AND H." + History.PROPERTY_MODIFICATION_DATE + " >= :" + SearchFilter.PROPERTY_MIN_TRANSFER_DATE);
                args.put(SearchFilter.PROPERTY_MIN_TRANSFER_DATE, date);
            }
            if (searchFilter.getTransferBy() != null) {
                query.append("   AND H." + History.PROPERTY_FAX_TO_MAIL_USER + " = :" + SearchFilter.PROPERTY_TRANSFER_BY);
                args.put(SearchFilter.PROPERTY_TRANSFER_BY, searchFilter.getTransferBy());
            }
            query.append(")");
        }

        // history maxTransferDate && transferBy
        if (searchFilter.getMaxTransferDate() != null || searchFilter.getTransferBy() != null) {
            query.append(" AND EXISTS (FROM " + History.class.getName() + " H WHERE H in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_HISTORY + ")");
            query.append("   AND H." + History.PROPERTY_TYPE + " = :transmissionType");
            args.put("transmissionType", HistoryType.TRANSMISSION);
            if (searchFilter.getMaxTransferDate() != null) {
                Date date = DateUtils.ceiling(searchFilter.getMaxTransferDate(), Calendar.DAY_OF_MONTH);
                query.append("   AND H." + History.PROPERTY_MODIFICATION_DATE + " <= :" + SearchFilter.PROPERTY_MAX_TRANSFER_DATE);
                args.put(SearchFilter.PROPERTY_MAX_TRANSFER_DATE, date);
            }
            if (searchFilter.getTransferBy() != null) {
                query.append("   AND H." + History.PROPERTY_FAX_TO_MAIL_USER + " = :" + SearchFilter.PROPERTY_TRANSFER_BY);
                args.put(SearchFilter.PROPERTY_TRANSFER_BY, searchFilter.getTransferBy());
            }
            query.append(")");
        }

        // history minArchivedDate && transferBy
        if (searchFilter.getMinArchivedDate() != null || searchFilter.getArchivedBy() != null) {
            query.append(" AND EXISTS (FROM " + History.class.getName() + " H WHERE H in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_HISTORY + ")");
            query.append("   AND H." + History.PROPERTY_TYPE + " = :archivedType");
            args.put("archivedType", HistoryType.ARCHIVED);
            if (searchFilter.getMinArchivedDate() != null) {
                Date date = DateUtils.truncate(searchFilter.getMinArchivedDate(), Calendar.DAY_OF_MONTH);
                query.append("   AND H." + History.PROPERTY_MODIFICATION_DATE + " >= :" + SearchFilter.PROPERTY_MIN_ARCHIVED_DATE);
                args.put(SearchFilter.PROPERTY_MIN_ARCHIVED_DATE, date);
            }
            if (searchFilter.getArchivedBy() != null) {
                query.append("   AND H." + History.PROPERTY_FAX_TO_MAIL_USER + " = :" + SearchFilter.PROPERTY_ARCHIVED_BY);
                args.put(SearchFilter.PROPERTY_ARCHIVED_BY, searchFilter.getArchivedBy());
            }
            query.append(")");
        }

        // history maxArchivedDate && transferBy
        if (searchFilter.getMaxArchivedDate() != null || searchFilter.getArchivedBy() != null) {
            query.append(" AND EXISTS (FROM " + History.class.getName() + " H WHERE H in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_HISTORY + ")");
            query.append("   AND H." + History.PROPERTY_TYPE + " = :archivedType");
            args.put("archivedType", HistoryType.ARCHIVED);
            if (searchFilter.getMaxArchivedDate() != null) {
                Date date = DateUtils.ceiling(searchFilter.getMaxArchivedDate(), Calendar.DAY_OF_MONTH);
                query.append("   AND H." + History.PROPERTY_MODIFICATION_DATE + " <= :" + SearchFilter.PROPERTY_MAX_ARCHIVED_DATE);
                args.put(SearchFilter.PROPERTY_MAX_ARCHIVED_DATE, date);
            }
            if (searchFilter.getArchivedBy() != null) {
                query.append("   AND H." + History.PROPERTY_FAX_TO_MAIL_USER + " = :" + SearchFilter.PROPERTY_ARCHIVED_BY);
                args.put(SearchFilter.PROPERTY_ARCHIVED_BY, searchFilter.getArchivedBy());
            }
            query.append(")");
        }

        // email minPrintingDate
        if (searchFilter.getMinPrintingDate() != null || searchFilter.getPrintingBy() != null) {
            query.append(" AND EXISTS (FROM " + History.class.getName() + " H WHERE H in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_HISTORY + ")");
            query.append("   AND H." + History.PROPERTY_TYPE + " = :printingType");
            args.put("printingType", HistoryType.PRINTING);
            if (searchFilter.getMinPrintingDate() != null) {
                Date date = DateUtils.truncate(searchFilter.getMinPrintingDate(), Calendar.DAY_OF_MONTH);
                query.append("   AND H." + History.PROPERTY_MODIFICATION_DATE + " >= :" + SearchFilter.PROPERTY_MIN_PRINTING_DATE);
                args.put(SearchFilter.PROPERTY_MIN_PRINTING_DATE, date);
            }
            if (searchFilter.getPrintingBy() != null) {
                query.append("   AND H." + History.PROPERTY_FAX_TO_MAIL_USER + " = :" + SearchFilter.PROPERTY_PRINTING_BY);
                args.put(SearchFilter.PROPERTY_PRINTING_BY, searchFilter.getPrintingBy());
            }
            query.append(")");
        }

        // email maxPrintingDate
        if (searchFilter.getMaxPrintingDate() != null || searchFilter.getPrintingBy() != null) {
            query.append(" AND EXISTS (FROM " + History.class.getName() + " H WHERE H in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_HISTORY + ")");
            query.append("   AND H." + History.PROPERTY_TYPE + " = :printingType");
            args.put("printingType", HistoryType.PRINTING);
            if (searchFilter.getMaxPrintingDate() != null) {
                Date date = DateUtils.ceiling(searchFilter.getMaxPrintingDate(), Calendar.DAY_OF_MONTH);
                query.append("   AND H." + History.PROPERTY_MODIFICATION_DATE + " <= :" + SearchFilter.PROPERTY_MAX_PRINTING_DATE);
                args.put(SearchFilter.PROPERTY_MAX_PRINTING_DATE, date);
            }
            if (searchFilter.getPrintingBy() != null) {
                query.append("   AND H." + History.PROPERTY_FAX_TO_MAIL_USER + " = :" + SearchFilter.PROPERTY_PRINTING_BY);
                args.put(SearchFilter.PROPERTY_PRINTING_BY, searchFilter.getPrintingBy());
            }
            query.append(")");
        }

        // email minReplyDate
        if (searchFilter.getMinReplyDate() != null || searchFilter.getReplyBy() != null) {
            query.append(" AND EXISTS (FROM " + History.class.getName() + " H WHERE H in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_HISTORY + ")");
            query.append("   AND H." + History.PROPERTY_TYPE + " = :replyType");
            args.put("replyType", HistoryType.REPLY);
            if (searchFilter.getMinReplyDate() != null) {
                Date date = DateUtils.truncate(searchFilter.getMinReplyDate(), Calendar.DAY_OF_MONTH);
                query.append("   AND H." + History.PROPERTY_MODIFICATION_DATE + " >= :" + SearchFilter.PROPERTY_MIN_REPLY_DATE);
                args.put(SearchFilter.PROPERTY_MIN_REPLY_DATE, date);
            }
            if (searchFilter.getReplyBy() != null) {
                query.append("   AND H." + History.PROPERTY_FAX_TO_MAIL_USER + " = :" + SearchFilter.PROPERTY_REPLY_BY);
                args.put(SearchFilter.PROPERTY_REPLY_BY, searchFilter.getReplyBy());
            }
            query.append(")");
        }

        // email maxReplyDate
        if (searchFilter.getMaxReplyDate() != null || searchFilter.getReplyBy() != null) {
            query.append(" AND EXISTS (FROM " + History.class.getName() + " H WHERE H in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_HISTORY + ")");
            query.append("   AND H." + History.PROPERTY_TYPE + " = :replyType");
            args.put("replyType", HistoryType.REPLY);
            if (searchFilter.getMaxReplyDate() != null) {
                Date date = DateUtils.ceiling(searchFilter.getMaxReplyDate(), Calendar.DAY_OF_MONTH);
                query.append("   AND H." + History.PROPERTY_MODIFICATION_DATE + " <= :" + SearchFilter.PROPERTY_MAX_REPLY_DATE);
                args.put(SearchFilter.PROPERTY_MAX_REPLY_DATE, date);
            }
            if (searchFilter.getReplyBy() != null) {
                query.append("   AND H." + History.PROPERTY_FAX_TO_MAIL_USER + " = :" + SearchFilter.PROPERTY_REPLY_BY);
                args.put(SearchFilter.PROPERTY_REPLY_BY, searchFilter.getReplyBy());
            }
            query.append(")");
        }

        // email sender
        if (StringUtils.isNotBlank(searchFilter.getSender())) {
            query.append(" AND lower(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_SENDER + ") LIKE lower(:" + SearchFilter.PROPERTY_SENDER + ")");
            args.put(SearchFilter.PROPERTY_SENDER, "%" + searchFilter.getSender() + "%");
        }

        // client
        if (searchFilter.getClient() != null) {
            query.append(" AND " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_CLIENT + " = :" + SearchFilter.PROPERTY_CLIENT);
            args.put(SearchFilter.PROPERTY_CLIENT, searchFilter.getClient());
        }

        // email subject
        if (StringUtils.isNotBlank(searchFilter.getDemandSubject())) {
            query.append(" AND lower(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_SUBJECT + ") LIKE lower(:" + SearchFilter.PROPERTY_DEMAND_SUBJECT + ")");
            args.put(SearchFilter.PROPERTY_DEMAND_SUBJECT, "%" + searchFilter.getDemandSubject() + "%");
        }

        // email demand type
        List<DemandType> demandType = searchFilter.getDemandType();
        if (CollectionUtils.isNotEmpty(demandType)) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_TYPE + " IN (:" + SearchFilter.PROPERTY_DEMAND_TYPE + ")");
            args.put(SearchFilter.PROPERTY_DEMAND_TYPE, demandType);

            if (demandType.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_TYPE + " IS NULL");
            }
            query.append(")");
        }

        // email ediCodeNumber
        if (StringUtils.isNotBlank(searchFilter.getEdiCodeNumber())) {
            query.append(" AND lower(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_EDI_ERROR + ") LIKE lower(:" + SearchFilter.PROPERTY_EDI_CODE_NUMBER + ")");
            args.put(SearchFilter.PROPERTY_EDI_CODE_NUMBER, "%" + searchFilter.getEdiCodeNumber() + "%");
        }

        // email projectReference
        if (StringUtils.isNotBlank(searchFilter.getProjectReference())) {
            query.append(" AND lower(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_PROJECT_REFERENCE + ") LIKE lower(:" + SearchFilter.PROPERTY_PROJECT_REFERENCE + ")");
            args.put(SearchFilter.PROPERTY_PROJECT_REFERENCE, "%" + searchFilter.getProjectReference() + "%");
        }

        // email priority
        List<Priority> priority = searchFilter.getPriority();
        if (CollectionUtils.isNotEmpty(priority)) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_PRIORITY + " IN (:" + SearchFilter.PROPERTY_PRIORITY + ")");
            args.put(SearchFilter.PROPERTY_PRIORITY, priority);

            if (priority.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_PRIORITY + " IS NULL");
            }
            query.append(")");
        }

        // email demand status
        List<DemandStatus> demandStatus = searchFilter.getDemandStatus();
        if (CollectionUtils.isNotEmpty(demandStatus)) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_STATUS + " IN (:" + SearchFilter.PROPERTY_DEMAND_STATUS + ")");
            args.put(SearchFilter.PROPERTY_DEMAND_STATUS, demandStatus);

            if (demandStatus.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_STATUS + " IS NULL");
            }
            query.append(")");
        }

        // email etat attente
        List<WaitingState> waitingStates = searchFilter.getWaitingStates();
        if (CollectionUtils.isNotEmpty(waitingStates)) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_WAITING_STATE + " IN (:" + SearchFilter.PROPERTY_WAITING_STATES + ")");
            args.put(SearchFilter.PROPERTY_WAITING_STATES, waitingStates);

            if (waitingStates.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_WAITING_STATE + " IS NULL");
            }
            query.append(")");
        }

        // email message
        if (StringUtils.isNotBlank(searchFilter.getMessage())) {
            query.append(" AND lower(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMMENT + ") LIKE lower(:" + SearchFilter.PROPERTY_MESSAGE + ")");
            args.put(SearchFilter.PROPERTY_MESSAGE, "%" + searchFilter.getMessage() + "%");
        }

        if (StringUtils.isNotBlank(searchFilter.getBody())) {
            query.append(" AND lower(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_ORIGINAL_EMAIL + "." + OriginalEmail.PROPERTY_CONTENT + ") LIKE lower(:" + SearchFilter.PROPERTY_BODY + ")");
            args.put(SearchFilter.PROPERTY_BODY, "%" + searchFilter.getBody() + "%");
        }

        // email gamme
        List<Range> gamme = searchFilter.getGamme();
        if (CollectionUtils.isNotEmpty(gamme)) {
            query.append(" AND ((EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + ")");
            query.append("   AND RR." + RangeRow.PROPERTY_RANGE + " IN (:" + SearchFilter.PROPERTY_GAMME + "))");
            args.put(SearchFilter.PROPERTY_GAMME, gamme);

            if (gamme.contains(null)) {
                query.append(" OR NOT EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + "))");
            }
            query.append("))");
        }

        // email localReference
        if (StringUtils.isNotBlank(searchFilter.getLocalReference())) {
            query.append(" AND (EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + ")");
            query.append("   AND lower(RR." + RangeRow.PROPERTY_COMMAND_NUMBER + ") LIKE lower(:" + SearchFilter.PROPERTY_LOCAL_REFERENCE + "))");
            query.append(" OR lower(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMPANY_REFERENCE + ") LIKE lower(:" + SearchFilter.PROPERTY_LOCAL_REFERENCE + ")");
            args.put(SearchFilter.PROPERTY_LOCAL_REFERENCE, "%" + searchFilter.getLocalReference() + "%");
            query.append(")");
        }

        // command Nb
        if (StringUtils.isNotBlank(searchFilter.getCommandNb())) {
            query.append(" AND (EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + ")");
            query.append("   AND lower(RR." + RangeRow.PROPERTY_COMMAND_NUMBER + ") LIKE lower(:" + SearchFilter.PROPERTY_COMMAND_NB + "))");
            query.append(" OR lower(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMPANY_REFERENCE + ") LIKE lower(:" + SearchFilter.PROPERTY_COMMAND_NB + ")");
            args.put(SearchFilter.PROPERTY_COMMAND_NB, "%" + searchFilter.getCommandNb() + "%");
            query.append(")");
        }

        // add same fecth liste as emailService#getEmailForFolder()
        String hqlForFetchStep1 = "SELECT " + EMAIL_IDENTIFIER + "." + TopiaEntity.PROPERTY_TOPIA_ID
                                    + " FROM " + Email.class.getName() + " " + EMAIL_IDENTIFIER
                                    + " WHERE " + EMAIL_IDENTIFIER + "." + TopiaEntity.PROPERTY_TOPIA_ID + " IN (";

        if (searchFilter.isAddGroupDemands()) {
            hqlForFetchStep1 += "SELECT CASE WHEN G IS NULL " +
                                    "THEN " + EMAIL_IDENTIFIER + "." + TopiaEntity.PROPERTY_TOPIA_ID +
                                    " ELSE E2." + TopiaEntity.PROPERTY_TOPIA_ID + " END " +
                                "FROM " + Email.class.getName() + " E " +
                                    "LEFT OUTER JOIN " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_EMAIL_GROUP + " AS G " +
                                    "LEFT OUTER JOIN G." + EmailGroup.PROPERTY_EMAIL + " AS E2 ";
        } else {
            hqlForFetchStep1 += "select " + EMAIL_IDENTIFIER + "." + TopiaEntity.PROPERTY_TOPIA_ID
                                + " FROM " + Email.class.getName() + " " + EMAIL_IDENTIFIER + " ";
        }
        hqlForFetchStep1 += query.toString() + ")";

        String hqlForFetchStep2 = "select distinct E FROM " + Email.class.getName() + " E " + addAllFecthes(
                Email.PROPERTY_PRIORITY,
                Email.PROPERTY_DEMAND_TYPE,
                Email.PROPERTY_CLIENT,
                Email.PROPERTY_WAITING_STATE,
                Email.PROPERTY_TAKEN_BY,
                Email.PROPERTY_LAST_ATTACHMENT_OPENER,
                Email.PROPERTY_LAST_PRINTING_USER,
                Email.PROPERTY_LAST_ATTACHMENT_OPENER
                ) + " WHERE " + EMAIL_IDENTIFIER + "." + TopiaEntity.PROPERTY_TOPIA_ID + " in ( :topiaIdsForFetch_ )";

        // code copied from topia to perform search
        PaginationResult<String> pageResult = findPage(EMAIL_IDENTIFIER, hqlForFetchStep1, args, pagination);
        List<String> step1ResultTopiaIds = pageResult.getElements();

        List<Email> sortedEntities;
        if (CollectionUtils.isEmpty(step1ResultTopiaIds)) {
            sortedEntities = Lists.newArrayList();
        } else {
            Map<String, Object> step2Args = Maps.newHashMap();
            step2Args.put("topiaIdsForFetch_", step1ResultTopiaIds);
            List<Email> entities = forHql(hqlForFetchStep2, step2Args).findAll();

            sortedEntities = sortAccordingToIds(entities, step1ResultTopiaIds);
        }

        PaginationResult<Email> result = PaginationResult.of(sortedEntities, pageResult.getCount(), pageResult.getCurrentPage());

        for (Email email : result.getElements()) {
            List<RangeRow> rangeRows = email.getRangeRow();
            if (rangeRows != null) {
                for (RangeRow rangeRow : rangeRows) {
                    Hibernate.initialize(rangeRow.getRange());
                }
            }
            Hibernate.initialize(email.getReplies());
            Hibernate.initialize(email.getAttachment());
            EmailGroup emailGroup = email.getEmailGroup();
            if (emailGroup != null) {
                Hibernate.initialize(emailGroup.getEmail());
                // initialize folder of the grouped demand to transmit the grouped emails of the same folder
                for (Email groupedEmail : emailGroup.getEmail()) {
                    Hibernate.initialize(groupedEmail.getMailFolder());
                    Hibernate.initialize(groupedEmail.getAttachment());
                }
            }
        }
        return result;
    }

    public Collection<Email> findArchivedEmails(String commandQuotationNumber, String company) {
        StringBuilder query = new StringBuilder();
        Map<String, Object> args = new HashMap<>();

        Set<Email> result = new HashSet<>();

        query.append(newFromClause("E") + " WHERE " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_MAIL_FOLDER + "." + MailFolder.PROPERTY_COMPANY + " = (:company)");
        args.put("company", company);

        query.append(" AND " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_STATUS + " = :archiveStatus");
        args.put("archiveStatus", DemandStatus.ARCHIVED);

        // kmorin 20150417 impossible de savoir pourquoi, mais sqlserver n'aime pas cette requete...
        // si on la fait en 2 fois ca passe

//        query.append(" AND (EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + ")");
//        query.append("   AND lower(RR." + RangeRow.PROPERTY_COMMAND_NUMBER + ") LIKE lower(:" + SearchFilter.PROPERTY_COMMAND_NB + "))");
//        query.append(" OR lower(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMPANY_REFERENCE + ") LIKE lower(:" + SearchFilter.PROPERTY_COMMAND_NB + ")");
//        args.put(SearchFilter.PROPERTY_COMMAND_NB, "%" + commandQuotationNumber + "%");

        StringBuilder query1 = new StringBuilder(query);
        query1.append(" AND (EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + ")");
        query1.append("   AND RR." + RangeRow.PROPERTY_COMMAND_NUMBER + " LIKE :" + SearchFilter.PROPERTY_COMMAND_NB + ")");

        args.put(SearchFilter.PROPERTY_COMMAND_NB, "%" + commandQuotationNumber + "%");

        query1.append(")");

        result.addAll(this.<Email>findAll(query1.toString(), args));

        StringBuilder query2 = new StringBuilder(query);
        query2.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMPANY_REFERENCE + " LIKE :" + SearchFilter.PROPERTY_COMMAND_NB);
        query2.append(")");

        result.addAll(this.<Email>findAll(query2.toString(), args));

        return result;
    }

    /**
     * Search for email using filter.
     *
     * @param emailFilter email filter
     * @param folder readable mail folders (for rights compute)
     * @param pagination pagination
     * @return email list matching query filters
     */
    public PaginationResult<Email> getEmailForFolder(EmailFilter emailFilter, MailFolder folder, PaginationParameter pagination) {

        long start = new Date().getTime();

        Map<String, Object> args = new HashMap<>();
        String query = filteredDemandsOfFolderCondition(folder, emailFilter, args);

        String orderClause = null;
        String tableToJoin = null;
        if (!pagination.getOrderClauses().isEmpty()) {
            orderClause = pagination.getOrderClauses().get(0).getClause();
            String clauseNoSum = orderClause.replaceAll("(SUM|SIZE)\\((.*)\\)", "$2");
            int dotIndex = clauseNoSum.indexOf('.');
            if (dotIndex >= 0) {
                tableToJoin = clauseNoSum.substring(0, dotIndex);
            } else {
                // il s'agit d'un champ portant sur un utilisateur,
                // donc on vérifie dans la conf du dossier si on trie sur le trigramme ou le nom complet
                MailFolder folderWithTrigrammConf = folder;
                while (folderWithTrigrammConf.getParent() != null
                      && folderWithTrigrammConf.getDisplayOnlyUserTrigraphInTables() == null) {
                    folderWithTrigrammConf = folderWithTrigrammConf.getParent();
                }
                boolean sortOnTrigraph = Boolean.TRUE.equals(folderWithTrigrammConf.getDisplayOnlyUserTrigraphInTables());
                boolean desc = pagination.getOrderClauses().get(0).isDesc();
                if (sortOnTrigraph) {
                    orderClause += "." + FaxToMailUser.PROPERTY_TRIGRAPH;
                    pagination = PaginationParameter.of(pagination.getPageNumber(), pagination.getPageSize(),
                                                        orderClause, desc);
                } else {
                    String originalClause = orderClause;
                    orderClause += "." + FaxToMailUser.PROPERTY_LAST_NAME;
                    pagination = PaginationParameter.of(pagination.getPageNumber(), pagination.getPageSize(),
                                                        originalClause + "." + FaxToMailUser.PROPERTY_LAST_NAME, desc,
                                                        originalClause + "." + FaxToMailUser.PROPERTY_FIRST_NAME, desc,
                                                        originalClause + "." + FaxToMailUser.PROPERTY_TRIGRAPH, desc);
                }
            }
        }

        boolean orderByWithSum = orderClause != null && orderClause.startsWith("SUM(");
        boolean orderByWithSize = orderClause != null && orderClause.startsWith("SIZE(");
        // add same fecth liste as emailService#getEmailForFolder()
        StringBuilder hqlForFetchStep1 = new StringBuilder("SELECT " + EMAIL_IDENTIFIER + "." + TopiaEntity.PROPERTY_TOPIA_ID);
        if (orderClause != null && !orderByWithSize) {
            // sql server ne supporte pas les requetes order by sur des champs non selectionnes
            hqlForFetchStep1.append(", ").append(orderClause);
        }
        if (orderByWithSize && tableToJoin != null) {
            hqlForFetchStep1.append(", ").append(tableToJoin).append(".").append(TopiaEntity.PROPERTY_TOPIA_ID);
        }
        hqlForFetchStep1.append(" FROM ").append(Email.class.getName()).append(" ").append(EMAIL_IDENTIFIER);

        if (RANGE_ROW_IDENTIFIER.equals(tableToJoin)) {
            hqlForFetchStep1.append(" LEFT JOIN ").append(EMAIL_IDENTIFIER).append(".").append(Email.PROPERTY_RANGE_ROW)
                    .append(" ").append(RANGE_ROW_IDENTIFIER);
        }

        if (orderClause != null) {
            addJoinsForOrderBy(orderClause, hqlForFetchStep1);
        }

        hqlForFetchStep1.append(" ").append(query).append(" GROUP BY ")
                .append(EMAIL_IDENTIFIER).append(".").append(TopiaEntity.PROPERTY_TOPIA_ID);
        // on ajoute les autres colones d'ordre dans le groupe by
        for (int indexClause = 1; indexClause < pagination.getOrderClauses().size(); indexClause++) {
            String clause = pagination.getOrderClauses().get(indexClause).getClause();
            if (!(EMAIL_IDENTIFIER + "." + TopiaEntity.PROPERTY_TOPIA_ID).equals(clause))
                hqlForFetchStep1.append(", ").append(clause);
        }
        if (orderClause != null && !orderByWithSum && !orderByWithSize) {
            hqlForFetchStep1.append(", ").append(orderClause);
        } else if (orderByWithSize && tableToJoin != null) {
            hqlForFetchStep1.append(", ").append(tableToJoin).append(".").append(TopiaEntity.PROPERTY_TOPIA_ID);
        }
        if (log.isTraceEnabled()) {
            log.trace("getEmailForFolder 1 - " + hqlForFetchStep1);
        }
        String hqlForFetchStep2 = "select distinct E FROM " + Email.class.getName() + " E " + addAllFecthes(
                Email.PROPERTY_PRIORITY,
                Email.PROPERTY_DEMAND_TYPE,
                Email.PROPERTY_CLIENT,
                Email.PROPERTY_WAITING_STATE,
                Email.PROPERTY_TAKEN_BY,
                Email.PROPERTY_LAST_ATTACHMENT_OPENER,
                Email.PROPERTY_LAST_PRINTING_USER
        ) + " WHERE " + EMAIL_IDENTIFIER + "." + TopiaEntity.PROPERTY_TOPIA_ID + " in ( :topiaIdsForFetch_ )";

        if (log.isTraceEnabled()) {
            log.trace("getEmailForFolder 2 - " + hqlForFetchStep2);
        }

        // code copied from topia to perform search
        long time = new Date().getTime();
        if (log.isTraceEnabled()) {
            log.trace("getEmailForFolder start query 1 : " + time);
        }

        PaginationResult pageResult = findPage(EMAIL_IDENTIFIER, hqlForFetchStep1.toString(), args, pagination);
        long time2 = new Date().getTime();
        if (log.isTraceEnabled()) {
            log.trace("getEmailForFolder end query 1 : " + time2 + " (" + (time2 - time) + ")");
        }

        List<String> step1ResultTopiaIds;
        if (orderClause != null && (!orderByWithSize || tableToJoin != null)) {
            step1ResultTopiaIds = Lists.transform(pageResult.getElements(), new Function<Object[], String>() {
               @Override
               public String apply(Object[] input) {
                   return String.valueOf(input[0]);
               }
            });
        } else {
            step1ResultTopiaIds = pageResult.getElements();
        }

        List<Email> sortedEntities;
        if (CollectionUtils.isEmpty(step1ResultTopiaIds)) {
            sortedEntities = Lists.newArrayList();
        } else {
            Map<String, Object> step2Args = Maps.newHashMap();
            step2Args.put("topiaIdsForFetch_", step1ResultTopiaIds);

            time = new Date().getTime();
            if (log.isTraceEnabled()) {
                log.trace("getEmailForFolder start query 2 : " + time);
            }

            List<Email> entities = forHql(hqlForFetchStep2, step2Args).findAll();

            time2 = new Date().getTime();
            if (log.isTraceEnabled()) {
                log.trace("getEmailForFolder end query 2 : " + time2 + " (" + (time2 - time) + ")");
            }
            sortedEntities = sortAccordingToIds(entities, step1ResultTopiaIds);

            long time3 = new Date().getTime();
            if (log.isTraceEnabled()) {
                log.trace("getEmailForFolder sort query 2 : " + time3 + " (" + (time3 - time2) + ")");
            }
        }

        time = new Date().getTime();
        if (log.isDebugEnabled()) {
            log.trace("getEmailForFolder init results : " + time);
        }

        PaginationResult<Email> result = PaginationResult.of(sortedEntities, pageResult.getCount(), pageResult.getCurrentPage());

        time2 = new Date().getTime();
        if (log.isTraceEnabled()) {
            log.trace("getEmailForFolder after pagination results : " + time2 + " (" + (time2 - time) + ")");
        }

        for (Email email : result.getElements()) {
            List<RangeRow> rangeRows = email.getRangeRow();
            if (rangeRows != null) {
                for (RangeRow rangeRow : rangeRows) {
                    Hibernate.initialize(rangeRow.getRange());
                }
            }
            Hibernate.initialize(email.getReplies());
            Hibernate.initialize(email.getAttachment());
            EmailGroup emailGroup = email.getEmailGroup();
            if (emailGroup != null) {
                Hibernate.initialize(emailGroup.getEmail());
                for (Email groupedEmail : emailGroup.getEmail()) {
                    // initialize folder of the grouped demand to transmit the grouped emails of the same folder
                    Hibernate.initialize(groupedEmail.getMailFolder());
                    // initialize the attachments for the attachments in the replies
                    Hibernate.initialize(groupedEmail.getAttachment());
                }
            }
        }

        long time3 = new Date().getTime();
        if (log.isTraceEnabled()) {
            log.trace("getEmailForFolder after hibernate initializing : " + time3 + " (" + (time3 - time2) + ")");
        }
        if (log.isDebugEnabled()) {
            log.debug("getEmailFolder total : " + (time3 - start));
        }

        return result;
    }

    protected void addJoinsForOrderBy(String clause, StringBuilder hqlForFetchStep1) {
        String clauseNoSize = clause.replaceAll("SIZE\\((.*)\\)", "$1");
        int dotIndex = clauseNoSize.indexOf('.');
        if (dotIndex >= 0) {
            String tableToJoin = clauseNoSize.substring(0, dotIndex);
            switch (tableToJoin) {
                case CLIENT_IDENTIFIER:
                    hqlForFetchStep1.append(" LEFT JOIN " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_CLIENT + " " + CLIENT_IDENTIFIER);
                    break;
                case PRIORITY_IDENTIFIER:
                    hqlForFetchStep1.append(" LEFT JOIN " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_PRIORITY + " " + PRIORITY_IDENTIFIER);
                    break;
                case WAITING_STATE_IDENTIFIER:
                    hqlForFetchStep1.append(" LEFT JOIN " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_WAITING_STATE + " " + WAITING_STATE_IDENTIFIER);
                    break;
                case DEMAND_TYPE_IDENTIFIER:
                    hqlForFetchStep1.append(" LEFT JOIN " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_TYPE + " " + DEMAND_TYPE_IDENTIFIER);
                    break;
                case TAKEN_BY_IDENTIFIER:
                    hqlForFetchStep1.append(" LEFT JOIN " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_TAKEN_BY + " " + TAKEN_BY_IDENTIFIER);
                    break;
                case LAST_ATTACHMENT_OPENER_IDENTIFIER:
                    hqlForFetchStep1.append(" LEFT JOIN " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_LAST_ATTACHMENT_OPENER + " " + LAST_ATTACHMENT_OPENER_IDENTIFIER);
                    break;
                case LAST_PRINTING_USER_IDENTIFIER:
                    hqlForFetchStep1.append(" LEFT JOIN " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_LAST_PRINTING_USER + " " + LAST_PRINTING_USER_IDENTIFIER);
                    break;
                case GROUP_IDENTIFIER:
                    hqlForFetchStep1.append(" LEFT JOIN " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_EMAIL_GROUP + " " + GROUP_IDENTIFIER);
                    break;
            }
        }
    }

    protected String filteredDemandsOfFolderCondition(MailFolder folder, EmailFilter emailFilter, Map<String, Object> args) {
        StringBuilder query = new StringBuilder();

        query.append(" WHERE " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_MAIL_FOLDER + " = :folder AND " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_STATUS + " != :archiveStatus");
        args.put("folder", folder);
        args.put("archiveStatus", DemandStatus.ARCHIVED);

        Set<Date> receptionDates = emailFilter.getReceptionDates();
        if (receptionDates != null) {
            query.append(" AND (");
            int i = 0;
            for (Date date : receptionDates) {
                query.append(" " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RECEPTION_DATE + " BETWEEN :" + EmailFilter.PROPERTY_RECEPTION_DATES + i + "Min");
                query.append(" AND :" + EmailFilter.PROPERTY_RECEPTION_DATES + i + "Max");

                args.put(EmailFilter.PROPERTY_RECEPTION_DATES + i + "Min", date);
                args.put(EmailFilter.PROPERTY_RECEPTION_DATES + i + "Max", DateUtils.addMinutes(date, 1));

                if (i++ < receptionDates.size() - 1) {
                    query.append(" OR");
                }
            }
            query.append(")");
        }

        Set<DemandStatus> demandStatus = emailFilter.getDemandStatus();
        if (demandStatus != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_STATUS + " in (:" + EmailFilter.PROPERTY_DEMAND_STATUS + ")");
            args.put(EmailFilter.PROPERTY_DEMAND_STATUS, demandStatus);

            if (demandStatus.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_STATUS + " IS NULL");
            }
            query.append(")");
        }

        Set<String> senders = emailFilter.getSenders();
        if (senders != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_SENDER + " in (:" + EmailFilter.PROPERTY_SENDERS + ")");
            args.put(EmailFilter.PROPERTY_SENDERS, senders);

            if (senders.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_SENDER + " IS NULL");
            }
            query.append(")");
        }

        Set<String> clientBrands = emailFilter.getClientBrands();
        if (clientBrands != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_CLIENT + "." + Client.PROPERTY_BRAND + " IN (:" + EmailFilter.PROPERTY_CLIENT_BRANDS + ")");
            args.put(EmailFilter.PROPERTY_CLIENT_BRANDS, clientBrands);

            if (clientBrands.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_CLIENT + " IS NULL");
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_CLIENT + "." + Client.PROPERTY_BRAND + " IS NULL");
            }
            query.append(")");
        }

        Set<String> clientCodes = emailFilter.getClientCodes();
        if (clientCodes != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_CLIENT + "." + Client.PROPERTY_CODE + " IN (:" + EmailFilter.PROPERTY_CLIENT_CODES + ")");
            args.put(EmailFilter.PROPERTY_CLIENT_CODES, clientCodes);

            if (clientCodes.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_CLIENT + " IS NULL");
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_CLIENT + "." + Client.PROPERTY_CODE + " IS NULL");
            }
            query.append(")");
        }

        Set<String> clientNames = emailFilter.getClientNames();
        if (clientNames != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_CLIENT + "." + Client.PROPERTY_NAME + " IN (:" + EmailFilter.PROPERTY_CLIENT_NAMES + ")");
            args.put(EmailFilter.PROPERTY_CLIENT_NAMES, clientNames);

            if (clientNames.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_CLIENT + " IS NULL");
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_CLIENT + "." + Client.PROPERTY_NAME + " IS NULL");
            }
            query.append(")");
        }

        Set<String> comments = emailFilter.getComments();
        if (comments != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMMENT + " in (:" + EmailFilter.PROPERTY_COMMENTS + ")");
            args.put(EmailFilter.PROPERTY_COMMENTS, comments);

            if (comments.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMMENT + " IS NULL");
            }
            query.append(")");
        }

        Set<String> demandObjects = emailFilter.getDemandObjects();
        if (demandObjects != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_OBJECT + " in (:" + EmailFilter.PROPERTY_DEMAND_OBJECTS + ")");
            args.put(EmailFilter.PROPERTY_DEMAND_OBJECTS, demandObjects);

            if (demandObjects.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_OBJECT + " IS NULL");
            }
            query.append(")");
        }

        Set<DemandType> demandTypes = emailFilter.getDemandTypes();
        if (demandTypes != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_TYPE + " in (:" + EmailFilter.PROPERTY_DEMAND_TYPES + ")");
            args.put(EmailFilter.PROPERTY_DEMAND_TYPES, demandTypes);

            if (demandTypes.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_TYPE + " IS NULL");
            }
            query.append(")");
        }

        Set<String> ediCodeNumbers = emailFilter.getEdiCodeNumbers();
        if (ediCodeNumbers != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_EDI_ERROR + " in (:" + EmailFilter.PROPERTY_EDI_CODE_NUMBERS + ")");
            args.put(EmailFilter.PROPERTY_EDI_CODE_NUMBERS, ediCodeNumbers);

            if (ediCodeNumbers.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_EDI_ERROR + " IS NULL");
            }
            query.append(")");
        }

        Set<FaxToMailUser> takenBys = emailFilter.getTakenBys();
        if (takenBys != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_TAKEN_BY + " in (:" + EmailFilter.PROPERTY_TAKEN_BYS + ")");
            args.put(EmailFilter.PROPERTY_TAKEN_BYS, takenBys);

            if (takenBys.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_TAKEN_BY + " IS NULL");
            }
            query.append(")");
        }

        Set<FaxToMailUser> lastAttachmentOpeners = emailFilter.getLastAttachmentOpeners();
        if (lastAttachmentOpeners != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_LAST_ATTACHMENT_OPENER + " in (:" + EmailFilter.PROPERTY_LAST_ATTACHMENT_OPENERS + ")");
            args.put(EmailFilter.PROPERTY_LAST_ATTACHMENT_OPENERS, lastAttachmentOpeners);

            if (lastAttachmentOpeners.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_LAST_ATTACHMENT_OPENER + " IS NULL");
            }
            query.append(")");
        }

        Set<FaxToMailUser> lastPrintingUsers = emailFilter.getLastPrintingUsers();
        if (lastPrintingUsers != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_LAST_PRINTING_USER + " in (:" + EmailFilter.PROPERTY_LAST_PRINTING_USERS + ")");
            args.put(EmailFilter.PROPERTY_LAST_PRINTING_USERS, lastPrintingUsers);

            if (lastPrintingUsers.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_LAST_PRINTING_USER + " IS NULL");
            }
            query.append(")");
        }

        Set<Date> lastPrintingDates = emailFilter.getLastPrintingDates();
        if (lastPrintingDates != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_LAST_PRINTING_DATE + " in (:" + EmailFilter.PROPERTY_LAST_PRINTING_DATES + ")");
            args.put(EmailFilter.PROPERTY_LAST_PRINTING_DATES, lastPrintingDates);

            if (lastPrintingDates.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_LAST_PRINTING_DATE + " IS NULL");
            }
            query.append(")");
        }

        Set<String> recipients = emailFilter.getRecipients();
        if (recipients != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RECIPIENT + " in (:" + EmailFilter.PROPERTY_RECIPIENTS + ")");
            args.put(EmailFilter.PROPERTY_RECIPIENTS, recipients);

            if (recipients.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RECIPIENT + " IS NULL");
            }
            query.append(")");
        }

        Set<Priority> priorities = emailFilter.getPriorities();
        if (priorities != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_PRIORITY + " in (:" + EmailFilter.PROPERTY_PRIORITIES + ")");
            args.put(EmailFilter.PROPERTY_PRIORITIES, priorities);

            if (priorities.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_PRIORITY + " IS NULL");
            }
            query.append(")");
        }

        Set<Range> ranges = emailFilter.getRanges();
        if (CollectionUtils.isNotEmpty(ranges)) {
            query.append(" AND ((EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + "))"
                                 + " AND (select count(*) FROM " + RangeRow.class.getName() + " RR"
                                 + " WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + ") AND RR." + RangeRow.PROPERTY_RANGE
                                 + " IN (:" + EmailFilter.PROPERTY_RANGES + ")");
            query.append(") > 0");

            if (ranges.contains(null)) {
                query.append(" OR NOT EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + "))");
            }
            query.append("))");

            args.put(EmailFilter.PROPERTY_RANGES, ranges);
        }

        Set<Long> productQuantities = emailFilter.getProductsQuantities();
        if (CollectionUtils.isNotEmpty(productQuantities)) {
            query.append(" AND ((EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + "))"
                                 + " AND (select sum(RR." + RangeRow.PROPERTY_PRODUCT_QUANTITY + ") " +
                                 "FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + ")");
            query.append(") IN (:" + EmailFilter.PROPERTY_PRODUCT_QUANTITIES + ")");

            if (productQuantities.contains(null) || productQuantities.contains(0L)) {
                query.append(" OR NOT EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + "))");
            }
            query.append("))");

            args.put(EmailFilter.PROPERTY_PRODUCT_QUANTITIES, productQuantities);
        }

        Set<Long> savQuantities = emailFilter.getSavQuantities();
        if (CollectionUtils.isNotEmpty(savQuantities)) {
            query.append(" AND ((EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + "))"
                         + " AND (select sum(RR." + RangeRow.PROPERTY_SAV_QUANTITY + ") " +
                                 "FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + ")");
            query.append(") IN (:" + EmailFilter.PROPERTY_SAV_QUANTITIES + ")");

            if (savQuantities.contains(null) || savQuantities.contains(0L)) {
                query.append(" OR NOT EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + "))");
            }
            query.append("))");

            args.put(EmailFilter.PROPERTY_SAV_QUANTITIES, savQuantities);
        }

        Set<Long> quotationQuantities = emailFilter.getQuotationQuantities();
        if (CollectionUtils.isNotEmpty(quotationQuantities)) {
            query.append(" AND ((EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + "))"
                                 + " AND (select sum(RR." + RangeRow.PROPERTY_QUOTATION_QUANTITY + ") " +
                                 "FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + ")");
            query.append(") IN (:" + EmailFilter.PROPERTY_QUOTATION_QUANTITIES + ")");

            if (quotationQuantities.contains(null) || quotationQuantities.contains(0L)) {
                query.append(" OR NOT EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + "))");
            }
            query.append("))");

            args.put(EmailFilter.PROPERTY_QUOTATION_QUANTITIES, quotationQuantities);
        }

        Set<String> projectReferences = emailFilter.getProjectReferences();
        if (projectReferences != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_PROJECT_REFERENCE + " in (:" + EmailFilter.PROPERTY_PROJECT_REFERENCES + ")");
            args.put(EmailFilter.PROPERTY_PROJECT_REFERENCES, projectReferences);

            if (projectReferences.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_PROJECT_REFERENCE + " IS NULL");
            }
            query.append(")");
        }

        Set<String> localReferences = emailFilter.getLocalReferences();
        if (localReferences != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMPANY_REFERENCE + " in (:" + EmailFilter.PROPERTY_LOCAL_REFERENCES + ")");
            args.put(EmailFilter.PROPERTY_LOCAL_REFERENCES, localReferences);

            if (localReferences.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMPANY_REFERENCE + " IS NULL");
            }
            query.append(")");
        }

        Set<String> references = emailFilter.getReferences();
        if (references != null) {
            query.append(" AND ((EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + ")");
            query.append("   AND RR." + RangeRow.PROPERTY_COMMAND_NUMBER + " IN (:" + EmailFilter.PROPERTY_LOCAL_REFERENCES + "))");
            query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMPANY_REFERENCE + " IN (:" + EmailFilter.PROPERTY_LOCAL_REFERENCES + ")");
            args.put(EmailFilter.PROPERTY_LOCAL_REFERENCES, references);

            if (references.contains(null)) {
                query.append(" OR (NOT EXISTS (FROM " + RangeRow.class.getName() + " RR WHERE RR in elements(" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_RANGE_ROW + "))");
                query.append(" AND " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMPANY_REFERENCE + " IS NULL)");
            }
            query.append("))");
        }

        Set<WaitingState> waitingStates = emailFilter.getWaitingStates();
        if (waitingStates != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_WAITING_STATE + " in (:" + EmailFilter.PROPERTY_WAITING_STATES + ")");
            args.put(EmailFilter.PROPERTY_WAITING_STATES, waitingStates);

            if (waitingStates.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_WAITING_STATE + " IS NULL");
            }
            query.append(")");
        }

        Set<String> subjects = emailFilter.getSubjects();
        if (subjects != null) {
            query.append(" AND (" + EMAIL_IDENTIFIER + "." + Email.PROPERTY_SUBJECT + " in (:" + EmailFilter.PROPERTY_SUBJECTS + ")");
            args.put(EmailFilter.PROPERTY_SUBJECTS, subjects);

            if (subjects.contains(null)) {
                query.append(" OR " + EMAIL_IDENTIFIER + "." + Email.PROPERTY_SUBJECT + " IS NULL");
            }
            query.append(")");
        }

        return query.toString();
    }

    /**
     * Retounre le nombre d'email par dossier.
     * 
     * @return le nombre d'email par dossier
     */
    public Map<String, Long> getMailCountByFolder() {
        String query = "SELECT mailFolder.topiaId, count(*) FROM " + Email.class.getName() + " group by mailFolder.topiaId";
        
        Map<String, Long> result = new HashMap<>();
        
        List<Object[]> queryResuts = findAll(query);
        for (Object[] queryResut : queryResuts) {
            String mailFolder = (String)queryResut[0];
            Long count = (Long)queryResut[1];
            result.put(mailFolder, count);
        }
        return result;
    }

    public QuantitiesByRange computeQuantitiesByRange(List<MailFolder> folders, boolean computeQuantitiesSubtotalsByState) {
        //compute inProgressQuantitiesByRange
        //compute inProgressQuantitiesSubtotal
        //correspond aux lignes sans état d'attente
        String allInProgressRangesQuery = "SELECT DISTINCT range" +
                " FROM " + Email.class.getName() + " AS email " +
                " INNER JOIN email." + Email.PROPERTY_RANGE_ROW + " AS rangeRow" +
                " INNER JOIN rangeRow." + RangeRow.PROPERTY_RANGE + " AS range" +
                " WHERE email." + Email.PROPERTY_MAIL_FOLDER + " IN :folders" +
                " AND email." + Email.PROPERTY_WAITING_STATE + " IS NULL" +
                " AND email." + Email.PROPERTY_DEMAND_STATUS + " <> '" + DemandStatus.ARCHIVED +  "'";

        Map<String, Object> args = new HashMap<>();
        args.put("folders", folders);
        List<Range> inProgressAllRanges = findAll(allInProgressRangesQuery, args);
        Map<String, Range> allInProgressRangesById = Maps.uniqueIndex(inProgressAllRanges, TopiaEntities.getTopiaIdFunction());

        // this has been modified from
        // SELECT range
        // to
        // SELECT range.topiaId
        // do to bug https://hibernate.atlassian.net/browse/HHH-1615 that can be reproduced
        // on sql server

        String inProgressQuantitiesByRangeQuery = "SELECT range." + TopiaEntity.PROPERTY_TOPIA_ID + "," +
                " SUM(rangeRow." + RangeRow.PROPERTY_PRODUCT_QUANTITY + ") AS prodQ," +
                " SUM(rangeRow." + RangeRow.PROPERTY_SAV_QUANTITY + ") AS savQ," +
                " SUM(rangeRow." + RangeRow.PROPERTY_QUOTATION_QUANTITY + ") AS quotQ" +
                " FROM " + Email.class.getName() + " AS email " +
                " INNER JOIN email." + Email.PROPERTY_RANGE_ROW + " AS rangeRow" +
                " INNER JOIN rangeRow." + RangeRow.PROPERTY_RANGE + " AS range" +
                " WHERE email." + Email.PROPERTY_MAIL_FOLDER + " IN :folders" +
                " AND email." + Email.PROPERTY_WAITING_STATE + " IS NULL" +
                " AND email." + Email.PROPERTY_DEMAND_STATUS + " <> '" + DemandStatus.ARCHIVED +  "'" +
                " GROUP BY range";

        args = new HashMap<>();
        args.put("folders", folders);

        Map<Range, Quantities> inProgressQuantitiesByRange = new TreeMap<>(new Comparator<Range>() {
            @Override
            public int compare(Range o1, Range o2) {
                return o1.getLabel().compareTo(o2.getLabel());
            }
        });
        Long inProgressProductTotalQuantity = 0L;
        Long inProgressSavTotalQuantity = 0L;
        Long inProgressQuotationTotalQuantity = 0L;

        List<Object[]> inProgressQueryResuts = findAll(inProgressQuantitiesByRangeQuery, args);
        for (Object[] queryResut : inProgressQueryResuts) {
            Range range = allInProgressRangesById.get(String.valueOf(queryResut[0]));

            Long productQuantity = (Long) queryResut[1];
            if (productQuantity != null) {
                inProgressProductTotalQuantity += productQuantity;
            }

            Long savQuantity = (Long) queryResut[2];
            if (savQuantity != null) {
                inProgressSavTotalQuantity += savQuantity;
            }

            Long quotationQuantity = (Long) queryResut[3];
            if (quotationQuantity != null) {
                inProgressQuotationTotalQuantity += quotationQuantity;
            }

            Quantities quantities = new Quantities(productQuantity, savQuantity, quotationQuantity);
            inProgressQuantitiesByRange.put(range, quantities);
        }

        Quantities inProgressTotalQuantities = new Quantities(inProgressProductTotalQuantity, inProgressSavTotalQuantity, inProgressQuotationTotalQuantity);

        //compute waitingQuantitiesByRange
        //compute waitingQuantitiesSubtotal
        //correspond aux lignes "avec état d'attente"
        String allWaitingRangesQuery = "SELECT DISTINCT range" +
                " FROM " + Email.class.getName() + " AS email " +
                " INNER JOIN email." + Email.PROPERTY_RANGE_ROW + " AS rangeRow" +
                " INNER JOIN rangeRow." + RangeRow.PROPERTY_RANGE + " AS range" +
                " WHERE email." + Email.PROPERTY_MAIL_FOLDER + " IN :folders" +
                " AND email." + Email.PROPERTY_WAITING_STATE + " IS NOT NULL" +
                " AND email." + Email.PROPERTY_DEMAND_STATUS + " <> '" + DemandStatus.ARCHIVED + "'" ;

        args = new HashMap<>();
        args.put("folders", folders);
        List<Range> waitingAllRanges = findAll(allWaitingRangesQuery, args);
        Map<String, Range> allWaitingRangesById = Maps.uniqueIndex(waitingAllRanges, TopiaEntities.getTopiaIdFunction());

        // this has been modified from
        // SELECT range
        // to
        // SELECT range.topiaId
        // do to bug https://hibernate.atlassian.net/browse/HHH-1615 that can be reproduced
        // on sql server

        String waitingQuantitiesByRangeQuery = "SELECT range." + TopiaEntity.PROPERTY_TOPIA_ID + "," +
                " SUM(rangeRow." + RangeRow.PROPERTY_PRODUCT_QUANTITY + ") AS prodQ," +
                " SUM(rangeRow." + RangeRow.PROPERTY_SAV_QUANTITY + ") AS savQ," +
                " SUM(rangeRow." + RangeRow.PROPERTY_QUOTATION_QUANTITY + ") AS quotQ" +
                " FROM " + Email.class.getName() + " AS email " +
                " INNER JOIN email." + Email.PROPERTY_RANGE_ROW + " AS rangeRow" +
                " INNER JOIN rangeRow." + RangeRow.PROPERTY_RANGE + " AS range" +
                " WHERE email." + Email.PROPERTY_MAIL_FOLDER + " IN :folders" +
                // " AND email." + Email.PROPERTY_TAKEN_BY + " IS NOT NULL" +
                " AND email." + Email.PROPERTY_WAITING_STATE + " IS NOT NULL" +
                " AND email." + Email.PROPERTY_DEMAND_STATUS + " <> '" + DemandStatus.ARCHIVED + "'" +
                " GROUP BY range";

        args = new HashMap<>();
        args.put("folders", folders);

        Map<Range, Quantities> waitingQuantitiesByRange = new TreeMap<>(new Comparator<Range>() {
            @Override
            public int compare(Range o1, Range o2) {
                return o1.getLabel().compareTo(o2.getLabel());
            }
        });
        Long waitingProductTotalQuantity = 0L;
        Long waitingSavTotalQuantity = 0L;
        Long waitingQuotationTotalQuantity = 0L;

        List<Object[]> waitingQueryResuts = findAll(waitingQuantitiesByRangeQuery, args);
        for (Object[] queryResut : waitingQueryResuts) {
            Range range = allWaitingRangesById.get(String.valueOf(queryResut[0]));

            Long productQuantity = (Long) queryResut[1];
            if (productQuantity != null) {
                waitingProductTotalQuantity += productQuantity;
            }

            Long savQuantity = (Long) queryResut[2];
            if (savQuantity != null) {
                waitingSavTotalQuantity += savQuantity;
            }

            Long quotationQuantity = (Long) queryResut[3];
            if (quotationQuantity != null) {
                waitingQuotationTotalQuantity += quotationQuantity;
            }

            Quantities quantities = new Quantities(productQuantity, savQuantity, quotationQuantity);
            waitingQuantitiesByRange.put(range, quantities);
        }

        Quantities waitingTotalQuantities = new Quantities(waitingProductTotalQuantity, waitingSavTotalQuantity, waitingQuotationTotalQuantity);

        //compute totalQuantities
        Long productTotalQuantity = waitingProductTotalQuantity + inProgressProductTotalQuantity;
        Long savTotalQuantity = waitingSavTotalQuantity + inProgressSavTotalQuantity;
        Long quotationTotalQuantity = waitingQuotationTotalQuantity + inProgressQuotationTotalQuantity;

        Quantities totalQuantities = new Quantities(productTotalQuantity, savTotalQuantity, quotationTotalQuantity);


        return new QuantitiesByRange(inProgressQuantitiesByRange, inProgressTotalQuantities, waitingQuantitiesByRange ,waitingTotalQuantities, totalQuantities);
    }

    /*
     * @deprecated because this need to be supported by topia (see http://forge.nuiton.org/issues/3427)
     */
    @Deprecated
    protected String addAllFecthes(String... fetchProperties) {
        StringBuilder hqlFromClauseBuilder = new StringBuilder();
            int fetchedPropertiesAliasIndex = 0;
            Map<String, String> aliases = Maps.newHashMap();
            for (String propertyName : fetchProperties) {

                // addAllFetches("a.b").addAllFetches("i.j.k") will produce:
                //   left join topiaEntity_.a fetchedProp0_ left join fetch fetchedProp0_.b
                //   left join topiaEntity_.i fetchedProp1_ left join fetch fetchedProp1_.j fetchedProp2_  left join fetchedProp2_.k

                // addAllFetches("a.b").addAllFetches("a.c") will produce:
                //   left join topiaEntity_.a fetchedProp0_ left join fetch fetchedProp1_.b
                //                                          left join fetch fetchedProp1_.c
                StringBuilder path = new StringBuilder(); // The path to reach the property

                // Loop over each part of the "a.b.c"
                for (String part : Splitter.on('.').split(propertyName)) {

                    // Look for the parent alias if already computed
                    String previousPath = path.toString();
                    String previousAlias = Objects.firstNonNull(aliases.get(previousPath), "E");

                    // Compute the current path and look for its alias
                    if (path.length() > 0) {
                        path.append('.');
                    }
                    path.append(part);
                    String currentPath = path.toString();
                    String currentAlias = aliases.get(currentPath);

                    // No current alias found, create and add it
                    if (Strings.isNullOrEmpty(currentAlias)) {
                        currentAlias = String.format("fetchedProp%d_", fetchedPropertiesAliasIndex++);
                        aliases.put(currentPath, currentAlias);

                        String fetch = String.format(" LEFT JOIN FETCH %s.%s %s ", previousAlias, part, currentAlias);
                        hqlFromClauseBuilder.append(fetch);
                    }
                }
            }
        return hqlFromClauseBuilder.toString();
    }

    /*
     * @deprecated beacause this need to be supported by topia (see http://forge.nuiton.org/issues/3427)
     */
    @Deprecated
    protected <O extends TopiaEntity> List<O> sortAccordingToIds(List<O> entities, final List<String> idsList) {

        // Cannot sort on second query, will sort according to the first result list
        final Map<String, O> entitiesIndex = Maps.uniqueIndex(entities, TopiaEntities.getTopiaIdFunction());
        Iterable<O> transformed = Iterables.transform(idsList, new Function<String, O>() {
            @Override
            public O apply(String input) {
                return entitiesIndex.get(input);
            }
        });

        List<O> result = Lists.newArrayList(transformed);
        return result;
    }

    public Set<Object> getDistinctValues(MailFolder folder, String[] properties, boolean sum) {
        Map<String, Object> args = new HashMap<>();
        String folderAndStatusCondition = " WHERE email." + Email.PROPERTY_MAIL_FOLDER + " = :folder" +
                " AND email." + Email.PROPERTY_DEMAND_STATUS + " != :archiveStatus";
        args.put("folder", folder);
        args.put("archiveStatus", DemandStatus.ARCHIVED);

        String propertyList;
        if (sum) {
            propertyList = "SUM(" + StringUtils.join(properties, "), SUM(") + ")";
        } else {
            propertyList = StringUtils.join(properties, ", ");
        }
        StringBuilder query = new StringBuilder("SELECT DISTINCT " + propertyList +
                    " FROM " + Email.class.getName() + " AS email");

        for (String property : properties) {
            if (property.startsWith("client.")) {
                query.append(" LEFT OUTER JOIN email." + Email.PROPERTY_CLIENT + " AS client");
                break;
            }
        }
        for (String property : properties) {
            if (property.startsWith("rangeRow.")) {
                query.append(" LEFT OUTER JOIN email." + Email.PROPERTY_RANGE_ROW + " AS rangeRow");
                break;
            }
        }
        query.append(folderAndStatusCondition);

        if (sum) {
            query.append(" GROUP BY email." + TopiaEntity.PROPERTY_TOPIA_ID);
        }

        List<Object> queryResults = findAll(query.toString(), args);
        Set<Object> result = new HashSet<>();
        if (properties.length == 1) {
            result.addAll(queryResults);

        } else {
            for (Object o1 : queryResults) {
                Object[] objects = (Object[]) o1;
                result.addAll(Arrays.asList(objects));
            }
        }
        
        // TODO echatellier 20140918 : on ajoute null dans tous les cas car les requetes precendente
        // sont trop lente juste pour savoir si on a vraiment besoin de savoir si l'on doit vraiment
        // avoir des valeurs nulles
        if (sum && result.contains(null)) {
            result.remove(null);
            result.add(0L);

        } else if (!result.contains("")) {
            result.add(null);
        }

        return result;
    }
}
