package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class StampTopiaDao extends AbstractStampTopiaDao<Stamp> {

    public Collection<Stamp> findAllForUser(FaxToMailUser user) {

        Map<String, Object> args = new HashMap<>();

        StringBuilder query = new StringBuilder("FROM " + Stamp.class.getName() + " S ");

        query.append("WHERE :user IN ELEMENTS (S." + Stamp.PROPERTY_USERS + ") ");
        args.put("user", user);

        int i = 0;
        for (FaxToMailUserGroup group : user.getUserGroups()) {
            query.append("OR :group" + i + " IN ELEMENTS (S." + Stamp.PROPERTY_GROUPS + ") ");
            args.put("group" + i++, group);
        }

        return findAll(query.toString(), args);
    }

} //StampTopiaDao
