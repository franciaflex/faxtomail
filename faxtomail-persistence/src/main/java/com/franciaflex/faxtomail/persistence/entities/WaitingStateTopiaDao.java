package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WaitingStateTopiaDao extends AbstractWaitingStateTopiaDao<WaitingState> {

    
    @Override
    public void delete(WaitingState entity) {

        // code from AbstractTopiaDao
        topiaJpaSupport.delete(entity);
        entity.notifyDeleted();
        topiaFiresSupport.notifyEntityDeleted(entity);

//        do nothing just to disable strange topia code
//        org.postgresql.util.PSQLException: Le nom de colonne topiaVersion n'a pas été trouvé dans ce ResultSet.
//        org.postgresql.jdbc2.AbstractJdbc2ResultSet.findColumn(AbstractJdbc2ResultSet.java:2728)
//        org.postgresql.jdbc2.AbstractJdbc2ResultSet.getLong(AbstractJdbc2ResultSet.java:2594)
//        com.mchange.v2.c3p0.impl.NewProxyResultSet.getLong(NewProxyResultSet.java:2625)
//        org.hibernate.type.descriptor.sql.BigIntTypeDescriptor$2.doExtract(BigIntTypeDescriptor.java:74)
//        org.hibernate.type.descriptor.sql.BasicExtractor.extract(BasicExtractor.java:64)
//        org.hibernate.type.AbstractStandardBasicType.nullSafeGet(AbstractStandardBasicType.java:267)
//        org.hibernate.type.AbstractStandardBasicType.nullSafeGet(AbstractStandardBasicType.java:263)
//        org.hibernate.type.AbstractStandardBasicType.nullSafeGet(AbstractStandardBasicType.java:253)
//        org.hibernate.type.AbstractStandardBasicType.hydrate(AbstractStandardBasicType.java:338)
//        org.hibernate.persister.entity.AbstractEntityPersister.hydrate(AbstractEntityPersister.java:2969)
//        org.hibernate.loader.Loader.loadFromResultSet(Loader.java:1695)
//        org.hibernate.loader.Loader.instanceNotYetLoaded(Loader.java:1627)
//        org.hibernate.loader.Loader.getRow(Loader.java:1514)
//        org.hibernate.loader.Loader.getRowFromResultSet(Loader.java:725)
//        org.hibernate.loader.Loader.processResultSet(Loader.java:952)
//        org.hibernate.loader.Loader.doQuery(Loader.java:920)
//        org.hibernate.loader.Loader.doQueryAndInitializeNonLazyCollections(Loader.java:354)
//        org.hibernate.loader.Loader.doList(Loader.java:2553)
//        org.hibernate.loader.Loader.doList(Loader.java:2539)
//        org.hibernate.loader.Loader.listIgnoreQueryCache(Loader.java:2369)
//        org.hibernate.loader.Loader.list(Loader.java:2364)
//        org.hibernate.loader.custom.CustomLoader.list(CustomLoader.java:353)
//        org.hibernate.internal.SessionImpl.listCustomQuery(SessionImpl.java:1873)
//        org.hibernate.internal.AbstractSessionImpl.list(AbstractSessionImpl.java:311)
//        org.hibernate.internal.SQLQueryImpl.list(SQLQueryImpl.java:141)
//        com.franciaflex.faxtomail.persistence.entities.GeneratedWaitingStateTopiaDao.delete(GeneratedEtatAttenteTopiaDao.java:65)
//        com.franciaflex.faxtomail.persistence.entities.GeneratedWaitingStateTopiaDao.delete(GeneratedEtatAttenteTopiaDao.java:1)

    }

    public Map<String, Long> getWaitingStateCountByFolder() {
        String query = "SELECT waitingState.topiaId, count(*) FROM " + Email.class.getName() + " group by waitingState.topiaId";
        
        Map<String, Long> result = new HashMap<>();
        
        List<Object[]> queryResuts = findAll(query);
        for (Object[] queryResut : queryResuts) {
            String etatAttente = (String)queryResut[0];
            Long count = (Long)queryResut[1];
            result.put(etatAttente, count);
        }
        return result;
    }
} //EtatAttenteTopiaDao
