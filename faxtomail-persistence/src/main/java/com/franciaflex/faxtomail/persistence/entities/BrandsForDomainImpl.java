package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 1.2
 */
public class BrandsForDomainImpl extends BrandsForDomainAbstract {

    @Override
    public void setBrands(List<String> brands) {
        String brandsAsString = StringUtils.join(brands, ",");
        setBrandsJson(brandsAsString);
    }

    @Override
    public List<String> getBrands() {
        String json = getBrandsJson();
        List<String> brands;
        if (json == null) {
            brands = new ArrayList<>();
        } else {
            brands = Lists.newArrayList(json.split(","));
        }
        return brands;
    }
}
