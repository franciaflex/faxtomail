package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Défini les colonnes de la table NewClient en base de données, mais non géré par Topia.
 * 
 * @author Eric Chatellier
 */
public class NewClient {

    public static final String PROPERTY_NAME = "name";
    public static final String PROPERTY_EMAIL_ADDRESS = "emailAddress";
    public static final String PROPERTY_FAX_NUMBER = "faxNumber";
    public static final String PROPERTY_CARACTERISTIC1 = "caracteristic1";
    public static final String PROPERTY_CARACTERISTIC2 = "caracteristic2";
    public static final String PROPERTY_CARACTERISTIC3 = "caracteristic3";
    public static final String PROPERTY_CODE = "code";
    public static final String PROPERTY_BRAND = "brand";
    public static final String PROPERTY_COMPANY = "company";
    public static final String PROPERTY_PERSON_IN_CHARGE = "personInCharge";

}
