package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 */
public enum HistoryType implements HasLabel {

    CREATION(n("faxtomail.historyType.creation"), true),
    OPENING(n("faxtomail.historyType.opening"), false),
    MODIFICATION(n("faxtomail.historyType.modification"), true),
    TRANSMISSION_TO_EDI(n("faxtomail.historyType.transmissionToEdi"), true),
    TRANSMITTED_TO_EDI(n("faxtomail.historyType.transmittedToEdi"), true),
    EDI_RETURN(n("faxtomail.historyType.ediReturn"), true),
    TRANSMISSION(n("faxtomail.historyType.transmission"), true),
    REPLY(n("faxtomail.historyType.reply"), false),
    PRINTING(n("faxtomail.historyType.printing"), false),
    ARCHIVED(n("faxtomail.historyType.archive"), true),
    GROUP(n("faxtomail.historyType.group"), true),
    ATTACHMENT_OPENING(n("faxtomail.historyType.attachmentOpening"), false),
    ATTACHMENT_ADDITION(n("faxtomail.historyType.attachmentAddition"), true),
    ATTACHMENT_MODIFICATION(n("faxtomail.historyType.attachmentModification"), true);

    protected String labelKey;

    protected boolean consideredAsModification;

    private HistoryType(String labelKey, boolean consideredAsModification) {
        this.labelKey = labelKey;
        this.consideredAsModification = consideredAsModification;
    }

    public String getLabel() {
        return t(labelKey);
    }

    public boolean isConsideredAsModification() {
        return consideredAsModification;
    }
}
