package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Required email fields for specific {@link WaitingState}.
 * 
 * @author Eric Chatellier
 */
public enum MailField {

    OBJECT(EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_OBJECT),
    PROJECT_REFERENCE(EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_PROJECT_REFERENCE),
    PRIORITY(EmailTopiaDao.PRIORITY_IDENTIFIER + "." + Priority.PROPERTY_LABEL),
    RANGE_ROW(null),
    CLIENT(null),
    WAITING_STATE(EmailTopiaDao.WAITING_STATE_IDENTIFIER + "." + WaitingState.PROPERTY_LABEL),
    COMPANY_REFERENCE(EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMPANY_REFERENCE),
    COMMENT(EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_COMMENT),
    DEMAND_TYPE(EmailTopiaDao.DEMAND_TYPE_IDENTIFIER + "." + DemandType.PROPERTY_LABEL),
    CLIENT_CODE(EmailTopiaDao.CLIENT_IDENTIFIER + "." + Client.PROPERTY_CODE),
    CLIENT_NAME(EmailTopiaDao.CLIENT_IDENTIFIER + "." + Client.PROPERTY_NAME),
    CLIENT_BRAND(EmailTopiaDao.CLIENT_IDENTIFIER + "." + Client.PROPERTY_BRAND),
    SENDER(EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_SENDER),
    REFERENCE(null),
    RECEPTION_DATE(EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_RECEPTION_DATE),
    RECIPIENT(EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_RECIPIENT),
    DEMAND_STATUS(EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_DEMAND_STATUS),
    PF_NB("SUM(" + EmailTopiaDao.RANGE_ROW_IDENTIFIER + "."  + RangeRow.PROPERTY_PRODUCT_QUANTITY + ")"),
    SAV_NB("SUM(" + EmailTopiaDao.RANGE_ROW_IDENTIFIER + "."  + RangeRow.PROPERTY_SAV_QUANTITY + ")"),
    QUOTATION_NB("SUM(" + EmailTopiaDao.RANGE_ROW_IDENTIFIER + "."  + RangeRow.PROPERTY_QUOTATION_QUANTITY + ")"),
    TAKEN_BY(EmailTopiaDao.TAKEN_BY_IDENTIFIER),
    LAST_ATTACHMENT_OPENING_IN_THIS_FOLDER_USER(EmailTopiaDao.LAST_ATTACHMENT_OPENER_IDENTIFIER),
    REPLIES("SIZE(" + EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_REPLIES + ")"),
    ATTACHMENT("SIZE(" + EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_ATTACHMENT + ")"),
    GROUP("SIZE(" + EmailTopiaDao.GROUP_IDENTIFIER + "." + EmailGroup.PROPERTY_EMAIL + ")"),
    EDI_RETURN(EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_EDI_ERROR),
    SUBJECT(EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_SUBJECT),
    LAST_PRINTING_USER(EmailTopiaDao.LAST_PRINTING_USER_IDENTIFIER),
    LAST_PRINTING_DATE(EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_LAST_PRINTING_DATE);

    private String orderProperty;

    MailField(String orderProperty) {
        this.orderProperty = orderProperty;
    }

    public String getOrderProperty() {
        return orderProperty;
    }

    public static MailField[] getCanBeRequiredMailFields() {
        return new MailField[] {
                OBJECT,
                PROJECT_REFERENCE,
                PRIORITY,
                RANGE_ROW,
                CLIENT,
                WAITING_STATE,
                COMPANY_REFERENCE,
                COMMENT
        };
    }

    public static MailField[] getTableFields() {
        return new MailField[] {
                PRIORITY,
                DEMAND_TYPE,
                OBJECT,
                CLIENT_CODE,
                CLIENT_NAME,
                CLIENT_BRAND,
                SENDER,
                SUBJECT,
                PROJECT_REFERENCE,
                COMPANY_REFERENCE,
                REFERENCE,
                RECEPTION_DATE,
                RECIPIENT,
                DEMAND_STATUS,
                WAITING_STATE,
                RANGE_ROW,
                PF_NB,
                SAV_NB,
                QUOTATION_NB,
                TAKEN_BY,
                COMMENT,
                EDI_RETURN,
                LAST_ATTACHMENT_OPENING_IN_THIS_FOLDER_USER,
                REPLIES,
                ATTACHMENT,
                GROUP,
                LAST_PRINTING_USER,
                LAST_PRINTING_DATE
        };
    }
}
