package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

public class ExtensionCommandTopiaDao extends AbstractExtensionCommandTopiaDao<ExtensionCommand> {
    
    public ExtensionCommand findByExtension(String extension) {
        String query = "FROM " + ExtensionCommand.class.getName() +
                " WHERE lower(" + ExtensionCommand.PROPERTY_EXTENSION + ") = :extension";

        Map<String, Object> args = new HashMap<>();
        args.put("extension", extension);
        ExtensionCommand result = findUniqueOrNull(query, args);
        return result;
    }

} //ExtensionCommandTopiaDao
