package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

import java.util.Date;
import java.util.List;

public class SearchFilter extends AbstractSerializableBean {

    /** serialVersionUID. */
    private static final long serialVersionUID = 7039570396201559352L;

    public static final String PROPERTY_SENDER = "sender";
    public static final String PROPERTY_DEMAND_OBJECT = "demandObject";
    public static final String PROPERTY_DEMAND_SUBJECT = "demandSubject";
    public static final String PROPERTY_MESSAGE = "message";
    public static final String PROPERTY_BODY = "body";
    public static final String PROPERTY_WAITING_STATES = "WaitingStates";
    public static final String PROPERTY_GAMME = "gamme";
    public static final String PROPERTY_TAKEN_BY = "takenBy";
    public static final String PROPERTY_PRIORITY = "priority";
    public static final String PROPERTY_DEMAND_TYPE = "demandType";
    public static final String PROPERTY_DEMAND_STATUS = "demandStatus";
    public static final String PROPERTY_EDI_CODE_NUMBER = "ediCodeNumber";
    public static final String PROPERTY_PROJECT_REFERENCE = "projectReference";
    public static final String PROPERTY_COMMAND_NB = "commandNb";
    public static final String PROPERTY_LOCAL_REFERENCE = "localReference";
    public static final String PROPERTY_MODIFIED_BY = "modifiedBy";
    public static final String PROPERTY_PRINTING_BY = "printingBy";
    public static final String PROPERTY_ARCHIVED_BY = "archivedBy";
    public static final String PROPERTY_TRANSFER_BY = "transferBy";
    public static final String PROPERTY_REPLY_BY = "replyBy";
    public static final String PROPERTY_MIN_MODIFICATION_DATE = "minModificationDate";
    public static final String PROPERTY_MAX_MODIFICATION_DATE = "maxModificationDate";
    public static final String PROPERTY_MIN_RECEPTION_DATE = "minReceptionDate";
    public static final String PROPERTY_MAX_RECEPTION_DATE = "maxReceptionDate";
    public static final String PROPERTY_MIN_PRINTING_DATE = "minPrintingDate";
    public static final String PROPERTY_MAX_PRINTING_DATE = "maxPrintingDate";
    public static final String PROPERTY_MIN_ARCHIVED_DATE = "minArchivedDate";
    public static final String PROPERTY_MAX_ARCHIVED_DATE = "maxArchivedDate";
    public static final String PROPERTY_MIN_TRANSFER_DATE = "minTransferDate";
    public static final String PROPERTY_MAX_TRANSFER_DATE = "maxTransferDate";
    public static final String PROPERTY_MIN_REPLY_DATE = "minReplyDate";
    public static final String PROPERTY_MAX_REPLY_DATE = "maxReplyDate";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ADD_GROUP_DEMANDS = "addGroupDemands";

    protected String sender;

    //protected String demandObject;

    protected String demandSubject;

    protected String message;
    
    protected String body;

    protected List<WaitingState> waitingStates;

    protected List<Priority> priority;

    protected List<DemandType> demandType;

    protected List<DemandStatus> demandStatus;

    protected List<Range> gamme;

    protected String ediCodeNumber;

    protected String projectReference;

    protected String commandNb;

    protected String localReference;

    protected FaxToMailUser takenBy;

    protected FaxToMailUser modifiedBy;

    protected FaxToMailUser printingBy;

    protected FaxToMailUser transferBy;

    protected FaxToMailUser archivedBy;

    protected FaxToMailUser replyBy;

    protected Date minModificationDate;

    protected Date maxModificationDate;

    protected Date minReceptionDate;

    protected Date maxReceptionDate;

    protected Date minPrintingDate;

    protected Date maxPrintingDate;

    protected Date minTransferDate;

    protected Date maxTransferDate;

    protected Date minArchivedDate;

    protected Date maxArchivedDate;

    protected Date minReplyDate;

    protected Date maxReplyDate;

    protected Client client;

    protected boolean addGroupDemands;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        String oldValue = this.sender;
        this.sender = sender;
        firePropertyChange(PROPERTY_SENDER, oldValue, sender);
    }

    /*public String getDemandObject() {
        return demandObject;
    }

    public void setDemandObject(String demandObject) {
        String oldValue = this.demandObject;
        this.demandObject = demandObject;
        firePropertyChange(PROPERTY_DEMAND_OBJECT, oldValue, demandObject);
    }*/


    public String getDemandSubject() {
        return demandSubject;
    }

    public void setDemandSubject(String demandSubject) {
        String oldValue = this.demandSubject;
        this.demandSubject = demandSubject;
        firePropertyChange(PROPERTY_DEMAND_OBJECT, oldValue, demandSubject);
    }

    public List<WaitingState> getWaitingStates() {
        return waitingStates;
    }

    public void setWaitingStates(List<WaitingState> waitingStates) {
        this.waitingStates = waitingStates;
        firePropertyChange(PROPERTY_WAITING_STATES, null, waitingStates);
    }

    public FaxToMailUser getTakenBy() {
        return takenBy;
    }

    public void setTakenBy(FaxToMailUser takenBy) {
        FaxToMailUser oldValue = this.takenBy;
        this.takenBy = takenBy;
        firePropertyChange(PROPERTY_TAKEN_BY, oldValue, takenBy);
    }

    public List<Priority> getPriority() {
        return priority;
    }

    public void setPriority(List<Priority> priority) {
        this.priority = priority;
        firePropertyChange(PROPERTY_PRIORITY, null, priority);
    }

    public List<DemandType> getDemandType() {
        return demandType;
    }

    public void setDemandType(List<DemandType> demandType) {
        this.demandType = demandType;
        firePropertyChange(PROPERTY_DEMAND_TYPE, null, demandType);
    }

    public List<DemandStatus> getDemandStatus() {
        return demandStatus;
    }

    public void setDemandStatus(List<DemandStatus> demandStatus) {
        this.demandStatus = demandStatus;
        firePropertyChange(PROPERTY_DEMAND_STATUS, null, demandStatus);
    }

    public String getEdiCodeNumber() {
        return ediCodeNumber;
    }

    public void setEdiCodeNumber(String ediCodeNumber) {
        String oldValue = this.ediCodeNumber;
        this.ediCodeNumber = ediCodeNumber;
        firePropertyChange(PROPERTY_EDI_CODE_NUMBER, oldValue, ediCodeNumber);
    }

    public String getProjectReference() {
        return projectReference;
    }

    public void setProjectReference(String projectReference) {
        String oldValue = this.projectReference;
        this.projectReference = projectReference;
        firePropertyChange(PROPERTY_PROJECT_REFERENCE, oldValue, projectReference);
    }

    public FaxToMailUser getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(FaxToMailUser modifiedBy) {
        FaxToMailUser oldValue = modifiedBy;
        this.modifiedBy = modifiedBy;
        firePropertyChange(PROPERTY_MODIFIED_BY, oldValue, modifiedBy);
    }

    public Date getMinModificationDate() {
        return minModificationDate;
    }

    public void setMinModificationDate(Date minModificationDate) {
        Date oldValue = this.minModificationDate;
        this.minModificationDate = minModificationDate;
        firePropertyChange(PROPERTY_MIN_MODIFICATION_DATE, oldValue, minModificationDate);
    }

    public Date getMaxModificationDate() {
        return maxModificationDate;
    }

    public void setMaxModificationDate(Date maxModificationDate) {
        Date oldValue = this.maxModificationDate;
        this.maxModificationDate = maxModificationDate;
        firePropertyChange(PROPERTY_MAX_MODIFICATION_DATE, oldValue, maxModificationDate);
    }

    public Date getMinReceptionDate() {
        return minReceptionDate;
    }

    public void setMinReceptionDate(Date minReceptionDate) {
        Date oldValue = this.minReceptionDate;
        this.minReceptionDate = minReceptionDate;
        firePropertyChange(PROPERTY_MIN_RECEPTION_DATE, oldValue, minReceptionDate);
    }

    public Date getMaxReceptionDate() {
        return maxReceptionDate;
    }

    public void setMaxReceptionDate(Date maxReceptionDate) {
        Date oldValue = this.maxReceptionDate;
        this.maxReceptionDate = maxReceptionDate;
        firePropertyChange(PROPERTY_MAX_RECEPTION_DATE, oldValue, maxReceptionDate);
    }

    public Date getMinPrintingDate() {
        return minPrintingDate;
    }

    public void setMinPrintingDate(Date minPrintingDate) {
        Date oldValue = this.minPrintingDate;
        this.minPrintingDate = minPrintingDate;
        firePropertyChange(PROPERTY_MIN_PRINTING_DATE, oldValue, minPrintingDate);
    }

    public Date getMaxPrintingDate() {
        return maxPrintingDate;
    }

    public void setMaxPrintingDate(Date maxPrintingDate) {
        Date oldValue = this.maxPrintingDate;
        this.maxPrintingDate = maxPrintingDate;
        firePropertyChange(PROPERTY_MAX_PRINTING_DATE, oldValue, maxPrintingDate);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        Object oldValue = getClient();
        this.client = client;
        firePropertyChange(PROPERTY_CLIENT, oldValue, client);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        String oldValue = this.message;
        this.message = message;
        firePropertyChange(PROPERTY_MESSAGE, oldValue, message);
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        String oldValue = this.body;
        this.body = body;
        firePropertyChange(PROPERTY_BODY, oldValue, body);
    }

    public List<Range> getGamme() {
        return gamme;
    }

    public void setGamme(List<Range> gamme) {
        this.gamme = gamme;
        firePropertyChange(PROPERTY_GAMME, null, gamme);
    }

    public String getCommandNb() {
        return commandNb;
    }

    public void setCommandNb(String commandNb) {
        String oldValue = this.commandNb;
        this.commandNb = commandNb;
        firePropertyChange(PROPERTY_COMMAND_NB, oldValue, commandNb);
    }

    public String getLocalReference() {
        return localReference;
    }

    public void setLocalReference(String localReference) {
        String oldValue = this.localReference;
        this.localReference = localReference;
        firePropertyChange(PROPERTY_LOCAL_REFERENCE, oldValue, localReference);
    }

    public FaxToMailUser getPrintingBy() {
        return printingBy;
    }

    public void setPrintingBy(FaxToMailUser printingBy) {
        FaxToMailUser oldValue = this.printingBy;
        this.printingBy = printingBy;
        firePropertyChange(PROPERTY_PRINTING_BY, oldValue, printingBy);
    }

    public FaxToMailUser getTransferBy() {
        return transferBy;
    }

    public void setTransferBy(FaxToMailUser transferBy) {
        FaxToMailUser oldValue = this.transferBy;
        this.transferBy = transferBy;
        firePropertyChange(PROPERTY_TRANSFER_BY, oldValue, transferBy);
    }

    public FaxToMailUser getArchivedBy() {
        return archivedBy;
    }

    public void setArchivedBy(FaxToMailUser archivedBy) {
        FaxToMailUser oldValue = this.archivedBy;
        this.archivedBy = archivedBy;
        firePropertyChange(PROPERTY_ARCHIVED_BY, oldValue, archivedBy);
    }

    public FaxToMailUser getReplyBy() {
        return replyBy;
    }

    public void setReplyBy(FaxToMailUser replyBy) {
        FaxToMailUser oldValue = this.replyBy;
        this.replyBy = replyBy;
        firePropertyChange(PROPERTY_REPLY_BY, oldValue, replyBy);
    }

    public Date getMinTransferDate() {
        return minTransferDate;
    }

    public void setMinTransferDate(Date minTransferDate) {
        Date oldValue = this.minTransferDate;
        this.minTransferDate = minTransferDate;
        firePropertyChange(PROPERTY_MIN_TRANSFER_DATE, oldValue, minTransferDate);
    }

    public Date getMaxTransferDate() {
        return maxTransferDate;
    }

    public void setMaxTransferDate(Date maxTransferDate) {
        Date oldValue = this.maxTransferDate;
        this.maxTransferDate = maxTransferDate;
        firePropertyChange(PROPERTY_MAX_TRANSFER_DATE, oldValue, maxTransferDate);
    }

    public Date getMinArchivedDate() {
        return minArchivedDate;
    }

    public void setMinArchivedDate(Date minArchivedDate) {
        Date oldValue = this.minArchivedDate;
        this.minArchivedDate = minArchivedDate;
        firePropertyChange(PROPERTY_MIN_ARCHIVED_DATE, oldValue, minArchivedDate);
    }

    public Date getMaxArchivedDate() {
        return maxArchivedDate;
    }

    public void setMaxArchivedDate(Date maxArchivedDate) {
        Date oldValue = this.maxArchivedDate;
        this.maxArchivedDate = maxArchivedDate;
        firePropertyChange(PROPERTY_MAX_ARCHIVED_DATE, oldValue, maxArchivedDate);
    }

    public Date getMinReplyDate() {
        return minReplyDate;
    }

    public void setMinReplyDate(Date minReplyDate) {
        Date oldValue = this.minReplyDate;
        this.minReplyDate = minReplyDate;
        firePropertyChange(PROPERTY_MIN_REPLY_DATE, oldValue, minReplyDate);
    }

    public Date getMaxReplyDate() {
        return maxReplyDate;
    }

    public void setMaxReplyDate(Date maxReplyDate) {
        Date oldValue = this.maxReplyDate;
        this.maxReplyDate = maxReplyDate;
        firePropertyChange(PROPERTY_MAX_REPLY_DATE, oldValue, maxReplyDate);
    }

    public boolean isAddGroupDemands() {
        return addGroupDemands;
    }

    public void setAddGroupDemands(boolean addGroupDemands) {
        Object oldValue = this.addGroupDemands;
        this.addGroupDemands = addGroupDemands;
        firePropertyChange(PROPERTY_ADD_GROUP_DEMANDS, oldValue, addGroupDemands);
    }
}
