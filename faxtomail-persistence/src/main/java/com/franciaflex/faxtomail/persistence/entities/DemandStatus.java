package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 */
public enum DemandStatus implements HasLabel {

    UNTREATED(n("faxtomail.demandStatus.untreated"), true),
    IN_PROGRESS(n("faxtomail.demandStatus.inProgress"), true),
    TRANSMISSION_TO_EDI(n("faxtomail.demandStatus.transmissionToEdi"), false),
    TRANSMITTED_TO_EDI(n("faxtomail.demandStatus.transmittedToEdi"), false),
    QUALIFIED(n("faxtomail.demandStatus.qualified"), true),
    ARCHIVED(n("faxtomail.demandStatus.archive"), false);

    protected String labelKey;

    protected boolean editableStatus;

    private DemandStatus(String labelKey, boolean editable) {
        this.labelKey = labelKey;
        this.editableStatus = editable;
    }

    public String getLabelKey() {
        return labelKey;
    }

    public String getLabel() {
        return t(labelKey);
    }

    public boolean isEditableStatus() {
        return editableStatus;
    }
}
