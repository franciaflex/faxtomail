package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id: EmailFilter.java 598 2014-09-03 13:58:29Z echatellier $
 * $HeadURL: https://svn.codelutin.com/faxtomail/trunk/faxtomail-persistence/src/main/java/com/franciaflex/faxtomail/persistence/entities/EmailFilter.java $
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

import java.util.Date;
import java.util.Set;

public class EmailFilter extends AbstractSerializableBean {

    /** serialVersionUID. */
    private static final long serialVersionUID = 7039570396201559352L;

    public static final String PROPERTY_PRIORITIES = "priorities";
    public static final String PROPERTY_DEMAND_TYPES = "demandTypes";
    public static final String PROPERTY_DEMAND_OBJECTS = "demandObjects";
    public static final String PROPERTY_CLIENT_CODES = "clientCodes";
    public static final String PROPERTY_CLIENT_NAMES = "clientNames";
    public static final String PROPERTY_CLIENT_BRANDS = "clientBrands";
    public static final String PROPERTY_SENDERS = "senders";
    public static final String PROPERTY_PROJECT_REFERENCES = "projectReferences";
    public static final String PROPERTY_LOCAL_REFERENCES = "localReferences";
    public static final String PROPERTY_REFERENCES = "references";
    public static final String PROPERTY_RECEPTION_DATES = "receptionDates";
    public static final String PROPERTY_RECIPIENTS = "recipients";
    public static final String PROPERTY_DEMAND_STATUS = "demandStatus";
    public static final String PROPERTY_RANGES = "ranges";
    public static final String PROPERTY_PRODUCT_QUANTITIES = "productsQuantities";
    public static final String PROPERTY_SAV_QUANTITIES = "savQuantities";
    public static final String PROPERTY_QUOTATION_QUANTITIES = "quotationQuantities";
    public static final String PROPERTY_TAKEN_BYS = "takenBys";
    public static final String PROPERTY_COMMENTS = "comments";
    public static final String PROPERTY_EDI_CODE_NUMBERS = "ediCodeNumbers";
    public static final String PROPERTY_LAST_ATTACHMENT_OPENERS = "lastAttachmentOpeners";
    public static final String PROPERTY_WAITING_STATES = "waitingStates";
    public static final String PROPERTY_SUBJECTS = "subjects";
    public static final String PROPERTY_LAST_PRINTING_USERS = "lastPrintingUsers";
    public static final String PROPERTY_LAST_PRINTING_DATES = "lastPrintingDates";

    protected Set<Priority> priorities;

    protected Set<DemandType> demandTypes;

    protected Set<String> demandObjects;

    protected Set<String> clientCodes;

    protected Set<String> clientNames;

    protected Set<String> clientBrands;

    protected Set<String> senders;

    protected Set<String> projectReferences;

    protected Set<String> localReferences;

    protected Set<String> references;

    protected Set<Date> receptionDates;

    protected Set<String> recipients;

    protected Set<DemandStatus> demandStatus;

    protected Set<Range> ranges;

    protected Set<Long> productsQuantities;

    protected Set<Long> savQuantities;

    protected Set<Long> quotationQuantities;

    protected Set<FaxToMailUser> takenBys;

    protected Set<String> comments;

    protected Set<String> ediCodeNumbers;

    protected Set<FaxToMailUser> lastAttachmentOpeners;

    protected Set<WaitingState> waitingStates;

    protected Set<String> subjects;

    protected Set<FaxToMailUser> lastPrintingUsers;

    protected Set<Date> lastPrintingDates;

    public Set<Priority> getPriorities() {
        return priorities;
    }

    public void setPriorities(Set<Priority> priorities) {
        this.priorities = priorities;
        firePropertyChange(PROPERTY_PRIORITIES, null, priorities);
    }

    public Set<DemandType> getDemandTypes() {
        return demandTypes;
    }

    public void setDemandTypes(Set<DemandType> demandTypes) {
        this.demandTypes = demandTypes;
        firePropertyChange(PROPERTY_DEMAND_TYPES, null, demandTypes);
    }

    public Set<String> getDemandObjects() {
        return demandObjects;
    }

    public void setDemandObjects(Set<String> demandObjects) {
        this.demandObjects = demandObjects;
        firePropertyChange(PROPERTY_DEMAND_OBJECTS, null, demandObjects);
    }

    public Set<String> getClientCodes() {
        return clientCodes;
    }

    public void setClientCodes(Set<String> clientCodes) {
        this.clientCodes = clientCodes;
        firePropertyChange(PROPERTY_CLIENT_CODES, null, clientCodes);
    }

    public Set<String> getClientNames() {
        return clientNames;
    }

    public void setClientNames(Set<String> clientNames) {
        this.clientNames = clientNames;
        firePropertyChange(PROPERTY_CLIENT_NAMES, null, clientNames);
    }

    public Set<String> getClientBrands() {
        return clientBrands;
    }

    public void setClientBrands(Set<String> clientBrands) {
        this.clientBrands = clientBrands;
        firePropertyChange(PROPERTY_CLIENT_BRANDS, null, clientBrands);
    }

    public Set<String> getSenders() {
        return senders;
    }

    public void setSenders(Set<String> senders) {
        this.senders = senders;
        firePropertyChange(PROPERTY_SENDERS, null, senders);
    }

    public Set<String> getProjectReferences() {
        return projectReferences;
    }

    public void setProjectReferences(Set<String> projectReferences) {
        this.projectReferences = projectReferences;
        firePropertyChange(PROPERTY_PROJECT_REFERENCES, null, projectReferences);
    }

    public Set<String> getLocalReferences() {
        return localReferences;
    }

    public void setLocalReferences(Set<String> localReferences) {
        this.localReferences = localReferences;
        firePropertyChange(PROPERTY_LOCAL_REFERENCES, null, localReferences);
    }

    public Set<String> getReferences() {
        return references;
    }

    public void setReferences(Set<String> references) {
        this.references = references;
        firePropertyChange(PROPERTY_REFERENCES, null, references);
    }

    public Set<Date> getReceptionDates() {
        return receptionDates;
    }

    public void setReceptionDates(Set<Date> receptionDates) {
        this.receptionDates = receptionDates;
        firePropertyChange(PROPERTY_RECEPTION_DATES, null, receptionDates);
    }

    public Set<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(Set<String> recipients) {
        this.recipients = recipients;
        firePropertyChange(PROPERTY_RECIPIENTS, null, recipients);
    }

    public Set<DemandStatus> getDemandStatus() {
        return demandStatus;
    }

    public void setDemandStatus(Set<DemandStatus> demandStatus) {
        this.demandStatus = demandStatus;
        firePropertyChange(PROPERTY_DEMAND_STATUS, null, demandStatus);
    }

    public Set<Range> getRanges() {
        return ranges;
    }

    public void setRanges(Set<Range> ranges) {
        this.ranges = ranges;
        firePropertyChange(PROPERTY_RANGES, null, ranges);
    }

    public Set<Long> getProductsQuantities() {
        return productsQuantities;
    }

    public void setProductsQuantities(Set<Long> productsQuantities) {
        this.productsQuantities = productsQuantities;
        firePropertyChange(PROPERTY_PRODUCT_QUANTITIES, null, productsQuantities);
    }

    public Set<Long> getSavQuantities() {
        return savQuantities;
    }

    public void setSavQuantities(Set<Long> savQuantities) {
        this.savQuantities = savQuantities;
        firePropertyChange(PROPERTY_SAV_QUANTITIES, null, savQuantities);
    }

    public Set<Long> getQuotationQuantities() {
        return quotationQuantities;
    }

    public void setQuotationQuantities(Set<Long> quotationQuantities) {
        this.quotationQuantities = quotationQuantities;
        firePropertyChange(PROPERTY_QUOTATION_QUANTITIES, null, quotationQuantities);
    }

    public Set<FaxToMailUser> getTakenBys() {
        return takenBys;
    }

    public void setTakenBys(Set<FaxToMailUser> takenBys) {
        this.takenBys = takenBys;
        firePropertyChange(PROPERTY_TAKEN_BYS, null, takenBys);
    }

    public Set<String> getComments() {
        return comments;
    }

    public void setComments(Set<String> comments) {
        this.comments = comments;
        firePropertyChange(PROPERTY_COMMENTS, null, comments);
    }

    public Set<String> getEdiCodeNumbers() {
        return ediCodeNumbers;
    }

    public void setEdiCodeNumbers(Set<String> ediCodeNumbers) {
        this.ediCodeNumbers = ediCodeNumbers;
        firePropertyChange(PROPERTY_EDI_CODE_NUMBERS, null, ediCodeNumbers);
    }

    public Set<FaxToMailUser> getLastAttachmentOpeners() {
        return lastAttachmentOpeners;
    }

    public void setLastAttachmentOpeners(Set<FaxToMailUser> lastAttachmentOpeners) {
        this.lastAttachmentOpeners = lastAttachmentOpeners;
        firePropertyChange(PROPERTY_LAST_ATTACHMENT_OPENERS, null, lastAttachmentOpeners);
    }

    public Set<WaitingState> getWaitingStates() {
        return waitingStates;
    }

    public void setWaitingStates(Set<WaitingState> waitingStates) {
        this.waitingStates = waitingStates;
        firePropertyChange(PROPERTY_WAITING_STATES,null, waitingStates);
    }

    public Set<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<String> subjects) {
        Object oldValue = getSubjects();
        this.subjects = subjects;
        firePropertyChange(PROPERTY_SUBJECTS, oldValue, subjects);
    }

    public Set<FaxToMailUser> getLastPrintingUsers() {
        return lastPrintingUsers;
    }

    public void setLastPrintingUsers(Set<FaxToMailUser> lastPrintingUsers) {
        this.lastPrintingUsers = lastPrintingUsers;
        firePropertyChange(PROPERTY_LAST_PRINTING_USERS, null, lastPrintingUsers);
    }

    public Set<Date> getLastPrintingDates() {
        return lastPrintingDates;
    }

    public void setLastPrintingDates(Set<Date> lastPrintingDates) {
        this.lastPrintingDates = lastPrintingDates;
        firePropertyChange(PROPERTY_LAST_PRINTING_DATES, null, lastPrintingDates);
    }

    public void clearFilter() {
        setClientCodes(null);
        setWaitingStates(null);
        setDemandStatus(null);
        setQuotationQuantities(null);
        setPriorities(null);
        setClientBrands(null);
        setComments(null);
        setDemandObjects(null);
        setDemandTypes(null);
        setEdiCodeNumbers(null);
        setLastAttachmentOpeners(null);
        setTakenBys(null);
        setLocalReferences(null);
        setReferences(null);
        setProjectReferences(null);
        setRanges(null);
        setProductsQuantities(null);
        setReceptionDates(null);
        setRecipients(null);
        setSavQuantities(null);
        setSenders(null);
        setSubjects(null);
        setClientNames(null);
        setLastPrintingUsers(null);
        setLastPrintingDates(null);
    }
}
