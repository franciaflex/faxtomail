package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public class MailFilterTopiaDao extends AbstractMailFilterTopiaDao<MailFilter> {
    
    @Override
    public void delete(MailFilter entity) {

        // code from AbstractTopiaDao
        topiaJpaSupport.delete(entity);
        entity.notifyDeleted();
        topiaFiresSupport.notifyEntityDeleted(entity);

//      do nothing just to disable strange topia code
//        org.hibernate.PropertyValueException: not-null property references a null or transient value : com.franciaflex.faxtomail.persistence.entities.MailFilterImpl.mailFolder
//        org.hibernate.engine.internal.Nullability.checkNullability(Nullability.java:106)
//        org.hibernate.event.internal.DefaultDeleteEventListener.deleteEntity(DefaultDeleteEventListener.java:279)
//        org.hibernate.event.internal.DefaultDeleteEventListener.onDelete(DefaultDeleteEventListener.java:160)
//        org.hibernate.event.internal.DefaultDeleteEventListener.onDelete(DefaultDeleteEventListener.java:73)
//        org.hibernate.internal.SessionImpl.fireDelete(SessionImpl.java:920)
//        org.hibernate.internal.SessionImpl.delete(SessionImpl.java:896)
//        org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport.delete(HibernateTopiaJpaSupport.java:218)
//        org.nuiton.topia.persistence.internal.AbstractTopiaDao.delete(AbstractTopiaDao.java:335)

    }

} //MailFilterTopiaDao
