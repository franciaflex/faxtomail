package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.internal.AbstractTopiaDao;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractFaxToMailTopiaDao<E extends TopiaEntity> extends AbstractTopiaDao<E> {

    private static final Log log = LogFactory.getLog(AbstractFaxToMailTopiaDao.class);

    /** Instance unique de Gson. (certainement pas le meilleur endroit pour la placer). */
    public static final Gson GSON_INSTANCE = new Gson();

    /**
     * Find all entity for current table not contained in provided collection.
     * 
     * @param others others elements
     * @return remaining elements
     */
    public List<E> forNotIn(Collection<E> others) {
        String query = "FROM " + getEntityClass().getName() + " E" +
                " WHERE E not in (:others)";
        Map<String, Object> args = new HashMap<>();
        args.put("others", others);
        return findAll(query, args);
    }

    public <K> List<K> find(String hql, Map<String, Object> hqlParameters, PaginationParameter pager) {
        if (log.isDebugEnabled()) {
            StringBuilder params = new StringBuilder();
            for (Map.Entry<String,Object> entry : hqlParameters.entrySet()) {
                params.append(entry.getKey());
                params.append(" : ");
                params.append(entry.getValue());
                params.append("\n");
            }
            log.debug("find:\n\t" + hql + "\n\t" + params.toString() + "\t" + GSON_INSTANCE.toJson(pager));
        }
        return super.find(hql, hqlParameters, pager);
    }

    /** only to log the query, in order to find why there is a java heap space exception */
    protected <O> List<O> findAll(String hql, Map<String, Object> hqlParameters) {
        if (log.isDebugEnabled()) {
            StringBuilder params = new StringBuilder();
            for (Map.Entry<String,Object> entry : hqlParameters.entrySet()) {
                params.append(entry.getKey());
                params.append(" : ");
                params.append(entry.getValue());
                params.append("\n");
            }
            log.debug("findAll:\n\t" + hql + "\t" + params);

        }
        return super.findAll(hql, hqlParameters);
    }

    protected boolean hqlContainsGroupBy(String hql) {
        return hql.toLowerCase().contains("group by");
    }

    // reprise de la méthode findPage(String hql, Map<String, Object> hqlParameters, PaginationParameter page) de AbstractTopiaDao
    // qui ne prend pas le nom de la table sur laquelle compter le topiaId en cas de jointure
    protected <O> PaginationResult<O> findPage(String topiaIdCountTable, String hql, Map<String, Object> hqlParameters, PaginationParameter page) {
        List<O> elements = find(hql, hqlParameters, page);

        String countHql = "select count(" + topiaIdCountTable + ".topiaId) ";
        if (hqlStartsWithSelect(hql)) {
            // must remove the from clause, otherwise some sql queries won't work.
            countHql += hql.substring(hql.toLowerCase().indexOf(" from "));
        } else {
            countHql += hql;
        }

        if (hqlContainsOrderBy(countHql)) {
            // must remove the order by clause, otherwise some sql queries won't work.
            countHql = countHql.substring(0, countHql.toLowerCase().indexOf("order by"));
        }

        if (hqlContainsGroupBy(countHql)) {
            // must remove the group by clause, otherwise some sql queries won't work.
            countHql = countHql.substring(0, countHql.toLowerCase().indexOf("group by"));
        }

        long count = count(countHql, hqlParameters);
        return PaginationResult.of(elements, count, page);
    }

}
