package com.franciaflex.faxtomail.persistence.entities;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class EmailImpl extends EmailAbstract {

    protected History firstOpeningHistory;

    protected History lastModificationHistory;

    protected History lastAttachmentOpeningInFolderHistory;

    public void findFirstOpeningHistory() {
        History result = null;
        Date date = null;
        Collection<History> histories = getHistory();
        if (histories != null) {
            for (History history : histories) {
                if (HistoryType.OPENING.equals(history.getType())) {
                    if (date == null || date.after(history.getModificationDate())) {
                        date = history.getModificationDate();
                        result = history;
                    }
                }
            }
        }
        firstOpeningHistory = result;
    }

    public void findLastModificationHistory() {
        History result = null;
        Date date = null;
        Collection<History> histories = getHistory();
        if (histories != null) {
            for (History history : histories) {
                if (history.getType().isConsideredAsModification()) {
                    if (date == null || date.before(history.getModificationDate())) {
                        date = history.getModificationDate();
                        result = history;
                    }
                }
            }
        }
        lastModificationHistory = result;
    }

    public void findLastAttachmentOpeningInFolderHistory() {
        History result = null;
        Collection<History> histories = getHistory();

        if (histories != null) {
            History transmissionHistory = null;
            for (History history : histories) {
                Date modificationDate = history.getModificationDate();
                if (HistoryType.ATTACHMENT_OPENING.equals(history.getType())) {
                    if ((transmissionHistory == null
                            || transmissionHistory.getModificationDate().before(modificationDate))
                            && (result == null
                            || result.getModificationDate().before(modificationDate))) {
                        result = history;
                    }

                } else if (HistoryType.TRANSMISSION.equals(history.getType())) {
                    if (transmissionHistory == null
                            || transmissionHistory.getModificationDate().before(modificationDate)) {

                        transmissionHistory = history;
                        if (result != null && result.getModificationDate().before(transmissionHistory.getModificationDate())) {
                            result = null;
                        }
                    }
                }
            }
        }
        lastAttachmentOpeningInFolderHistory = result;
    }

    protected History getFirstOpeningHistory() {
        if (firstOpeningHistory == null) {
            findFirstOpeningHistory();
        }
        return firstOpeningHistory;
    }

    @Override
    public FaxToMailUser getFirstOpeningUser() {
        History history = getFirstOpeningHistory();
        return history != null ? history.getFaxToMailUser() : null;
    }

    @Override
    public Date getFirstOpeningDate() {
        History history = getFirstOpeningHistory();
        return history != null ? history.getModificationDate() : null;
    }

    protected History getLastModificationHistory() {
        if (lastModificationHistory == null) {
            findLastModificationHistory();
        }
        return lastModificationHistory;
    }

    @Override
    public FaxToMailUser getLastModificationUser() {
        History history = getLastModificationHistory();
        return history != null ? history.getFaxToMailUser() : null;
    }

    @Override
    public Date getLastModificationDate() {
        History history = getLastModificationHistory();
        return history != null ? history.getModificationDate() : null;
    }

    protected History getLastAttachmentOpeningInThisFolderHistory() {
        if (lastAttachmentOpeningInFolderHistory == null) {
            findLastAttachmentOpeningInFolderHistory();
        }
        return lastAttachmentOpeningInFolderHistory;
    }

    @Override
    public FaxToMailUser getLastAttachmentOpeningInFolderUser() {
        History history = getLastAttachmentOpeningInThisFolderHistory();
        return history != null ? history.getFaxToMailUser() : null;
    }

    @Override
    public Date getLastAttachmentOpeningInFolderDate() {
        History history = getLastAttachmentOpeningInThisFolderHistory();
        return history != null ? history.getModificationDate() : null;
    }

    @Override
    public void setHistory(Collection<History> history) {
        firstOpeningHistory = null;
        lastModificationHistory = null;
        lastAttachmentOpeningInFolderHistory = null;
        super.setHistory(history);
    }

    public String getReference() {
        List<String> reference = new ArrayList<String>();
        if (StringUtils.isNotBlank(getCompanyReference())) {
            reference.add(getCompanyReference());
        }
        Collection<RangeRow> rangeRow = getRangeRow();
        if (rangeRow != null) {
            Collection<String> commandNumbers = Collections2.transform(rangeRow, new Function<RangeRow, String>() {
                @Override
                public String apply(RangeRow input) {
                    return input != null ? input.getCommandNumber() : "";
                }
            });
            Collections2.filter(commandNumbers, new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return StringUtils.isNotBlank(input);
                }
            });
            reference.addAll(commandNumbers);
        }
        return StringUtils.join(reference, ", ");
    }

    @Override
    public String getTitle() {
        String result = getObject();
        String ref = getReference();
        if (!ref.isEmpty()) {
            result = ref + " - " + result;
        }
        return Strings.nullToEmpty(result);
    }
}
