package com.franciaflex.faxtomail.persistence.hibernate;

/*
 * #%L
 * FaxToMail :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * See https://community.jboss.org/wiki/Java5EnumUserType.
 * and inspired by http://2lips.pl/blog/usertype-enumset-mysql-set-column.
 * 
 * @author Eric Chatellier
 *
 * @param <T> 
 */
public abstract class EnumSetUserType<T extends Enum<T>> implements UserType {

    private Class<T> typeClazz = null;
    private Class<? extends EnumSet> returnedType = null;

    public EnumSetUserType(Class<T> typeClazz) {
        this.typeClazz = typeClazz;
        returnedType = EnumSet.noneOf(typeClazz).getClass();
    }

    public Class returnedClass() {
        return returnedType;
    }

    public int[] sqlTypes() {
        return new int[] { Types.LONGVARCHAR };
    }

    public boolean isMutable() {
        return false;
    }

    /**
     * Retrieve an instance of the mapped class from a JDBC resultset.
     * Implementors should handle possibility of null values.
     * 
     * @param rs a JDBC result set
     * @param names the column names
     * @param owner the containing entity
     * @return Object
     * @throws HibernateException
     * @throws SQLException
     */
    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
        String name = rs.getString(names[0]);
        EnumSet<T> result = null;
        if (!rs.wasNull()) {
            String[] values = name.split(",");
            Set<T> enumList = new LinkedHashSet<T>();
            for (String value : values) {
                if (!value.isEmpty()) {
                    enumList.add(Enum.valueOf(typeClazz, value));
                }
            }
            if (enumList.isEmpty()) {
                result = EnumSet.noneOf(typeClazz);
            } else {
                result = EnumSet.copyOf(enumList);
            }
        }
        return result;
    }

    /**
     * Write an instance of the mapped class to a prepared statement.
     * Implementors should handle possibility of null values. A multi-column
     * type should be written to parameters starting from <tt>index</tt>.
     * 
     * @param st a JDBC prepared statement
     * @param value the object to write
     * @param index statement parameter index
     * @param session session
     * @throws HibernateException
     * @throws SQLException
     */
    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
        if (null == value) {
            st.setNull(index, Types.VARCHAR);
        } else {
            Set<T> values = (Set<T>)value;
            String sqlValue = "";
            if (!values.isEmpty()) {
                StringBuilder buf = new StringBuilder();
                for (T val : values) {
                    buf.append(val.name()).append(",");
                }
                sqlValue = buf.substring(0, buf.length() - 1);
            }
            st.setString(index, sqlValue);
        }
    }

    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    public Serializable disassemble(Object value) throws HibernateException {
        return (Enum<?>) value;
    }

    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    public boolean equals(Object x, Object y) throws HibernateException {
        if (x == y) {
            return true;
        }
        if (null == x || null == y) {
            return false;
        }
        return x.equals(y);
    }

    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }

    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
}
