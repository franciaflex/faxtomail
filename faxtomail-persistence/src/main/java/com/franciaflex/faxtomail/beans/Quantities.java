package com.franciaflex.faxtomail.beans;

/*-
 * #%L
 * FaxToMail :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * @author Kevin Morin (Code Lutin)
 * @since 2.2.3
 */
public class Quantities {

    private final Long productQuantity;

    private final Long savQuantity;

    private final Long quotationQuantity;

    public Quantities(Long productQuantity, Long savQuantity, Long quotationQuantity) {
        this.productQuantity = productQuantity;
        this.savQuantity = savQuantity;
        this.quotationQuantity = quotationQuantity;
    }

    public Long getProductQuantity() {
        return productQuantity;
    }

    public Long getSavQuantity() {
        return savQuantity;
    }

    public Long getQuotationQuantity() {
        return quotationQuantity;
    }

}
