package com.franciaflex.faxtomail;

/*
 * #%L
 * Extranet ENC-AHI :: Persistence
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.ConfigOptionDef;
import org.nuiton.util.version.Version;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import static org.nuiton.i18n.I18n.n;

public enum FaxToMailConfigurationOption implements ConfigOptionDef {

    SMTP_HOST(
            "faxtomail.smtp.host",
            "Nom d'hôte du serveur SMTP",
            null, String.class),

    SMTP_PORT(
            "faxtomail.smtp.port",
            "Le port du serveur SMTP",
            "25", Integer.class),

    SMTP_USER(
            "faxtomail.smtp.user",
            "Utilisateur du serveur smtp",
            null, String.class),

    SMTP_PASSWORD(
            "faxtomail.smtp.password",
            "Mot de passe de l'utilisateur du smtp",
            null, String.class),

    SMTP_SSL(
            "faxtomail.smtp.ssl",
            "Utilisation on non du mode ssl pour le stmp",
            Constants.FALSE, Boolean.class),

    MAIL_DELETE(
            "faxtomail.mail.delete",
            "Marque les mails comme étant supprimé sur le serveur",
            Constants.FALSE, Boolean.class),

    MAIL_EXPUNGE(
            "faxtomail.mail.expunge",
            "Supprime définitivement les mails marqués comme supprimés",
            Constants.FALSE, Boolean.class),

    DEV_MODE(
            "faxtomail.devMode",
            "Mode développement, court-circuite l'envoi de mail",
            Constants.FALSE, Boolean.class),

    USE_FOLDER_CACHE(
            "faxtomail.useFolderCache",
            "Utilisation du cache des emails des dossiers, pour éviter de recharger les éléments quand on revient sur un dossier",
            Constants.FALSE, Boolean.class),

    LOG_CONFIGURATION_FILE(
            "logConfigurationFile",
            "Chemin vers le fichier de configuration des journaux",
            null, String.class),

    IMPORT_FILE_ENCODING(
            "faxtomail.import.file.encoding",
            "Encodage des fichiers des fichiers d'import",
            StandardCharsets.UTF_8.toString(), String.class),

    LDAP_MOCK(
            "faxtomail.ldap.mock",
            "Utilisation d'un service ldap mock", Constants.FALSE, Boolean.class),

    LDAP_HOST(
            "faxtomail.ldap.host",
            "Adresse du serveur LDAP", null, String.class),

    LDAP_PORT(
            "faxtomail.ldap.port",
            "Port du serveur LDAP", "389", Integer.class),

    LDAP_USER(
            "faxtomail.ldap.user",
            "Nom d'utilisateur pour la connexion au serveur ldap", null, String.class),

    LDAP_PASSWORD(
            "faxtomail.ldap.password",
            "Mot de passe pour la connexion au serveur LDAP", null, String.class),

    LDAP_BASEDN(
            "faxtomail.ldap.basedn",
            "Mot de passe pour la connexion au serveur LDAP", "DC=mac-groupe,DC=net", String.class),

    LDAP_ADMIN_GROUPS(
            "faxtomail.ldap.admin.groups",
            "DN du groupe ldap ayant les autorisations d'accéder à la partie admin de l'interface web", null, String.class),

    LDAP_TEST_PRINCIPAL(
            "faxtomail.ldap.test.principal",
            "Principal de test pour forcer un utilsateur particulier", null, String.class),

    JOB_EDI_EXPRESSION(
            "faxtomail.job.edi.expression",
            "Expression cron de lancement du job EDI", "0 */5 * * * ?", String.class),

    JOB_MAIL_EXPRESSION(
            "faxtomail.job.mail.expression",
            "Expression cron de lancement du job Mail", "0 * * * * ?", String.class),

    JOB_CLIENT_EXPRESSION(
            "faxtomail.job.client.expression",
            "Expression cron de lancement du job Client", "0 */15 * * * ?", String.class),

    JOB_IMAGE_CLEANUP_EXPRESSION(
            "faxtomail.job.imageCleanup.expression",
            "Expression cron de lancement du job Client", "0 */1 * * * ?", String.class),

    ARCHIVE_IMPORT_COMMIT_TRESHOLD(
            "faxtomail.archiveImport.commitTreshold.expression",
            "Nombre d'archives à importer avant de commiter", "100", Integer.class),

    // TRANSIENT CONFIG
    VERSION(
            "faxtomail.version",
            n("faxtomail.config.option.version.description"),
            "",
            Version.class,
            true),

    SITE_URL(
            "faxtomail.site.url",
            n("faxtomail.config.option.site.url.description"),
            "http://maven-site.forge.codelutin.com/faxtomail",
            URL.class,
            true),

    ORGANIZATION_NAME(
            "faxtomail.organizationName",
            n("faxtomail.config.option.organizationName.description"),
            "",
            String.class,
            true),

    INCEPTION_YEAR(
            "faxtomail.inceptionYear",
            n("faxtomail.config.option.inceptionYear.description"),
            "2012",
            Integer.class,
            true),

    BASEDIR(
            "faxtomail.basedir",
            n("faxtomail.config.option.basedir.description"),
            "${user.home}/.faxtomail",
            File.class,
            true
            ),
    
    DEMO_DIRECTORY(
            "faxtomail.demo.directory",
            n("faxtomail.config.option.demo.directory.description"),
            "${faxtomail.basedir}/demo",
            File.class,
            true),

    DATA_DIRECTORY(
            "faxtomail.data.directory",
            n("faxtomail.config.option.data.directory.description"),
            "${faxtomail.basedir}/data",
            File.class,
            true),

    // NOT TRANSIENT CONFIG

    CSV_SEPARATOR(
            "faxtomail.csv.separator",
            n("faxtomail.config.option.csv.separator.description"),
            ";",
            char.class),

    DEFAULT_IMAGE_IF_MALFORMED_URL(
            "faxtomail.defaultImageIfMalformedUrl",
            n("faxtomail.config.option.defaultImageIfMalformedUrl.description"),
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABNCAQAAAD6SAkhAAAAAmJLR0QA/4ePzL8AAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQffDBYPJSU3laKFAAAAGXRFWHRDb21tZW50AENyZWF0ZWQgd2l0aCBHSU1QV4EOFwAAAMVJREFUaN7t1zEOgzAQRcHFh6amzoHTQoMQ2LSR8q3Z0trmycVqlm2vbtYa5/Py9l97bY6M6kNSM7qQ3IxHSHLGLSQ74wpJzzhD8jOq2hwZVct3nyGjxoOYmbGOIZkZw4+kZnQhuRmPkOSMW0h2xhWSnnGG5GdUtTkyqtocGa+XndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZndmZ/Td7B+isiSDikctVAAAAAElFTkSuQmCC",
            String.class),

    // UI

    FAXTOMAIL_LAUNCH_MODE(
            "faxtomail.launch.mode",
            n("faxtomail.config.option.launch.mode.description"),
            null,
            String.class,
            true
    ),

    FAXTOMAIL_I18N_DIRECTORY(
            "faxtomail.i18n.directory",
            n("faxtomail.config.option.i18n.directory.description"),
            "${faxtomail.basedir}/i18n",
            File.class,
            true
    ),

    START_ACTION_FILE(
            "faxtomail.startActionFile",
            n("faxtomail.config.option.startActionFile.description"),
            "${faxtomail.basedir}/faxtomail-start-action",
            File.class,
            true
    ),
    FAXTOMAIL_I18N_LOCALE(
            "faxtomail.i18n.locale",
            n("faxtomail.config.option.i18n.locale.description"),
            Locale.FRANCE.getCountry(),
            Locale.class
    ),

    UI_CONFIG_FILE(
            "faxtomail.ui.config.file",
            n("faxtomail.config.option.ui.config.file.description"),
            "${faxtomail.data.directory}/faxtomailUI.xml",
            File.class
    ),

    AUTO_POPUP_NUMBER_EDITOR(
            "faxtomail.ui.autoPopupNumberEditor",
            n("faxtomail.config.option.ui.autoPopupNumberEditor.description"),
            String.valueOf(false),
            Boolean.class
    ),

    SHOW_NUMBER_EDITOR_BUTTON(
            "faxtomail.ui.showNumberEditorButton",
            n("faxtomail.config.option.ui.showNumberEditorButton.description"),
            String.valueOf(true),
            Boolean.class
    ),

    COLOR_ALTERNATE_ROW(
            "faxtomail.ui.color.alternateRow",
            n("faxtomail.config.option.ui.color.alternateRow.description"),
            new Color(217, 217, 217).toString(),
            Color.class
    ),
    COLOR_SELECTED_ROW(
            "faxtomail.ui.color.selectedRow",
            n("faxtomail.config.option.ui.color.selectedRow.description"),
            new Color(57,105,138).toString(),
            Color.class
    ),

    /*
    UIDefaults defaults = UIManager.getLookAndFeelDefaults();
defaults.put("Table.alternateRowColor", new Color(217, 217, 217));
     */
    COLOR_BLOCKING_LAYER(
            "faxtomail.ui.color.blockingLayer",
            n("faxtomail.config.option.ui.color.blockingLayer.description"),
            new Color(200, 200, 200).toString(),
            Color.class
    ),

    COLOR_GROUPED_DEMAND_WARNING_DIALOG("faxtomail.ui.color.groupedDemandWarningDialog",
                                        n("faxtomail.ui.color.groupedDemandWarningDialog.description"),
                                        new Color(255, 255, 0).toString(),
                                        Color.class
    ),

    FONT_SIZE_GROUPED_DEMAND_WARNING_DIALOG("faxtomail.ui.fontSize.groupedDemandWarningDialog",
                                            n("faxtomail.ui.fontSize.groupedDemandWarningDialog.description"),
                                            "20.0f",
                                            Float.class
    ),

    DELAY_GROUPED_DEMAND_WARNING_DIALOG("faxtomail.ui.delay.groupedDemandWarningDialog",
                                        n("faxtomail.ui.delay.groupedDemandWarningDialog.description"),
                                        "0",
                                        Integer.class
    ),

    SHORTCUT_CLOSE_POPUP(
            "faxtomail.ui.shortcut.closePopup",
            n("faxtomail.config.option.ui.shortcut.closePopup.description"),
            "alt pressed F",
            KeyStroke.class
    ),

    DATE_FORMAT(
            "faxtomail.ui.dateFormat",
            n("faxtomail.config.option.ui.dateFormat.description"),
            "dd/MM/yyyy",
            String.class
    ),
    
    RESULT_PER_PAGE(
            "faxtomail.ui.resultPerPage",
            n("faxtomail.config.option.ui.resultPerPage.description"),
            "50",
            Integer.class
    ),

    REFRESH_LIST_INTERVAL(
            "faxtomail.ui.refreshListInterval",
            n("faxtomail.config.option.ui.refreshListInterval.description"),
            "300",
            Integer.class
    ),

    DEFAULT_ZOOM_VALUE(
            "faxtomail.ui.defaultZoomValue",
            n("faxtomail.config.option.ui.defaultZoomValue.description"),
            "1.0f",
            Float.class
    ),
    DEFAULT_ZOOM_STEP_SIZE(
            "faxtomail.ui.defaultZoomStepSize",
            n("faxtomail.config.option.ui.defaultZoomStepSize.description"),
            "0.5f",
            Float.class
    ),

    DEFAULT_PRINT_MARGIN(
            "faxtomail.ui.defaultPrintMargin",
            n("faxtomail.config.option.ui.defaultPrintMargin.description"),
            "15",
            Integer.class
    ),

    PRINT_DETAIL_PAGE(
            "faxtomail.ui.printDetailPage",
            n("faxtomail.config.option.ui.printDetailPage.description"),
            "true",
            Boolean.class
    ),

    CLIENT_COMBOBOX_FILTER_START_CHARS(
            "faxtomail.ui.clientComboBoxFilterStartChars",
            n("faxtomail.config.option.ui.clientComboBoxFilterStartChars.description"),
            "3",
            Integer.class
    ),

    MAX_WIDTH_IMAGE_IN_TEXT_PANE(
            "faxtomail.ui.maxWidthImageInTextPane",
            n("faxtomail.config.option.ui.maxWidthImageInTextPane.description"),
            "800",
            Integer.class
    ),

    IMAGEMAGICK_LOCATION(
            "faxtomail.imageMagickLocation",
            n("faxtomail.config.option.imageMagickLocation.description"),
            "magick",
            String.class
    ),

    CONVERT_LOCATION(
            "faxtomail.convertLocation",
            n("faxtomail.config.option.convertLocation.description"),
            "convert",
            String.class
    ),

    CONVERT_MAX_NUMBER(
            "faxtomail.convert.maxNumber",
            n("faxtomail.config.option.convert.maxNumber.description"),
            "1000",
            Integer.class
    ),

    CONVERT_MAX_SIZE(
            "faxtomail.convert.maxSize",
            n("faxtomail.config.option.convert.maxSize.description"),
            "1024",
            Integer.class
    ),

    CONVERT_QUALITY(
            "faxtomail.convert.quality",
            n("faxtomail.config.option.convert.maxSize.description"),
            "50",
            Integer.class
    ),

    PROTECTED_EMAIL_DOMAINS(
            "faxtomail.protectedEmailDomains",
            n("faxtomail.config.option.protectedEmailDomains.description"),
            " ",
            String.class
    );

    /** Configuration key. */
    private final String key;

    /** I18n key of option description */
    private final String description;

    /** Type of option */
    private final Class<?> type;

    /** Default value of option. */
    private String defaultValue;

    /** Flag to not keep option value on disk */
    private boolean isTransient;

    /** Flag to not allow option value modification */
    private boolean isFinal;

    FaxToMailConfigurationOption(String key,
                             String description,
                             String defaultValue,
                             Class<?> type,
                             boolean isTransient) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
        this.isTransient = isTransient;
        this.isFinal = isTransient;
    }

    FaxToMailConfigurationOption(String key,
                             String description,
                             String defaultValue,
                             Class<?> type) {
        this(key, description, defaultValue, type, false);
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public boolean isTransient() {
        return isTransient;
    }

    @Override
    public boolean isFinal() {
        return isFinal;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void setTransient(boolean newValue) {
        // not used
    }

    @Override
    public void setFinal(boolean newValue) {
        // not used
    }

    private static class Constants {
        protected static final String FALSE = "false";
    }
}
