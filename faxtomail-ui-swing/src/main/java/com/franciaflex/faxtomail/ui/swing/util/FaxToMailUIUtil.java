package com.franciaflex.faxtomail.ui.swing.util;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.ExtensionCommand;
import com.franciaflex.faxtomail.persistence.entities.GeneratedPDFPage;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.Reply;
import com.franciaflex.faxtomail.persistence.entities.ReplyContent;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.franciaflex.faxtomail.services.service.ConfigurationService;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.RangeRowModel;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUIModel;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.JAXXUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.jaxx.application.swing.util.ApplicationUIUtil;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;

import javax.imageio.ImageIO;
import javax.media.jai.PlanarImage;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import static org.nuiton.i18n.I18n.t;

/**
 * @author kmorin - morin@codelutin.com
 *
 */
public final class FaxToMailUIUtil extends ApplicationUIUtil {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FaxToMailUIUtil.class);

    public static final List<String> EDITABLE_EXTENSIONS = Lists.newArrayList("PDF", "PNG", "JPG", "JPEG", "GIF",
                                                                              "TIF", "BMP", "TXT");

    private FaxToMailUIUtil() {
        // never instanciate util class
    }

    public static FaxToMailUIContext getApplicationContext(JAXXObject ui) {
        return (FaxToMailUIContext) ApplicationUIUtil.getApplicationContext(ui);
    }

    public static void setParentUI(JAXXObject ui, FaxToMailUI<?, ?> parentUI) {
        JAXXUtil.initContext(ui, parentUI);
        setApplicationContext(ui, parentUI.getHandler().getContext());
    }

    public static Map<MailFolder, FolderTreeNode> initFolderTree(final FaxToMailUIContext context,
                                                                         JTree navigationTree,
                                                                         Collection<MailFolder> folders,
                                                                         final boolean colorNotAffectedFolders) {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("root");

        Map<MailFolder, FolderTreeNode> nodesByFolder = new HashMap<>();
        List<MailFolder> orderedFolders = new ArrayList<>(folders);
        orderedFolders.sort(Ordering.natural().onResultOf(MailFolder::getName));
        for (MailFolder folder : orderedFolders) {
            nodesByFolder.putAll(FaxToMailUIUtil.createFolderTree(context, root, folder));

            FolderTreeNode node = nodesByFolder.get(folder);
            MailFolder parent = folder.getParent();

            while (parent != null) {
                FolderTreeNode parentNode = nodesByFolder.get(parent);
                if (parentNode == null) {
                    parentNode = new FolderTreeNode(parent);
                    parentNode.setCanRead(false);
                    parentNode.setCanSelect(true);
                    nodesByFolder.put(parent, parentNode);
                }

                parentNode.add(node);
                node = parentNode;
                parent = parent.getParent();
            }
            root.add(node);
        }

        TreeModel treeModel = new DefaultTreeModel(root);
        navigationTree.setModel(treeModel);

        // use the folder icon for the leaf
        DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer() {
            @Override
            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel,
                                                          boolean expanded, boolean leaf, int row, boolean hasFocus) {
                Component component = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

                if (FolderTreeNode.class.isAssignableFrom(value.getClass())) {
                    FolderTreeNode node = (FolderTreeNode) value;
                    setEnabled(node.isCanSelect());

                    // En italique si l'utilisateur n'a pas le droit de lecture (sur un parent par exemple)
                    Font font = component.getFont();
                    if (node.isCanRead()) {
                        font = font.deriveFont(Font.PLAIN);
                    } else {
                        font = font.deriveFont(Font.ITALIC);
                    }
                    component.setFont(font);

                    // Affichage en gris par defaut, mais en noir si le dossier est vraiment affecté
                    // à l'utilisateur
                    Color foreground;
                    if (!colorNotAffectedFolders
                            || context.getCurrentUser().isAffectedFoldersEmpty()
                            || context.getCurrentUser().containsAffectedFolders(node.getMailFolder())) {

                        foreground = sel ? Color.WHITE : Color.BLACK;

                    } else {
                        foreground = Color.GRAY;
                    }
                    component.setForeground(foreground);

                }

                return component;
            }
        };
        Icon folderIcon = renderer.getDefaultClosedIcon();
        renderer.setLeafIcon(folderIcon);
        navigationTree.setCellRenderer(renderer);

        return nodesByFolder;
    }

    /**
     * Creates the tree nodes and returns a map of the nodes by folder
     * @param parent the node parent
     * @param folder the folder to add to the tree
     * @return the map of the nodes by folder
     */
    protected static Map<MailFolder, FolderTreeNode> createFolderTree(FaxToMailUIContext context,
                                                                      DefaultMutableTreeNode parent,
                                                                       MailFolder folder) {
        Map<MailFolder, FolderTreeNode> result = new HashMap<>();

        FolderTreeNode node = new FolderTreeNode(folder);
        node.setCanSelect(true);
        node.setCanRead(true);
        result.put(folder, node);

        parent.add(node);
        if (folder.isChildrenNotEmpty()) {
            List<MailFolder> children = new ArrayList<>(folder.getChildren());
            CollectionUtils.filter(children, object -> !object.isArchiveFolder());
            children.sort(Comparator.comparing(MailFolder::getName));
            for (MailFolder child : children) {
                result.putAll(createFolderTree(context, node, child));
            }
        }

        // set the folder instance with the needed data in the context
        MailFolder currentFolder = context.getCurrentMailFolder();
        if (currentFolder != null && currentFolder.getTopiaId().equals(folder.getTopiaId())) {
            context.setCurrentMailFolder(folder);
        }

        return result;
    }

    public static Map<String, Integer> computeQuantities(Collection<RangeRowModel> rangeRows) {
        int quotationNb = 0;
        int pfNb = 0;
        int savNb = 0;
        for (RangeRowModel rangeRow : rangeRows) {
            if (rangeRow.isValid()) {
                Integer quotationQuantity = rangeRow.getQuotationQuantity();
                if (quotationQuantity != null) {
                    quotationNb += quotationQuantity;
                }

                Integer productQuantity = rangeRow.getProductQuantity();
                if (productQuantity != null) {
                    pfNb += productQuantity;
                }

                Integer savQuantity = rangeRow.getSavQuantity();
                if (savQuantity != null) {
                    savNb += savQuantity;
                }
            }
        }
        Map<String, Integer> result = new HashMap<String, Integer>();
        result.put(DemandeUIModel.PROPERTY_QUOTATION_NB, quotationNb);
        result.put(DemandeUIModel.PROPERTY_PF_NB, pfNb);
        result.put(DemandeUIModel.PROPERTY_SAV_NB, savNb);
        return result;
    }

    public static boolean isFileAPDF(AttachmentFile file) {
        return isFileAPDF(file.getFilename());
    }

    public static boolean isFileAPDF(String filename) {
        return FileUtil.extension(filename).toUpperCase().equals("PDF");
    }

    public static boolean isFileATxt(AttachmentFile file) {
        return isFileATxt(file.getFilename());
    }

    public static boolean isFileATxt(String filename) {
        return FileUtil.extension(filename).toUpperCase().equals("TXT");
    }

    public static boolean isFileATif(AttachmentFile file) {
        return isFileATif(file.getFilename());
    }

    public static boolean isFileATif(String filename) {
        return FileUtil.extension(filename).toUpperCase().equals("TIF");
    }

    public static boolean isFileTypeEditable(String fileName) {
        String extension = FileUtil.extension(fileName);
        return EDITABLE_EXTENSIONS.contains(extension.toUpperCase());
    }

    public static void setEmailContentInTextPane(DemandeUIHandler handler,
                                                 final DemandeUIModel demandeUIModel,
                                                 JPanel textPanePanel,
                                                 FaxToMailConfiguration config) {

        List<String> contents = demandeUIModel.getHtmlContent();

        if (CollectionUtils.isNotEmpty(contents)) {
            StringBuilder result = new StringBuilder();
            for (String content:contents) {
                if (!content.isEmpty()) {
                    result.append(content).append("<br/>");
                }
            }
            addHtmlTextPane(handler, demandeUIModel, textPanePanel, result.toString());
        }

        // if there is no html content or if no html content can be correctly displayed (ie throws an exception)
        // then display plain content as html (prevent plain content with html tags to be raw displayed)
        if (textPanePanel.getComponentCount() == 0) {
            contents = demandeUIModel.getPlainContent();

            if (CollectionUtils.isNotEmpty(contents)) {
                for (String content : contents) {
                    if (content != null && content.trim().length() > 0) {
                        content = content.replaceAll( "(\r\n|\r|\n)", "<br>");

                        addHtmlTextPane(handler, demandeUIModel, textPanePanel, content);
                    }
                }
            }
        }

        // if there is no html content or if no html content can be correctly displayed (ie throws an exception)
        // fail back to plain old TextPane
        if (textPanePanel.getComponentCount() == 0) {

            contents = demandeUIModel.getPlainContent();

            if (CollectionUtils.isNotEmpty(contents)) {
                for (String content : contents) {
                    if (content != null && content.trim().length() > 0) {
                        final JTextPane textPane = new JTextPane();
                        boolean newDemand = StringUtils.isBlank(demandeUIModel.getTopiaId());
                        textPane.setEditable(newDemand);
                        if (newDemand) {
                            textPane.addKeyListener(new KeyAdapter() {

                                @Override
                                public void keyReleased(KeyEvent e) {
                                    demandeUIModel.getOriginalEmail().setContent(textPane.getText());
                                }
                            });
                        }
                        textPane.setText(content);
                        textPane.setAlignmentX(Component.LEFT_ALIGNMENT);
                        textPanePanel.add(textPane);
                        textPanePanel.add(Box.createVerticalStrut(3));
                    }
                }
            }
        }

        MailFolder mailFolder = demandeUIModel.getMailFolder();
        while (mailFolder.getParent() != null && mailFolder.getShowAttachmentPreview() == null) {
            mailFolder = mailFolder.getParent();
        }
        if (Boolean.TRUE.equals(mailFolder.getShowAttachmentPreview()) && demandeUIModel.getDemandStatus() != DemandStatus.ARCHIVED) {
            for (Attachment attachment : demandeUIModel.getAttachment()) {
                if (!attachment.isInlineAttachment() && !attachment.isMailContent() && !attachment.isAddedByUser()) {
                    addAttachmentPane(handler, textPanePanel, attachment, config);
                }
            }
        }
    }

    /**
     * Force le chargement des attachmentFile (edited and original) pour un attachment.
     *
     * @param context context
     * @param attachment attachment
     */
    public static void forceAttachmentFileLoading(FaxToMailUIContext context, Attachment attachment) {
        if (log.isDebugEnabled()) {
            log.debug("Force attachment loading " + attachment.getOriginalFileName());
        }
        if (attachment.isPersisted()) {
            try(FaxToMailServiceContext serviceContext = context.newServiceContext()) {
                EmailService service = serviceContext.getEmailService();
                if (attachment.getOriginalFile() == null) {
                    AttachmentFile file = service.getAttachmentFile(attachment.getTopiaId(), true);
                    attachment.setOriginalFile(file);
                }
                if (attachment.getEditedFile() == null) {
                    AttachmentFile file = service.getAttachmentFile(attachment.getTopiaId(), false);
                    attachment.setEditedFile(file);
                }
            } catch (IOException eee) {
                log.error("Error while loading attachment file", eee);
            }
        }
    }

    public static AttachmentFile getLazyOriginalFile(FaxToMailUIContext context, Attachment attachment) {
        if (log.isDebugEnabled()) {
            log.debug("Force attachment loading " + attachment.getOriginalFileName());
        }
        if (attachment.isPersisted()) {
            try(FaxToMailServiceContext serviceContext = context.newServiceContext()) {
                EmailService service = serviceContext.getEmailService();
                if (attachment.getOriginalFile() == null) {
                    AttachmentFile file = service.getAttachmentFile(attachment.getTopiaId(), true);
                    attachment.setOriginalFile(file);
                }
            } catch (IOException eee) {
                log.error("Error while loading original file", eee);
            }
        }

        AttachmentFile result = attachment.getOriginalFile();
        return result;
    }

    public static Collection<GeneratedPDFPage> getLazyGeneratedPDFPage(FaxToMailUIContext context, Attachment attachment) {
        if (log.isDebugEnabled()) {
            log.debug("Force generated pages loading " + attachment.getOriginalFileName());
        }
        if (attachment.isPersisted()) {
            try(FaxToMailServiceContext serviceContext = context.newServiceContext()) {
                EmailService service = serviceContext.getEmailService();
                if (attachment.getGeneratedPDFPages() == null) {
                    Collection<GeneratedPDFPage> pages = service.getGeneratedPDFPage(attachment.getTopiaId());
                    attachment.setGeneratedPDFPages(pages);
                }
            } catch (IOException eee) {
                log.error("Error while loading generated PDF pages", eee);
            }
        }

        Collection<GeneratedPDFPage> generatedPDFPages = attachment.getGeneratedPDFPages();
        return generatedPDFPages;
    }

    public static AttachmentFile getLazyEditedFile(FaxToMailUIContext context, Attachment attachment) {
        if (log.isDebugEnabled()) {
            log.debug("Force attachment loading " + attachment.getOriginalFileName());
        }
        if (attachment.isPersisted()) {
            try(FaxToMailServiceContext serviceContext = context.newServiceContext()) {
                EmailService service = serviceContext.getEmailService();
                if (attachment.getEditedFile() == null) {
                    AttachmentFile file = service.getAttachmentFile(attachment.getTopiaId(), false);
                    attachment.setEditedFile(file);
                }
            } catch (IOException eee) {
                log.error("Error while loading edited file", eee);
            }
        }

        return attachment.getEditedFile();
    }



    /**
     * Force le chargement des contenu source binaire des réponses.
     *
     * @param context context
     * @param reply reply
     */
    public static void forceReplyContentLoading(FaxToMailUIContext context, Reply reply) {
        if (log.isDebugEnabled()) {
            log.debug("Force source loading " + reply.getSubject());
        }
        if (reply.isPersisted() && reply.getReplyContent() == null) {
            try(FaxToMailServiceContext serviceContext = context.newServiceContext()) {
                EmailService service = serviceContext.getEmailService();
                ReplyContent replyContent = service.getReplyContent(reply.getTopiaId());
                reply.setReplyContent(replyContent);
            }  catch (IOException eee) {
                log.error("Error while loading reply content", eee);
            }
        }
    }

    /**
     * Imprime un attachment file.
     * 
     * @param attachmentFile
     * @param defaultPrinter if {@code true}, do not display print dialog and print with default printer
     * @return true if file has been printed, false otherwise
     */
    public static boolean print(AttachmentFile attachmentFile, boolean defaultPrinter, FaxToMailConfiguration config, Function<Integer, Void> callBack) {

        File file = preparePrint(attachmentFile);

        return printWithPdfRenderer(attachmentFile.getFilename(), Collections.singletonList(file), defaultPrinter, config, callBack);
    }

    /**
     * preparation de l'impression d'un attachment file.
     *
     * @param attachmentFile
     * @return File to print
     */
    public static File preparePrint(AttachmentFile attachmentFile) {
        File file;
        try {
            if (!FaxToMailUIUtil.isFileAPDF(attachmentFile)) {
                file = convertFileToPdf(attachmentFile);
            } else {
                file = File.createTempFile("faxtomail-", ".tmp");
                FileUtils.copyFile(attachmentFile.getFile(), file);
            }

        } catch (IOException | DocumentException e) {
            throw new ApplicationTechnicalException(
                    t("jaxx.application.error.cannot.print"), e);

        }
        return file;
    }

    /**
     * Imprime du texte.
     *
     * @param text text to print
     * @return File to print
     **/
    public static File preparePrintText(String text) {
        // convert text content to inputstream
        byte[] content = text.getBytes(StandardCharsets.UTF_8);
        File file;
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(content)){

            file = convertFileToPdf(byteArrayInputStream, FileType.TEXT);

        } catch (IOException | DocumentException e) {
            throw new ApplicationTechnicalException(
                    t("jaxx.application.error.cannot.print"), e);
        }
        return file;
    }

    /**
     * Imprime un attachment file.
     * 
     * @param printName print job name
     * @param files files to print
     * @param defaultPrinter if {@code true}, do not display print dialog and print with default printer
     * @return true if file has been printed, false otherwise
     */
    public static boolean printWithPdfRenderer(final String printName,
                                               final List<File> files,
                                               final boolean defaultPrinter,
                                               final FaxToMailConfiguration config,
                                               final Function<Integer, Void> callBack) {

        final PrinterJob printJob = PrinterJob.getPrinterJob();
        printJob.setJobName(printName);

        // Send print job to default printer
        final PrintRequestAttributeSet attributes = new HashPrintRequestAttributeSet();
        final boolean result = defaultPrinter || printJob.printDialog(attributes);

        if (result) {
            //Use SwingWorker so that printing does not block UI (cf. #9786)
            SwingWorker worker = new SwingWorker<Integer, Void>() {
                @Override
                public Integer doInBackground() {

                    FilesPrintable filesPrintable = new FilesPrintable(files, config.getDefaultPrintMargin());
                    printJob.setPrintable(filesPrintable);

                    try {
                        printJob.print(attributes);
                    } catch (PrinterException ex) {
                        if (log.isErrorEnabled()) {
                            log.error("can't print", ex);
                        }
                    }

                    if (log.isInfoEnabled()) {
                        log.info(filesPrintable.getCountPages() + " fichiers ont été imprimés");
                    }

                    return filesPrintable.getCountPages();
                }

                @Override
                protected void done() {
                    super.done();
                    if (callBack != null) {
                        Integer countPages = 0;
                        try {
                            countPages = get();
                        } catch (InterruptedException e) {
                            if (log.isErrorEnabled()) {
                                log.error("Error on finish print", e);
                            }
                            Thread.currentThread().interrupt();

                        } catch (ExecutionException e) {
                            if (log.isErrorEnabled()) {
                                log.error("Error on finish print", e);
                            }
                        }
                        callBack.apply(countPages);
                    }
                }
            };
            worker.execute();
        }

        return result;
    }

    protected static class FilesPrintable implements Printable {

        protected final Iterator<File> filesToPrint;
        protected final int marginMillimeter;

        protected PDFRenderer currentRenderer;
        protected PDDocument pdDocument;
        protected int countPages;
        protected int pageIndexFile;

        public FilesPrintable(Iterable<File> filesToPrint, int marginMillimeter) {
            this.filesToPrint = filesToPrint.iterator();
            this.marginMillimeter = marginMillimeter;
            this.countPages = 0;
            this.pageIndexFile = 0;
        }

        public int getCountPages() {
            return countPages;
        }

        @Override
        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
            File printedFile = null;
            try {
                if (pageIndex >= countPages) {
                    IOUtils.closeQuietly(pdDocument);
                    if (filesToPrint.hasNext()) {
                        printedFile = filesToPrint.next();
                        pdDocument = PDDocument.load(printedFile);
                        currentRenderer = new PDFRenderer(pdDocument);
                        pageIndexFile = countPages;
                        countPages += pdDocument.getNumberOfPages();
                    } else {
                        return Printable.NO_SUCH_PAGE;
                    }
                }

                //Init pageFormat with margin from conf (do not use default params)
                Paper paper = pageFormat.getPaper();
                double margin = marginMillimeter * 2.83465; // convert millimeters to points
                paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight()
                        - margin * 2);
                pageFormat.setPaper(paper);

                double paperWidth = (int) pageFormat.getImageableWidth();
                double paperHeight = (int) pageFormat.getImageableHeight();
                int paperX = (int) pageFormat.getImageableX();
                int paperY = (int) pageFormat.getImageableY();

                if (log.isDebugEnabled()) {
                    log.debug("page width " + paperWidth);
                    log.debug("page height " + paperHeight);
                    log.debug("page x " + paperX);
                    log.debug("page y " + paperY);
                }

                BufferedImage image = currentRenderer.renderImageWithDPI(pageIndex - pageIndexFile, 300);

                int imageWidth = image.getWidth();
                int imageHeight = image.getHeight();
                double widthRatio = paperWidth / imageWidth;
                double heightRatio = paperHeight / imageHeight;

                if (log.isDebugEnabled()) {
                    log.debug("width ratio : " + widthRatio);
                    log.debug("height ratio : " + heightRatio);
                }

                int width;
                int height;
                // if the image is smaller than the page
                if (widthRatio >= 1 && heightRatio >= 1) {
                    width = imageWidth;
                    height = imageHeight;

                } else {
                    double minRatio = Math.min(widthRatio, heightRatio);
                    width = (int) (minRatio * imageWidth);
                    height = (int) (minRatio * imageHeight);
                }

                if (log.isDebugEnabled()) {
                    log.debug("image width : " + width);
                    log.debug("image height : " + height);
                }

                //use full size
                graphics.setClip(paperX, paperY, width, height);
                graphics.drawImage(image, paperX, paperY, width, height, null);

                return Printable.PAGE_EXISTS;

            } catch (Exception e) {
                String fileName = "unknown";
                if (null != printedFile){
                    fileName = printedFile.getName();
                }

                if (log.isErrorEnabled()) {
                    log.error("error while printing file : " + fileName, e);
                }
                IOUtils.closeQuietly(pdDocument);
                throw new PrinterException("Error while printing file : " + fileName + " : " + e.getMessage());
            }
        }
    }


    public static File convertFileToPdf(AttachmentFile attachmentFile) throws IOException, DocumentException {
        File file = attachmentFile.getFile();
        FileType type;
        if (FaxToMailUIUtil.isFileATxt(attachmentFile)) {
            type = FileType.TEXT;

        } else if (FaxToMailUIUtil.isFileATif(attachmentFile)) {
            type = FileType.TIF;

        } else {
            type = FileType.OTHER;
        }
        File result = convertFileToPdf(new FileInputStream(file), type);
        return result;
    }

    protected enum FileType {
        TEXT, TIF, OTHER
    }

    protected static File convertFileToPdf(InputStream inputStream, FileType type) throws IOException, DocumentException {
        File target = File.createTempFile("faxtomail-", ".tmp");
        target.deleteOnExit();

        Document document = new Document();
        FileOutputStream fos = new FileOutputStream(target);
        PdfWriter writer = PdfWriter.getInstance(document, fos);
        writer.open();

        document.setPageSize(PageSize.A4);
        document.open();

        if (type == FileType.TEXT) {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                StringBuilder text = new StringBuilder();
                String line = br.readLine();
                while (line != null) {
                    text.append(line).append("\n");
                    line = br.readLine();
                }
                document.add(new Paragraph(text.toString()));
                br.close();

            } finally {
                IOUtils.closeQuietly(br);
            }

        } else if (type == FileType.TIF) {
            try {
                FileInputStream fis = (FileInputStream) inputStream;
                FileChannel channel = fis.getChannel();
                ByteBuffer buffer = ByteBuffer.allocate((int) channel.size());
                channel.read(buffer);
                SeekableStream stream = new ByteArraySeekableStream(buffer.array());
                String[] names = ImageCodec.getDecoderNames(stream);
                ImageDecoder tifImageDecoder = ImageCodec.createImageDecoder(names[0], stream, null);

                for (int i = 0 ; i < tifImageDecoder.getNumPages() ; i++) {
                    RenderedImage renderedImage = tifImageDecoder.decodeAsRenderedImage(i);
                    java.awt.Image awtImage = PlanarImage.wrapRenderedImage(renderedImage).getAsBufferedImage();
                    document.newPage();
                    com.itextpdf.text.Image image = com.itextpdf.text.Image.getInstance(writer, awtImage, 1.0f);
                    image.scaleToFit(PageSize.A4.getWidth() - document.leftMargin() - document.rightMargin(),
                                     PageSize.A4.getHeight() - document.topMargin() - document.bottomMargin());
                    document.add(image);
                }
                fis.close();
            } finally {
                IOUtils.closeQuietly(inputStream);
            }

        } else {
            byte[] bytes = IOUtils.toByteArray(inputStream);
            com.itextpdf.text.Image image = com.itextpdf.text.Image.getInstance(bytes);
            float pageWidth = document.getPageSize().getWidth()- document.leftMargin() - document.rightMargin();
            float pageHeight = document.getPageSize().getHeight()- document.topMargin() - document.bottomMargin();
            float imageWidth = image.getWidth();
            float imageHeight = image.getHeight();
            if (pageWidth < imageWidth || pageHeight < imageHeight) {
                float scaler = Math.min(pageWidth / imageWidth, pageHeight / imageHeight) * 100;
                image.scalePercent(scaler);
            }
            document.add(image);
        }

        document.close();
        writer.close();

        IOUtils.closeQuietly(inputStream);

        return target;
    }


    public static String getEditedFileName(String originalFileName) {
        return t("faxtomail.attachment.editedFile.name", originalFileName) + ".pdf";
    }

    /**
     * Ouvre un attachment en fonction de la configuration des extensions, ou à default le open système.
     *
     * @param context
     * @param attachment
     */
    public static void openFile(FaxToMailUIContext context, AttachmentFile attachment) {

        File file = attachment.getFile();
        String filename = attachment.getFilename();
        String extension = FilenameUtils.getExtension(filename);

        // get configuration extension command
        ExtensionCommand extCommand = null;
        if (StringUtils.isNotBlank(extension)) {
            try (FaxToMailServiceContext serviceContext = context.newServiceContext()) {
                ConfigurationService service = serviceContext.getConfigurationService();
                extCommand = service.getExtensionCommand(extension);
            } catch (IOException eee) {
                log.error("Error while getting extension command",eee);
            }
        }

        // open file
        if (extCommand != null && StringUtils.isNotBlank(extCommand.getOpenAttachmentCommand())) {
            String command = extCommand.getOpenAttachmentCommand();
            String[] args = StringUtil.split(command, " ");
            List<String> comArgs = new ArrayList<String>();
            for (String arg : args) {
                String localArg = arg;
                localArg = localArg.replace("%f", file.getAbsolutePath());
                comArgs.add(localArg);
            }
            ProcessBuilder pb = new ProcessBuilder(comArgs);
            // run process
            if (log.isDebugEnabled()) {
                log.debug("Open attachment with command : " + comArgs);
            }
            try {
                pb.start();
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Cannot run convert command", e);
                }
            }
        } else {

            try {
                if (SystemUtils.IS_OS_WINDOWS) {
                    // tentative pour résoudre http://forge.codelutin.com/issues/5650
                    // pas d'idée précise du bug, mais ca semble mieux fonctionner à l'arrache
                    String command = "rundll32 SHELL32.DLL,ShellExec_RunDLL "+ file.getAbsoluteFile();
                    if (log.isDebugEnabled()) {
                        log.debug("Opening file with command : " + command);
                    }
                    Runtime.getRuntime().exec(command);
                } else {
                    // version to use for xfce
                    // DesktopUtil.open(file);
                    Desktop desktop = getDesktopForOpen();
                    desktop.open(file);
                }
            } catch (Exception ex) {
                throw new ApplicationTechnicalException(
                        t("jaxx.application.error.cannot.open"), ex);
            }
        }
    }

    public static void openUrl(String url) {

        String os = System.getProperty("os.name").toLowerCase();
        Runtime rt = Runtime.getRuntime();

        try {

            if (os.contains("win")) {
                rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
            }

            if (os.contains("mac")) {
                rt.exec("open " + url);
            }

            if (os.contains("nix") || os.contains("nux")) {
                rt.exec("xdg-open " + url);
            }
        } catch (IOException eee) {
            log.error("Could not open url : " + url, eee);
        }
    }

    /**
     * Send email.
     * 
     * @param subject
     * @param body
     */
    public static void email(String subject, String body) {

        try {
            URI mailtoURI = new URI("mailto", null, null, "subject=" + subject + "&body=" + body, null);
            Desktop desktop = getDesktopForMail();
            desktop.mail(mailtoURI);
        } catch (Exception e) {
            throw new ApplicationTechnicalException(
                    t("jaxx.application.error.cannot.mail"), e);
        }
    }

    public static boolean isRangePanelVisible(DemandType demandType) {
        return demandType != null && FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.RANGE_ROW);
    }

    protected static void addHtmlTextPane(DemandeUIHandler handler,
                                          DemandeUIModel demandeUIModel,
                                          JPanel textPanePanel,
                                          String content) {

        Preconditions.checkNotNull(content);

        // the meta tag makes the content is not displayed
        content = content.replaceAll("<meta (.*?)>(</meta>)?", "");

        if (log.isTraceEnabled()) {
            log.trace("Content before mail = " + content);
        }

        for (Attachment attachment : demandeUIModel.getAttachment()) {
            String key = attachment.getContentId();
            if (key == null) {
                key = attachment.getOriginalFileName();
            }

            // get file content
            forceAttachmentFileLoading(handler.getContext(), attachment);
            AttachmentFile attachmentFile = attachment.getOriginalFile();
            File file = attachmentFile.getFile();

            // replace the inline attachments with the extracted attachment file url
            // match les patterns:
            // <td background="cid:bg.gif" height="52">
            // <img border=0 src="cid:bg.gif" />
            // <img src='cid:5e9ef859-ea65-4f9b-a9fa-30d4a2c5837c'
            content = content.replaceAll("(\\w+)=([\"'])cid:" + Pattern.quote(key) + "([\"'])", "$1=$2" + file.toURI() + "$3");

            if (log.isDebugEnabled()) {
                log.debug("Mapping attachment id " + key + " to file " + file.toURI());
            }
        }

        if (log.isTraceEnabled()) {
            log.trace("Content after mail = " + content);
        }

        try {
            // on reformate les urls pour supprimer les caractères qui vont pas (ex espaces)
            // cf #7741
            content = FaxToMailServiceUtils.encodeImageSourcesInEmail(content, handler.getConfig().getDefaultImageIfMalformedUrl());

            HTMLPane panel= new HTMLPane();

            panel.init(textPanePanel.getParent().getHeight());
            panel.loadContent(content);

            textPanePanel.add(panel);

            textPanePanel.add(Box.createVerticalStrut(3));

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Error when displaying email content", e);
            }
        }
    }

    protected static void addAttachmentPane(final DemandeUIHandler handler,
                                            JPanel textPanePanel,
                                            final Attachment attachment,
                                            FaxToMailConfiguration config) {

        if (isFileTypeEditable(attachment.getOriginalFileName())) {

            Box box = Box.createVerticalBox();
            box.setBorder(BorderFactory.createTitledBorder(attachment.getOriginalFileName()));
            box.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseClicked(MouseEvent e) {
                    if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                        handler.getUI().getAttachmentsButton().editAttachment(attachment);
                    }
                }
            });

            try {
                if (isFileAPDF(attachment.getOriginalFileName())) {

                    //get back images
                    Collection<GeneratedPDFPage> pages = getLazyGeneratedPDFPage(handler.getContext(),attachment);

                    if (pages != null && !pages.isEmpty()) {
                        //Got pre-generated pages
                        for (GeneratedPDFPage page : pages) {
                            InputStream in = new ByteArrayInputStream(page.getPage());
                            BufferedImage pageImage = ImageIO.read(in);
                            JImagePanel imagePanel = new JImagePanel();
                            imagePanel.setScaleImageToFitPanel(true);
                            imagePanel.setImage(pageImage);
                            imagePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
                            box.add(imagePanel);
                            box.add(Box.createVerticalStrut(3));
                        }
                    } else {
                        //No pre-generated pages - old school on the fly generation
                        try {
                            File originalFile =  getLazyOriginalFile(handler.getContext(),attachment).getFile();
                            PDDocument pdDocument = PDDocument.load(originalFile);
                            PDFRenderer renderer = new PDFRenderer(pdDocument);
                            for (int i = 0; i < pdDocument.getNumberOfPages(); i++) {
                                JImagePanel imagePanel = new JImagePanel();
                                imagePanel.setScaleImageToFitPanel(true);
                                imagePanel.setImage(renderer.renderImage(i));
                                imagePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
                                box.add(imagePanel);
                                box.add(Box.createVerticalStrut(3));
                            }
                        } catch (IOException eee) {
                            log.error("Error generating PDF pages", eee);
                        }
                    }

                } else if (isFileATif(attachment.getOriginalFileName())) {
                    File originalFile =  getLazyOriginalFile(handler.getContext(),attachment).getFile();
                    FileInputStream fis = new FileInputStream(originalFile);
                    try {
                        FileChannel channel = fis.getChannel();
                        ByteBuffer buffer = ByteBuffer.allocate((int) channel.size());
                        channel.read(buffer);
                        SeekableStream stream = new ByteArraySeekableStream(buffer.array());
                        String[] names = ImageCodec.getDecoderNames(stream);
                        ImageDecoder tifImageDecoder = ImageCodec.createImageDecoder(names[0], stream, null);

                        for (int i = 0 ; i < tifImageDecoder.getNumPages() ; i++) {
                            Image awtImage;
                            try {
                                RenderedImage renderedImage = tifImageDecoder.decodeAsRenderedImage(i);
                                awtImage = PlanarImage.wrapRenderedImage(renderedImage).getAsBufferedImage();
                            } catch (Exception e) {
                                if (log.isErrorEnabled()) {
                                    log.error("Error on load tif (page " + i + ")", e);
                                }
                                awtImage = generateErrorImage(e.getMessage());
                            }

                            JImagePanel imagePanel = new JImagePanel();

                            int maxWidth = config.getMaxWidthImageInTextPane();
                            imagePanel.setMaxWidth(maxWidth);
                            
                            imagePanel.setScaleImageToFitPanel(true);
                            imagePanel.setImage(awtImage);
                            imagePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
                            box.add(imagePanel);
                            box.add(Box.createVerticalStrut(3));
                        }

                    } finally {
                        IOUtils.closeQuietly(fis);
                    }

                } else if (isFileATxt(attachment.getOriginalFileName())) {
                    JTextPane textPane = new JTextPane();
                    File originalFile =  getLazyOriginalFile(handler.getContext(),attachment).getFile();
                    textPane.setText(FileUtils.readFileToString(originalFile, StandardCharsets.UTF_8));
                    textPane.setAlignmentX(Component.LEFT_ALIGNMENT);
                    box.add(textPane);

                } else {
                    JImagePanel imagePanel = new JImagePanel();

                    int maxWidth = config.getMaxWidthImageInTextPane();
                    imagePanel.setMaxWidth(maxWidth);

                    imagePanel.setScaleImageToFitPanel(true);
                    File originalFile =  getLazyOriginalFile(handler.getContext(),attachment).getFile();
                    imagePanel.setImage(originalFile);
                    imagePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
                    box.add(imagePanel);
                }

                box.setAlignmentX(Component.LEFT_ALIGNMENT);
                textPanePanel.add(box);

            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Error while reading the file " + attachment.getOriginalFileName(), e);
                }
            }
        }
    }

    protected static BufferedImage generateErrorImage(String message) {
        BufferedImage image = new BufferedImage(600, 800, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics = image.createGraphics();

        graphics.setBackground(Color.WHITE);
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, 600, 800);

        String fontName = graphics.getFont().toString();
        Font font = new Font(fontName, Font.BOLD, 20);

        graphics.setFont(font);
        graphics.setColor(Color.RED);
        graphics.drawString(t("faxtomail.image.error"), 50, 200);

        if (message != null) {
            font = new Font(fontName, Font.PLAIN, 14);
            graphics.setFont(font);
            graphics.drawString(message, 60, 230);
        }

        graphics.dispose();

        return image;
    }

    public static String getQuotedReplyContent(String signing, String receptionDate, String sender, String originalEmail) {
        String quotedReply = t("faxtomail.reply.message",
                               signing,
                               receptionDate,
                               sender,
                               originalEmail);
        return quotedReply;
    }

    public static String getQuotedForwardContent(String signing, String subject, String receptionDate, String sender,
                                                 String recipient, String ccRecipient, String originalEmail) {
        String recipients = t("faxtomail.reply.label.to") + " " + recipient;
        if (StringUtils.isNotBlank(ccRecipient)) {
            recipients += "<br/>" + t("faxtomail.reply.label.cc") + " " + ccRecipient;
        }
        return t("faxtomail.forward.message",
                               signing,
                               subject,
                               receptionDate,
                               sender,
                               recipients,
                               originalEmail);
    }

    public static void initScrollPaneBars(final JScrollPane scrollPane) {
        SwingUtilities.invokeLater(() -> {
            scrollPane.getVerticalScrollBar().setValue(0);
            scrollPane.getVerticalScrollBar().setUnitIncrement(30);
            scrollPane.getHorizontalScrollBar().setValue(0);
            scrollPane.getHorizontalScrollBar().setUnitIncrement(30);
        });
    }

    /**
     * Method that set the from on a reply depending on config and type : on fax, use the from from the config, on
     * email, if the to contains a protected email (from the config), use the from from the config (refs #10750)
     *
     * @param currentDemand the current demand
     * @param model the reply UI model
     * @param to the email recipient (should be the sender of the original email)
     * @param config FaxtoMail configuration
     */
    public static void setFromOnReply(DemandeUIModel currentDemand, ReplyFormUIModel model, String to, FaxToMailConfiguration config){
        if (currentDemand.isFax()) {
            MailFolder folder = currentDemand.getMailFolder();
            String from = FaxToMailServiceUtils.addFaxDomainToFaxNumber(FaxToMailServiceUtils.getFaxFromNumber(folder), folder);
            model.setFrom(from);
        } else {
            String[] protectedEmailDomains = config.getProtectedEmailDomains().split(",");
            if (Arrays.stream(protectedEmailDomains).parallel().anyMatch(to::contains)) {
                MailFolder folder = currentDemand.getMailFolder();
                Collection<String> replyAddresses = folder.getReplyAddresses();
                if (!replyAddresses.isEmpty()) {
                    String from = Iterables.get(replyAddresses, 0);
                    model.setFrom(from);
                }
            }
        }
    }


}
