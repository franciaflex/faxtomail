package com.franciaflex.faxtomail.ui.swing.util;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailFolder;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * @author kmorin - kmorin@codelutin.com
 *
 */
public class FolderTreeNode extends DefaultMutableTreeNode {

    protected boolean canSelect;
    protected boolean canRead;

    public FolderTreeNode(MailFolder folder) {
        super(folder);
    }

    public MailFolder getMailFolder() {
        return (MailFolder) userObject;
    }

    public boolean isCanSelect() {
        return canSelect;
    }

    public void setCanSelect(boolean canSelect) {
        this.canSelect = canSelect;
    }

    public boolean isCanRead() {
        return canRead;
    }

    public void setCanRead(boolean canRead) {
        this.canRead = canRead;
    }

    @Override
    public String toString() {
        MailFolder folder = (MailFolder) userObject;
        return folder.getName();
    }
}
