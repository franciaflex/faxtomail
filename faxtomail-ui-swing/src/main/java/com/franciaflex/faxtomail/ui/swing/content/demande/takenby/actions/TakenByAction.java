package com.franciaflex.faxtomail.ui.swing.content.demande.takenby.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.takenby.TakenByFormUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.takenby.TakenByFormUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.takenby.TakenByFormUIModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class TakenByAction extends AbstractFaxToMailAction<TakenByFormUIModel, TakenByFormUI, TakenByFormUIHandler> {

    public TakenByAction(TakenByFormUIHandler handler) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.takenBy.tip"));
    }

    @Override
    public void doAction() throws Exception {
		FaxToMailUIContext context = getContext();
        try(FaxToMailServiceContext serviceContext = context.newServiceContext()) {
            EmailService emailService = serviceContext.getEmailService();

            FaxToMailUser user = getModel().getTakenBy();

            Collection<DemandeUIModel> demands = getModel().getDemandsToTake();

            List<Email> emails = new ArrayList<>();

            for (DemandeUIModel demand : demands) {
                Email email = demand.toEntity();
                emails.add(email);
            }

            List<Email> returnedEmails = emailService.takeBy(emails, user);

            for (DemandeUIModel demand : demands) {
                for (Email email : returnedEmails) {
                    if (demand.getTopiaId().equals(email.getTopiaId())) {
                        demand.fromEntity(email);
                    }
                }
            }
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        handler.closeFrame();
    }
}
