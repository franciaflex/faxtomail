package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandesUIHandler;
import org.nuiton.jaxx.application.swing.ApplicationUI;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * @author Kevin Morin (Code Lutin)
 */
public abstract class SaveAndOpenModalFrameAction<FC extends ApplicationUI> extends SaveDemandeAction {

    protected FC frameContent;
    protected JFrame frame;

    public SaveAndOpenModalFrameAction(DemandesUIHandler handler) {
        super(handler.getCurrentDemandHandler());
    }

    public abstract String getTitle();

    public abstract Dimension getDimension();

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        frame = getHandler().openModalFrame(frameContent, getTitle(), getDimension());

        getContext().addPropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (!getContext().isBusy()) {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {

                            bringCurrentDemandToFront();
                            if (frame != null) {
                                frame.toFront();
                                frame = null;
                            }
                        }
                    });
                    getContext().removePropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, this);
                }
            }
        });
    }

}
