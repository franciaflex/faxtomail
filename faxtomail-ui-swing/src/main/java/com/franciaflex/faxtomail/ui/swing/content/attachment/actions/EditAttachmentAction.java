package com.franciaflex.faxtomail.ui.swing.content.attachment.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.Stamp;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.UserService;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.MainUI;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentEditorUI;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentEditorUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentEditorUIModel;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorUIModel;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import org.nuiton.jaxx.application.swing.action.ApplicationActionUI;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class EditAttachmentAction extends AbstractFaxToMailAction<AttachmentEditorUIModel, AttachmentEditorUI, AttachmentEditorUIHandler> {

    protected Attachment attachment;
    protected Collection<Stamp> stamps;
    protected JFrame frame;

    public EditAttachmentAction(AttachmentEditorUIHandler handler, Attachment attachment) {
        super(handler, false);
        this.attachment = attachment;
        String attachmentName = attachment.getEditedFileName() != null ? attachment.getEditedFileName() : attachment.getOriginalFileName();
        setActionDescription(t("faxtomail.action.attachment.edit.tip", attachmentName));
    }

    @Override
    public boolean prepareAction() throws Exception {
         getContext().closeSecondaryFrame();
        return super.prepareAction();
    }

    @Override
    public void doAction() throws Exception {
        getModel().fireAttachmentOpened(attachment, false);
        FaxToMailUIUtil.forceAttachmentFileLoading(getContext(), attachment);

        FaxToMailUser currentUser = getContext().getCurrentUser();
        try (FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            UserService userService = serviceContext.getUserService();
            stamps = userService.getPdfEditorStamps(currentUser);
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        final PDFEditorUI dialogContent = new PDFEditorUI(getUI());

        MainUI mainUI = getContext().getMainUI();
        if (frame == null){
            frame = getHandler().openFrame(dialogContent, attachment.getOriginalFileName(), mainUI.getSize());

            frame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    dialogContent.getModel().toEntity(attachment);
                }
            });

            getContext().addPropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (!getContext().isBusy()) {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                if (frame != null) {
                                    frame.toFront();
                                    frame = null;
                                    getContext().setActionUI(new ApplicationActionUI(null, getContext()));
                                }
                            }
                        });
                        getContext().removePropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, this);
                    }
                }
            });
        }

        getContext().setActionUI(new ApplicationActionUI(frame, getContext()));
        PDFEditorUIModel pdfEditorUIModel = dialogContent.getModel();
        pdfEditorUIModel.fromEntity(attachment);
        pdfEditorUIModel.setDemand(getContext().getCurrentEmail());
        pdfEditorUIModel.setStamps(stamps);

        frame.setVisible(true);
        frame.toFront();
    }

    @Override
    protected void releaseAction() {
        super.releaseAction();
        stamps = null;
    }
}
