package com.franciaflex.faxtomail.ui.swing.content.demande;

/*
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.EmailFilter;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.Priority;
import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.persistence.entities.RangeRow;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.actions.LoadFolderEmailsAction;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.swing.table.filter.AbstractTableFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;
import org.nuiton.util.pagination.PaginationParameter;

import javax.swing.table.TableModel;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class DemandeListTableFilter extends AbstractTableFilter<JXTable> {

    private static final Log log = LogFactory.getLog(DemandeListTableFilter.class);

    protected DemandeListUIHandler handler;

    public DemandeListTableFilter(JXTable table, DemandeListUIHandler handler) {
        super(table);
        this.handler = handler;
    }

    @Override
    protected boolean execute(int col, Collection<Object> items) {
        EmailFilter emailFilter = handler.getModel().getEmailFilter();
        String property = ((ColumnIdentifier) getTable().getColumn(col).getIdentifier()).getPropertyName();
        boolean filtered = isFiltered(col);

        switch (property) {
            case Email.PROPERTY_DEMAND_STATUS:
                emailFilter.setDemandStatus(getFilterProperty(items, filtered, DemandStatus.class));
                break;

            case Email.PROPERTY_RECEPTION_DATE:
                emailFilter.setReceptionDates(getFilterProperty(items, filtered, Date.class));
                break;

            case Email.PROPERTY_RECIPIENT:
                emailFilter.setRecipients(getFilterProperty(items, filtered, String.class));
                break;

            case Email.PROPERTY_SENDER:
                emailFilter.setSenders(getFilterProperty(items, filtered, String.class));
                break;

            case Email.PROPERTY_OBJECT:
                emailFilter.setDemandObjects(getFilterProperty(items, filtered, String.class));
                break;

            case DemandeUIModel.PROPERTY_CLIENT_CODE:
                emailFilter.setClientCodes(getFilterProperty(items, filtered, String.class));
                break;

            case DemandeUIModel.PROPERTY_CLIENT_NAME:
                emailFilter.setClientNames(getFilterProperty(items, filtered, String.class));
                break;

            case DemandeUIModel.PROPERTY_CLIENT_BRAND:
                emailFilter.setClientBrands(getFilterProperty(items, filtered, String.class));
                break;

            case Email.PROPERTY_DEMAND_TYPE:
                emailFilter.setDemandTypes(getFilterProperty(items, filtered, DemandType.class));
                break;

            case Email.PROPERTY_EDI_ERROR:
                emailFilter.setEdiCodeNumbers(getFilterProperty(items, filtered, String.class));
                break;

            case Email.PROPERTY_WAITING_STATE:
                emailFilter.setWaitingStates(getFilterProperty(items, filtered, WaitingState.class));
                break;

            case Email.PROPERTY_TAKEN_BY:
                emailFilter.setTakenBys(getFilterProperty(items, filtered, FaxToMailUser.class));
                break;

            case Email.PROPERTY_PRIORITY:
                emailFilter.setPriorities(getFilterProperty(items, filtered, Priority.class));
                break;

            case Email.PROPERTY_PROJECT_REFERENCE:
                emailFilter.setProjectReferences(getFilterProperty(items, filtered, String.class));
                break;

            case Email.PROPERTY_COMPANY_REFERENCE:
                emailFilter.setLocalReferences(getFilterProperty(items, filtered, String.class));
                break;

            case DemandeUIModel.PROPERTY_REFERENCE:
                emailFilter.setReferences(getFilterProperty(items, filtered, String.class));
                break;

            case Email.PROPERTY_RANGE_ROW:
                emailFilter.setRanges(getFilterProperty(items, filtered, Range.class));
                break;

            case DemandeUIModel.PROPERTY_PF_NB:
                emailFilter.setProductsQuantities(getFilterProperty(items, filtered, Long.class));
                break;

            case DemandeUIModel.PROPERTY_SAV_NB:
                emailFilter.setSavQuantities(getFilterProperty(items, filtered, Long.class));
                break;

            case DemandeUIModel.PROPERTY_QUOTATION_NB:
                emailFilter.setQuotationQuantities(getFilterProperty(items, filtered, Long.class));
                break;

            case Email.PROPERTY_LAST_ATTACHMENT_OPENER:
                emailFilter.setLastAttachmentOpeners(getFilterProperty(items, filtered, FaxToMailUser.class));
                break;

            case Email.PROPERTY_COMMENT:
                emailFilter.setComments(getFilterProperty(items, filtered, String.class));
                break;

            case Email.PROPERTY_SUBJECT:
                emailFilter.setSubjects(getFilterProperty(items, filtered, String.class));
                break;

            case Email.PROPERTY_LAST_PRINTING_DATE:
                emailFilter.setLastPrintingDates(getFilterProperty(items, filtered, Date.class));
                break;

            case Email.PROPERTY_LAST_PRINTING_USER:
                emailFilter.setLastPrintingUsers(getFilterProperty(items, filtered, FaxToMailUser.class));
                break;
        }

        executeFilter();
        return true;
    }

    private <T> HashSet<T> getFilterProperty(Collection items, boolean filtered, Class<T> clazz) {
        return !filtered ? null : new HashSet<T>(items);
    }

    @Override
    public void modelChanged(TableModel model) {
        fireFilterChange();
    }

    @Override
    public Set<Object> distinctValuesForColumn(int i) {
        log.info("distinctValuesForColumn " + i);
        MailFolder selectedFolder = handler.getModel().getSelectedFolder();
        if (selectedFolder == null) {
            return null;
        }

        JXTable dataTable = handler.getUI().getDataTable();
        try (FaxToMailServiceContext serviceContext = handler.getContext().newServiceContext()) {
            EmailService emailService = serviceContext.getEmailService();
            String property = ((ColumnIdentifier) dataTable.getColumn(i).getIdentifier()).getPropertyName();

            boolean sum = false;
            String[] properties;
            switch (property) {
                case DemandeUIModel.PROPERTY_CLIENT_CODE:
                    properties = new String[]{"client." + Client.PROPERTY_CODE};
                    break;

                case DemandeUIModel.PROPERTY_CLIENT_NAME:
                    properties = new String[]{"client." + Client.PROPERTY_NAME};
                    break;

                case DemandeUIModel.PROPERTY_CLIENT_BRAND:
                    properties = new String[]{"client." + Client.PROPERTY_BRAND};
                    break;

                case DemandeUIModel.PROPERTY_REFERENCE:
                    properties = new String[]{"email." + Email.PROPERTY_COMPANY_REFERENCE,
                            "rangeRow." + RangeRow.PROPERTY_COMMAND_NUMBER};
                    break;

                case Email.PROPERTY_RANGE_ROW:
                    properties = new String[]{"rangeRow." + RangeRow.PROPERTY_RANGE};
                    break;

                case DemandeUIModel.PROPERTY_PF_NB:
                    properties = new String[]{"rangeRow." + RangeRow.PROPERTY_PRODUCT_QUANTITY};
                    sum = true;
                    break;

                case DemandeUIModel.PROPERTY_SAV_NB:
                    properties = new String[]{"rangeRow." + RangeRow.PROPERTY_SAV_QUANTITY};
                    sum = true;
                    break;

                case DemandeUIModel.PROPERTY_QUOTATION_NB:
                    properties = new String[]{"rangeRow." + RangeRow.PROPERTY_QUOTATION_QUANTITY};
                    sum = true;
                    break;

                case Email.PROPERTY_ATTACHMENT:
                case Email.PROPERTY_REPLIES:
                case Email.PROPERTY_EMAIL_GROUP:
                case DemandeUIModel.PROPERTY_GROUPED_DEMANDES:
                    return null;

                default:
                    properties = new String[]{"email." + property};
            }

            Set<Object> result = new HashSet<>(emailService.getDistinctValues(selectedFolder,
                    properties,
                    sum));

            if (Email.PROPERTY_RECEPTION_DATE.equals(property)) {
                Set<Object> filteredDates = new HashSet<>();
                if (log.isDebugEnabled()) {
                    log.debug("result size " + result.size() + " " + result);
                }
                for (Object o : result) {

                    // le dao revoie toujours null, meme si le cas est impossible en theorie pour
                    // PROPERTY_RECEPTION_DATE, donc dans ce cas, on ignore le cas null
                    if (o != null) {
                        Date date = (Date) o;
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        cal.set(Calendar.SECOND, 0);
                        cal.set(Calendar.MILLISECOND, 0);
                        filteredDates.add(cal.getTime());
                    }
                }
                result = new HashSet<>(filteredDates);
            }

            return result;
        } catch (IOException eee) {
            log.error("Cannot get distinct values for column", eee);
            return null;
        }
    }

    @Override
    public String toString(Object obj) {
        String s = null;
        if (obj != null) {
            Decorator<?> decorator = handler.getDecorator(obj.getClass(), null);
            if (decorator != null) {
                s = decorator.toString(obj);
            }
        }
        if (s == null) {
            s = JAXXUtil.getStringValue(obj);
        }
        return s;
    }

    @Override
    public void clear() {
        super.clear();
        handler.getModel().getEmailFilter().clearFilter();
        executeFilter();
    }

    public void executeFilter() {
        FaxToMailUIContext context = handler.getContext();

        // reset pagination when folder change
        PaginationParameter currentPaginationParameter = context.getCurrentPaginationParameter();
        if (currentPaginationParameter != null) {
            handler.getModel().setPaginationParameter(currentPaginationParameter);
            context.setCurrentPaginationParameter(null);

        } else {
            handler.getModel().resetPaginationParameterPage();
        }

        LoadFolderEmailsAction loadFolderEmailsAction =
                context.getActionFactory().createLogicAction(handler, LoadFolderEmailsAction.class);

        if (context.isActionInProgress(null)) {
            context.getActionEngine().runFullInternalAction(loadFolderEmailsAction);
        } else {
            context.getActionEngine().runAction(loadFolderEmailsAction);
        }
    }

}
