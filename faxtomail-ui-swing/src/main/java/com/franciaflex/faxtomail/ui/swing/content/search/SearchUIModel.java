package com.franciaflex.faxtomail.ui.swing.content.search;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.Priority;
import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.persistence.entities.SearchFilter;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailBeanUIModel;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Date;
import java.util.List;

/**
 * @author kmorin - kmorin@codelutin.com
 *
 */
public class SearchUIModel extends AbstractFaxToMailBeanUIModel<SearchFilter, SearchUIModel> {

    public static final String PROPERTY_ALLOWED_CLIENTS = "allowedClients";
    public static final String PROPERTY_RESULTS = "results";
    public static final String PROPERTY_MENU_ENABLED = "menuEnabled";
    public static final String PROPERTY_GROUP_ENABLED = "groupEnabled";
    public static final String PROPERTY_PAGINATION_PARAMETER = "paginationParameter";
    public static final String PROPERTY_PAGINATION_RESULT = "paginationResult";
    public static final String PROPERTY_RESULT_PER_PAGE = "resultPerPage";

    protected final SearchFilter editObject = new SearchFilter();

    protected List<Client> allowedClients;

    protected int resultPerPage = 50;
    protected PaginationParameter paginationParameter = PaginationParameter.of(0, resultPerPage, Email.PROPERTY_RECEPTION_DATE, false);
    protected PaginationResult<Email> paginationResult = PaginationResult.of(null, 0, paginationParameter);
    protected List<DemandeUIModel> results;

    protected boolean menuEnabled;
    protected boolean groupEnabled;

    protected static Binder<SearchUIModel, SearchFilter> toBeanBinder =
            BinderFactory.newBinder(SearchUIModel.class,
                    SearchFilter.class);

    protected static Binder<SearchFilter, SearchUIModel> fromBeanBinder =
            BinderFactory.newBinder(SearchFilter.class, SearchUIModel.class);

    public SearchUIModel() {
        super(fromBeanBinder, toBeanBinder);
        setAddGroupDemands(true);
    }

    public void setSender(String sender) {
        Object oldValue = getSender();
        editObject.setSender(sender);
        firePropertyChanged(SearchFilter.PROPERTY_SENDER, oldValue, sender);
    }

    public String getSender() {
        return editObject.getSender();
    }

   /* public void setDemandObject(String demandObject) {
        Object oldValue = getDemandObject();
        editObject.setDemandObject(demandObject);
        firePropertyChanged(SearchFilter.PROPERTY_DEMAND_OBJECT, oldValue, demandObject);
    }

    public String getDemandObject() {
        return editObject.getDemandObject();
    }*/

    public void setDemandSubject(String demandSubject) {
        Object oldValue = getDemandSubject();
        editObject.setDemandSubject(demandSubject);
        firePropertyChanged(SearchFilter.PROPERTY_DEMAND_SUBJECT, oldValue, demandSubject);
    }

    public String getDemandSubject() {
        return editObject.getDemandSubject();
    }

    public void setClient(Client client) {
        Object oldValue = getClient();
        editObject.setClient(client);
        firePropertyChanged(SearchFilter.PROPERTY_CLIENT, oldValue, client);
    }

    public Client getClient() {
        return editObject.getClient();
    }

    public List<Client> getAllowedClients() {
        return allowedClients;
    }

    public void setAllowedClients(List<Client> allowedClients) {
        Object oldValue = getAllowedClients();
        this.allowedClients = allowedClients;
        firePropertyChange(PROPERTY_ALLOWED_CLIENTS, oldValue, allowedClients);
    }

    public void setMessage(String message) {
        Object oldValue = getMessage();
        editObject.setMessage(message);
        firePropertyChanged(SearchFilter.PROPERTY_MESSAGE, oldValue, message);
    }

    public String getMessage() {
        return editObject.getMessage();
    }

    public void setBody(String body) {
        Object oldValue = getBody();
        editObject.setBody(body);
        firePropertyChanged(SearchFilter.PROPERTY_BODY, oldValue, body);
    }

    public String getBody() {
        return editObject.getBody();
    }

    public void setWaitingState(List<WaitingState> waitingStates) {
        editObject.setWaitingStates(waitingStates);
        firePropertyChanged(SearchFilter.PROPERTY_WAITING_STATES, null, waitingStates);
    }

    public List<WaitingState> getWaitingStates() {
        return editObject.getWaitingStates();
    }

    public void setTakenBy(FaxToMailUser faxToMailUser) {
        Object oldValue = getTakenBy();
        editObject.setTakenBy(faxToMailUser);
        firePropertyChanged(SearchFilter.PROPERTY_TAKEN_BY, oldValue, faxToMailUser);
    }

    public FaxToMailUser getTakenBy() {
        return editObject.getTakenBy();
    }
    
    public FaxToMailUser getPrintingBy() {
        return editObject.getPrintingBy();
    }
    
    public void setPrintingBy(FaxToMailUser faxToMailUser) {
        Object oldValue = getPrintingBy();
        editObject.setPrintingBy(faxToMailUser);
        firePropertyChanged(SearchFilter.PROPERTY_PRINTING_BY, oldValue, faxToMailUser);
    }
    
    public FaxToMailUser getArchivedBy() {
        return editObject.getArchivedBy();
    }
    
    public void setArchivedBy(FaxToMailUser faxToMailUser) {
        Object oldValue = getArchivedBy();
        editObject.setArchivedBy(faxToMailUser);
        firePropertyChanged(SearchFilter.PROPERTY_ARCHIVED_BY, oldValue, faxToMailUser);
    }

    public FaxToMailUser getReplyBy() {
        return editObject.getReplyBy();
    }

    public void setReplyBy(FaxToMailUser faxToMailUser) {
        Object oldValue = getReplyBy();
        editObject.setReplyBy(faxToMailUser);
        firePropertyChanged(SearchFilter.PROPERTY_REPLY_BY, oldValue, faxToMailUser);
    }

    public List<Priority> getPriority() {
        return editObject.getPriority();
    }

    public void setPriority(List<Priority> priority) {
        editObject.setPriority(priority);
        firePropertyChanged(SearchFilter.PROPERTY_PRIORITY, null, priority);
    }

    public List<DemandType> getDemandType() {
        return editObject.getDemandType();
    }

    public void setDemandType(List<DemandType> demandType) {
        editObject.setDemandType(demandType);
        firePropertyChanged(SearchFilter.PROPERTY_DEMAND_TYPE, null, demandType);
    }

    public List<DemandStatus> getDemandStatus() {
        return editObject.getDemandStatus();
    }

    public void setDemandStatus(List<DemandStatus> demandStatus) {
        editObject.setDemandStatus(demandStatus);
        firePropertyChanged(SearchFilter.PROPERTY_DEMAND_STATUS, null, demandStatus);
    }

    public void setEdiCodeNumber(String ediCodeNumber) {
        Object oldValue = getEdiCodeNumber();
        editObject.setEdiCodeNumber(ediCodeNumber);
        firePropertyChanged(SearchFilter.PROPERTY_EDI_CODE_NUMBER, oldValue, ediCodeNumber);
    }

    public String getEdiCodeNumber() {
        return editObject.getEdiCodeNumber();
    }

    public void setProjectReference(String projectReference) {
        Object oldValue = getProjectReference();
        editObject.setProjectReference(projectReference);
        firePropertyChanged(SearchFilter.PROPERTY_PROJECT_REFERENCE, oldValue, projectReference);
    }

    public String getProjectReference() {
        return editObject.getProjectReference();
    }

    public String getCommandNb() {
        return editObject.getCommandNb();
    }

    public void setCommandNb(String commandNb) {
        Object oldValue = getCommandNb();
        editObject.setCommandNb(commandNb);
        firePropertyChanged(SearchFilter.PROPERTY_COMMAND_NB, oldValue, commandNb);
    }

    public String getLocalReference() {
        return editObject.getLocalReference();
    }
    
    public void setLocalReference(String localReference) {
        Object oldValue = getLocalReference();
        editObject.setLocalReference(localReference);
        firePropertyChanged(SearchFilter.PROPERTY_LOCAL_REFERENCE, oldValue, localReference);
    }

    public FaxToMailUser getModifiedBy() {
        return editObject.getModifiedBy();
    }

    public void setModifiedBy(FaxToMailUser modifiedBy) {
        Object oldValue = getModifiedBy();
        editObject.setModifiedBy(modifiedBy);
        firePropertyChanged(SearchFilter.PROPERTY_MODIFIED_BY, oldValue, modifiedBy);
    }

    public FaxToMailUser getTransferBy() {
        return editObject.getTransferBy();
    }

    public void setTransferBy(FaxToMailUser transferBy) {
        Object oldValue = getTransferBy();
        editObject.setTransferBy(transferBy);
        firePropertyChanged(SearchFilter.PROPERTY_TRANSFER_BY, oldValue, transferBy);
    }

    public Date getMinModificationDate() {
        return editObject.getMinModificationDate();
    }

    public void setMinModificationDate(Date minModificationDate) {
        Object oldValue = getMinModificationDate();
        editObject.setMinModificationDate(minModificationDate);
        firePropertyChanged(SearchFilter.PROPERTY_MIN_MODIFICATION_DATE, oldValue, minModificationDate);
    }

    public Date getMaxModificationDate() {
        return editObject.getMaxModificationDate();
    }

    public void setMaxModificationDate(Date maxModificationDate) {
        Object oldValue = getMaxModificationDate();
        editObject.setMaxModificationDate(maxModificationDate);
        firePropertyChanged(SearchFilter.PROPERTY_MAX_MODIFICATION_DATE, oldValue, maxModificationDate);
    }

    public Date getMinReceptionDate() {
        return editObject.getMinReceptionDate();
    }

    public void setMinReceptionDate(Date minReceptionDate) {
        Object oldValue = getMinReceptionDate();
        editObject.setMinReceptionDate(minReceptionDate);
        firePropertyChanged(SearchFilter.PROPERTY_MIN_RECEPTION_DATE, oldValue, minReceptionDate);
    }

    public Date getMaxReceptionDate() {
        return editObject.getMaxReceptionDate();
    }

    public void setMaxReceptionDate(Date maxReceptionDate) {
        Object oldValue = getMaxReceptionDate();
        editObject.setMaxReceptionDate(maxReceptionDate);
        firePropertyChanged(SearchFilter.PROPERTY_MAX_RECEPTION_DATE, oldValue, maxReceptionDate);
    }

    public Date getMinPrintingDate() {
        return editObject.getMinPrintingDate();
    }

    public void setMinPrintingDate(Date minPrintingDate) {
        Object oldValue = getMinPrintingDate();
        editObject.setMinPrintingDate(minPrintingDate);
        firePropertyChanged(SearchFilter.PROPERTY_MIN_PRINTING_DATE, oldValue, minPrintingDate);
    }

    public Date getMaxPrintingDate() {
        return editObject.getMaxPrintingDate();
    }

    public void setMaxPrintingDate(Date maxPrintingDate) {
        Object oldValue = getMaxPrintingDate();
        editObject.setMaxPrintingDate(maxPrintingDate);
        firePropertyChanged(SearchFilter.PROPERTY_MAX_PRINTING_DATE, oldValue, maxPrintingDate);
    }
    
    public Date getMinArchivedDate() {
        return editObject.getMinArchivedDate();
    }

    public void setMinArchivedDate(Date minArchivedDate) {
        Object oldValue = getMinArchivedDate();
        editObject.setMinArchivedDate(minArchivedDate);
        firePropertyChanged(SearchFilter.PROPERTY_MIN_ARCHIVED_DATE, oldValue, minArchivedDate);
    }
    
    public Date getMaxArchivedDate() {
        return editObject.getMaxArchivedDate();
    }

    public void setMaxArchivedDate(Date maxArchivedDate) {
        Object oldValue = getMaxArchivedDate();
        editObject.setMaxArchivedDate(maxArchivedDate);
        firePropertyChanged(SearchFilter.PROPERTY_MAX_ARCHIVED_DATE, oldValue, maxArchivedDate);
    }

    public Date getMinReplyDate() {
        return editObject.getMinReplyDate();
    }

    public void setMinReplyDate(Date minReplyDate) {
        Object oldValue = getMinReplyDate();
        editObject.setMinReplyDate(minReplyDate);
        firePropertyChanged(SearchFilter.PROPERTY_MIN_REPLY_DATE, oldValue, minReplyDate);
    }

    public Date getMaxReplyDate() {
        return editObject.getMaxReplyDate();
    }

    public void setMaxReplyDate(Date maxReplyDate) {
        Object oldValue = getMaxReplyDate();
        editObject.setMaxReplyDate(maxReplyDate);
        firePropertyChanged(SearchFilter.PROPERTY_MAX_REPLY_DATE, oldValue, maxReplyDate);
    }
    
    public Date getMinTransferDate() {
        return editObject.getMinTransferDate();
    }

    public void setMinTransferDate(Date minTransferDate) {
        Object oldValue = getMinTransferDate();
        editObject.setMinTransferDate(minTransferDate);
        firePropertyChanged(SearchFilter.PROPERTY_MIN_TRANSFER_DATE, oldValue, minTransferDate);
    }
    
    public Date getMaxTransferDate() {
        return editObject.getMaxTransferDate();
    }

    public void setMaxTransferDate(Date maxTransferDate) {
        Object oldValue = getMaxTransferDate();
        editObject.setMaxTransferDate(maxTransferDate);
        firePropertyChanged(SearchFilter.PROPERTY_MAX_TRANSFER_DATE, oldValue, maxTransferDate);
    }
    
    public List<Range> getGamme() {
        return editObject.getGamme();
    }
    
    public void setGamme(List<Range> gamme) {
        editObject.setGamme(gamme);
        firePropertyChanged(SearchFilter.PROPERTY_GAMME, null, gamme);
    }

    public List<DemandeUIModel> getResults() {
        return results;
    }

    public void setResults(List<DemandeUIModel> results) {
        this.results = results;
        firePropertyChanged(PROPERTY_RESULTS, null, results);
    }

    public void setResultPerPage(int resultPerPage) {
        int oldValue = this.resultPerPage;
        this.resultPerPage = resultPerPage;
        firePropertyChanged(PROPERTY_RESULT_PER_PAGE, oldValue, resultPerPage);
    }

    public int getResultPerPage() {
        return resultPerPage;
    }

    public void setPaginationParameter(PaginationParameter paginationParameter) {
        Object oldValue = this.paginationParameter;
        this.paginationParameter = paginationParameter;
        firePropertyChanged(PROPERTY_PAGINATION_PARAMETER, oldValue, paginationParameter);
    }

    public PaginationParameter getPaginationParameter() {
        return paginationParameter;
    }
    
    public void resetPaginationParameter() {
        setPaginationParameter(PaginationParameter.of(0, resultPerPage, TopiaEntity.PROPERTY_TOPIA_CREATE_DATE, false));
    }

    public void setPaginationResult(PaginationResult<Email> paginationResult) {
        Object oldValue = this.paginationResult;
        this.paginationResult = paginationResult;
        firePropertyChanged(PROPERTY_PAGINATION_RESULT, oldValue, paginationResult);
    }

    public PaginationResult<Email> getPaginationResult() {
        return paginationResult;
    }

    public boolean isMenuEnabled() {
        return menuEnabled;
    }

    public void setMenuEnabled(boolean menuEnabled) {
        Object oldValue = isMenuEnabled();
        this.menuEnabled = menuEnabled;
        firePropertyChanged(PROPERTY_MENU_ENABLED, oldValue, menuEnabled);
    }

    public boolean isGroupEnabled() {
        return groupEnabled;
    }

    public void setGroupEnabled(boolean groupEnabled) {
        Object oldValue = isGroupEnabled();
        this.groupEnabled = groupEnabled;
        firePropertyChanged(PROPERTY_GROUP_ENABLED, oldValue, groupEnabled);
    }

    public boolean isAddGroupDemands() {
        return editObject.isAddGroupDemands();
    }

    public void setAddGroupDemands(boolean addGroupDemands) {
        Object oldValue = isAddGroupDemands();
        editObject.setAddGroupDemands(addGroupDemands);
        firePropertyChange(SearchFilter.PROPERTY_ADD_GROUP_DEMANDS, oldValue, addGroupDemands);
    }

    @Override
    protected SearchFilter newEntity() {
        return new SearchFilter();
    }
}
