package com.franciaflex.faxtomail.ui.swing.actions;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.FaxToMailScreen;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.MainUIHandler;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.context.JAXXContextEntryDef;

/**
 * Action to change the screen.
 * 
 * Will just check that the current screen can be quit via
 * the {@link com.franciaflex.faxtomail.ui.swing.content.MainUIHandler#quitCurrentScreen()}.
 *
 * @author kmorin - kmorin@codelutin.com
 */
public abstract class AbstractChangeScreenAction extends AbstractMainUIFaxToMailAction {

    /**
     * Context entry to keep previous screen.
     */
    protected static final JAXXContextEntryDef<FaxToMailScreen> PREVIOUS_SCREEN =
            JAXXUtil.newContextEntryDef("previousScreen", FaxToMailScreen.class);

    /**
     * Screen where to go.
     */
    protected FaxToMailScreen screen;

    /**
     * Flag to skip the check of current screen.
     */
    protected boolean skipCheckCurrentScreen;

    protected AbstractChangeScreenAction(MainUIHandler handler,
                                         boolean hideBody,
                                         FaxToMailScreen screen) {
        super(handler, hideBody);
        this.screen = screen;
    }

    public void setSkipCheckCurrentScreen(boolean skipCheckCurrentScreen) {
        this.skipCheckCurrentScreen = skipCheckCurrentScreen;
    }

    protected void setScreen(FaxToMailScreen screen) {
        this.screen = screen;
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        result &= skipCheckCurrentScreen || getHandler().quitCurrentScreen();
        return result;
    }

    @Override
    public void doAction() throws Exception {

        FaxToMailUIContext context = getContext();

        FaxToMailScreen previousScreen = context.getScreen();
        if (getUI() != null) {
            if (previousScreen == null) {
                PREVIOUS_SCREEN.removeContextValue(getUI());

            } else if (screen != previousScreen) {
                PREVIOUS_SCREEN.setContextValue(getUI(), previousScreen);
            }
        }

        // clean current screen
        context.setScreen(null);

        // change screen
        context.setScreen(screen);
    }

}
