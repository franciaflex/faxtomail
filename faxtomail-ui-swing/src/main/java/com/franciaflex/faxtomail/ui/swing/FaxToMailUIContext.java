package com.franciaflex.faxtomail.ui.swing;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.persistence.RessourceClassLoader;
import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.EmailFilter;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaApplicationContext;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.Priority;
import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.InitFaxToMailService;
import com.franciaflex.faxtomail.services.service.ReferentielService;
import com.franciaflex.faxtomail.services.service.UserService;
import com.franciaflex.faxtomail.services.service.ldap.AuthenticationException;
import com.franciaflex.faxtomail.services.service.ldap.Contact;
import com.franciaflex.faxtomail.ui.swing.content.MainUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.search.SearchUIModel;
import com.franciaflex.faxtomail.ui.swing.util.DemandeListUISwingSessionState;
import com.franciaflex.faxtomail.ui.swing.util.FolderTreeSwingSessionState;
import com.franciaflex.faxtomail.ui.swing.util.UIMessageNotifier;
import com.franciaflex.faxtomail.ui.swing.util.data.Handler;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import jaxx.runtime.swing.editor.bean.BeanDoubleList;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.swing.session.BeanDoubleListState;
import jaxx.runtime.swing.session.BeanFilterableComboBoxState;
import jaxx.runtime.swing.session.State;
import jaxx.runtime.swing.session.SwingSession;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractBean;
import org.jdesktop.swingx.JXLoginPane;
import org.jdesktop.swingx.JXLoginPane.Status;
import org.jdesktop.swingx.auth.LoginService;
import org.jdesktop.swingx.error.ErrorInfo;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.ClassPathI18nInitializer;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.ApplicationConfiguration;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;
import org.nuiton.jaxx.application.swing.ApplicationUI;
import org.nuiton.jaxx.application.swing.ApplicationUIContext;
import org.nuiton.jaxx.application.swing.action.ApplicationActionEngine;
import org.nuiton.jaxx.application.swing.action.ApplicationActionFactory;
import org.nuiton.jaxx.application.swing.action.ApplicationActionUI;
import org.nuiton.jaxx.application.swing.action.ApplicationUIAction;
import org.nuiton.jaxx.application.swing.util.ApplicationErrorHelper;
import org.nuiton.jaxx.application.swing.util.Cancelable;
import org.nuiton.util.converter.ConverterUtil;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Closeable;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * UI application context.
 *
 * @author kmorin - morin@codelutin.com
 *
 */
public class FaxToMailUIContext extends AbstractBean implements Closeable, UIMessageNotifier, ApplicationUIContext {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FaxToMailUIContext.class);

    public static final String PROPERTY_SCREEN = "screen";

    public static final String PROPERTY_LOCALE = "locale";

    public static final Set<String> PROPERTIES_TO_SAVE = Sets.newHashSet(PROPERTY_LOCALE);

    public static final String PROPERTY_BUSY = "busy";

    public static final String PROPERTY_HIDE_BODY = "hideBody";

    public static final String PROPERTY_ACTION_IN_PROGRESS = "actionInProgress";

    /**
     * Application context (only one for all the application).
     */
    private static FaxToMailUIContext applicationContext;

    /**
     * Application global configuration.
     */
    protected final FaxToMailConfiguration config;

    /**
     * ClassLoader ressource.
     */
    protected final RessourceClassLoader resourceLoader;

    /**
     * Topia application context.
     */
    protected FaxToMailTopiaApplicationContext topiaApplicationContext;

    /**
     * Swing session used to save ui states.
     */
    protected final SwingSession swingSession;

    /**
     * Error helper.
     */
    protected final ApplicationErrorHelper errorHelper;

    /**
     * Current screen displayed in ui.
     */
    protected FaxToMailScreen screen;

    /**
     * Current locale used in application.
     */
    protected Locale locale;

    /**
     * Busy state ({@code true} when a blocking action is running).
     */
    protected boolean busy;

    /**
     * Flag to hide (or not) the body of application.
     */
    protected boolean hideBody;

    /**
     * Flag to know if an action is already in progress.
     */
    protected boolean actionInProgress;

    /**
     * Message notifiers.
     */
    protected final Set<UIMessageNotifier> messageNotifiers;

    private MainUI mainUI;

    private ApplicationActionUI actionUI;

    private JFrame secondaryFrame;

    private final ApplicationActionFactory faxToMailActionFactory;

    private final ApplicationActionEngine faxToMailActionEngine;

    /** L'utilisateur actuellement connecté. */
    protected FaxToMailUser currentUser;

    /** Le dossier courant (pour ???). */
    protected MailFolder currentMailFolder;

    protected PaginationParameter currentPaginationParameter;

    /** L'email courant (pour quoi faire ?). */
    protected DemandeUIModel currentEmail;

    protected Map<DemandeUIModel, JFrame> demandesFrames = new HashMap<>();

    //FIXME rustine en attendant de trouver pourquoi ca rame autant
    protected boolean useFolderCache;

    protected FolderData currentFolderEmails = new FolderData();

    /** Les dossiers étendus dans la liste (pour pouvoir les étendres encore au rechargement de l'UI). */
    protected List<MailFolder> listExpandedFolders = new ArrayList<MailFolder>();
    /** Les dossiers étendus dans la sélection de dossier pour transfert (pour pouvoir les étendres encore au rechargement de l'UI). */
    protected List<MailFolder> transmitExpandedFolders = new ArrayList<MailFolder>();

    protected SearchUIModel search;

    /** Active user cache. */
    protected List<FaxToMailUser> faxtomailUserCache;
    /** Waiting state cache. */
    protected List<WaitingState> waitingStateCache;
    /** Demand type cache. */
    protected List<DemandType> demandTypeCache;
    /** Priority cache. */
    protected List<Priority> priorityCache;
    /** Range cache. */
    protected List<Range> rangeCache;
    /**  */
    protected List<Contact> contactCache;

    protected List<Client> clientCache;

    protected EmailFilter emailFilter = new EmailFilter();

    //JC-180220- used to fix #9713
    protected String lastDisplayedMailFolder;

    public static FaxToMailUIContext newContext(FaxToMailConfiguration config) {
        Preconditions.checkNotNull(config);
        Preconditions.checkState(applicationContext == null,
                                 "Application context was already opened!");
        applicationContext = new FaxToMailUIContext(config);
        return applicationContext;
    }

    public static FaxToMailUIContext getApplicationContext() {
        return applicationContext;
    }

    public ApplicationErrorHelper getErrorHelper() {
        return applicationContext.errorHelper;
    }

    @Override
    public String getI18nPrefix() {
        return "faxtomail.property.";
    }

    @Override
    public String getDateFormat() {
        return getConfig().getDateFormat();
    }

    protected FaxToMailUIContext(FaxToMailConfiguration config) {
        this.config = config;
        this.resourceLoader = new RessourceClassLoader(Thread.currentThread().getContextClassLoader());
        topiaApplicationContext = new FaxToMailTopiaApplicationContext(config.getTopiaProperties());

        FaxToMailServiceContext serviceContext = newServiceContext();
        useFolderCache = serviceContext.getApplicationConfig().isUseFolderCache();

        InitFaxToMailService initFaxToMailService = serviceContext.getInitFaxToMailService();
        initFaxToMailService.init();

        Map<Class, State> additionalStates = Maps.newHashMap();
        additionalStates.put(BeanFilterableComboBox.class, new BeanFilterableComboBoxState());
        additionalStates.put(BeanDoubleList.class, new BeanDoubleListState());
        additionalStates.put(DemandeListUI.class, new DemandeListUISwingSessionState());
        additionalStates.put(JTree.class, new FolderTreeSwingSessionState());
        File uiConfigFile = getConfig().getUIConfigFile();
        this.swingSession = new SwingSession(uiConfigFile, false, additionalStates);
        if (!uiConfigFile.exists()) {
            swingSession.loadSafeStates();
        }

        this.errorHelper = new ApplicationErrorHelper(this) {

            @Override
            public void showWarningDialog(String message) {

                JOptionPane.showMessageDialog(context.getActionUI(), "<html><body>" + message + "</body></html>",
                                              t("application.error.ui.business.warning"),
                                              JOptionPane.WARNING_MESSAGE);
            }

            @Override
            public void showErrorDialog(String message, Throwable cause) {
                if (log.isErrorEnabled()) {
                    log.error(message, cause);
                }
                super.showErrorDialog(message, cause);
            }

            @Override
            public void reportError(ErrorInfo errorInfo) throws NullPointerException {
                // empeche la fenetre d'erreur de boucler déjà
            }
        };

        UIMessageNotifier logMessageNotifier = new UIMessageNotifier() {

            @Override
            public void showInformationMessage(String message) {
                if (StringUtils.isNotBlank(message)) {
                    message = message.replaceAll("\\<strong\\>", "");
                    message = message.replaceAll("\\<.strong\\>", "");
                    message = message.replaceAll("\\<li\\>", "");
                    message = message.replaceAll("\\<.li\\>", "");
                    message = message.replaceAll("\\<ul\\>", "");
                    message = message.replaceAll("\\<.ul\\>", "");
                    if (log.isInfoEnabled()) {
                        log.info(message);
                    }
                }
            }
        };

        this.messageNotifiers = Sets.newHashSet();
        addMessageNotifier(logMessageNotifier);

        faxToMailActionFactory = new FaxToMailActionFactory();
        faxToMailActionEngine = new ApplicationActionEngine(faxToMailActionFactory);
    }

    @Override
    public ApplicationConfiguration getConfiguration() {
        return config;
    }

    @Override
    public Component getBodyUI() {
        MainUI mainUI = getMainUI();
        return mainUI == null ? null : mainUI.getBody();
    }

    @Override
    public Component getStatusUI() {
        MainUI mainUI = getMainUI();
        return mainUI == null ? null : mainUI.getStatus();
    }

    //------------------------------------------------------------------------//
    //-- Open / close methods                                               --//
    //------------------------------------------------------------------------//

    public void init() {

        Handler.install();

        config.prepareDirectories();

        // use our special classLoader (which will read some files from resources from a configuration directory)
        Thread.currentThread().setContextClassLoader(getResourceLoader());

        // converters are stored in current classloader, we need then to rescan them
        // each time we change current classloader
        ConverterUtil.deregister();
        ConverterUtil.initConverters();

        // Use shutdownHook to close context on System.exit
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

            @Override
            public void run() {
                if (log.isInfoEnabled()) {
                    log.info("Close context on shutdown");
                }
                close();
            }
        }));

        //--------------------------------------------------------------------//
        // init i18n
        //--------------------------------------------------------------------//

        Locale i18nLocale = config.getI18nLocale();

        if (log.isInfoEnabled()) {
            log.info(String.format("Starts i18n with locale [%s]", i18nLocale));
        }
        
        try {
            // production case
            I18n.init(new DefaultI18nInitializer("faxtomail-i18n"), i18nLocale);
        } catch (RuntimeException ex) {
            // fallback case : strange ide behaviour (eclipse)
            I18n.init(new ClassPathI18nInitializer(), i18nLocale);
        }

        // try to autologin user from username of current X session
        autologinUser();

        // init cache
        initContextCache();

        //--------------------------------------------------------------------//
        // init action UI
        //--------------------------------------------------------------------//
        setActionUI(new ApplicationActionUI(null, this));

    }

    /**
     * Init list used in application wide scope and put it in cache.
     */
    protected void initContextCache() {
        if (log.isInfoEnabled()) {
            log.info("Adding referentiel in cache");
        }
        FaxToMailServiceContext serviceContext = newServiceContext();
        // referentiel
        ReferentielService referentielService = serviceContext.getReferentielService();
        setRangeCache(referentielService.getAllRange());
        setDemandTypeCache(referentielService.getAllDemandType());
        setWaitingStateCache(referentielService.getAllWaitingState());
        setPriorityCache(referentielService.getAllPriority());
        // users
        UserService userService = serviceContext.getUserService();
        setFaxtomailUserCache(userService.getAllActiveUsers());

        if (!config.isLdapMock()) {
            try {
                setContactCache(serviceContext.getLdapService().getUserAndEmails());

            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Error while getting contact from ldap", e);
                }
            }
        } else {
            Contact testContact = new Contact("Test Contact", "faxtomail-tests@codelutin.com");
            List<Contact> contacts = new ArrayList<>();
            contacts.add(testContact);
            setContactCache(contacts);
        }

        List<MailFolder> folders = serviceContext.getMailFolderService()
                .getRootMailFoldersWithReadingRights(getCurrentUser());
        if (CollectionUtils.isNotEmpty(folders)) {
            List<Client> clients = serviceContext.getClientService().getClientsForFolder(folders.get(0));
            setClientCache(clients);
        }
    }

    /**
     * Recupere l'utilisateur connecté ou demande les identifiants à l'utilisateur.
     */
    protected void autologinUser() {

        String principal = getLoggedInUsername();

        // l'authentification par defaut se fait par trigramme
        final FaxToMailServiceContext serviceContext = newServiceContext();
        try {
            try {
                FaxToMailUser currentUser = serviceContext.getLdapService().getUserFromPrincipal(principal);
                if (log.isInfoEnabled()) {
                    log.info("Connected as " + currentUser.getFirstName() + " " + currentUser.getLastName());
                }
                setCurrentUser(currentUser);
            } catch (AuthenticationException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't anthenticate user", ex);
                }

            }

            // si ca n'a pas fonctionné, on retente par login mot de passe
            while (getCurrentUser() == null) {
                JXLoginPane pane = new JXLoginPane();
                pane.setLoginService(new LoginService() {
                    @Override
                    public boolean authenticate(String name, char[] password, String server) throws Exception {
                        boolean result = false;
                        try {
                            FaxToMailUser user = serviceContext.getLdapService().authenticateUser(name, String.valueOf(password));
                            setCurrentUser(user);
                            result = true;
                        } catch (AuthenticationException ex) {
                            if (log.isWarnEnabled()) {
                                log.warn("Can't anthenticate user", ex);
                            }
                        }
                        return result;
                    }
                });

                // show login dialog
                Status loginStatus = JXLoginPane.showLoginDialog(null, pane);
                if (loginStatus == Status.CANCELLED) {
                    throw new ApplicationBusinessException("Authentication canceled");
                }
            }

        } finally {
            IOUtils.closeQuietly(serviceContext);
        }
    }

    /**
     * Retourne l'utilsateur connecté sur la session utilisateur (ou a defaut un utilisateur de test).
     * 
     * @return trigramme
     */
    protected String getLoggedInUsername() {
        String result = getConfig().getLdapTestPrincipal();
        if (StringUtils.isBlank(result)) {
            result = System.getProperty("user.name");
        }
        return result;
    }

    public void open() {

        setLocale(config.getI18nLocale());

        // save back to config
        saveContextToConfig();

        // list when programId or campaingId change to save the configuration
        addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                if (PROPERTIES_TO_SAVE.contains(evt.getPropertyName())) {
                    saveContextToConfig();
                }
            }
        });
    }

    @Override
    public void close() {

//        // Clear data references
        messageNotifiers.clear();
//        validationContext = null;
//        IOUtils.closeQuietly(dataContext);

        setScreen(null);

//        IOUtils.closeQuietly(serviceContext);

//        // remove listeners
        PropertyChangeListener[] listeners = getPropertyChangeListeners();
        for (PropertyChangeListener listener : listeners) {
            if (log.isDebugEnabled()) {
                log.debug("Remove listener: " + listener);
            }
            removePropertyChangeListener(listener);
        }
        setMainUI(null);
        if (actionUI != null) {

            // close action ui
            actionUI.getModel().clear();
        }
        setActionUI(null);
    }

    //------------------------------------------------------------------------//
    //-- Service methods                                                    --//
    //------------------------------------------------------------------------//

    public FaxToMailServiceContext newServiceContext() {
        FaxToMailServiceContext serviceContext = FaxToMailServiceContext.newServiceContext(topiaApplicationContext);
        serviceContext.setApplicationConfig(config);
        return serviceContext;
    }

    //------------------------------------------------------------------------//
    //-- Config methods                                                     --//
    //------------------------------------------------------------------------//

    public FaxToMailConfiguration getConfig() {
        return config;
    }

    protected void saveContextToConfig() {
        config.setI18nLocale(getLocale());
        config.save();
    }

    //------------------------------------------------------------------------//
    //-- UI methods                                                         --//
    //------------------------------------------------------------------------//

    public MainUI getMainUI() {
        return mainUI;
    }

    public void setMainUI(MainUI mainUI) {
        this.mainUI = mainUI;
    }

    public JFrame getSecondaryFrame() {
        return secondaryFrame;
    }

    public void setSecondaryFrame(JFrame secondaryFrame) {
        this.secondaryFrame = secondaryFrame;
    }

    public void closeSecondaryFrame() {
        JFrame secondaryFrame = getSecondaryFrame();
        if (secondaryFrame != null) {
            Container contentPane = secondaryFrame.getContentPane();
            if (contentPane instanceof ApplicationUI) {
                ApplicationUI dialogContent = (ApplicationUI) contentPane;
                AbstractApplicationUIHandler handler = dialogContent.getHandler();
                if (handler instanceof Cancelable) {
                    ((Cancelable) handler).cancel();
                }
            }
        }
    }

    @Override
    public ApplicationActionUI getActionUI() {
        return actionUI;
    }

    @Override
    public ApplicationActionUI getExistingActionUI() {
        while (actionUI == null) {

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                // ignore this one
            }
        }
        return actionUI;
    }

    public void setActionUI(ApplicationActionUI actionUI) {
        this.actionUI = actionUI;
    }

    public SwingSession getSwingSession() {
        return swingSession;
    }

    public FaxToMailScreen getScreen() {
        return screen;
    }

    public void setScreen(FaxToMailScreen screen) {
        Object oldValue = getScreen();
        this.screen = screen;
        firePropertyChange(PROPERTY_SCREEN, oldValue, screen);
    }

    @Override
    public boolean isBusy() {
        return busy;
    }

    @Override
    public void setBusy(boolean busy) {
        this.busy = busy;
        firePropertyChange(PROPERTY_BUSY, null, busy);
    }

    @Override
    public boolean isHideBody() {
        return hideBody;
    }

    @Override
    public void setHideBody(boolean hideBody) {
        this.hideBody = hideBody;
        firePropertyChange(PROPERTY_HIDE_BODY, null, hideBody);
    }

    @Override
    public boolean isActionInProgress(ApplicationUIAction action) {
        return actionInProgress;
    }

    @Override
    public void setActionInProgress(ApplicationUIAction action, boolean actionInProgress) {
        this.actionInProgress = actionInProgress;
        firePropertyChange(PROPERTY_ACTION_IN_PROGRESS, null, actionInProgress);
    }

    @Override
    public Color getColorBlockingLayer() {
        return getConfig().getColorBlockingLayer();
    }

    @Override
    public ApplicationActionFactory getActionFactory() {
        return faxToMailActionFactory;
    }

    @Override
    public ApplicationActionEngine getActionEngine() {
        return faxToMailActionEngine;
    }
    //------------------------------------------------------------------------//
    //-- UIMessageNotifier methods                                          --//
    //------------------------------------------------------------------------//

    public void addMessageNotifier(UIMessageNotifier messageNotifier) {
        this.messageNotifiers.add(messageNotifier);
    }

    public void removeMessageNotifier(UIMessageNotifier messageNotifier) {
        this.messageNotifiers.remove(messageNotifier);
    }

    @Override
    public void showInformationMessage(String message) {
        for (UIMessageNotifier messageNotifier : messageNotifiers) {
            messageNotifier.showInformationMessage(message);
        }
    }

    public RessourceClassLoader getResourceLoader() {
        return resourceLoader;
    }

    //------------------------------------------------------------------------//
    //-- Other methods                                                      --//
    //------------------------------------------------------------------------//

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
        firePropertyChange(PROPERTY_LOCALE, null, locale);
    }

    public FaxToMailUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(FaxToMailUser currentUser) {
        this.currentUser = currentUser;
    }

    public MailFolder getCurrentMailFolder() {
        return currentMailFolder;
    }

    public void setCurrentMailFolder(MailFolder currentMailFolder) {
        this.currentMailFolder = currentMailFolder;
    }

    public PaginationParameter getCurrentPaginationParameter() {
        return currentPaginationParameter;
    }

    public void setCurrentPaginationParameter(PaginationParameter currentPaginationParameter) {
        this.currentPaginationParameter = currentPaginationParameter;
    }

    public DemandeUIModel getCurrentEmail() {
        return currentEmail;
    }

    public void setCurrentEmail(DemandeUIModel currentEmail) {
        this.currentEmail = currentEmail;
    }

    public FolderData getCurrentFolderEmails() {
        return currentFolderEmails;
    }

    public List<MailFolder> getListExpandedFolders() {
        return listExpandedFolders;
    }

    public void setListExpandedFolders(List<MailFolder> listExpandedFolders) {
        this.listExpandedFolders = listExpandedFolders;
    }

    public List<MailFolder> getTransmitExpandedFolders() {
        return transmitExpandedFolders;
    }

    public void setTransmitExpandedFolders(List<MailFolder> transmitExpandedFolders) {
        this.transmitExpandedFolders = transmitExpandedFolders;
    }

    public SearchUIModel getSearch() {
        return search;
    }

    public void setSearch(SearchUIModel search) {
        this.search = search;
    }

    public List<FaxToMailUser> getFaxtomailUserCache() {
        return faxtomailUserCache;
    }

    public void setFaxtomailUserCache(List<FaxToMailUser> faxtomailUserCache) {
        this.faxtomailUserCache = faxtomailUserCache;
    }

    public List<WaitingState> getWaitingStateCache() {
        return waitingStateCache;
    }

    public void setWaitingStateCache(List<WaitingState> waitingStateCache) {
        this.waitingStateCache = waitingStateCache;
    }

    public List<DemandType> getDemandTypeCache() {
        return demandTypeCache;
    }

    public void setDemandTypeCache(List<DemandType> demandTypeCache) {
        this.demandTypeCache = demandTypeCache;
    }

    public List<Priority> getPriorityCache() {
        return priorityCache;
    }

    public void setPriorityCache(List<Priority> priorityCache) {
        this.priorityCache = priorityCache;
    }

    public List<Range> getRangeCache() {
        return rangeCache;
    }

    public void setRangeCache(List<Range> rangeCache) {
        this.rangeCache = rangeCache;
    }

    public List<Contact> getContactCache() {
        return contactCache;
    }

    public void setContactCache(List<Contact> contactCache) {
        this.contactCache = contactCache;
    }

    public boolean isUseFolderCache() {
        return useFolderCache;
    }

    public List<Client> getClientCache() {
        return clientCache;
    }

    public void setClientCache(List<Client> clientCache) {
        this.clientCache = clientCache;
    }

    public class FolderData {

        protected MailFolder folder;
        protected List<DemandeUIModel> demands;
        protected PaginationResult<Email> paginationResult;

        public MailFolder getFolder() {
            return folder;
        }

        public void setFolder(MailFolder folder) {
            this.folder = folder;
        }

        public List<DemandeUIModel> getDemands() {
            return demands;
        }

        public void setDemands(List<DemandeUIModel> demands) {
            this.demands = new ArrayList<DemandeUIModel>(demands);
        }

        public void removeDemand(DemandeUIModel demand) {
            this.demands.remove(demand);
        }

        public void removeAllDemands(Collection<DemandeUIModel> demands) {
            this.demands.removeAll(demands);
        }

        public PaginationResult<Email> getPaginationResult() {
            return paginationResult;
        }

        public void setPaginationResult(PaginationResult<Email> paginationResult) {
            this.paginationResult = paginationResult;
        }
    }

    public EmailFilter getEmailFilter() {
        return emailFilter;
    }

    public void setEmailFilter(EmailFilter emailFilter) {
        this.emailFilter = emailFilter;
    }

    public JFrame getFrameForDemande(DemandeUIModel demande) {
        return demandesFrames.get(demande);
    }
    public void setFrameForDemande(DemandeUIModel demande, JFrame frame) {
        demandesFrames.put(demande, frame);
    }

    public void removeDemande(DemandeUIModel demande) {
        demandesFrames.remove(demande);
    }

    public String getLastDisplayedMailFolder() {
        return lastDisplayedMailFolder;
    }

    public void setLastDisplayedMailFolder(String lastDisplayedMailFolder) {
        this.lastDisplayedMailFolder = lastDisplayedMailFolder;
    }
}
