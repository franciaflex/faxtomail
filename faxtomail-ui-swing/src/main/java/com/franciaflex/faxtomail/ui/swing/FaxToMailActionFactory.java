package com.franciaflex.faxtomail.ui.swing;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.actions.AbstractMainUIFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;
import org.nuiton.jaxx.application.swing.action.AbstractApplicationAction;
import org.nuiton.jaxx.application.swing.action.ApplicationActionFactory;

import static org.nuiton.i18n.I18n.t;

/**
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class FaxToMailActionFactory extends ApplicationActionFactory {

    @Override
    public <A extends AbstractApplicationAction> A createLogicAction(AbstractApplicationUIHandler handler,
                                                                     Class<A> actionName) {
        FaxToMailUIContext context = (FaxToMailUIContext) handler.getContext();
        if (AbstractMainUIFaxToMailAction.class.isAssignableFrom(actionName) && context.getMainUI() != null) {
            handler = context.getMainUI().getHandler();
        }

        try {
            // create action
            A result = ConstructorUtils.invokeConstructor(actionName, (AbstractFaxToMailUIHandler) handler);
            return result;

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("application.action.create.error", actionName), e);
        }
    }
}
