package com.franciaflex.faxtomail.ui.swing.content.attachment;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.decorator.Decorator;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Renderer of a attachement editor in a table cell.
 *
 * @author kmorin - morin@codelutin.com
 */
public class AttachmentCellRenderer extends DefaultTableCellRenderer {

    public static final String TEXT_PATTERN = "<html><body>%s</body></html>";

    private static final long serialVersionUID = 1L;

    private final String noneText;

    private final Decorator<Attachment> decorator;

//    private Font defaulfFont;
//
//    private Font selectedFont;

    public static AttachmentCellRenderer newRender(Decorator<Attachment> decorator) {
        return new AttachmentCellRenderer(decorator);
    }

    protected AttachmentCellRenderer(Decorator<Attachment> decorator) {
        setHorizontalAlignment(CENTER);
        setIcon(SwingUtil.createActionIcon("attachment"));
        this.noneText = n("faxtomail.attachmentEditor.none.tip");
        this.decorator = decorator;
    }

    @Override
    protected void setValue(Object value) {
        // do nothing
    }

    @Override
    public JComponent getTableCellRendererComponent(JTable table,
                                                    Object value,
                                                    boolean isSelected,
                                                    boolean hasFocus,
                                                    int row,
                                                    int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        List<Attachment> attachments = (List<Attachment>) value;

        String toolTipTextValue;

        if (CollectionUtils.isEmpty(attachments)) {

            // use HTML to show the tooltip in italic
            toolTipTextValue = "<i>" + t(noneText) + "</i>";


        } else {

            StringBuilder sb = new StringBuilder();
            for (Attachment attachment : attachments) {
                sb.append("<br/>").append(decorator.toString(attachment));
            }
            // use html to display the tooltip on several lines
            toolTipTextValue = sb.substring(5);
        }
        String textValue = t("faxtomail.attachmentCellRenderer.text", attachments != null ? attachments.size() : 0);
        boolean editable = table.isCellEditable(row, column);
        toolTipTextValue = String.format(TEXT_PATTERN, toolTipTextValue);
        setEnabled(editable);
        setText(textValue);
        setToolTipText(toolTipTextValue);

        return this;
    }
}
