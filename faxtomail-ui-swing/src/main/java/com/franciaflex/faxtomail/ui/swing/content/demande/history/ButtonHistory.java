package com.franciaflex.faxtomail.ui.swing.content.demande.history;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.History;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.toolbar.AbstractToolbarPopupButton;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;

import javax.swing.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Button to edit attachments.
 *
 * @author kmorin - morin@codelutin.com
 */
public class ButtonHistory extends AbstractToolbarPopupButton<HistoryListUI> {

    private PropertyChangeListener listener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Collection<History> histories = (Collection<History>) evt.getNewValue();
            setText(getButtonText(histories));
        }
    };

    public ButtonHistory(DemandeUIModel model) {
        setToolTipText(t("faxtomail.historyList.action.tip"));
        init(model);
    }

    public String getButtonText(Collection<History> histories) {
        int replyNb = CollectionUtils.size(histories);
        return t("faxtomail.historyList.text", replyNb);
    }

    public void init(DemandeUIModel model) {
        if (popup.getModel() != null) {
            popup.getModel().removePropertyChangeListener(Email.PROPERTY_HISTORY, listener);
        }
        popup.setModel(model);
        if (model != null) {
            popup.getModel().addPropertyChangeListener(Email.PROPERTY_HISTORY, listener);
            setText(getButtonText(model.getHistory()));
        }
    }

    @Override
    protected String getActionIcon() {
        return "view-history";
    }

    @Override
    protected HistoryListUI createNewPopup() {
        return new HistoryListUI(FaxToMailUIContext.getApplicationContext(),
                                 SwingUtil.getParentContainer(this, JFrame.class));
    }

    @Override
    public void onCloseUI() {
        super.onCloseUI();
        if (popup.getModel() != null) {
            popup.getModel().removePropertyChangeListener(Email.PROPERTY_HISTORY, listener);
        }
    }
}
