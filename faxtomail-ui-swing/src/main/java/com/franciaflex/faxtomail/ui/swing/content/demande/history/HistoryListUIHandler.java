package com.franciaflex.faxtomail.ui.swing.content.demande.history;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.History;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.toolbar.AbstractToolbarPopupHandler;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.google.common.base.Function;
import com.google.common.collect.Ordering;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.model.JaxxDefaultListModel;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.JXList;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.nuiton.i18n.I18n;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class HistoryListUIHandler extends AbstractToolbarPopupHandler<DemandeUIModel, HistoryListUI> {

    static {
        n("faxtomail.demande.receptionDate.label");
        n("faxtomail.demande.ediError.label");
        n("faxtomail.demande.projectReference.label");
        n("faxtomail.demande.sender.label");
        n("faxtomail.demande.fax.label");
        n("faxtomail.demande.recipient.label");
        n("faxtomail.demande.subject.label");
        n("faxtomail.demande.clientCode.label");
        n("faxtomail.demande.mailFolder.label");
        n("faxtomail.demande.attachment.label");
        n("faxtomail.demande.demandStatus.label");
        n("faxtomail.demande.demandType.label");
        n("faxtomail.demande.priority.label");
        n("faxtomail.demande.waitingState.label");
        n("faxtomail.demande.takenBy.label");
        n("faxtomail.demande.rangeRow.label");
    }

    protected Action sendAction;

    @Override
    public void afterInit(HistoryListUI ui) {
        super.afterInit(ui);

        final JXList list = ui.getHistories();
        list.setCellRenderer(new HistoryListCellRenderer());
        JaxxDefaultListModel<History> listModel = new JaxxDefaultListModel<History>();
        List<History> histories = new ArrayList<History>();
        listModel.setAllElements(histories);
        list.setModel(listModel);
        list.addHighlighter(HighlighterFactory.createAlternateStriping());


        final PropertyChangeListener listener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Collection<History> newValue = (Collection<History>) evt.getNewValue();
                List<History> histories = new ArrayList<History>();
                if (newValue != null) {
                    histories.addAll(newValue);
                }
                //sort histories by modification date
                Collections.sort(histories, Ordering.natural().onResultOf(new Function<History, Comparable>() {
                    @Override
                    public Comparable apply(History history) {
                        return history.getModificationDate();
                    }
                }));
                ((JaxxDefaultListModel) getUI().getHistories().getModel()).setAllElements(histories);
            }
        };
        getUI().addPropertyChangeListener(HistoryListUI.PROPERTY_MODEL, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                DemandeUIModel oldModel = (DemandeUIModel) evt.getOldValue();
                if (oldModel != null) {
                    oldModel.removePropertyChangeListener(Email.PROPERTY_HISTORY, listener);
                }
                DemandeUIModel newModel = (DemandeUIModel) evt.getNewValue();
                if (newModel != null) {
                    newModel.addPropertyChangeListener(Email.PROPERTY_HISTORY, listener);
                    listener.propertyChange(new PropertyChangeEvent(newModel, Email.PROPERTY_HISTORY, null, newModel.getHistory()));
                }
            }
        });

        sendAction = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
//                AbstractToolbarPopupHandler.this.ui.dispose();
//                AbstractToolbarPopupHandler.this.ui.setVisible(false);
                StringBuilder historyAsString = new StringBuilder();
                for (History history : getModel().getHistory()) {
                    historyAsString.append(t("faxtomail.common.on.label") + " " + decorate(history.getModificationDate()))
                                   .append(" : " + JAXXUtil.getStringValue(history.getType().getLabel()));
                    String decoratedFields = decorateFields(history.getFields());
                    if (!decoratedFields.isEmpty()) {
                        historyAsString.append(" (" + decoratedFields + ")");
                    }
                    historyAsString.append(" : ").append(t("faxtomail.common.by.label"));
                    historyAsString.append(" ").append(decorateUser(history.getFaxToMailUser(), true));
                    historyAsString.append("\n");
                }

                StringSelection stringSelection = new StringSelection(historyAsString.toString());
                Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                clpbrd.setContents (stringSelection, null);

                FaxToMailUIUtil.email(t("faxtomail.demande.history.send.subject", getModel().getTitle()),
                                      t("faxtomail.demande.history.send.body"));

                closeEditor();
            }
        };

        ImageIcon actionIcon = SwingUtil.createActionIcon("reply");
        sendAction.putValue(Action.SMALL_ICON, actionIcon);
        sendAction.putValue(Action.LARGE_ICON_KEY, actionIcon);
        sendAction.putValue(Action.NAME, "send");
        sendAction.putValue(Action.SHORT_DESCRIPTION, t("faxtomail.demande.history.action.send.tip"));

        JButton sendButton = new JButton(sendAction);
        sendButton.setText(null);
        sendButton.setFocusPainted(false);
        sendButton.setRequestFocusEnabled(false);
        sendButton.setFocusable(false);

        ui.getBody().getRightDecoration().add(sendButton, 0);
    }

    @Override
    public void onCloseUI() {
    }

    @Override
    public SwingValidator<DemandeUIModel> getValidator() {
        return null;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    public String decorateFields(Set<String> fields) {
        List<String> fieldLabels = new ArrayList<String>();
        if (fields != null) {
            for (String field : fields) {
                String label;
                if (I18n.hasKey("faxtomail.demande." + field + ".label")) {
                    label = t("faxtomail.demande." + field + ".label");
                } else {
                    label = field;
                }
                fieldLabels.add(label);
            }
        }
        return StringUtils.join(fieldLabels, ", ");
    }

    protected class HistoryListCellRenderer extends HistoryItem
                                            implements ListCellRenderer<History> {

        public HistoryListCellRenderer() {
            super(HistoryListUIHandler.this);

            getModel().addPropertyChangeListener(HistoryItemModel.PROPERTY_FIELDS, new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (evt.getNewValue() != null) {
                        add(getFields(), 1);
                    } else {
                        remove(getFields());
                    }
                }
            });
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends History> list, History history, int index, boolean isSelected, boolean cellHasFocus) {
            getModel().fromEntity(history);
            if (CollectionUtils.isEmpty(getModel().getFields())) {
                remove(getFields());
            }
            return this;
        }
    }
}
