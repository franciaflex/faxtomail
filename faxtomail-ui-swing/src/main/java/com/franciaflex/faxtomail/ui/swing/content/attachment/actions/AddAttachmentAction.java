package com.franciaflex.faxtomail.ui.swing.content.attachment.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.AttachmentImpl;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentEditorUI;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentEditorUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentEditorUIModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class AddAttachmentAction extends AbstractFaxToMailAction<AttachmentEditorUIModel, AttachmentEditorUI, AttachmentEditorUIHandler> {

    public AddAttachmentAction(AttachmentEditorUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        if (result) {
            File file = getUI().getFile().getSelectedFile();
            if (file != null && !file.exists()) {
                getContext().getErrorHelper().showErrorDialog(t("faxtomail.action.attachment.add.fileDoesNotExist",
                                                                file.getPath()));
            }
            result = file != null && file.exists();
        }

        return result;
    }

    @Override
    public void doAction() throws Exception {
        Attachment attachment = new AttachmentImpl();
        attachment.setAddedByUser(true);

        File file = getUI().getFile().getSelectedFile();
        // warning, this not NOT be closed (will be by blob on commit)
        InputStream is = new FileInputStream(file);
        FaxToMailServiceContext serviceContext = getContext().newServiceContext();
        AttachmentFile attachmentFile = serviceContext.getEmailService().getAttachmentFileFromStream(is);
        attachmentFile.setFilename(file.getName());
        attachment.setOriginalFile(attachmentFile);
        getModel().addAttachment(attachment);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        getUI().getFile().setSelectedFilePath(null);
    }

    @Override
    public void postFailedAction(Throwable error) {
        super.postFailedAction(error);
        getContext().getErrorHelper().showErrorDialog(t("swing.error.cannot.copy.file"));
    }
}
