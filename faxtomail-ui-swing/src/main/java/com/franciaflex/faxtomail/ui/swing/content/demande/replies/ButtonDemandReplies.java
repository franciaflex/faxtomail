package com.franciaflex.faxtomail.ui.swing.content.demande.replies;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.Reply;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.toolbar.AbstractToolbarPopupButton;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;

import javax.swing.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Button to edit replies.
 *
 * @author kmorin - morin@codelutin.com
 */
public class ButtonDemandReplies extends AbstractToolbarPopupButton<DemandRepliesUI> {

    protected String textKey;

    private PropertyChangeListener listener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Collection<Reply> replies = (Collection<Reply>) evt.getNewValue();
            setText(getButtonText(replies));
        }
    };

    public ButtonDemandReplies(DemandeUIModel model) {
        this(n("faxtomail.demandReplies.text"), model);
    }

    public ButtonDemandReplies(String textKey, DemandeUIModel model) {
        this.textKey = textKey;
        setToolTipText(t("faxtomail.demandReplies.action.tip"));
        init(model);
    }

    public String getButtonText(Collection<Reply> replies) {
        int replyNb = CollectionUtils.size(replies);
        return t(textKey, replyNb);
    }

    public void init(DemandeUIModel model) {
        if (popup.getModel() != null) {
            popup.getModel().removePropertyChangeListener(Email.PROPERTY_REPLIES, listener);
        }
        popup.setModel(model);
        if (model != null) {
            popup.getModel().addPropertyChangeListener(Email.PROPERTY_REPLIES, listener);
            setText(getButtonText(model.getReplies()));
        }
    }

    @Override
    protected String getActionIcon() {
        return "reply";
    }

    @Override
    protected DemandRepliesUI createNewPopup() {
        DemandRepliesUI ui = new DemandRepliesUI(FaxToMailUIContext.getApplicationContext(),
                                                 SwingUtil.getParentContainer(this, JFrame.class));
        return ui;
    }

    public DemandeUIModel getBean() {
        return popup.getModel();
    }

    public void setForward(boolean forward) {
        popup.setForward(forward);
    }

    @Override
    public void onCloseUI() {
        super.onCloseUI();
        if (popup.getModel() != null) {
            popup.getModel().removePropertyChangeListener(Email.PROPERTY_REPLIES, listener);
        }
    }
}
