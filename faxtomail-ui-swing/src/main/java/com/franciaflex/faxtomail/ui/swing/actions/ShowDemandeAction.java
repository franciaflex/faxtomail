package com.franciaflex.faxtomail.ui.swing.actions;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.services.service.exceptions.AlreadyLockedMailException;
import com.franciaflex.faxtomail.services.service.exceptions.FolderNotReadableException;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.MainUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandesUI;
import jaxx.runtime.SwingUtil;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * To show demande screen.
 *
 * @author kmorin - morin@codelutin.com
 */
public class ShowDemandeAction extends AbstractMainUIFaxToMailAction {

    private static final Log log = LogFactory.getLog(ShowDemandeAction.class);

    // if true, take the email when opening
    protected boolean takeEmail;

    protected boolean readonly = false;

    protected JFrame frame;

    public ShowDemandeAction(MainUIHandler handler) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.goto.demand.tip"));
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        DemandeUIModel currentEmail = getContext().getCurrentEmail();

        String topiaId = currentEmail.getTopiaId();
        if (StringUtils.isNotBlank(topiaId)) {
            FaxToMailUser currentUser = getContext().getCurrentUser();

            try (FaxToMailServiceContext serviceContext = getContext().newServiceContext()){
                EmailService emailService = serviceContext.getEmailService();

                // tentative de verrouillages des email existants
                Email email = emailService.lockEmail(topiaId, currentUser);

                FaxToMailUser takenBy = email.getTakenBy();

                // si le mail est pris par quelqu'un d'autre, le prend-on ou pas ?
                if (currentEmail.isEditable() && takenBy != null && !currentUser.equals(takenBy)) {
                    MailFolder folderWithMustTakeToEditDemand = email.getMailFolder();

                    //Récupération de la configuration sur l'obligation de prendre le document pour éditer
                    while (folderWithMustTakeToEditDemand.getParent() != null
                           && folderWithMustTakeToEditDemand.getMustTakeToEditDemand() == null) {
                        folderWithMustTakeToEditDemand = folderWithMustTakeToEditDemand.getParent();
                    }
                    if (BooleanUtils.isTrue(folderWithMustTakeToEditDemand.getMustTakeToEditDemand())) {
                        String htmlMessage = String.format(
                                AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                                t("faxtomail.alert.alreadyTakenBy.message", decorate(takenBy)),
                                t("faxtomail.alert.alreadyTakenBy.help"));

                        int answer = JOptionPane.showConfirmDialog(getHandler().getTopestUI(),
                                                                   htmlMessage,
                                                                   t("faxtomail.alert.alreadyTakenBy.title"),
                                                                   JOptionPane.YES_NO_CANCEL_OPTION,
                                                                   JOptionPane.QUESTION_MESSAGE);

                        // si on annule, on délock
                        if (answer == JOptionPane.CANCEL_OPTION) {
                            result = false;
                            emailService.unlockEmail(topiaId);

                        } else {
                            // sinon on ouvre

                            // si on ne prend pas, le mail est en readonly
                            if (answer == JOptionPane.NO_OPTION) {
                                currentEmail.setEditable(false);
                                takeEmail = false;
                            }
                            // si on prend, on prend et on rend editable
                            else if (answer == JOptionPane.YES_OPTION) {
                                takeEmail = true;
                                currentEmail.setEditable(true);

                            }
                        }
                    } else {
                        takeEmail = false;
                        JOptionPane.showConfirmDialog(getHandler().getTopestUI(),
                                                      t("faxtomail.alert.alreadyTakenBy.message", decorate(takenBy)),
                                                      t("faxtomail.alert.alreadyTakenBy.title"),
                                                      JOptionPane.DEFAULT_OPTION,
                                                      JOptionPane.WARNING_MESSAGE);
                    }
                }
            }
            // le mail est locké, soit on rejette, soit on ouvre en lecture seule, en fonction de la conf du dossier
            catch (AlreadyLockedMailException ex) {

                MailFolder mailFolder = currentEmail.getMailFolder();
                while (mailFolder != null && mailFolder.getLockedDemandsOpenableInReadOnly() == null) {
                    mailFolder = mailFolder.getParent();
                }

                result = mailFolder != null && BooleanUtils.isTrue(mailFolder.getLockedDemandsOpenableInReadOnly());
                if (result) {
                    log.info("Opening demand as readonly");
                    readonly = true;
                } else {
                    String htmlMessage = t("faxtomail.alert.alreadyLockedBy.message", decorate(ex.getLockedBy()));
                    JOptionPane.showMessageDialog(getHandler().getTopestUI(),
                                                  htmlMessage,
                                                  t("faxtomail.alert.alreadyLockedBy.title"),
                                                  JOptionPane.ERROR_MESSAGE);
                }

            }
            // l'utilisateur n'a pas les droits de lecture sur le dossier
            catch (FolderNotReadableException e) {
                result = false;

                String htmlMessage = t("faxtomail.alert.userNotAuthorizedToReadEmail.message", e.getForbiddenFolder().getName());
                JOptionPane.showMessageDialog(getHandler().getTopestUI(),
                                              htmlMessage,
                                              t("faxtomail.alert.userNotAuthorizedToReadEmail.title"),
                                              JOptionPane.ERROR_MESSAGE);
            }
        }
        return result;
    }

    @Override
    public void doAction() throws Exception {

        DemandeUIModel currentEmail = getContext().getCurrentEmail();

        if (StringUtils.isNotBlank(currentEmail.getTopiaId())) {
            try (FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {

                Email email = serviceContext.getEmailService().openEmail(currentEmail.getTopiaId(),
                        getContext().getCurrentUser(),
                        takeEmail);

                MailFolder currentFolder = currentEmail.getMailFolder();
                MailFolder actualFolder = email.getMailFolder();
                if (currentFolder != null && !currentFolder.equals(actualFolder)) {
                    EventQueue.invokeLater(() -> displayWarningMessage(t("faxtomail.alert.emailMoved.title"),
                            t("faxtomail.alert.emailMoved.message",
                                    currentFolder.getName(),
                                    actualFolder.getName(),
                                    currentEmail.getTitle())));
                }

                currentEmail.fromEntity(email, true);

                MailFolder folder = currentEmail.getMailFolder();
                if (currentEmail.getArchiveDate() != null
                        || !folder.isFolderWritable()
                        || !currentEmail.getDemandStatus().isEditableStatus()) {
                    currentEmail.setEditable(false);
                }
            }

            List<Client> allowedClients = getContext().getClientCache();
            currentEmail.setAllowedClients(allowedClients);
            currentEmail.setEditable(currentEmail.isEditable() && !readonly);
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        DemandeUIModel currentEmail = getContext().getCurrentEmail();

        frame = getContext().getFrameForDemande(currentEmail);

        if (frame == null) {
            final DemandesUI screenUI = new DemandesUI(getUI());
            String screenTitle = screenUI.getModel().getCurrentDemand().getTitle();
            ImageIcon icon = SwingUtil.createActionIcon("email-group");
            screenUI.getDemandsPanel().setLeftDecoration(new JLabel(icon));

            frame = getHandler().openFrame(screenUI, screenTitle, new Dimension(800, 600));

            getContext().setFrameForDemande(currentEmail, frame);

            for (DemandeUIModel demande : currentEmail.getGroupedDemandes()) {
                getContext().setFrameForDemande(demande, frame);
            }

            frame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    DemandeUIModel currentEmail = getContext().getCurrentEmail();
                    getContext().removeDemande(currentEmail);

                    for (DemandeUIModel demande : currentEmail.getGroupedDemandes()) {
                        getContext().removeDemande(demande);
                    }
                }
            });

            getContext().addPropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (!getContext().isBusy()) {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                if (frame != null) {
                                    frame.toFront();
                                    frame.setVisible(true);
                                    frame = null;
                                }
                            }
                        });
                        getContext().removePropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, this);
                    }
                }
            });
        } else {
            frame.toFront();
            frame.setVisible(true);
        }
    }

}
