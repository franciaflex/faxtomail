package com.franciaflex.faxtomail.ui.swing.content.reply;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFileImpl;
import com.franciaflex.faxtomail.persistence.entities.AttachmentImpl;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.SigningForDomain;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.ldap.Contact;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.reply.actions.AddAttachmentToReplyAction;
import com.franciaflex.faxtomail.ui.swing.content.reply.actions.SenderChangedAction;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.google.common.collect.Lists;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.Cancelable;
import org.nuiton.validator.bean.AbstractValidator;

import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.DefaultCaret;
import javax.swing.text.JTextComponent;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class ReplyFormUIHandler extends AbstractFaxToMailUIHandler<ReplyFormUIModel, ReplyFormUI> implements Cancelable {

    private static final Log log = LogFactory.getLog(ReplyFormUIHandler.class);

    private static final String SIGNING_CLASS_PREFIX = "faxToMailSigning";

    private static final String CONTENT_SEPARATOR = "<br/><br/>----------------<br/><br/>";

    // classe à mettre sur le div de la signature pour pouvoir la remplacer
    private String signingClass;

    protected AddAttachmentToReplyAction addAttachmentToReplyAction;
    protected SenderChangedAction senderChangedAction;

    @Override
    public void beforeInit(ReplyFormUI ui) {
        super.beforeInit(ui);

        addAttachmentToReplyAction = getContext().getActionFactory().createLogicAction(this, AddAttachmentToReplyAction.class);
        senderChangedAction = getContext().getActionFactory().createLogicAction(this, SenderChangedAction.class);

        ReplyFormUIModel model = new ReplyFormUIModel();
        //TODO kmorin 20140813 action ?
        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            long maxSize = serviceContext.getConfigurationService().getEmailMaxSize();
            model.setMaxAttachmentLength(maxSize);
        } catch (IOException eee){
            log.error("Error setting maxAttachmentLength",eee);
        }

        this.ui.setContextValue(model);
    }

    @Override
    public void afterInit(final ReplyFormUI ui) {
        initUI(ui);

        //  Turn off automatic scrolling for message cf. #10746
		Component view = ui.getMessageScrollPane().getViewport().getView();
		if (view instanceof JTextComponent) {
			JTextComponent textComponent = (JTextComponent)view;
			DefaultCaret caret = (DefaultCaret)textComponent.getCaret();
			caret.setUpdatePolicy(DefaultCaret.UPDATE_WHEN_ON_EDT);
		}

        JTextPane editor = ui.getMessage();

        editor.setContentType("text/html");

        editor.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
        Font font = new Font(Font.MONOSPACED, Font.PLAIN, 16);
        editor.setFont(font);

        ReplyFormUIModel model = getModel();
        model.addPropertyChangeListener(ReplyFormUIModel.PROPERTY_ORIGINAL_DEMAND, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                DemandeUIModel demand = (DemandeUIModel) evt.getNewValue();
                ReplyFormUIModel model = (ReplyFormUIModel) evt.getSource();

                List<DemandeUIModel> models = new ArrayList<>();
                models.addAll(demand.getGroupedDemandes());

                // on crée les pj disponibles (versions éditées et non éditées)
                for (DemandeUIModel demandeUIModel : models) {
                    // remove newline character (cf #6960)
                    String prefix = StringUtils.remove(demandeUIModel.getTitle(), '\n');

                    for (Attachment attachment : demandeUIModel.getAttachment()) {

                        ReplyAttachmentModel replyOriginalAttachmentModel =
                                 new ReplyAttachmentModel(attachment, true, prefix + " - " + attachment.getOriginalFileName());
                        model.addAvailableAttachment(replyOriginalAttachmentModel);

                        if (StringUtils.isNotEmpty(attachment.getEditedFileName())) {
                            ReplyAttachmentModel replyEditedAttachmentModel =
                                    new ReplyAttachmentModel(attachment, false, prefix + " - " + attachment.getEditedFileName());

                            if (addAttchmentToReply(demand, model, demandeUIModel, attachment)) {
                                model.addAttachment(replyEditedAttachmentModel);
                                addAttachmentToReplyAction.setReplyAttachmentModel(replyEditedAttachmentModel);
                                getContext().getActionEngine().runInternalAction(addAttachmentToReplyAction);
                            } else {
                                model.addAvailableAttachment(replyEditedAttachmentModel);
                            }

                        } else if (addAttchmentToReply(demand, model, demandeUIModel, attachment)) {
                            model.addAttachment(replyOriginalAttachmentModel);
                            model.removeAvailableAttachment(replyOriginalAttachmentModel);
                            addAttachmentToReplyAction.setReplyAttachmentModel(replyOriginalAttachmentModel);
                            getContext().getActionEngine().runInternalAction(addAttachmentToReplyAction);
                        }
                    }
                }

                String subject;
                if (model.isForward()) {
                    subject = t("faxtomail.reply.forwardsubject", demand.getSubject());
                } else {
                    subject = t("faxtomail.reply.subject", demand.getSubject());
                }
                model.setSubject(subject);

                String recipient = demand.getRecipient();
                String signing;

                // this have to be disable for read only mode (useless)
                if (!model.isReadonly()) {

                    // attachment combo
                    JComboBox<AttachmentFile> addAttachmentFile = ui.getAddAttachmentFile();
                    addAttachmentFile.setModel(SwingUtil.newComboModel(model.getAvailableAttachments().toArray()));
                    addAttachmentFile.setSelectedItem(null);

                    // sender combo
                    MailFolder folder = demand.getMailFolder();
                    List<String> folderReplyAdresses = new ArrayList<>();
                    while (folder != null) {
                        folderReplyAdresses.addAll(folder.getReplyAddresses());
                        folder = folder.getParent();
                    }

                    List<String> replyToAddresses = Lists.newArrayList();
                    String selectedItem = null;
                    if (!demand.isFax() && recipient != null) {
                        replyToAddresses.add(recipient);
                        model.setFrom(recipient);
                        selectedItem = recipient;
                    }
                    replyToAddresses.addAll(folderReplyAdresses);
                    Collections.sort(replyToAddresses);

                    ui.getFromComboBox().setBeanType(String.class);
                    initBeanFilterableComboBox(ui.getFromComboBox(), replyToAddresses, selectedItem);

                    model.setSenderAllowedAddresses(replyToAddresses);

                    // contacts
                    initBeanFilterableComboBox(ui.getContactField(), getContext().getContactCache(), null);

                    // signatures
                    signing = getSigningParagraph(createSigning(model.getFrom()));

                } else {
                    model.setFrom(recipient);
                    signing = "";
                }

                String quotedText = "";
                String plainContent = StringUtils.join(demand.getPlainContent(), CONTENT_SEPARATOR);
                try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
                    String htmlContent = serviceContext.getEmailService().extractHtmlContent(demand.getAttachment(), demand.getHtmlContent());
                    if (plainContent != null && !plainContent.isEmpty()) {
                        // replace les <o:p> et </o:p> ajoutés par exchange car ça fait apparaitre des boîtes dans l'interface de réponse
                        quotedText = plainContent.replace("<o:p>","");
                        quotedText = quotedText.replace("</o:p>","");
                        // replace les < et > car ça fait apparaitre des boîtes dans l'interface de réponse
                        quotedText = quotedText.replace("<","&lt;");
                        quotedText = quotedText.replace(">","&gt;");

                        quotedText = quotedText.replaceAll("\n", "<br/> ");
                    } else {
                        quotedText = htmlContent;
                        // replace les <o:p> et </o:p> ajoutés par exchange car ça fait apparaitre des boîtes dans l'interface de réponse
                        quotedText = quotedText.replace("<o:p>","");
                        quotedText = quotedText.replace("</o:p>","");
                        //suppression des commentaires html
                        quotedText = quotedText.replaceAll("(?=<!--)([\\s\\S]*?)-->", "");
                    }
                } catch (IOException eee){
                    log.error("Error setting maxAttachmentLength",eee);
                }

                String quotedReply;
                if (model.isForward()) {
                    quotedReply = FaxToMailUIUtil.getQuotedForwardContent(signing,
                                                                          demand.getSubject(),
                                                                          decorate(demand.getReceptionDate()),
                                                                          demand.getSender(),
                                                                          demand.getRecipient(),
                                                                          demand.getCcRecipients(),
                                                                          quotedText);
                } else {
                    quotedReply = FaxToMailUIUtil.getQuotedReplyContent(signing, decorate(demand.getReceptionDate()), demand.getSender(), quotedText);
                }
                model.setMessage(quotedReply);
            }
        });

        model.addPropertyChangeListener(ReplyFormUIModel.PROPERTY_ATTACHMENT, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                updateAttachmentPanel((Collection<ReplyAttachmentModel>) evt.getNewValue());
            }
        });

        model.addPropertyChangeListener(ReplyFormUIModel.PROPERTY_READONLY, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                updateAttachmentPanel(getModel().getAttachments());
            }
        });

        model.addPropertyChangeListener(ReplyFormUIModel.PROPERTY_FROM, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                ReplyFormUIModel model = (ReplyFormUIModel) evt.getSource();
                if (!model.isReadonly()) {
                    getContext().getActionEngine().runAction(senderChangedAction);
                }
            }
        });

        JComboBox addAttachmentFile = ui.getAddAttachmentFile();
        addAttachmentFile.setEditor(new FileComboBoxEditor());

        getValidator().addPropertyChangeListener(AbstractValidator.VALID_PROPERTY, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                ReplyFormUIModel model = getModel();
                if (log.isDebugEnabled()) {
                    log.debug("Model [" + model +
                                      "] pass to valid state [" +
                                      evt.getNewValue() + "]");
                }
                model.setValid((Boolean) evt.getNewValue());
            }
        });

    }

    protected boolean addAttchmentToReply(DemandeUIModel demand, ReplyFormUIModel model, DemandeUIModel demandeUIModel, Attachment attachment) {
        return model.isForward() && demandeUIModel.equals(demand)
            && !attachment.isInlineAttachment() && !attachment.isLessImportant()
               && !attachment.isMailContent() && !attachment.isAddedByUser();
    }

    @Override
    public void cancel() {
        closeFrame();
    }

    @Override
    public void onCloseUI() {
        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            serviceContext.getEmailService().deleteTempAttachmentFiles(getModel().getOriginalDemand().getAttachment());
        } catch (IOException eee){
            log.error("Error deleting temp files",eee);
        }
    }

    @Override
    public SwingValidator<ReplyFormUIModel> getValidator() {
        return getUI().getValidator();
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getMessage();
    }

    @Override
    public Component getTopestUI() {
        return getParentContainer(Window.class);
    }

    public void removeAttachment(ReplyAttachmentModel attachment) {
        ReplyFormUIModel model = getModel();

        model.removeAttachment(attachment);

        model.addAvailableAttachment(attachment);
        ui.getAddAttachmentFile().addItem(attachment);
    }

    public void addAttachment() {
        ReplyFormUIModel model = getModel();

        JComboBox addAttachmentFile = ui.getAddAttachmentFile();
        ReplyAttachmentModel replyAttachmentModel = null;
        Object selectedItem = addAttachmentFile.getSelectedItem();

        if (ReplyAttachmentModel.class.isAssignableFrom(selectedItem.getClass())) {
            replyAttachmentModel = (ReplyAttachmentModel) selectedItem;
            addAttachmentToReplyAction.setReplyAttachmentModel(replyAttachmentModel);
            getContext().getActionEngine().runActionAndWait(addAttachmentToReplyAction);

        } else if (File.class.isAssignableFrom(selectedItem.getClass())) {
            File file = (File) selectedItem;

            AttachmentFile attachmentFile = new AttachmentFileImpl();
            String fileName = file.getName();
            attachmentFile.setFilename(fileName);

            try {
                attachmentFile.setContent(FileUtils.readFileToByteArray(file));

            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Error while converting the file " + fileName + " into a byte[]", e);
                }
            }

            Attachment attachment = new AttachmentImpl();
            attachment.setOriginalFile(attachmentFile);
            replyAttachmentModel = new ReplyAttachmentModel(attachment, true, fileName);

        }
        if (replyAttachmentModel != null) {
            model.addAttachment(replyAttachmentModel);

            model.removeAvailableAttachment(replyAttachmentModel);
            ui.getAddAttachmentFile().removeItem(replyAttachmentModel);

            addAttachmentFile.setSelectedItem(null);
        }
    }

    protected void updateAttachmentPanel(Collection<ReplyAttachmentModel> attachments) {
        JPanel attachmentsPanel = ui.getAttachmentsPanel();
        attachmentsPanel.removeAll();
        for (ReplyAttachmentModel attachment : attachments) {
            AttachmentItem item = new AttachmentItem();
            item.setHandler(this);
            item.setReplyAttachmentModel(attachment);
            attachmentsPanel.add(item);
        }
        attachmentsPanel.updateUI();
    }

    public void openLocation() {
        // use last selected file
        ReplyFormUIModel model = getModel();

        File startFile = model.getLastVisitedDirectory();
        JFileChooser fc = new JFileChooser(startFile);

//        fc.setDialogTitle(view.getTitle());

        // used to enable file selection
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int returnVal = fc.showOpenDialog(ui);
        model.setLastVisitedDirectory(fc.getCurrentDirectory());

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            // get selected to display in ui
            File file = fc.getSelectedFile();

            ui.getAddAttachmentFile().setSelectedItem(file);
        }
    }

    public void openAttachment(ReplyAttachmentModel attachment) {
//TODO kmorin 20140813 action ?
        FaxToMailUIUtil.openFile(getContext(), attachment.getAttachmentFile());

    }

    protected class FileComboBoxEditor implements ComboBoxEditor {

        protected ReplyAttachmentModel oldValue;

        protected JTextField editorComponent = new JTextField();

        @Override
        public Component getEditorComponent() {
            return editorComponent;
        }

        @Override
        public void setItem(Object anObject) {
            String text;
            ReplyAttachmentModel replyAttachmentModel = null;
            if (anObject != null) {
                if (ReplyAttachmentModel.class.isAssignableFrom(anObject.getClass())) {
                    replyAttachmentModel = (ReplyAttachmentModel) anObject;

                } else if (File.class.isAssignableFrom(anObject.getClass())) {
                    File file = (File) anObject;

                    AttachmentFile attachmentFile = new AttachmentFileImpl();
                    String fileName = file.getName();
                    attachmentFile.setFilename(fileName);

                    replyAttachmentModel = new ReplyAttachmentModel(new AttachmentImpl(), true, fileName);
                }
            }

            if (replyAttachmentModel != null)  {
                text = replyAttachmentModel.getLabel();
                oldValue = replyAttachmentModel;

            } else {
                text = "";
            }
            if (! text.equals(editorComponent.getText())) {
                editorComponent.setText(text);
            }
        }

        @Override
        public Object getItem() {
            Object newValue = editorComponent.getText();

            if (oldValue != null)  {
                // The original value is not a string. Should return the value in it's
                // original type.
                if (newValue.equals(oldValue.getLabel())) {
                    return oldValue;

                } else {
                    // Must take the value from the editor and get the value and cast it to the new type.
                    Class<?> cls = oldValue.getClass();
                    try {
                        Method method = cls.getMethod("valueOf", String.class);
                        newValue = method.invoke(oldValue, editorComponent.getText());
                    } catch (Exception ex) {
                        // Fail silently and return the newValue (a String object)
                    }
                }
            }
            return newValue;
        }

        @Override
        public void selectAll() {
            editorComponent.selectAll();
            editorComponent.requestFocus();
        }

        @Override
        public void addActionListener(ActionListener l) {
            editorComponent.addActionListener(l);
        }

        @Override
        public void removeActionListener(ActionListener l) {
            editorComponent.removeActionListener(l);
        }
    }

    /**
     * Close current dialog ui, and reopen a new one for transfering repons to new recipient.
     */
    public void forward() {
        closeFrame();

        try {
            // display a new ui with a copy of original ui model
            ReplyFormUI dialogContent = new ReplyFormUI(ui);
            ReplyFormUIModel replyModel = dialogContent.getModel();
            ReplyFormUIModel uiModel = ui.getModel();
            replyModel.fromModel(uiModel);

            String content = replyModel.getMessage();
            content = Pattern.compile("<html>.*<body>", Pattern.DOTALL).matcher(content).replaceAll("");
            content = Pattern.compile("</body>.*</html>", Pattern.DOTALL).matcher(content).replaceAll("");
            String originalReply = content.replaceAll("\n", "<br/> ");

            // clear fields that need to be filled by hand
            replyModel.setReadonly(false);
            replyModel.setForward(true);
            replyModel.setOriginalDemand(getModel().getOriginalDemand());
            replyModel.setReadSentDate(null);
            replyModel.setTo(null);
            replyModel.setCc(null);
            replyModel.setCci(null);
            replyModel.setSubject(t("faxtomail.reply.forwardsubject", replyModel.getSubject()));

            String quotedReply = FaxToMailUIUtil.getQuotedForwardContent(dialogContent.getHandler().createSigning(replyModel.getFrom()),
                                                                         uiModel.getSubject(),
                                                                         decorate(uiModel.getReadSentDate()),
                                                                         uiModel.getFrom(),
                                                                         uiModel.getTo(),
                                                                         uiModel.getCc(),
                                                                         originalReply);
            replyModel.setMessage(quotedReply);

            openModalFrame(dialogContent, replyModel.getSubject(), new Dimension(800, 600));

        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't display forward frame", ex);
            }
            getContext().getErrorHelper().showErrorDialog(t("faxtomail.demandReplies.error"), ex);
        }

    }

    public void addTo() {
        ReplyFormUIModel model = getModel();
        Contact contact = model.getContact();
        if (contact != null) {
            String to = model.getTo();
            String email = contact.getEmail();
            if (StringUtils.isNotBlank(to)) {
                to += ";" + email;
            } else {
                to = email;
            }
            model.setTo(to);
        }
    }

    public void addCc() {
        ReplyFormUIModel model = getModel();
        Contact contact = model.getContact();
        if (contact != null) {
            String cc = model.getCc();
            String email = contact.getEmail();
            if (StringUtils.isNotBlank(cc)) {
                cc += ";" + email;
            } else {
                cc = email;
            }
            model.setCc(cc);
        }
    }

    public void addCci() {
        ReplyFormUIModel model = getModel();
        Contact contact = model.getContact();
        if (contact != null) {
            String cci = model.getCci();
            String email = contact.getEmail();
            if (StringUtils.isNotBlank(cci)) {
                cci += ";" + email;
            } else {
                cci = email;
            }
            model.setCci(cci);
        }
    }

    public void addReferences() {
        ReplyFormUIModel model = getModel();
        String message = model.getMessage();
        DemandeUIModel demand = model.getOriginalDemand();
        if (message != null && demand != null) {
            message = createReferences(message, demand);
            model.setMessage(message);
        }
    }

    protected String createReferences(String message, DemandeUIModel demande) {
        String refs = "";
        String projectReference = demande.getProjectReference();
        String companyReference = demande.getCompanyReference();
        String commandNumber = "";
        if (CollectionUtils.isNotEmpty(demande.getValidRangeRowModels())) {
            commandNumber = demande.getValidRangeRowModels().get(0).getCommandNumber();
        }

        if (StringUtils.isNotBlank(projectReference)) {
            refs += t("faxtomail.reply.projectReference", projectReference);
        }
        if (StringUtils.isNotBlank(companyReference) || StringUtils.isNotBlank(commandNumber)) {
            if (StringUtils.isBlank(companyReference)) {
                companyReference = commandNumber;
            } else if (StringUtils.isNotBlank(commandNumber)) {
                companyReference += " " + commandNumber;
            }
            refs += t("faxtomail.reply.companyReference", companyReference);
        }
        if (StringUtils.isNotBlank(refs)) {
            refs += "<br>";
        }

        //insert refs après le premier body
        StringBuilder returnMessage;
        if (message.contains("<body>")) {
            String[] parts = message.split("(?<=<body>)");
                returnMessage = new StringBuilder(parts[0] + "<p>" + refs + "</p>" + parts[1]);
            if (parts.length>2){
                for (int i=2;i<parts.length;i++){
                    returnMessage.append(parts[i]);
                }
            }

        } else {
            returnMessage = new StringBuilder(refs + message);
        }

        return returnMessage.toString();
    }

    public void replaceSigning() {
        ReplyFormUIModel model = getModel();
        String signing = createSigningContent(model.getFrom());
        String message = model.getMessage();
        if (message != null) {
            message = Pattern.compile(getSigningParagraph(".*?"), Pattern.DOTALL).matcher(message).replaceFirst(getSigningParagraph(signing));
            model.setMessage(message);
        }
    }

    public String createSigning(String from) {
        signingClass = SIGNING_CLASS_PREFIX + new Date().getTime();
        String signing = createSigningContent(from);
        return getSigningParagraph(signing);
    }

    protected String createSigningContent(String from) {
        FaxToMailUser currentUser = getContext().getCurrentUser();
        String signing = currentUser.getFirstName() + " " + currentUser.getLastName() + "<br/>";
        if (from != null) {
            signing += from + "<br/>";
        }

        SigningForDomain signingForDomain = getModel().getSigning();
        if (signingForDomain != null) {
            if (signingForDomain.getImage() != null) {
                signing += "<img src=\"" + signingForDomain.getImage() + "\"/>";
            } else {
                signing += signingForDomain.getText().replaceAll("\\n", "<br/>");

                if (log.isInfoEnabled()) {
                    log.info("signing " + signing);
                }
            }
        }
        return signing;
    }

    protected String getSigningParagraph(String signing) {
        return "<div class=\"" + signingClass + "\">" + signing + "</div>";
    }
}
