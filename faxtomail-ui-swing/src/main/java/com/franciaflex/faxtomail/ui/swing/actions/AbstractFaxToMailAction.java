package com.franciaflex.faxtomail.ui.swing.actions;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUI;
import org.jdesktop.beans.AbstractBean;
import org.nuiton.jaxx.application.swing.action.AbstractApplicationAction;
import org.nuiton.jaxx.application.type.ApplicationProgressionModel;

import javax.swing.JFrame;

/**
 * FaxToMail base action.
 *
 * @author kmorin - morin@codelutin.com
 */
public abstract class AbstractFaxToMailAction <M extends AbstractBean, UI extends FaxToMailUI<M, ?>, H extends AbstractFaxToMailUIHandler<M, UI>>
        extends AbstractApplicationAction<M, UI, H> {

    public abstract void doAction() throws Exception;

    protected AbstractFaxToMailAction(H handler, boolean hideBody) {
        super(handler, hideBody);
    }

    @Override
    public FaxToMailUIContext getContext() {
        return handler.getContext();
    }

    @Override
    protected ApplicationProgressionModel getProgressionModel() {
        return getContext().getActionUI().getModel().getProgressionModel();
    }

    @Override
    protected FaxToMailConfiguration getConfig() {
        return getContext().getConfig();
    }

    @Override
    protected void sendMessage(String message) {
        getContext().showInformationMessage(message);
    }

    @Override
    protected void createProgressionModelIfRequired(int total) {
        ApplicationProgressionModel progressionModel = getProgressionModel();
        if (progressionModel == null) {
            progressionModel = new ApplicationProgressionModel();
            progressionModel.setTotal(total);
            progressionModel.setMessage("");
            progressionModel.setCurrent(0);
            setProgressionModel(progressionModel);
        } else {
            progressionModel.adaptTotal(total);
        }
    }

    /**
     * On met la fenetre de la demande courante en avant (en général, c'est pour pouvoir remettre une autre fenetre devant...)
     */
    protected void bringCurrentDemandToFront() {
        JFrame frameForDemande = getContext().getFrameForDemande(getContext().getCurrentEmail());
        if (frameForDemande != null) {
            frameForDemande.toFront();
        }
    }
}
