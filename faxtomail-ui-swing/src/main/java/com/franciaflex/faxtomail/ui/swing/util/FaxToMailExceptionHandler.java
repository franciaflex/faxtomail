package com.franciaflex.faxtomail.ui.swing.util;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.action.ApplicationActionException;
import org.nuiton.jaxx.application.swing.util.ApplicationErrorHelper;
import org.nuiton.jaxx.application.swing.util.ApplicationExceptionHandler;

/**
 * FaxToMail global exception handler.
 * 
 * Catch all application uncaught and display it in a custom JoptionPane
 * or JXErrorPane.
 * 
 * See http://stackoverflow.com/a/4448569/1165234 for details.
 *
 * @author kmorin - morin@codelutin.com
 */
public class FaxToMailExceptionHandler extends ApplicationExceptionHandler {

    private static final Log log =
            LogFactory.getLog(FaxToMailExceptionHandler.class);

    public FaxToMailExceptionHandler(ApplicationErrorHelper errorHelper) {
        super(errorHelper);
    }

    protected void handleException(String tname, Throwable ex) {

        Throwable cause = getCause(ex);
        if (log.isErrorEnabled()) {
            log.error("Global application exception [" + tname + "] : " + (cause != null ? cause.getMessage() : ""), ex);
        }

//        boolean backToScreen = false;
//
//        AbstractFaxToMailAction action = null;
//
        if (cause instanceof ApplicationActionException) {

//            ApplicationActionException actionException = (ApplicationActionException) cause;
            cause = cause.getCause();

            if (log.isDebugEnabled()) {
                log.debug("Action error cause:", cause);
            }

//            action = (AbstractFaxToMailAction) actionException.getAction();
//
//            if (action instanceof AbstractChangeScreenAction) {
//                backToScreen = true;
//            }
        }

        // FIXME 20141218 kmorin
        // TODO remove this when we use the ng actions
        // catch ClassCastException to avoid errors like these :
        // java.lang.ClassCastException: javax.swing.plaf.FontUIResource cannot be cast to java.awt.Color
        if (cause instanceof ClassCastException) {
            if (log.isErrorEnabled()) {
                log.error("ClassCastException caught", cause);
            }
            return;
        }

        showErrorDialog(cause != null ? cause.getMessage() : "", cause);

//        if (backToScreen) {
//
////            action.getContext().setFallBackScreen();
//        }
    }

}
