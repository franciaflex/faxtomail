<!--
  #%L
  FaxToMail :: UI
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JPanel id="pdfEditorUIPanel"
        layout='{new BorderLayout()}'
        implements='com.franciaflex.faxtomail.ui.swing.util.FaxToMailUI&lt;PDFEditorUIModel, PDFEditorUIHandler&gt;'>

  <import>
    com.franciaflex.faxtomail.ui.swing.util.FaxToMailUI
    com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil

    com.franciaflex.faxtomail.ui.swing.util.JImagePanel

    javax.swing.SwingConstants

    jaxx.runtime.swing.editor.NumberEditor

  </import>

  <script><![CDATA[

    public PDFEditorUI(FaxToMailUI parentUI) {
        FaxToMailUIUtil.setParentUI(this, parentUI);
    }

  ]]></script>

  <PDFEditorUIModel id='model' initializer='getContextValue(PDFEditorUIModel.class)'/>

  <JToolBar id="toolbar"
            constraints='BorderLayout.NORTH'>

    <JToggleButton id='noteButton' buttonGroup="actionGroup" value="NOTE"/>
                   <!--onActionPerformed="handler.addNote()"/>-->
    <JToggleButton id='crossButton' buttonGroup="actionGroup" value="CROSS"/>
                   <!--onActionPerformed="handler.addCross()"/>-->
    <JToggleButton id='hLineButton' buttonGroup="actionGroup" value="HLINE"/>
    <JToggleButton id='vLineButton' buttonGroup="actionGroup" value="VLINE"/>
                   <!--onActionPerformed="handler.addLine()"/>-->
    <JToggleButton id='highlighterButton' buttonGroup="actionGroup" value="HIGHLIGHTER"/>
                    <!--onActionPerformed="handler.addHighlighter()"/>-->

    <JSeparator id="actionGroupSeparator" constructorParams="SwingConstants.VERTICAL"/>

    <JButton id="prevPageButton"
             onActionPerformed="getModel().decPageIndex()"/>
    <NumberEditor id='pageNumber'
                  constructorParams='this' />
    <JLabel id="pageTotal"/>
    <JButton id="nextPageButton"
             onActionPerformed="getModel().incPageIndex()"/>

    <JButton id="zoomOutButton"
             onActionPerformed="handler.zoomOut()"/>
    <JLabel id="zoomLabel"/>
    <JButton id="zoomInButton"
             onActionPerformed="handler.zoomIn()"/>

    <JButton id="rotateClockwiseButton"
             onActionPerformed="handler.rotateClockwise()"/>
    <JButton id="rotateAntiClockwiseButton"
             onActionPerformed="handler.rotateAntiClockwise()"/>

    <JSeparator constructorParams="SwingConstants.VERTICAL"/>

    <JButton id="printButton"
             onActionPerformed="handler.print()"/>

  </JToolBar>

  <JScrollPane id="documentScrollPane" constraints='BorderLayout.CENTER'>
    <JPanel>
      <JPanel id="container" layout="{null}">
        <JImagePanel id="documentPanel" onMouseClicked="handler.addEditionComponent(event)">
        </JImagePanel>
      </JPanel>
    </JPanel>
  </JScrollPane>

  <JPanel layout='{new GridLayout(1, 0)}' constraints='BorderLayout.SOUTH'>
    <JButton id='cancelButton' onActionPerformed='handler.cancel()'/>
    <JButton id='validateButton'/>
  </JPanel>
</JPanel>
