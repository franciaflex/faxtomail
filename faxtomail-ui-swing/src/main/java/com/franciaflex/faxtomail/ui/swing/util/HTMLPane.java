package com.franciaflex.faxtomail.ui.swing.util;

/*-
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.html.HTMLAnchorElement;

public class HTMLPane extends JFXPanel {

    private static final Log log = LogFactory.getLog(HTMLPane.class);

    private static WebEngine engine;

    public void init(int height) {
        //This is needed for refreshes (if not present, JavaFX closes itself and nothing is displayed anymore)
        Platform.setImplicitExit(false);

        Platform.runLater(() -> {
            WebView browser = new WebView();
            browser.setMinHeight(200);
            browser.setPrefHeight(height);
            engine = browser.getEngine();
            engine.getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>()
            {

                public void changed(ObservableValue<? extends Worker.State> o, Worker.State old, final Worker.State state)
                {
                    if (state == Worker.State.SUCCEEDED)
                    {
                        //Manage opening links in external browser and prevent opening them in WebView
                        Document document = engine.getDocument();
                        if (document != null) {
                            NodeList nodeList = document.getElementsByTagName("a");
                            for (int i = 0; i < nodeList.getLength(); i++) {
                                Node node = nodeList.item(i);
                                EventTarget eventTarget = (EventTarget) node;
                                eventTarget.addEventListener("click", evt -> {
                                    EventTarget target = evt.getCurrentTarget();
                                    HTMLAnchorElement anchorElement = (HTMLAnchorElement) target;
                                    String href = anchorElement.getHref();
                                    //handle opening URL outside JavaFX WebView
                                    FaxToMailUIUtil.openUrl(href);
                                    evt.preventDefault();
                                }, false);
                            }
                        }
                    }
                }
            });
            Scene scene = new Scene(browser);
            this.setScene(scene);
        });
    }

    public void loadContent(final String content) {
        Platform.runLater(() -> engine.loadContent(content));
    }

}
