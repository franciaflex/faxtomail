package com.franciaflex.faxtomail.ui.swing.actions;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.FaxToMailScreen;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.MainUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.search.SearchUIModel;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * To show demande screen.
 *
 * @author kmorin - morin@codelutin.com
 */
public class ShowRechercheAction extends AbstractChangeScreenAction {

    public ShowRechercheAction(MainUIHandler handler) {
        super(handler, true, FaxToMailScreen.SEARCH);
        setActionDescription(t("faxtomail.action.goto.search.tip"));
    }

    @Override
    public void doAction() throws Exception {
        FaxToMailUIContext context = getContext();
        context.setCurrentPaginationParameter(null);

        FaxToMailUser currentUser = context.getCurrentUser();
        try(FaxToMailServiceContext faxToMailServiceContext = context.newServiceContext()){
            List<Client> allowedClients = faxToMailServiceContext.getClientService().getAllClientsForUser(currentUser);
            getUI().setContextValue(allowedClients, SearchUIModel.PROPERTY_ALLOWED_CLIENTS);
        }

        super.doAction();
    }

}
