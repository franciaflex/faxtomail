package com.franciaflex.faxtomail.ui.swing.content.demande;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.EmailFilter;
import com.franciaflex.faxtomail.persistence.entities.EmailTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kmorin - kmorin@codelutin.com
 *
 */
public class DemandeListUIModel extends AbstractSerializableBean {

    public static final String PROPERTY_FOLDERS = "folders";
    public static final String PROPERTY_EMAILS = "emails";
    public static final String PROPERTY_PAGINATION_PARAMETER = "paginationParameter";
    public static final String PROPERTY_PAGINATION_RESULT = "paginationResult";
    public static final String PROPERTY_SELECTED_EMAILS = "selectedEmails";
    public static final String PROPERTY_EMAIL_NB = "emailNb";
    public static final String PROPERTY_SELECTED_FOLDER = "selectedFolder";
    public static final String PROPERTY_QUOTATION_NB = "quotationNb";
    public static final String PROPERTY_PF_NB = "pfNb";
    public static final String PROPERTY_SAV_NB = "savNb";
    public static final String PROPERTY_REPLY_ACTIVATED = "replyActivated";
    public static final String PROPERTY_REPLY_ENABLED = "replyEnabled";
    public static final String PROPERTY_FORWARD_ACTIVATED = "forwardActivated";
    public static final String PROPERTY_FORWARD_ENABLED = "forwardEnabled";
    public static final String PROPERTY_TAKENBY_ENABLED = "takenByEnabled";
    public static final String PROPERTY_TAKENBY_ACTIVATED = "takenByActivated";
    public static final String PROPERTY_ARCHIVE_ENABLED = "archiveEnabled";
    public static final String PROPERTY_TRANSMIT_ENABLED = "transmitEnabled";
    public static final String PROPERTY_PRINT_ENABLED = "printEnabled";
    public static final String PROPERTY_COMPUTE_QUANTITIES_BY_RANGE_ENABLED = "computeQuantitiesByRangeEnabled";
    public static final String PROPERTY_NEW_DEMAND_ENABLED = "newDemandEnabled";
    public static final String PROPERTY_RESULT_PER_PAGE = "resultPerPage";
    public static final String PROPERTY_ORDER_BY_MAILFIELD = "orderByMailField";
    public static final String PROPERTY_ORDER_DESC = "orderDesc";
    public static final String PROPERTY_ENABLE_CHANGE_RESULT_PER_PAGE = "enableChangeResultPerPage";
    public static final String PROPERTY_CAN_SORT_RESULTS = "canSortResults";

    protected List<MailFolder> folders;

    protected List<DemandeUIModel> emails;

    protected EmailFilter emailFilter = new EmailFilter();

    protected int resultPerPage = 50;

    protected MailField orderByMailField = MailField.RECEPTION_DATE;

    protected boolean orderDesc = false;

    protected boolean enableChangeResultPerPage = true;

    protected boolean canSortResults = false;

    protected PaginationParameter paginationParameter = PaginationParameter.of(0, resultPerPage, TopiaEntity.PROPERTY_TOPIA_CREATE_DATE, false);

    protected PaginationResult<Email> paginationResult = PaginationResult.of(null, 0, paginationParameter);

    /** Currently selected email in table. */
    protected List<DemandeUIModel> selectedEmails;

    protected MailFolder selectedFolder;

    protected boolean replyActivated;

    protected boolean replyEnabled;

    protected boolean forwardActivated;

    protected boolean forwardEnabled;

    protected boolean takenByEnabled;

    protected boolean takenByActivated;

    protected boolean archiveEnabled;

    protected boolean transmitEnabled;

    protected boolean printEnabled;

    protected boolean computeQuantitiesByRangeEnabled;

    protected boolean newDemandEnabled;

    protected int quotationNb;

    protected int pfNb;

    protected int savNb;

    protected Boolean displayOnlyUserTrigraphInTables;

    public List<MailFolder> getFolders() {
        return folders;
    }

    public void setFolders(List<MailFolder> folders) {
        Object oldValue = getFolders();
        this.folders = folders;
        firePropertyChange(PROPERTY_FOLDERS, oldValue, folders);
    }

    public List<DemandeUIModel> getEmails() {
        return emails;
    }

    public void setEmails(List<DemandeUIModel> emails) {
        this.emails = emails;
        firePropertyChange(PROPERTY_EMAILS, null, emails);
    }

    public EmailFilter getEmailFilter() {
        return emailFilter;
    }

    public void setEmailFilter(EmailFilter emailFilter) {
        this.emailFilter = emailFilter;
    }

    public void setPaginationParameter(PaginationParameter paginationParameter) {
        this.paginationParameter = paginationParameter;
        firePropertyChange(PROPERTY_PAGINATION_PARAMETER, null, paginationParameter);
    }

    public PaginationParameter getPaginationParameter() {
        return paginationParameter;
    }

    public void resetPaginationParameterPage() {
        resetOrderPaginationParameterForPage(0);
    }

    public void resetOrderPaginationParameter() {
        resetOrderPaginationParameterForPage(paginationParameter.getPageNumber());
    }

    protected void resetOrderPaginationParameterForPage(int page) {

        PaginationParameter.PaginationParameterBuilder builder = PaginationParameter.builder(page, resultPerPage);
        builder.addOrder(orderByMailField.getOrderProperty(), orderDesc);

        if (MailField.RECEPTION_DATE != orderByMailField) {

            builder.addOrder(MailField.RECEPTION_DATE.getOrderProperty());
        }
        builder.addOrder(EmailTopiaDao.EMAIL_IDENTIFIER + "." + TopiaEntity.PROPERTY_TOPIA_ID);

        setPaginationParameter(builder.build());
    }

    public void setPaginationResult(PaginationResult<Email> paginationResult) {
        PaginationResult<Email> oldValue = this.paginationResult;
        this.paginationResult = paginationResult;
        firePropertyChange(PROPERTY_PAGINATION_RESULT, oldValue, paginationResult);
    }

    public PaginationResult<Email> getPaginationResult() {
        return paginationResult;
    }

    public List<DemandeUIModel> getSelectedEmails() {
        return selectedEmails;
    }

    public void setSelectedEmails(List<DemandeUIModel> selectedEmails) {
        Object oldValue = getSelectedEmails();
        this.selectedEmails = selectedEmails;
        firePropertyChange(PROPERTY_SELECTED_EMAILS, oldValue, selectedEmails);
    }

    public int getEmailNb() {
        return emails == null ? 0 : emails.size();
    }

    public void addEmail(DemandeUIModel email) {
        if (emails == null) {
            emails = new ArrayList<DemandeUIModel>();
        }
        Object oldValue = getEmailNb();
        emails.add(email);
        Object newValue = getEmailNb();
        firePropertyChange(PROPERTY_EMAILS, null, emails);
        firePropertyChange(PROPERTY_EMAIL_NB, oldValue, newValue);
    }

    public void removeEmails(List<DemandeUIModel> emails) {
        if (emails != null) {
            Object oldValue = getEmailNb();
            this.emails.removeAll(emails);
            Object newValue = getEmailNb();
            firePropertyChange(PROPERTY_EMAILS, null, this.emails);
            firePropertyChange(PROPERTY_EMAIL_NB, oldValue, newValue);
        }
    }

    public MailFolder getSelectedFolder() {
        return selectedFolder;
    }

    public void setSelectedFolder(MailFolder selectedFolder) {
        Object oldValue = getSelectedFolder();
        this.selectedFolder = selectedFolder;

        if (selectedFolder != null) {
            MailFolder folderWithDisplayOnlyUserTrigraphInTables = selectedFolder;
            while (folderWithDisplayOnlyUserTrigraphInTables.getParent() != null
                   && folderWithDisplayOnlyUserTrigraphInTables.getDisplayOnlyUserTrigraphInTables() == null) {
                folderWithDisplayOnlyUserTrigraphInTables = folderWithDisplayOnlyUserTrigraphInTables.getParent();
            }
            displayOnlyUserTrigraphInTables = folderWithDisplayOnlyUserTrigraphInTables.getDisplayOnlyUserTrigraphInTables();
        }

        firePropertyChange(PROPERTY_SELECTED_FOLDER, oldValue, selectedFolder);
    }

    public boolean isReplyActivated() {
        return replyActivated;
    }

    public void setReplyActivated(boolean replyActivated) {
        Object oldValue = isReplyActivated();
        this.replyActivated = replyActivated;
        firePropertyChange(PROPERTY_REPLY_ACTIVATED, oldValue, replyActivated);
    }

    public boolean isReplyEnabled() {
        return replyEnabled;
    }

    public void setReplyEnabled(boolean replyEnabled) {
        Object oldValue = isReplyEnabled();
        this.replyEnabled = replyEnabled;
        firePropertyChange(PROPERTY_REPLY_ENABLED, oldValue, replyEnabled);
    }

    public boolean isForwardActivated() {
        return forwardActivated;
    }

    public void setForwardActivated(boolean forwardActivated) {
        Object oldValue = isForwardActivated();
        this.forwardActivated = forwardActivated;
        firePropertyChange(PROPERTY_FORWARD_ACTIVATED, oldValue, forwardActivated);
    }

    public boolean isForwardEnabled() {
        return forwardEnabled;
    }

    public void setForwardEnabled(boolean forwardEnabled) {
        Object oldValue = isForwardEnabled();
        this.forwardEnabled = forwardEnabled;
        firePropertyChange(PROPERTY_FORWARD_ENABLED, oldValue, forwardEnabled);
    }

    public boolean isTakenByActivated() {
        return takenByActivated;
    }

    public void setTakenByActivated(boolean takenByActivated) {
        Object oldValue = isTakenByActivated();
        this.takenByActivated = takenByActivated;
        firePropertyChange(PROPERTY_TAKENBY_ACTIVATED, oldValue, takenByActivated);
    }

    public boolean isTakenByEnabled() {
        return takenByEnabled;
    }

    public void setTakenByEnabled(boolean takenByEnabled) {
        Object oldValue = isTakenByEnabled();
        this.takenByEnabled = takenByEnabled;
        firePropertyChange(PROPERTY_TAKENBY_ENABLED, oldValue, takenByEnabled);
    }

    public boolean isArchiveEnabled() {
        return archiveEnabled;
    }

    public void setArchiveEnabled(boolean archiveEnabled) {
        Object oldValue = isArchiveEnabled();
        this.archiveEnabled = archiveEnabled;
        firePropertyChange(PROPERTY_ARCHIVE_ENABLED, oldValue, archiveEnabled);
    }
    
    public boolean isTransmitEnabled() {
        return transmitEnabled;
    }

    public void setPrintEnabled(boolean printEnabled) {
        Object oldValue = this.printEnabled;
        this.printEnabled = printEnabled;
        firePropertyChange(PROPERTY_PRINT_ENABLED, oldValue, printEnabled);
    }

    public boolean isPrintEnabled() {
        return printEnabled;
    }

    public void setTransmitEnabled(boolean transmitEnabled) {
        Object oldValue = this.transmitEnabled;
        this.transmitEnabled = transmitEnabled;
        firePropertyChange(PROPERTY_TRANSMIT_ENABLED, oldValue, transmitEnabled);
    }

    public boolean isComputeQuantitiesByRangeEnabled() {
        return computeQuantitiesByRangeEnabled;
    }

    public void setComputeQuantitiesByRangeEnabled(boolean computeQuantitiesByRangeEnabled) {
        Object oldValue = isComputeQuantitiesByRangeEnabled();
        this.computeQuantitiesByRangeEnabled = computeQuantitiesByRangeEnabled;
        firePropertyChange(PROPERTY_COMPUTE_QUANTITIES_BY_RANGE_ENABLED, oldValue, computeQuantitiesByRangeEnabled);
    }

    public boolean isNewDemandEnabled() {
        return newDemandEnabled;
    }

    public void setNewDemandEnabled(boolean newDemandEnabled) {
        Object oldValue = isNewDemandEnabled();
        this.newDemandEnabled = newDemandEnabled;
        firePropertyChange(PROPERTY_NEW_DEMAND_ENABLED, oldValue, newDemandEnabled);
    }

    public int getQuotationNb() {
        return quotationNb;
    }

    public void setQuotationNb(int quotationNb) {
        Object oldValue = getPfNb();
        this.quotationNb = quotationNb;
        firePropertyChange(PROPERTY_QUOTATION_NB, oldValue, quotationNb);
    }

    public int getPfNb() {
        return pfNb;
    }

    public void setPfNb(int pfNb) {
        Object oldValue = getPfNb();
        this.pfNb = pfNb;
        firePropertyChange(PROPERTY_PF_NB, oldValue, pfNb);
    }

    public int getSavNb() {
        return savNb;
    }

    public void setSavNb(int savNb) {
        Object oldValue = getSavNb();
        this.savNb = savNb;
        firePropertyChange(PROPERTY_SAV_NB, oldValue, savNb);
    }
    
    public void setResultPerPage(int resultPerPage) {
        int oldValue = this.resultPerPage;
        this.resultPerPage = resultPerPage;
        firePropertyChange(PROPERTY_RESULT_PER_PAGE, oldValue, resultPerPage);
    }

    public int getResultPerPage() {
        return resultPerPage;
    }

    public MailField getOrderByMailField() {
        return orderByMailField;
    }

    public void setOrderByMailField(MailField orderByMailField) {
        Object oldValue = getOrderByMailField();
        this.orderByMailField = orderByMailField;
        firePropertyChange(PROPERTY_ORDER_BY_MAILFIELD, oldValue, orderByMailField);
    }

    public boolean isOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(boolean orderDesc) {
        Object oldValue = isOrderDesc();
        this.orderDesc = orderDesc;
        firePropertyChange(PROPERTY_ORDER_DESC, oldValue, orderDesc);
    }

    public boolean isEnableChangeResultPerPage() {
        return enableChangeResultPerPage;
    }

    public void setEnableChangeResultPerPage(boolean enableChangeResultPerPage) {
        Object oldValue = isEnableChangeResultPerPage();
        this.enableChangeResultPerPage = enableChangeResultPerPage;
        firePropertyChange(PROPERTY_ENABLE_CHANGE_RESULT_PER_PAGE, oldValue, enableChangeResultPerPage);
    }

    public boolean isDisplayOnlyUserTrigraphInTables() {
        return Boolean.TRUE.equals(displayOnlyUserTrigraphInTables);
    }
    
    public boolean isCanSortResults() {
        return canSortResults;
    }

    public void setCanSortResults(boolean canSortResults) {
        this.canSortResults = canSortResults;
        firePropertyChange(PROPERTY_CAN_SORT_RESULTS, null, canSortResults);
    }
}
