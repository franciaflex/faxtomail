package com.franciaflex.faxtomail.ui.swing;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.ui.swing.content.MainUI;
import com.franciaflex.faxtomail.ui.swing.content.MainUIHandler;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailExceptionHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.widgets.extra.SplashScreen;

import javax.net.ssl.SSLContext;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.BorderUIResource;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * @author kmorin - kmorin@codelutin.com
 *
 */
public class RunFaxToMail {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RunFaxToMail.class);

    public static final int NORMAL_EXIT_CODE = 0;

    public static final int UPATE_EXIT_CODE = 88;

    public static void main(String... args) {

        if (log.isInfoEnabled()) {
            log.info("Starting FaxToMail with arguments: " + Arrays.toString(args));
        }

        // Create configuration
        FaxToMailConfiguration config =
                new FaxToMailConfiguration("faxToMail.properties", args);

        try {
            String protocols = String.join(" ",
                    SSLContext
                            .getDefault()
                            .getSupportedSSLParameters()
                            .getProtocols()
            );

            log.debug("Set tls version to " + protocols);
            System.setProperty("mail.smtp.ssl.protocols", protocols);
        } catch (NoSuchAlgorithmException e) {
            log.error("Could not set tls version. Force to v1.2");
            System.setProperty("mail.smtp.ssl.protocols", "TLSv1.2");
        }

        Font font;
        try {
            InputStream fontStream = RunFaxToMail.class.getResourceAsStream("/PoetsenOne-Regular.ttf");
            font = Font.createFont(Font.TRUETYPE_FONT, fontStream).deriveFont(30f);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("error while loading font, use the default one", e);
            }
            font = new Font("Verdana", Font.BOLD, 20);
        }
        SplashScreen.Title title = new SplashScreen.Title("FaxToMail " + config.getVersion(),
                                                          font,
                                                          new Point(20, 40),
                                                          new Color(200, 20, 40));
        final SplashScreen splashScreen = new SplashScreen("/splashscreen.png", title);
        splashScreen.setRemanence(500);

        // Create application context
        try {
            // Create application context
            FaxToMailUIContext context = FaxToMailUIContext.newContext(config);

            // override default exception management (after config init)
            Thread.setDefaultUncaughtExceptionHandler(new FaxToMailExceptionHandler(context.getErrorHelper()));

            // prepare context (mainly init configs, i18n)
            context.init();

            // Prepare ui look&feel and load ui properties
            try {
                SwingUtil.initNimbusLoookAndFeel();
            } catch (Exception e) {
                // could not find nimbus look-and-feel
                if (log.isWarnEnabled()) {
                    log.warn("Failed to init nimbus look and feel", e);
                }
            }

            if (config.isFullLaunchMode()) {
                if (log.isInfoEnabled()) {
                    log.info("Full launch mode, try to update.");
                }
            }

            if (log.isInfoEnabled()) {
                log.info("Will start FaxToMail...");
            }
            startFaxToMail(context, true);

        } finally {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    splashScreen.complete();
                }
            });
        }
    }

    public static void startFaxToMail(final FaxToMailUIContext context, boolean openContext) {

        if (openContext) {
            context.open();
        }

        UIManager.put("Table.focusCellHighlightBorder", new BorderUIResource.LineBorderUIResource(Color.BLACK));

        final MainUI mainUI = new MainUI(context);
        context.addMessageNotifier(mainUI.getHandler());

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                mainUI.setVisible(true);
            }
        });
    }

    public static void closeFaxToMail(MainUIHandler handler, Integer exitCode) {

        FaxToMailUIContext context = handler.getContext();

        // close ui
        handler.onCloseUI();

        //close context
        try {
            context.getSwingSession().save();

        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("error while saving the swing session", e);
            }
        }
        context.close();

        if (exitCode != null) {
            System.exit(exitCode);
        }
    }

}
