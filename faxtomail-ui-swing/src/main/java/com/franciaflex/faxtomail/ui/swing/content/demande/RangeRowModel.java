package com.franciaflex.faxtomail.ui.swing.content.demande;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.persistence.entities.RangeRow;
import com.franciaflex.faxtomail.persistence.entities.RangeRowImpl;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class RangeRowModel extends AbstractFaxToMailBeanUIModel<RangeRow, RangeRowModel> {

    protected final RangeRow editObject = new RangeRowImpl();

    protected static Binder<RangeRowModel, RangeRow> toBeanBinder =
            BinderFactory.newBinder(RangeRowModel.class,
                                    RangeRow.class);

    protected static Binder<RangeRow, RangeRowModel> fromBeanBinder =
            BinderFactory.newBinder(RangeRow.class, RangeRowModel.class);


    public RangeRowModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    public void setCommandNumber(String commandNumber) {
        Object oldValue = getCommandNumber();
        editObject.setCommandNumber(commandNumber);
        firePropertyChange(RangeRow.PROPERTY_COMMAND_NUMBER, oldValue, commandNumber);
    }

    public String getCommandNumber() {
        return editObject.getCommandNumber();
    }

    public void setQuotationQuantity(Integer quotationQuantity) {
        if (quotationQuantity == null) {
            quotationQuantity = 0;
        }
        Object oldValue = getQuotationQuantity();
        editObject.setQuotationQuantity(quotationQuantity);
        firePropertyChange(RangeRow.PROPERTY_QUOTATION_QUANTITY, oldValue, quotationQuantity);
    }

    public Integer getQuotationQuantity() {
        return editObject.getQuotationQuantity();
    }

    public void setProductQuantity(Integer productQuantity) {
        if (productQuantity == null) {
            productQuantity = 0;
        }
        Object oldValue = getProductQuantity();
        editObject.setProductQuantity(productQuantity);
        firePropertyChange(RangeRow.PROPERTY_PRODUCT_QUANTITY, oldValue, productQuantity);
    }

    public Integer getProductQuantity() {
        return editObject.getProductQuantity();
    }

    public void setSavQuantity(Integer savQuantity) {
        if (savQuantity == null) {
            savQuantity = 0;
        }
        Object oldValue = getSavQuantity();
        editObject.setSavQuantity(savQuantity);
        firePropertyChange(RangeRow.PROPERTY_SAV_QUANTITY, oldValue, savQuantity);
    }

    public Integer getSavQuantity() {
        return editObject.getSavQuantity();
    }

    public void setRange(Range range) {
        Object oldValue = getRange();
        editObject.setRange(range);
        firePropertyChange(RangeRow.PROPERTY_RANGE, oldValue, range);
        setValid(range != null);
    }

    public Range getRange() {
        return editObject.getRange();
    }

    public String getTopiaId() {
        return editObject.getTopiaId();
    }

    public void setTopiaId(String id) {
        editObject.setTopiaId(id);
    }

    @Override
    protected RangeRow newEntity() {
        return new RangeRowImpl();
    }

}
