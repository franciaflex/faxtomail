package com.franciaflex.faxtomail.ui.swing.content.transmit.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.actions.LoadFolderEmailsAction;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.actions.DisplayPageAction;
import com.franciaflex.faxtomail.ui.swing.content.transmit.MailFolderChooserUI;
import com.franciaflex.faxtomail.ui.swing.content.transmit.MailFolderChooserUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.transmit.MailFolderChooserUIModel;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUI;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.JAXXUtil;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class TransmitAction extends AbstractFaxToMailAction<MailFolderChooserUIModel, MailFolderChooserUI, MailFolderChooserUIHandler> {

    public TransmitAction(MailFolderChooserUIHandler handler) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.transmit.tip"));
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        MailFolderChooserUIModel model = getModel();
        List<DemandeUIModel> demandeUIModels = new ArrayList<>(model.getDemandeUIModels());

        for (final DemandeUIModel demandeUIModel : model.getDemandeUIModels()) {

            final MailFolder mailFolder = demandeUIModel.getMailFolder();
            List<DemandeUIModel> groupedDemandes = demandeUIModel.getGroupedDemandes();
            // on cherche les demandes groupées qui sont dans le même groupe, pour demander si on les déplace aussi
            Collection<DemandeUIModel> sameFolderGroupedDemands =
                    Collections2.filter(groupedDemandes, new Predicate<DemandeUIModel>() {
                @Override
                public boolean apply(DemandeUIModel groupDemandeUIModel) {
                    return !demandeUIModel.equals(groupDemandeUIModel)
                                && groupDemandeUIModel.getDemandStatus().isEditableStatus()
                                && mailFolder.equals(groupDemandeUIModel.getMailFolder());
                }
            });

            if (!sameFolderGroupedDemands.isEmpty()) {
                StringBuilder groupedDemandTitles = new StringBuilder();
                for (DemandeUIModel groupedDemand : sameFolderGroupedDemands) {
                    groupedDemandTitles.append("- ").append(groupedDemand.getTitle()).append("<br/>");
                }

                int i = JOptionPane.showConfirmDialog(null,
                                                      t("faxtomail.alert.transmit.groupedDemandsInSameFolder.message",
                                                        demandeUIModel.getTitle(),
                                                        groupedDemandTitles.toString()),
                                                      t("faxtomail.alert.transmit.groupedDemandsInSameFolder.title"),
                                                      JOptionPane.YES_NO_OPTION,
                                                      JOptionPane.QUESTION_MESSAGE);
                if (i == JOptionPane.YES_OPTION) {
                    demandeUIModels.addAll(sameFolderGroupedDemands);
                }
            }
        }
        model.setDemandeUIModels(demandeUIModels);

        return result;
    }

    @Override
    public void doAction() throws Exception {
        MailFolderChooserUIModel model = getModel();

        Collection<String> emailIds = Collections2.transform(model.getDemandeUIModels(), DemandeUIModel::getTopiaId);

        try(FaxToMailServiceContext faxToMailServiceContext = getContext().newServiceContext()) {
            faxToMailServiceContext.getEmailService().transmit(emailIds, model.getMailFolder(), getContext().getCurrentUser());
        }
    }

    @Override
    public void postSuccessAction() {

        super.postSuccessAction();
        MailFolderChooserUIHandler handler = getHandler();

        FaxToMailUI parentUI = (FaxToMailUI) getUI().getContextValue(JAXXContext.class, JAXXUtil.PARENT);
        handler.closeFrame();
        if (parentUI != null) {
            if (DemandeUI.class.isAssignableFrom(parentUI.getClass())) {

                JFrame frameForDemande = getContext().getFrameForDemande(getContext().getCurrentEmail());
                frameForDemande.dispose();

                getContext().getMainUI().getHandler().reloadDemandList();

            } else if (DemandeListUI.class.isAssignableFrom(parentUI.getClass())) {
                DemandeListUI demandeListUI = (DemandeListUI) parentUI;
                DemandeListUIHandler listHandler = demandeListUI.getHandler();

                //update demand list using action to keep track of where we are in the list
                LoadFolderEmailsAction loadFolderEmailsAction = new LoadFolderEmailsAction(listHandler);
                getContext().getActionEngine().runAction(loadFolderEmailsAction);
            }
        }

    }
}
