package com.franciaflex.faxtomail.ui.swing.actions;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.ui.swing.FaxToMailScreen;
import com.franciaflex.faxtomail.ui.swing.content.MainUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;

import static org.nuiton.i18n.I18n.t;

/**
 * To show demande screen.
 *
 * @author kmorin - morin@codelutin.com
 */
public class ShowDemandeListAction extends AbstractChangeScreenAction {

    public ShowDemandeListAction(MainUIHandler handler) {
        super(handler, true, FaxToMailScreen.LIST);
        setActionDescription(t("faxtomail.action.goto.demandList.tip"));
    }

    @Override
    public void doAction() throws Exception {
        getContext().setSearch(null);
        getContext().setCurrentPaginationParameter(null);
        super.doAction();
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        MailFolder currentMailFolder = getContext().getCurrentMailFolder();
        DemandeUIModel currentEmail = getContext().getCurrentEmail();

        if (currentMailFolder == null && currentEmail != null) {
            currentMailFolder = currentEmail.getMailFolder();
        }
        DemandeListUI listUI = (DemandeListUI) getHandler().getCurrentBody();
        listUI.getModel().setSelectedFolder(currentMailFolder);
    }
}
