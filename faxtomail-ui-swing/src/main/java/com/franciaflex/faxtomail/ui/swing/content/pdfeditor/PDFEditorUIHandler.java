package com.franciaflex.faxtomail.ui.swing.content.pdfeditor;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.HistoryType;
import com.franciaflex.faxtomail.persistence.entities.Stamp;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorUIModel.EditionComponent;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorUIModel.Page;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.actions.DisplayPageAction;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.actions.GenerateAnnotatedAttachmentAction;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.itextpdf.text.DocumentException;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.ComponentMover;
import jaxx.runtime.swing.ComponentResizer;
import jaxx.runtime.swing.JAXXButtonGroup;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.xerces.impl.dv.util.Base64;
import org.nuiton.jaxx.application.swing.util.Cancelable;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class PDFEditorUIHandler extends AbstractFaxToMailUIHandler<PDFEditorUIModel, PDFEditorUI> implements CloseableUI, Cancelable {

    private static final Log log = LogFactory.getLog(PDFEditorUIHandler.class);

    protected ComponentMover cm = new ComponentMover();
    protected ComponentResizer cr = new ComponentResizer();

    protected File file;

    protected int numberOfPages;

    protected PropertyChangeListener busyListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Boolean newvalue = (Boolean) evt.getNewValue();
            updateBusyState(newvalue != null && newvalue);
        }
    };

    @Override
    public void beforeInit(PDFEditorUI ui) {
        super.beforeInit(ui);
        final PDFEditorUIModel model = new PDFEditorUIModel();
        model.setZoom(getConfig().getDefaultZoomValue());
        ui.setContextValue(model);

        // ecoute des changements de l'état busy
        getContext().addPropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, busyListener);

    }

    @Override
    public void afterInit(PDFEditorUI pdfEditorUI) {
        initUI(pdfEditorUI);

        cm.setDragInsets(cr.getDragInsets());
        cm.setEdgeInsets(new Insets(0, 0, 0, 0));

        final PDFEditorUIModel model = getModel();

        JAXXButtonGroup actionGroup = ui.getActionGroup();
        actionGroup.addPropertyChangeListener(JAXXButtonGroup.SELECTED_VALUE_PROPERTY, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Object newValue = evt.getNewValue();
                Object selectedComponent;
                if (newValue instanceof String) {
                    String value = (String) newValue;
                    selectedComponent = PDFEditorUIModel.EditionComponent.valueOf(value);

                } else {
                    selectedComponent = newValue;
                }
                model.setSelectedComponent(selectedComponent);
            }
        });

        String value = (String) actionGroup.getSelectedValue();
        model.setSelectedComponent(PDFEditorUIModel.EditionComponent.valueOf(value));

        ui.getContainer().addContainerListener(new ContainerListener() {
            @Override
            public void componentAdded(ContainerEvent e) {
                Component child = e.getChild();
                Page currentPage = model.getCurrentPage();
                Class<? extends Component> childClass = child.getClass();
                if (childClass.isAssignableFrom(PDFEditorNoteUI.class)) {
                    currentPage.addNote((PDFEditorNoteUI) child);

                } else if (childClass.isAssignableFrom(PDFEditorCrossUI.class)) {
                    currentPage.addCross((PDFEditorCrossUI) child);

                } else if (childClass.isAssignableFrom(PDFEditorLineUI.class)) {
                    currentPage.addLine((PDFEditorLineUI) child);

                } else if (childClass.isAssignableFrom(PDFEditorHighlighterUI.class)) {
                    currentPage.addHighlighter((PDFEditorHighlighterUI) child);

                } else if (childClass.isAssignableFrom(PDFEditorStampTextUI.class)) {
                    currentPage.addTextStamp((PDFEditorStampTextUI) child);

                } else if (childClass.isAssignableFrom(PDFEditorStampImageUI.class)) {
                    currentPage.addImageStamp((PDFEditorStampImageUI) child);
                }
            }

            @Override
            public void componentRemoved(ContainerEvent e) {
                Component child = e.getChild();
                Page currentPage = model.getCurrentPage();
                Class<? extends Component> childClass = child.getClass();
                if (childClass.isAssignableFrom(PDFEditorNoteUI.class)) {
                    currentPage.removeNote((PDFEditorNoteUI) child);

                } else if (childClass.isAssignableFrom(PDFEditorCrossUI.class)) {
                    currentPage.removeCross((PDFEditorCrossUI) child);

                } else if (childClass.isAssignableFrom(PDFEditorLineUI.class)) {
                    currentPage.removeLine((PDFEditorLineUI) child);

                } else if (childClass.isAssignableFrom(PDFEditorHighlighterUI.class)) {
                    currentPage.removeHighlighter((PDFEditorHighlighterUI) child);

                } else if (childClass.isAssignableFrom(PDFEditorStampTextUI.class)) {
                    currentPage.removeTextStamp((PDFEditorStampTextUI) child);

                } else if (childClass.isAssignableFrom(PDFEditorStampImageUI.class)) {
                    currentPage.removeImageStamp((PDFEditorStampImageUI) child);
                }
            }
        });

        FaxToMailUIUtil.initScrollPaneBars(getUI().getDocumentScrollPane());

        model.addPropertyChangeListener(PDFEditorUIModel.PROPERTY_CURRENT_PAGE_INDEX,
                                        new PropertyChangeListener() {
                                            @Override
                                            public void propertyChange(PropertyChangeEvent evt) {
                                                Integer pageNb = (Integer) evt.getNewValue();
                                                Integer prevPageNb = (Integer) evt.getOldValue();
                                                updatePageNumber(pageNb, prevPageNb);
                                            }
                                        }
        );

        model.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (Attachment.PROPERTY_ORIGINAL_FILE.equals(evt.getPropertyName())
                        || Attachment.PROPERTY_EDITED_FILE.equals(evt.getPropertyName())) {

                    PDFEditorUIModel model = (PDFEditorUIModel) evt.getSource();
                    AttachmentFile attachmentFile = model.getNotNullFile();

                    if (attachmentFile != null) {
                        file = attachmentFile.getFile();
                        if (!FaxToMailUIUtil.isFileAPDF(attachmentFile)) {
                            try {
                                file = convertFileToPdf(attachmentFile);

                            } catch (IOException | DocumentException e) {
                                if (log.isErrorEnabled()) {
                                    log.error("error while converting file to pdf", e);
                                }
                                getContext().getErrorHelper().showErrorDialog(t("faxtomail.pdfEditor.convertToPdf.error"));
                            }
                        }

                        if (FaxToMailUIUtil.isFileAPDF(attachmentFile)) {
                                model.setPageNumber(getNumberOfPages());
                                model.setCurrentPageIndex(1);
                        }
                        getUI().setCursor(Cursor.getDefaultCursor());
                        model.firePropertyChanged(PDFEditorUIModel.PROPERTY_CURRENT_PAGE_INDEX, null, 1);
                    }
                }
            }
        });

        model.addPropertyChangeListener(PDFEditorUIModel.PROPERTY_ZOOM, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                displayPage(model.getCurrentPageIndex(),
                            (Float) evt.getOldValue(),
                            model.getRotation());
            }
        });

        model.addPropertyChangeListener(PDFEditorUIModel.PROPERTY_ROTATION, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                displayPage(model.getCurrentPageIndex(),
                            model.getZoom(),
                            (Integer) evt.getOldValue());
            }
        });

        model.addPropertyChangeListener(PDFEditorUIModel.PROPERTY_STAMPS, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Collection<Stamp> stamps = (Collection<Stamp>) evt.getNewValue();
                updateStamps(stamps);
            }
        });
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    @Override
    public SwingValidator<PDFEditorUIModel> getValidator() {
        return null;
    }

    @Override
    public boolean quitUI() {

        GenerateAnnotatedAttachmentAction generateAnnotatedAttachmentAction =
                getContext().getActionFactory().createLogicAction(this, GenerateAnnotatedAttachmentAction.class);
        generateAnnotatedAttachmentAction.setCloseFrameWhenDone(false);

        boolean result = quitScreen2(
                true,
                getModel().isModify(),
                null,
                t("faxtomail.pdfEditor.askSaveBeforeLeaving.save"),
                true,
                generateAnnotatedAttachmentAction
        );

        // soit les modifs doivent etre annulées, soit elles ont été enregistrées, donc on peut reseter le model
        if (result) {
            getModel().setModify(false);
        }

        return result;
    }

    @Override
    public Component getTopestUI() {
        return getUI();
    }

    @Override
    public void onCloseUI() {
        getContext().removePropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, busyListener);
    }

    public void addEditionComponent(MouseEvent event) {
        Object selectedComponent = getModel().getSelectedComponent();

        int x = event.getX();
        int y = event.getY();
        if (selectedComponent instanceof PDFEditorUIModel.EditionComponent) {
            EditionComponent editionComponent = (EditionComponent) selectedComponent;
            switch (editionComponent) {
                case NOTE:
                    addNote(x, y);
                    break;
                case CROSS:
                    addCross(x, y);
                    break;
                case HLINE:
                    addHLine(x, y);
                    break;
                case VLINE:
                    addVLine(x, y);
                    break;
                case HIGHLIGHTER:
                    addHighlighter(x, y);
                    break;
            }

        } else if (selectedComponent instanceof Stamp) {
            addStamp((Stamp) selectedComponent, x, y);
        }
    }

    public void addNote(int x, int y) {
        PDFEditorNoteUI note = new PDFEditorNoteUI();
        note.setZoom(getModel().getZoom());
        String title = decorate(new Date()) + " - " + getContext().getCurrentUser().getTrigraph();
        note.setTitle(title);
        addPanel(note, x, y);
    }

    public void addCross(int x, int y) {
        PDFEditorCrossUI cross = new PDFEditorCrossUI();
        addPanel(cross, x, y);
        cr.registerComponent(cross);
    }

    public void addHLine(int x, int y) {
        PDFEditorLineUI line = new PDFEditorLineUI();
        line.setHorizontal(true);
        addPanel(line, x, y);
        cr.registerComponent(ComponentResizer.DIRECTION_HORIZONTAL, line);
    }

    public void addVLine(int x, int y) {
        PDFEditorLineUI line = new PDFEditorLineUI();
        line.setHorizontal(false);
        addPanel(line, x, y);
        cr.registerComponent(ComponentResizer.DIRECTION_VERTICAL, line);
    }

    public void addHighlighter(int x, int y) {
        PDFEditorHighlighterUI highlighter = new PDFEditorHighlighterUI();
        addPanel(highlighter, x, y);
        cr.registerComponent(highlighter);
    }

    public void addStamp(Stamp stamp, int x, int y) {

        float zoom = getModel().getZoom();

        if (stamp.getImage() != null) {
            String base64Image = stamp.getImage().split(",")[1];
            byte[] imageContent = Base64.decode(base64Image);
            ImageIcon imageIcon = new ImageIcon(imageContent);

            PDFEditorStampImageUI stampUI = new PDFEditorStampImageUI();
            Image image = imageIcon.getImage();
            stampUI.setImage(image);

            int scaledWidth = (int) (image.getWidth(null) * zoom);
            int scaledHeight = (int) (image.getHeight(null) * zoom);
            Dimension scaledSize = new Dimension(scaledWidth, scaledHeight);
            stampUI.setSize(scaledSize);
            stampUI.setPreferredSize(scaledSize);

            addPanel(stampUI, x, y);

        } else {
            PDFEditorStampTextUI stampUI = new PDFEditorStampTextUI();
            stampUI.setText(stamp.getText());
            stampUI.setZoom(zoom);
            addPanel(stampUI, x, y);
        }

    }

    protected void addPanel(JPanel panel, int x, int y) {
        JPanel container = ui.getContainer();
        container.add(panel, 0);

        Insets insets = container.getInsets();
        Dimension size = panel.getPreferredSize();
        panel.setBounds(x + insets.left, y + insets.top,
                        size.width, size.height);

        container.updateUI();

        cm.registerComponent(panel);
    }

    protected void updatePageNumber(Integer pageNb, Integer prevPageNb) {
        if (pageNb != null) {
            if (prevPageNb != null) {
                Page p = getModel().getPage(prevPageNb);
                for (JPanel panel : p.getNotes()) {
                    panel.setVisible(false);
                }
                for (JPanel panel : p.getCrosses()) {
                    panel.setVisible(false);
                }
                for (JPanel panel : p.getLines()) {
                    panel.setVisible(false);
                }
                for (JPanel panel : p.getHighlighters()) {
                    panel.setVisible(false);
                }
            }

            displayPage(pageNb, getModel().getZoom(), getModel().getRotation());
        }
    }

    protected void displayPage(int pageNb, float previousZoom, int previousRotation) {
        if (pageNb < 1 || pageNb > getNumberOfPages()) {
            return;
        }

        DisplayPageAction displayPageAction = new DisplayPageAction(this, pageNb, previousZoom, previousRotation);

        getContext().getActionEngine().runAction(displayPageAction);

    }

    protected void updateStamps(Collection<Stamp> stamps) {
        PDFEditorUI ui = getUI();
        JAXXButtonGroup actionGroup = ui.getActionGroup();
        Icon stampIcon = SwingUtil.createImageIcon("stamp.png");
        JToolBar toolbar = ui.getToolbar();

        int i = toolbar.getComponentIndex(ui.getActionGroupSeparator());

        for (Stamp stamp : stamps) {
            JToggleButton button = new JToggleButton(stamp.getLabel(), stampIcon);
            button.setToolTipText(stamp.getDescription());
            button.putClientProperty("$value", stamp);

            actionGroup.add(button);

            toolbar.add(button, i++);
        }
    }

    public File convertFileToPdf(AttachmentFile attachmentFile) throws IOException, DocumentException {
        File target = FaxToMailUIUtil.convertFileToPdf(attachmentFile);
        // convert content to blob
        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            AttachmentFile attachmentFileNew = serviceContext.getEmailService().getAttachmentFileFromStream(new FileInputStream(target));
            attachmentFileNew.setFilename(attachmentFile.getFilename() + ".pdf");
            attachmentFileNew.setRotation(0);
            getModel().setEditedFile(attachmentFileNew);
        }

        return target;
    }

    public void zoomOut() {
        PDFEditorUIModel model = getModel();
        float zoom = model.getZoom();
        model.setZoom(zoom - getConfig().getDefaultZoomStepSize());
    }

    public void zoomIn() {
        PDFEditorUIModel model = getModel();
        float zoom = model.getZoom();
        model.setZoom(zoom + getConfig().getDefaultZoomStepSize());
    }

    public void rotateClockwise() {
        int rotation = getModel().getRotation();
        getModel().setRotation((360 + rotation + 90) % 360);
    }

    public void rotateAntiClockwise() {
        int rotation = getModel().getRotation();
        getModel().setRotation((360 + rotation - 90) % 360);
    }

    public void print() {
        PDFEditorUIModel model = getModel();
        AttachmentFile attachmentFile = model.getNotNullFile();
        boolean print = FaxToMailUIUtil.print(attachmentFile, false, getContext().getConfig(), null);

        // TODO kmorin 20140702 à mettre dans une action ou avec un loading ou qqchose
        // j'ai essayé une action vite fait mais ca ferme l'éditeur à la fin de l'action
        if (print) {
            DemandeUIModel demand = model.getDemand();
            try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
                Email email = serviceContext.getEmailService().addToHistory(demand.getTopiaId(),
                        HistoryType.PRINTING,
                        getContext().getCurrentUser(),
                        new Date(),
                        attachmentFile.getFilename());
                demand.setHistory(email.getHistory());
            } catch (IOException eee) {
                log.error("Error creating history entry",eee);
            }
        }
    }

    @Override
    public void cancel() {
        closeFrame();
    }

    public BufferedImage getPage(int pageNumber, float zoom){
        synchronized(PDFEditorUIHandler.class) {
            try (PDDocument pdDocument = PDDocument.load(file)) {
                PDFRenderer renderer = new PDFRenderer(pdDocument);
                return renderer.renderImage(pageNumber - 1, zoom);
            } catch (Throwable eee) { //catch also JavaHeapSpace
                log.debug("Error getting page " + pageNumber, eee);
                getContext().getErrorHelper().showErrorDialog(t("faxtomail.pdfEditor.readPdf.error"));
                return null;
            }
        }
    }

    public synchronized int getNumberOfPages(){
        if (numberOfPages == 0){
            try (PDDocument pdDocument = PDDocument.load(file)){
                numberOfPages = pdDocument.getNumberOfPages();
            } catch (IOException eee){
                log.info("Error getting number of pages", eee);
                getContext().getErrorHelper().showErrorDialog(t("faxtomail.pdfEditor.readPdf.error"));
            }
        }
        return numberOfPages;
    }

    public ComponentResizer getCr() {
        return cr;
    }

    protected void updateBusyState(boolean busy) {
        if (busy) {
            // ui bloquee
            if (log.isDebugEnabled()) {
                log.debug("block ui in busy mode");
            }
            ui.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        } else {
            // ui debloquee
            if (log.isDebugEnabled()) {
                log.debug("unblock ui in none busy mode");
            }
            ui.setCursor(Cursor.getDefaultCursor());
        }
    }
}
