package com.franciaflex.faxtomail.ui.swing.content.search;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.HasLabel;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.SearchFilter;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.ShowDemandeListAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.search.actions.SearchAction;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailDemandListHandler;
import com.franciaflex.faxtomail.ui.swing.util.DemandeTableModel;
import com.franciaflex.faxtomail.ui.swing.util.PaginationComboModel;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;
import org.nuiton.jaxx.application.swing.util.CloseableUI;
import org.nuiton.util.pagination.PaginationParameter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler of UI {@link SearchUIHandler}.
 *
 * @author kmorin - morin@codelutin.com
 *
 */
public class SearchUIHandler extends AbstractFaxToMailDemandListHandler<SearchUIModel, SearchUI> implements CloseableUI {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SearchUIHandler.class);

    protected List<MailField> tableColumns;

    @Override
    public void beforeInit(SearchUI ui) {
        super.beforeInit(ui);

        SearchUIModel searchUIModel = getContext().getSearch();
        if (searchUIModel == null) {
            searchUIModel = new SearchUIModel();

            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -2);
            Date twoMonthsAgo = cal.getTime();

            searchUIModel.setMaxReceptionDate(now);
            searchUIModel.setMinReceptionDate(twoMonthsAgo);

            List<Client> allowedClients = ui.getContextValue(List.class, SearchUIModel.PROPERTY_ALLOWED_CLIENTS);
            searchUIModel.setAllowedClients(allowedClients);
        }

        getUI().setContextValue(searchUIModel);
    }

    @Override
    public void afterInit(SearchUI ui) {

        initUI(ui);

        // this loading take 1,5 seconds
        List<FaxToMailUser> users = getContext().getFaxtomailUserCache();
        SearchUIModel model = getModel();
        initBeanFilterableComboBox(ui.getTakenByComboBox(),
                                   users,
                                   model.getTakenBy());
        initBeanFilterableComboBox(ui.getModifiedByComboBox(),
                                   users,
                                   model.getModifiedBy());
        initBeanFilterableComboBox(ui.getArchivedByComboBox(),
                                   users,
                                   model.getArchivedBy());
        initBeanFilterableComboBox(ui.getTransferByComboBox(),
                                   users,
                                   model.getTransferBy());
        initBeanFilterableComboBox(ui.getPrintedByComboBox(),
                                   users,
                                   model.getPrintingBy());
        initBeanFilterableComboBox(ui.getRepliedByComboBox(),
                                   users,
                                   model.getReplyBy());

        final BeanFilterableComboBox<Client> clientComboBox = ui.getClientComboBox();
        clientComboBox.getComboBoxModel().setWildcardCharacter(null);
        initBeanFilterableComboBox(clientComboBox, Collections.singletonList(model.getClient()), model.getClient());

        KeyAdapter clientComboBoxKeyAdapter = new KeyAdapter() {

            @Override
            public void keyTyped(KeyEvent e) {
                String text = clientComboBox.getComboBoxModel().getFilterText();
                text+=e.getKeyChar();

                int filterStartChars = getConfig().getClientComboBoxFilterStartChars();

                if (text.length()==filterStartChars){
                    //init list when x chars entered
                    try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
                        List<Client> clients = serviceContext.getClientService().getAllClientsForUserFilter(getContext().getCurrentUser(), text);
                        clientComboBox.getComboBoxModel().removeAllElements();
                        clientComboBox.getComboBoxModel().addAllElements(clients);
                    } catch (IOException eee) {
                        log.error("Error initialising client combobox");
                    }
                } else if (text.length()<filterStartChars){
                    //empty list if less than x chars
                    clientComboBox.getComboBoxModel().removeAllElements();
                } else {
                    // do nothing if more than x chars entered
                }
            }
        };

        clientComboBox.getCombobox().getEditor().getEditorComponent().addKeyListener(clientComboBoxKeyAdapter);

        initCheckBoxComboBox(ui.getDocTypeComboBox(),
                             getContext().getDemandTypeCache(),
                             model.getDemandType(),
                             SearchFilter.PROPERTY_DEMAND_TYPE,
                             true);
        initCheckBoxComboBox(ui.getPriorityComboBox(),
                             getContext().getPriorityCache(),
                             model.getPriority(),
                             SearchFilter.PROPERTY_PRIORITY,
                             true);
        initCheckBoxComboBox(ui.getWaitingStateComboBox(),
                             getContext().getWaitingStateCache(),
                             model.getWaitingStates(),
                             SearchFilter.PROPERTY_WAITING_STATES,
                             true);
        initCheckBoxComboBox(ui.getStatusComboBox(),
                             Arrays.asList(DemandStatus.values()),
                             model.getDemandStatus(),
                             SearchFilter.PROPERTY_DEMAND_STATUS,
                             false);
        initCheckBoxComboBox(ui.getGammeComboBox(),
                             getContext().getRangeCache(),
                             model.getGamme(),
                             SearchFilter.PROPERTY_GAMME,
                             false);

        final JXTable dataTable = getUI().getDataTable();
        dataTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        initDemandeTable(dataTable, true);

        model.addPropertyChangeListener(SearchUIModel.PROPERTY_RESULTS, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                List<DemandeUIModel> emails = (List<DemandeUIModel>) evt.getNewValue();
                DemandeTableModel dataTableModel = (DemandeTableModel) dataTable.getModel();
                dataTableModel.setRows(emails);

                DemandeUIModel currentEmail = getContext().getCurrentEmail();
                if (currentEmail != null) {
                    int row = dataTableModel.getRowIndex(currentEmail);
                    if (row > 0) {
                        dataTable.setRowSelectionInterval(row, row);
                        dataTable.scrollRowToVisible(row);
                    }
                }
            }
        });
        
        if (getContext().getSearch() != null) {
            runSearchAction();
        }

        // int combo box for result per page
        ui.getResultPerPageCombo().setModel(new PaginationComboModel());
        int resultPerPage = getConfig().getResultPerPage();
        ui.getModel().setResultPerPage(resultPerPage);
        ui.getResultPerPageCombo().setSelectedItem(resultPerPage);
        ui.getResultPerPageCombo().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                updateResultPerPage(e);
            }
        });
    }

    @Override
    protected void initTextField(JTextField jTextField) {
        super.initTextField(jTextField);
        jTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchDemandes();
            }
        });
    }

    @Override
    protected <HL extends HasLabel> void initCheckBoxComboBox(JComboBox<HL> comboBox, List<HL> values, List<HL> selection, String property, boolean addNull) {
        super.initCheckBoxComboBox(comboBox, values, selection, property, addNull);
        comboBox.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    searchDemandes();
                }
            }
        });
    }

    @Override
    protected void initDatePicker(JXDatePicker picker) {
        super.initDatePicker(picker);
        picker.getEditor().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    searchDemandes();
                }
            }
        });
    }

    protected void onDoubleClickOnDemande(DemandeUIModel selectedEmail) {
        getContext().setCurrentPaginationParameter(getModel().getPaginationParameter());
    }

    public void searchDemandes() {
        getModel().resetPaginationParameter();
        runSearchAction();
    }

    public void goToNextPage() {
        getModel().setPaginationParameter(getModel().getPaginationResult().getNextPage());
        runSearchAction();
    }
    
    public void goToPreviousPage() {
        getModel().setPaginationParameter(getModel().getPaginationResult().getPreviousPage());
        runSearchAction();
    }
    
    public void updateResultPerPage(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            int resultPerPage = (Integer)event.getItem();
            getConfig().setResultPerPage(resultPerPage);
            getConfig().save();
            getModel().setResultPerPage(resultPerPage);
            getModel().resetPaginationParameter();
            if (CollectionUtils.isNotEmpty(getModel().getResults())) {
                runSearchAction();
            }
        }
    }
    
    protected void runSearchAction() {
        FaxToMailUIContext context = getContext();
        PaginationParameter currentPaginationParameter = context.getCurrentPaginationParameter();
        if (currentPaginationParameter != null) {
            getModel().setPaginationParameter(currentPaginationParameter);
            context.setCurrentPaginationParameter(null);
        }

        try {
            SearchAction searchAction = new SearchAction(this);
            if (context.isActionInProgress(null)) {
                context.getActionEngine().runFullInternalAction(searchAction);
            } else {
                context.getActionEngine().runAction(searchAction);
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("error while searching", e);
            }
            context.getErrorHelper().showErrorDialog(t("faxtomail.search.action.error"));
        }
    }

    @Override
    public List<MailField> getColumns() {
        if (tableColumns == null) {
            try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
                tableColumns = serviceContext.getConfigurationService().getSearchDisplayColumns();
            } catch (IOException eee) {
                log.error("Error getting configuration", eee);
            }
        }
        return tableColumns;
    }

    @Override
    protected boolean isDisplayOnlyUserTrigraphInTables() {
        return false;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getModifiedByComboBox();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
    }

    @Override
    public boolean quitUI() {
//        boolean result = quitScreen(
//                getModel().isValid(),
//                getModel().isModify(),
//                _("tutti.editCruise.askCancelEditBeforeLeaving.cancelSaveCruise"),
//                _("tutti.editCruise.askSaveBeforeLeaving.saveCruise"),
//                ui.getSaveButton().getAction()
//        );
//        return result;
        return true;
    }

    @Override
    public SwingValidator<SearchUIModel> getValidator() {
        return null;
    }

    @Override
    protected void beforeOpenPopup(int rowIndex, int columnIndex) {
        super.beforeOpenPopup(rowIndex, columnIndex);

        JXTable dataTable = getUI().getDataTable();
        int selectedRowCount = dataTable.getSelectedRowCount();

        boolean menuEnabled = selectedRowCount == 1;

        if (menuEnabled) {
            DemandeTableModel dataTableModel = (DemandeTableModel) dataTable.getModel();
            rowIndex = dataTable.convertRowIndexToModel(rowIndex);
            DemandeUIModel selectedEmail = dataTableModel.getEntry(rowIndex);
            menuEnabled = selectedEmail.getArchiveDate() == null;
        }

        SearchUIModel model = getModel();

        model.setMenuEnabled(menuEnabled);
    }

    public void goToMenu() {
        JXTable dataTable = ui.getDataTable();
        int rowIndex = dataTable.getSelectedRow();
        rowIndex = dataTable.convertRowIndexToModel(rowIndex);
        DemandeUIModel selectedEmail = ((DemandeTableModel) dataTable.getModel()).getEntry(rowIndex);
        getContext().setCurrentEmail(selectedEmail);
        getContext().setCurrentMailFolder(selectedEmail.getMailFolder());
        getContext().getActionEngine().runAction(new ShowDemandeListAction(getContext().getMainUI().getHandler()));
    }

    @Override
    protected MailField[] getEditableTableProperties() {
        return new MailField[] { MailField.ATTACHMENT, MailField.REPLIES };
    }

    @Override
    public void reloadList() {
        //Do not reload list automagically
        //searchDemandes();
    }


}
