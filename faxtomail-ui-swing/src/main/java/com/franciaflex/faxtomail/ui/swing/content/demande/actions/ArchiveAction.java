package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.exceptions.AlreadyLockedMailException;
import com.franciaflex.faxtomail.services.service.exceptions.FolderNotReadableException;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Action d'archivage d'un mail.
 * 
 * Passe le mail et statut archivé et déplace le mail dans le dossier d'archive le plus proche.
 * 
 * @author Kevin Morin (Code Lutin)
 */
public class ArchiveAction extends SaveDemandeAndExitAction {

    private static final Log log = LogFactory.getLog(ArchiveAction.class);

    protected DemandeUIModel previousState;

    public ArchiveAction(DemandeUIHandler handler) {
        super(handler);
        setActionDescription(t("faxtomail.action.archive.tip"));
    }

    @Override
    public boolean prepareAction() throws Exception {
        int i = JOptionPane.showConfirmDialog(
                null,
                t("faxtomail.alert.archiveConfirmation.message"),
                t("faxtomail.alert.archiveConfirmation.title"),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        boolean result = i == JOptionPane.YES_OPTION;
        return result;
    }

    @Override
    public void doAction() throws Exception {
        DemandeUIModel model = getModel();

        //copy state to revert in case of error
        previousState = new DemandeUIModel();
        previousState.setDemandStatus(model.getDemandStatus());
        previousState.setTakenBy(model.getTakenBy());
        previousState.setMailFolder(model.getMailFolder());

        // déplacement du mail dans le dossier d'archive le plus proche
        // cela permet de déplacer les archives dans d'autre dossier pour leur appliquer
        // des droits différents
        // les dossiers d'archive ne sont pas visible dans l'arbre des dossiers, mais les archives
        // peuvent être consultées dans la recherche
        MailFolder folder = model.getMailFolder();
        MailFolder archiveChild = null;
        do {
            Collection<MailFolder> children = folder.getChildren();
            if (children != null) {
                Optional<MailFolder> optArchiveChild = Iterables.tryFind(children, mailFolder -> mailFolder.isArchiveFolder());
                if (optArchiveChild.isPresent()) {
                    archiveChild = optArchiveChild.get();
                }
            }
            folder = folder.getParent();

        } while (archiveChild == null && folder != null);

        // le dossier d'archive peut ne pas exister. Dans ce cas on laisse le mail dans le dossier
        // d'origine
        if (archiveChild != null) {
            model.setMailFolder(archiveChild);
        }

        model.setArchiveDate(new Date());
        model.setDemandStatus(DemandStatus.ARCHIVED);
        model.setTakenBy(null);

        try(FaxToMailServiceContext faxToMailServiceContext = getContext().newServiceContext()) {
            faxToMailServiceContext.getEmailService().unlockEmail(model.getTopiaId());
        }

        //cannot be reverted in postFailedAction
        model.clearPDFPages();

        super.doAction();
    }

    @Override
    public void postSuccessAction() {
        //Reloading list for #10071
        //We are archiving from demand and not demandList so we can reload demand list doing :
        getContext().getMainUI().getHandler().reloadDemandList();

        super.postSuccessAction();
    }

    @Override
    public void postFailedAction(Throwable error) {
        super.postFailedAction(error);

        //revert modifications made in the action
        DemandeUIModel model = getModel();
        model.setMailFolder(previousState.getMailFolder());
        model.setArchiveDate(null);
        model.setDemandStatus(previousState.getDemandStatus());
        model.setTakenBy(previousState.getTakenBy());
        try(FaxToMailServiceContext faxToMailServiceContext = getContext().newServiceContext()) {
            faxToMailServiceContext.getEmailService().lockEmail(model.getTopiaId(),getContext().getCurrentUser());
        } catch (AlreadyLockedMailException | FolderNotReadableException | IOException e) {
            log.warn("Could not lock back email after error archiving");
        }
        previousState = null;
    }

}
