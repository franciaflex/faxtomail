package com.franciaflex.faxtomail.ui.swing.content.transmit;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.franciaflex.faxtomail.ui.swing.util.FolderTreeNode;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.Component;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class MailFolderChooserUIHandler extends AbstractFaxToMailUIHandler<MailFolderChooserUIModel, MailFolderChooserUI> implements Cancelable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractFaxToMailUIHandler.class);

    @Override
    public void afterInit(MailFolderChooserUI mailFolderChooserUI) {
        initUI(mailFolderChooserUI);

        MailFolderChooserUIModel model = getModel();
        MailFolder currentMailFolder = model.getDemandeUIModels().get(0).getMailFolder();

        // on recupere seulement les dossiers qui accepte l'ensemble des états d'attentes de l'ensemble des demandes
        Set<WaitingState> allWaitingStates = new HashSet<WaitingState>();
        for (DemandeUIModel demande : model.getDemandeUIModels()) {
            if (demande.getWaitingState() != null) {
                allWaitingStates.add(demande.getWaitingState());
            }
        }

        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            Collection<MailFolder> foldersWithWaitingState = null;
            // foldersWithWaitingState reste à null dans le cas ou l'arbre ne doit pas être filtré
            // sinon l'appel peut renvoyer une liste de vide dans le cas où aucun dossier n'est selectionnable
            if (!allWaitingStates.isEmpty()) {
                foldersWithWaitingState = serviceContext.getMailFolderService().getFoldersWithWaitingState(allWaitingStates);
            }

            // init tree
            Collection<MailFolder> folders = serviceContext.getMailFolderService().
                    getRootMailFoldersWithMoveRights(getContext().getCurrentUser());
            JTree navigationTree = ui.getNavigationTree();

            Map<MailFolder, FolderTreeNode> nodesByFolder =
                    FaxToMailUIUtil.initFolderTree(getContext(), navigationTree, folders, false);

            for (Map.Entry<MailFolder, FolderTreeNode> entry : nodesByFolder.entrySet()) {
                FolderTreeNode node = nodesByFolder.get(entry.getKey());

                // le dossier n'est pas grisé si:
                // - le n'est pas le dossier courrant
                // - l'utilsateur à les droits sur le dossier
                // - le dossier cible à les mêmes etatAttente/typeDemande que toutes les demandes à bouger
                node.setCanSelect(!currentMailFolder.equals(entry.getKey()) && entry.getKey().isFolderMoveable() &&
                        (foldersWithWaitingState == null || foldersWithWaitingState.contains(entry.getKey())));
            }

            navigationTree.addTreeSelectionListener(new TreeSelectionListener() {
                @Override
                public void valueChanged(TreeSelectionEvent e) {
                    FolderTreeNode folderNode = (FolderTreeNode) e.getPath().getLastPathComponent();
                    MailFolder folder = folderNode.getMailFolder();
                    if (!folderNode.isCanSelect()) {
                        folder = null;
                    }
                    getModel().setMailFolder(folder);
                }
            });

            // fix jaxx binding, apparement, il faut lui refaire un set null
            getModel().setMailFolder(null);
        } catch (IOException eee) {
            log.error("Error initialising UI", eee);
        }
    }

    @Override
    public void onCloseUI() {
    }

    @Override
    public SwingValidator<MailFolderChooserUIModel> getValidator() {
        return null;
    }

    @Override
    public void cancel() {
        closeFrame();
    }

    @Override
    protected JComponent getComponentToFocus() {
        return ui.getNavigationTree();
    }

    @Override
    public Component getTopestUI() {
        return getUI();
    }
}
