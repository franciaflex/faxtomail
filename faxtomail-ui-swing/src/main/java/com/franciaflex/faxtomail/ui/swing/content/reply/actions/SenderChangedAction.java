package com.franciaflex.faxtomail.ui.swing.content.reply.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.SigningForDomain;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUI;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 2.0.4
 */
public class SenderChangedAction extends AbstractFaxToMailAction<ReplyFormUIModel, ReplyFormUI, ReplyFormUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SenderChangedAction.class);

    private SigningForDomain signingForDomain;

    public SenderChangedAction(ReplyFormUIHandler handler) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.senderChanged.tip"));
    }

    @Override
    public boolean prepareAction() throws Exception {
        return super.prepareAction() && !getModel().isReadonly();
    }

    @Override
    public void doAction() throws Exception {
        ReplyFormUIModel model = getModel();
        try (FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            signingForDomain = null;
            if (model.getOriginalDemand() != null && !model.getOriginalDemand().isFax() && model.getFrom() != null) {
                signingForDomain = serviceContext.getConfigurationService().getSigningForEmailAddress(model.getFrom()).orNull();
            }
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getModel().setSigning(signingForDomain);
        getHandler().replaceSigning();
    }

    @Override
    protected void releaseAction() {
        super.releaseAction();
        signingForDomain = null;
    }
}