package com.franciaflex.faxtomail.ui.swing.content.demande.takenby;

/*-
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.nuiton.jaxx.application.swing.util.Cancelable;
import org.nuiton.validator.bean.AbstractValidator;

import javax.swing.JComponent;
import java.awt.Component;
import java.awt.Window;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class TakenByFormUIHandler extends AbstractFaxToMailUIHandler<TakenByFormUIModel, TakenByFormUI> implements Cancelable {

    @Override
    public void afterInit(TakenByFormUI ui) {
        initUI(ui);

        // users
        initBeanFilterableComboBox(ui.getTakenByComboBox(), getModel().getUsers(), getModel().getTakenBy());

        getValidator().addPropertyChangeListener(AbstractValidator.VALID_PROPERTY, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                getModel().setValid((Boolean) evt.getNewValue());
            }
        });
    }

    @Override
    public void cancel() {
        closeFrame();
    }

    @Override
    public void onCloseUI() {
    }

    @Override
    public SwingValidator<TakenByFormUIModel> getValidator() {
        return getUI().getValidator();
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getTakenByComboBox();
    }

    @Override
    public Component getTopestUI() {
        return getParentContainer(Window.class);
    }

}
