package com.franciaflex.faxtomail.ui.swing.content.demande.demandgroup;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.toolbar.AbstractToolbarPopupButton;
import jaxx.runtime.SwingUtil;

import javax.swing.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Button to edit attachments.
 *
 * @author kmorin - morin@codelutin.com
 */
public class ButtonEmailGroup extends AbstractToolbarPopupButton<DemandGroupUI> {

    private final PropertyChangeListener listener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Collection<DemandeUIModel> demands = (Collection<DemandeUIModel>) evt.getNewValue();
            if (demands != null) {
                int size = demands.size();
                setText(t("faxtomail.demandGroup.text", size));
            }
        }
    };

    public ButtonEmailGroup() {
        setToolTipText(t("faxtomail.demandGroup.action.tip"));
        int demandNb = popup.getModel().sizeGroupedDemandes();
        setText(t("faxtomail.demandGroup.text", demandNb));
        popup.getModel().addPropertyChangeListener(DemandeUIModel.PROPERTY_GROUPED_DEMANDES, listener);
    }

    @Override
    protected String getActionIcon() {
        return "group";
    }

    @Override
    protected DemandGroupUI createNewPopup() {
        return new DemandGroupUI(FaxToMailUIContext.getApplicationContext(),
                                 SwingUtil.getParentContainer(this, JFrame.class));
    }

    @Override
    public void onCloseUI() {
        super.onCloseUI();
        popup.getModel().removePropertyChangeListener(DemandeUIModel.PROPERTY_GROUPED_DEMANDES, listener);
    }

}
