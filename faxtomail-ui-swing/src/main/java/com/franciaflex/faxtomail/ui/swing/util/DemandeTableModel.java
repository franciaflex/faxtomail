package com.franciaflex.faxtomail.ui.swing.util;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import jaxx.runtime.SwingUtil;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;

/**
 * @author kmorin - kmorin@codelutin.com
 *
 */
public class DemandeTableModel extends AbstractTableModel {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(DemandeTableModel.class);

    public static final BiMap<MailField, ColumnIdentifier<Email>> COLUMN_IDENTIFIERS = HashBiMap.create(MailField.values().length);
    static {
        COLUMN_IDENTIFIERS.put(MailField.SENDER,
            ColumnIdentifier.<Email>newReadOnlyId(
                Email.PROPERTY_SENDER,
                n("faxtomail.demandeList.table.header.sender"),
                n("faxtomail.demandeList.table.header.sender.tip")));

        COLUMN_IDENTIFIERS.put(MailField.OBJECT,
            ColumnIdentifier.<Email>newReadOnlyId(
                    Email.PROPERTY_OBJECT,
                    n("faxtomail.demandeList.table.header.object"),
                    n("faxtomail.demandeList.table.header.object.tip")));

        COLUMN_IDENTIFIERS.put(MailField.RECEPTION_DATE,
            ColumnIdentifier.<Email>newReadOnlyId(
                    Email.PROPERTY_RECEPTION_DATE,
                    n("faxtomail.demandeList.table.header.receptionDate"),
                    n("faxtomail.demandeList.table.header.receptionDate.tip")));

        COLUMN_IDENTIFIERS.put(MailField.RECIPIENT,
            ColumnIdentifier.<Email>newReadOnlyId(
                    Email.PROPERTY_RECIPIENT,
                    n("faxtomail.demandeList.table.header.recipient"),
                    n("faxtomail.demandeList.table.header.recipient.tip")));

        COLUMN_IDENTIFIERS.put(MailField.CLIENT_CODE,
            ColumnIdentifier.<Email>newReadOnlyId(
                    DemandeUIModel.PROPERTY_CLIENT_CODE,
                    n("faxtomail.demandeList.table.header.clientCode"),
                    n("faxtomail.demandeList.table.header.clientCode.tip")));

        COLUMN_IDENTIFIERS.put(MailField.CLIENT_NAME,
            ColumnIdentifier.<Email>newReadOnlyId(
                    DemandeUIModel.PROPERTY_CLIENT_NAME,
                    n("faxtomail.demandeList.table.header.clientName"),
                    n("faxtomail.demandeList.table.header.clientName.tip")));

        COLUMN_IDENTIFIERS.put(MailField.CLIENT_BRAND,
            ColumnIdentifier.<Email>newReadOnlyId(
                    DemandeUIModel.PROPERTY_CLIENT_BRAND,
                    n("faxtomail.demandeList.table.header.brand"),
                    n("faxtomail.demandeList.table.header.brand.tip")));

        COLUMN_IDENTIFIERS.put(MailField.DEMAND_STATUS,
            ColumnIdentifier.<Email>newReadOnlyId(
                    Email.PROPERTY_DEMAND_STATUS,
                    n("faxtomail.demandeList.table.header.status"),
                    n("faxtomail.demandeList.table.header.status.tip")));

        COLUMN_IDENTIFIERS.put(MailField.DEMAND_TYPE,
            ColumnIdentifier.<Email>newReadOnlyId(
                    Email.PROPERTY_DEMAND_TYPE,
                    n("faxtomail.demandeList.table.header.type"),
                    n("faxtomail.demandeList.table.header.type.tip")));

        COLUMN_IDENTIFIERS.put(MailField.EDI_RETURN,
            ColumnIdentifier.<Email>newReadOnlyId(
                    Email.PROPERTY_EDI_ERROR,
                    n("faxtomail.demandeList.table.header.ediCodeNumber"),
                    n("faxtomail.demandeList.table.header.ediCodeNumber.tip")));

        COLUMN_IDENTIFIERS.put(MailField.WAITING_STATE,
            ColumnIdentifier.<Email>newReadOnlyId(
                    Email.PROPERTY_WAITING_STATE,
                    n("faxtomail.demandeList.table.header.waitingState"),
                    n("faxtomail.demandeList.table.header.waitingState.tip")));

        COLUMN_IDENTIFIERS.put(MailField.TAKEN_BY,
            ColumnIdentifier.<Email>newReadOnlyId(
                    Email.PROPERTY_TAKEN_BY,
                    n("faxtomail.demandeList.table.header.user"),
                    n("faxtomail.demandeList.table.header.user.tip")));

        COLUMN_IDENTIFIERS.put(MailField.PRIORITY,
            ColumnIdentifier.<Email>newId(
                    Email.PROPERTY_PRIORITY,
                    n("faxtomail.demandeList.table.header.priority"),
                    n("faxtomail.demandeList.table.header.priority.tip")));

        COLUMN_IDENTIFIERS.put(MailField.PROJECT_REFERENCE,
            ColumnIdentifier.<Email>newReadOnlyId(
                    Email.PROPERTY_PROJECT_REFERENCE,
                    n("faxtomail.demandeList.table.header.projectReference"),
                    n("faxtomail.demandeList.table.header.projectReference.tip")));

        COLUMN_IDENTIFIERS.put(MailField.COMPANY_REFERENCE,
            ColumnIdentifier.<Email>newReadOnlyId(
                    Email.PROPERTY_COMPANY_REFERENCE,
                    n("faxtomail.demandeList.table.header.companyReference"),
                    n("faxtomail.demandeList.table.header.companyReference.tip")));

        COLUMN_IDENTIFIERS.put(MailField.REFERENCE,
            ColumnIdentifier.<Email>newReadOnlyId(
                    DemandeUIModel.PROPERTY_REFERENCE,
                    n("faxtomail.demandeList.table.header.reference"),
                    n("faxtomail.demandeList.table.header.reference.tip")));

        COLUMN_IDENTIFIERS.put(MailField.RANGE_ROW,
            ColumnIdentifier.<Email>newReadOnlyId(
                    Email.PROPERTY_RANGE_ROW,
                    n("faxtomail.demandeList.table.header.range"),
                    n("faxtomail.demandeList.table.header.range.tip")));

        COLUMN_IDENTIFIERS.put(MailField.PF_NB,
            ColumnIdentifier.<Email>newReadOnlyId(
                    DemandeUIModel.PROPERTY_PF_NB,
                    n("faxtomail.demandeList.table.header.pfNb"),
                    n("faxtomail.demandeList.table.header.pfNb.tip")));

        COLUMN_IDENTIFIERS.put(MailField.SAV_NB,
            ColumnIdentifier.<Email>newReadOnlyId(
                    DemandeUIModel.PROPERTY_SAV_NB,
                    n("faxtomail.demandeList.table.header.savNb"),
                    n("faxtomail.demandeList.table.header.savNb.tip")));

        COLUMN_IDENTIFIERS.put(MailField.QUOTATION_NB,
            ColumnIdentifier.<Email>newReadOnlyId(
                    DemandeUIModel.PROPERTY_QUOTATION_NB,
                    n("faxtomail.demandeList.table.header.quotationNb"),
                    n("faxtomail.demandeList.table.header.quotationNb.tip")));

        COLUMN_IDENTIFIERS.put(MailField.REPLIES,
            ColumnIdentifier.<Email>newId(
                    Email.PROPERTY_REPLIES,
                    n("faxtomail.demandeList.table.header.replies"),
                    n("faxtomail.demandeList.table.header.replies.tip")));

        COLUMN_IDENTIFIERS.put(MailField.ATTACHMENT,
            ColumnIdentifier.<Email>newId(
                    Email.PROPERTY_ATTACHMENT,
                    n("faxtomail.demandeList.table.header.attachment"),
                    n("faxtomail.demandeList.table.header.attachment.tip")));

        COLUMN_IDENTIFIERS.put(MailField.GROUP,
            ColumnIdentifier.<Email>newId(
                    DemandeUIModel.PROPERTY_GROUPED_DEMANDES,
                    n("faxtomail.demandeList.table.header.emailGroup"),
                    n("faxtomail.demandeList.table.header.emailGroup.tip")));

        COLUMN_IDENTIFIERS.put(MailField.LAST_ATTACHMENT_OPENING_IN_THIS_FOLDER_USER,
            ColumnIdentifier.<Email>newId(
                    Email.PROPERTY_LAST_ATTACHMENT_OPENER,
                    n("faxtomail.demandeList.table.header.lastAttachmentOpeningUser"),
                    n("faxtomail.demandeList.table.header.lastAttachmentOpeningUser.tip")));

        COLUMN_IDENTIFIERS.put(MailField.COMMENT,
            ColumnIdentifier.<Email>newId(
                    Email.PROPERTY_COMMENT,
                    n("faxtomail.demandeList.table.header.comment"),
                    n("faxtomail.demandeList.table.header.comment.tip")));

        COLUMN_IDENTIFIERS.put(MailField.SUBJECT,
            ColumnIdentifier.<Email>newId(
                    Email.PROPERTY_SUBJECT,
                    n("faxtomail.demandeList.table.header.subject"),
                    n("faxtomail.demandeList.table.header.subject.tip")));

        COLUMN_IDENTIFIERS.put(MailField.LAST_PRINTING_USER,
            ColumnIdentifier.<Email>newId(
                    Email.PROPERTY_LAST_PRINTING_USER,
                    n("faxtomail.demandeList.table.header.lastPrintingUser"),
                    n("faxtomail.demandeList.table.header.lastPrintingUser.tip")));

        COLUMN_IDENTIFIERS.put(MailField.LAST_PRINTING_DATE,
            ColumnIdentifier.<Email>newId(
                    Email.PROPERTY_LAST_PRINTING_DATE,
                    n("faxtomail.demandeList.table.header.lastPrintingDate"),
                    n("faxtomail.demandeList.table.header.lastPrintingDate.tip")));

    };

    public DemandeTableModel(TableColumnModelExt columnModel, MailField... editableProperties) {

        this.identifiers = Lists.newArrayListWithCapacity(columnModel.getColumnCount());
        for (TableColumn tc : columnModel.getColumns(true)) {
            this.identifiers.add((ColumnIdentifier<DemandeUIModel>) tc.getIdentifier());
        }

        List<ColumnIdentifier> nonEditableColumns = new ArrayList<ColumnIdentifier>(COLUMN_IDENTIFIERS.values());
        for (MailField editableProperty : editableProperties) {
            nonEditableColumns.remove(COLUMN_IDENTIFIERS.get(editableProperty));
        }
        setNoneEditableCols(nonEditableColumns.toArray(new ColumnIdentifier[nonEditableColumns.size()]));
    }

    public DemandeUIModel createNewRow() {
        return new DemandeUIModel();
    }

    /**
     * Data in the model.
     *
     * @since 0.2
     */
    protected List<DemandeUIModel> rows;

    /**
     * Set of non editable columns.
     *
     * @since 0.2
     */
    protected Set<ColumnIdentifier<?>> noneEditableCols;

    /**
     * Identifiers of columns (in initial order).
     *
     * @since 1.1
     */
    protected final List<ColumnIdentifier<DemandeUIModel>> identifiers;

    public final List<DemandeUIModel> getRows() {
        return rows;
    }

    public final void setRows(List<DemandeUIModel> data) {

        // can't accept a empty data list
        Preconditions.checkNotNull(data, "Data list can not be null.");

        this.rows = null;
        if (log.isDebugEnabled()) {
            log.debug("Set " + data.size() + " row(s) in table model " + this);
        }
        this.rows = data;
        onRowsChanged(data);
        fireTableDataChanged();
    }

    public final void addNewRow() {
        DemandeUIModel newValue = createNewRow();
        addNewRow(newValue);
    }

    public final void addNewRow(DemandeUIModel newValue) {

        addNewRow(getRowCount(), newValue);
    }

    public final void addNewRow(int rowIndex, DemandeUIModel newValue) {

        Preconditions.checkNotNull(newValue, "Row can not be null.");

        List<DemandeUIModel> data = getRows();
        Preconditions.checkNotNull(data, "Data list can not be null.");

        data.add(rowIndex, newValue);

        onRowAdded(rowIndex, newValue);
        fireTableRowsInserted(rowIndex, rowIndex);
    }

    public final void fireTableRowsInserted(DemandeUIModel newValue) {

        Preconditions.checkNotNull(newValue, "Row can not be null.");

        int rowIndex = getRowIndex(newValue);
        fireTableRowsInserted(rowIndex, rowIndex);
    }

    public final int updateRow(DemandeUIModel row) {
        Preconditions.checkNotNull(row, "Row can not be null.");

        List<DemandeUIModel> data = getRows();
        Preconditions.checkNotNull(data, "Data list can not be null.");

        int rowIndex = data.indexOf(row);

        fireTableRowsUpdated(rowIndex, rowIndex);
        return rowIndex;
    }

    public final DemandeUIModel removeRow(int rowIndex) {
        SwingUtil.ensureRowIndex(this, rowIndex);

        List<DemandeUIModel> data = getRows();

        DemandeUIModel result = data.remove(rowIndex);

        fireTableRowsDeleted(rowIndex, rowIndex);
        return result;
    }

    protected void onRowsChanged(List<DemandeUIModel> data) {
        // by default do nothing
    }

    protected void onRowAdded(int rowIndex, DemandeUIModel newValue) {
        // by default do nothing
    }

    public final int getRowIndex(DemandeUIModel row) {
        int result = rows == null ? -1 : rows.indexOf(row);
        return result;
    }

    // cf #5944 java.lang.ArrayIndexOutOfBoundsException: the rowIndex was 15, but should be int [0,14]
    public final DemandeUIModel getEntry(int rowIndex) {
        SwingUtil.ensureRowIndex(this, rowIndex);
        int rowCount = getRowCount();
        if (rowIndex > rowCount) {
            if (log.isErrorEnabled()) {
                log.error("Error getting entry " + rowIndex + " int table model with " + rowCount + " items");
            }
            return null;
        }
        List<DemandeUIModel> data = getRows();
        DemandeUIModel result = data == null ? null : data.get(rowIndex);
        return result;
    }

    public final void setNoneEditableCols(ColumnIdentifier<?>... noneEditableCols) {
        this.noneEditableCols = Sets.newHashSet(noneEditableCols);
    }

    @Override
    public final int getRowCount() {
        return rows == null ? 0 : rows.size();
    }

    @Override
    public final int getColumnCount() {
        return identifiers.size();
    }

    @Override
    public final Object getValueAt(int rowIndex, int columnIndex) {
        DemandeUIModel entry = getEntry(rowIndex);
        ColumnIdentifier<DemandeUIModel> identifier = getIdentifier(columnIndex);
        Object result = identifier.getValue(entry);
        return result;
    }

    @Override
    public final void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (log.isDebugEnabled()) {
            log.debug("setValueAt " + aValue);
        }
        DemandeUIModel entry = getEntry(rowIndex);
        ColumnIdentifier<DemandeUIModel> identifier = getIdentifier(columnIndex);
        setValueAt(aValue, rowIndex, columnIndex, identifier, entry);
    }

    @Override
    public final boolean isCellEditable(int rowIndex, int columnIndex) {
        ColumnIdentifier<DemandeUIModel> identifier = getIdentifier(columnIndex);
        return isCellEditable(rowIndex, columnIndex, identifier);
    }

    protected void setValueAt(Object aValue,
                              int rowIndex,
                              int columnIndex,
                              ColumnIdentifier<DemandeUIModel> propertyName,
                              DemandeUIModel entry) {
        if (log.isDebugEnabled()) {
            log.debug("setValueAt " + aValue);
        }
        propertyName.setValue(entry, aValue);
    }

    protected boolean isCellEditable(int rowIndex,
                                     int columnIndex,
                                     ColumnIdentifier<DemandeUIModel> propertyName) {
        DemandeUIModel entry = getEntry(rowIndex);


        boolean result = (COLUMN_IDENTIFIERS.get(MailField.ATTACHMENT).equals(propertyName) ||
                          COLUMN_IDENTIFIERS.get(MailField.REPLIES).equals(propertyName) ||
                          (entry != null && entry.isEditable()))
                && !noneEditableCols.contains(propertyName);
        return result;
    }

    public final void fireTableCellUpdated(int rowIndex,
                                           ColumnIdentifier<DemandeUIModel>... identifiers) {
        for (ColumnIdentifier<DemandeUIModel> identifier : identifiers) {
            int columnIndex = this.identifiers.indexOf(identifier);
            fireTableCellUpdated(rowIndex, columnIndex);
        }
    }

    public final void fireTableRowUpdatedShell(Set<DemandeUIModel> shell) {

        int minRowIndex1 = getColumnCount();
        int maxRowIndex1 = 0;

        for (DemandeUIModel r : shell) {
            int rowIndex1 = getRowIndex(r);
            minRowIndex1 = Math.min(minRowIndex1, rowIndex1);
            maxRowIndex1 = Math.max(maxRowIndex1, rowIndex1);
        }
        fireTableRowsUpdated(minRowIndex1, maxRowIndex1);
    }

    protected void collectShell(DemandeUIModel row, Set<DemandeUIModel> collectedRows) {

        // by default just add the incoming row
        collectedRows.add(row);
    }

    @Override
    public String getColumnName(int columnIndex) {
        return getIdentifier(columnIndex).getPropertyName();
    }

    protected ColumnIdentifier<DemandeUIModel> getIdentifier(int columnIndex) {
        ColumnIdentifier<DemandeUIModel> identifier = identifiers.get(columnIndex);
        return identifier;
    }

    public int getColumnIndex(final String propertyName) {
        return Iterables.indexOf(identifiers, new Predicate<ColumnIdentifier<DemandeUIModel>>() {
            @Override
            public boolean apply(ColumnIdentifier<DemandeUIModel> input) {
                return ObjectUtils.equals(propertyName, input.getPropertyName());
            }
        });
    }

    public Pair<Integer, Integer> getCell(DemandeUIModel row, final String propertyName) {

        int rowIndex = getRowIndex(row);
        int columnIndex = getColumnIndex(propertyName);

        Pair<Integer, Integer> cell = Pair.of(rowIndex, columnIndex);

        return cell;
    }

    public void moveUp(DemandeUIModel row) {

        int rowIndex = getRowIndex(row);

        if (log.isInfoEnabled()) {
            log.info("Will move up row of index: " + rowIndex);
        }
        rows.remove(rowIndex);
        rows.add(rowIndex - 1, row);
        fireTableRowsUpdated(rowIndex - 1, rowIndex);

    }

    public void moveDown(DemandeUIModel row) {

        int rowIndex = getRowIndex(row);

        if (log.isInfoEnabled()) {
            log.info("Will move down row of index: " + rowIndex);
        }
        rows.remove(rowIndex);
        rows.add(rowIndex + 1, row);
        fireTableRowsUpdated(rowIndex, rowIndex + 1);

    }

    public boolean isFirstRow(DemandeUIModel row) {

        int rowIndex = getRowIndex(row);
        return rowIndex == 0;

    }

    public boolean isLastRow(DemandeUIModel row) {

        int rowIndex = getRowIndex(row);
        return rowIndex == getRowCount() - 1;

    }

}
