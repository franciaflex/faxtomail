package com.franciaflex.faxtomail.ui.swing.content.reply;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.SigningForDomain;
import com.franciaflex.faxtomail.services.service.ldap.Contact;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.google.common.base.Preconditions;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.File;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class ReplyFormUIModel extends AbstractSerializableBean {

    public static final String PROPERTY_CONTACT = "contact";
    public static final String PROPERTY_TO = "to";
    public static final String PROPERTY_CC = "cc";
    public static final String PROPERTY_CCI = "cci";
    public static final String PROPERTY_FROM = "from";
    public static final String PROPERTY_SUBJECT = "subject";
    public static final String PROPERTY_MESSAGE = "message";
    public static final String PROPERTY_ORIGINAL_DEMAND = "originalDemand";
    public static final String PROPERTY_SIGNING = "signing";
    public static final String PROPERTY_ATTACHMENT = "attachment";
    public static final String PROPERTY_MAX_ATTACHMENT_LENGTH = "maxAttachmentLength";
    public static final String PROPERTY_TOTAL_ATTACHMENT_LENGTH = "totalAttachmentLength";
    public static final String PROPERTY_VALID = "valid";
    public static final String PROPERTY_SENDER_ALLOWED_DOMAINS = "senderAllowedDomains";
    public static final String PROPERTY_SENDER_ALLOWED_ADDRESSES = "senderAllowedAddresses";
    public static final String PROPERTY_READONLY = "readonly";
    public static final String PROPERTY_EDITABLE = "editable";
    public static final String PROPERTY_READ_SENT_DATE = "readSentDate";
    public static final String PROPERTY_FORWARD = "forward";

    protected Contact contact;
    protected String to;
    protected String cc;
    protected String cci;
    protected String from;
    protected String subject;
    protected String message;
    protected DemandeUIModel originalDemand;
    protected SigningForDomain signing;
    protected long maxAttachmentLength = 0;
    protected long totalAttachmentLength = 0;
    protected boolean valid = true;
    protected boolean forward;

    /** Flag pour marquer les champs non editables dans le cas d'une lecture d'une réponse déjà envoyée. */
    protected boolean readonly = false;

    /** Flag pour activer ou non la possibilité de transferer la demande. */
    protected boolean editable = false;
    
    /** Date d'envoi d'une réponse ouverte en lecture seule (peut être {@code null}). */
    protected Date readSentDate;

    protected List<String> senderAllowedDomains;
    protected List<String> senderAllowedAddresses;

    protected Set<ReplyAttachmentModel> attachments = new HashSet<ReplyAttachmentModel>();
    protected Set<ReplyAttachmentModel> availableAttachments = new HashSet<ReplyAttachmentModel>();

    protected File lastVisitedDirectory = FileUtils.getUserDirectory();

    protected Binder<ReplyFormUIModel, ReplyFormUIModel> copyBinder = BinderFactory.newBinder(ReplyFormUIModel.class);

    public void fromModel(ReplyFormUIModel other) {
        copyBinder.copyExcluding(other, this, PROPERTY_ORIGINAL_DEMAND);
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        Object oldValue = getContact();
        this.contact = contact;
        firePropertyChange(PROPERTY_CONTACT, oldValue, contact);
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        Object oldValue = getTo();
        this.to = to;
        firePropertyChange(PROPERTY_TO, oldValue, to);
    }

    public String getCc() {
        return cc;
    }
    public void setCc(String cc) {
        Object oldValue = this.cc;
        this.cc = cc;
        firePropertyChange(PROPERTY_CC, oldValue, cc);
    }

    public String getCci() {
        return cci;
    }

    public void setCci(String cci) {
        Object oldValue = this.cci;
        this.cci = cci;
        firePropertyChange(PROPERTY_CCI, oldValue, cci);
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        Object oldValue = getFrom();
        this.from = from;
        firePropertyChange(PROPERTY_FROM, oldValue, from);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        Object oldValue = getSubject();
        this.subject = subject;
        firePropertyChange(PROPERTY_SUBJECT, oldValue, subject);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        Object oldValue = getMessage();
        this.message = message;
        firePropertyChange(PROPERTY_MESSAGE, oldValue, message);
    }

    public DemandeUIModel getOriginalDemand() {
        return originalDemand;
    }

    public void setOriginalDemand(DemandeUIModel originalDemand) {
        Object oldValue = getOriginalDemand();
        this.originalDemand = originalDemand;
        firePropertyChange(PROPERTY_ORIGINAL_DEMAND, oldValue, originalDemand);
    }

    public SigningForDomain getSigning() {
        return signing;
    }

    public void setSigning(SigningForDomain signing) {
        Object oldValue = getSigning();
        this.signing = signing;
        firePropertyChange(PROPERTY_SIGNING, oldValue, signing);
    }

    public Set<ReplyAttachmentModel> getAttachments() {
        return attachments;
    }

    public void addAttachment(ReplyAttachmentModel attachment) {
        attachments.add(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachments());
        setTotalAttachmentLength(totalAttachmentLength + attachment.getLength());
    }

    public void removeAttachment(ReplyAttachmentModel attachment) {
        attachments.remove(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachments());

        setTotalAttachmentLength(totalAttachmentLength - attachment.getLength());
    }
    
    public void setAttachments(Set<ReplyAttachmentModel> attachments) {
        this.attachments = attachments;
        if (availableAttachments != null) {
            availableAttachments.removeAll(attachments);
        }
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachments());
    }

    public Set<ReplyAttachmentModel> getAvailableAttachments() {
        return availableAttachments;
    }

    public void addAvailableAttachment(ReplyAttachmentModel attachment) {
        availableAttachments.add(attachment);
    }

    public void removeAvailableAttachment(ReplyAttachmentModel attachment) {
        availableAttachments.remove(attachment);
    }

    public File getLastVisitedDirectory() {
        return lastVisitedDirectory;
    }

    public void setLastVisitedDirectory(File lastVisitedDirectory) {
        this.lastVisitedDirectory = lastVisitedDirectory;
    }

    public boolean isReadonly() {
        return readonly;
    }

    public void setReadonly(boolean readonly) {
        Object oldValue = isReadonly();
        this.readonly = readonly;
        firePropertyChange(PROPERTY_READONLY, oldValue, readonly);
    }

    public boolean isEditable() {
        return editable;
    }
    
    public void setEditable(boolean editable) {
        Object oldValue = isEditable();
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, oldValue, editable);
    }

    public void setReadSentDate(Date readSentDate) {
        Object oldValue = this.readSentDate;
        this.readSentDate = readSentDate;
        firePropertyChange(PROPERTY_READ_SENT_DATE, oldValue, readSentDate);
    }
    
    public Date getReadSentDate() {
        return readSentDate;
    }

    public long getMaxAttachmentLength() {
        return maxAttachmentLength / 1024;
    }

    public void setMaxAttachmentLength(long maxAttachmentLength) {
        Object oldValue = getMaxAttachmentLength();
        this.maxAttachmentLength = maxAttachmentLength;
        firePropertyChange(PROPERTY_MAX_ATTACHMENT_LENGTH, oldValue, maxAttachmentLength);
    }

    public long getTotalAttachmentLength() {
        return totalAttachmentLength / 1024;
    }

    public void setTotalAttachmentLength(long totalAttachmentLength) {
        Object oldValue = getTotalAttachmentLength();
        this.totalAttachmentLength = totalAttachmentLength;
        firePropertyChange(PROPERTY_TOTAL_ATTACHMENT_LENGTH, oldValue, getTotalAttachmentLength());
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        Object oldValue = isValid();
        this.valid = valid;
        firePropertyChange(PROPERTY_VALID, oldValue, valid);
    }

    /**
     * Appelée par la validation.
     * 
     * Voir le fichier src/main/resources/com/franciaflex/faxtomail/ui/swing/content/reply/ReplyFormUIModel-error-validation.xml
     * 
     * @param field field to validate
     * @return validity
     */
    public boolean isValid(String field) {
        boolean result = true;
        if (PROPERTY_FROM.equals(field)) {
            // valid if the email address is in the list of the authorized addresses
            // or if the domain is in the authorized domains, or if the domain list is empty
            // 20150917 kmorin #7533 Ne plus prendre en compte les noms de domaine dans les adresses expéditeur dans les réponses
            result = isEmailAllowed(getFrom());// || isDomainAllowed(getFrom());
        }
        return result;
    }

    public boolean isForward() {
        return forward;
    }

    public void setForward(boolean forward) {
        Object oldValue = isForward();
        this.forward = forward;
        firePropertyChange(PROPERTY_FORWARD, oldValue, forward);
    }

    /**
     * Test si un email est present dans une liste (sans tenir compte de la casse).
     * 
     * @param mail email to test
     * @return true/false
     */
    protected boolean isEmailAllowed(String mail) {
        Preconditions.checkNotNull(mail);
        List<String> senderAllowedAddresses = getSenderAllowedAddresses();
        if (senderAllowedAddresses != null) {
            for (String address : senderAllowedAddresses) {
                if (mail.equalsIgnoreCase(address)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Test si un email se termine par un des domaines autorisés.
     * 
     * @param mail email to test
     * @return true/false
     * @deprecated avant, l'utilisateur pouvait saisir n'importe quelle adresse expediteur, a condition qu'elle soit sur des domaines definis dans la conf
     */
    @Deprecated
    protected boolean isDomainAllowed(String mail) {
        Preconditions.checkNotNull(mail);
        List<String> senderAllowedDomains = getSenderAllowedDomains();

        if (CollectionUtils.isEmpty(senderAllowedDomains)) {
            return true;
        }
        for (String domain : senderAllowedDomains) {
            if (mail.endsWith(domain)) {
                return true;
            }
        }
        return false;
    }

    // Do not remove the unused parameter, it is just for the validation
    // to bind the totalAttachmentLength property
    public boolean isAttachmentSizeValid(long totalAttachmentLength) {
        boolean valid = maxAttachmentLength == 0 || this.totalAttachmentLength < maxAttachmentLength;
        return valid;
    }

    /**
     * @deprecated avant, l'utilisateur pouvait saisir n'importe quelle adresse expediteur, a condition qu'elle soit sur des domaines definis dans la conf
     */
    @Deprecated
    public List<String> getSenderAllowedDomains() {
        return senderAllowedDomains;
    }

    /**
     * @deprecated avant, l'utilisateur pouvait saisir n'importe quelle adresse expediteur, a condition qu'elle soit sur des domaines definis dans la conf
     */
    @Deprecated
    public void setSenderAllowedDomains(List<String> senderAllowedDomains) {
        Object oldValue = getSenderAllowedDomains();
        this.senderAllowedDomains = senderAllowedDomains;
        firePropertyChange(PROPERTY_SENDER_ALLOWED_DOMAINS, oldValue, senderAllowedDomains);
    }

    public void setSenderAllowedAddresses(List<String> senderAllowedAddresses) {
        Object oldValue = getSenderAllowedAddresses();
        this.senderAllowedAddresses = senderAllowedAddresses;
        firePropertyChange(PROPERTY_SENDER_ALLOWED_ADDRESSES, oldValue, senderAllowedAddresses);
    }

    public List<String> getSenderAllowedAddresses() {
        return senderAllowedAddresses;
    }
}
