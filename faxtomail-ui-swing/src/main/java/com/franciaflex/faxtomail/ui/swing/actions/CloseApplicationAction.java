package com.franciaflex.faxtomail.ui.swing.actions;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.RunFaxToMail;
import com.franciaflex.faxtomail.ui.swing.content.MainUIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * To close FaxToMail Application.
 *
 * @author kmorin - morin@codelutin.com
 */
public class CloseApplicationAction extends AbstractChangeScreenAction {

    protected int exitCode = RunFaxToMail.NORMAL_EXIT_CODE;

    public CloseApplicationAction(MainUIHandler handler) {
        super(handler, false, null);
        setActionDescription(t("faxtomail.action.exit.tip"));
    }

    @Override
    public void doAction() throws Exception {
        super.doAction();
        RunFaxToMail.closeFaxToMail(getHandler(), exitCode);
    }

    @Override
    public void releaseAction() {
        exitCode = RunFaxToMail.NORMAL_EXIT_CODE;
        super.releaseAction();
    }
}
