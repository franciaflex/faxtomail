package com.franciaflex.faxtomail.ui.swing.content.transmit;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import org.jdesktop.beans.AbstractBean;

import java.util.List;

/**
 * Model for mail folder chooser UI.
 * 
 * @author Eric Chatellier
 */
public class MailFolderChooserUIModel extends AbstractBean {

    protected static final String PROPERTY_MAIL_FOLDER = "mailFolder";
    protected static final String PROPERTY_DEMANDE_U_I_MODELS = "demandeUIModels";

    /** Le dossier de destination du mail après validation de l'action .*/
    protected MailFolder mailFolder;

    /** L'ensemble des demandes concernées par le déplacement. */
    protected List<DemandeUIModel> demandeUIModels;

    public void setMailFolder(MailFolder mailFolder) {
        MailFolder oldValue = this.mailFolder;
        this.mailFolder = mailFolder;
        firePropertyChange(PROPERTY_MAIL_FOLDER, oldValue, mailFolder);
    }
    
    public MailFolder getMailFolder() {
        return mailFolder;
    }

    public void setDemandeUIModels(List<DemandeUIModel> demandeUIModels) {
        this.demandeUIModels = demandeUIModels;
        firePropertyChange(PROPERTY_DEMANDE_U_I_MODELS, null, demandeUIModels);
    }

    public List<DemandeUIModel> getDemandeUIModels() {
        return demandeUIModels;
    }
}
