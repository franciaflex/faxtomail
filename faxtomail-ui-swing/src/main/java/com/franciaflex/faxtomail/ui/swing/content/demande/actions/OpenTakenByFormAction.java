package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*-
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.MailFolderService;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.takenby.TakenByFormUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.takenby.TakenByFormUIModel;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

public class OpenTakenByFormAction extends AbstractFaxToMailAction<DemandeListUIModel, DemandeListUI, DemandeListUIHandler> {

    protected TakenByFormUI frameContent;
    protected JFrame frame;

    public OpenTakenByFormAction(DemandeListUIHandler handler) {
        super(handler, false);
    }

    @Override
    public void doAction() throws Exception {
		List<DemandeUIModel> demandsToTake = getModel().getSelectedEmails();
        DemandeUIModel demand = demandsToTake.get(0);

        TakenByFormUIModel model = new TakenByFormUIModel();

        if (demandsToTake.size() == 1) {
            model.setTakenBy(demand.getTakenBy());
        }

        model.setDemandsToTake(demandsToTake);

        MailFolder folder = demand.getMailFolder();

        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            MailFolderService mailFolderService = serviceContext.getMailFolderService();
            List<FaxToMailUser> users = (List<FaxToMailUser>) mailFolderService.getUsersForFolder(folder.getTopiaId());

            model.setUsers(users);
            getUI().setContextValue(model);
        }

		frameContent = new TakenByFormUI(getUI());
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        frame = getHandler().openModalFrame(frameContent,
                t("faxtomail.takenby.modalTitle"),
                new Dimension(300, 40));

        getContext().addPropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (!getContext().isBusy()) {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {

                            bringCurrentDemandToFront();
                            if (frame != null) {
                                frame.toFront();
                                frame = null;
                            }
                        }
                    });
                    getContext().removePropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, this);
                }
            }
        });
    }

}
