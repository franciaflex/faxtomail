package com.franciaflex.faxtomail.ui.swing.content.search.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.SearchFilter;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.search.SearchToGroupUI;
import com.franciaflex.faxtomail.ui.swing.content.search.SearchToGroupUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.search.SearchUIModel;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.util.pagination.PaginationResult;

import javax.swing.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class SearchToGroupAction extends AbstractFaxToMailAction<SearchUIModel, SearchToGroupUI, SearchToGroupUIHandler> {

    public SearchToGroupAction(SearchToGroupUIHandler handler) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.search.tip"));
    }

    @Override
    public void doAction() throws Exception {
        SearchUIModel model = getModel();

        SearchFilter searchFilter = model.toEntity();
        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            PaginationResult<Email> paginationResult = serviceContext.getEmailService().search(searchFilter,
                    getContext().getCurrentUser(), getModel().getPaginationParameter());

            List<Email> emails = paginationResult.getElements();
            List<DemandeUIModel> result = new ArrayList<>();

            Map<String, WaitingState> waitingStateById = Maps.uniqueIndex(getContext().getWaitingStateCache(), TopiaEntities.getTopiaIdFunction());

            for (Email email : emails) {
                DemandeUIModel demand = new DemandeUIModel();
                demand.fromEntityExcluding(email, Sets.newHashSet(Email.PROPERTY_HISTORY));
                demand.recomputeValidRangeRows();

                demand.setValid(handler.isDemandeValid(demand));
                WaitingState waitingState = email.getWaitingState();
                if (waitingState != null) {
                    demand.setWaitingState(waitingStateById.get(waitingState.getTopiaId()));
                }
                result.add(demand);
            }
            model.setResults(result);
            model.setPaginationParameter(paginationResult.getCurrentPage());
            model.setPaginationResult(paginationResult);

            getContext().addPropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (!getContext().isBusy()) {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                final JFrame frame = getUI().getParentContainer(JFrame.class);
                                if (frame != null) {
                                    frame.toFront();
                                }
                            }
                        });
                        getContext().removePropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, this);
                    }
                }
            });
        }
    }

}
