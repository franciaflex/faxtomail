package com.franciaflex.faxtomail.ui.swing.util;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.HasLabel;
import com.google.common.collect.Iterables;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class CheckBoxComboBoxModel<HL extends HasLabel> extends AbstractListModel<HL> implements ComboBoxModel<HL> {

    private List<Object> selection;
    private List<HL> values;

    public CheckBoxComboBoxModel(List<HL> values, List<HL> selection) {
        this.values = new ArrayList<HL>();
        if (values != null) {
            this.values.addAll(values);
        }

        this.selection = new ArrayList<Object>();
        if (selection != null) {
            this.selection.addAll(selection);
        }
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selection = new ArrayList<Object>(selection);
        if (selection.contains(anItem)) {
            selection.remove(anItem);

        // do not add the item if it si not in the universe values
        } else if (values.contains(anItem)) {
            selection.add(anItem);
        }
    }

    @Override
    public Object getSelectedItem() {
        return selection;
    }

    @Override
    public int getSize() {
        return values.size();
    }

    @Override
    public HL getElementAt(int index) {
        return Iterables.get(values, index);
    }
}
