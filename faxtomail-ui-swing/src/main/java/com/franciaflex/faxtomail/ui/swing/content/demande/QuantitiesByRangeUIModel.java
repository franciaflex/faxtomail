package com.franciaflex.faxtomail.ui.swing.content.demande;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import org.jdesktop.beans.AbstractBean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class QuantitiesByRangeUIModel extends AbstractBean {

    public static final String PROPERTY_ROOT_FOLDER = "rootFolder";
    public static final String PROPERTY_ENCOURS_RANGE_ROWS = "enCoursRangeRows";
    public static final String PROPERTY_ENATTENTE_RANGE_ROWS = "enAttenteRangeRows";
    public static final String PROPERTY_TOTALS_RANGE_ROWS = "totalsRangeRows";

    protected MailFolder rootFolder;

    protected List<RangeRowModel> enCoursRangeRows = new ArrayList<>();
    protected List<RangeRowModel> enAttenteRangeRows = new ArrayList<>();
    protected List<RangeRowModel> totalsRangeRows = new ArrayList<>();

    public MailFolder getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(MailFolder rootFolder) {
        Object oldValue = getRootFolder();
        this.rootFolder = rootFolder;
        firePropertyChange(PROPERTY_ROOT_FOLDER, oldValue, rootFolder);
    }

    public List<RangeRowModel> getEnCoursRangeRows() {
        return enCoursRangeRows;
    }

    public void setEnCoursRangeRows(Collection<RangeRowModel> rangeRows) {
        this.enCoursRangeRows = new ArrayList<>(rangeRows);
        firePropertyChange(PROPERTY_ENCOURS_RANGE_ROWS, null, this.enCoursRangeRows);
    }

    public List<RangeRowModel> getEnAttenteRangeRows() {
        return enAttenteRangeRows;
    }

    public void setEnAttenteRangeRows(Collection<RangeRowModel> rangeRows) {
        this.enAttenteRangeRows = new ArrayList<>(rangeRows);
        firePropertyChange(PROPERTY_ENATTENTE_RANGE_ROWS, null, this.enAttenteRangeRows);
    }

    public List<RangeRowModel> getTotalsRangeRows() {
        return totalsRangeRows;
    }

    public void setTotalsRangeRows(Collection<RangeRowModel> rangeRows) {
        this.totalsRangeRows = new ArrayList<>(rangeRows);
        firePropertyChange(PROPERTY_TOTALS_RANGE_ROWS, null, this.totalsRangeRows);
    }
}
