package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.SigningForDomain;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandesUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUI;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUIModel;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import jaxx.runtime.JAXXUtil;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class SaveAndOpenReplyAllFormAction extends SaveAndOpenModalFrameAction<ReplyFormUI> {

    public SaveAndOpenReplyAllFormAction(DemandesUIHandler handler) {
        super(handler);
        setTakeIfNotTaken(true);
    }

    @Override
    public String getTitle() {
        return t("faxtomail.reply.title", getModel().getTitle());
    }

    @Override
    public Dimension getDimension() {
        return new Dimension(800, 600);
    }

    @Override
    public void doAction() throws Exception {
        super.doAction();

        frameContent = new ReplyFormUI(getUI());
        ReplyFormUIModel model = frameContent.getModel();
        DemandeUIModel currentDemand = getModel();

        SigningForDomain signingForDomain = null;
        if (!currentDemand.isFax() && currentDemand.getRecipient() != null) {
            try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
                signingForDomain = serviceContext.getConfigurationService().getSigningForEmailAddress(currentDemand.getRecipient()).orNull();
            }
        }
        model.setSigning(signingForDomain);
        model.setForward(false);
        model.setOriginalDemand(currentDemand);

        String sender = JAXXUtil.getStringValue(currentDemand.getSender());
        if (currentDemand.isFax()) {
            MailFolder folder = currentDemand.getMailFolder();
            sender = FaxToMailServiceUtils.addFaxDomainToFaxNumber(sender, folder);
        }
        model.setTo(sender);
        String cc = currentDemand.getToRecipients();
        cc = cc.substring(cc.indexOf(";") + 1);;
        if (StringUtils.isNotBlank(currentDemand.getCcRecipients())) {
            cc = cc + ";" + currentDemand.getCcRecipients();
        };
        model.setCc(cc);

        FaxToMailUIUtil.setFromOnReply(currentDemand, model, sender, getConfig());

        frameContent.getHandler().addReferences();
    }
}
