package com.franciaflex.faxtomail.ui.swing.util;

/*-
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jaxx.runtime.validator.swing.SwingValidatorMessageWidget;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FaxToMailValidatorMessageWidget extends SwingValidatorMessageWidget implements CloseableUI {

    private static final long serialVersionUID = 5769683678417660752L;

    @Override
    public boolean quitUI() {
        popup.dispose();
        return true;
    }
}
