package com.franciaflex.faxtomail.ui.swing.util;

/*-
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIModel;
import jaxx.runtime.swing.session.State;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 2.4
 */
public class DemandeListUISwingSessionState implements State {

    protected MailField orderByMailField = MailField.RECEPTION_DATE;
    protected boolean orderDesc = false;

    public DemandeListUISwingSessionState() {
    }

    public DemandeListUISwingSessionState(MailField orderByMailField, boolean orderDesc) {
        this.orderByMailField = orderByMailField;
        this.orderDesc = orderDesc;
    }

    public MailField getOrderByMailField() {
        return orderByMailField;
    }

    public void setOrderByMailField(MailField orderByMailField) {
        this.orderByMailField = orderByMailField;
    }

    public boolean isOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(boolean orderDesc) {
        this.orderDesc = orderDesc;
    }

    protected DemandeListUI checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof DemandeListUI)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (DemandeListUI) o;
    }

    @Override
    public State getState(Object o) {
        DemandeListUI ui = checkComponent(o);
        DemandeListUIModel model = ui.getModel();
        DemandeListUISwingSessionState result = new DemandeListUISwingSessionState();

        result.setOrderByMailField(model.getOrderByMailField());
        result.setOrderDesc(model.isOrderDesc());

        return result;
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof DemandeListUISwingSessionState)) {
            throw new IllegalArgumentException("invalid state");
        }

        DemandeListUI ui = checkComponent(o);
        DemandeListUIModel model = ui.getModel();
        DemandeListUISwingSessionState demandeListUISwingSessionState = (DemandeListUISwingSessionState) state;
        model.setOrderByMailField(demandeListUISwingSessionState.getOrderByMailField());
        model.setOrderDesc(demandeListUISwingSessionState.isOrderDesc());
    }
}
