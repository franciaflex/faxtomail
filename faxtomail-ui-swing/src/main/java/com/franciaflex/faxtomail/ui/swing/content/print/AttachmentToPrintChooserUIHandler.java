package com.franciaflex.faxtomail.ui.swing.content.print;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.ui.swing.actions.PrintOnDefaultPrinterAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class AttachmentToPrintChooserUIHandler extends AbstractFaxToMailUIHandler<AttachmentToPrintChooserUIModel, AttachmentToPrintChooserUI>
                                                implements Cancelable {

    private static final Log log = LogFactory.getLog(AttachmentToPrintChooserUIHandler.class);

    private boolean printInlineAttachment;

    @Override
    public void afterInit(AttachmentToPrintChooserUI attachmentToPrintChooserUI) {
        initUI(attachmentToPrintChooserUI);

        AttachmentToPrintChooserUIModel model = getModel();
        DemandeUIModel demand = model.getDemand();

        MailFolder folder = demand.getMailFolder();
        while (folder.getParent() != null && folder.getPrintInlineAttachments() == null) {
            folder = folder.getParent();
        }
        printInlineAttachment = Boolean.TRUE.equals(folder.getPrintInlineAttachments());

        // add demand details
        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            EmailService emailService = serviceContext.getEmailService();
            Email email = getModel().getDemand().toEntity();
            final AttachmentFile demandDetailAttachment = emailService.getEmailDetailAsAttachment(email);

            JCheckBox checkBox = new JCheckBox(demandDetailAttachment.getFilename(), true);
            ui.getAttachmentPanel().add(checkBox);

            // add attached files
            getModel().addAttachmentToPrint(demandDetailAttachment);
            checkBox.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    log.info("state changed " + e.getStateChange());
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        getModel().addAttachmentToPrint(demandDetailAttachment);
                    } else {
                        getModel().removeAttachmentToPrint(demandDetailAttachment);
                    }
                }
            });
        } catch(IOException eee) {
            log.error("Error adding demand details",eee);
        }

        for (Attachment attachment : demand.getAttachment()) {

            if (attachment.getOriginalFile() == null) {
                FaxToMailUIUtil.forceAttachmentFileLoading(getContext(), attachment);
            }
            createCheckBox(attachment, attachment.getOriginalFile());

            final AttachmentFile file = attachment.getEditedFile();
            if (file != null) {
                createCheckBox(attachment, file);
            }
        }
    }

    protected void createCheckBox(Attachment attachment, final AttachmentFile attachmentFile) {
        if (attachmentFile == null) {
            return;
        }
        String attachmentName = attachmentFile.getFilename();
        boolean printable = FaxToMailUIUtil.isFileTypeEditable(attachmentName);
        boolean selected = (printInlineAttachment || !attachment.isInlineAttachment()) && printable;
        JCheckBox checkBox = new JCheckBox(attachmentName, selected);
        checkBox.setEnabled(printable);
        ui.getAttachmentPanel().add(checkBox);

        if (printable) {
            getModel().addAttachmentToPrint(attachmentFile);

            checkBox.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        getModel().addAttachmentToPrint(attachmentFile);
                    } else {
                        getModel().removeAttachmentToPrint(attachmentFile);
                    }
                }
            });
        }
    }

    @Override
    public void onCloseUI() {
    }

    @Override
    public SwingValidator<AttachmentToPrintChooserUIModel> getValidator() {
        return null;
    }

    @Override
    public void cancel() {
        closeFrame();
    }

    @Override
    protected JComponent getComponentToFocus() {
        return ui.getAttachmentPanel();
    }

    @Override
    public Component getTopestUI() {
        return getUI();
    }

    public void print() {
        Multimap<DemandeUIModel, AttachmentFile> attachmentToPrints = HashMultimap.create();
        attachmentToPrints.putAll(getModel().getDemand(), getModel().getAttachmentsToPrint());

        PrintOnDefaultPrinterAction action = new PrintOnDefaultPrinterAction(this,
                                             attachmentToPrints,
                                             false,
                                             false,
                                             false) {

            @Override
            public void postSuccessAction() {
                super.postSuccessAction();
                closeFrame();
            }
        };
        getContext().getActionEngine().runAction(action);
    }

}
