package com.franciaflex.faxtomail.ui.swing.util;

/*-
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import jaxx.runtime.swing.session.State;

import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class FolderTreeSwingSessionState implements State {

    protected List<String> expandedFolderIds;

    public FolderTreeSwingSessionState() {
    }

    public List<String> getExpandedFolderIds() {
        return expandedFolderIds;
    }

    public void setExpandedFolderIds(List<String> expandedFolderIds) {
        this.expandedFolderIds = expandedFolderIds;
    }

    protected JTree checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof JTree)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (JTree) o;
    }

    @Override
    public State getState(Object o) {
        JTree tree = checkComponent(o);
        FolderTreeSwingSessionState result = new FolderTreeSwingSessionState();

        List<String> expandedFolderIds = new ArrayList<>();
        for (int i = 0 ; i < tree.getRowCount() ; i++) {
            if (tree.isExpanded(i)) {
                TreePath path = tree.getPathForRow(i);
                FolderTreeNode folderNode = (FolderTreeNode) path.getLastPathComponent();
                MailFolder mailFolder = folderNode.getMailFolder();
                if (mailFolder != null) {
                    expandedFolderIds.add(mailFolder.getTopiaId());
                }
            }
        }
        result.setExpandedFolderIds(expandedFolderIds);

        return result;
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof FolderTreeSwingSessionState)) {
            throw new IllegalArgumentException("invalid state");
        }

        JTree tree = checkComponent(o);
        FolderTreeSwingSessionState jTreeSwingSessionState = (FolderTreeSwingSessionState) state;

        for (int i = 0 ; i < tree.getRowCount() ; i++) {
            TreePath path = tree.getPathForRow(i);
            FolderTreeNode folderNode = (FolderTreeNode) path.getLastPathComponent();
            MailFolder mailFolder = folderNode.getMailFolder();
            if (mailFolder != null && jTreeSwingSessionState.getExpandedFolderIds().contains(mailFolder.getTopiaId())) {
                tree.expandRow(i);
            } else {
                tree.collapseRow(i);
            }
        }
    }

}
