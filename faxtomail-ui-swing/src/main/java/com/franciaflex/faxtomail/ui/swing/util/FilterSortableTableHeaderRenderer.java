package com.franciaflex.faxtomail.ui.swing.util;

/*-
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIModel;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.CompoundIcon;
import jaxx.runtime.swing.table.filter.FilterTableHeaderRenderer;
import jaxx.runtime.swing.table.filter.TableFilter;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import java.awt.Component;
import java.awt.Image;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 2.4
 */
public class FilterSortableTableHeaderRenderer extends FilterTableHeaderRenderer {

    protected final DemandeListUIModel demandeListUIModel;

    protected final ImageIcon sortAscIcon;
    protected final ImageIcon sortDescIcon;

    public FilterSortableTableHeaderRenderer(TableFilter<?> tableFilter, DemandeListUIModel demandeListUIModel) {
        super(tableFilter);
        this.demandeListUIModel = demandeListUIModel;

        ImageIcon icon = SwingUtil.createImageIcon("sort_table_asc.png");
        sortAscIcon = new ImageIcon(icon.getImage().getScaledInstance(12, 12, Image.SCALE_SMOOTH));

        icon = SwingUtil.createImageIcon("sort_table_desc.png");
        sortDescIcon = new ImageIcon(icon.getImage().getScaledInstance(12, 12, Image.SCALE_SMOOTH));
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                   boolean hasFocus, int row, int column) {

        TableColumnModelExt colModel = (TableColumnModelExt) table.getColumnModel();
        TableColumnExt columnExt = colModel.getColumnExt(column);
        MailField mailField = DemandeTableModel.COLUMN_IDENTIFIERS.inverse().get(columnExt.getIdentifier());

        boolean focusCell = hasFocus && mailField.getOrderProperty() != null;
        final JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, focusCell, row, column);

        if (demandeListUIModel.getOrderByMailField() != null
            && demandeListUIModel.getOrderByMailField().equals(mailField)) {

            Icon originalIcon = label.getIcon();
            Icon sortIcon;
            if (demandeListUIModel.isOrderDesc()) {
                sortIcon = sortDescIcon;
            } else {
                sortIcon = sortAscIcon;
            }
            if (originalIcon == null) {
                label.setIcon(sortIcon);
            } else {
                label.setIcon(new CompoundIcon(sortIcon, originalIcon));
            }
            label.setHorizontalTextPosition(SwingConstants.TRAILING);
        }

        return label;
    }

}
