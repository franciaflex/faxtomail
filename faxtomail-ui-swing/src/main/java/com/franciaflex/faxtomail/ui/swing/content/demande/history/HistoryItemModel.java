package com.franciaflex.faxtomail.ui.swing.content.demande.history;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.History;
import com.franciaflex.faxtomail.persistence.entities.HistoryImpl;
import com.franciaflex.faxtomail.persistence.entities.HistoryType;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailBeanUIModel;
import com.google.common.collect.Sets;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Date;
import java.util.Set;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class HistoryItemModel extends AbstractFaxToMailBeanUIModel<History, HistoryItemModel> {

    public static final String PROPERTY_FIELDS = "fields";

    protected static Binder<HistoryItemModel, History> toBeanBinder =
            BinderFactory.newBinder(HistoryItemModel.class,
                                    History.class);

    protected static Binder<History, HistoryItemModel> fromBeanBinder =
            BinderFactory.newBinder(History.class, HistoryItemModel.class);

    protected final History editObject = new HistoryImpl();

    public HistoryItemModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    public HistoryType getType() {
        return editObject.getType();
    }

    public void setType(HistoryType type) {
        Object oldValue = editObject.getType();
        editObject.setType(type);
        firePropertyChanged(History.PROPERTY_TYPE, oldValue, type);
    }

    public void setFields(Set<String> fields) {
        editObject.setFields(fields != null ? Sets.newLinkedHashSet(fields) : null);
        firePropertyChanged(PROPERTY_FIELDS, null, fields);
    }

    public Set<String> getFields() {
        return editObject.getFields();
    }

    public Date getModificationDate() {
        return editObject.getModificationDate();
    }

    public void setModificationDate(Date modificationDate) {
        Object oldValue = editObject.getModificationDate();
        editObject.setModificationDate(modificationDate);
        firePropertyChanged(History.PROPERTY_MODIFICATION_DATE, oldValue, modificationDate);
    }

    public FaxToMailUser getFaxToMailUser() {
        return editObject.getFaxToMailUser();
    }

    public void setFaxToMailUser(FaxToMailUser faxToMailUser) {
        Object oldValue = editObject.getFaxToMailUser();
        editObject.setFaxToMailUser(faxToMailUser);
        firePropertyChanged(History.PROPERTY_FAX_TO_MAIL_USER, oldValue, faxToMailUser);
    }

    @Override
    protected History newEntity() {
        return new HistoryImpl();
    }
}
