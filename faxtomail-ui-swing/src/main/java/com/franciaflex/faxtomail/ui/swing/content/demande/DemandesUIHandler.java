package com.franciaflex.faxtomail.ui.swing.content.demande;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Configuration;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.MailAction;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.actions.*;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailBeanUIModel;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;
import org.nuiton.jaxx.application.swing.ApplicationUI;
import org.nuiton.jaxx.application.swing.action.AbstractApplicationAction;
import org.nuiton.jaxx.application.swing.tab.CustomTab;
import org.nuiton.jaxx.application.swing.tab.TabContainerHandler;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;
import org.nuiton.jaxx.application.swing.tab.TabHandler;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.DefaultSingleSelectionModel;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class DemandesUIHandler extends AbstractFaxToMailUIHandler<DemandesUIModel, DemandesUI> implements TabContainerHandler, CloseableUI {

    private static final Log log = LogFactory.getLog(DemandesUIHandler.class);

    protected Configuration config;

    protected final PropertyChangeListener demandEnabledAndValidListener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            if (evt.getPropertyName().equals(AbstractFaxToMailBeanUIModel.PROPERTY_VALID)
                    || evt.getPropertyName().equals(DemandeUIModel.PROPERTY_EDITABLE)
                    || evt.getPropertyName().equals(Email.PROPERTY_ARCHIVE_DATE)) {

                DemandesUI ui = getUI();
                ui.processDataBinding(DemandesUI.BINDING_ARCHIVE_BUTTON_ENABLED);
                ui.processDataBinding(DemandesUI.BINDING_GROUP_BUTTON_ENABLED);
                ui.processDataBinding(DemandesUI.BINDING_REPLY_BUTTON_ENABLED);
                ui.processDataBinding(DemandesUI.BINDING_SAVE_BUTTON_ENABLED);
                ui.processDataBinding(DemandesUI.BINDING_TRANSMIT_BUTTON_ENABLED);

            }

            if (evt.getPropertyName().equals(AbstractFaxToMailBeanUIModel.PROPERTY_VALID)
                    || evt.getPropertyName().equals(DemandeUIModel.PROPERTY_EDITABLE)
                    || evt.getPropertyName().equals(Email.PROPERTY_WAITING_STATE)) {

                updateButtonEnability();
            }
        }
    };

    @Override
    public void beforeInit(DemandesUI ui) {
        super.beforeInit(ui);

        DemandeUIModel currentEmail = getContext().getCurrentEmail();
        currentEmail.setCloseable(false);

        DemandesUIModel model = new DemandesUIModel();

        try(FaxToMailServiceContext faxToMailServiceContext = getContext().newServiceContext()) {
            config = faxToMailServiceContext.getConfigurationService().getConfiguration();
        } catch (IOException eee) {
            log.error("Error getting config", eee);
        }

        this.ui.setContextValue(model);
    }

    @Override
    public void afterInit(DemandesUI ui) {
        initUI(ui);

        DemandesUIModel model = getModel();

        model.addDemandsListener(new DemandsListener() {
            @Override
            public void onDemandAdded(DemandeUIModel demand, int index, boolean added) {

                JTabbedPane tabPanel = getTabPanel();
                if (added) {
                    DemandeUI demandeUI = new DemandeUI(getUI(), demand);
                    demandeUI.setName("demandPanel" + index);
                    getContext().getSwingSession().add(demandeUI, true);
                    demand.addPropertyChangeListener(demandEnabledAndValidListener);
                    tabPanel.add(demandeUI);
                    setCustomTab(index, demand);
                }

                tabPanel.setSelectedIndex(index);
            }

            @Override
            public void onDemandRemoved(DemandeUIModel demand, int index) {
                demand.removePropertyChangeListener(demandEnabledAndValidListener);
            }
        });

        DemandeUIModel currentEmail = getContext().getCurrentEmail();
        model.addDemand(currentEmail);

    }

    protected void updateButtonEnability() {
        DemandeUIModel model = getModel().getCurrentDemand();
        boolean editable = model.isEditable();
        boolean archived = model.getArchiveDate() != null;

        boolean transmitEnabled = editable && isActionEnabled(model, MailAction.TRANSMIT);
        getModel().setTransmitEnabled(transmitEnabled);

        boolean archiveEnabled = editable && isActionEnabled(model, MailAction.ARCHIVE);
        getModel().setArchiveEnabled(archiveEnabled);

        boolean groupEnabled = isActionEnabled(model, MailAction.GROUP);
        getModel().setGroupEnabled(groupEnabled);

        boolean printEnabled = isActionEnabled(model, MailAction.PRINT);
        getModel().setPrintEnabled(printEnabled);

        MailFolder folderWithShowReplyAction = model.getMailFolder();
        while (folderWithShowReplyAction.getParent() != null
               && folderWithShowReplyAction.getShowReplyAction() == null) {
            folderWithShowReplyAction = folderWithShowReplyAction.getParent();
        }
        getModel().setReplyActivated(Boolean.TRUE.equals(folderWithShowReplyAction.getShowReplyAction()));

        boolean replyEnabled = (editable || archived) && isActionEnabled(model, MailAction.REPLY);
        getModel().setReplyEnabled(replyEnabled);

        MailFolder folderWithShowForwardAction = model.getMailFolder();
        while (folderWithShowForwardAction.getParent() != null
               && folderWithShowForwardAction.getShowForwardAction() == null) {
            folderWithShowForwardAction = folderWithShowForwardAction.getParent();
        }
        getModel().setForwardActivated(Boolean.TRUE.equals(folderWithShowForwardAction.getShowForwardAction()));

        boolean forwardEnabled = (editable || archived) && isActionEnabled(model, MailAction.FORWARD);
        getModel().setForwardEnabled(forwardEnabled);
    }

    @Override
    public boolean quitUI() {
        closeButtonPopups();

        getContext().closeSecondaryFrame();

        JTabbedPane tabPanel = getTabPanel();
        int tabCount = tabPanel.getTabCount();
        boolean quit = true;
        int i = 0;
        while (quit && i < tabCount) {
            tabPanel.setSelectedIndex(i);
            DemandeUIHandler tabHandler = (DemandeUIHandler) getTabHandler(i++);
            quit = tabHandler.quitUI();
        }
        ui.getDemandGroupHighlightDialog().dispose();
        ui.getValidatorMessageWidget().quitUI();
        return quit;
    }

    @Override
    public void onCloseUI() {
//        closeButtonPopups();
        int tabCount = getTabPanel().getTabCount();
        for (int i = 0 ; i < tabCount ; i++) {
            DemandeUIHandler tabHandler = (DemandeUIHandler) getTabHandler(i);
            tabHandler.getModel().removePropertyChangeListener(demandEnabledAndValidListener);
            tabHandler.onCloseUI();
        }

        ui.getDemandGroupButton().onCloseUI();

        getContext().getMainUI().getHandler().reloadDemandList();
    }

    @Override
    public SwingValidator<DemandesUIModel> getValidator() {
        return null;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    @Override
    public Component getTopestUI() {
        return getUI();
    }

    /**
     * Returns the tab handler of the tab i.
     *
     * @param index the index of the tab
     * @return the tab handler of the index i if the handler implements
     * the {@link org.nuiton.jaxx.application.swing.tab.TabHandler} interface,
     * <code>null</code> otherwise
     */
    @Override
    public TabHandler getTabHandler(int index) {
        TabHandler tabHandler = null;
        JTabbedPane tabPanel = getTabPanel();
        if (index >= 0 && index < tabPanel.getTabCount()) {
            Component tab = tabPanel.getComponentAt(index);
            if (ApplicationUI.class.isInstance(tab)) {
                ApplicationUI faxToMailTab = (ApplicationUI) tabPanel.getComponentAt(index);
                AbstractApplicationUIHandler handler = faxToMailTab.getHandler();
                if (TabHandler.class.isInstance(handler)) {
                    tabHandler = (TabHandler) handler;
                }
            }
        }
        return tabHandler;
    }

    @Override
    public void setCustomTab(int index, TabContentModel model) {
        JTabbedPane tabPanel = getTabPanel();
        tabPanel.setTabComponentAt(index, new CustomTab(model, this));
    }

    @Override
    public boolean removeTab(int i) {
        TabHandler tabHandler = getTabHandler(i);
        boolean remove = tabHandler.onRemoveTab();
        if (remove) {
            getTabPanel().removeTabAt(i);
            getModel().removeDemand(i);
        }
        return remove;
    }

    @Override
    protected void initUI(DemandesUI ui) {
        super.initUI(ui);
        init();
    }

    @Override
    public JTabbedPane getTabPanel() {
        return getUI().getDemandsTabPane();
    }

    @Override
    public void init() {
        getTabPanel().setModel(new DefaultSingleSelectionModel() {

            private static final long serialVersionUID = 1L;

            @Override
            public void setSelectedIndex(int index) {
                int currentIndex = getTabPanel().getSelectedIndex();
                boolean mustChangeTab = onTabChanged(currentIndex, index);

                if (mustChangeTab) {
                    super.setSelectedIndex(index);
                }
                updateButtonEnability();
            }

        });
    }

    @Override
    public boolean onTabChanged(int currentIndex, int newIndex) {
        boolean change = true;
        if (currentIndex != newIndex) {
            TabHandler handler = getTabHandler(currentIndex);
            if (handler != null) {
                change = handler.onHideTab(currentIndex, newIndex);
            }
        }
        if (change) {
            getModel().setCurrentIndex(newIndex);
            getContext().setCurrentEmail(getModel().getCurrentDemand());

            TabHandler handler = getTabHandler(newIndex);
            if (handler != null) {
                handler.onShowTab(currentIndex, newIndex);
            }

        }
        return change;
    }

    public void registerValidator(SwingValidator validator) {
        ui.getValidatorMessageWidget().registerValidator(validator);
    }

    public void clearValidators() {
        ui.getValidatorMessageWidget().clearValidators();
    }

    public void closeButtonPopups() {
        getUI().getDemandGroupHighlightDialog().setVisible(false);

        DemandeUI demandeUI = (DemandeUI) getTabPanel().getSelectedComponent();
        demandeUI.getHandler().closeButtonPopups();
        getUI().getDemandGroupButton().setSelected(false);
    }

    public void save() {
        closeButtonPopups();
        getContext().closeSecondaryFrame();
        DemandeUI demandeUI = (DemandeUI) getTabPanel().getSelectedComponent();
        SaveDemandeAndExitAction action = new SaveDemandeAndExitAction(demandeUI.getHandler());
        action.setTakeIfNotTaken(true);
        getContext().getActionEngine().runAction(action);
    }

    public void transmitDemande() {
        closeButtonPopups();
        runAction(SaveAndOpenMailFolderChooserAction.class);
    }

    public void print() {
        closeButtonPopups();
        runAction(SaveAndOpenChooseAttachmentToPrintAction.class);
    }

    public void archive() {
        closeButtonPopups();
        DemandeUI demandeUI = (DemandeUI) getTabPanel().getSelectedComponent();
        ArchiveAction action = new ArchiveAction(demandeUI.getHandler());
        getContext().getActionEngine().runAction(action);
    }

    public void reply() {
        closeButtonPopups();
        runAction(SaveAndOpenReplyFormAction.class);
    }

    public void replyAll() {
        closeButtonPopups();
        runAction(SaveAndOpenReplyAllFormAction.class);
    }

    public void forward() {
        closeButtonPopups();
        runAction(SaveAndOpenForwardFormAction.class);
    }

    public void group() {
        closeButtonPopups();
        runAction(SaveAndOpenSearchToGroupAction.class);
    }

    public DemandeUIHandler getCurrentDemandHandler() {
        DemandeUI demandeUI = (DemandeUI) getTabPanel().getSelectedComponent();
        return demandeUI.getHandler();
    }

    public void runAction(Class<? extends AbstractApplicationAction> actionClass) {
        AbstractApplicationAction action = getContext().getActionFactory().createLogicAction(this, actionClass);
        getContext().getActionEngine().runAction(action);
    }

    public void showDemandGroupHighlightDialog() {
        if (getModel().getCurrentDemand().getGroupedDemandes().size() > 1) {
            getUI().getDemandGroupHighlightDialog().setVisible(true);
        }
    }
}
