package com.franciaflex.faxtomail.ui.swing.content.pdfeditor.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentEditorUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorCrossUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorHighlighterUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorLineUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorNoteUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorStampImageUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorStampTextUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorUIModel;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.JAXXUtil;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class GenerateAnnotatedAttachmentAction extends AbstractFaxToMailAction<PDFEditorUIModel, PDFEditorUI, PDFEditorUIHandler> {

    private boolean closeFrameWhenDone = true;

    public GenerateAnnotatedAttachmentAction(PDFEditorUIHandler handler) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.generateAnnotatedAttachment.tip"));
    }

    public void setCloseFrameWhenDone(boolean closeFrameWhenDone) {
        this.closeFrameWhenDone = closeFrameWhenDone;
    }

    @Override
    public void doAction() throws Exception {

        PDFEditorUIModel model = getModel();
        AttachmentFile attachmentFile = model.getNotNullFile();
        File file = attachmentFile.getFile();

        InputStream inputStream = new FileInputStream(file);

        // to save protected pdfs
        PdfReader.unethicalreading = true;
        PdfReader pdfReader = new PdfReader(inputStream);

        //File target = new File(FileUtils.getTempDirectory(), getModel().getOriginalFile().getName() + "-annoté.pdf");
        File target = File.createTempFile("faxtomail-", ".tmp");
        target.deleteOnExit();

        FileOutputStream fos = new FileOutputStream(target);
        PdfStamper pdfStamper = new PdfStamper(pdfReader, fos);

        int pageNb = model.getPages().length;
        float zoom = model.getZoom();
        int rotation = model.getRotation();

        for (int i = 1 ; i <= pageNb ; i++) {
            PdfContentByte cb = pdfStamper.getOverContent(i);

            PDFEditorUIModel.Page page = model.getPage(i);

            for (PDFEditorNoteUI note : page.getNotes()) {
                addNoteToPdf(zoom, rotation, cb, note);
            }

            for (PDFEditorCrossUI panel : page.getCrosses()) {
                addCrossToPdf(zoom, rotation, cb, panel);
            }

            for (PDFEditorLineUI panel : page.getLines()) {
                addLineToPdf(zoom, rotation, cb, panel);
            }

            for (PDFEditorHighlighterUI panel : page.getHighlighters()) {
                addHighlightToPdf(zoom, rotation, cb, panel);
            }

            for (PDFEditorStampTextUI panel : page.getTextStamps()) {
                addTextStampToPdf(zoom, rotation, cb, panel);
            }

            for (PDFEditorStampImageUI panel : page.getImageStamps()) {
                addImageStampToPdf(zoom, rotation, cb, panel);
            }
        }

        pdfStamper.close();
        pdfReader.close();

        // convert content to blob
        InputStream is = new BufferedInputStream(new FileInputStream(target));
        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            AttachmentFile attachmentFileNew = serviceContext.getEmailService().getAttachmentFileFromStream(is);
            attachmentFileNew.setRotation(rotation);
            String fileName = model.getOriginalFile().getFilename();
            attachmentFileNew.setFilename(FaxToMailUIUtil.getEditedFileName(fileName));
            model.setEditedFile(attachmentFileNew);
        }
    }

    protected void addHighlightToPdf(float zoom, int rotation, PdfContentByte cb, PDFEditorHighlighterUI panel) {
        Coordinates coordinates = computeCoordinates(panel, rotation, zoom);

        cb.saveState();
        PdfGState gs1 = new PdfGState();
        gs1.setFillOpacity(0.4f);
        cb.setGState(gs1);

        cb.setColorFill(BaseColor.YELLOW);
        cb.rectangle(coordinates.x, coordinates.y, coordinates.width, coordinates.height);
        cb.fill();
        cb.restoreState();
    }

    protected void addLineToPdf(float zoom, int rotation, PdfContentByte cb, PDFEditorLineUI panel) {
        Coordinates coordinates = computeCoordinates(panel, rotation, zoom);
        int width = coordinates.width;
        int height = coordinates.height;
        int x = coordinates.x;
        int y = coordinates.y;

        if (rotation % 180 == 0 ^ !panel.isHorizontal()) {
            y += height / 2;
        } else {
            x += width / 2;
        }

        cb.saveState();
        cb.setColorStroke(BaseColor.BLUE);
        cb.moveTo(x, y);
        if (rotation % 180 == 0 ^ !panel.isHorizontal()) {
            cb.lineTo((float)x + width, y);
        } else {
            cb.lineTo(x, (float)y + height);
        }
        cb.stroke();
        cb.restoreState();
    }

    protected void addCrossToPdf(float zoom, int rotation, PdfContentByte cb, PDFEditorCrossUI panel) {
        Coordinates coordinates = computeCoordinates(panel, rotation, zoom);
        int width = coordinates.width;
        int height = coordinates.height;
        int x = coordinates.x;
        int y = coordinates.y;

        cb.saveState();
        cb.setColorStroke(BaseColor.BLUE);
        cb.moveTo(x, y);
        cb.lineTo((float)x + width, (float)y + height);
        cb.stroke();
        cb.moveTo((float)x + width, y);
        cb.lineTo(x, (float)y + height);
        cb.stroke();
        cb.restoreState();
    }

    protected void addNoteToPdf(float zoom, int rotation, PdfContentByte cb, PDFEditorNoteUI note) throws DocumentException, IOException {
        Coordinates coordinates = computeCoordinates(note, rotation, zoom);
        int width = coordinates.width;
        int height = coordinates.height;
        int x = coordinates.x;
        int y = coordinates.y;

        Insets insets = note.getInsets();

        cb.saveState();
        cb.setColorFill(BaseColor.YELLOW);
        cb.rectangle(x, y, width, height);
        cb.fill();
        cb.restoreState();

        cb.saveState();
        JTextArea textArea = note.getNoteText();
        float fontSize = textArea.getFont().getSize2D() / zoom;
        BaseFont titleFont = BaseFont.createFont(BaseFont.HELVETICA_OBLIQUE, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
        BaseFont font = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);

        String text = note.getText();
        String[] rows = text.split("\n");
        int rowHeight = (int) (((double)textArea.getHeight() / rows.length) / zoom);

        int xxx = x;
        int yyy = y;
        if (rotation == 0) {
            xxx += insets.left;
            yyy += height - rowHeight - insets.top / zoom;

        } else if (rotation == 90) {
            xxx += rowHeight + insets.top / zoom;
            yyy += insets.left;

        } else if (rotation == 180) {
            xxx += width - insets.right / zoom;
            yyy += rowHeight - insets.top / zoom;

        } else if (rotation == 270) {
            xxx += width - rowHeight - insets.left / zoom;
            yyy += height - insets.top / zoom;
        }

        showTextAligned(cb, note.getTitle(), xxx, yyy, rotation, titleFont, fontSize);

        for (String s : rows) {
            if (s.isEmpty()) {
                s = " ";
            }
            if (rotation == 0) {
                yyy -= rowHeight;

            } else if (rotation == 90) {
                xxx += rowHeight;

            } else if (rotation == 180) {
                yyy += rowHeight;

            } else if (rotation == 270) {
                xxx -= rowHeight;
            }
            showTextAligned(cb, s, xxx, yyy, rotation, font, fontSize);
        }

        cb.restoreState();
    }

    protected void addTextStampToPdf(float zoom, int rotation, PdfContentByte cb, PDFEditorStampTextUI stamp) throws DocumentException, IOException {
        Coordinates coordinates = computeCoordinates(stamp, rotation, zoom);
        int width = coordinates.width;
        int height = coordinates.height;
        int x = coordinates.x;
        int y = coordinates.y;

        Insets insets = stamp.getInsets();

        cb.saveState();
        JLabel label = stamp.getStampText();
        float fontSize = label.getFont().getSize2D() / zoom;
        BaseFont font = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);

        String text = stamp.getText();
        String[] rows = text.split("\n");
        int rowHeight = (int) (((double)stamp.getHeight() / rows.length) / zoom);

        int xxx = x;
        int yyy = y;
        if (rotation == 0) {
            xxx += insets.left;
            yyy += height - rowHeight - insets.top / zoom;

        } else if (rotation == 90) {
            xxx += rowHeight + insets.top / zoom;
            yyy += insets.left;

        } else if (rotation == 180) {
            xxx += width - insets.right / zoom;
            yyy += rowHeight - insets.top / zoom;

        } else if (rotation == 270) {
            xxx += width - rowHeight - insets.left / zoom;
            yyy += height - insets.top / zoom;
        }

        for (String s : rows) {
            if (s.isEmpty()) {
                s = " ";
            }
            showTextAligned(cb, s, xxx, yyy, rotation, font, fontSize);

            if (rotation == 0) {
                yyy -= rowHeight;

            } else if (rotation == 90) {
                xxx += rowHeight;

            } else if (rotation == 180) {
                yyy += rowHeight;

            } else if (rotation == 270) {
                xxx -= rowHeight;
            }
        }

        cb.restoreState();
    }

    protected void addImageStampToPdf(float zoom, int rotation, PdfContentByte cb, PDFEditorStampImageUI stamp) throws DocumentException, IOException {
        Coordinates coordinates = computeCoordinates(stamp, rotation, zoom);
        int x = coordinates.x;
        int y = coordinates.y;

        cb.saveState();

        java.awt.Image image = stamp.getImage();
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), 1);
        Graphics gc = bufferedImage.createGraphics();
        gc.drawImage(image, 0, 0, null);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "jpeg", bos);
        byte[] imageByteArray = bos.toByteArray();

        Image stampImage = Image.getInstance(imageByteArray);
        stampImage.setAbsolutePosition(x, y);
        stampImage.setRotationDegrees(rotation);
        cb.addImage(stampImage);

        cb.restoreState();
    }

    protected Coordinates computeCoordinates(Component panel, int rotation, float zoom) {
        Point location = panel.getLocation();

        int width = rotation % 180 == 0 ? panel.getWidth() : panel.getHeight();
        int height = rotation % 180 == 0 ? panel.getHeight() : panel.getWidth();

        width = (int) (width / zoom);
        height = (int) (height / zoom);

        int x = 0;
        int y = 0;

        if (rotation == 0) {
            x = (int) (location.x / zoom);
            y = (int) ((getUI().getContainer().getHeight() - location.y) / zoom) - height;

        } else if (rotation == 180) {
            x = (int) ((getUI().getContainer().getWidth() - location.x) / zoom) - width;
            y = (int) (location.y / zoom);

        } else if (rotation == 90) {
            x = (int) (location.y / zoom);
            y = (int) (location.x / zoom);

        } else if (rotation == 270) {
            x = (int) ((getUI().getContainer().getHeight() - location.y) / zoom) - width;
            y = (int) ((getUI().getContainer().getWidth() - location.x) / zoom) - height;
        }

        return new Coordinates(width, height, x, y);
    }

    protected void showTextAligned(PdfContentByte canvas, String text, float x, float y, float rotation, BaseFont baseFont, float fontSize) {
        canvas.saveState();

        ColumnText ct = new ColumnText(canvas);

        float lly = -1;
        float ury = 2;
        float llx = 0;
        float urx = 20000;

        if (rotation == 0) {
            llx += x;
            lly += y;
            urx += x;
            ury += y;

        } else {
            double alpha = rotation * Math.PI / 180.0;
            float cos = (float)Math.cos(alpha);
            float sin = (float)Math.sin(alpha);
            canvas.concatCTM(cos, sin, -sin, cos, x, y);
        }

        Font font = new Font(baseFont, fontSize);
        ct.setSimpleColumn(new Phrase(text, font), llx, lly, urx, ury, 0, Element.ALIGN_LEFT);
        ct.setAlignment(Element.ALIGN_LEFT);

        try {
            ct.go();
        }
        catch (DocumentException e) {
            throw new ExceptionConverter(e);
        }
        canvas.restoreState();
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        PDFEditorUIModel model = getModel();

        AttachmentEditorUI parentUI = (AttachmentEditorUI) getUI().getContextValue(JAXXContext.class, JAXXUtil.PARENT);
        parentUI.getModel().fireAttachmentEdited(model.toEntity());

        model.setModify(false);

        if (closeFrameWhenDone) {
            getHandler().closeFrame();
        }
    }

    private class Coordinates {
        private int x, y, width, height;

        Coordinates(int width, int height, int x, int y) {
            this.height = height;
            this.width = width;
            this.x = x;
            this.y = y;
        }
    }
}
