package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandesUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.print.AttachmentToPrintChooserUI;

import java.awt.*;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class SaveAndOpenChooseAttachmentToPrintAction extends SaveAndOpenModalFrameAction<AttachmentToPrintChooserUI> {

    public SaveAndOpenChooseAttachmentToPrintAction(DemandesUIHandler handler) {
        super(handler);
    }

    @Override
    public String getTitle() {
        return t("faxtomail.chooseAttachmentToPrint.title");
    }

    @Override
    public Dimension getDimension() {
        return new Dimension(350, 500);
    }

    @Override
    public void doAction() throws Exception {
        MailFolder selectedFolder = getModel().getMailFolder();
        while (selectedFolder.getParent() != null && selectedFolder.getPrintActionEqualTakeAction() == null) {
            selectedFolder = selectedFolder.getParent();
        }
        boolean take = Boolean.TRUE.equals(selectedFolder.getPrintActionEqualTakeAction());
        setTakeIfNotTaken(take);

        super.doAction();

        frameContent = new AttachmentToPrintChooserUI(getUI(), getModel());

    }
}
