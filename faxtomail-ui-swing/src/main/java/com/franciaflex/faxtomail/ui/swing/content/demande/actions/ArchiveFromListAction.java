package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.MailAction;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class ArchiveFromListAction extends SaveDemandeFromListAction {

    protected List<DemandeUIModel> demandsToArchive;

    public ArchiveFromListAction(DemandeListUIHandler handler) {
        super(handler);
        setActionDescription(t("faxtomail.action.archive.tip"));
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result;

        demandsToArchive = new ArrayList<>();
        List<DemandeUIModel> currentEmails = getModel().getSelectedEmails();

        StringBuilder notArchivableDemandTitles = new StringBuilder();
        for (DemandeUIModel demandeUIModel : currentEmails) {
            // is the demand archivable?
            if (!demandeUIModel.isEditable() ||
                    !handler.isActionEnabled(demandeUIModel, MailAction.ARCHIVE)) {
                notArchivableDemandTitles.append("- ").append(demandeUIModel.getTitle()).append("<br/>");

            } else {
                demandsToArchive.add(demandeUIModel);
            }
        }

        if (demandsToArchive.isEmpty()) {
            displayWarningMessage(t("faxtomail.alert.noDemandToArchive.title"),
                                  t("faxtomail.alert.noDemandToArchive.message", notArchivableDemandTitles.toString()));
            result = false;

        } else {
            String message;
            if (notArchivableDemandTitles.length() > 0) {
                message = t("faxtomail.alert.archivesWithNotArchivableConfirmation.message", notArchivableDemandTitles.toString());

            } else if (demandsToArchive.size() == 1) {
                message = t("faxtomail.alert.archiveConfirmation.message");

            } else {
                message = t("faxtomail.alert.archivesConfirmation.message");
            }
            int i = JOptionPane.showConfirmDialog(null,
                                                  message,
                                                  t("faxtomail.alert.archiveConfirmation.title"),
                                                  JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE);

            result = i == JOptionPane.YES_OPTION;
        }

        return result && super.prepareAction();
    }

    @Override
    public void doAction() throws Exception {

        // déplacement du mail dans le dossier d'archive le plus proche
        // cela permet de déplacer les archives dans d'autre dossier pour leur appliquer
        // des droits différents
        // les dossiers d'archive ne sont pas visible dans l'arbre des dossiers, mais les archives
        // peuvent être consultées dans la recherche
        MailFolder folder = demandsToArchive.get(0).getMailFolder();
        MailFolder archiveChild = null;
        do {
            if (folder.getChildren() != null) {
                Optional<MailFolder> optArchiveChild = Iterables.tryFind(folder.getChildren(), new Predicate<MailFolder>() {
                    @Override
                    public boolean apply(MailFolder mailFolder) {
                        return mailFolder.isArchiveFolder();
                    }
                });
                if (optArchiveChild.isPresent()) {
                    archiveChild = optArchiveChild.get();
                }
            }
            folder = folder.getParent();

        } while (archiveChild == null && folder != null);

        for (DemandeUIModel email : demandsToArchive) {
            email.setArchiveDate(new Date());
            email.setDemandStatus(DemandStatus.ARCHIVED);
            email.setTakenBy(null);
            // le dossier d'archive peut ne pas exister. Dans ce cas on laisse le mail dans le dossier
            // d'origine
            if (archiveChild != null) {
                email.setMailFolder(archiveChild);
            }

            //Clear PDF pages when archiving
            email.clearPDFPages();
        }

        modifiedProperties = new String[] { Email.PROPERTY_ARCHIVE_DATE };
        super.doAction();
    }

    @Override
    public void postSuccessAction() {
        //reload list to fix #10071
        getHandler().reloadList();
        //do not do super.postSuccessAction(); as it only useful
        //for selection handling that is already managed in reloading
    }

}
