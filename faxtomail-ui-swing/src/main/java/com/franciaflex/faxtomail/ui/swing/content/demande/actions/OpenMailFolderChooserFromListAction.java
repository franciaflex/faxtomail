package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailAction;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.transmit.MailFolderChooserUI;
import com.franciaflex.faxtomail.ui.swing.content.transmit.MailFolderChooserUIModel;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class OpenMailFolderChooserFromListAction extends AbstractFaxToMailAction<DemandeListUIModel, DemandeListUI, DemandeListUIHandler> {
    
    protected List<DemandeUIModel> demandsToTransmit;
    protected MailFolderChooserUI frameContent;
    protected JFrame frame;

    public OpenMailFolderChooserFromListAction(DemandeListUIHandler handler) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.openMailFolderChooser.tip"));
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        List<DemandeUIModel> selectedEmails = getModel().getSelectedEmails();
        demandsToTransmit = new ArrayList<DemandeUIModel>();

        StringBuilder notTransmitableDemandsTitle = new StringBuilder();
        for (DemandeUIModel demandeUIModel : selectedEmails) {
            if (!demandeUIModel.isEditable() ||
                    !handler.isActionEnabled(demandeUIModel, MailAction.TRANSMIT)) {

                notTransmitableDemandsTitle.append("- ").append(demandeUIModel.getTitle()).append("<br/>");
            } else {
                demandsToTransmit.add(demandeUIModel);
            }
        }


        if (demandsToTransmit.isEmpty()) {
            displayWarningMessage(t("faxtomail.alert.noDemandToTransmit.title"),
                                  t("faxtomail.alert.noDemandToTransmit.message", notTransmitableDemandsTitle));
            result = false;

        } else if (!notTransmitableDemandsTitle.toString().isEmpty()) {
            displayWarningMessage(t("faxtomail.alert.demandsNotTransmittable.title"),
                                  t("faxtomail.alert.demandsNotTransmittable.message", notTransmitableDemandsTitle.toString()));
        }
        
        return result;
    }

    @Override
    public void doAction() throws Exception {
        MailFolderChooserUIModel model = new MailFolderChooserUIModel();
        model.setDemandeUIModels(demandsToTransmit);
        frameContent = new MailFolderChooserUI(getUI(), model);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        frame = getHandler().openModalFrame(frameContent, t("faxtomail.chooseMailFolder.title"), new Dimension(350, 500));

        getContext().addPropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (!getContext().isBusy()) {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {

                            bringCurrentDemandToFront();
                            if (frame != null) {
                                frame.toFront();
                                frame = null;
                            }
                        }
                    });
                    getContext().removePropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, this);
                }
            }
        });
    }

}
