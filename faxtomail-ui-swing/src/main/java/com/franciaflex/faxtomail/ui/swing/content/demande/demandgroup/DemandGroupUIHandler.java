package com.franciaflex.faxtomail.ui.swing.content.demande.demandgroup;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.content.demande.demandgroup.actions.OpenGroupedDemandAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.toolbar.AbstractToolbarPopupHandler;
import com.google.common.collect.Iterables;
import jaxx.runtime.validator.swing.SwingValidator;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.HighlighterFactory;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class DemandGroupUIHandler extends AbstractToolbarPopupHandler<DemandeUIModel, DemandGroupUI> {

    protected final PropertyChangeListener groupedDemandListener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            AbstractTableModel tableModel = (AbstractTableModel) getUI().getDemandGroup().getModel();
            tableModel.fireTableDataChanged();
        }
    };

    @Override
    public void beforeInit(DemandGroupUI ui) {
        super.beforeInit(ui);

        DemandeUIModel currentEmail = getContext().getCurrentEmail();
        this.ui.setContextValue(currentEmail);
    }

    @Override
    public void afterInit(DemandGroupUI ui) {
        super.afterInit(ui);

        initTable(ui.getDemandGroup());

        getModel().addPropertyChangeListener(DemandeUIModel.PROPERTY_GROUPED_DEMANDES, groupedDemandListener);
    }

    protected void initTable(JXTable table) {

        TableModel demandGroupTableModel = new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return getModel().sizeGroupedDemandes();
            }

            @Override
            public int getColumnCount() {
                return 1;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                return Iterables.get(getModel().getGroupedDemandes(), rowIndex);
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return true;
            }
        };

        demandGroupTableModel.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                getUI().pack();
            }
        });

        TableColumnModel columnModel = new DefaultTableColumnModel();
        TableColumn column = new TableColumn();
        column.setCellRenderer(new DemandGroupItemRenderer());
        column.setCellEditor(new DemandGroupItemEditor());
        columnModel.addColumn(column);

        table.setModel(demandGroupTableModel);
        table.setColumnModel(columnModel);
        table.addHighlighter(HighlighterFactory.createAlternateStriping());
    }

    @Override
    public void onCloseUI() {
        getModel().removePropertyChangeListener(DemandeUIModel.PROPERTY_GROUPED_DEMANDES, groupedDemandListener);
        ui.dispose();
    }

    @Override
    public SwingValidator<DemandeUIModel> getValidator() {
        return null;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    public void openDemande(DemandeUIModel demande) {
        closeEditor();

        OpenGroupedDemandAction openAction = new OpenGroupedDemandAction(this, demande);
        getContext().getActionEngine().runAction(openAction);
    }

    protected class DemandGroupItemRenderer extends DemandGroupItem
                                            implements TableCellRenderer {

        public DemandGroupItemRenderer() {
            super(DemandGroupUIHandler.this);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            DemandeUIModel demand = (DemandeUIModel) value;
            setDemande(demand);
            return this;
        }
    }

    protected class DemandGroupItemEditor extends AbstractCellEditor implements TableCellEditor {

        protected DemandGroupItem demandGroupItem;

        public DemandGroupItemEditor() {
            demandGroupItem = new DemandGroupItem(DemandGroupUIHandler.this);
        }

        public Component getTableCellEditorComponent(JTable table, Object value,
                                                     boolean isSelected, int row, int column) {
            DemandeUIModel demand = (DemandeUIModel) value;
            demandGroupItem.setDemande(demand);

            return demandGroupItem;
        }

        @Override
        public Object getCellEditorValue() {
            return demandGroupItem.getDemande();
        }
    }
}
