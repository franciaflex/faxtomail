package com.franciaflex.faxtomail.ui.swing.util;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.HasLabel;
import com.franciaflex.faxtomail.persistence.entities.MailAction;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.DecoratorService;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.swing.editor.bean.BeanUIUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.ApplicationDataUtil;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;
import org.nuiton.jaxx.application.swing.ApplicationUI;
import org.nuiton.jaxx.application.swing.action.AbstractApplicationAction;
import org.nuiton.jaxx.application.swing.action.ApplicationActionUI;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;
import org.nuiton.jaxx.application.swing.util.Cancelable;
import org.nuiton.jaxx.application.swing.util.CloseableUI;
import org.nuiton.util.beans.BeanUtil;
import org.nuiton.validator.bean.AbstractValidator;
import org.nuiton.validator.bean.simple.SimpleBeanValidator;

import javax.swing.AbstractAction;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.table.TableColumnModel;
import javax.swing.text.JTextComponent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Contract of any UI handler.
 *
 * @author kmorin - morin@codelutin.com
 *
 */
public abstract class AbstractFaxToMailUIHandler<M, UI extends FaxToMailUI<M, ?>>
        extends AbstractApplicationUIHandler<M, UI> implements UIMessageNotifier {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractFaxToMailUIHandler.class);

    protected final PropertyChangeListener listModelListener = new PropertyChangeListener() {

        final Set<String> excludeProperties = getPropertiesToIgnore();

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Object newValue = evt.getNewValue();
            Object oldValue = evt.getOldValue();
            if (!excludeProperties.contains(evt.getPropertyName())
                    //check that the new value and old value are not both null
                    && newValue != oldValue) {
                ((AbstractFaxToMailBeanUIModel) evt.getSource()).setModify(true);
            }
        }
    };

    @Override
    public void showInformationMessage(String message) {
        getContext().showInformationMessage(message);
    }

    public FaxToMailUIContext getContext() {
        return (FaxToMailUIContext) super.getContext();
    }

    public FaxToMailConfiguration getConfig() {
        return getContext().getConfig();
    }

    @Override
    public Component getTopestUI() {
        return getContext().getMainUI();
    }

    @Override
    public <O> Decorator<O> getDecorator(Class<O> type, String name) {
        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            DecoratorService decoratorService = serviceContext.getDecoratorService();

            Preconditions.checkNotNull(type);

            Decorator<O> decorator = decoratorService.getDecoratorByType(type, name);
            return decorator;
        } catch (IOException eee) {
            log.error("Error while getting decorator",eee);
        }

        return null;
    }

    // sonar signale cette methode comme inutile, mais elle augmente la visibilité en fait
    // il ne faut pas la supprimer
    public String decorate(Serializable s) {
        return super.decorate(s);    
    }
   
    public String decorateUser(FaxToMailUser user, boolean systemIfNull) {
        if (user == null && systemIfNull) {
            return t("faxtomail.systemUser");
        }
        return decorate(user);
    }

    public void setText(ItemEvent e, String property) {
        String value = JAXXUtil.getStringValue(e.getItem());
        ApplicationDataUtil.setProperty(getModel(), property, value);
    }

    @Override
    public void setText(KeyEvent event, String property) {
        JTextComponent field = (JTextComponent) event.getSource();
        if (field.isEditable()) {
            super.setText(event, property);
        }
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void listenValidatorValid(SimpleBeanValidator validator,
                                        final AbstractFaxToMailBeanUIModel model) {
        validator.addPropertyChangeListener(AbstractValidator.VALID_PROPERTY, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (log.isDebugEnabled()) {
                    log.debug("Model [" + model +
                                      "] pass to valid state [" +
                                      evt.getNewValue() + "]");
                }
                model.setValid((Boolean) evt.getNewValue());
            }
        });
    }

    protected void listModelIsModify(AbstractFaxToMailBeanUIModel model) {
        model.addPropertyChangeListener(listModelListener);
    }

    protected Set<String> getPropertiesToIgnore() {
        return Sets.newHashSet(
                AbstractFaxToMailBeanUIModel.PROPERTY_MODIFY,
                AbstractFaxToMailBeanUIModel.PROPERTY_VALID);
    }

    public JFrame openFrame(ApplicationUI dialogContent,
                           String title, Dimension dim) {

        final JFrame result = new JFrame();
        //mask frame to prevent resizing, show visible to true only after end of loading everything cf. #9988
        result.setVisible(false);
        result.setResizable(true);
        getContext().setSecondaryFrame(result);

        result.setName(dialogContent.getClass().getName());
        result.setTitle(title);
        result.setContentPane((Container) dialogContent);

        final AbstractApplicationUIHandler handler = dialogContent.getHandler();

        if (handler instanceof Cancelable) {

            // add a auto-close action
            JRootPane rootPane = result.getRootPane();

            KeyStroke shortcutClosePopup = getContext().getConfiguration().getShortcutClosePopup();

            rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                    shortcutClosePopup, "close");
            rootPane.getActionMap().put("close", new AbstractAction() {
                private static final long serialVersionUID = 1L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    ((org.nuiton.jaxx.application.swing.util.Cancelable) handler).cancel();
                }
            });
        }

        result.setSize(dim);
        SwingUtil.center(getContext().getMainUI(), result);

        result.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        result.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                closeFrame(result, handler);
            }

            @Override
            public void windowClosed(WindowEvent e) {
                Component ui = (Component) e.getSource();
                if (log.isDebugEnabled()) {
                    log.debug("Destroy ui " + ui);
                }
                getContext().getSwingSession().updateState();
                JAXXUtil.destroy(ui);
            }

        });

        result.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                getContext().setActionUI(new ApplicationActionUI(result, getContext()));
            }
        });

        getContext().getSwingSession().add(result, true);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                result.setVisible(true);
            }
        });

        return result;
    }

    /**
     * Open frame and disable parent frame
     */
    public JFrame openModalFrame(final ApplicationUI dialogContent, String title, Dimension dim) {
        final JFrame result = new JFrame();
        result.setResizable(true);
        getContext().setSecondaryFrame(result);

        final Frame parentContainer = getParentContainer(Frame.class);
        parentContainer.setEnabled(false);
        parentContainer.setFocusableWindowState(false);

        result.setName(dialogContent.getClass().getName());
        result.setTitle(title);
        result.setContentPane((Container) dialogContent);

        final AbstractApplicationUIHandler handler = dialogContent.getHandler();

        if (handler instanceof Cancelable) {

            // add a auto-close action
            JRootPane rootPane = result.getRootPane();

            KeyStroke shortcutClosePopup = getContext().getConfiguration().getShortcutClosePopup();

            rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                    shortcutClosePopup, "close");
            rootPane.getActionMap().put("close", new AbstractAction() {
                private static final long serialVersionUID = 1L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    ((Cancelable) handler).cancel();
                }
            });
        }

        result.setSize(dim);
        SwingUtil.center(parentContainer, result);

        result.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        result.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                closeFrame(result, handler);
            }

            @Override
            public void windowClosed(WindowEvent e) {
                parentContainer.setEnabled(true);
                parentContainer.setFocusableWindowState(true);
                parentContainer.toFront();

                getContext().getSwingSession().updateState();

                Component ui = (Component) e.getSource();
                if (log.isDebugEnabled()) {
                    log.debug("Destroy ui " + ui);
                }
                JAXXUtil.destroy(ui);
            }
        });

        result.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                getContext().setActionUI(new ApplicationActionUI(result, getContext()));
            }
        });

        getContext().getSwingSession().add(result, true);

        result.setVisible(true);

        return result;
    }

    /**
     * Gros copier/coller de quitScreen() pour appeler runActionAndWait au lieu de saveAction.actionPerformed(null); par defaut
     * pour tenter de corriger un problème de concurrence entre l'action de sauvegarde de l'email
     * et l'action de rechargement de l'arbre qui fait un closeTransaction au même moment.
     * 
     * @param modelIsValid
     * @param modelIsModify
     * @param askGiveUpMessage
     * @param askSaveMessage
     * @param saveAction
     * @return
     */
    protected boolean quitScreen2(boolean modelIsValid, boolean modelIsModify, String askGiveUpMessage,
                                  String askSaveMessage, boolean showHelp, AbstractApplicationAction saveAction) {
        boolean result;

        if (!modelIsValid) {

            // model is not valid
            // ask user to qui or not
            result = askCancelEditBeforeLeaving(askGiveUpMessage, showHelp);

        } else if (modelIsModify) {

            // something is modify ask user what to do
            int answer = askSaveBeforeLeaving(askSaveMessage, showHelp);
            switch (answer) {
            case JOptionPane.YES_OPTION:

                // ok save
                //saveAction.actionPerformed(null);
                //XXX echatellier 2010731 : action bloquante
                if (getContext().isActionInProgress(null)) {
                    getContext().getActionEngine().runInternalAction(saveAction);
                } else {
                    getContext().getActionEngine().runActionAndWait(saveAction);
                }
                result = true;
                break;
            case JOptionPane.NO_OPTION:

                // do not save but can still quit the screen (so nothing to do)
                result = true;
                break;
            default:
                // do not save and stay here (so nothing to do)
                result = false;

            }
        } else {

            // model is valid and not modify, can safely quit screen
            result = true;
        }
        return result;
    }

    public int askSaveBeforeLeaving(String message, boolean showHelp) {
        int result;
        if (showHelp) {
            result = super.askSaveBeforeLeaving(message);
        } else {
            result = JOptionPane.showConfirmDialog(
                    getTopestUI(),
                    message,
                    t("jaxx.application.common.askSaveBeforeLeaving.title"),
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
        }
        return result;
    }

    public boolean askCancelEditBeforeLeaving(String message, boolean showHelp) {
        boolean result;
        if (showHelp) {
            result = super.askCancelEditBeforeLeaving(message);
        } else {
            result = JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(
                    getTopestUI(),
                    message,
                    t("jaxx.application.common.askCancelEditBeforeLeaving.title"),
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
        }
        return result;
    }

    public void closeFrame() {
        JFrame frame = getParentContainer(JFrame.class);
        closeFrame(frame, this);
    }

    protected void closeFrame(JFrame frame, AbstractApplicationUIHandler handler) {
        handler.onCloseUI();
        if (frame != null) {
            boolean canClose = true;
            if (handler instanceof CloseableUI) {
                canClose = ((CloseableUI) handler).quitUI();
            }
            if (canClose) {
                frame.dispose();
            }
        }
    }

    @Override
    protected <E> void initBeanFilterableComboBox(BeanFilterableComboBox<E> comboBox, List<E> data, E selectedData, String decoratorContext) {
        super.initBeanFilterableComboBox(comboBox, data, selectedData, decoratorContext);
        comboBox.getComboBoxModel().setWildcardCharacter(null);
    }

    protected <HL extends HasLabel> void initCheckBoxComboBox(final JComboBox<HL> comboBox,
                                                              java.util.List<HL> values,
                                                              java.util.List<HL> selection,
                                                              String property,
                                                              boolean addNull) {

        java.util.List<HL> universe = new ArrayList<HL>();
        if (addNull) {
            universe.add(null);
        }
        if (values != null) {
            universe.addAll(values);
        }
        final ComboBoxModel<HL> comboModel = new CheckBoxComboBoxModel<HL>(universe, selection);
        comboBox.setModel(comboModel);
        comboBox.setRenderer(new CheckBoxListCellRenderer<HL>() {
            @Override
            protected boolean isCheckBoxSelected(Object value) {
                return ((java.util.List<Object>) comboModel.getSelectedItem()).contains(value);
            }
        });
        final Method mutator = BeanUtil.getMutator(getModel(), property);
        comboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                //reopen the combo
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        comboBox.showPopup();
                    }
                });

                if (e.getStateChange() == ItemEvent.SELECTED) {
                    BeanUIUtil.invokeMethod(mutator,
                                            getModel(),
                                            e.getItem());
                }
            }
        });
    }

    public boolean isActionEnabled(DemandeUIModel model, MailAction action) {

        WaitingState waitingState = model.getWaitingState();
        boolean valid = model.isValid();

        MailFolder folderWithInvalidFormDisabledActions = model.getMailFolder();
        while (folderWithInvalidFormDisabledActions.getParent() != null
               && !folderWithInvalidFormDisabledActions.isUseCurrentLevelInvalidFormDisabledActions()) {
            folderWithInvalidFormDisabledActions = folderWithInvalidFormDisabledActions.getParent();
        }
        return waitingState == null && (valid || !FaxToMailServiceUtils.contains(folderWithInvalidFormDisabledActions.getInvalidFormDisabledActions(), action))
                        || waitingState != null &&
                        (valid && !FaxToMailServiceUtils.contains(waitingState.getValidFormDisabledActions(), action)
                                || !valid && !FaxToMailServiceUtils.contains(waitingState.getInvalidFormDisabledActions(), action));
    }

    protected <R, B> TableColumnExt addComboDataColumnToModel(TableColumnModel model,
                                                              ColumnIdentifier<R> identifier,
                                                              Decorator<B> decorator,
                                                              List<B> data) {
        JComboBox comboBox = new JComboBox();
        comboBox.setMaximumRowCount(20);
        comboBox.setRenderer(newListCellRender(decorator));

        List<B> dataToList = Lists.newArrayList(data);

        // add a null value at first position
        if (!dataToList.isEmpty() && dataToList.get(0) != null) {
            dataToList.add(0, null);
        }
        SwingUtil.fillComboBox(comboBox, dataToList, null);

        ObjectToStringConverter converter = BeanUIUtil.newDecoratedObjectToStringConverter(decorator);
        BeanUIUtil.decorate(comboBox, converter);
        ComboBoxCellEditor editor = new ComboBoxCellEditor(comboBox);

        return addColumnToModel(model,
                                editor,
                                newTableCellRender(decorator),
                                identifier);
    }
}
