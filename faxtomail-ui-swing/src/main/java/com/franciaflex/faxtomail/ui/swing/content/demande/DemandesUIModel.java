package com.franciaflex.faxtomail.ui.swing.content.demande;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class DemandesUIModel extends AbstractBean {

    public static final String PROPERTY_CURRENT_DEMAND = "currentDemand";
    public static final String PROPERTY_TRANSMIT_ENABLED = "transmitEnabled";
    public static final String PROPERTY_ARCHIVE_ENABLED = "archiveEnabled";
    public static final String PROPERTY_GROUP_ENABLED = "groupEnabled";
    public static final String PROPERTY_PRINT_ENABLED = "printEnabled";
    public static final String PROPERTY_REPLY_ACTIVATED = "replyActivated";
    public static final String PROPERTY_REPLY_ENABLED = "replyEnabled";
    public static final String PROPERTY_FORWARD_ACTIVATED = "forwardActivated";
    public static final String PROPERTY_FORWARD_ENABLED = "forwardEnabled";

    protected boolean transmitEnabled;
    protected boolean archiveEnabled;
    protected boolean groupEnabled;
    protected boolean printEnabled;
    protected boolean replyActivated;
    protected boolean replyEnabled;
    protected boolean forwardActivated;
    protected boolean forwardEnabled;

    protected List<DemandeUIModel> demands = new ArrayList<DemandeUIModel>();

    protected List<com.franciaflex.faxtomail.ui.swing.content.demande.DemandsListener> listeners = new ArrayList<com.franciaflex.faxtomail.ui.swing.content.demande.DemandsListener>();

    protected int currentIndex = -1;

    public boolean isTransmitEnabled() {
        return transmitEnabled;
    }

    public void setTransmitEnabled(boolean transmitEnabled) {
        Object oldValue = isTransmitEnabled();
        this.transmitEnabled = transmitEnabled;
        firePropertyChange(PROPERTY_TRANSMIT_ENABLED, oldValue, transmitEnabled);
    }

    public boolean isArchiveEnabled() {
        return archiveEnabled;
    }

    public void setArchiveEnabled(boolean archiveEnabled) {
        Object oldValue = isArchiveEnabled();
        this.archiveEnabled = archiveEnabled;
        firePropertyChange(PROPERTY_ARCHIVE_ENABLED, oldValue, archiveEnabled);
    }

    public boolean isGroupEnabled() {
        return groupEnabled;
    }

    public void setGroupEnabled(boolean groupEnabled) {
        Object oldValue = isGroupEnabled();
        this.groupEnabled = groupEnabled;
        firePropertyChange(PROPERTY_GROUP_ENABLED, oldValue, groupEnabled);
    }

    public boolean isPrintEnabled() {
        return printEnabled;
    }

    public void setPrintEnabled(boolean printEnabled) {
        Object oldValue = isPrintEnabled();
        this.printEnabled = printEnabled;
        firePropertyChange(PROPERTY_PRINT_ENABLED, oldValue, printEnabled);
    }

    public boolean isReplyActivated() {
        return replyActivated;
    }

    public void setReplyActivated(boolean replyActivated) {
        Object oldValue = isReplyActivated();
        this.replyActivated = replyActivated;
        firePropertyChange(PROPERTY_REPLY_ACTIVATED, oldValue, replyActivated);
    }

    public boolean isReplyEnabled() {
        return replyEnabled;
    }

    public void setReplyEnabled(boolean replyEnabled) {
        Object oldValue = isReplyEnabled();
        this.replyEnabled = replyEnabled;
        firePropertyChange(PROPERTY_REPLY_ENABLED, oldValue, replyEnabled);
    }

    public boolean isForwardActivated() {
        return forwardActivated;
    }

    public void setForwardActivated(boolean forwardActivated) {
        Object oldValue = isForwardActivated();
        this.forwardActivated = forwardActivated;
        firePropertyChange(PROPERTY_FORWARD_ACTIVATED, oldValue, forwardActivated);
    }

    public boolean isForwardEnabled() {
        return forwardEnabled;
    }

    public void setForwardEnabled(boolean forwardEnabled) {
        Object oldValue = isForwardEnabled();
        this.forwardEnabled = forwardEnabled;
        firePropertyChange(PROPERTY_FORWARD_ENABLED, oldValue, forwardEnabled);
    }

    protected List<DemandeUIModel> getDemands() {
        return demands;
    }

    public void addDemand(DemandeUIModel demand) {
        boolean added = !demands.contains(demand);
        if (added) {
            demands.add(demand);
        }
        int index = demands.indexOf(demand);
        fireDemandAdded(demand, index, added);
    }

    public void removeDemand(int i) {
        DemandeUIModel demand = demands.get(i);
        demands.remove(i);
        fireDemandRemoved(demand, i);
    }

    public boolean containsDemand(DemandeUIModel demand) {
        return demands.contains(demand);
    }

    public void addDemandsListener(com.franciaflex.faxtomail.ui.swing.content.demande.DemandsListener listener) {
        listeners.add(listener);
    }

    public void removeDemandsListener(com.franciaflex.faxtomail.ui.swing.content.demande.DemandsListener listener) {
        listeners.remove(listener);
    }

    protected void fireDemandAdded(DemandeUIModel demand, int index, boolean added) {
        for (DemandsListener listener : listeners) {
            listener.onDemandAdded(demand, index, added);
        }
    }

    protected void fireDemandRemoved(DemandeUIModel demand, int index) {
        for (DemandsListener listener : listeners) {
            listener.onDemandRemoved(demand, index);
        }
    }

    public DemandeUIModel getCurrentDemand() {
        int currentIndex = getCurrentIndex();
        if (currentIndex < 0 || currentIndex >= demands.size()) {
            return null;
        }
        return demands.get(currentIndex);
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        Object oldValue = getCurrentDemand();
        this.currentIndex = currentIndex;
        Object newValue = getCurrentDemand();
        firePropertyChange(PROPERTY_CURRENT_DEMAND, oldValue, newValue);
    }
}
