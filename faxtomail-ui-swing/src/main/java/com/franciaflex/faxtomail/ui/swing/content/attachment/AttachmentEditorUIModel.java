package com.franciaflex.faxtomail.ui.swing.content.attachment;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailBeanUIModel;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class AttachmentEditorUIModel extends AbstractFaxToMailBeanUIModel<AttachmentModelAware, AttachmentEditorUIModel> {

    //public static final String PROPERTY_FILE = "file";
    public static final String PROPERTY_EDITABLE = "editable";
    public static final String PROPERTY_CAN_VIEW_ORIGINAL_ATTACHMENTS = "canViewOriginalAttachments";
    public static final String PROPERTY_MAIL_CONTENT_AS_INLINE_ATTACHMENT = "mailContentAsInlineAttachment";
    public static final String PROPERTY_INLINE_ATTACHMENTS = "inlineAttachments";
    public static final String PROPERTY_SELECTED_ATTACHMENT = "selectedAttachment";

    public static final Comparator<Attachment> ATTACHMENT_COMPARATOR = new Comparator<Attachment>() {
        @Override
        public int compare(Attachment o1, Attachment o2) {
            if (o1 == null) {
                return o2 == null ? 0 : -1;
            }
            
            if (o2 == null) {
                return 1;
            }

            if (o1.isLessImportant() ^ o2.isLessImportant()) {
                return o1.isLessImportant() ? 1 : -1;
            }

            if (o1.isInlineAttachment() ^ o2.isInlineAttachment()) {
                return o1.isInlineAttachment() ? 1 : -1;
            }

            if (o1.isMailContent() ^ o2.isMailContent()) {
                return o1.isMailContent() ? 1 : -1;
            }

            return ObjectUtils.compare(StringUtils.lowerCase(o1.getOriginalFileName()),
                                       StringUtils.lowerCase(o2.getOriginalFileName()));
        }
    };

    //protected File file;
    protected boolean editable = true;

    protected boolean canViewOriginalAttachments;

    protected boolean mailContentAsInlineAttachment;

    protected final List<Attachment> attachments = new ArrayList<Attachment>();
    protected final List<Attachment> inlineAttachments = new ArrayList<Attachment>();

    protected Attachment selectedAttachment;

    protected List<AttachmentListener> openingListeners = new ArrayList<AttachmentListener>();

    protected static Binder<AttachmentEditorUIModel, AttachmentModelAware> toBeanBinder =
            BinderFactory.newBinder(AttachmentEditorUIModel.class,
                                    AttachmentModelAware.class);

    protected static Binder<AttachmentModelAware, AttachmentEditorUIModel> fromBeanBinder =
            BinderFactory.newBinder(AttachmentModelAware.class, AttachmentEditorUIModel.class);

    protected AttachmentEditorUIModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        Object oldValue = isEditable();
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, oldValue, editable);
    }

    public boolean isCanViewOriginalAttachments() {
        return canViewOriginalAttachments;
    }

    public void setCanViewOriginalAttachments(boolean canViewOriginalAttachments) {
        Object oldValue = isCanViewOriginalAttachments();
        this.canViewOriginalAttachments = canViewOriginalAttachments;
        firePropertyChange(PROPERTY_CAN_VIEW_ORIGINAL_ATTACHMENTS, oldValue, canViewOriginalAttachments);
    }

    public boolean isMailContentAsInlineAttachment() {
        return mailContentAsInlineAttachment;
    }

    public void setMailContentAsInlineAttachment(boolean mailContentAsInlineAttachment) {
        Object oldValue = isMailContentAsInlineAttachment();
        this.mailContentAsInlineAttachment = mailContentAsInlineAttachment;
        firePropertyChange(PROPERTY_MAIL_CONTENT_AS_INLINE_ATTACHMENT, oldValue, mailContentAsInlineAttachment);
    }

    public List<Attachment> getAttachment() {
        return attachments;
    }

    public void addAllAttachment(List<Attachment> attachments) {
        Object oldValue = new ArrayList<Attachment>(getAttachment());
        this.attachments.addAll(attachments);
        firePropertyChange(AttachmentModelAware.PROPERTY_ATTACHMENT, oldValue, this.attachments);
    }

    public void addAttachment(Attachment attachment) {
        Object oldValue = new ArrayList<Attachment>(getAttachment());
        attachments.add(attachment);
        firePropertyChange(AttachmentModelAware.PROPERTY_ATTACHMENT, oldValue, attachments);
    }

    public void removeAttachment(Attachment attachment) {
        Object oldValue = new ArrayList<>(getAttachment());
        attachments.remove(attachment);
        firePropertyChange(AttachmentModelAware.PROPERTY_ATTACHMENT, oldValue, attachments);
    }

    public void setAttachment(List<Attachment> attachments) {
        Object oldAttachmentsValue = new ArrayList<>(getAttachment());
        Object oldInlineAttachmentsValue = new ArrayList<>(getInlineAttachments());
        this.attachments.clear();
        this.inlineAttachments.clear();

        if (attachments != null) {

            inlineAttachments.addAll(Collections2.filter(attachments, new Predicate<Attachment>() {
                @Override
                public boolean apply(Attachment input) {
                    return input.isInlineAttachment() || mailContentAsInlineAttachment && input.isMailContent();
                }
            }));

            this.attachments.addAll(attachments);

            if (inlineAttachments.size() > 1) {
                this.attachments.removeAll(inlineAttachments);

            } else {
                inlineAttachments.clear();
            }

            sortAttachments();
        }
        firePropertyChange(AttachmentModelAware.PROPERTY_ATTACHMENT, oldAttachmentsValue, this.attachments);
        firePropertyChange(PROPERTY_INLINE_ATTACHMENTS, oldInlineAttachmentsValue, this.inlineAttachments);
    }

    public List<Attachment> getInlineAttachments() {
        return inlineAttachments;
    }

    public void sortAttachments() {
        Collections.sort(this.attachments, ATTACHMENT_COMPARATOR);
        Collections.sort(this.inlineAttachments, ATTACHMENT_COMPARATOR);
    }

    public Attachment getSelectedAttachment() {
        return selectedAttachment;
    }

    public void setSelectedAttachment(Attachment selectedAttachment) {
        Object oldValue = getSelectedAttachment();
        this.selectedAttachment = selectedAttachment;
        firePropertyChange(PROPERTY_SELECTED_ATTACHMENT, oldValue, selectedAttachment);
    }

    @Override
    protected AttachmentModelAware newEntity() {
        return null;
    }

    public void addAttachmentListener(AttachmentListener listener) {
        openingListeners.add(listener);
    }

    public void removeAttachmentListener(AttachmentListener listener) {
        openingListeners.remove(listener);
    }

    public void fireAttachmentOpened(Attachment attachment, boolean original) {
        for (AttachmentListener listener : openingListeners) {
            listener.onAttachmentOpened(attachment, original);
        }
    }

    public void fireAttachmentEdited(Attachment attachment) {
        for (AttachmentListener listener : openingListeners) {
            listener.onAttachmentEdited(attachment);
        }
    }

}
