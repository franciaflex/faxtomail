package com.franciaflex.faxtomail.ui.swing.content.search.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.search.SearchToGroupUI;
import com.franciaflex.faxtomail.ui.swing.content.search.SearchToGroupUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.search.SearchUIModel;
import com.franciaflex.faxtomail.ui.swing.util.DemandeTableModel;
import org.jdesktop.swingx.JXTable;

import javax.swing.*;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class GroupAction extends AbstractFaxToMailAction<SearchUIModel, SearchToGroupUI, SearchToGroupUIHandler> {

    public GroupAction(SearchToGroupUIHandler handler) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.group.tip"));
    }

    protected DemandeUIModel currentEmail;
    protected DemandeUIModel selectedEmail;

    @Override
    public boolean prepareAction() throws Exception {

        JXTable dataTable = getUI().getDataTable();
        int rowIndex = dataTable.getSelectedRow();
        rowIndex = dataTable.convertRowIndexToModel(rowIndex);
        selectedEmail = ((DemandeTableModel) dataTable.getModel()).getEntry(rowIndex);

        currentEmail = getContext().getCurrentEmail();

        int i = JOptionPane.showConfirmDialog(
                null,
                t("faxtomail.alert.groupConfirmation.message", currentEmail.getTitle(), selectedEmail.getTitle()),
                t("faxtomail.alert.groupConfirmation.title"),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        boolean result = i == JOptionPane.YES_OPTION;
        return result;
    }

    @Override
    public void doAction() throws Exception {
        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            EmailService emailService = serviceContext.getEmailService();

            FaxToMailUser currentUser = getContext().getCurrentUser();
            Email email = emailService.groupEmails(currentEmail.getTopiaId(), selectedEmail.getTopiaId(), currentUser);
            currentEmail.fromEntity(email);
            currentEmail.setGroupedDemandes(email.getEmailGroup());
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        handler.showInformationMessage(t("faxtomail.demande.group.successful"));
        handler.closeFrame();
    }
}
