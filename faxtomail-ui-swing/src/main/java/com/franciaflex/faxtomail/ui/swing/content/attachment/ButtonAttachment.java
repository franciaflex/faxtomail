package com.franciaflex.faxtomail.ui.swing.content.attachment;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.util.toolbar.AbstractToolbarPopupButton;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;

import javax.swing.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Button to edit attachments.
 *
 * @author kmorin - morin@codelutin.com
 */
public class ButtonAttachment extends AbstractToolbarPopupButton<AttachmentEditorUI> {

    protected AttachmentModelAware attachmentModelAware;

    protected String textKey;

    private final PropertyChangeListener listener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            List<Attachment> attachments = (List<Attachment>) evt.getNewValue();
            setText(getButtonText(attachments));

            if (attachmentModelAware != null) {
                List<Attachment> oldValue = (List<Attachment>) evt.getOldValue();
                List<Attachment> toAdd = ListUtils.subtract(attachments, oldValue);
                attachmentModelAware.addAllAttachment(toAdd);

                Collection<Attachment> toRemove = CollectionUtils.subtract(oldValue, attachments);
                for (Attachment attachment : toRemove) {
                    attachmentModelAware.removeAttachment(attachment);
                }
            }
        }
    };

    public ButtonAttachment(AttachmentModelAware model) {
        this(n("faxtomail.attachmentEditor.text"), model);
    }

    public ButtonAttachment(String textKey, AttachmentModelAware model) {
        this.textKey = textKey;
        setToolTipText(t("faxtomail.attachmentEditor.action.tip"));
        init(model);
    }

    @Override
    protected String getActionIcon() {
        return "attachment";
    }

    @Override
    protected AttachmentEditorUI createNewPopup() {
        return new AttachmentEditorUI(FaxToMailUIContext.getApplicationContext(),
                                      SwingUtil.getParentContainer(this, JFrame.class));
    }

    public String getButtonText(Collection<Attachment> attachments) {
        int attachmentNb = CollectionUtils.size(attachments);
        return t(textKey, attachmentNb);
    }

    public void init(AttachmentModelAware model) {
        attachmentModelAware = model;

        popup.getModel().setCanViewOriginalAttachments(model != null && model.isCanViewOriginalAttachments());
        popup.getModel().setMailContentAsInlineAttachment(model != null && model.isMailContentAsInlineAttachment());

        popup.getModel().removePropertyChangeListener(AttachmentModelAware.PROPERTY_ATTACHMENT, listener);
        popup.getModel().fromEntity(model);
        popup.getModel().addPropertyChangeListener(AttachmentModelAware.PROPERTY_ATTACHMENT, listener);

        setText(getButtonText(model != null ? model.getAttachment() : null));
    }

    public AttachmentEditorUIModel getBean() {
        return popup.getModel();
    }

    public void setEditable(boolean editable) {
        popup.getModel().setEditable(editable);
    }

    @Override
    public void onCloseUI() {
        super.onCloseUI();
        popup.getModel().removePropertyChangeListener(AttachmentModelAware.PROPERTY_ATTACHMENT, listener);
    }

    public void editAttachment(Attachment attachment) {
        popup.getHandler().editAttachment(attachment);
    }
}
