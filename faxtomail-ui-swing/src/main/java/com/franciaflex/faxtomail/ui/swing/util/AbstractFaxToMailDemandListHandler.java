package com.franciaflex.faxtomail.ui.swing.util;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.HasLabel;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.Priority;
import com.franciaflex.faxtomail.persistence.entities.RangeRow;
import com.franciaflex.faxtomail.persistence.entities.Reply;
import com.franciaflex.faxtomail.services.DecoratorService;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.ShowDemandeAction;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentCellEditor;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentCellRenderer;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.demandgroup.DemandGroupCellRenderer;
import com.franciaflex.faxtomail.ui.swing.content.demande.replies.RepliesCellEditor;
import com.franciaflex.faxtomail.ui.swing.content.demande.replies.RepliesCellRenderer;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.decorator.Decorator;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorFactory;
import org.nuiton.validator.NuitonValidatorResult;

import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public abstract class AbstractFaxToMailDemandListHandler<M, UI extends FaxToMailUI<M, ?>> extends AbstractFaxToMailUIHandler<M, UI>
                                                                                          implements ReloadableListUI {

    protected NuitonValidator<DemandeUIModel> demandeUIModelNuitonValidator = NuitonValidatorFactory.newValidator(DemandeUIModel.class);;

    protected boolean colorizeInvalidDemands = true;

    public void updateColorizeInvalidDemands() {
        MailFolder mailFolder = getContext().getCurrentMailFolder();
        while (mailFolder != null && mailFolder.getColorizeInvalidDemands() == null) {
            mailFolder = mailFolder.getParent();
        }
        colorizeInvalidDemands = mailFolder != null && BooleanUtils.isNotFalse(mailFolder.getColorizeInvalidDemands());
    }

    public void initDemandeTable(final JXTable table, boolean sortable) {
//        table.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);

        populateColumnModel(table, sortable);

        table.getTableHeader().setReorderingAllowed(false);

        Highlighter highlighter = new Highlighter() {
            @Override
            public Component highlight(Component renderer, ComponentAdapter adapter) {
                int viewRow = adapter.row;
                int modelRow = adapter.convertRowIndexToModel(viewRow);
                DemandeUIModel row = ((DemandeTableModel) table.getModel()).getEntry(modelRow);
                boolean highlight = row.getTakenBy() == null;
                if (highlight) {
                    Font font = renderer.getFont();
                    renderer.setFont(font.deriveFont(Font.BOLD));
                }
                highlight = row.getArchiveDate() != null;
                if (highlight) {
                    Font font = renderer.getFont();
                    renderer.setFont(font.deriveFont(Font.ITALIC));
                    renderer.setForeground(adapter.isSelected() ? Color.WHITE : new Color(96, 96, 96));
                }
                return renderer;
            }

            @Override
            public void addChangeListener(ChangeListener l) {
            }

            @Override
            public void removeChangeListener(ChangeListener l) {
            }

            @Override
            public ChangeListener[] getChangeListeners() {
                return new ChangeListener[0];
            }
        };
        table.addHighlighter(highlighter);

        HighlightPredicate rowIsInvalidPredicate = new HighlightPredicate() {

            @Override
            public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
                int viewRow = adapter.row;
                int modelRow = adapter.convertRowIndexToModel(viewRow);
                DemandeUIModel row = ((DemandeTableModel) table.getModel()).getEntry(modelRow);
                return colorizeInvalidDemands && !row.isValid() && row.getTakenBy() != null;
            }
        };
        Color color = new Color(255, 51, 51);
        table.addHighlighter(new ColorHighlighter(rowIsInvalidPredicate, color, Color.WHITE, color.darker(), Color.WHITE));

        table.addMouseListener(getDataTableMouseListener());
    }

    public abstract List<MailField> getColumns();

    public String populateColumnModel(JXTable table, boolean sortable) {
        List<MailField> columns = getColumns();

        TableColumnModelExt columnModel = new DefaultTableColumnModelExt();

        if (CollectionUtils.isEmpty(columns)) {
            columns = Lists.newArrayList(MailField.getTableFields());
        }

        for (MailField columnName : columns) {

            if (MailField.PRIORITY.equals(columnName)) {
                addComboDataColumnToModel(columnModel,
                                          DemandeTableModel.COLUMN_IDENTIFIERS.get(columnName),
                                          getDecorator(Priority.class, null),
                                          getContext().getPriorityCache()).setSortable(sortable);

            } else if (MailField.ATTACHMENT.equals(columnName)) {
                addColumnToModel(columnModel,
                                 AttachmentCellEditor.newEditor(ui),
                                 AttachmentCellRenderer.newRender(getDecorator(Attachment.class, null)),
                                 DemandeTableModel.COLUMN_IDENTIFIERS.get(columnName)).setSortable(sortable);

            } else if (MailField.REPLIES.equals(columnName)) {
                addColumnToModel(columnModel,
                                 RepliesCellEditor.newEditor(ui),
                                 RepliesCellRenderer.newRender(getDecorator(Reply.class, null)),
                                 DemandeTableModel.COLUMN_IDENTIFIERS.get(columnName)).setSortable(sortable);

            } else if (MailField.GROUP.equals(columnName)) {
                addColumnToModel(columnModel,
                                 null,
                                 DemandGroupCellRenderer.newRender(),
                                 DemandeTableModel.COLUMN_IDENTIFIERS.get(columnName)).setSortable(sortable);

            } else {
                TableCellRenderer tableCellRenderer;
                switch (columnName) {
                    case RECEPTION_DATE:
                    case LAST_PRINTING_DATE:
                        tableCellRenderer = newTableCellRender(Date.class);
                        break;

                    case RANGE_ROW:
                        tableCellRenderer = new RangeTableCellRenderer();
                        break;

                    case PF_NB:
                        tableCellRenderer = new QuantityTableCellRenderer(new Function<RangeRow, Integer>() {
                            @Override
                            public Integer apply(RangeRow input) {
                                return input != null ? input.getProductQuantity() : null;
                            }
                        });
                        break;
                    case SAV_NB:
                        tableCellRenderer = new QuantityTableCellRenderer(new Function<RangeRow, Integer>() {
                            @Override
                            public Integer apply(RangeRow input) {
                                return input != null ? input.getSavQuantity() : null;
                            }
                        });
                        break;

                    case DEMAND_STATUS:
                        tableCellRenderer = newTableCellRender(DemandStatus.class);
                        break;

                    case DEMAND_TYPE:
                    case WAITING_STATE:
                        tableCellRenderer = newTableCellRender(HasLabel.class);
                        break;

                    case TAKEN_BY:
                    case LAST_ATTACHMENT_OPENING_IN_THIS_FOLDER_USER:
                    case LAST_PRINTING_USER:
                        String name = null;
                        if (isDisplayOnlyUserTrigraphInTables()) {
                            name = DecoratorService.SHORT;
                        }
                        tableCellRenderer = newTableCellRender(FaxToMailUser.class, name);
                        break;

                    default:
                        tableCellRenderer = newTableCellRender(new Decorator<Object>(Object.class) {
                            @Override
                            public String toString(Object bean) {
                                return bean == null ? "" : bean.toString();
                            }
                        });
                }
                addColumnToModel(columnModel,
                                 null,
                                 tableCellRenderer,
                                 DemandeTableModel.COLUMN_IDENTIFIERS.get(columnName)).setSortable(sortable);
            }
        }

        TableModel tableModel = new DemandeTableModel(columnModel, getEditableTableProperties());
        table.setModel(tableModel);
        table.setColumnModel(columnModel);

        return StringUtils.join(columns, "-");
    }

    protected abstract boolean isDisplayOnlyUserTrigraphInTables();

    protected MouseListener getDataTableMouseListener() {
        return new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    JXTable table = (JXTable) evt.getSource();
                    int rowIndex = table.getSelectedRow();
                    if (rowIndex < 0 || rowIndex >= table.getRowCount()) {
                        return;
                    }
                    rowIndex = table.convertRowIndexToModel(rowIndex);

                    DemandeTableModel tableModel = (DemandeTableModel) table.getModel();
                    DemandeUIModel selectedEmail = tableModel.getEntry(rowIndex);
                    onDoubleClickOnDemande(selectedEmail);

                    openDemand(selectedEmail);
                }
            }
        };
    }

    /**
     * Auto select closest row near clic-mouse position and display popup menu.
     */
    @Override
    public void autoSelectRowInTable(MouseEvent e, JPopupMenu popup) {
        boolean rightClick = SwingUtilities.isRightMouseButton(e);
        if (rightClick) {
            // get the coordinates of the mouse click
            Point p = e.getPoint();

            JXTable source = (JXTable) e.getSource();

            int[] selectedRows = source.getSelectedRows();

            // get the row index at this point
            int rowIndex = source.rowAtPoint(p);
            if (rowIndex == -1 || !ArrayUtils.contains(selectedRows, rowIndex)) {
                source.clearSelection();
            }
        }

        super.autoSelectRowInTable(e, popup);
    }

    protected abstract MailField[] getEditableTableProperties();

    protected void onDoubleClickOnDemande(DemandeUIModel selectedEmail) {
    }

    protected void openDemand(DemandeUIModel selectedEmail) {

        FaxToMailUIContext context = getContext();

        context.setCurrentEmail(selectedEmail);
        context.getActionEngine().runAction(new ShowDemandeAction(context.getMainUI().getHandler()));
    }

    public boolean isDemandeValid(DemandeUIModel d) {
        NuitonValidatorResult result = demandeUIModelNuitonValidator.validate(d);
        return result.isValid();
    }

    public class QuantityTableCellRenderer extends DefaultTableCellRenderer {

        protected Function<RangeRow, Integer> getQuantityFunction;

        public QuantityTableCellRenderer(Function<RangeRow, Integer> getQuantityFunction) {
            this.getQuantityFunction = getQuantityFunction;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasfocus, int row, int column) {
            super.getTableCellRendererComponent(table, value, isSelected, hasfocus, row, column);

            DemandeTableModel tableModel = (DemandeTableModel) table.getModel();
            int rowInModel = table.convertRowIndexToModel(row);
            DemandeUIModel demande = tableModel.getEntry(rowInModel);

            String tooltipText = null;

            List<String> ranges = new ArrayList<String>();
            Collection<RangeRow> rangeRows = demande.getRangeRow();
            if (rangeRows != null) {
                for (RangeRow rangeRow : rangeRows) {
                    Integer qty = getQuantityFunction.apply(rangeRow);
                    if (qty != null && qty > 0) {
                        ranges.add(rangeRow.getRange().getLabel() + " : " + qty);
                    }
                }
            }

            if (!ranges.isEmpty()) {
                tooltipText = "<html><body>" + StringUtils.join(ranges, "<br/>") + "</body></html>";
            }
            setToolTipText(tooltipText);

            return this;
        }
    }

    public class RangeTableCellRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasfocus, int row, int column) {
            super.getTableCellRendererComponent(table, value, isSelected, hasfocus, row, column);

            DemandeTableModel tableModel = (DemandeTableModel) table.getModel();
            int rowInModel = table.convertRowIndexToModel(row);
            DemandeUIModel demande = tableModel.getEntry(rowInModel);

            String text = null;

            List<RangeRow> rangeRows = demande.getRangeRow();
            if (CollectionUtils.isNotEmpty(rangeRows)) {
                text = decorate(rangeRows.get(0).getRange());
            }
            setText(text);
            setToolTipText(text);

            return this;
        }
    }
}
