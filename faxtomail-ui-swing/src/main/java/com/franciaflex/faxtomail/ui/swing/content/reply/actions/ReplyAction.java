package com.franciaflex.faxtomail.ui.swing.content.reply.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyAttachmentModel;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUI;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUIModel;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class ReplyAction extends AbstractFaxToMailAction<ReplyFormUIModel, ReplyFormUI, ReplyFormUIHandler> {

    private static final Log log = LogFactory.getLog(ReplyAction.class);

    public ReplyAction(ReplyFormUIHandler handler) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.reply.tip"));
    }

    @Override
    public void doAction() throws Exception {
        ReplyFormUIModel model = getModel();
        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            EmailService emailService = serviceContext.getEmailService();
            DemandeUIModel originalDemand = getModel().getOriginalDemand();

            Collection<AttachmentFile> attachmentFiles = Collections2.transform(model.getAttachments(), new Function<ReplyAttachmentModel, AttachmentFile>() {
                @Override
                public AttachmentFile apply(ReplyAttachmentModel replyAttachmentModel) {
                    return replyAttachmentModel.getAttachmentFile();
                }
            });
            Email email = emailService.reply(model.getFrom(),
                    model.getTo(),
                    model.getCc(),
                    model.getCci(),
                    model.getSubject(),
                    model.getMessage(),
                    attachmentFiles,
                    originalDemand.getTopiaId(),
                    getContext().getCurrentUser());

            originalDemand.fromEntityIncluding(email, Sets.newHashSet(Email.PROPERTY_REPLIES, Email.PROPERTY_HISTORY));
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        handler.closeFrame();
    }
}
