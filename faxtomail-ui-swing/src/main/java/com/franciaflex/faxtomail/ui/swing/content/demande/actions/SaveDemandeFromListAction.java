package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.DemandeTableModel;
import org.jdesktop.swingx.JXTable;

import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class SaveDemandeFromListAction extends AbstractFaxToMailAction<DemandeListUIModel, DemandeListUI, DemandeListUIHandler> {

    protected String[] modifiedProperties;

    protected List<DemandeUIModel> selectedEmails;

    //Ligne de l'élément courant avant rafraichissement
    protected int selectedRowBeforeRefresh;

    //première ligne d'une multisélection avant rafraichissement
    protected int firstSelectedRowBeforeRefresh;

    public SaveDemandeFromListAction(DemandeListUIHandler handler) {
        super(handler, false);
    }

    public void setModifiedProperties(String... properties) {
        modifiedProperties = properties;
    }

    @Override
    public boolean prepareAction() throws Exception {

        //save selections before performing action
        DemandeListUIModel model = getModel();
        JXTable dataTable = getUI().getDataTable();
        DemandeTableModel dataTableModel = (DemandeTableModel) dataTable.getModel();
        selectedEmails = model.getSelectedEmails();
        firstSelectedRowBeforeRefresh = dataTable.getSelectedRow();

        //Si on travaille sur un current email, on sauve la ligne (pour resélection après refresh)
        DemandeUIModel currentEmail = getContext().getCurrentEmail();
        if (currentEmail != null) {
            selectedRowBeforeRefresh = dataTableModel.getRowIndex(currentEmail);
        }

        return super.prepareAction();
    }

    @Override
    public void doAction() throws Exception {
        FaxToMailUIContext context = getContext();

        try(FaxToMailServiceContext serviceContext = context.newServiceContext()) {
            EmailService emailService = serviceContext.getEmailService();

            for (DemandeUIModel model : getModel().getSelectedEmails()) {

                String topiaId = model.getTopiaId();
                Email persistedEmail = emailService.getFullEmailById(topiaId);

                FaxToMailUser currentUser = context.getCurrentUser();
                Email email = model.toEntity(persistedEmail);

                email = emailService.saveEmail(email, currentUser, modifiedProperties);
                model.fromEntity(email);
            }
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        DemandeUIModel currentEmail = getContext().getCurrentEmail();

        //Cherche à resélectionner ce qui était sélectionné avant le refresh (ou l'action provoquant le refresh)
        JXTable dataTable = getUI().getDataTable();
        DemandeTableModel dataTableModel = (DemandeTableModel) dataTable.getModel();

        //Si des emails sont sélectionnés, on les resélectionne
        if (selectedEmails != null && !selectedEmails.isEmpty()) {
            for (DemandeUIModel email : selectedEmails) {
                int row = dataTableModel.getRowIndex(email);
                if (row > 0) {
                    dataTable.addRowSelectionInterval(row, row);
                    dataTable.scrollRowToVisible(row);
                }
            }

            //si rien de sélectionné = la sélection a  disparu => re-sélectionne la première ligne de l'ancienne sélection
            if (dataTable.getSelectedRow() < 0 && firstSelectedRowBeforeRefresh > 0) {
                int rowSelected = Math.max(0, Math.min(dataTable.getRowCount() - 1, firstSelectedRowBeforeRefresh));
                dataTable.addRowSelectionInterval(rowSelected, rowSelected);
                dataTable.scrollRowToVisible(rowSelected);
            }

        } else if (currentEmail != null) {
            //on reselectionne le current email que si pas d'autre sélection (sinon rajoute un élément non sélectionné
            //à la sélection, potentiel commportement indésirable pour l'utilisateur )
            int row = dataTableModel.getRowIndex(currentEmail);
            if (row > 0) {
                dataTable.setRowSelectionInterval(row, row);
                dataTable.scrollRowToVisible(row);
            } else if (selectedRowBeforeRefresh > 0 && selectedRowBeforeRefresh<dataTable.getRowCount()) {
                //si on retrouve pas le current email, sélectionne la ligne d'avant refresh
                dataTable.setRowSelectionInterval(selectedRowBeforeRefresh, selectedRowBeforeRefresh);
                dataTable.scrollRowToVisible(selectedRowBeforeRefresh);
            } else if (selectedRowBeforeRefresh > 1 ){
                //si pas possible sélectionne la ligne précédent la ligne d'avant refresh
                int rowSelected = Math.max(0, Math.min(dataTable.getRowCount() - 1, firstSelectedRowBeforeRefresh - 1 ));
                dataTable.setRowSelectionInterval(rowSelected, rowSelected);
                dataTable.scrollRowToVisible(rowSelected);
            }
        }
        //si pas de sélection ici c'est qu'on ne peut plus rien faire

        getHandler().resetTimer();
    }
}
