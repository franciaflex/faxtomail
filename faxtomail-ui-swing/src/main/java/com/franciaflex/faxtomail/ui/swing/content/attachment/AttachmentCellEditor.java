package com.franciaflex.faxtomail.ui.swing.content.attachment;

/*
 * #%L
 * Tutti :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.HistoryType;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.DemandeTableModel;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUI;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;

/**
 * To edit attachments from a table cell.
 *
 * @author kmorin - morin@codelutin.com
 */
public class AttachmentCellEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(AttachmentCellEditor.class);

    public static TableCellEditor newEditor(FaxToMailUI ui) {

        return new AttachmentCellEditor(FaxToMailUIContext.getApplicationContext());
    }

    protected final ButtonAttachment editorButton;

    protected FaxToMailUIContext context;

    protected AttachmentListener listener;

    public AttachmentCellEditor(FaxToMailUIContext context) {
        this.context = context;

        this.editorButton = new ButtonAttachment("faxtomail.attachmentCellRenderer.text", null);
        this.editorButton.setBorder(new LineBorder(Color.BLACK));
        this.editorButton.setEditable(false);
        addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(ChangeEvent e) {
                editorButton.setSelected(false);
            }

            @Override
            public void editingCanceled(ChangeEvent e) {
                editorButton.setSelected(false);
            }
        });
    }

    @Override
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {
        DemandeTableModel tableModel = (DemandeTableModel) table.getModel();

        int modelRow = table.convertRowIndexToModel(row);
        final DemandeUIModel model = tableModel.getEntry(modelRow);

        editorButton.init(model);

        listener = new AttachmentListener() {
            @Override
            public void onAttachmentOpened(Attachment attachment, boolean original) {
                String topiaId = model.getTopiaId();
                if (topiaId != null) {
                    String filename;
                    if (original) {
                        filename = attachment.getOriginalFileName();
                    } else {
                        filename = FaxToMailUIUtil.getEditedFileName(attachment.getOriginalFileName());
                    }

                    try (FaxToMailServiceContext serviceContext = context.newServiceContext()) {
                        Email email = serviceContext.getEmailService().addToHistory(topiaId,
                                HistoryType.ATTACHMENT_OPENING,
                                context.getCurrentUser(),
                                new Date(),
                                filename);

                        // different cas:
                        // depuis la liste, on doit mettre à jour Email.PROPERTY_LAST_ATTACHMENT_OPENER
                        // depuis l'edition d'une demande, Email.PROPERTY_HISTORY
                        model.fromEntityIncluding(email, Sets.newHashSet(Email.PROPERTY_HISTORY, Email.PROPERTY_LAST_ATTACHMENT_OPENER));
                    } catch (IOException eee){
                        log.error("Cannot create history entry",eee);
                    }
                }
            }

            @Override
            public void onAttachmentEdited(Attachment attachment) {
                String topiaId = model.getTopiaId();
                try (FaxToMailServiceContext serviceContext = context.newServiceContext()) {
                    Email email = serviceContext.getEmailService().addToHistory(topiaId,
                            HistoryType.ATTACHMENT_MODIFICATION,
                            context.getCurrentUser(),
                            new Date(),
                            FaxToMailUIUtil.getEditedFileName(attachment.getOriginalFileName()));

                    // different cas:
                    // depuis la liste, on doit mettre à jour Email.PROPERTY_LAST_ATTACHMENT_OPENER
                    // depuis l'edition d'une demande, Email.PROPERTY_HISTORY
                    model.fromEntityIncluding(email, Sets.newHashSet(Email.PROPERTY_HISTORY, Email.PROPERTY_LAST_ATTACHMENT_OPENER));
                }  catch (IOException eee){
                    log.error("Cannot create history entry",eee);
                }
            }
        };

        editorButton.getBean().addAttachmentListener(listener);

        return editorButton;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    @Override
    public Object getCellEditorValue() {

        AttachmentEditorUIModel model = editorButton.getBean();
        Preconditions.checkNotNull(model, "No model found in editor.");

        List<Attachment> result = new ArrayList<>();
        if (model.getAttachment() != null) {
            result.addAll(model.getAttachment());
        }
        if (model.getInlineAttachments() != null) {
            result.addAll(model.getInlineAttachments());
        }
        if (log.isDebugEnabled()) {
            log.debug("editor value: " + result);
        }

        return result;
    }

    @Override
    public boolean stopCellEditing() {
        boolean b = super.stopCellEditing();
        if (b) {
            editorButton.getBean().removeAttachmentListener(listener);
            editorButton.init(null);
        }
        return b;
    }

    @Override
    public void cancelCellEditing() {
        editorButton.getBean().removeAttachmentListener(listener);
        editorButton.init(null);
        super.cancelCellEditing();
    }
}
