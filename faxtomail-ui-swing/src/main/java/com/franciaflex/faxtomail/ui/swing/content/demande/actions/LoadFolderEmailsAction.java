package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.DemandeTableModel;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.util.pagination.PaginationResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Action de chargement des demandes d'un dossier lors de la selection d'un dossier dans l'arbre
 * des dossiers.
 * 
 * @author Kevin Morin (Code Lutin)
 */
public class LoadFolderEmailsAction extends AbstractFaxToMailAction<DemandeListUIModel, DemandeListUI, DemandeListUIHandler> {

    private final static Log log = LogFactory.getLog(LoadFolderEmailsAction.class);

    protected List<DemandeUIModel> selectedEmails;

    //Ligne de l'élément courant avant rafraichissement
    protected int selectedRowBeforeRefresh;

    //première ligne d'une multisélection avant rafraichissement
    protected int firstSelectedRowBeforeRefresh;

    public LoadFolderEmailsAction(DemandeListUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result;

        DemandeListUIModel model = getModel();
        MailFolder folder = model.getSelectedFolder();

        //save selections before refresh
        JXTable dataTable = getUI().getDataTable();
        DemandeTableModel dataTableModel = (DemandeTableModel) dataTable.getModel();
        selectedEmails = model.getSelectedEmails();
        firstSelectedRowBeforeRefresh = dataTable.getSelectedRow();

        //Si on travaille sur un current email, on sauve la ligne (pour resélection après refresh)
        DemandeUIModel currentEmail = getContext().getCurrentEmail();
        if (currentEmail != null) {
            selectedRowBeforeRefresh = dataTableModel.getRowIndex(currentEmail);
        }

        if (folder == null) {
            result = false;

        } else {
            //FIXME enlever la rustine
            FaxToMailUIContext.FolderData currentFolderEmails = getContext().getCurrentFolderEmails();
            if (getContext().isUseFolderCache() && folder.equals(currentFolderEmails.getFolder())) {
                model.setEmails(new ArrayList<>(currentFolderEmails.getDemands()));
                PaginationResult<Email> paginationResult = currentFolderEmails.getPaginationResult();
                model.setPaginationParameter(paginationResult.getCurrentPage());
                model.setPaginationResult(paginationResult);
                result = false;

            } else {
                setActionDescription(t("faxtomail.action.loadFolderEmails.tip", folder.getName()));

                // change name to save the state of the column width for every folder
                // (as every folder can have a different header)
                getContext().getSwingSession().updateState();

                MailFolder folderWithCanChangeOrderInTable = folder;
                while (folderWithCanChangeOrderInTable.getCanChangeOrderInTable() == null
                       && folderWithCanChangeOrderInTable.getParent() != null) {
                    folderWithCanChangeOrderInTable = folderWithCanChangeOrderInTable.getParent();
                }
                boolean sortable = Boolean.TRUE.equals(folderWithCanChangeOrderInTable.getCanChangeOrderInTable());
                String columns = handler.populateColumnModel(dataTable, sortable);

                // on change de nom en fonction du dossier pour pouvoir sauvegarder le tri des colonnes
                // dans la session en fonction du dossier
                //JC-180220- if instruction is a hack for  #9713
                if (getContext().getLastDisplayedMailFolder() != null && !getContext().getLastDisplayedMailFolder().equals("demandeListUI" + folder.getTopiaId())){
                    getUI().setName("demandeListUI" + folder.getTopiaId());
                    getContext().getSwingSession().add(getUI(), true);
                    getContext().setLastDisplayedMailFolder("demandeListUI" + folder.getTopiaId());
                }

                // on force l'ajout du splitpane car l'ui de liste change de nom en fonction du dossier
                // mais on veut que l'arbre et le séparateur soient constants
                getContext().getSwingSession().add(getUI().getMainSplitPane(), true);

                // on change de nom en fonction des colonnes pour pouvoir sauvegarder la taille des colonnes
                // dans la session en fonction des colonnes affichées
                dataTable.setName("dataTable" + columns);
                getContext().getSwingSession().add(dataTable, true);

                // fixes #5528 filtre sur les colonnes : il manque l'icone qui indique que la colonne est filtrée
                handler.updateEmailFilterWithContextEmailFilter();
                model.resetOrderPaginationParameter();
                result = true;
            }
        }

        getHandler().updateColorizeInvalidDemands();

        return result;
    }

    @Override
    public void doAction() throws Exception {
        DemandeListUIHandler handler = getHandler();
        DemandeListUIModel model = getModel();
        MailFolder folder = model.getSelectedFolder();
        FaxToMailUser currentUser = getContext().getCurrentUser();

        try (FaxToMailServiceContext faxToMailServiceContext = getContext().newServiceContext()) {
            PaginationResult<Email> paginationResult = faxToMailServiceContext.getEmailService()
                    .getEmailForFolder(folder,
                            currentUser,
                            getModel().getEmailFilter(),
                            model.getPaginationParameter());
            List<Email> emails = paginationResult.getElements();
            if (log.isDebugEnabled()) {
                log.debug(emails.size() + " emails in folder " + folder.getName());
            }

            Map<String, WaitingState> waitingStateById = Maps.uniqueIndex(getContext().getWaitingStateCache(), TopiaEntities.getTopiaIdFunction());

            List<DemandeUIModel> demands = new ArrayList<DemandeUIModel>();
            for (Email email : emails) {
                DemandeUIModel demand = new DemandeUIModel();
                demand.fromEntityExcluding(email, Sets.newHashSet(Email.PROPERTY_MAIL_FOLDER, Email.PROPERTY_HISTORY));
                demand.recomputeValidRangeRows();

                demand.setMailFolder(folder);
                demand.setEditable(folder.isFolderWritable() && demand.getDemandStatus().isEditableStatus());
                WaitingState waitingState = email.getWaitingState();
                if (waitingState != null) {
                    demand.setWaitingState(waitingStateById.get(waitingState.getTopiaId()));
                }
                demand.setValid(handler.isDemandeValid(demand));
                demands.add(demand);
            }

            //FIXME enlever la rustine
            if (getContext().isUseFolderCache()) {
                FaxToMailUIContext.FolderData currentFolderEmails = getContext().getCurrentFolderEmails();
                currentFolderEmails.setFolder(folder);
                currentFolderEmails.setDemands(demands);
                currentFolderEmails.setPaginationResult(paginationResult);
            }

            model.setEmails(demands);
            model.setPaginationParameter(paginationResult.getCurrentPage());
            model.setPaginationResult(paginationResult);
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        DemandeUIModel currentEmail = getContext().getCurrentEmail();

        //Cherche à resélectionner ce qui était sélectionné avant le refresh (ou l'action provoquant le refresh)
        JXTable dataTable = getUI().getDataTable();
        DemandeTableModel dataTableModel = (DemandeTableModel) dataTable.getModel();

        //Si des emails sont sélectionnés, on les resélectionne
        if (selectedEmails != null && !selectedEmails.isEmpty()) {
            for (DemandeUIModel email : selectedEmails) {
                int row = dataTableModel.getRowIndex(email);
                if (row > 0) {
                    dataTable.addRowSelectionInterval(row, row);
                    dataTable.scrollRowToVisible(row);
                }
            }

            //si rien de sélectionné = la sélection a  disparu => re-sélectionne la première ligne de l'ancienne sélection
            if (dataTable.getSelectedRow() < 0 && firstSelectedRowBeforeRefresh > 0 && firstSelectedRowBeforeRefresh < dataTable.getRowCount()) {
                    dataTable.addRowSelectionInterval(firstSelectedRowBeforeRefresh, firstSelectedRowBeforeRefresh);
                    dataTable.scrollRowToVisible(firstSelectedRowBeforeRefresh);
            }

        } else if (currentEmail != null) {
            //on reselectionne le current email que si pas d'autre sélection (sinon rajoute un élément non sélectionné
            //à la sélection, potentiel commportement indésirable pour l'utilisateur )
            int row = dataTableModel.getRowIndex(currentEmail);
            if (row > 0) {
                dataTable.setRowSelectionInterval(row, row);
                dataTable.scrollRowToVisible(row);
            } else if (selectedRowBeforeRefresh > 0 && selectedRowBeforeRefresh<dataTable.getRowCount()) {
                //si on retrouve pas le current email, sélectionne la ligne d'avant refresh
                dataTable.setRowSelectionInterval(selectedRowBeforeRefresh, selectedRowBeforeRefresh);
                dataTable.scrollRowToVisible(selectedRowBeforeRefresh);
            } else if (selectedRowBeforeRefresh > 1 && selectedRowBeforeRefresh<dataTable.getRowCount()+1 ){
                //si pas possible sélectionne la ligne précédent la ligne d'avant refresh
                dataTable.setRowSelectionInterval(selectedRowBeforeRefresh-1, selectedRowBeforeRefresh-1);
                dataTable.scrollRowToVisible(selectedRowBeforeRefresh-1);
            }
        }
        //si pas de sélection ici c'est qu'on ne peut plus rien faire

        getHandler().resetTimer();
    }

    @Override
    protected void releaseAction() {
        super.releaseAction();
        getUI().getNavigationTree().setEnabled(true);
    }
}
