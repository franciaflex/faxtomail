package com.franciaflex.faxtomail.ui.swing.content.demande.demandgroup;

/*
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.util.CloseButton;
import jaxx.runtime.SwingUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 1.3
 */
public class DemandGroupHighlightDialog extends JDialog {

    protected final ButtonEmailGroup buttonEmailGroup;
    protected boolean popupMoving;
    protected final Timer timer;

    public DemandGroupHighlightDialog(FaxToMailUIContext context, ButtonEmailGroup buttonEmailGroup) {
        super(SwingUtil.getParentContainer(buttonEmailGroup, JFrame.class));

        this.buttonEmailGroup = buttonEmailGroup;

        int delayGroupedDemandWarningDialog = context.getConfig().getDelayGroupedDemandWarningDialog();

        if (delayGroupedDemandWarningDialog > 0) {

            timer = new Timer(delayGroupedDemandWarningDialog, new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });

        } else {
            timer = null;
        }

        init(context.getConfig().getColorGroupedDemandWarningDialog(),
             context.getConfig().getFontSizeGroupedDemandWarningDialog());
    }

    protected void init(Color bgColor, float fontSize) {
        setUndecorated(true);
        setAlwaysOnTop(true);
        setResizable(false);

        getContentPane().setLayout(new BorderLayout());

        JLabel jLabel = new JLabel(t("faxtomail.demandGroup.warning.label"));
        jLabel.setFont(jLabel.getFont().deriveFont(Font.BOLD, fontSize));

        JPanel messagePanel = new JPanel();
        messagePanel.setBackground(bgColor);
        messagePanel.add(jLabel);
        messagePanel.add(new CloseButton(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        }));

        getContentPane().add(messagePanel, BorderLayout.CENTER);

        getContentPane().add(new ArrowPanel(bgColor), BorderLayout.EAST);

        pack();

        buttonEmailGroup.addHierarchyBoundsListener(new HierarchyBoundsAdapter() {

            @Override
            public void ancestorMoved(HierarchyEvent e) {
                if (isShowing()) {

                    // place dialog just under the button
                    popupMoving = true;
                    try {
                        place();

                    } finally {
                        popupMoving = false;
                    }
                }
            }
        });
    }

    protected void place() {
        Point point = new Point(buttonEmailGroup.getLocationOnScreen());
        point.translate(- getWidth() - 5, (buttonEmailGroup.getHeight() - getHeight()) / 2);
        setLocation(point.getLocation());
    }

    @Override
    public void setVisible(boolean b) {
        if (!isVisible() && b) {
            place();

            if (timer != null) {
                timer.start();
            }

        } else if (!b && timer != null) {
            timer.stop();
        }

        super.setVisible(b);

    }

    private class ArrowPanel extends JPanel {
        private final int pannelWidth = 5;
        private final int pannelHeight = 10;
        private final Color arrowColor;

        ArrowPanel(Color color) {
            arrowColor = color;
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(pannelWidth, pannelHeight);
        }

        @Override
        public Color getBackground() {
            return null;
        }

        @Override
        public void paint(Graphics g) {
            Point point1 = new Point(0, (getHeight() + pannelHeight) / 2 );
            Point point2 = new Point(point1.x, point1.y - pannelHeight);
            Point point3 = new Point(point1.x + pannelWidth, point1.y - pannelHeight / 2);

            g.setColor(arrowColor);
            g.drawLine(point1.x, point1.y, point2.x, point2.y);
            g.drawLine(point1.x, point1.y, point3.x, point3.y);
            g.drawLine(point2.x, point2.y, point3.x, point3.y);
            g.fillPolygon(new int[]{point1.x, point2.x, point3.x}, new int[]{point1.y, point2.y, point3.y}, 3);
        }
    }

}
