package com.franciaflex.faxtomail.ui.swing.content.search;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.HasLabel;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.SearchFilter;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.search.actions.GroupAction;
import com.franciaflex.faxtomail.ui.swing.content.search.actions.SearchToGroupAction;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailDemandListHandler;
import com.franciaflex.faxtomail.ui.swing.util.DemandeTableModel;
import com.franciaflex.faxtomail.ui.swing.util.PaginationComboModel;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.nuiton.jaxx.application.swing.util.Cancelable;
import org.nuiton.util.pagination.PaginationParameter;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class SearchToGroupUIHandler extends AbstractFaxToMailDemandListHandler<SearchUIModel, SearchToGroupUI>
                                    implements Cancelable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SearchToGroupUIHandler.class);

    protected List<MailField> tableColumns;

    @Override
    public void beforeInit(SearchToGroupUI ui) {
        super.beforeInit(ui);

        SearchUIModel searchUIModel = new SearchUIModel();
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -2);
        Date twoMonthsAgo = cal.getTime();
        searchUIModel.setMaxReceptionDate(now);
        searchUIModel.setMinReceptionDate(twoMonthsAgo);

        List<Client> allowedClients = ui.getContextValue(List.class, SearchUIModel.PROPERTY_ALLOWED_CLIENTS);
        searchUIModel.setAllowedClients(allowedClients);

        getUI().setContextValue(searchUIModel);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    @Override
    public void afterInit(SearchToGroupUI ui) {
        initUI(ui);

        List<FaxToMailUser> users = getContext().getFaxtomailUserCache();
        SearchUIModel model = getModel();
        initBeanFilterableComboBox(ui.getTakenByComboBox(),
                                   users,
                                   model.getTakenBy());
        initBeanFilterableComboBox(ui.getModifiedByComboBox(),
                                   users,
                                   model.getModifiedBy());
        initBeanFilterableComboBox(ui.getArchivedByComboBox(),
                                   users,
                                   model.getArchivedBy());
        initBeanFilterableComboBox(ui.getTransferByComboBox(),
                                   users,
                                   model.getTransferBy());
        initBeanFilterableComboBox(ui.getPrintedByComboBox(),
                                   users,
                                   model.getPrintingBy());
        initBeanFilterableComboBox(ui.getRepliedByComboBox(),
                                   users,
                                   model.getReplyBy());

        BeanFilterableComboBox<Client> clientComboBox = ui.getClientComboBox();
        clientComboBox.getComboBoxModel().setWildcardCharacter(null);
        initBeanFilterableComboBox(clientComboBox,
                                   model.getAllowedClients(),
                                   model.getClient());

        initCheckBoxComboBox(ui.getDocTypeComboBox(),
                             getContext().getDemandTypeCache(),
                             model.getDemandType(),
                             SearchFilter.PROPERTY_DEMAND_TYPE,
                             true);
        initCheckBoxComboBox(ui.getPriorityComboBox(),
                             getContext().getPriorityCache(),
                             model.getPriority(),
                             SearchFilter.PROPERTY_PRIORITY,
                             true);
        initCheckBoxComboBox(ui.getWaitingStateComboBox(),
                             getContext().getWaitingStateCache(),
                             model.getWaitingStates(),
                             SearchFilter.PROPERTY_WAITING_STATES,
                             true);
        initCheckBoxComboBox(ui.getStatusComboBox(),
                             Arrays.asList(DemandStatus.values()),
                             model.getDemandStatus(),
                             SearchFilter.PROPERTY_DEMAND_STATUS,
                             false);
        initCheckBoxComboBox(ui.getGammeComboBox(),
                             getContext().getRangeCache(),
                             model.getGamme(),
                             SearchFilter.PROPERTY_GAMME,
                             false);

        final JXTable dataTable = getUI().getDataTable();
        dataTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        updateColorizeInvalidDemands();
        initDemandeTable(dataTable, true);

        model.addPropertyChangeListener(SearchUIModel.PROPERTY_RESULTS, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                List<DemandeUIModel> emails = (List<DemandeUIModel>) evt.getNewValue();
                DemandeTableModel dataTableModel = (DemandeTableModel) dataTable.getModel();
                dataTableModel.setRows(emails);

                DemandeUIModel currentEmail = getContext().getCurrentEmail();
                if (currentEmail != null) {
                    int row = dataTableModel.getRowIndex(currentEmail);
                    if (row > 0) {
                        dataTable.setRowSelectionInterval(row, row);
                    }
                }
            }
        });

        dataTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    ListSelectionModel selectionModel = (ListSelectionModel) e.getSource();
                    SearchUIModel model = getModel();
                    int selectedIndex = selectionModel.getLeadSelectionIndex();
                    selectedIndex = dataTable.convertRowIndexToModel(selectedIndex);
                    DemandeTableModel dataTableModel = (DemandeTableModel) dataTable.getModel();
                    DemandeUIModel selectedModel = selectedIndex < 0 ? null : dataTableModel.getEntry(selectedIndex);
                    model.setGroupEnabled(!selectionModel.isSelectionEmpty() && isDemandSelectable(selectedModel));
                }
            }
        });

        HighlightPredicate rowAlreadyInGroupPredicate = new HighlightPredicate() {
            @Override
            public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
                int viewRow = adapter.row;
                int modelRow = adapter.convertRowIndexToModel(viewRow);
                DemandeTableModel dataTableModel = (DemandeTableModel) dataTable.getModel();
                DemandeUIModel row = dataTableModel.getEntry(modelRow);
                return !isDemandSelectable(row);
            }
        };
        dataTable.addHighlighter(new ColorHighlighter(rowAlreadyInGroupPredicate, null, Color.GRAY, Color.GRAY, Color.WHITE));

        // int combo box for result per page
        ui.getResultPerPageCombo().setModel(new PaginationComboModel());
        int resultPerPage = getConfig().getResultPerPage();
        ui.getModel().setResultPerPage(resultPerPage);
        ui.getResultPerPageCombo().setSelectedItem(resultPerPage);
        ui.getResultPerPageCombo().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                updateResultPerPage(e);
            }
        });

        getContext().getSwingSession().add(ui, true);
    }


    @Override
    protected void initTextField(JTextField jTextField) {
        super.initTextField(jTextField);
        jTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchDemandes();
            }
        });
    }

    @Override
    protected <HL extends HasLabel> void initCheckBoxComboBox(JComboBox<HL> comboBox, List<HL> values, List<HL> selection, String property, boolean addNull) {
        super.initCheckBoxComboBox(comboBox, values, selection, property, addNull);
        comboBox.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    searchDemandes();
                }
            }
        });
    }

    @Override
    protected void initDatePicker(JXDatePicker picker) {
        super.initDatePicker(picker);
        picker.getEditor().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    searchDemandes();
                }
            }
        });
    }

    public void searchDemandes() {
        getModel().resetPaginationParameter();
        runSearchAction();
    }

    public void goToNextPage() {
        getModel().setPaginationParameter(getModel().getPaginationResult().getNextPage());
        runSearchAction();
    }
    
    public void goToPreviousPage() {
        getModel().setPaginationParameter(getModel().getPaginationResult().getPreviousPage());
        runSearchAction();
    }
    
    public void updateResultPerPage(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            int resultPerPage = (Integer)event.getItem();
            getConfig().setResultPerPage(resultPerPage);
            getConfig().save();
            getModel().setResultPerPage(resultPerPage);
            getModel().resetPaginationParameter();
            if (CollectionUtils.isNotEmpty(getModel().getResults())) {
                runSearchAction();
            }
        }
    }
    
    protected void runSearchAction() {
        FaxToMailUIContext context = getContext();
        PaginationParameter currentPaginationParameter = context.getCurrentPaginationParameter();
        if (currentPaginationParameter != null) {
            getModel().setPaginationParameter(currentPaginationParameter);
            context.setCurrentPaginationParameter(null);
        }

        try {
            SearchToGroupAction searchAction = new SearchToGroupAction(this);
            if (context.isActionInProgress(null)) {
                context.getActionEngine().runFullInternalAction(searchAction);
            } else {
                context.getActionEngine().runAction(searchAction);
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("error while searching", e);
            }
            context.getErrorHelper().showErrorDialog(t("faxtomail.search.action.error"));
        }
    }

    @Override
    public List<MailField> getColumns() {
        if (tableColumns == null) {
            try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
                tableColumns = serviceContext.getConfigurationService().getSearchDisplayColumns();
            } catch (IOException eee) {
                log.error("Error getting configuration",eee);
            }
        }
        return tableColumns;
    }

    @Override
    protected boolean isDisplayOnlyUserTrigraphInTables() {
        return false;
    }

    protected boolean isDemandSelectable(DemandeUIModel demand) {
        DemandeUIModel currentDemand = getContext().getCurrentEmail();
        return !currentDemand.equals(demand) && !currentDemand.getGroupedDemandes().contains(demand);
    }

    @Override
    protected MouseListener getDataTableMouseListener() {
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                    group();
                }
            }
        };
    }

    @Override
    protected MailField[] getEditableTableProperties() {
        return new MailField[0];
    }

    @Override
    public void onCloseUI() {
    }

    @Override
    public SwingValidator<SearchUIModel> getValidator() {
        return null;
    }

    @Override
    public void cancel() {
        getContext().getSwingSession().updateState();
        closeFrame();
    }

    @Override
    public Component getTopestUI() {
        return getUI();
    }

    public void group(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            group();
        }
    }

    public void group() {
        if (getModel().isGroupEnabled()) {
            getContext().getActionEngine().runAction(new GroupAction(this));
        }
    }

    @Override
    public void reloadList() {
        searchDemandes();
    }
}
