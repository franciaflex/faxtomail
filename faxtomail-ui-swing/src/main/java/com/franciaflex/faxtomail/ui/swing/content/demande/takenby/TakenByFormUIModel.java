package com.franciaflex.faxtomail.ui.swing.content.demande.takenby;

/*-
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.List;

public class TakenByFormUIModel extends AbstractSerializableBean {

    public static final String PROPERTY_TAKENBY = "takenBy";

    public static final String PROPERTY_DEMANDSTOTAKE = "demandsToTake";

    public static final String PROPERTY_USERS = "users";

    public static final String PROPERTY_VALID = "valid";

    protected FaxToMailUser takenBy;

    protected List<DemandeUIModel> demandsToTake;

    protected List<FaxToMailUser> users;

    protected boolean valid;

    public FaxToMailUser getTakenBy() {
        return this.takenBy;
    }

    public void setTakenBy(FaxToMailUser takenBy){
        Object oldValue = getTakenBy();
        this.takenBy = takenBy;
        firePropertyChange(PROPERTY_TAKENBY, oldValue, takenBy);
    }

    public List<DemandeUIModel> getDemandsToTake() {
        return demandsToTake;
    }

    public void setDemandsToTake(List<DemandeUIModel> demandsToTake) {
        Object oldValue = getDemandsToTake();
        this.demandsToTake = demandsToTake;
        firePropertyChange(PROPERTY_DEMANDSTOTAKE, oldValue, demandsToTake);
    }

    public List<FaxToMailUser> getUsers() {
        return users;
    }

    public void setUsers(List<FaxToMailUser> users) {
        Object oldValue = getUsers();
        this.users = users;
        firePropertyChange(PROPERTY_USERS, oldValue, users);
    }

    public Boolean isValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        Object oldValue = isValid();
        this.valid = valid;
        firePropertyChange(PROPERTY_VALID, oldValue, valid);
    }
}
