package com.franciaflex.faxtomail.ui.swing.actions;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.RunFaxToMail;
import com.franciaflex.faxtomail.ui.swing.content.MainUIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * To reload FaxToMail application.
 *
 * @author kmorin - morin@codelutin.com
 */
public class ReloadFaxToMailAction extends AbstractChangeScreenAction {

    public ReloadFaxToMailAction(MainUIHandler handler) {
        super(handler, true, null);
        setActionDescription(t("faxtomail.action.reloadFaxToMail"));
    }

    @Override
    public void doAction() throws Exception {

        // Close the application, will exit and restart application
        RunFaxToMail.closeFaxToMail(getHandler(), RunFaxToMail.UPATE_EXIT_CODE);
    }

}
