package com.franciaflex.faxtomail.ui.swing.content;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.FaxToMailScreen;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.RunFaxToMail;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUI;
import com.franciaflex.faxtomail.ui.swing.content.search.SearchUI;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailDemandListHandler;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUI;
import com.franciaflex.faxtomail.ui.swing.util.RemoveablePropertyChangeListener;
import com.google.common.base.Preconditions;
import jaxx.runtime.JAXXBinding;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.swing.session.SwingSession;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.jaxx.application.swing.action.ApplicationActionUI;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeListenerProxy;
import java.io.IOException;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * @author kmorin - morin@codelutin.com
 *
 */
public class MainUIHandler extends AbstractFaxToMailUIHandler<FaxToMailUIContext, MainUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MainUIHandler.class);

    protected JComponent currentBody;

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void beforeInit(MainUI ui) {
        super.beforeInit(ui);
        FaxToMailUIContext context = getContext();
        ui.setContextValue(context);
        context.setMainUI(ui);
        context.setActionUI(new ApplicationActionUI(ui, context));

        context.addPropertyChangeListener(new RemoveablePropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if (propertyName.equals(FaxToMailUIContext.PROPERTY_SCREEN)) {
                    setScreen((FaxToMailScreen) evt.getNewValue());
                }
            }
        });
        ui.setContextValue(ui, MainUI.class.getName());

        ui.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                getContext().setActionUI(new ApplicationActionUI(getUI(), getContext()));
            }
        });

        // ecoute des changements de l'état busy
        context.addPropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, new RemoveablePropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Boolean newvalue = (Boolean) evt.getNewValue();
                updateBusyState(newvalue != null && newvalue);
            }
        });

        // ecoute des changements de l'état busy
        context.addPropertyChangeListener(FaxToMailUIContext.PROPERTY_HIDE_BODY, new RemoveablePropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Boolean newvalue = (Boolean) evt.getNewValue();
                if (getUI() != null && getUI().getBody() != null) {
                    getUI().getBody().setVisible(newvalue != null && newvalue);
                }
            }
        });
    }

    protected void updateBusyState(boolean busy) {
        if (busy) {
            // ui bloquee
            if (log.isDebugEnabled()) {
                log.debug("block ui in busy mode");
            }
            ui.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        } else {
            // ui debloquee
            if (log.isDebugEnabled()) {
                log.debug("unblock ui in none busy mode");
            }
            ui.setCursor(Cursor.getDefaultCursor());
        }
    }

    @Override
    public void afterInit(MainUI ui) {

        initUI(ui);

        getUI().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (quitCurrentScreen()) {
                    RunFaxToMail.closeFaxToMail(MainUIHandler.this, RunFaxToMail.NORMAL_EXIT_CODE);
                }
            }
        });

        // Init SwingSession
        SwingSession swingSession = getContext().getSwingSession();
        swingSession.add(ui);
        try {
            swingSession.save();

        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("error while saving the swing session", e);
            }
        }

        getContext().addPropertyChangeListener(FaxToMailUIContext.PROPERTY_ACTION_IN_PROGRESS, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                boolean inProgress = (boolean) evt.getNewValue();
                MainUI ui = getUI();
                ui.getSearchButton().setEnabled(!inProgress);
                ui.getDemandListButton().setEnabled(!inProgress);
            }
        });

        changeTitle();

        JToolBar bar = ui.getBottomBar();
        ui.getStatus().addWidget(bar, 0);

        getContext().setScreen(FaxToMailScreen.LIST);

        // désactivation de l'onglet gestion des fax si aucun dossier
        // l'utilisateur a peut etre seulement les droits sur les archives
        DemandeListUI demandeListUI = (DemandeListUI) getCurrentBody();
        if (CollectionUtils.isEmpty(demandeListUI.getModel().getFolders())) {
            getUI().getDemandListButton().setVisible(false);
            getContext().setScreen(FaxToMailScreen.SEARCH);
        } else {
            // on force l'ajout du splitpane car l'ui de liste change de nom en fonction du dossier
            // pour pouvoir sauvegarder le tri des colonnes dans la session en fonction du dossier
            // mais on veut que l'arbre et le séparateur soient constants
            getContext().getSwingSession().add(demandeListUI.getMainSplitPane(), true);
        }
    }

    @Override
    protected JComponent getComponentToFocus() {
        return currentBody;
    }

    @Override
    public void onCloseUI() {

        FaxToMailUIContext context = getContext();

        // remove any screen
        context.setScreen(null);
        context.removeMessageNotifier(this);

        // clean context

        PropertyChangeListener[] propertyChangeListeners =
                context.getPropertyChangeListeners();
        for (PropertyChangeListener listener : propertyChangeListeners) {
            if (listener instanceof PropertyChangeListenerProxy) {
                PropertyChangeListenerProxy proxy = (PropertyChangeListenerProxy) listener;
                listener = proxy.getListener();
            }
            if (listener instanceof RemoveablePropertyChangeListener) {
                if (log.isDebugEnabled()) {
                    log.debug("Remove listener: " + listener);
                }
                context.removePropertyChangeListener(listener);
            }
        }

        if (ui != null) {

            // clean ui

            JAXXBinding[] bindings = ui.getDataBindings();
            for (JAXXBinding binding : bindings) {
                JAXXUtil.removeDataBinding(ui, binding.getId());
            }
            ui.setVisible(false);
            ui.dispose();
        }
    }

    @Override
    public SwingValidator<FaxToMailUIContext> getValidator() {
        return null;
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public void reloadUI() {

        //close ui
        onCloseUI();

        // restart ui
        RunFaxToMail.startFaxToMail(getContext(), false);
    }

    public boolean acceptLocale(Locale l, String expected) {
        return l != null && l.toString().equals(expected);
    }

    public void changeLocale(Locale locale) {

        // change locale (and save configuration)
        getModel().setLocale(locale);

        // change i18n locale
        I18n.setDefaultLocale(getConfig().getI18nLocale());

        // close reload
        reloadUI();
    }

    @Override
    public final void showInformationMessage(String message) {
        ui.getStatus().setStatus("<html><body>" + message + "</body></html>");
    }

    public boolean quitCurrentScreen() {

        boolean canClose;
        if (getContext().getScreen() == null || currentBody == null) {

            // no screen, surely can quit
            canClose = true;
            if (log.isWarnEnabled()) {
                log.warn("==================================================");
                log.warn("No screen, Should then skipCheckCurrent in action.");
                log.warn("==================================================");
            }
        } else {
            FaxToMailUI<?, ?> body = (FaxToMailUI<?, ?>) currentBody;
            Preconditions.checkNotNull(currentBody);
            AbstractFaxToMailUIHandler<?, ?> handler = body.getHandler();
            if (handler instanceof CloseableUI) {

                // try to quit UI
                canClose = ((CloseableUI) handler).quitUI();
            } else {

                // can always close ui
                canClose = true;
            }
        }
        return canClose;
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void setScreen(FaxToMailScreen screen) {

        // close current body (if any)
        if (currentBody != null) {
            FaxToMailUI<?, ?> body = (FaxToMailUI<?, ?>) currentBody;
            body.getHandler().onCloseUI();

            try {
                getContext().getSwingSession().save();

            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("error while saving the swing session", e);
                }
            }

            ui.getBody().remove(currentBody);

            currentBody = null;
        }

        if (screen != null) {

            // load new body

            JComponent screenUI;
            JToolBar rightDecoration = null;
            String screenTitle;

            Icon icon;
            switch (screen) {
                case SEARCH:
                    // cf #6426, if a Nimbus ClassCastException, then retry to create the ui
                    // TODO remove this when we use the ng actions
                    try {
                        screenUI = new SearchUI(ui);

                    } catch (ClassCastException e) {
                        if (log.isErrorEnabled()) {
                            log.error("ClassCastException caught", e);
                        }
                        screenUI = new SearchUI(ui);
                    }
                    screenTitle = t("faxtomail.search.title");
                    icon = ui.getSearchButton().getIcon();
                    break;

                default:
                case LIST:
                    ui.getDemandListButton().setVisible(true);

                    // cf #6426, if a Nimbus ClassCastException, then retry to create the ui
                    // TODO remove this when we use the ng actions
                    try {
                        screenUI = new DemandeListUI(ui);

                    } catch (ClassCastException e) {
                        if (log.isErrorEnabled()) {
                            log.error("ClassCastException caught", e);
                        }
                        screenUI = new DemandeListUI(ui);
                    }
                    screenTitle = t("faxtomail.demandeList.title");
                    icon = ui.getDemandListButton().getIcon();
                    break;
            }

            this.currentBody = screenUI;
            getContext().getSwingSession().add(currentBody, true);
            ui.getBody().setTitle(screenTitle);
            ui.getBody().setLeftDecoration(new JLabel(icon));
            ui.getBody().setRightDecoration(rightDecoration);
            ui.getBody().add(currentBody);

        }
    }

    public void changeTitle() {

        ui.setTitle(t("faxtomail.main.title.application",
                      getConfig().getVersion()));
    }

    public void setBodyTitle(String title) {
        ui.getBody().setTitle(title);
    }

    public JComponent getCurrentBody() {
        return currentBody;
    }

    public void reloadDemandList() {
        if (log.isDebugEnabled()) {
            log.debug("Reload demand list");
        }
        ((FaxToMailUI<?, ? extends AbstractFaxToMailDemandListHandler>) currentBody).getHandler().reloadList();
    }
}
