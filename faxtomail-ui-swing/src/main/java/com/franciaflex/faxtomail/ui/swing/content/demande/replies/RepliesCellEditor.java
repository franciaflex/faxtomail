package com.franciaflex.faxtomail.ui.swing.content.demande.replies;

/*
 * #%L
 * Tutti :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.DemandeTableModel;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUI;
import com.google.common.base.Preconditions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.util.EventObject;

/**
 * To edit replies from a table cell.
 *
 * @author kmorin - morin@codelutin.com
 */
public class RepliesCellEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(RepliesCellEditor.class);

    public static TableCellEditor newEditor(FaxToMailUI ui) {
        return new RepliesCellEditor(FaxToMailUIContext.getApplicationContext());
    }

    protected final ButtonDemandReplies editorButton;

    protected FaxToMailUIContext context;

    public RepliesCellEditor(FaxToMailUIContext context) {
        this.context = context;

        this.editorButton = new ButtonDemandReplies("faxtomail.demandRepliesCellRenderer.text", null);
        this.editorButton.setBorder(new LineBorder(Color.BLACK));
        this.editorButton.setForward(false);
        addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(ChangeEvent e) {
                editorButton.setSelected(false);
            }

            @Override
            public void editingCanceled(ChangeEvent e) {
                editorButton.setSelected(false);
            }
        });
    }

    @Override
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {
        DemandeTableModel tableModel = (DemandeTableModel) table.getModel();

        int modelRow = table.convertRowIndexToModel(row);
        final DemandeUIModel model = (DemandeUIModel) tableModel.getEntry(modelRow);
        editorButton.init(model);

        return editorButton;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    @Override
    public Object getCellEditorValue() {

        DemandeUIModel model = editorButton.getBean();
        Preconditions.checkNotNull(model, "No model found in editor.");

        Object result = model.getReplies();
        if (log.isDebugEnabled()) {
            log.debug("editor value: " + result);
        }

        return result;
    }

    @Override
    public boolean stopCellEditing() {
        boolean b = super.stopCellEditing();
        if (b) {
            editorButton.init(null);
        }
        return b;
    }

    @Override
    public void cancelCellEditing() {
        editorButton.init(null);
        super.cancelCellEditing();
    }

}
