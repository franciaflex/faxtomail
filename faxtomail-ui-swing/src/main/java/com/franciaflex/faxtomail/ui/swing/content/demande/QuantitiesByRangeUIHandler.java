package com.franciaflex.faxtomail.ui.swing.content.demande;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableColumnModel;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class QuantitiesByRangeUIHandler extends AbstractFaxToMailUIHandler<QuantitiesByRangeUIModel, QuantitiesByRangeUI> implements Cancelable {

    protected RangeTableModel enCoursRangeTableModel;
    protected RangeTableModel enAttenteRangeTableModel;
    protected RangeTableModel totalsRangeTableModel;

    //should have 2 models, one for each table, possible ?

    @Override
    public void beforeInit(QuantitiesByRangeUI ui) {
        super.beforeInit(ui);

        QuantitiesByRangeUIModel dialogModel = new QuantitiesByRangeUIModel();
        dialogModel.setRootFolder(getContext().getCurrentMailFolder());
        this.ui.setContextValue(dialogModel);
    }

    @Override
    public void afterInit(QuantitiesByRangeUI quantitiesByRangeUI) {
        initUI(quantitiesByRangeUI);

        final JXTable enCoursRangeTable = ui.getEnCoursRangeTable();

        TableColumnModelExt columnModel = new DefaultTableColumnModelExt();
        addColumnToModel(columnModel,
                         null,
                         newTableCellRender(Range.class),
                         RangeTableModel.RANGE_COLUMN);
        addIntegerColumnToModel(columnModel,
                                RangeTableModel.QUOTATION_QUANTITY_COLUMN,
                                null,
                                enCoursRangeTable);
        addIntegerColumnToModel(columnModel,
                                RangeTableModel.PRODUCT_QUANTITY_COLUMN,
                                null,
                                enCoursRangeTable);
        addIntegerColumnToModel(columnModel,
                                RangeTableModel.SAV_QUANTITY_COLUMN,
                                null,
                                enCoursRangeTable);

        columnModel.getColumn(0).setPreferredWidth(120);

        enCoursRangeTableModel = new RangeTableModel(columnModel);
        /*enCoursRangeTableModel.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent tableModelEvent) {
                enCoursRangeTable.setPreferredScrollableViewportSize(enCoursRangeTable.getSize());

            }
        });*/
        enCoursRangeTableModel.setRows(getModel().getEnCoursRangeRows());
        enCoursRangeTable.setModel(enCoursRangeTableModel);
        enCoursRangeTable.setColumnModel(columnModel);
        enCoursRangeTable.getTableHeader().setReorderingAllowed(false);
        enCoursRangeTable.getTableHeader().setVisible(true);

        ui.getEnCoursTablePanel().add(enCoursRangeTable.getTableHeader(),BorderLayout.NORTH);

        enCoursRangeTable.getTableHeader().setResizingAllowed(false);

        //enCoursRangeTable.setPreferredScrollableViewportSize(enCoursRangeTable.getSize());
        //enCoursRangeTable.setPreferredScrollableViewportSize(enCoursRangeTable.ca);

        //final JScrollPane scrollPane = ui.getScrollPane();
        //scrollPane.setBorder(null);
        //scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        //Hack to display full table : rowCount + 2 to get subTotal and header ; 19 is row size
        //scrollPane.setMinimumSize(new Dimension(0,(enCoursRangeTableModel.getRowCount()+2)*19));

        getModel().addPropertyChangeListener(QuantitiesByRangeUIModel.PROPERTY_ENCOURS_RANGE_ROWS,
                                             new PropertyChangeListener() {
                                                 @Override
                                                 public void propertyChange(PropertyChangeEvent evt) {
                                                     setEnCoursRangeRows((List<RangeRowModel>) evt.getNewValue());
                                                     //enCoursRangeTable.setPreferredScrollableViewportSize(enCoursRangeTable.getSize());
                                                 }
                                             });

        final JXTable enAttenteRangeTable = ui.getEnAttenteRangeTable();

        TableColumnModelExt enAttenteColumnModel = new DefaultTableColumnModelExt();
        addColumnToModel(enAttenteColumnModel,
                null,
                newTableCellRender(Range.class),
                RangeTableModel.RANGE_COLUMN);
        addIntegerColumnToModel(enAttenteColumnModel,
                RangeTableModel.QUOTATION_QUANTITY_COLUMN,
                null,
                enAttenteRangeTable);
        addIntegerColumnToModel(enAttenteColumnModel,
                RangeTableModel.PRODUCT_QUANTITY_COLUMN,
                null,
                enAttenteRangeTable);
        addIntegerColumnToModel(enAttenteColumnModel,
                RangeTableModel.SAV_QUANTITY_COLUMN,
                null,
                enAttenteRangeTable);

        enAttenteColumnModel.getColumn(0).setPreferredWidth(120);

        enAttenteRangeTableModel = new RangeTableModel(enAttenteColumnModel);
        enAttenteRangeTableModel.setRows(getModel().getEnAttenteRangeRows());
        enAttenteRangeTable.setModel(enAttenteRangeTableModel);
        enAttenteRangeTable.setColumnModel(enAttenteColumnModel);
        enAttenteRangeTable.getTableHeader().setReorderingAllowed(false);
        enAttenteRangeTable.getTableHeader().setResizingAllowed(false);

        getModel().addPropertyChangeListener(QuantitiesByRangeUIModel.PROPERTY_ENATTENTE_RANGE_ROWS,
                new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        setEnAttenteRangeRows((List<RangeRowModel>) evt.getNewValue());
                    }
                });



        final JXTable totalsRangeTable = ui.getTotalsRangeTable();

        TableColumnModelExt totalsColumnModel = new DefaultTableColumnModelExt();
        addColumnToModel(totalsColumnModel,
                null,
                newTableCellRender(Range.class),
                RangeTableModel.RANGE_COLUMN);
        addIntegerColumnToModel(totalsColumnModel,
                RangeTableModel.QUOTATION_QUANTITY_COLUMN,
                null,
                totalsRangeTable);
        addIntegerColumnToModel(totalsColumnModel,
                RangeTableModel.PRODUCT_QUANTITY_COLUMN,
                null,
                totalsRangeTable);
        addIntegerColumnToModel(totalsColumnModel,
                RangeTableModel.SAV_QUANTITY_COLUMN,
                null,
                totalsRangeTable);
        totalsColumnModel.getColumn(0).setPreferredWidth(120);

        totalsRangeTableModel = new RangeTableModel(totalsColumnModel);
        totalsRangeTableModel.setRows(getModel().getTotalsRangeRows());
        totalsRangeTable.setModel(totalsRangeTableModel);
        totalsRangeTable.setColumnModel(totalsColumnModel);
        totalsRangeTable.getTableHeader().setReorderingAllowed(false);
        totalsRangeTable.getTableHeader().setResizingAllowed(false);


        getModel().addPropertyChangeListener(QuantitiesByRangeUIModel.PROPERTY_TOTALS_RANGE_ROWS,
                new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        setTotalsRangeRows((List<RangeRowModel>) evt.getNewValue());
                    }
                });
    }

    @Override
    public SwingValidator<QuantitiesByRangeUIModel> getValidator() {
        return null;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    @Override
    public void onCloseUI() {

    }

    @Override
    public void cancel() {
        closeDialog();
    }

    public void setEnCoursRangeRows(List<RangeRowModel> rows) {
        enCoursRangeTableModel.setRows(rows);
    }

    public void setEnAttenteRangeRows(List<RangeRowModel> rows) {
        enAttenteRangeTableModel.setRows(rows);
    }
    public void setTotalsRangeRows(List<RangeRowModel> rows) {
        totalsRangeTableModel.setRows(rows);
    }
}
