package com.franciaflex.faxtomail.ui.swing.util.toolbar;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public abstract class AbstractToolbarPopupButton<UI extends AbstractToolbarPopupUI> extends JToggleButton {

    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(AbstractToolbarPopupButton.class);

    protected final UI popup;

    protected final WindowListener windowListener = new WindowAdapter() {

        @Override
        public void windowOpened(WindowEvent e) {
            setSelected(true);
        }

        @Override
        public void windowClosing(WindowEvent e) {
            setSelected(false);
        }

        @Override
        public void windowClosed(WindowEvent e) {
            setSelected(false);
        }

    };


    protected boolean popupMoving;

    public AbstractToolbarPopupButton() {

        ImageIcon actionIcon = SwingUtil.createActionIcon(getActionIcon());
        setIcon(actionIcon);
        setDisabledIcon(actionIcon);

        popup = createNewPopup();

        popup.addWindowListener(windowListener);

        addChangeListener(new ChangeListener() {

            // window focus listener to force the popup to be on top of the demand frame, but not on top of other windows
            final WindowAdapter windowAdapter = new WindowAdapter() {
                @Override
                public void windowGainedFocus(WindowEvent e) {
                    if (popup.isVisible()) {
                        if (log.isDebugEnabled()) {
                            log.debug("windowGainedFocus : popup always on top");
                        }
                        popup.setAlwaysOnTop(true);
                    }
                }

                @Override
                public void windowLostFocus(WindowEvent e) {
                    Window oppositeWindow = e.getOppositeWindow();
                    if (popup.isVisible() && !popup.equals(oppositeWindow)) {
                        if (log.isDebugEnabled()) {
                            log.debug("windowLostFocus : popup not always on top, bring to front opposite window");
                        }
                        popup.setAlwaysOnTop(false);

                        if (oppositeWindow != null) {
                            oppositeWindow.toFront();
                        }
                    }
                }
            };

            @Override
            public void stateChanged(ChangeEvent e) {
                JFrame buttonFrame = SwingUtil.getParentContainer(AbstractToolbarPopupButton.this, JFrame.class);
                if (isSelected()) {
                    if (!popup.isVisible()) {
                        popup.openEditor(AbstractToolbarPopupButton.this);
                        if (log.isDebugEnabled()) {
                            log.debug("stateChanged addWindowListener");
                        }
                        if (buttonFrame != null) {
                            buttonFrame.addWindowFocusListener(windowAdapter);
                        }
                    }
                } else {
                    if (popup.isVisible()) {
                        if (log.isDebugEnabled()) {
                            log.debug("stateChanged removeWindowListener");
                        }
                        if (buttonFrame != null) {
                            buttonFrame.removeWindowFocusListener(windowAdapter);
                        }
                    }
                    popup.closeEditor();
                }
            }
        });

        addHierarchyBoundsListener(new HierarchyBoundsAdapter() {

            @Override
            public void ancestorMoved(HierarchyEvent e) {
                if (popup.isShowing()) {

                    // place dialog just under the button
                    Point point = new Point(getLocationOnScreen());
                    point.translate(-popup.getWidth() + getWidth(), getHeight());
                    popupMoving = true;
                    try {
                        popup.setLocation(point);
                    } finally {
                        popupMoving = false;
                    }
                }
            }
        });
    }

    protected abstract String getActionIcon();

    protected abstract UI createNewPopup();

    public void onCloseUI() {
        popup.removeWindowListener(windowListener);
        popup.dispose();
        setSelected(false);
    }
}
