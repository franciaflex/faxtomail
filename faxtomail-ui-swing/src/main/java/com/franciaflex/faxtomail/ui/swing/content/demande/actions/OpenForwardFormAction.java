package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.SigningForDomain;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUI;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUIModel;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import jaxx.runtime.JAXXUtil;
import org.apache.commons.lang3.StringUtils;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 1.1
 */
public class OpenForwardFormAction extends AbstractFaxToMailAction<DemandeListUIModel, DemandeListUI, DemandeListUIHandler> {

    protected ReplyFormUI frameContent;
    protected JFrame frame;

    public OpenForwardFormAction(DemandeListUIHandler handler) {
        super(handler, false);
    }

    @Override
    public void doAction() throws Exception {
        DemandeUIModel demand = getModel().getSelectedEmails().get(0);

        if (StringUtils.isNotBlank(demand.getTopiaId())) {
            try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
                Email email = serviceContext.getEmailService().getFullEmailById(demand.getTopiaId(), getContext().getCurrentUser());
                demand.fromEntity(email);

                getContext().setCurrentEmail(demand);

                frameContent = new ReplyFormUI(getUI());
                ReplyFormUIModel model = frameContent.getModel();

                SigningForDomain signingForDomain = null;
                if (!demand.isFax() && demand.getRecipient() != null) {
                    signingForDomain = serviceContext.getConfigurationService().getSigningForEmailAddress(demand.getRecipient()).orNull();
                }
                model.setSigning(signingForDomain);

                model.setForward(true);
                model.setOriginalDemand(demand);

                String sender = JAXXUtil.getStringValue(demand.getSender());
                FaxToMailUIUtil.setFromOnReply(demand, model, sender, getConfig());

                frameContent.getHandler().addReferences();
            }
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        frame = getHandler().openModalFrame(frameContent,
                                            t("faxtomail.reply.forwardsubject", getContext().getCurrentEmail().getSubject()),
                                            new Dimension(800, 600));

        getContext().addPropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (!getContext().isBusy()) {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {

                            bringCurrentDemandToFront();
                            if (frame != null) {
                                frame.toFront();
                                frame = null;
                            }
                        }
                    });
                    getContext().removePropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, this);
                }
            }
        });
    }

}
