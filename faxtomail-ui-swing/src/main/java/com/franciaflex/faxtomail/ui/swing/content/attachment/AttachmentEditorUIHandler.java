package com.franciaflex.faxtomail.ui.swing.content.attachment;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.ui.swing.content.attachment.actions.EditAttachmentAction;
import com.franciaflex.faxtomail.ui.swing.content.attachment.actions.OpenAttachmentAction;
import com.franciaflex.faxtomail.ui.swing.util.toolbar.AbstractToolbarPopupHandler;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.google.common.collect.Iterables;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.HidorButton;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.HighlighterFactory;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * @author kmorin - morin@codelutin.com
 */
public class AttachmentEditorUIHandler extends AbstractToolbarPopupHandler<AttachmentEditorUIModel, AttachmentEditorUI> {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(AttachmentEditorUIHandler.class);

    @Override
    public void afterInit(AttachmentEditorUI ui) {

        ui.getFile().setDialogOwner(ui);
        super.afterInit(ui);

        initTable(ui.getAttachments(), getModel().getAttachment());
        initTable(ui.getInlineAttachments(), getModel().getInlineAttachments());

        ui.getInlineAttachmentsHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                getUI().pack();
            }
        });

        getModel().addPropertyChangeListener(AttachmentModelAware.PROPERTY_ATTACHMENT, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                AbstractTableModel tableModel = (AbstractTableModel) getUI().getAttachments().getModel();
                tableModel.fireTableDataChanged();
            }
        });

        getModel().addPropertyChangeListener(AttachmentEditorUIModel.PROPERTY_INLINE_ATTACHMENTS, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                AbstractTableModel tableModel = (AbstractTableModel) getUI().getInlineAttachments().getModel();
                tableModel.fireTableDataChanged();
            }
        });

    }

    protected void initTable(JXTable table, final Collection<Attachment> attachmentList) {

        TableModel attachmentTableModel = new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return CollectionUtils.size(attachmentList);
            }

            @Override
            public int getColumnCount() {
                return 1;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                return Iterables.get(attachmentList, rowIndex);
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return true;
            }
        };

        attachmentTableModel.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                getUI().pack();
            }
        });

        TableColumnModel columnModel = new DefaultTableColumnModel();
        TableColumn column = new TableColumn();
        column.setCellRenderer(new AttachmentItemRenderer());
        column.setCellEditor(new AttachmentItemEditor());
        columnModel.addColumn(column);

        table.setModel(attachmentTableModel);
        table.setColumnModel(columnModel);
        table.addHighlighter(HighlighterFactory.createAlternateStriping());
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getFile();
    }

    @Override
    public void onCloseUI() {
    }

    @Override
    public SwingValidator<AttachmentEditorUIModel> getValidator() {
        return null;
    }

    @Override
    public void autoSelectRowInTable(MouseEvent event, JPopupMenu popup) {
        super.autoSelectRowInTable(event, popup);

        if (event.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(event)) {
            Point p = event.getPoint();
            JXTable source = (JXTable) event.getSource();
            // get the row index at this point
            int rowIndex = source.rowAtPoint(p);
            int modelRowIndex = rowIndex == -1 ? -1 : source.convertRowIndexToModel(rowIndex);

            Attachment attachment = (Attachment) source.getModel().getValueAt(modelRowIndex, 0);
            if (getModel().isEditable()) {
                editAttachment(attachment);
            } else {
                openAttachment(attachment);
            }
        }
    }

    public void openOriginalAttachment(Attachment attachment) {
        OpenAttachmentAction openAttachmentAction = new OpenAttachmentAction(this);
        openAttachmentAction.setAttachment(attachment, true);

        // run Internal to prevent multiple successive execution (#5668)
        getContext().getActionEngine().runAction(openAttachmentAction);
    }

    public void openAttachment(Attachment attachment) {
        OpenAttachmentAction openAttachmentAction = new OpenAttachmentAction(this);
        openAttachmentAction.setAttachment(attachment, attachment.getEditedFileName() == null);

        // run Internal to prevent multiple successive execution (#5668)
        getContext().getActionEngine().runAction(openAttachmentAction);
    }

    public void editAttachment(Attachment attachment) {
        AttachmentEditorUIModel model = getModel();

        if (model.isEditable() && FaxToMailUIUtil.isFileTypeEditable(attachment.getOriginalFileName())) {
            EditAttachmentAction editAttachmentAction = new EditAttachmentAction(this, attachment);
            // run Internal to prevent multiple successive execution (#5668)
            getContext().getActionEngine().runAction(editAttachmentAction);

        } else if (attachment.getEditedFileName() != null) {
            OpenAttachmentAction openAttachmentAction = new OpenAttachmentAction(this);
            openAttachmentAction.setAttachment(attachment, false);
            // run Internal to prevent multiple successive execution (#5668)
            getContext().getActionEngine().runAction(openAttachmentAction);
        }
    }

    public void removeAttachment(AttachmentItem attachmentItem) {
        Attachment attachment = attachmentItem.getAttachment();
        int answer = JOptionPane.showConfirmDialog(ui,
                                                   t("faxtomail.attachmentEditor.deleteAttachment.message",
                                                     attachment.getOriginalFileName()),
                                                   t("faxtomail.attachmentEditor.deleteAttachment.title"),
                                                   JOptionPane.YES_NO_OPTION
        );

        if (answer == JOptionPane.YES_OPTION) {

            ui.getAttachments().getCellEditor().stopCellEditing();
            getModel().removeAttachment(attachment);

            ui.pack();
        }
    }

    public boolean isAttachmentEditable(Attachment attachment) {
        if (attachment != null) {
            return FaxToMailUIUtil.isFileTypeEditable(attachment.getOriginalFileName())
                    && (getModel().isEditable() || attachment.getEditedFileName() != null);
        } else {
            return false;
        }
    }

    public boolean isAttachmentRemovable(Attachment attachment) {
        return attachment != null && attachment.getTopiaId() == null;
    }

    @Override
    protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {
        super.beforeOpenPopup(modelRowIndex, modelColumnIndex);
        AttachmentEditorUIModel model = getModel();
        Attachment attachment = model.getAttachment().get(modelRowIndex);
        model.setSelectedAttachment(attachment);
    }

    public void markItemAsLessImportant() {
        setSelectedAttachmentLessImportant(true);
    }

    public void unmarkItemAsLessImportant() {
        setSelectedAttachmentLessImportant(false);
    }

    protected void setSelectedAttachmentLessImportant(boolean lessImportant) {
        AttachmentEditorUIModel model = getModel();
        Attachment selectedAttachment = model.getSelectedAttachment();
        selectedAttachment.setLessImportant(lessImportant);
        model.sortAttachments();
        model.setSelectedAttachment(null);
    }

    protected class AttachmentItemRenderer extends AttachmentItem implements TableCellRenderer {

        public AttachmentItemRenderer() {
            setHandler(AttachmentEditorUIHandler.this);
        }

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            Attachment attachment = (Attachment) value;
            setAttachment(attachment);

            if (attachment != null) {
                JLabel label = getAttachmentNameLabel();
                String actionIconName = attachment.isAddedByUser() ? "user" : "email";
                Icon icon = SwingUtil.createActionIcon(actionIconName);
                label.setIcon(icon);
            }

            setEditable(getModel().isEditable());
            setOriginalViewable(getModel().isCanViewOriginalAttachments());

            return this;
        }
    }

    protected class AttachmentItemEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {

        protected AttachmentItem attachmentItem;

        public AttachmentItemEditor() {
            attachmentItem = new AttachmentItem();
            attachmentItem.setHandler(AttachmentEditorUIHandler.this);

            // add action listener to stopCellEditing when a button is clicked
            // this prevent strange NPE with focus change
            attachmentItem.getOpenAttachmentButton().addActionListener(this);
            attachmentItem.getEditAttachmentButton().addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            // stop cell editing when open or edit button is clicked
            stopCellEditing();
        }

        public Component getTableCellEditorComponent(JTable table, Object value,
                                                     boolean isSelected, int row, int column) {
            Attachment attachment = (Attachment) value;
            attachmentItem.setAttachment(attachment);

            if (attachment != null) {
                JLabel label = attachmentItem.getAttachmentNameLabel();
                String actionIconName = attachment.isAddedByUser() ? "user" : "email";
                Icon icon = SwingUtil.createActionIcon(actionIconName);
                label.setIcon(icon);
            }

            attachmentItem.setEditable(getModel().isEditable());
            attachmentItem.setOriginalViewable(getModel().isCanViewOriginalAttachments());

            return attachmentItem;
        }

        @Override
        public Object getCellEditorValue() {
            return attachmentItem.getAttachment();
        }
    }

}
