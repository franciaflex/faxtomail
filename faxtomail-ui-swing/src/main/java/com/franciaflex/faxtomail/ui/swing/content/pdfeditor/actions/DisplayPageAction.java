package com.franciaflex.faxtomail.ui.swing.content.pdfeditor.actions;

/*-
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2018 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorCrossUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorHighlighterUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorLineUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorNoteUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorStampImageUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorStampTextUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorUI;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.pdfeditor.PDFEditorUIModel;
import com.franciaflex.faxtomail.ui.swing.util.JImagePanel;
import jaxx.runtime.swing.ComponentResizer;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

public class DisplayPageAction extends AbstractFaxToMailAction<PDFEditorUIModel, PDFEditorUI, PDFEditorUIHandler> {

    protected final int pageNb;
    protected final float previousZoom;
    protected final int previousRotation;


    public DisplayPageAction(PDFEditorUIHandler handler, int pageNb, float previousZoom, int previousRotation) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.displayPage.tip", pageNb));
        this.pageNb = pageNb;
        this.previousZoom = previousZoom;
        this.previousRotation = previousRotation;
    }

    @Override
    public void doAction() throws Exception {

       // Thread.sleep(5000);

        int rotation = getModel().getRotation();
        float zoom = getModel().getZoom();

        BufferedImage image =  handler.getPage(pageNb, zoom);

        int width = image.getWidth();
        int height = image.getHeight();

        JPanel container = getUI().getContainer();
        Dimension containerSize = new Dimension(rotation % 180 == 0 ? width : height,
                rotation % 180 == 0 ? height : width);
        container.setPreferredSize(containerSize);
        container.setMinimumSize(containerSize);
        container.setMaximumSize(containerSize);
        container.setSize(containerSize);

        JImagePanel documentPanel = getUI().getDocumentPanel();
        documentPanel.setRotation(rotation);
        documentPanel.setImage(image);

        Insets insets = container.getInsets();
        Rectangle rect = container.getBounds();

        float zoomRatio = zoom / previousZoom;
        int rotationDiff = rotation - previousRotation;

        PDFEditorUIModel.Page p = getModel().getPage(pageNb);

        for (PDFEditorNoteUI panel : p.getNotes()) {
            displayPageNote(zoom, insets, rect, zoomRatio, rotationDiff, panel);
        }

        boolean orientation180 = Math.abs(rotationDiff) % 180 == 0;

        for (PDFEditorCrossUI panel : p.getCrosses()) {
            displayPageCross(insets, rect, zoomRatio, rotationDiff, orientation180, panel);
        }

        List<PDFEditorLineUI> lines = p.getLines();
        getHandler().getCr().deregisterComponent(lines.toArray(new PDFEditorLineUI[lines.size()]));

        for (PDFEditorLineUI panel : lines) {
            displayPageLine(insets, rect, zoomRatio, rotationDiff, orientation180, panel);
        }

        for (PDFEditorHighlighterUI panel : p.getHighlighters()) {
            displayPageHighlighter(insets, rect, zoomRatio, rotationDiff, orientation180, panel);
        }

        for (PDFEditorStampTextUI panel : p.getTextStamps()) {
            displayPageTextStamp(zoom, insets, rect, zoomRatio, rotationDiff, panel);
        }

        for (PDFEditorStampImageUI panel : p.getImageStamps()) {
            displayPageImageStamp(zoom, insets, rect, zoomRatio, rotationDiff, panel);
        }

        container.updateUI();

    }

    protected void displayPageNote(float zoom, Insets insets, Rectangle rect, float zoomRatio, int rotationDiff, PDFEditorNoteUI panel) {
        panel.setVisible(true);

        panel.setZoom(zoom);
        Rectangle bounds = panel.getBounds();
        int x, y;

        if (rotationDiff == 0) {
            x = bounds.x;
            y = bounds.y;

        } else if (rotationDiff == 90 || rotationDiff == -270) {
            x = rect.width - bounds.height / 2 - bounds.width / 2 - bounds.y;
            y = bounds.x + bounds.width / 2 - bounds.height / 2;

        } else {
            x = bounds.y + bounds.height / 2 - bounds.width / 2;
            y = rect.height - bounds.height / 2 - bounds.width / 2 - bounds.x;
        }

        panel.setBounds((int) (zoomRatio * x) + insets.left,
                (int) (zoomRatio * y) + insets.top,
                (int) (zoomRatio * bounds.width), (int) (zoomRatio * bounds.height));
    }

    protected void displayPageCross(Insets insets, Rectangle rect, float zoomRatio, int rotationDiff, boolean orientation180, PDFEditorCrossUI panel) {
        panel.setVisible(true);

        Rectangle bounds = panel.getBounds();

        int newWidth = orientation180 ? bounds.width : bounds.height;
        int newHeight = orientation180 ? bounds.height : bounds.width;

        int x, y;

        if (rotationDiff == 0) {
            x = bounds.x;
            y = bounds.y;

        } else if (rotationDiff == 90 || rotationDiff == -270) {
            x = rect.width - newWidth - bounds.y;
            y = bounds.x;

        } else {
            x = bounds.y;
            y = rect.height - newHeight - bounds.x;
        }

        panel.setBounds((int) (zoomRatio * x) + insets.left,
                (int) (zoomRatio * y) + insets.top,
                (int) (zoomRatio * newWidth), (int) (zoomRatio * newHeight));
    }

    protected void displayPageLine(Insets insets, Rectangle rect, float zoomRatio, int rotationDiff, boolean orientation180, PDFEditorLineUI panel) {
        panel.setVisible(true);

        Rectangle bounds = panel.getBounds();

        boolean horizontal = panel.isHorizontal();
        panel.setHorizontal(orientation180 ? horizontal : !horizontal);

        int newWidth = orientation180 ? bounds.width : bounds.height;
        int newHeight = orientation180 ? bounds.height : bounds.width;

        int x, y;

        if (rotationDiff == 0) {
            x = bounds.x;
            y = bounds.y;

        } else if (rotationDiff == 90 || rotationDiff == -270) {
            x = rect.width - newWidth - bounds.y;
            y = bounds.x;

        } else {
            x = bounds.y;
            y = rect.height - newHeight - bounds.x;
        }

        panel.setBounds((int) (zoomRatio * x) + insets.left,
                (int) (zoomRatio * y) + insets.top,
                (int) (zoomRatio * newWidth), (int) (zoomRatio * newHeight));

        getHandler().getCr().registerComponent(panel.isHorizontal() ? ComponentResizer.DIRECTION_HORIZONTAL : ComponentResizer.DIRECTION_VERTICAL,
                panel);
    }

    protected void displayPageHighlighter(Insets insets, Rectangle rect, float zoomRatio, int rotationDiff, boolean orientation180, PDFEditorHighlighterUI panel) {
        panel.setVisible(true);

        Rectangle bounds = panel.getBounds();

        int newWidth = orientation180 ? bounds.width : bounds.height;
        int newHeight = orientation180 ? bounds.height : bounds.width;

        int x, y;

        if (rotationDiff == 0) {
            x = bounds.x;
            y = bounds.y;

        } else if (rotationDiff == 90 || rotationDiff == -270) {
            x = rect.width - newWidth - bounds.y;
            y = bounds.x;

        } else {
            x = bounds.y;
            y = rect.height - newHeight - bounds.x;
        }

        panel.setBounds((int) (zoomRatio * x) + insets.left,
                (int) (zoomRatio * y) + insets.top,
                (int) (zoomRatio * newWidth), (int) (zoomRatio * newHeight));
    }

    protected void displayPageTextStamp(float zoom, Insets insets, Rectangle rect, float zoomRatio, int rotationDiff, PDFEditorStampTextUI panel) {
        panel.setVisible(true);

        panel.setZoom(zoom);
        Rectangle bounds = panel.getBounds();
        int x, y;

        if (rotationDiff == 0) {
            x = bounds.x;
            y = bounds.y;

        } else if (rotationDiff == 90 || rotationDiff == -270) {
            x = rect.width - bounds.height / 2 - bounds.width / 2 - bounds.y;
            y = bounds.x + bounds.width / 2 - bounds.height / 2;

        } else {
            x = bounds.y + bounds.height / 2 - bounds.width / 2;
            y = rect.height - bounds.height / 2 - bounds.width / 2 - bounds.x;
        }

        panel.setBounds((int) (zoomRatio * x) + insets.left,
                (int) (zoomRatio * y) + insets.top,
                (int) (zoomRatio * bounds.width), (int) (zoomRatio * bounds.height));
    }

    protected void displayPageImageStamp(float zoom, Insets insets, Rectangle rect, float zoomRatio, int rotationDiff, PDFEditorStampImageUI panel) {
        panel.setVisible(true);

        Rectangle bounds = panel.getBounds();
        int x, y;

        if (rotationDiff == 0) {
            x = bounds.x;
            y = bounds.y;

        } else if (rotationDiff == 90 || rotationDiff == -270) {
            x = rect.width - bounds.height / 2 - bounds.width / 2 - bounds.y;
            y = bounds.x + bounds.width / 2 - bounds.height / 2;

        } else {
            x = bounds.y + bounds.height / 2 - bounds.width / 2;
            y = rect.height - bounds.height / 2 - bounds.width / 2 - bounds.x;
        }

        panel.setBounds((int) (zoomRatio * x) + insets.left,
                (int) (zoomRatio * y) + insets.top,
                (int) (zoomRatio * bounds.width), (int) (zoomRatio * bounds.height));
    }

}
