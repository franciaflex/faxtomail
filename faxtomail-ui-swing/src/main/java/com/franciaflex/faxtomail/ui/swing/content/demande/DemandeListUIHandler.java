package com.franciaflex.faxtomail.ui.swing.content.demande;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.Configuration;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.EmailFilter;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.History;
import com.franciaflex.faxtomail.persistence.entities.HistoryImpl;
import com.franciaflex.faxtomail.persistence.entities.HistoryType;
import com.franciaflex.faxtomail.persistence.entities.MailAction;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.OriginalEmailImpl;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.PrintOnDefaultPrinterAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.actions.ComputeQuantitiesByRangeAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.actions.LoadFolderEmailsAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.actions.SaveDemandeFromListAction;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailDemandListHandler;
import com.franciaflex.faxtomail.ui.swing.util.DemandeTableModel;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.franciaflex.faxtomail.ui.swing.util.FilterSortableTableHeaderRenderer;
import com.franciaflex.faxtomail.ui.swing.util.FolderTreeNode;
import com.franciaflex.faxtomail.ui.swing.util.PaginationComboModel;
import com.franciaflex.faxtomail.ui.swing.util.RemoveablePropertyChangeListener;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.swing.table.filter.TableFilter;
import jaxx.runtime.swing.table.filter.TableRowFilterSupport;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.swing.action.ApplicationUIAction;
import org.nuiton.jaxx.application.swing.util.CloseableUI;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JRootPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.TableCellEditor;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.IntrospectionException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Handler of UI {@link com.franciaflex.faxtomail.ui.swing.content.demande.DemandeListUIHandler}.
 *
 * @author kmorin - morin@codelutin.com
 */
public class DemandeListUIHandler extends AbstractFaxToMailDemandListHandler<DemandeListUIModel, DemandeListUI> implements CloseableUI {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DemandeListUIHandler.class);
    public static final String REFRESH_KEY = "F5";
    public static final String REFRESH_FOLDER_ACTION = "refreshFolder";

    protected Configuration config;

    protected DemandeListTableFilter tableFilter;

    protected Timer refreshListTimer;

    protected final Binder<EmailFilter, EmailFilter> binder = BinderFactory.newBinder(EmailFilter.class);

    protected final PropertyChangeListener actionInProgressListener = new RemoveablePropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            boolean inProgress = (boolean) evt.getNewValue();
            getUI().getNavigationTree().setEnabled(!inProgress);
        }
    };

    public TableFilter<JXTable> getTableFilter() {
        return tableFilter;
    }

    protected final PropertyChangeListener emailFilterPropertyChangeListener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            EmailFilter emailFilter = (EmailFilter) evt.getSource();
            binder.copy(emailFilter, getContext().getEmailFilter(), evt.getPropertyName());
        }
    };

    protected final PropertyChangeListener selectedDemandeChangeListener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            String propertyName = evt.getPropertyName();
            if (Email.PROPERTY_PRIORITY.equals(propertyName)) {
                if (evt.getNewValue() != null || evt.getOldValue() != null) {
                    SaveDemandeFromListAction saveAction =
                            getContext().getActionFactory().createLogicAction(DemandeListUIHandler.this,
                                                                              SaveDemandeFromListAction.class);
                    saveAction.setModifiedProperties(propertyName);
                    getContext().getActionEngine().runAction(saveAction);
                }
            }

        }
    };

    public Configuration getConfiguration() {
        return config;
    }

    @Override
    public void beforeInit(DemandeListUI ui) {
        super.beforeInit(ui);

        try (FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            config = serviceContext.getConfigurationService().getConfiguration();

            DemandeListUIModel model = new DemandeListUIModel();
            Collection<MailFolder> folders = serviceContext.getMailFolderService()
                    .getRootMailFoldersWithReadingRights(getContext().getCurrentUser());
            model.setFolders(new ArrayList<>(folders));

            this.ui.setContextValue(model);
        } catch (IOException eee){
            log.error("Cannot init UI");
        }
    }

    @Override
    public void afterInit(DemandeListUI ui) {

        initUI(ui);

        ui.getMainSplitPane().addComponentListener(new java.awt.event.ComponentAdapter() {

            @Override
            public void componentResized(ComponentEvent e) {
                // on réajoute l'ui à la session sinon le séparateur entre l'arbre et la liste se positionne mal
                getContext().getSwingSession().add(getUI().getMainSplitPane(), true);
            }
        });

        final DemandeListUIModel model = getModel();

        // init table
        final JXTable dataTable = getUI().getDataTable();

        initDemandeTable(dataTable);

        tableFilter = new DemandeListTableFilter(dataTable, this);

        Decorator<Object> decorator = new Decorator<Object>(Object.class) {
            @Override
            public String toString(Object bean) {
                String s = null;
                if (bean != null) {
                    Decorator<?> decorator = getDecorator(bean.getClass(), null);
                    if (decorator != null) {
                        s = decorator.toString(bean);
                    }
                }
                if (s == null) {
                    s = JAXXUtil.getStringValue(bean);
                }
                return s;
            }
        };

        FilterSortableTableHeaderRenderer tableHeaderRenderer = new FilterSortableTableHeaderRenderer(tableFilter, model);
        TableRowFilterSupport.forFilter(tableFilter)
                             .searchable(true)
                             .searchDecorator(decorator)
                             .useTableRenderers(true)
                             .setPopupDefaultSize(new Dimension(250, 290))
                             .setTableHeaderRenderer(tableHeaderRenderer)
                             .apply();

        updateEmailFilterWithContextEmailFilter();

        dataTable.getTableHeader().addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (model.isCanSortResults() && SwingUtilities.isLeftMouseButton(e)) {
                    TableColumnModelExt colModel = (TableColumnModelExt) dataTable.getColumnModel();
                    int vColumnIndex = colModel.getColumnIndexAtX(e.getX());
                    TableColumnExt column = colModel.getColumnExt(vColumnIndex);
                    MailField mailField = DemandeTableModel.COLUMN_IDENTIFIERS.inverse().get(column.getIdentifier());
                    if (mailField.getOrderProperty() != null) {
                        boolean changeSortOrder = Objects.equals(model.getOrderByMailField(), mailField);
                        model.setOrderByMailField(mailField);
                        model.setOrderDesc(changeSortOrder && !model.isOrderDesc());
                        tableFilter.executeFilter();
                    }
                }
            }
        });

        //Need to stop timer when manipulating headers in order to prevent #9791
        dataTable.getTableHeader().addMouseListener(new MouseAdapter(){
            @Override
            public void mousePressed(MouseEvent me) {
                refreshListTimer.stop();
            }

            @Override
            public void mouseReleased(MouseEvent me) {
                refreshListTimer.start();
            }
        });

        dataTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel source = (ListSelectionModel) e.getSource();

                DemandeListUIModel model = getModel();
                if (source.isSelectionEmpty()) {
                    model.setSelectedEmails(null);

                } else {
                    List<DemandeUIModel> selectedRows = new ArrayList<DemandeUIModel>();
                    DemandeTableModel dataTableModel = (DemandeTableModel) dataTable.getModel();
                    for (int i = source.getMinSelectionIndex(); i <= source.getMaxSelectionIndex(); i++) {
                        if (source.isSelectedIndex(i)) {
                            selectedRows.add(dataTableModel.getEntry(i));
                        }
                    }
                    model.setSelectedEmails(selectedRows);
                }
            }
        });

        model.addPropertyChangeListener(DemandeListUIModel.PROPERTY_EMAILS, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                List<DemandeUIModel> emails = (List<DemandeUIModel>) evt.getNewValue();
                DemandeTableModel dataTableModel = (DemandeTableModel) dataTable.getModel();
                dataTableModel.setRows(emails);

                int quotationNb = 0;
                int pfNb = 0;
                int savNb = 0;
                for (DemandeUIModel email : emails) {
                    quotationNb += email.getQuotationNb();
                    pfNb += email.getPfNb();
                    savNb += email.getSavNb();
                }

                DemandeListUIModel model = (DemandeListUIModel) evt.getSource();
                model.setQuotationNb(quotationNb);
                model.setPfNb(pfNb);
                model.setSavNb(savNb);
            }
        });

        model.addPropertyChangeListener(DemandeListUIModel.PROPERTY_SELECTED_EMAILS, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                List<DemandeUIModel> oldDemands = (List<DemandeUIModel>) evt.getOldValue();
                if (oldDemands != null) {
                    for (DemandeUIModel demand : oldDemands) {
                        demand.removePropertyChangeListener(selectedDemandeChangeListener);
                    }
                }

                List<DemandeUIModel> newDemands = (List<DemandeUIModel>) evt.getNewValue();
                if (newDemands != null) {
                    for (DemandeUIModel demand : newDemands) {
                        demand.addPropertyChangeListener(selectedDemandeChangeListener);
                    }
                }

                resetTimer();
            }
        });

        // init tree
        final JTree navigationTree = ui.getNavigationTree();
        final Map<MailFolder, FolderTreeNode> nodesByFolder =
                FaxToMailUIUtil.initFolderTree(getContext(), navigationTree, model.getFolders(), true);

        getContext().addPropertyChangeListener(FaxToMailUIContext.PROPERTY_ACTION_IN_PROGRESS, actionInProgressListener);

        navigationTree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                TableCellEditor cellEditor = dataTable.getCellEditor();
                if (cellEditor != null) {
                    cellEditor.stopCellEditing();
                }

                FolderTreeNode folderNode = (FolderTreeNode) e.getPath().getLastPathComponent();
                if (folderNode.isCanSelect()) {
                    MailFolder folder = folderNode.getMailFolder();

                    getModel().setSelectedFolder(folder);
                }
            }
        });

        model.addPropertyChangeListener(DemandeListUIModel.PROPERTY_SELECTED_FOLDER, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                MailFolder folder = (MailFolder) evt.getNewValue();
                getContext().setCurrentMailFolder(folder);
                loadFolderDemands(nodesByFolder);

                if (folder != null) {
                    MailFolder folderWithShowReplyAction = folder;
                    while (folderWithShowReplyAction.getParent() != null
                           && folderWithShowReplyAction.getShowReplyAction() == null) {
                        folderWithShowReplyAction = folderWithShowReplyAction.getParent();
                    }
                    getModel().setReplyActivated(Boolean.TRUE.equals(folderWithShowReplyAction.getShowReplyAction()));

                    MailFolder folderWithShowForwardAction = folder;
                    while (folderWithShowForwardAction.getParent() != null
                           && folderWithShowForwardAction.getShowForwardAction() == null) {
                        folderWithShowForwardAction = folderWithShowForwardAction.getParent();
                    }
                    getModel().setForwardActivated(Boolean.TRUE.equals(folderWithShowForwardAction.getShowForwardAction()));

                    //Init takenby visibility
                    MailFolder folderWithShowTakenByAction = folder;
                    while (folderWithShowTakenByAction.getParent() != null
                            && folderWithShowTakenByAction.getShowTakenByAction() == null) {
                        folderWithShowTakenByAction = folderWithShowTakenByAction.getParent();
                    }
                    getModel().setTakenByActivated(Boolean.TRUE.equals(folderWithShowTakenByAction.getShowTakenByAction()));

                    DefaultMutableTreeNode node = nodesByFolder.get(folder);
                    if (node != null) {
                        navigationTree.setSelectionPath(new TreePath(node.getPath()));
                    }
                }
            }
        });

        model.addPropertyChangeListener(DemandeListUIModel.PROPERTY_CAN_SORT_RESULTS, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                boolean canSortResults = (boolean) evt.getNewValue();
                if (!canSortResults) {
                    model.setOrderByMailField(MailField.RECEPTION_DATE);
                    model.setOrderDesc(false);
                }
            }
        });

        // int combo box for result per page
        ui.getResultPerPageCombo().setModel(new PaginationComboModel());
        int resultPerPage = getConfig().getResultPerPage();
        getModel().setResultPerPage(resultPerPage);
        ui.getResultPerPageCombo().setSelectedItem(resultPerPage);
        ui.getResultPerPageCombo().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                updateResultPerPage(e);
            }
        });

        // add refresh shortcut
        JRootPane rootPane = getContext().getMainUI().getRootPane();
        rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(REFRESH_KEY), REFRESH_FOLDER_ACTION);
        ApplicationUIAction<LoadFolderEmailsAction> refreshAction = getContext().getActionFactory()
                .createUIAction(this, LoadFolderEmailsAction.class);
        rootPane.getActionMap().put(REFRESH_FOLDER_ACTION, refreshAction);

        int interval = getConfig().getRefreshListInterval() * 1000;
        refreshListTimer = new Timer(interval, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoadFolderEmailsAction refreshAction = getContext().getActionFactory()
                        .createLogicAction(DemandeListUIHandler.this, LoadFolderEmailsAction.class);
                getContext().getActionEngine().runAction(refreshAction);
            }
        });

        loadFolderDemands(nodesByFolder);

    }

    public void resetTimer() {
        refreshListTimer.restart();
    }

    public void updateEmailFilterWithContextEmailFilter() {
        EmailFilter emailFilter = getModel().getEmailFilter();
        emailFilter.removePropertyChangeListener(emailFilterPropertyChangeListener);
        filtersFromContextToModel();
        emailFilter.addPropertyChangeListener(emailFilterPropertyChangeListener);
    }

    protected void filtersFromContextToModel() {
        EmailFilter contextEmailFilter = getContext().getEmailFilter();
        EmailFilter currentEmailFilter = getModel().getEmailFilter();

        currentEmailFilter.clearFilter();

        List<MailField> fields = getColumns();
        if (CollectionUtils.isEmpty(fields)) {
            fields = Lists.newArrayList(MailField.getTableFields());
        }

        List<String> propertiesToBind = new ArrayList<String>();

        for (int i = 0; i < fields.size(); i++) {
            String property = null;

            MailField field = fields.get(i);
            switch (field) {
                case DEMAND_STATUS:
                    property = EmailFilter.PROPERTY_DEMAND_STATUS;
                    break;

                case RECEPTION_DATE:
                    property = EmailFilter.PROPERTY_RECEPTION_DATES;
                    break;

                case RECIPIENT:
                    property = EmailFilter.PROPERTY_RECIPIENTS;
                    break;

                case SENDER:
                    property = EmailFilter.PROPERTY_SENDERS;
                    break;

                case OBJECT:
                    property = EmailFilter.PROPERTY_DEMAND_OBJECTS;
                    break;

                case CLIENT_CODE:
                    property = EmailFilter.PROPERTY_CLIENT_CODES;
                    break;

                case CLIENT_NAME:
                    property = EmailFilter.PROPERTY_CLIENT_NAMES;
                    break;

                case CLIENT_BRAND:
                    property = EmailFilter.PROPERTY_CLIENT_BRANDS;
                    break;

                case DEMAND_TYPE:
                    property = EmailFilter.PROPERTY_DEMAND_TYPES;
                    break;

                case EDI_RETURN:
                    property = EmailFilter.PROPERTY_EDI_CODE_NUMBERS;
                    break;

                case WAITING_STATE:
                    property = EmailFilter.PROPERTY_WAITING_STATES;
                    break;

                case TAKEN_BY:
                    property = EmailFilter.PROPERTY_TAKEN_BYS;
                    break;

                case PRIORITY:
                    property = EmailFilter.PROPERTY_PRIORITIES;
                    break;

                case PROJECT_REFERENCE:
                    property = EmailFilter.PROPERTY_PROJECT_REFERENCES;
                    break;

                case COMPANY_REFERENCE:
                    property = EmailFilter.PROPERTY_LOCAL_REFERENCES;
                    break;

                case REFERENCE:
                    property = EmailFilter.PROPERTY_REFERENCES;
                    break;

                case RANGE_ROW:
                    property = EmailFilter.PROPERTY_RANGES;
                    break;

                case PF_NB:
                    property = EmailFilter.PROPERTY_PRODUCT_QUANTITIES;
                    break;

                case SAV_NB:
                    property = EmailFilter.PROPERTY_SAV_QUANTITIES;
                    break;

                case QUOTATION_NB:
                    property = EmailFilter.PROPERTY_QUOTATION_QUANTITIES;
                    break;

                case LAST_ATTACHMENT_OPENING_IN_THIS_FOLDER_USER:
                    property = EmailFilter.PROPERTY_LAST_ATTACHMENT_OPENERS;
                    break;

                case COMMENT:
                    property = EmailFilter.PROPERTY_COMMENTS;
                    break;

                case SUBJECT:
                    property = EmailFilter.PROPERTY_SUBJECTS;
                    break;

                case LAST_PRINTING_USER:
                    property = EmailFilter.PROPERTY_LAST_PRINTING_USERS;
                    break;

                case LAST_PRINTING_DATE:
                    property = EmailFilter.PROPERTY_LAST_PRINTING_DATES;
                    break;
            }

            if (log.isTraceEnabled()) {
                log.trace(i + "property to copy to model's emailfilter : " + property);
            }
            if (property != null) {
                try {
                    Method getter = new PropertyDescriptor(property, EmailFilter.class).getReadMethod();
                    Collection value = (Collection) getter.invoke(contextEmailFilter);
                    tableFilter.setFilterState(i, value);

                } catch (IntrospectionException e) {
                    if (log.isErrorEnabled()) {
                        log.error("error while introspecting emailfilter for property " + property, e);
                    }
                } catch (InvocationTargetException | IllegalAccessException e) {
                    if (log.isErrorEnabled()) {
                        log.error("error while calling getter for property " + property, e);
                    }
                }
                propertiesToBind.add(property);

            } else {
                tableFilter.setFilterState(i, null);
            }
        }

        binder.copy(contextEmailFilter, currentEmailFilter, propertiesToBind.toArray(new String[propertiesToBind.size()]));
    }

    public String populateColumnModel(JXTable table, boolean sortable) {
        return super.populateColumnModel(table, sortable);
    }

    @Override
    protected boolean isDisplayOnlyUserTrigraphInTables() {
        return getModel().isDisplayOnlyUserTrigraphInTables();
    }

    protected void loadFolderDemands(Map<MailFolder, FolderTreeNode> nodesByFolder) {
        DemandeListUIModel model = getModel();
        MailFolder folder = model.getSelectedFolder();
        if (folder != null) {
            FolderTreeNode folderNode = nodesByFolder.get(folder);
            if (folderNode != null) {
                boolean readable = folderNode.isCanRead();

                while (folder.getAllowCreateDemandIntoFolder() == null
                        && folder.getParent() != null) {
                    folder = folder.getParent();
                }

                model.setComputeQuantitiesByRangeEnabled(readable);
                model.setNewDemandEnabled(readable && Boolean.TRUE.equals(folder.getAllowCreateDemandIntoFolder()));
            }
        }

        folder = model.getSelectedFolder();
        if (folder != null) {
            MailFolder folderWithNbElementToDisplay = folder;
            while (!folderWithNbElementToDisplay.isUseCurrentLevelNbElementToDisplay()
                   && folderWithNbElementToDisplay.getParent() != null) {
                folderWithNbElementToDisplay = folderWithNbElementToDisplay.getParent();
            }

            boolean enableChangeResultPerPage = folderWithNbElementToDisplay.getNbElementToDisplay() == null;
            model.setEnableChangeResultPerPage(enableChangeResultPerPage);
            int resultPerPage;
            if (!enableChangeResultPerPage) {
                resultPerPage = folderWithNbElementToDisplay.getNbElementToDisplay();
            } else {
                resultPerPage = getConfig().getResultPerPage();
            }
            model.setResultPerPage(resultPerPage);
            ui.getResultPerPageCombo().setSelectedItem(resultPerPage);

            MailFolder folderWithCanChangeOrderInTable = folder;
            while (folderWithCanChangeOrderInTable.getCanChangeOrderInTable() == null
                   && folderWithCanChangeOrderInTable.getParent() != null) {
                folderWithCanChangeOrderInTable = folderWithCanChangeOrderInTable.getParent();
            }
            model.setCanSortResults(Boolean.TRUE.equals(folderWithCanChangeOrderInTable.getCanChangeOrderInTable()));
        }

        tableFilter.executeFilter();
    }

    public void goToNextPage() {
        getModel().setPaginationParameter(getModel().getPaginationResult().getNextPage());
        runListAction();
    }
    
    public void goToPreviousPage() {
        getModel().setPaginationParameter(getModel().getPaginationResult().getPreviousPage());
        runListAction();
    }
    
    public void updateResultPerPage(ItemEvent event) {
        DemandeListUIModel model = getModel();
        if (event.getStateChange() == ItemEvent.SELECTED && model.isEnableChangeResultPerPage()) {
            int resultPerPage = (Integer)event.getItem();
            model.setResultPerPage(resultPerPage);
            getConfig().setResultPerPage(resultPerPage);
            getConfig().save();
            model.resetPaginationParameterPage();
            runListAction();
        }
    }
    
    protected void runListAction() {
        if (getModel().getSelectedFolder() != null) { // can be when update result per page
            LoadFolderEmailsAction loadFolderEmailsAction =
                    getContext().getActionFactory().createLogicAction(DemandeListUIHandler.this,
                                                                      LoadFolderEmailsAction.class);
            getContext().getActionEngine().runAction(loadFolderEmailsAction);
        }
    }

    public void initDemandeTable(final JXTable table) {
        HighlightPredicate reportNotTakenPredicate = new HighlightPredicate() {
            @Override
            public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
                int viewRow = adapter.row;
                int modelRow = adapter.convertRowIndexToModel(viewRow);
                DemandeUIModel row = ((DemandeTableModel) table.getModel()).getEntry(modelRow);
                MailFolder folder = getModel().getSelectedFolder();
                while (folder.getOpenAttachmentReportNoTaken() == null
                        && folder.getParent() != null) {
                    folder = folder.getParent();
                }

                return Boolean.TRUE.equals(folder.getOpenAttachmentReportNoTaken())
                        && row.getLastAttachmentOpener() != null
                        && !row.getLastAttachmentOpener().equals(row.getTakenBy());
            }
        };
        Color color = Color.ORANGE;
        table.addHighlighter(new ColorHighlighter(reportNotTakenPredicate, color, Color.WHITE, color.darker(), Color.WHITE));

        initDemandeTable(table, false);
    }

    @Override
    public List<MailField> getColumns() {
        List<MailField> tableColumns = null;
        MailFolder selectedFolder = getModel().getSelectedFolder();
        if (selectedFolder != null) {
            while (!selectedFolder.isUseCurrentLevelTableColumns() && selectedFolder.getParent() != null) {
                selectedFolder = selectedFolder.getParent();
            }
            tableColumns = selectedFolder.getFolderTableColumns();
        }
        return tableColumns;
    }

    @Override
    protected MailField[] getEditableTableProperties() {
        MailField[] result;
        if (getModel().getSelectedFolder() != null && getModel().getSelectedFolder().isFolderWritable()) {
            result = new MailField[] { MailField.PRIORITY, MailField.ATTACHMENT, MailField.REPLIES };
        } else {
            result = new MailField[] { MailField.ATTACHMENT, MailField.REPLIES };
        }
        return result;
    }

    @Override
    protected void onDoubleClickOnDemande(DemandeUIModel selectedEmail) {
        super.onDoubleClickOnDemande(selectedEmail);
        selectedEmail.removePropertyChangeListener(selectedDemandeChangeListener);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getNavigationTree();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }

        DemandeListUIModel model = getModel();
        model.getEmailFilter().removePropertyChangeListener(emailFilterPropertyChangeListener);

        List<DemandeUIModel> selectedEmails = model.getSelectedEmails();
        if (selectedEmails != null) {
            for (DemandeUIModel demand : selectedEmails) {
                demand.removePropertyChangeListener(selectedDemandeChangeListener);
            }
        }

        // remove refresh shortcut
        JRootPane rootPane = getContext().getMainUI().getRootPane();
        rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).remove(KeyStroke.getKeyStroke(REFRESH_KEY));
        rootPane.getActionMap().remove(REFRESH_FOLDER_ACTION);

        getContext().removePropertyChangeListener(FaxToMailUIContext.PROPERTY_ACTION_IN_PROGRESS, actionInProgressListener);

    }

    @Override
    public boolean quitUI() {
        return true;
    }

    @Override
    public SwingValidator<DemandeListUIModel> getValidator() {
        return null;
    }

    @Override
    protected void beforeOpenPopup(int rowIndex, int columnIndex) {
        super.beforeOpenPopup(rowIndex, columnIndex);

        int selectedRowCount = getUI().getDataTable().getSelectedRowCount();

        DemandeListUIModel model = getModel();

        model.setReplyEnabled(selectedRowCount == 1
                                      && model.getSelectedEmails().get(0).isEditable()
                                      && isActionEnabled(model.getSelectedEmails().get(0), MailAction.REPLY));

        model.setForwardEnabled(selectedRowCount == 1
                              && model.getSelectedEmails().get(0).isEditable()
                              && isActionEnabled(model.getSelectedEmails().get(0), MailAction.FORWARD));

        model.setArchiveEnabled(model.getSelectedFolder().isFolderWritable() && selectedRowCount > 0);
        model.setTransmitEnabled(model.getSelectedFolder().isFolderWritable() && selectedRowCount > 0);
        model.setPrintEnabled(selectedRowCount > 0);
        model.setTakenByEnabled(model.getSelectedFolder().isFolderWritable() && selectedRowCount > 0);
    }

    /**
     * During rigth clic, auto-select located under mouse position (not done by default by java) and
     * display contextual menu.
     * 
     * @param e event
     * @param popup popup menu to display
     */
    public void autoSelectNodeInTree(MouseEvent e, JPopupMenu popup) {

        boolean rightClick = SwingUtilities.isRightMouseButton(e);

        if (rightClick) {

            JTree source = (JTree) e.getSource();

            // get the row index at this point
            int rowIndex = source.getClosestRowForLocation(e.getX(), e.getY());

            if (log.isDebugEnabled()) {
                log.debug("At point [" + e.getPoint() + "] found Row " + rowIndex);
            }

            // select row (could empty selection)
            if (rowIndex == -1) {
                source.clearSelection();
            } else {
                // set selection
                source.setSelectionRow(rowIndex);
            }

            // on right click show popup
            popup.show(source, e.getX(), e.getY());
        }
    }

    public void computeQuantitiesByRange() {
        QuantitiesByRangeUI dialogContent = new QuantitiesByRangeUI(ui);
        getContext().getActionEngine().runAction(new ComputeQuantitiesByRangeAction(dialogContent.getHandler()));
    }

    public void newDemand() {
        FaxToMailUser currentUser = getContext().getCurrentUser();
        Date now = new Date();

        List<History> histories = new ArrayList<History>();
        DemandeUIModel email = new DemandeUIModel();
        History history = new HistoryImpl();
        history.setFaxToMailUser(currentUser);
        history.setType(HistoryType.CREATION);
        history.setModificationDate(now);
        histories.add(history);

        history = new HistoryImpl();
        history.setFaxToMailUser(currentUser);
        history.setType(HistoryType.OPENING);
        history.setModificationDate(now);
        histories.add(history);

        email.setMailFolder(getModel().getSelectedFolder());
        email.setReceptionDate(now);
        email.setTakenBy(currentUser);
        email.setHistory(histories);
        email.setDemandStatus(DemandStatus.UNTREATED);
        email.setMatchingClients(new ArrayList<Client>());

        OriginalEmailImpl originalEmail = new OriginalEmailImpl();
        originalEmail.setContent("");
        email.setOriginalEmail(originalEmail); // can't be null

        openDemand(email);
    }

    public void print() {
        DemandeListUIModel model = getModel();
        MailFolder selectedFolder = model.getSelectedFolder();
        while (selectedFolder.getParent() != null && selectedFolder.getPrintActionEqualTakeAction() == null) {
            selectedFolder = selectedFolder.getParent();
        }
        boolean take = Boolean.TRUE.equals(selectedFolder.getPrintActionEqualTakeAction());

        selectedFolder = model.getSelectedFolder();
        while (selectedFolder.getParent() != null && selectedFolder.getPrintActionEqualTakeOnlyIfNotTaken() == null) {
            selectedFolder = selectedFolder.getParent();
        }
        boolean takeOnlyIfNotTaken = Boolean.TRUE.equals(selectedFolder.getPrintActionEqualTakeOnlyIfNotTaken());

        Multimap<DemandeUIModel, AttachmentFile> attachmentToPrints = HashMultimap.create();

        for (DemandeUIModel demandeUIModel : model.getSelectedEmails()) {

            List<Attachment> attachments = demandeUIModel.getAttachment();
            if (CollectionUtils.isEmpty(attachments)) {
                attachmentToPrints.put(demandeUIModel, null);

            } else {

                selectedFolder = model.getSelectedFolder();
                while (selectedFolder.getParent() != null && selectedFolder.getPrintInlineAttachments() == null) {
                    selectedFolder = selectedFolder.getParent();
                }
                boolean printInlineAttachment = Boolean.TRUE.equals(selectedFolder.getPrintInlineAttachments());

                for (Attachment attachment : attachments) {
                    if (printInlineAttachment || !attachment.isInlineAttachment()) {
                        // force lazy loading
                        // TODO kmorin 20140813 action ?
                        FaxToMailUIUtil.forceAttachmentFileLoading(getContext(), attachment);

                        AttachmentFile attachmentFile = attachment.getEditedFile();
                        if (attachmentFile == null) {
                            attachmentFile = attachment.getOriginalFile();
                        }
                        attachmentToPrints.put(demandeUIModel, attachmentFile);
                    }
                }
            }
        }

        Boolean printDemandDetails = getContext().getConfig().getPrintDetailPage();

        PrintOnDefaultPrinterAction action = new PrintOnDefaultPrinterAction(this,
                                             attachmentToPrints,
                                             take,
                                             takeOnlyIfNotTaken,
                                             printDemandDetails);
        getContext().getActionEngine().runAction(action);
    }

    @Override
    public void reloadList() {
        runListAction();
    }
}
