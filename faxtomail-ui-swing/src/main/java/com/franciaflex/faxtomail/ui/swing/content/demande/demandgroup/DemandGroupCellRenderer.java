package com.franciaflex.faxtomail.ui.swing.content.demande.demandgroup;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Renderer of a attachement editor in a table cell.
 *
 * @author kmorin - morin@codelutin.com
 */
public class DemandGroupCellRenderer extends DefaultTableCellRenderer {

    public static final String TEXT_PATTERN = "<html><body>%s</body></html>";

    private static final long serialVersionUID = 1L;

    private final String noneText;

    public static DemandGroupCellRenderer newRender() {
        return new DemandGroupCellRenderer();
    }

    protected DemandGroupCellRenderer() {
        setHorizontalAlignment(CENTER);
        setIcon(SwingUtil.createActionIcon("group"));
        this.noneText = n("faxtomail.demandGroup.empty");
    }

    @Override
    protected void setValue(Object value) {
        // do nothing
    }

    @Override
    public JComponent getTableCellRendererComponent(JTable table,
                                                    Object value,
                                                    boolean isSelected,
                                                    boolean hasFocus,
                                                    int row,
                                                    int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        List<DemandeUIModel> demands = (List<DemandeUIModel>) value;

        String toolTipTextValue;

        if (CollectionUtils.isEmpty(demands)) {

            // use HTML to show the tooltip in italic
            toolTipTextValue = "<i>" + t(noneText) + "</i>";


        } else {

            StringBuilder sb = new StringBuilder();
            for (DemandeUIModel demand : demands) {
                sb.append("<br/>").append(demand.getTitle());
            }
            // use html to display the tooltip on several lines
            toolTipTextValue = sb.substring(5);
        }
        String textValue = t("faxtomail.demandGroupCellRenderer.text", demands != null ? demands.size() : 0);
        toolTipTextValue = String.format(TEXT_PATTERN, toolTipTextValue);
        setText(textValue);
        setToolTipText(toolTipTextValue);

        return this;
    }
}
