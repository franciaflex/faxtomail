package com.franciaflex.faxtomail.ui.swing.actions;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.ui.swing.content.MainUIHandler;
import jaxx.runtime.swing.AboutPanel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Calendar;

import static org.nuiton.i18n.I18n.t;

/**
 * To show about panel.
 *
 * @author Kevin Morin
 */
public class ShowAboutAction extends AbstractMainUIFaxToMailAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ShowAboutAction.class);

    protected AboutPanel about;

    public ShowAboutAction(MainUIHandler handler) {
        super(handler, false);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        about.showInDialog(getUI(), true);
    }

    @Override
    public void doAction() throws Exception {

        about = null;

        String name = "faxtomail";
//        String licensePath = "META-INF/" + name + "-LICENSE.txt";
//        String thirdPartyPath = "META-INF/" + name + "-THIRD-PARTY.txt";

        about = new AboutPanel();
        about.setTitle(t("faxtomail.about.title"));
        about.setAboutText(t("faxtomail.about.message"));

        FaxToMailConfiguration config = getConfig();
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int inceptionYear = config.getInceptionYear();
        String years;
        if (currentYear != inceptionYear) {
            years = inceptionYear + "-" + currentYear;
        } else {
            years = inceptionYear + "";
        }

        about.setBottomText(t("faxtomail.about.bottomText",
                              config.getOrganizationName(),
                              years,
                              config.getVersion()));
//        about.setLicenseFile(licensePath);
//        about.setThirdpartyFile(thirdPartyPath);
        about.buildTopPanel();

        about.init();
    }

}
