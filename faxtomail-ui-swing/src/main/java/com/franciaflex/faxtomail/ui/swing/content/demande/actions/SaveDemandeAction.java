package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.EmailImpl;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.RangeRow;
import com.franciaflex.faxtomail.persistence.entities.Reply;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.RangeRowModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.RangeTableModel;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.util.beans.BeanMonitor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class SaveDemandeAction extends AbstractFaxToMailAction<DemandeUIModel, DemandeUI, DemandeUIHandler> {

    protected boolean takeIfNotTaken;

    public SaveDemandeAction(DemandeUIHandler handler) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.save.tip"));
    }

    public boolean isTakeIfNotTaken() {
        return takeIfNotTaken;
    }

    public void setTakeIfNotTaken(boolean takeIfNotTaken) {
        this.takeIfNotTaken = takeIfNotTaken;
    }

    @Override
    public void doAction() throws Exception {
        DemandeUIModel model = getModel();
        FaxToMailUIContext context = getContext();

        Email persistedEmail;
        String topiaId = model.getTopiaId();
        try(FaxToMailServiceContext serviceContext = context.newServiceContext()) {
            if (StringUtils.isNotBlank(topiaId)) {
                persistedEmail = serviceContext.getEmailService().getFullEmailById(topiaId);
            } else {
                persistedEmail = new EmailImpl();
            }
            model.setRangeRow(persistedEmail.getRangeRow());

            Collection<RangeRowModel> rows = model.getValidRangeRowModels();

            //remove null range rows
            boolean cont = true;
            while (cont) {
                try {
                    model.removeRangeRow(null);
                } catch (IllegalArgumentException eee) {
                    cont = false;
                }
            }
            Collection<RangeRow> rangeRows = model.getRangeRow();
            if (rangeRows == null) {
                rangeRows = new ArrayList<>();
            }

            Map<String, RangeRow> rangeRowsById = Maps.uniqueIndex(rangeRows, TopiaEntities.getTopiaIdFunction());

            model.setRangeRow(new ArrayList<RangeRow>());
            for (RangeRowModel rangeRowModel : rows) {
                RangeRow rangeRow = rangeRowsById.get(rangeRowModel.getTopiaId());
                if (rangeRow == null) {
                    model.addRangeRow(rangeRowModel.toEntity());
                } else {
                    model.addRangeRow(rangeRowModel.toEntity(rangeRow));
                }
            }

            FaxToMailUser currentUser = context.getCurrentUser();
            DemandStatus demandStatus = model.getDemandStatus();
            if (demandStatus != DemandStatus.ARCHIVED && takeIfNotTaken && model.getTakenBy() == null) {
                model.setTakenBy(getContext().getCurrentUser());
            }

            Email email = model.toEntity(persistedEmail);
            if (email.getOriginalEmail() == null) {
                email.setOriginalEmail(model.getOriginalEmail());
            }

            Collection<Attachment> attachments = model.getAttachment();
            Collection<Reply> replies = model.getReplies();

            BeanMonitor monitor = getHandler().getMonitor();
            String[] modifiedProperties = monitor.getModifiedProperties();
            email = serviceContext.getEmailService().saveEmail(email, attachments, replies, currentUser, modifiedProperties);

            model.fromEntity(email);
            MailFolder folder = model.getMailFolder();
            boolean folderWritable = folder.isFolderWritable();
            if (model.getArchiveDate() != null
                    || !folderWritable
                    || !model.getDemandStatus().isEditableStatus()) {
                model.setEditable(false);
            }

            // reload range rows with ids
            RangeTableModel rangeTableModel = (RangeTableModel) getUI().getRangeTable().getModel();
            model.recomputeValidRangeRows();
            rangeTableModel.setRows(new ArrayList<>(model.getValidRangeRowModels()));

            getModel().setModify(false);
            monitor.clearModified();
        }
    }
}
