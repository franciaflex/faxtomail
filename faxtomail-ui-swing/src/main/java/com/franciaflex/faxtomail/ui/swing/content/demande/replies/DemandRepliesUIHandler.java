package com.franciaflex.faxtomail.ui.swing.content.demande.replies;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFileImpl;
import com.franciaflex.faxtomail.persistence.entities.AttachmentImpl;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.Reply;
import com.franciaflex.faxtomail.persistence.entities.ReplyContent;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.replies.actions.OpenReplyAction;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyAttachmentModel;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUI;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUIModel;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.franciaflex.faxtomail.ui.swing.util.toolbar.AbstractToolbarPopupHandler;
import com.google.common.collect.Iterables;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.util.MimeMessageUtils;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.HighlighterFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JFrame;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.Component;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.regex.Pattern;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class DemandRepliesUIHandler extends AbstractToolbarPopupHandler<DemandeUIModel, DemandRepliesUI> {

    private final static Log log = LogFactory.getLog(DemandRepliesUIHandler.class);

    @Override
    public void afterInit(DemandRepliesUI ui) {
        super.afterInit(ui);

        initTable(ui.getReplies());

        final PropertyChangeListener listener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                updateTable();
                JAXXUtil.processDataBinding(getUI(),
                                            DemandRepliesUI.BINDING_NO_REPLY_LABEL_VISIBLE,
                                            DemandRepliesUI.BINDING_REPLIES_VISIBLE);
            }
        };
        getUI().addPropertyChangeListener(DemandRepliesUI.PROPERTY_MODEL, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                DemandeUIModel oldModel = (DemandeUIModel) evt.getOldValue();
                if (oldModel != null) {
                    oldModel.removePropertyChangeListener(Email.PROPERTY_REPLIES, listener);
                }
                DemandeUIModel newModel = (DemandeUIModel) evt.getNewValue();
                if (newModel != null) {
                    newModel.addPropertyChangeListener(Email.PROPERTY_REPLIES, listener);
                }
                updateTable();
            }
        });
    }

    protected void updateTable() {
        AbstractTableModel tableModel = (AbstractTableModel) getUI().getReplies().getModel();
        tableModel.fireTableDataChanged();
    }

    protected void initTable(JXTable table) {

        TableModel demandRepliesTableModel = new AbstractTableModel() {
            @Override
            public int getRowCount() {
                DemandeUIModel model = getModel();
                int count = model == null ? 0 : model.sizeReplies();
                return count;
            }

            @Override
            public int getColumnCount() {
                return 1;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                DemandeUIModel model = getModel();
                return model == null ? null : Iterables.get(model.getReplies(), rowIndex);
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return true;
            }
        };

        demandRepliesTableModel.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                getUI().pack();
            }
        });

        TableColumnModel columnModel = new DefaultTableColumnModel();
        TableColumn column = new TableColumn();
        DemandReplyItemRenderer renderer = new DemandReplyItemRenderer();
        DemandReplyItemEditor editor = new DemandReplyItemEditor();
        column.setCellRenderer(renderer);
        column.setCellEditor(editor);
        columnModel.addColumn(column);

        table.setModel(demandRepliesTableModel);
        table.setColumnModel(columnModel);
        table.addHighlighter(HighlighterFactory.createAlternateStriping());
    }

    @Override
    public void onCloseUI() {
    }

    @Override
    public SwingValidator<DemandeUIModel> getValidator() {
        return null;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    public void openReply(DemandReplyItem item) {
        closeEditor();

        FaxToMailUIContext context = getContext();
        OpenReplyAction action = context.getActionFactory().createLogicAction(this, OpenReplyAction.class);
        action.setItem(item);
        context.getActionEngine().runActionAndWait(action);
    }
    
    public void openForward(DemandReplyItem item) {
        closeEditor();

        Reply reply = item.getReply();
        FaxToMailUIUtil.forceReplyContentLoading(getContext(), reply);
        boolean forward = item.isForward();

        try {
            ReplyFormUI dialogContent = new ReplyFormUI(ui);
            // TODO echatellier 20140804 : c'est très technique et très bas niveau, ca devrait se trouver dans les services
            ReplyContent replyContent = reply.getReplyContent();
            MimeMessage message = MimeMessageUtils.createMimeMessage(null, replyContent.getSource());
            ReplyFormUIModel replyModel = dialogContent.getModel();
            replyModel.setEditable(ui.getModel().isEditable());
            replyModel.setForward(forward);
            replyModel.setOriginalDemand(ui.getModel());
            replyModel.setReadSentDate(reply.getSentDate());
            replyModel.setSubject(t("faxtomail.reply.forwardsubject", message.getSubject()));

            if (message.isMimeType("multipart/*")) {
                decomposeMultipartEmail(message, replyModel);

            } else {
                String content = FaxToMailServiceUtils.getTextFromMessage(message);
                replyModel.setMessage(content);
            }

            List<String> toRecipients = FaxToMailServiceUtils.convertAddressesToStrings(message.getRecipients(Message.RecipientType.TO));
            List<String> ccRecipients = FaxToMailServiceUtils.convertAddressesToStrings(message.getRecipients(Message.RecipientType.CC));

            String quotedReply = FaxToMailUIUtil.getQuotedForwardContent(dialogContent.getHandler().createSigning(replyModel.getFrom()),
                                                                         reply.getSubject(),
                                                                         decorate(reply.getSentDate()),
                                                                         message.getFrom()[0].toString(),
                                                                         StringUtils.join(toRecipients, " ; "),
                                                                         StringUtils.join(ccRecipients, " ; "),
                                                                         replyModel.getMessage().replaceAll("\n", "<br/> "));
            replyModel.setMessage(quotedReply);

            JFrame frame = openFrame(dialogContent, t("faxtomail.reply.forward", message.getSubject()), new Dimension(800, 600));

            frame.setVisible(true);

        } catch (Exception e) {
            getContext().getErrorHelper().showErrorDialog(t("faxtomail.demandReplies.error"), e);
        }

    }

    /**
     * Decompose a multipart part.
     * - sets the email content if the part contains a text bodypart
     * - adds attachments to the email
     *
     * @param part the part to decompose
     * @throws Exception
     */
    public void decomposeMultipartEmail(Part part, ReplyFormUIModel reply) throws Exception {
        DataSource dataSource = part.getDataHandler().getDataSource();
        MimeMultipart mimeMultipart = new MimeMultipart(dataSource);
        int multiPartCount = mimeMultipart.getCount();

        for (int j = 0; j < multiPartCount; j++) {
            BodyPart bp = mimeMultipart.getBodyPart(j);

            // if it is a text part, the,n this is the email content
            String disposition = bp.getDisposition();
            if (bp.isMimeType("text/*") &&  !Part.ATTACHMENT.equals(disposition)) {
                String content = FaxToMailServiceUtils.getTextFromPart(bp);
                content = Pattern.compile("<html>.*<body>", Pattern.DOTALL).matcher(content).replaceAll("");
                content = Pattern.compile("</body>.*</html>", Pattern.DOTALL).matcher(content).replaceAll("");
                reply.setMessage(content);

                // if it is multipart part, decompose it
            } else if (bp.isMimeType("multipart/*")) {
                decomposeMultipartEmail(bp, reply);

                // else, this is an attachment
            } else {
                String fileName = bp.getFileName();
                if (fileName == null) {
                    String[] header = bp.getHeader("Content-ID");
                    if (header != null && header.length > 0) {
                        fileName = header[0];
                        // remove the guillemets between the id
                        fileName = fileName.replaceFirst("^<(.*)>$", "$1");
                    }
                }
                if (fileName == null) {
                    fileName = t("faxtomail.email.content.attachment.unnamed", j);
                }
                fileName = MimeUtility.decodeText(fileName);
                if (log.isDebugEnabled()) {
                    log.debug("FileName : " + fileName);
                }

                ByteArrayOutputStream fos = new ByteArrayOutputStream();

                DataHandler dh = bp.getDataHandler();
                dh.writeTo(fos);

                // copy content into an empty attachment
                AttachmentFile attachmentFile = new AttachmentFileImpl();
                attachmentFile.setContent(fos.toByteArray());
                attachmentFile.setFilename(fileName);

                Attachment attachment = new AttachmentImpl();
                attachment.setOriginalFile(attachmentFile);

                ReplyAttachmentModel replyAttachmentModel = new ReplyAttachmentModel(attachment, true, fileName);

                reply.addAttachment(replyAttachmentModel);
            }
        }
    }

    protected class DemandReplyItemRenderer extends DemandReplyItem
                                            implements TableCellRenderer {

        public DemandReplyItemRenderer() {
            super(DemandRepliesUIHandler.this);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            Reply reply = (Reply) value;
            setReply(reply);

            setForward(DemandRepliesUIHandler.this.getUI().isForward());
            return this;
        }
    }

    protected class DemandReplyItemEditor extends AbstractCellEditor implements TableCellEditor {

        protected DemandReplyItem demandReplyItem;

        public DemandReplyItemEditor() {
            demandReplyItem = new DemandReplyItem(DemandRepliesUIHandler.this);
        }

        public Component getTableCellEditorComponent(JTable table, Object value,
                                                     boolean isSelected, int row, int column) {
            Reply reply = (Reply) value;
            demandReplyItem.setReply(reply);
            demandReplyItem.setForward(getUI().isForward());
            return demandReplyItem;
        }

        @Override
        public Object getCellEditorValue() {
            return demandReplyItem.getReply();
        }
    }
}
