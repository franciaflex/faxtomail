package com.franciaflex.faxtomail.ui.swing.util;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.color.CMMException;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class JImagePanel extends JPanel {

    private static final Log log = LogFactory.getLog(JImagePanel.class);

    protected Image image;
    protected Image scaledImage;
    protected int rotation = 0;
    protected int maxWidth = 0;


    protected boolean scaleImageToFitPanel;

    protected ComponentAdapter resizeAdapter = new ComponentAdapter() {
        @Override
        public void componentResized(ComponentEvent e) {
            Component component = e.getComponent();
            if (image != null) {
                int srcWidth = component.getWidth();
                int srcHeight = component.getHeight();
                int newWidth = rotation % 180 == 0 ? srcWidth : srcHeight;
                int newHeight = rotation % 180 == 0 ? srcHeight : srcWidth;
                scaledImage = image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
            }
            repaint();
        }
    };

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    public int getMaxWidth() {
        return maxWidth;
    }

    public void setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
    }

    public boolean isScaleImageToFitPanel() {
        return scaleImageToFitPanel;
    }

    public void setScaleImageToFitPanel(boolean scaleImageToFitPanel) {
        if (this.scaleImageToFitPanel != scaleImageToFitPanel) {
            if (scaleImageToFitPanel) {
                addComponentListener(resizeAdapter);
            } else {
                removeComponentListener(resizeAdapter);
            }
        }
        this.scaleImageToFitPanel = scaleImageToFitPanel;
    }

    public Image getImage() {
        return image;
    }

    public Image getScaledImage() {
        return scaledImage;
    }

    public void setImage(File img) {
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(img);

        } catch (IOException| CMMException e) {
            log.error("error while reading image " + img.getAbsolutePath(), e);
            bufferedImage = null;
        }
        if (bufferedImage != null) {
            setImage(bufferedImage);
        }
    }

    public void setImage(Image img) {
        this.image = img;
        this.scaledImage = img;

        int srcWidth = img.getWidth(null);
        int srcHeight = img.getHeight(null);
        int newWidth = rotation % 180 == 0 ? srcWidth : srcHeight;
        int newHeight = rotation % 180 == 0 ? srcHeight : srcWidth;

        if (getMaxWidth() > 0 && newWidth > getMaxWidth()) {
            newHeight = newHeight * getMaxWidth() / newWidth;
            newWidth = getMaxWidth();
            scaledImage = image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
        }

        Dimension size = new Dimension(newWidth, newHeight);
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
        setSize(size);
    }

    public void setImage(String img) {
        setImage(new ImageIcon(img).getImage());
    }

    @Override
    public void paintComponent(Graphics g) {
        if (scaledImage != null) {
            Graphics2D g2d = (Graphics2D) g;

            double width = getSize().getWidth();
            double height = getSize().getHeight();

            double translateX;
            double translateY;

            switch (rotation) {
                case 90:
                    translateX = width;
                    translateY = 0;
                    break;

                case 180:
                    translateX = width;
                    translateY = height;
                    break;

                case 270:
                    translateX = 0;
                    translateY = height;
                    break;

                default:
                    translateX = 0;
                    translateY = 0;
            }
            g2d.translate(translateX, translateY);
            g2d.rotate(Math.toRadians(rotation), 0, 0);

            Insets insets = getInsets();
            int leftInset = rotation % 180 == 0 ? insets.left : insets.top;
            int topInset = rotation % 180 == 0 ? insets.top : insets.left;
            g2d.drawImage(scaledImage, leftInset, topInset, this);

        }

    }
}
