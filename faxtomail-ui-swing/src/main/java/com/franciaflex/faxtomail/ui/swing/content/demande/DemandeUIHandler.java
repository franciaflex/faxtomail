package com.franciaflex.faxtomail.ui.swing.content.demande;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.History;
import com.franciaflex.faxtomail.persistence.entities.HistoryImpl;
import com.franciaflex.faxtomail.persistence.entities.HistoryType;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.persistence.entities.RangeRow;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentListener;
import com.franciaflex.faxtomail.ui.swing.content.demande.actions.SaveDemandeAction;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailBeanUIModel;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.google.common.collect.Sets;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.tab.TabHandler;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.MoveToNextEditableCellAction;
import org.nuiton.jaxx.application.swing.table.MoveToNextEditableRowAction;
import org.nuiton.jaxx.application.swing.table.MoveToPreviousEditableCellAction;
import org.nuiton.jaxx.application.swing.table.MoveToPreviousEditableRowAction;
import org.nuiton.jaxx.application.swing.util.CloseableUI;
import org.nuiton.util.beans.BeanMonitor;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Handler of UI {@link DemandeUIHandler}.
 *
 * @author kmorin - morin@codelutin.com
 *
 */
public class DemandeUIHandler extends AbstractFaxToMailUIHandler<DemandeUIModel, DemandeUI> implements CloseableUI, TabHandler {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DemandeUIHandler.class);

    protected BeanMonitor monitor;

    protected PropertyChangeListener rangeRowListener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            DemandeUIModel model = getModel();
            String propertyName = evt.getPropertyName();

            if (AbstractFaxToMailBeanUIModel.PROPERTY_VALID.equals(propertyName)) {
                RangeRowModel row = (RangeRowModel) evt.getSource();
                Boolean valid = (Boolean) evt.getNewValue();
                if (Boolean.TRUE.equals(valid)) {
                    model.addValidRangeRow(row);
                } else {
                    model.removeValidRangeRow(row);
                }
            }

            if (AbstractFaxToMailBeanUIModel.PROPERTY_VALID.equals(propertyName)
                    || RangeRow.PROPERTY_QUOTATION_QUANTITY.equals(propertyName)
                    || RangeRow.PROPERTY_PRODUCT_QUANTITY.equals(propertyName)
                    || RangeRow.PROPERTY_SAV_QUANTITY.equals(propertyName)) {

                RangeTableModel rangeTableModel = (RangeTableModel) getUI().getRangeTable().getModel();
                List<RangeRowModel> rangeRows = rangeTableModel.getRows();
                Map<String, Integer> quantities = FaxToMailUIUtil.computeQuantities(rangeRows);
                model.setQuotationNb(quantities.get(DemandeUIModel.PROPERTY_QUOTATION_NB));
                model.setPfNb(quantities.get(DemandeUIModel.PROPERTY_PF_NB));
                model.setSavNb(quantities.get(DemandeUIModel.PROPERTY_SAV_NB));
            }

        }
    };

    protected final PropertyChangeListener demandTypeListener = new PropertyChangeListener() {

        private int dividerLocation = -1;

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            DemandType oldType = (DemandType) evt.getOldValue();
            DemandType newType = (DemandType) evt.getNewValue();

            boolean oldRangePanelVisible = FaxToMailUIUtil.isRangePanelVisible(oldType);
            boolean newRangePanelVisible = FaxToMailUIUtil.isRangePanelVisible(newType);

            JSplitPane leftVerticalSplitPanel = getUI().getLeftVerticalSplitPanel();

            if (oldRangePanelVisible != newRangePanelVisible) {
                if (newRangePanelVisible) {
                    if (dividerLocation < 0) {
                        dividerLocation = leftVerticalSplitPanel.getLeftComponent().getPreferredSize().height + 10;
                    }
                    leftVerticalSplitPanel.setDividerLocation(dividerLocation);

                } else {
                    dividerLocation = leftVerticalSplitPanel.getDividerLocation();
                    leftVerticalSplitPanel.setDividerLocation(leftVerticalSplitPanel.getHeight());
                }

                leftVerticalSplitPanel.setName("leftVerticalSplitPanel" + newRangePanelVisible);
                getContext().getSwingSession().add(leftVerticalSplitPanel, true);
            }
        }
    };

    protected AttachmentListener attachmentListener = new AttachmentListener() {

        @Override
        public void onAttachmentOpened(Attachment attachment, boolean original) {
            String topiaId = getModel().getTopiaId();
            if (topiaId != null && attachment.isPersisted()) {
                String filename;
                if (original) {
                    filename = attachment.getOriginalFileName();
                } else {
                    filename = FaxToMailUIUtil.getEditedFileName(attachment.getOriginalFileName());
                }
                try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
                    Email email = serviceContext.getEmailService().addToHistory(topiaId,
                            HistoryType.ATTACHMENT_OPENING,
                            getContext().getCurrentUser(),
                            new Date(),
                            filename);
                    getModel().setHistory(email.getHistory());
                } catch (IOException eee) {
                    log.error("Cannot get back email history",eee);
                }
            }
        }

        @Override
        public void onAttachmentEdited(Attachment attachment) {
            String topiaId = getModel().getTopiaId();
            if (topiaId != null && attachment.isPersisted()) {
                //TODO create a history manually and add it to the email, but do not save it
//                            FaxToMailServiceContext serviceContext = getContext().newServiceContext();
//                            Email email = serviceContext.getEmailService().addToHistory(topiaId,
//                                                                                      HistoryType.ATTACHMENT_MODIFICATION,
//                                                                                      getContext().getCurrentUser(),
//                                                                                      new Date(),
//                                                                                      FaxToMailUIUtil.getEditedFileName(attachment.getOriginalFileName()));
//                            getModel().setHistory(email.getHistory());

                History history = new HistoryImpl();
                history.setType(HistoryType.ATTACHMENT_MODIFICATION);
                history.setFaxToMailUser(getContext().getCurrentUser());
                history.setModificationDate(new Date());
                history.setFields(Sets.newHashSet(attachment.getOriginalFileName()));
                getModel().getHistory().add(history);
            }
            getModel().setModify(true);
        }

    };


    @Override
    public void afterInit(DemandeUI ui) {

        initUI(ui);

        final DemandeUIModel model = getModel();
        Set<String> propertiesToIgnore = getPropertiesToIgnore();
        monitor = new BeanMonitor(true, propertiesToIgnore.toArray(new String[propertiesToIgnore.size()]));
        monitor.setBean(model);

        MailFolder folder = model.getMailFolder();

        initBeanFilterableComboBox(ui.getPriorityComboBox(), getContext().getPriorityCache(), model.getPriority());
        initBeanFilterableComboBox(ui.getStatusComboBox(), Arrays.asList(DemandStatus.values()), model.getDemandStatus());

        // utilisation des etats d'attente défini pour le dossier (si défini)
        List<DemandType> demandTypes = new ArrayList<>();
        Collection<DemandType> folderDemandTypes = getDemandTypeForFolder(folder);
        if (CollectionUtils.isEmpty(folderDemandTypes)) {
            demandTypes.addAll(getContext().getDemandTypeCache());
        } else {
            demandTypes.addAll(folderDemandTypes);
        }
        initBeanFilterableComboBox(ui.getDocTypeComboBox(), demandTypes, model.getDemandType());

        // utilisation des etats d'attente défini pour le dossier (si défini)
        List<WaitingState> waitingStates = new ArrayList<>();
        Collection<WaitingState> folderWaitingStates = getWaitingStateForFolder(folder);
        if (CollectionUtils.isEmpty(folderWaitingStates)) {
            waitingStates.addAll(getContext().getWaitingStateCache());
        } else {
            waitingStates.addAll(folderWaitingStates);
        }
        initBeanFilterableComboBox(ui.getWaitingStateComboBox(), waitingStates, model.getWaitingState());

        final JPanel mailBodyPanel = ui.getMailBodyPanel();
        FaxToMailUIUtil.setEmailContentInTextPane(this, model, mailBodyPanel, getConfig());

        FaxToMailUIUtil.initScrollPaneBars(ui.getMailBodyScrollPane());

        // init table
        final JXTable table = ui.getRangeTable();

        // utilisation des gammes définies pour le dossier (si définies)
        List<Range> ranges = new ArrayList<>();
        Collection<Range> folderRanges = getRangeForFolder(folder);
        if (CollectionUtils.isEmpty(folderRanges)) {
            ranges.addAll(getContext().getRangeCache());
        } else {
            ranges.addAll(folderRanges);
        }
        ranges.sort((o1, o2) -> ObjectUtils.compare(o1.getLabel(), o2.getLabel()));

        TableColumnModelExt columnModel = new DefaultTableColumnModelExt();
        addComboDataColumnToModel(columnModel,
                                  RangeTableModel.RANGE_COLUMN,
                                  getDecorator(Range.class, null),
                                  ranges);
        addColumnToModel(columnModel,
                         RangeTableModel.COMMAND_NUMBER_COLUMN);
        addIntegerColumnToModel(columnModel,
                                RangeTableModel.QUOTATION_QUANTITY_COLUMN,
                                null,
                                table);
        addIntegerColumnToModel(columnModel,
                                RangeTableModel.PRODUCT_QUANTITY_COLUMN,
                                null,
                                table);
        addIntegerColumnToModel(columnModel,
                                RangeTableModel.SAV_QUANTITY_COLUMN,
                                null,
                                table);

        // init range model
        final RangeTableModel rangeTableModel = new RangeTableModel(columnModel);
        List<RangeRowModel> rangeRowModels = new ArrayList<RangeRowModel>(model.getValidRangeRowModels());
        rangeTableModel.setRows(rangeRowModels);

        Map<String, Integer> quantities = FaxToMailUIUtil.computeQuantities(rangeRowModels);
        model.setQuotationNb(quantities.get(DemandeUIModel.PROPERTY_QUOTATION_NB));
        model.setPfNb(quantities.get(DemandeUIModel.PROPERTY_PF_NB));
        model.setSavNb(quantities.get(DemandeUIModel.PROPERTY_SAV_NB));

        // add listener to update row validity and quantity totals
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            protected RangeRowModel currentRow;

            @Override
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel source = (ListSelectionModel) e.getSource();
                if (currentRow != null) {
                    currentRow.removePropertyChangeListener(rangeRowListener);
                }

                if (source.isSelectionEmpty()) {
                    currentRow = null;
                } else {
                    int rowIndex = source.getLeadSelectionIndex();
                    rowIndex = table.convertRowIndexToModel(rowIndex);
                    currentRow = rangeTableModel.getEntry(rowIndex);
                    currentRow.addPropertyChangeListener(rangeRowListener);
                }
            }
        });

        table.setModel(rangeTableModel);
        table.setColumnModel(columnModel);
        table.getTableHeader().setReorderingAllowed(false);

        final MoveToNextEditableCellAction nextCellAction =
                MoveToNextEditableCellAction.newAction(rangeTableModel, table);
        final MoveToPreviousEditableCellAction previousCellAction =
                MoveToPreviousEditableCellAction.newAction(rangeTableModel, table);

        final MoveToNextEditableRowAction nextRowAction =
                MoveToNextEditableRowAction.newAction(rangeTableModel, table);
        final MoveToPreviousEditableRowAction previousRowAction =
                MoveToPreviousEditableRowAction.newAction(rangeTableModel, table);

        KeyAdapter keyAdapter = new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                TableCellEditor editor = table.getCellEditor();

                int keyCode = e.getKeyCode();
                if (keyCode == KeyEvent.VK_LEFT ||
                        (keyCode == KeyEvent.VK_TAB && e.isShiftDown())) {
                    e.consume();
                    if (editor != null) {
                        editor.stopCellEditing();
                    }
                    previousCellAction.actionPerformed(null);

                } else if (keyCode == KeyEvent.VK_RIGHT ||
                        keyCode == KeyEvent.VK_TAB) {
                    e.consume();
                    if (editor != null) {
                        editor.stopCellEditing();
                    }
                    nextCellAction.actionPerformed(null);

                } else if (keyCode == KeyEvent.VK_UP ||
                        (keyCode == KeyEvent.VK_ENTER && e.isShiftDown())) {
                    e.consume();
                    if (editor != null) {
                        editor.stopCellEditing();
                    }
                    previousRowAction.actionPerformed(null);

                } else if (e.getKeyCode() == KeyEvent.VK_ENTER ||
                        keyCode == KeyEvent.VK_DOWN) {
                    e.consume();
                    if (editor != null) {
                        editor.stopCellEditing();
                    }
                    nextRowAction.actionPerformed(null);
                }
            }
        };

        table.addKeyListener(keyAdapter);
        HighlightPredicate rowIsInvalidPredicate = new HighlightPredicate() {
            @Override
            public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
                AbstractApplicationTableModel model = (AbstractApplicationTableModel) table.getModel();
                int viewRow = adapter.row;
                int modelRow = adapter.convertRowIndexToModel(viewRow);
                RangeRowModel row = (RangeRowModel) model.getEntry(modelRow);
                boolean result = !row.isValid();
                return result;
            }
        };
        Color color = new Color(255, 51, 51);
        table.addHighlighter(new ColorHighlighter(rowIsInvalidPredicate, color, Color.WHITE, color.darker(), Color.WHITE));

        SwingValidator validator = getValidator();
        listenValidatorValid(validator, model);

        // if new fishingOperation can already cancel his creation
        model.setModify(false);

        ui.getAttachmentsButton().getBean().addAttachmentListener(attachmentListener);

        JSplitPane leftVerticalSplitPanel = getUI().getLeftVerticalSplitPanel();

        model.addPropertyChangeListener(Email.PROPERTY_DEMAND_TYPE, demandTypeListener);

        leftVerticalSplitPanel.setName("leftVerticalSplitPanel" + FaxToMailUIUtil.isRangePanelVisible(model.getDemandType()));
        getContext().getSwingSession().add(leftVerticalSplitPanel, true);

        JPopupMenu menu = ui.getMatchingClientsPopup();
        Collection<Client> matchingClients = model.getMatchingClients();
        if (matchingClients != null) {
            for (final Client client : matchingClients) {
                menu.add(new JMenuItem(new AbstractAction(decorate(client)) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        getModel().setClient(client);
                    }
                }));
            }
        }

        Date now = new Date();
        final BeanFilterableComboBox<Client> clientComboBox = ui.getClientComboBox();
        clientComboBox.getComboBoxModel().setWildcardCharacter(null);
        initBeanFilterableComboBox(clientComboBox, Collections.singletonList(model.getClient()), model.getClient());

        long time = new Date().getTime()- now.getTime();
        if (log.isDebugEnabled()) {
            log.debug("Chargement combo clients : " + time + " ms");
        }

        KeyAdapter clientComboBoxKeyAdapter = new KeyAdapter() {

            @Override
            public void keyTyped(KeyEvent e) {
                String text = clientComboBox.getComboBoxModel().getFilterText();
                text+=e.getKeyChar();

                int filterStartChars = getConfig().getClientComboBoxFilterStartChars();

                if (text.length()==filterStartChars){
                    //init list when x chars entered
                    try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
                        List<Client> clients = serviceContext.getClientService().getClientsForFolder(getContext().getCurrentMailFolder(), text);
                        clientComboBox.getComboBoxModel().removeAllElements();
                        clientComboBox.getComboBoxModel().addAllElements(clients);
                    } catch (IOException eee) {
                        log.error("Cannot populate client combobox",eee);
                    }
                } else if (text.length()<filterStartChars){
                    //empty list if less than x chars
                    clientComboBox.getComboBoxModel().removeAllElements();
                } else {
                    // do nothing if more than x chars entered
                }
            }
        };

        clientComboBox.getCombobox().getEditor().getEditorComponent().addKeyListener(clientComboBoxKeyAdapter);

        listModelIsModify(model);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getObjectField();
    }

    @Override
    protected Set<String> getPropertiesToIgnore() {
        Set<String> result = super.getPropertiesToIgnore();
        result.add(DemandeUIModel.PROPERTY_EDITABLE);
        result.add(Email.PROPERTY_HISTORY);
        result.add(DemandeUIModel.PROPERTY_GROUPED_DEMANDES);
        result.add(Email.PROPERTY_REPLIES);
        result.add(DemandeUIModel.PROPERTY_VALID_RANGE_ROW_MODELS);
        return result;
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }

            DemandeUIModel model = getModel();

            model.removePropertyChangeListener(listModelListener);
            model.removePropertyChangeListener(Email.PROPERTY_DEMAND_TYPE, demandTypeListener);
            for (RangeRowModel row : model.getValidRangeRowModels()) {
                row.removePropertyChangeListener(rangeRowListener);
            }

            ui.getAttachmentsButton().onCloseUI();
            ui.getHistoryButton().onCloseUI();
            ui.getAttachmentsButton().getBean().removeAttachmentListener(attachmentListener);
            ui.getDemandRepliesButton().onCloseUI();

            clearValidators();
    }

    public void closeButtonPopups() {
        ui.getAttachmentsButton().setSelected(false);
        ui.getHistoryButton().setSelected(false);
        ui.getDemandRepliesButton().setSelected(false);
    }

    @Override
    public boolean quitUI() {
        MailFolder folderWithShowHelp = getModel().getMailFolder();
        while (folderWithShowHelp.getParent() != null && folderWithShowHelp.getDisplayHelpOnMessages() == null) {
            folderWithShowHelp = folderWithShowHelp.getParent();
        }
        boolean showHelp = Boolean.TRUE.equals(folderWithShowHelp.getDisplayHelpOnMessages());
        String askCancelEditBeforeLeavingMessage;
        String askSaveBeforeLeavingMessage;
        if (showHelp) {
            askCancelEditBeforeLeavingMessage = n("faxtomail.demande.askCancelEditBeforeLeaving.cancelSave");
            askSaveBeforeLeavingMessage = n("faxtomail.demande.askSaveBeforeLeaving.save");
        } else {
            askCancelEditBeforeLeavingMessage = n("faxtomail.demande.askCancelEditBeforeLeaving.cancelSave.noHelp");
            askSaveBeforeLeavingMessage = n("faxtomail.demande.askSaveBeforeLeaving.save.noHelp");
        }
        boolean result = quitScreen2(
                true,
                getModel().isModify(),
                t(askCancelEditBeforeLeavingMessage),
                t(askSaveBeforeLeavingMessage),
                showHelp,
                getContext().getActionFactory().createLogicAction(this, SaveDemandeAction.class)
        );

        if (result) {
            try(FaxToMailServiceContext faxToMailServiceContext = getContext().newServiceContext()) {
                faxToMailServiceContext.getEmailService().unlockEmail(getModel().getTopiaId());
            } catch (IOException e) {
                log.warn("Could not unlock email");
            }
        }

        return result;
    }

    @Override
    public SwingValidator<DemandeUIModel> getValidator() {
        return ui.getValidator();
    }

    @Override
    public Component getTopestUI() {
        return getUI();
    }

    /**
     * Récupère recursivement jusqu'au parent, les etats d'attentes définis pour un dossier.
     * 
     * @param folder base folder
     * @return etat d'attente to use
     */
    protected Collection<WaitingState> getWaitingStateForFolder(MailFolder folder) {
        Collection<WaitingState> result = null;
        while (CollectionUtils.isEmpty(result) && folder != null) {
            result = folder.getWaitingStates();
            folder = folder.getParent();
        }
        return result;
    }

    /**
     * Récupère recursivement jusqu'au parent, les types de demande définies pour un dossier.
     * 
     * @param folder base folder
     * @return etat d'attente to use
     */
    protected Collection<DemandType> getDemandTypeForFolder(MailFolder folder) {
        Collection<DemandType> result = null;
        while (CollectionUtils.isEmpty(result) && folder != null) {
            result = folder.getDemandTypes();
            folder = folder.getParent();
        }
        return result;
    }

    /**
     * Récupère recursivement jusqu'au parent, les gammes définies pour un dossier.
     * 
     * @param folder base folder
     * @return etat d'attente to use
     */
    protected Collection<Range> getRangeForFolder(MailFolder folder) {
        Collection<Range> result = null;
        while (CollectionUtils.isEmpty(result) && folder != null) {
            result = folder.getRanges();
            folder = folder.getParent();
        }
        return result;
    }

    public BeanMonitor getMonitor() {
        return monitor;
    }

    @Override
    public boolean onHideTab(int currentIndex, int newIndex) {
        closeButtonPopups();
        return true;
    }

    @Override
    public void onShowTab(int currentIndex, int newIndex) {
        registerValidators(getValidator());
        JFrame frame = SwingUtil.getParentContainer(getUI(), JFrame.class);
        if (frame != null) {
            frame.setTitle(getModel().getTitle());
        }
    }

    @Override
    public boolean onRemoveTab() {
        boolean result = quitUI();
        if (result) {
            onCloseUI();
        }
        return result;
    }

    public void showMatchingClientsPopup(ActionEvent event) {
        JButton button = (JButton) event.getSource();
        getUI().getMatchingClientsPopup().show(button, 0, button.getBounds().height);
    }

    protected void registerValidators(SwingValidator... validators) {
        DemandesUI demandesUI = SwingUtil.getParent(getUI(), DemandesUI.class);
        DemandesUIHandler handler = demandesUI.getHandler();
        handler.clearValidators();
        for (SwingValidator validator : validators) {
            handler.registerValidator(validator);
        }
    }

    protected void clearValidators() {
        DemandesUI demandesUI = SwingUtil.getParent(getUI(), DemandesUI.class);
        DemandesUIHandler handler = demandesUI.getHandler();
        handler.clearValidators();
    }
}
