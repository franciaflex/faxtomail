package com.franciaflex.faxtomail.ui.swing.content.print;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import org.jdesktop.beans.AbstractBean;

import java.util.Collection;
import java.util.HashSet;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class AttachmentToPrintChooserUIModel extends AbstractBean {

    public static final String PROPERTY_ATTACHEMENTS_TO_PRINT = "attachmentsToPrint";

    protected DemandeUIModel demand;

    protected Collection<AttachmentFile> attachmentsToPrint = new HashSet<AttachmentFile>();

    public DemandeUIModel getDemand() {
        return demand;
    }

    public void setDemand(DemandeUIModel demand) {
        this.demand = demand;
    }

    public Collection<AttachmentFile> getAttachmentsToPrint() {
        return attachmentsToPrint;
    }

    public void setAttachmentsToPrint(Collection<AttachmentFile> attachmentsToPrint) {
        Object oldValue = new HashSet<AttachmentFile>(this.attachmentsToPrint);
        this.attachmentsToPrint.clear();
        this.attachmentsToPrint.addAll(attachmentsToPrint);
        firePropertyChange(PROPERTY_ATTACHEMENTS_TO_PRINT, oldValue, this.attachmentsToPrint);
    }

    public void addAttachmentToPrint(AttachmentFile attachmentFile) {
        Object oldValue = new HashSet<AttachmentFile>(this.attachmentsToPrint);
        this.attachmentsToPrint.add(attachmentFile);
        firePropertyChange(PROPERTY_ATTACHEMENTS_TO_PRINT, oldValue, this.attachmentsToPrint);
    }

    public void removeAttachmentToPrint(AttachmentFile attachmentFile) {
        Object oldValue = new HashSet<AttachmentFile>(this.attachmentsToPrint);
        this.attachmentsToPrint.remove(attachmentFile);
        firePropertyChange(PROPERTY_ATTACHEMENTS_TO_PRINT, oldValue, this.attachmentsToPrint);
    }
}
