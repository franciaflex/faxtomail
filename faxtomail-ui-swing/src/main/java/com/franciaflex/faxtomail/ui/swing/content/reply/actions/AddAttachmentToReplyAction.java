package com.franciaflex.faxtomail.ui.swing.content.reply.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyAttachmentModel;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUI;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUIModel;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class AddAttachmentToReplyAction extends AbstractFaxToMailAction<ReplyFormUIModel, ReplyFormUI, ReplyFormUIHandler> {

    protected ReplyAttachmentModel replyAttachmentModel;

    public AddAttachmentToReplyAction(ReplyFormUIHandler handler) {
        super(handler, false);
        setActionDescription("faxtomail.action.addAttachmentToReply.tip");
    }

    public ReplyAttachmentModel getReplyAttachmentModel() {
        return replyAttachmentModel;
    }

    public void setReplyAttachmentModel(ReplyAttachmentModel replyAttachmentModel) {
        this.replyAttachmentModel = replyAttachmentModel;
    }

    @Override
    public void doAction() throws Exception {
        Attachment attachment = replyAttachmentModel.getAttachment();
        if (replyAttachmentModel.getAttachmentFile() == null && attachment.isPersisted()) {
            try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
                EmailService service = serviceContext.getEmailService();
                boolean original = replyAttachmentModel.isOriginal();
                AttachmentFile file = service.getAttachmentFile(attachment.getTopiaId(),
                        original);
                if (original) {
                    attachment.setOriginalFile(file);
                } else {
                    attachment.setEditedFile(file);
                }
            }
        }
    }
}
