package com.franciaflex.faxtomail.ui.swing.content.pdfeditor;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.AttachmentImpl;
import com.franciaflex.faxtomail.persistence.entities.Stamp;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailBeanUIModel;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class PDFEditorUIModel extends AbstractFaxToMailBeanUIModel<Attachment, PDFEditorUIModel> {

    public static final String PROPERTY_PAGES = "pages";
    public static final String PROPERTY_CURRENT_PAGE_INDEX = "currentPageIndex";
    public static final String PROPERTY_ZOOM = "zoom";
    public static final String PROPERTY_ROTATION = "rotation";
    public static final String PROPERTY_STAMPS = "stamps";

    protected final Attachment editObject = new AttachmentImpl();

    protected DemandeUIModel demand;

    protected Collection<Stamp> stamps = new HashSet<>();

    protected Object selectedComponent;

    public enum EditionComponent {
        NOTE, CROSS, HLINE, VLINE, HIGHLIGHTER
    }

    public class Page {

        protected List<PDFEditorNoteUI> notes = new ArrayList<>();
        protected List<PDFEditorCrossUI> crosses = new ArrayList<>();
        protected List<PDFEditorLineUI> lines = new ArrayList<>();
        protected List<PDFEditorHighlighterUI> highlighters = new ArrayList<>();
        protected List<PDFEditorStampTextUI> textStamps = new ArrayList<>();
        protected List<PDFEditorStampImageUI> imageStamps = new ArrayList<>();

        public List<PDFEditorNoteUI> getNotes() {
            return notes;
        }

        public void addNote(PDFEditorNoteUI note) {
            notes.add(note);
            setModify(true);
        }

        public void removeNote(PDFEditorNoteUI note) {
            notes.remove(note);
            setModify(true);
        }

        public List<PDFEditorCrossUI> getCrosses() {
            return crosses;
        }

        public void addCross(PDFEditorCrossUI cross) {
            crosses.add(cross);
            setModify(true);
        }

        public void removeCross(PDFEditorCrossUI cross) {
            crosses.remove(cross);
            setModify(true);
        }

        public List<PDFEditorLineUI> getLines() {
            return lines;
        }

        public void addLine(PDFEditorLineUI line) {
            lines.add(line);
            setModify(true);
        }

        public void removeLine(PDFEditorLineUI line) {
            lines.remove(line);
            setModify(true);
        }

        public List<PDFEditorHighlighterUI> getHighlighters() {
            return highlighters;
        }

        public void addHighlighter(PDFEditorHighlighterUI highlighter) {
            highlighters.add(highlighter);
            setModify(true);
        }

        public void removeHighlighter(PDFEditorHighlighterUI highlighter) {
            highlighters.remove(highlighter);
            setModify(true);
        }

        public List<PDFEditorStampTextUI> getTextStamps() {
            return textStamps;
        }

        public void addTextStamp(PDFEditorStampTextUI stamp) {
            textStamps.add(stamp);
            setModify(true);
        }

        public void removeTextStamp(PDFEditorStampTextUI stamp) {
            textStamps.remove(stamp);
            setModify(true);
        }

        public List<PDFEditorStampImageUI> getImageStamps() {
            return imageStamps;
        }

        public void addImageStamp(PDFEditorStampImageUI stamp) {
            imageStamps.add(stamp);
            setModify(true);
        }

        public void removeImageStamp(PDFEditorStampImageUI stamp) {
            imageStamps.remove(stamp);
            setModify(true);
        }
    }

    protected Page[] pages;

    // first page is 1 not 0
    protected int currentPageIndex = -1;

    protected float zoom = 1.0f;

    protected int rotation = 0;

    protected static Binder<PDFEditorUIModel, Attachment> toBeanBinder =
            BinderFactory.newBinder(PDFEditorUIModel.class,
                                    Attachment.class);

    protected static Binder<Attachment, PDFEditorUIModel> fromBeanBinder =
            BinderFactory.newBinder(Attachment.class, PDFEditorUIModel.class);

    public PDFEditorUIModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    public void setOriginalFile(AttachmentFile file) {
        Object oldValue = getOriginalFile();
        editObject.setOriginalFile(file);
        firePropertyChange(Attachment.PROPERTY_ORIGINAL_FILE, oldValue, file);
    }

    public AttachmentFile getOriginalFile() {
        return editObject.getOriginalFile();
    }

    public void setEditedFile(AttachmentFile file) {
        Object oldValue = getEditedFile();
        editObject.setEditedFile(file);
        firePropertyChange(Attachment.PROPERTY_EDITED_FILE, oldValue, file);
        setRotation(file != null ? file.getRotation() : 0);
    }

    public AttachmentFile getEditedFile() {
        return editObject.getEditedFile();
    }

    public AttachmentFile getNotNullFile() {
        AttachmentFile file = getEditedFile();
        if (file == null) {
            file = getOriginalFile();
        }
        return file;
    }

    public String getTopiaId() {
        return editObject.getTopiaId();
    }

    public void setTopiaId(String id) {
        Object oldValue = getTopiaId();
        editObject.setTopiaId(id);
        firePropertyChange(TopiaEntity.PROPERTY_TOPIA_ID, oldValue, id);
    }

    public Page[] getPages() {
        return pages;
    }

    public void setPageNumber(int nb) {
        pages = new Page[nb];
        for (int i = 0 ; i < nb ; i++) {
            pages[i] = new Page();
        }
        firePropertyChanged(PROPERTY_PAGES, null, pages);
    }

    public Page getCurrentPage() {
        if (pages == null || currentPageIndex < 1 || currentPageIndex > pages.length) {
            return null;
        }
        return pages[currentPageIndex - 1];
    }

    public Page getPage(int i) {
        if (pages == null || i < 1 || i > pages.length) {
            return null;
        }
        return pages[i - 1];
    }

    public int getCurrentPageIndex() {
        return currentPageIndex;
    }

    public void setCurrentPageIndex(int currentPageIndex) {
        Object oldValue = getCurrentPageIndex();
        currentPageIndex = Math.max(1, currentPageIndex);
        if (pages != null) {
            currentPageIndex = Math.min(pages.length, currentPageIndex);
        }
        this.currentPageIndex = currentPageIndex;
        firePropertyChange(PROPERTY_CURRENT_PAGE_INDEX, oldValue, currentPageIndex);
    }

    public void decPageIndex() {
        setCurrentPageIndex(currentPageIndex - 1);
    }

    public void incPageIndex() {
        setCurrentPageIndex(currentPageIndex + 1);
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        float normalizedZoomValue = Math.min(Math.max(zoom, 1f), 15f);
        float oldValue = getZoom();
        this.zoom = normalizedZoomValue;
        firePropertyChanged(PROPERTY_ZOOM, oldValue, normalizedZoomValue);
    }

    public int getRotation() {
        return rotation;
    }

    public void setRotation(int rotation) {
        Object oldValue = getRotation();
        this.rotation = rotation;
        firePropertyChanged(PROPERTY_ROTATION, oldValue, rotation);
    }

    public DemandeUIModel getDemand() {
        return demand;
    }

    public void setDemand(DemandeUIModel demand) {
        this.demand = demand;
    }

    public Collection<Stamp> getStamps() {
        return stamps;
    }

    public void setStamps(Collection<Stamp> stamps) {
        this.stamps.clear();
        if (stamps != null) {
            this.stamps.addAll(stamps);
        }
        firePropertyChange(PROPERTY_STAMPS, null, this.stamps);
    }

    public Object getSelectedComponent() {
        return selectedComponent;
    }

    public void setSelectedComponent(Object selectedComponent) {
        this.selectedComponent = selectedComponent;
    }

    @Override
    protected Attachment newEntity() {
        return new AttachmentImpl();
    }
}
