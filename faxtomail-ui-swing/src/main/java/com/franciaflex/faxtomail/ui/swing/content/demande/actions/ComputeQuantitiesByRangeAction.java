package com.franciaflex.faxtomail.ui.swing.content.demande.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.beans.Quantities;
import com.franciaflex.faxtomail.beans.QuantitiesByRange;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.persistence.entities.RangeImpl;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.QuantitiesByRangeUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.QuantitiesByRangeUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.QuantitiesByRangeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.RangeRowModel;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class ComputeQuantitiesByRangeAction extends AbstractFaxToMailAction<QuantitiesByRangeUIModel, QuantitiesByRangeUI, QuantitiesByRangeUIHandler> {

    private boolean error = false;

    public ComputeQuantitiesByRangeAction(QuantitiesByRangeUIHandler handler) {
        super(handler, false);
        setActionDescription(t("faxtomail.action.computeQuantitiesByRange.tip"));
    }

    @Override
    public void doAction() throws Exception {
        MailFolder folder = getModel().getRootFolder();
        try (FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            QuantitiesByRange quantitiesByRange = serviceContext.getEmailService().computeQuantitiesByRange(folder);

            //inProgress demands Model building
            List<RangeRowModel> inProgressRangeRows = new ArrayList<>();
            Map<Range, Quantities> inProgressQuantitiesByRangeMap = quantitiesByRange.getInProgressQuantitiesByRange();
            for (Map.Entry<Range, Quantities> entry : inProgressQuantitiesByRangeMap.entrySet()) {
                RangeRowModel row = new RangeRowModel();
                row.setRange(entry.getKey());

                Quantities quantities = entry.getValue();

                Long productQuantity = quantities.getProductQuantity();
                if (productQuantity == null) {
                    productQuantity = 0L;
                }
                row.setProductQuantity(productQuantity.intValue());

                Long savQuantity = quantities.getSavQuantity();
                if (savQuantity == null) {
                    savQuantity = 0L;
                }
                row.setSavQuantity(savQuantity.intValue());

                Long quotationQuantity = quantities.getQuotationQuantity();
                if (quotationQuantity == null) {
                    quotationQuantity = 0L;
                }
                row.setQuotationQuantity(quotationQuantity.intValue());

                inProgressRangeRows.add(row);
            }

            Quantities inProgressDemandsQuantities = quantitiesByRange.getInProgressDemandsQuantities();
            if (inProgressDemandsQuantities != null) {
                RangeRowModel inProgressRow = createRangeRowModelForQuantities(inProgressDemandsQuantities,
                        t("faxtomail.rangeRows.quantities.subtotal.label"));
                inProgressRangeRows.add(inProgressRow);
            }

            //waiting demands Model building
            List<RangeRowModel> waitingRangeRows = new ArrayList<>();
            Map<Range, Quantities> waitingQuantitiesByRangeMap = quantitiesByRange.getWaitingQuantitiesByRange();
            for (Map.Entry<Range, Quantities> entry : waitingQuantitiesByRangeMap.entrySet()) {
                RangeRowModel row = new RangeRowModel();
                row.setRange(entry.getKey());

                Quantities quantities = entry.getValue();

                Long productQuantity = quantities.getProductQuantity();
                if (productQuantity == null) {
                    productQuantity = 0L;
                }
                row.setProductQuantity(productQuantity.intValue());

                Long savQuantity = quantities.getSavQuantity();
                if (savQuantity == null) {
                    savQuantity = 0L;
                }
                row.setSavQuantity(savQuantity.intValue());

                Long quotationQuantity = quantities.getQuotationQuantity();
                if (quotationQuantity == null) {
                    quotationQuantity = 0L;
                }
                row.setQuotationQuantity(quotationQuantity.intValue());

                waitingRangeRows.add(row);
            }

            Quantities waitingDemandsQuantities = quantitiesByRange.getWaitingQuantities();
            if (waitingDemandsQuantities != null) {
                RangeRowModel waitingRow = createRangeRowModelForQuantities(waitingDemandsQuantities,
                        t("faxtomail.rangeRows.quantities.subtotal.label"));
                waitingRangeRows.add(waitingRow);
            }

            RangeRowModel totalRow = createRangeRowModelForQuantities(quantitiesByRange.getTotalQuantities(), "");
            List<RangeRowModel> totalRangeRows = new ArrayList<>();
            totalRangeRows.add(totalRow);

            getModel().setEnCoursRangeRows(inProgressRangeRows);
            getModel().setEnAttenteRangeRows(waitingRangeRows);
            getModel().setTotalsRangeRows(totalRangeRows);
        }
    }

    protected RangeRowModel createRangeRowModelForQuantities(Quantities quantities, String label) {
        RangeRowModel row = new RangeRowModel();
        Range range = new RangeImpl();
        range.setLabel(label);
        row.setRange(range);
        row.setProductQuantity(quantities.getProductQuantity().intValue());
        row.setSavQuantity(quantities.getSavQuantity().intValue());
        row.setQuotationQuantity(quantities.getQuotationQuantity().intValue());
        return row;
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        error = false;
    }

    @Override
    public void postFailedAction(Throwable error) {
        super.postFailedAction(error);
        this.error = true;
    }

    @Override
    protected void releaseAction() {
        super.releaseAction();

        if (!error) {
            handler.openDialog(getUI(), t("faxtomail.quantitiesByRange.title"), new Dimension(450, 300));
        }
        error = false;
    }
}
