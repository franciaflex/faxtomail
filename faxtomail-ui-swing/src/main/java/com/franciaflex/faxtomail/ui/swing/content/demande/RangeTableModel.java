package com.franciaflex.faxtomail.ui.swing.content.demande;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.RangeRow;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class RangeTableModel extends AbstractApplicationTableModel<RangeRowModel> {

    public static final ColumnIdentifier<RangeRow> RANGE_COLUMN = ColumnIdentifier.newId(
            RangeRow.PROPERTY_RANGE,
            n("faxtomail.rangeRows.table.header.range"),
            n("faxtomail.rangeRows.table.header.range.tip"));

    public static final ColumnIdentifier<RangeRow> COMMAND_NUMBER_COLUMN = ColumnIdentifier.newId(
            RangeRow.PROPERTY_COMMAND_NUMBER,
            n("faxtomail.rangeRows.table.header.commandNumber"),
            n("faxtomail.rangeRows.table.header.commandNumber.tip"));

    public static final ColumnIdentifier<RangeRow> QUOTATION_QUANTITY_COLUMN = ColumnIdentifier.newId(
            RangeRow.PROPERTY_QUOTATION_QUANTITY,
            n("faxtomail.rangeRows.table.header.quotationColumn"),
            n("faxtomail.rangeRows.table.header.quotationColumn.tip"));

    public static final ColumnIdentifier<RangeRow> PRODUCT_QUANTITY_COLUMN = ColumnIdentifier.newId(
            RangeRow.PROPERTY_PRODUCT_QUANTITY,
            n("faxtomail.rangeRows.table.header.productColumn"),
            n("faxtomail.rangeRows.table.header.productColumn.tip"));

    public static final ColumnIdentifier<RangeRow> SAV_QUANTITY_COLUMN = ColumnIdentifier.newId(
            RangeRow.PROPERTY_SAV_QUANTITY,
            n("faxtomail.rangeRows.table.header.savQuantity"),
            n("faxtomail.rangeRows.table.header.savQuantity.tip"));

    public RangeTableModel(TableColumnModelExt columnModel) {
        super(columnModel, true, true);
        setNoneEditableCols();
    }

    @Override
    public RangeRowModel createNewRow() {
        RangeRowModel row = new RangeRowModel();
        row.setProductQuantity(0);
        row.setSavQuantity(0);
        row.setQuotationQuantity(0);
        row.setValid(false);
        return row;
    }

}
