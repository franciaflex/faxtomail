package com.franciaflex.faxtomail.ui.swing.content.demande;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentImpl;
import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.EmailGroup;
import com.franciaflex.faxtomail.persistence.entities.EmailImpl;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.History;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.OriginalEmail;
import com.franciaflex.faxtomail.persistence.entities.Priority;
import com.franciaflex.faxtomail.persistence.entities.RangeRow;
import com.franciaflex.faxtomail.persistence.entities.Reply;
import com.franciaflex.faxtomail.persistence.entities.ReplyImpl;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentModelAware;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailBeanUIModel;
import com.google.common.collect.Lists;
import jaxx.runtime.JAXXUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.util.MimeMessageUtils;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * @author kmorin - kmorin@codelutin.com
 *
 */
public class DemandeUIModel extends AbstractFaxToMailBeanUIModel<Email, DemandeUIModel> implements AttachmentModelAware, TabContentModel {

    private static final Log log = LogFactory.getLog(DemandeUIModel.class);

    public static final String PROPERTY_QUOTATION_NB = "quotationNb";
    public static final String PROPERTY_PF_NB = "pfNb";
    public static final String PROPERTY_SAV_NB = "savNb";
    public static final String PROPERTY_CLIENT_CODE = "clientCode";
    public static final String PROPERTY_CLIENT_NAME = "clientName";
    public static final String PROPERTY_CLIENT_BRAND = "clientBrand";
    public static final String PROPERTY_REFERENCE = "reference";
    public static final String PROPERTY_EDITABLE = "editable";
    public static final String PROPERTY_LAST_ATTACHMENT_OPENING_IN_THIS_FOLDER_USER = "lastAttachmentOpeningInThisFolderUser";
    public static final String PROPERTY_GROUPED_DEMANDES = "groupedDemandes";
    public static final String PROPERTY_VALID_RANGE_ROW_MODELS = "validRangeRowModels";

    protected final Email editObject = new EmailImpl();

    protected final List<Attachment> attachments = new ArrayList<>();

    protected final List<Reply> replies = new ArrayList<>();

    protected final List<DemandeUIModel> groupedDemandes = new ArrayList<>();

    protected final List<RangeRowModel> validRangeRowModels = new ArrayList<>();

    protected int quotationNb;

    protected int pfNb;

    protected int savNb;

    protected boolean editable = true;

    protected List<String> htmlContents;

    protected List<String> plainContents;

    protected String subject;

    protected List<String> toRecipients;

    protected List<String> ccRecipients;

    protected List<Client> allowedClients;

    protected boolean closeable;

    protected Boolean canViewOriginalAttachments;

    protected Boolean mailContentAsInlineAttachment;

    protected static final Binder<DemandeUIModel, Email> toBeanBinder =
            BinderFactory.newBinder(DemandeUIModel.class,
                                    Email.class);

    protected static final Binder<Email, DemandeUIModel> fromBeanBinder =
            BinderFactory.newBinder(Email.class, DemandeUIModel.class);

    protected static Binder<Attachment, Attachment> fromAttachmentBinder =
            BinderFactory.newBinder(Attachment.class);
    
    protected static Binder<Reply, Reply> fromReplyBinder = BinderFactory.newBinder(Reply.class);

    public DemandeUIModel() {
        super(fromBeanBinder, toBeanBinder);

        addPropertyChangeListener(Email.PROPERTY_RANGE_ROW, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Collection<RangeRow> rangeRows = (Collection<RangeRow>) evt.getNewValue();
                int quotationNb = 0;
                int pfNb = 0;
                int savNb = 0;
                if (rangeRows != null) {
                    for (RangeRow rangeRow : rangeRows) {
                        if (rangeRow != null) {
                            Integer quotationQuantity = rangeRow.getQuotationQuantity();
                            if (quotationQuantity != null) {
                                quotationNb += quotationQuantity;
                            }

                            Integer productQuantity = rangeRow.getProductQuantity();
                            if (productQuantity != null) {
                                pfNb += productQuantity;
                            }

                            Integer savQuantity = rangeRow.getSavQuantity();
                            if (savQuantity != null) {
                                savNb += savQuantity;
                            }
                        }
                    }
                }
                setQuotationNb(quotationNb);
                setPfNb(pfNb);
                setSavNb(savNb);
            }
        });

    }

    /**
     * Surcharge pour dupliquer correctement les pieces jointes sans charger le contenu binaire en mémoire.
     */
    @Override
    public void fromEntity(Email entity) {
        fromEntity(entity, false);
    }

    public void fromEntity(Email entity, boolean decomposeEmail) {
        fromBeanBinder.copyExcluding(entity, this,
                                     Email.PROPERTY_ATTACHMENT,
                                     Email.PROPERTY_REPLIES,
                                     Email.PROPERTY_EMAIL_GROUP,
                                     Email.PROPERTY_SUBJECT);
        loadAttachments(entity);
        setGroupedDemandes(entity.getEmailGroup());
        loadReplies(entity);
        this.subject = entity.getSubject();
        if (decomposeEmail) {
            decomposeEmail();
        }
    }

    public void fromEntityExcluding(Email entity, Set<String> properties) {
        // on ajoute les exclusions par defaut
        // si les exclusions étaient déjà exclue par l'appelant les boolean seront faux
        // et on a pas besoin de faire un chargement specifique de ces entités.
        boolean includeAttachment = properties.add(Email.PROPERTY_ATTACHMENT);
        boolean includeDemandGroup = properties.add(Email.PROPERTY_EMAIL_GROUP);
        boolean includeReply = properties.add(Email.PROPERTY_REPLIES);
        boolean includeSubject = properties.add(Email.PROPERTY_SUBJECT);
        fromBeanBinder.copyExcluding(entity, this, properties.toArray(new String[properties.size()]));
        if (includeAttachment) {
            loadAttachments(entity);
        }
        if (includeDemandGroup) {
            setGroupedDemandes(entity.getEmailGroup());
        }
        if (includeReply) {
            loadReplies(entity);
        }
        if (includeSubject) {
            this.subject = entity.getSubject();
        }
    }

    public void fromEntityIncluding(Email entity, Set<String> properties) {
        fromBeanBinder.copy(entity, this, properties.toArray(new String[properties.size()]));
        if (properties.isEmpty() || properties.contains(Email.PROPERTY_ATTACHMENT)) {
            loadAttachments(entity);
        }
        if (properties.isEmpty() || properties.contains(Email.PROPERTY_EMAIL_GROUP)) {
            setGroupedDemandes(entity.getEmailGroup());
        }
        if (properties.isEmpty() || properties.contains(Email.PROPERTY_REPLIES)) {
            loadReplies(entity);
        }
        if (properties.isEmpty() || properties.contains(Email.PROPERTY_SUBJECT)) {
            this.subject = entity.getSubject();
        }
    }

    protected void loadAttachments(Email entity) {
        // On ne copie pas entièrement les attachments pour ne pas force le chargement du contenu binaire du fichier en mémoire.
        List<Attachment> attachmentCopy = new ArrayList<>();
        if (entity.getAttachment() != null) {
            for (Attachment attachment : entity.getAttachment()) {
                Attachment clone = new AttachmentImpl();
                fromAttachmentBinder.copyExcluding(attachment, clone, Attachment.PROPERTY_EDITED_FILE, Attachment.PROPERTY_ORIGINAL_FILE, Attachment.PROPERTY_GENERATED_PDFPAGES);
                attachmentCopy.add(clone);
            }
        }
        setAttachment(attachmentCopy);
    }

    /**
     * On ne copie pas entièrement les réponses pour ne pas forcer le chargement du contenu binaire du fichier en mémoire.
     * 
     * @param entity entity
     */
    protected void loadReplies(Email entity) {
        
        List<Reply> replyCopy = new ArrayList<>();
        if (entity.getReplies() != null) {
            for (Reply reply : entity.getReplies()) {
                Reply clone = new ReplyImpl();
                fromReplyBinder.copyExcluding(reply, clone, Reply.PROPERTY_REPLY_CONTENT);
                replyCopy.add(clone);
            }
        }
        setReplies(replyCopy);
    }

    @Override
    public Email toEntity() {
        Email result = newEntity();
        toBeanBinder.copyExcluding(this, result, Email.PROPERTY_ATTACHMENT, Email.PROPERTY_ORIGINAL_EMAIL);
        return result;
    }

    @Override
    public Email toEntity(Email entity) {
        toBeanBinder.copyExcluding(this, entity, Email.PROPERTY_ATTACHMENT, Email.PROPERTY_ORIGINAL_EMAIL);
        return entity;
    }

    public String getTopiaId() {
        return editObject.getTopiaId();
    }

    public void setTopiaId(String id) {
        editObject.setTopiaId(id);
    }

    public void setSender(String sender) {
        Object oldValue = getSender();
        editObject.setSender(sender);
        firePropertyChanged(Email.PROPERTY_SENDER, oldValue, sender);
    }

    public String getSender() {
        String sender = editObject.getSender();
        if (sender == null) {
            sender = t("faxtomail.demande.sender.manualCreation");
        }
        return sender;
    }

    public void setRecipient(String recipient) {
        Object oldValue = getRecipient();
        editObject.setRecipient(recipient);
        firePropertyChanged(Email.PROPERTY_RECIPIENT, oldValue, recipient);
    }

    public String getRecipient() {
        return editObject.getRecipient();
    }

    public void setObject(String object) {
        Object oldValue = getObject();
        editObject.setObject(object);
        firePropertyChanged(Email.PROPERTY_OBJECT, oldValue, object);
    }

    public String getObject() {
        return editObject.getObject();
    }

    public String getSubject() {
//        if (subject == null) {
//            decomposeEmail();
//        }
        return subject;
    }

    public String getToRecipients() {
        if (toRecipients == null) {
            decomposeEmail();
        }
        return StringUtils.join(toRecipients, ";");
    }

    public String getCcRecipients() {
        if (ccRecipients == null) {
            decomposeEmail();
        }
        return StringUtils.join(ccRecipients, ";");
    }

    public List<String> getPlainContent() {
        if (plainContents == null) {
            decomposeEmail();
        }
        return plainContents;
    }

    public List<String> getHtmlContent() {
        if (htmlContents == null) {
            decomposeEmail();
        }
        return htmlContents;
    }

    public void setOriginalEmail(OriginalEmail originalEmail) {
        editObject.setOriginalEmail(originalEmail);
    }

    public OriginalEmail getOriginalEmail() {
        return editObject.getOriginalEmail();
    }

    public String getOriginalEmailContent() {
        OriginalEmail originalEmail = getOriginalEmail();
        return originalEmail != null ? originalEmail.getContent() : null;
    }

    public boolean isFax() {
        return editObject.isFax();
    }

    public void setFax(boolean fax) {
        Object oldValue = isFax();
        editObject.setFax(fax);
        firePropertyChanged(Email.PROPERTY_FAX, oldValue, fax);
    }

    public void setClient(Client client) {
        Object oldValue = getClient();
        editObject.setClient(client);
        firePropertyChanged(Email.PROPERTY_CLIENT, oldValue, client);
    }

    public Client getClient() {
        return editObject.getClient();
    }

    public String getClientCode() {
        Client client = getClient();
        return client != null ? client.getCode() : null;
    }

    public String getClientName() {
        Client client = getClient();
        return client != null ? client.getName() : null;
    }


    public String getClientBrand() {
        Client client = getClient();
        return client != null ? client.getBrand() : null;
    }

    public Collection<Client> getMatchingClients() {
        return editObject.getMatchingClients();
    }

    public void setMatchingClients(Collection<Client> matchingClients) {
        Object oldValue = getMatchingClients();
        editObject.setMatchingClients(matchingClients);
        firePropertyChanged(Email.PROPERTY_MATCHING_CLIENTS, oldValue, matchingClients);
    }

    public void setWaitingState(WaitingState waitingState) {
        Object oldValue = getWaitingState();
        editObject.setWaitingState(waitingState);
        firePropertyChanged(Email.PROPERTY_WAITING_STATE, oldValue, waitingState);
    }

    public WaitingState getWaitingState() {
        return editObject.getWaitingState();
    }

    public void setTakenBy(FaxToMailUser faxToMailUser) {
        Object oldValue = getTakenBy();
        editObject.setTakenBy(faxToMailUser);
        firePropertyChanged(Email.PROPERTY_TAKEN_BY, oldValue, faxToMailUser);
    }

    public FaxToMailUser getTakenBy() {
        return editObject.getTakenBy();
    }

    public Priority getPriority() {
        return editObject.getPriority();
    }

    public void setPriority(Priority priority) {
        Object oldValue = getPriority();
        editObject.setPriority(priority);
        firePropertyChanged(Email.PROPERTY_PRIORITY, oldValue, priority);
    }

    public DemandType getDemandType() {
        return editObject.getDemandType();
    }

    public void setDemandType(DemandType demandType) {
        Object oldValue = getDemandType();
        editObject.setDemandType(demandType);
        firePropertyChanged(Email.PROPERTY_DEMAND_TYPE, oldValue, demandType);
    }

    public DemandStatus getDemandStatus() {
        return editObject.getDemandStatus();
    }

    public void setDemandStatus(DemandStatus demandStatus) {
        Object oldValue = getDemandStatus();
        editObject.setDemandStatus(demandStatus);
        firePropertyChanged(Email.PROPERTY_DEMAND_STATUS, oldValue, demandStatus);
    }

    public void setReceptionDate(Date receptionDate) {
        Object oldValue = getReceptionDate();
        editObject.setReceptionDate(receptionDate);
        firePropertyChanged(Email.PROPERTY_RECEPTION_DATE, oldValue, receptionDate);
    }

    public Date getReceptionDate() {
        return editObject.getReceptionDate();
    }

    public void setEdiError(String ediCodeNumber) {
        Object oldValue = getEdiError();
        editObject.setEdiError(ediCodeNumber);
        firePropertyChanged(Email.PROPERTY_EDI_ERROR, oldValue, ediCodeNumber);
    }

    public String getEdiError() {
        return editObject.getEdiError();
    }

    public void setProjectReference(String projectReference) {
        Object oldValue = getProjectReference();
        editObject.setProjectReference(projectReference);
        firePropertyChanged(Email.PROPERTY_PROJECT_REFERENCE, oldValue, projectReference);
    }

    public String getProjectReference() {
        return editObject.getProjectReference();
    }

    public void setCompanyReference(String companyReference) {
        Object oldValue = getCompanyReference();
        Object refOldValue = getReference();
        editObject.setCompanyReference(companyReference);
        firePropertyChanged(Email.PROPERTY_COMPANY_REFERENCE, oldValue, companyReference);
        firePropertyChanged(PROPERTY_REFERENCE, refOldValue, getReference());
    }

    public String getCompanyReference() {
        return editObject.getCompanyReference();
    }

    public String getReference() {
        return editObject.getReference();
    }

    public void setHistory(Collection<History> history) {
        editObject.setHistory(history);
        firePropertyChanged(Email.PROPERTY_HISTORY, null, history);
    }

    public Collection<History> getHistory() {
        return editObject.getHistory();
    }

    public int sizeHistory() {
        return editObject.sizeHistory();
    }

    public FaxToMailUser getFirstOpeningUser() {
        return editObject.getFirstOpeningUser();
    }

    public Date getFirstOpeningDate() {
        return editObject.getFirstOpeningDate();
    }

    public FaxToMailUser getLastModificationUser() {
        return editObject.getLastModificationUser();
    }

    public Date getLastModificationDate() {
        return editObject.getLastModificationDate();
    }

    /**
     * Utilisé depuis la liste des mails seulement pour des raisons de performances sans charger
     * l'historique.
     * 
     * Contient la même information que {@code #lastAttachmentOpeningInThisFolderHistory} normalement.
     * 
     * @return lastAttachmentOpener
     */
    public FaxToMailUser getLastAttachmentOpener() {
        return editObject.getLastAttachmentOpener();
    }

    public void setLastAttachmentOpener(FaxToMailUser lastAttachmentOpener) {
        editObject.setLastAttachmentOpener(lastAttachmentOpener);
    }

    public FaxToMailUser getLastAttachmentOpeningInFolderUser() {
        return editObject.getLastAttachmentOpeningInFolderUser();
    }

    public Date getLastAttachmentOpeningInFolderDate() {
        return editObject.getLastAttachmentOpeningInFolderDate();
    }

    public FaxToMailUser getLastPrintingUser() {
        return editObject.getLastPrintingUser();
    }

    public void setLastPrintingUser(FaxToMailUser lastPrintingUser) {
        editObject.setLastPrintingUser(lastPrintingUser);
    }

    public Date getLastPrintingDate() {
        return editObject.getLastPrintingDate();
    }

    public void setLastPrintingDate(Date lastPrintingDate) {
        editObject.setLastPrintingDate(lastPrintingDate);
    }

    @Override
    public List<Attachment> getAttachment() {
        return new ArrayList<>(attachments);
    }

    @Override
    public void addAllAttachment(List<Attachment> attachment) {
        Object oldValue = new ArrayList<>(getAttachment());
        attachments.addAll(attachment);
        firePropertyChange(Email.PROPERTY_ATTACHMENT, oldValue, getAttachment());
    }

    @Override
    public void addAttachment(Attachment attachment) {
        Object oldValue = new ArrayList<>(getAttachment());
        attachments.add(attachment);
        firePropertyChange(Email.PROPERTY_ATTACHMENT, oldValue, getAttachment());
    }

    @Override
    public void removeAttachment(Attachment attachment) {
        Object oldValue = new ArrayList<>(getAttachment());
        attachments.remove(attachment);
        firePropertyChange(Email.PROPERTY_ATTACHMENT, oldValue, getAttachment());
    }

    @Override
    public boolean isCanViewOriginalAttachments() {
        if (canViewOriginalAttachments == null) {
            MailFolder folder = getMailFolder();
            while (folder.getParent() != null && folder.getCanViewOriginalAttachments() == null) {
                folder = folder.getParent();
            }
            canViewOriginalAttachments = folder.getCanViewOriginalAttachments();
        }
        return Boolean.TRUE.equals(canViewOriginalAttachments);
    }

    @Override
    public boolean isMailContentAsInlineAttachment() {
        if (mailContentAsInlineAttachment == null) {
            MailFolder folder = getMailFolder();
            while (folder.getParent() != null && folder.getMailContentWithInlineAttachments() == null) {
                folder = folder.getParent();
            }
            mailContentAsInlineAttachment = folder.getMailContentWithInlineAttachments();
        }
        return Boolean.TRUE.equals(mailContentAsInlineAttachment);
    }

    public void setAttachment(List<Attachment> attachment) {
        Object oldValue = new ArrayList<Attachment>(getAttachment());
        attachments.clear();
        if (attachment != null) {
            attachments.addAll(attachment);
        }
        firePropertyChange(Email.PROPERTY_ATTACHMENT, oldValue, getAttachment());
    }

    public List<RangeRow> getRangeRow() {
        return editObject.getRangeRow();
    }

    public void addRangeRow(RangeRow rangeRow) {
        Object oldValue = null;
        if (getRangeRow() != null) {
            oldValue = new ArrayList<RangeRow>(getRangeRow());
        }
        String refOldValue = getReference();
        editObject.addRangeRow(rangeRow);
        firePropertyChange(Email.PROPERTY_RANGE_ROW, oldValue, getRangeRow());
        firePropertyChanged(PROPERTY_REFERENCE, refOldValue, getReference());
    }

    public void addAllRangeRow(List<RangeRow> rangeRow) {
        Object oldValue = null;
        if (getRangeRow() != null) {
            oldValue = new ArrayList<RangeRow>(getRangeRow());
        }
        String refOldValue = getReference();
        editObject.addAllRangeRow(rangeRow);
        firePropertyChange(Email.PROPERTY_RANGE_ROW, oldValue, getRangeRow());
        firePropertyChanged(PROPERTY_REFERENCE, refOldValue, getReference());
    }

    public void removeRangeRow(RangeRow rangeRow) {
        Object oldValue = null;
        if (getRangeRow() != null) {
            oldValue = new ArrayList<RangeRow>(getRangeRow());
        }
        // on met à jour la référence, qui est la concaténation des numéros de commande et de "notre" référence
        String refOldValue = getReference();
        editObject.removeRangeRow(rangeRow);
        firePropertyChange(Email.PROPERTY_RANGE_ROW, oldValue, getRangeRow());
        firePropertyChanged(PROPERTY_REFERENCE, refOldValue, getReference());
    }

    public void setRangeRow(List<RangeRow> rangeRow) {
        ArrayList<RangeRow> oldValue = new ArrayList<RangeRow>();
        if (getRangeRow() != null) {
            oldValue.addAll(getRangeRow());
        }
        if (rangeRow == null) {
            rangeRow = new ArrayList<RangeRow>();
        }
        // on met à jour la référence, qui est la concaténation des numéros de commande et de "notre" référence
        String refOldValue = getReference();
        editObject.setRangeRow(rangeRow);
        firePropertyChange(Email.PROPERTY_RANGE_ROW, oldValue, getRangeRow());
        firePropertyChanged(PROPERTY_REFERENCE, refOldValue, getReference());
    }

    public MailFolder getMailFolder() {
        return editObject.getMailFolder();
    }

    public void setMailFolder(MailFolder mailFolder) {
        Object oldValue = getMailFolder();
        editObject.setMailFolder(mailFolder);
        //throw an lazy initialisation exception as soon as you can if the mailfolder is not initialized (cf #6345 and #6346)
        mailFolder.getTopiaId();
        firePropertyChanged(Email.PROPERTY_MAIL_FOLDER, oldValue, mailFolder);
    }

    public Date getArchiveDate() {
        return editObject.getArchiveDate();
    }

    public void setArchiveDate(Date archiveDate) {
        Object oldValue = getArchiveDate();
        editObject.setArchiveDate(archiveDate);
        firePropertyChanged(Email.PROPERTY_ARCHIVE_DATE, oldValue, archiveDate);
    }

    public int getQuotationNb() {
        return quotationNb;
    }

    public void setQuotationNb(int quotationNb) {
        Object oldValue = getQuotationNb();
        this.quotationNb = quotationNb;
        firePropertyChange(PROPERTY_QUOTATION_NB, oldValue, quotationNb);
    }

    public int getPfNb() {
        return pfNb;
    }

    public void setPfNb(int pfNb) {
        Object oldValue = getPfNb();
        this.pfNb = pfNb;
        firePropertyChange(PROPERTY_PF_NB, oldValue, pfNb);
    }

    public int getSavNb() {
        return savNb;
    }

    public void setSavNb(int savNb) {
        Object oldValue = getSavNb();
        this.savNb = savNb;
        firePropertyChange(PROPERTY_SAV_NB, oldValue, savNb);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        Object oldValue = isEditable();
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, oldValue, editable);
    }

    public void setComment(String comment) {
        Object oldValue = getComment();
        editObject.setComment(comment);
        firePropertyChange(Email.PROPERTY_COMMENT, oldValue, comment);
    }

    public String getComment() {
        return editObject.getComment();
    }

    public void setGroupedDemandes(EmailGroup emailGroup) {
        Object oldValue = new ArrayList<DemandeUIModel>(getGroupedDemandes());
        groupedDemandes.clear();
        if (emailGroup != null) {
            Collection<Email> emails = emailGroup.getEmail();
            if (emails != null) {
                for (Email email : emails) {
                    DemandeUIModel demandeUIModel = new DemandeUIModel();
                    demandeUIModel.setObject(email.getObject());
                    demandeUIModel.setCompanyReference(email.getCompanyReference());
                    demandeUIModel.setTopiaId(email.getTopiaId());
                    demandeUIModel.setMailFolder(email.getMailFolder());
                    demandeUIModel.setDemandStatus(email.getDemandStatus());
                    demandeUIModel.loadAttachments(email);
                    groupedDemandes.add(demandeUIModel);
                }
            }
        } else {
            groupedDemandes.add(this);
        }
        firePropertyChange(PROPERTY_GROUPED_DEMANDES, oldValue, getGroupedDemandes());
    }

    public void addGroupedDemande(DemandeUIModel demand) {
        Object oldValue = new ArrayList<DemandeUIModel>(getGroupedDemandes());
        groupedDemandes.add(demand);
        firePropertyChange(PROPERTY_GROUPED_DEMANDES, oldValue, getGroupedDemandes());

    }

    public List<DemandeUIModel> getGroupedDemandes() {
        return groupedDemandes;
    }

    public int sizeGroupedDemandes() {
        return groupedDemandes.size();
    }

    public void addAllReplies(List<Reply> replies) {
        Object oldValue = new ArrayList<Reply>(getReplies());
        editObject.addAllReplies(replies);
        firePropertyChange(Email.PROPERTY_REPLIES, oldValue, getAttachment());
    }

    public void addReplies(Reply reply) {
        editObject.addReplies(reply);
        firePropertyChange(Email.PROPERTY_REPLIES, null, getReplies());
    }

    public void removeReply(Reply reply) {
        Object oldValue = new ArrayList<Reply>(getReplies());
        editObject.removeReplies(reply);
        firePropertyChange(Email.PROPERTY_REPLIES, oldValue, getReplies());
    }

    public void setReplies(List<Reply> replies) {
        editObject.setReplies(replies);
        firePropertyChange(Email.PROPERTY_REPLIES, null, getReplies());
    }

    public List<Reply> getReplies() {
        return editObject.getReplies();
    }

    public int sizeReplies() {
        return editObject.sizeReplies();
    }


    public List<RangeRowModel> getValidRangeRowModels() {
        return validRangeRowModels;
    }

    public void setValidRangeRowModels(List<RangeRowModel> validRangeRowModels) {
        Object oldValue = new ArrayList<RangeRowModel>(validRangeRowModels);
        this.validRangeRowModels.clear();
        this.validRangeRowModels.addAll(validRangeRowModels);
        firePropertyChanged(PROPERTY_VALID_RANGE_ROW_MODELS, oldValue, validRangeRowModels);
    }

    public void addValidRangeRow(RangeRowModel row) {
        Object oldValue = new ArrayList<RangeRowModel>(validRangeRowModels);
        if (!validRangeRowModels.contains(row)) {
            validRangeRowModels.add(row);
        }
        firePropertyChanged(PROPERTY_VALID_RANGE_ROW_MODELS, oldValue, validRangeRowModels);
    }

    public void removeValidRangeRow(RangeRowModel row) {
        Object oldValue = new ArrayList<RangeRowModel>(validRangeRowModels);
        validRangeRowModels.remove(row);
        firePropertyChanged(PROPERTY_VALID_RANGE_ROW_MODELS, oldValue, validRangeRowModels);
    }

    public void recomputeValidRangeRows() {
        ArrayList<RangeRowModel> validRangeRowModels = new ArrayList<RangeRowModel>();
        Collection<RangeRow> rangeRows = getRangeRow();
        if (rangeRows != null) {
            for (RangeRow rangeRow : rangeRows) {
                if (rangeRow != null) {
                    RangeRowModel rangeRowModel = new RangeRowModel();
                    rangeRowModel.fromEntity(rangeRow);
                    if (rangeRowModel.isValid()) {
                        validRangeRowModels.add(rangeRowModel);
                    }
                }
            }
        }
        setValidRangeRowModels(validRangeRowModels);
    }

    public List<Client> getAllowedClients() {
        return allowedClients;
    }

    public void setAllowedClients(List<Client> allowedClients) {
        this.allowedClients = allowedClients;
    }

    /**
     * Appelée par la validation.
     * 
     * Voir le fichier src/main/resources/com/franciaflex/faxtomail/ui/swing/content/demande/DemandeUIModel-error-validation.xml
     * 
     * @param field field to validate
     * @return validity
     */
    public boolean isValid(String field) {
        DemandType demandType = getDemandType();
        boolean result = demandType == null;
        if (!result) {
            if (Email.PROPERTY_CLIENT.equals(field)) {
                result = !FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.CLIENT)
                            || getClient() != null;

            } else if (PROPERTY_VALID_RANGE_ROW_MODELS.equals(field)) {
                result = !FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.RANGE_ROW)
                            || CollectionUtils.isNotEmpty(getValidRangeRowModels());

            } else if (Email.PROPERTY_PROJECT_REFERENCE.equals(field)) {
                result = !FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.PROJECT_REFERENCE)
                            || StringUtils.isNotBlank(getProjectReference());

            } else if (Email.PROPERTY_OBJECT.equals(field)) {
                result = !FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.OBJECT)
                            || StringUtils.isNotBlank(getObject());

            } else if (Email.PROPERTY_COMMENT.equals(field)) {
                result = !FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.COMMENT)
                        || StringUtils.isNotBlank(getComment());

            } else if (Email.PROPERTY_COMPANY_REFERENCE.equals(field)) {
                result = !FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.COMPANY_REFERENCE)
                        || StringUtils.isNotBlank(getCompanyReference());

            } else if (Email.PROPERTY_PRIORITY.equals(field)) {
                result = !FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.PRIORITY)
                        || getPriority() != null;

            } else if (Email.PROPERTY_WAITING_STATE.equals(field)) {
                result = !FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.WAITING_STATE)
                        || getWaitingState() != null;
            }
        }
        return result;
    }

    @Override
    protected Email newEntity() {
        return new EmailImpl();
    }

    @Override
    public int hashCode() {
        return editObject.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !obj.getClass().isAssignableFrom(DemandeUIModel.class)) {
            return false;
        }
        DemandeUIModel other = (DemandeUIModel) obj;
        return editObject.equals(other.editObject);
    }

    protected void decomposeEmail() {
        if (t("faxtomail.demande.sender.manualCreation").equals(getSender())) {
            plainContents = Lists.newArrayList(getOriginalEmailContent());
            toRecipients = Lists.newArrayList(getRecipient());

        } else {
            try {
                // ce code peut provoquer une NPE avec les données de test

                Properties properties = new Properties();
                // set the mail.mime.address.strict to false to avoid
                // javax.mail.internet.AddressException: Domain contains illegal character errors when recipients contains []
                properties.setProperty("mail.mime.address.strict", "false");

                Session session = Session.getInstance(properties);
                MimeMessage message = MimeMessageUtils.createMimeMessage(session, getOriginalEmailContent());

                if (subject == null && message.getSubject() != null) {
                    subject = FaxToMailServiceUtils.getDecodedSubject(message.getSubject());
                }

                toRecipients = FaxToMailServiceUtils.convertAddressesToStrings(message.getRecipients(Message.RecipientType.TO));
                ccRecipients = FaxToMailServiceUtils.convertAddressesToStrings(message.getRecipients(Message.RecipientType.CC));

                plainContents = null;
                htmlContents = null;
                if (message.isMimeType("multipart/*")) {
                    decomposeMultipartEmail(message);

                } else if (message.isMimeType("text/html")) {
                    String content = FaxToMailServiceUtils.getTextFromMessage(message);
                    if (htmlContents == null) {
                        htmlContents = new ArrayList<>();
                    }
                    htmlContents.add(JAXXUtil.getStringValue(content));
                } else if (message.isMimeType("text/*")) {
                    String content = FaxToMailServiceUtils.getTextFromMessage(message);
                    if (plainContents == null) {
                        plainContents = new ArrayList<>();
                    }
                    plainContents.add(JAXXUtil.getStringValue(content));
                }

            } catch (Exception e) {
                if (log.isWarnEnabled()) {
                    log.warn("error while parsing the original email content, may come from the imported archives", e);
                }
                //may comes from the imported archives
                plainContents = Lists.newArrayList(getOriginalEmailContent());
                toRecipients = Lists.newArrayList(getRecipient());
            }
        }
    }

    /**
     * Decompose a multipart part.
     * - sets the email content if the part contains a text bodypart
     * - adds attachments to the email
     *
     * @param part the part to decompose
     * @throws Exception
     */
    protected void decomposeMultipartEmail(Part part) throws Exception {
        DataSource dataSource = part.getDataHandler().getDataSource();
        MimeMultipart mimeMultipart = new MimeMultipart(dataSource);
        int multiPartCount = mimeMultipart.getCount();

        for (int j = 0; j < multiPartCount; j++) {
            BodyPart bp = mimeMultipart.getBodyPart(j);

            // if it is a text part, then this is the email content
            String disposition = bp.getDisposition();
            if (bp.isMimeType("text/*") && !Part.ATTACHMENT.equals(disposition)) {
                String content = FaxToMailServiceUtils.getTextFromPart(bp);

                if (bp.isMimeType("text/plain")) {
                    if (plainContents == null) {
                        plainContents = new ArrayList<String>();
                    }
                    plainContents.add(JAXXUtil.getStringValue(content));

                } else {
                    if (htmlContents == null) {
                        htmlContents = new ArrayList<String>();
                    }

                    htmlContents.add(content);

                }

            // if it is multipart part, decompose it
            } else if (bp.isMimeType("multipart/*")) {
                decomposeMultipartEmail(bp);
            }

        }
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public String getTitle() {
        return editObject.getTitle();
    }

    @Override
    public String getIcon() {
        return "email";
    }

    @Override
    public boolean isCloseable() {
        return closeable;
    }

    public void setCloseable(boolean closeable) {
        this.closeable = closeable;
    }

    public void clearPDFPages(){
        for (Attachment attachment:attachments){
            attachment.clearGeneratedPDFPages();
        }
    }
}
