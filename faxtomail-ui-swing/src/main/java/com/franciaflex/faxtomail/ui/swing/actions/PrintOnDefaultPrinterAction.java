package com.franciaflex.faxtomail.ui.swing.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.HistoryType;
import com.franciaflex.faxtomail.persistence.entities.MailAction;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.util.AbstractFaxToMailUIHandler;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class PrintOnDefaultPrinterAction extends AbstractFaxToMailAction {

    protected Multimap<DemandeUIModel, AttachmentFile> attachmentsToPrintByDemand;
    protected boolean take;
    protected boolean takeOnlyIfNotTaken;
    protected boolean printDemandDetails;
    protected List<DemandeUIModel> notPrintableDemands;

    public PrintOnDefaultPrinterAction(AbstractFaxToMailUIHandler handler,
                                       Multimap<DemandeUIModel, AttachmentFile> attachmentsToPrintByDemand,
                                       boolean take,
                                       boolean takeOnlyIfNotTaken,
                                       boolean printDemandDetails) {

        super(handler, false);
        this.attachmentsToPrintByDemand = attachmentsToPrintByDemand;
        this.take = take;
        this.takeOnlyIfNotTaken = takeOnlyIfNotTaken;
        this.printDemandDetails = printDemandDetails;

        setActionDescription(t("faxtomail.action.print.tip"));
    }

    @Override
    public void doAction() throws Exception {
        FaxToMailUser currentUser = getContext().getCurrentUser();
        final AbstractFaxToMailUIHandler handler = (AbstractFaxToMailUIHandler) getHandler();

        int printedFileNb = 0;
        Multimap<DemandeUIModel, AttachmentFile> nonPrintedAttachment = HashMultimap.create();
        notPrintableDemands = new ArrayList<>();

        List<DemandeUIModel> orderedDemands = new ArrayList<>(attachmentsToPrintByDemand.keySet());
        // order the demands by reception date
        Collections.sort(orderedDemands, new Comparator<DemandeUIModel>() {

            @Override
            public int compare(DemandeUIModel o1, DemandeUIModel o2) {
                return o1.getReceptionDate().compareTo(o2.getReceptionDate());
            }
        });

        try (FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            EmailService emailService = serviceContext.getEmailService();

            List<File> filesToPrint = Lists.newArrayList();

            for (DemandeUIModel demandeUIModel : orderedDemands) {

                if (!handler.isActionEnabled(demandeUIModel, MailAction.PRINT)) {
                    notPrintableDemands.add(demandeUIModel);

                } else {
                    if (take && (demandeUIModel.getTakenBy() == null || !takeOnlyIfNotTaken)) {
                        demandeUIModel.setTakenBy(currentUser);
                        Email persistedEmail = emailService.getFullEmailById(demandeUIModel.getTopiaId());
                        Email email = demandeUIModel.toEntity(persistedEmail);
                        emailService.saveEmail(email, currentUser);
                    }

                    List<String> printedFiles = new ArrayList<String>();

                    if (printDemandDetails) {
                        // generate details as pdf
                        Email email = demandeUIModel.toEntity();
                        final AttachmentFile demandDetailAttachment = emailService.getEmailDetailAsAttachment(email);
                        // print details
                        filesToPrint.add(FaxToMailUIUtil.preparePrint(demandDetailAttachment));
                        printedFiles.add(demandDetailAttachment.getFilename());
                    }

                    for (AttachmentFile attachmentFile : attachmentsToPrintByDemand.get(demandeUIModel)) {
                        if (attachmentFile != null) {
                            if (FaxToMailUIUtil.isFileTypeEditable(attachmentFile.getFilename())) {
                                filesToPrint.add(FaxToMailUIUtil.preparePrint(attachmentFile));
                                printedFiles.add(attachmentFile.getFilename());
                            } else {
                                nonPrintedAttachment.put(demandeUIModel, attachmentFile);
                            }
                        }
                    }

                    Email email = emailService.addToHistory(demandeUIModel.getTopiaId(),
                            HistoryType.PRINTING,
                            getContext().getCurrentUser(),
                            new Date(),
                            printedFiles.toArray(new String[printedFiles.size()]));
                    printedFileNb += printedFiles.size();
                    demandeUIModel.setHistory(email.getHistory());
                    demandeUIModel.setLastPrintingUser(email.getLastPrintingUser());
                    demandeUIModel.setLastPrintingDate(email.getLastPrintingDate());
                }
            }

            // print another page for non printed attachments
            if (!nonPrintedAttachment.isEmpty()) {
                String errorPageContent = generateErrorPageStream(nonPrintedAttachment);
                filesToPrint.add(FaxToMailUIUtil.preparePrintText(errorPageContent));
            }

            FaxToMailUIUtil.printWithPdfRenderer(t("faxtomail.print.jobName"), filesToPrint, true, getContext().getConfig(), new Function<Integer, Void>() {
                @Override
                public Void apply(Integer input) {
                    handler.showInformationMessage(t("faxtomail.print.success.message", input));
                    return null;
                }
            });
        }
        handler.showInformationMessage(t("faxtomail.print.inProgress.message"));

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        if (!notPrintableDemands.isEmpty()) {
            StringBuilder notPrintableDemandTitles = new StringBuilder();
            for (DemandeUIModel demandeUIModel : notPrintableDemands) {
                notPrintableDemandTitles.append("- ").append(demandeUIModel.getTitle()).append("<br/>");
            }

            displayWarningMessage(t("faxtomail.action.print.notPrintableDemands.title"),
                                  t("faxtomail.action.print.notPrintableDemands.message",
                                    notPrintableDemandTitles.toString()));
        }
    }

    /**
     * Retourne un flux de données qui correspond à une page de récapitulatif des pièces jointes
     * non imprimées.
     *
     * @param nonPrintedAttachment not printed attachment list
     * @return input stream with content
     */
    protected String generateErrorPageStream(Multimap<DemandeUIModel, AttachmentFile> nonPrintedAttachment) {

        // generate text content
        StringBuilder builder = new StringBuilder();
        builder.append("\nLes pièces-jointes suivantes n'ont pas pu être imprimées:\n");

        for (DemandeUIModel email : nonPrintedAttachment.keySet()) {
            Collection<AttachmentFile> attachments = nonPrintedAttachment.get(email);

            builder.append(" * ");
            if (!Strings.isNullOrEmpty(email.getCompanyReference())) {
                builder.append(email.getCompanyReference() + " - ");
            }
            builder.append(Strings.nullToEmpty(email.getObject()) + " : \n");
            for (AttachmentFile attachmentFile : attachments) {
                builder.append("    - " + attachmentFile.getFilename() + "\n");
            }
        }
        return builder.toString();
    }
}
