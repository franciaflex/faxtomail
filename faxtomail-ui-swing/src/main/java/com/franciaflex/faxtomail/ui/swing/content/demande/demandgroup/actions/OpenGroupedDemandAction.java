package com.franciaflex.faxtomail.ui.swing.content.demande.demandgroup.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.services.service.exceptions.AlreadyLockedMailException;
import com.franciaflex.faxtomail.services.service.exceptions.FolderNotReadableException;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandesUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.demandgroup.DemandGroupUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.demandgroup.DemandGroupUIHandler;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import javax.swing.*;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class OpenGroupedDemandAction extends AbstractFaxToMailAction<DemandeUIModel, DemandGroupUI, DemandGroupUIHandler> {

    protected DemandeUIModel demandToOpen;
    protected boolean takeEmail;

    public OpenGroupedDemandAction(DemandGroupUIHandler handler, DemandeUIModel demandToOpen) {
        super(handler, false);
        this.demandToOpen = demandToOpen;
        setActionDescription(t("faxtomail.action.goto.demand.tip"));
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        JFrame currentFrame = getContext().getFrameForDemande(getModel());
        DemandesUI parentContainer = (DemandesUI) currentFrame.getContentPane();
        if (parentContainer.getModel().containsDemand(demandToOpen)) {
            openDemandScreen();
            result = false;

        } else {
            String topiaId = demandToOpen.getTopiaId();
            if (StringUtils.isNotBlank(topiaId)) {
                FaxToMailUser currentUser = getContext().getCurrentUser();

                try (FaxToMailServiceContext serviceContext = getContext().newServiceContext()){
                    EmailService emailService = serviceContext.getEmailService();

                    // tentative de verrouillages des email existants
                    Email email = emailService.lockEmail(topiaId, currentUser);

                    FaxToMailUser takenBy = email.getTakenBy();

                    // si le mail est pris par quelqu'un d'autre, le prend-on ou pas ?
                    if (takenBy != null && !currentUser.equals(takenBy)) {

                        MailFolder folderWithMustTakeToEditDemand = email.getMailFolder();
                        while (folderWithMustTakeToEditDemand.getParent() != null
                               && folderWithMustTakeToEditDemand.getMustTakeToEditDemand() == null) {
                            folderWithMustTakeToEditDemand = folderWithMustTakeToEditDemand.getParent();
                        }
                        if (BooleanUtils.isTrue(folderWithMustTakeToEditDemand.getMustTakeToEditDemand())) {
                            String htmlMessage = String.format(
                                    AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                                    t("faxtomail.alert.alreadyTakenBy.message", decorate(takenBy)),
                                    t("faxtomail.alert.alreadyTakenBy.help"));

                            int answer = JOptionPane.showConfirmDialog(getHandler().getTopestUI(),
                                                                       htmlMessage,
                                                                       t("faxtomail.alert.alreadyTakenBy.title"),
                                                                       JOptionPane.YES_NO_CANCEL_OPTION,
                                                                       JOptionPane.QUESTION_MESSAGE);

                            // si on annule, on délock
                            if (answer == JOptionPane.CANCEL_OPTION) {
                                result = false;
                                emailService.unlockEmail(topiaId);

                            } else {
                                // sinon on ouvre

                                // si on ne prend pas, le mail est en readonly
                                if (answer == JOptionPane.NO_OPTION) {
                                    demandToOpen.setEditable(false);
                                    takeEmail = false;
                                }
                                // si on prend, on prend et on rend editable
                                else if (answer == JOptionPane.YES_OPTION) {
                                    takeEmail = true;
                                    demandToOpen.setEditable(true);
                                }
                            }

                        } else {
                            takeEmail = false;
                            JOptionPane.showConfirmDialog(getHandler().getTopestUI(),
                                                          t("faxtomail.alert.alreadyTakenBy.message", decorate(takenBy)),
                                                          t("faxtomail.alert.alreadyTakenBy.title"),
                                                          JOptionPane.DEFAULT_OPTION,
                                                          JOptionPane.WARNING_MESSAGE);
                        }
                    }
                }
                // le mail est locké, on ne fait rien
                catch (AlreadyLockedMailException ex) {
                    result = false;

                    String htmlMessage = t("faxtomail.alert.alreadyLockedBy.message", decorate(ex.getLockedBy()));
                    JOptionPane.showMessageDialog(getHandler().getTopestUI(),
                                                  htmlMessage,
                                                  t("faxtomail.alert.alreadyLockedBy.title"),
                                                  JOptionPane.ERROR_MESSAGE);
                }
                // l'utilisateur n'a pas les droits de lecture sur le dossier
                catch (FolderNotReadableException e) {
                    result = false;

                    String htmlMessage = t("faxtomail.alert.userNotAuthorizedToReadEmail.message", e.getForbiddenFolder().getName());
                    JOptionPane.showMessageDialog(getHandler().getTopestUI(),
                                                  htmlMessage,
                                                  t("faxtomail.alert.userNotAuthorizedToReadEmail.title"),
                                                  JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        return result;
    }


    @Override
    public void doAction() throws Exception {
        MailFolder folder = demandToOpen.getMailFolder();

        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {

            if (StringUtils.isNotBlank(demandToOpen.getTopiaId())) {

                Email email = serviceContext.getEmailService().openEmail(demandToOpen.getTopiaId(),
                        getContext().getCurrentUser(),
                        takeEmail);

                demandToOpen.fromEntity(email, true);
                demandToOpen.recomputeValidRangeRows();

                if (demandToOpen.getArchiveDate() != null
                        || !folder.isFolderWritable()
                        || !demandToOpen.getDemandStatus().isEditableStatus()) {
                    demandToOpen.setEditable(false);
                }
                demandToOpen.setCloseable(true);

            }

            List<Client> allowedClients = serviceContext.getClientService().getClientsForFolder(folder);
            demandToOpen.setAllowedClients(allowedClients);
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        openDemandScreen();
    }

    protected void openDemandScreen() {
        JFrame currentFrame = getContext().getFrameForDemande(getModel());
        DemandesUI parentContainer = (DemandesUI) currentFrame.getContentPane();
        parentContainer.getModel().addDemand(demandToOpen);
    }
}
