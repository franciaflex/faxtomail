package com.franciaflex.faxtomail.ui.swing.content.demande.replies.actions;

/*
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.Reply;
import com.franciaflex.faxtomail.persistence.entities.ReplyContent;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.franciaflex.faxtomail.ui.swing.FaxToMailUIContext;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.demande.DemandeUIModel;
import com.franciaflex.faxtomail.ui.swing.content.demande.replies.DemandRepliesUI;
import com.franciaflex.faxtomail.ui.swing.content.demande.replies.DemandRepliesUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.demande.replies.DemandReplyItem;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUI;
import com.franciaflex.faxtomail.ui.swing.content.reply.ReplyFormUIModel;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.util.MimeMessageUtils;

import javax.mail.Message;
import javax.mail.internet.MimeMessage;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 1.1
 */
public class OpenReplyAction extends AbstractFaxToMailAction<DemandeUIModel, DemandRepliesUI, DemandRepliesUIHandler> {

    protected DemandReplyItem item;
    protected ReplyFormUI frameContent;
    protected JFrame frame;

    public OpenReplyAction(DemandRepliesUIHandler handler) {
        super(handler, false);
    }

    public void setItem(DemandReplyItem item) {
        this.item = item;
    }

    @Override
    public void doAction() throws Exception {

        DemandeUIModel demand = getModel();
        try(FaxToMailServiceContext serviceContext = getContext().newServiceContext()) {
            Email email = serviceContext.getEmailService().getFullEmailById(demand.getTopiaId(), getContext().getCurrentUser());
            demand.fromEntity(email);

            Reply reply = item.getReply();
            FaxToMailUIUtil.forceReplyContentLoading(getContext(), reply);
            boolean forward = item.isForward();

            frameContent = new ReplyFormUI(getUI());
            // TODO echatellier 20140804 : c'est très technique et très bas niveau, ca devrait se trouver dans les services
            ReplyContent replyContent = reply.getReplyContent();
            MimeMessage message = MimeMessageUtils.createMimeMessage(null, replyContent.getSource());
            ReplyFormUIModel replyModel = frameContent.getModel();
            replyModel.setEditable(demand.isEditable());
            replyModel.setForward(forward);
            replyModel.setOriginalDemand(demand);
            replyModel.setReadonly(true);
            replyModel.setReadSentDate(reply.getSentDate());
            replyModel.setSubject(message.getSubject());
            replyModel.setTo(StringUtils.join(message.getRecipients(Message.RecipientType.TO), ";"));
            if (email.isFax()) {
                replyModel.setFrom(email.getMailFolder().getFaxFromNumber());
            } else {
                replyModel.setFrom(message.getFrom()[0].toString());
            }
            if (ArrayUtils.isNotEmpty(message.getRecipients(Message.RecipientType.CC))) {
                replyModel.setCc(StringUtils.join(message.getRecipients(Message.RecipientType.CC), ";"));
            }
            if (ArrayUtils.isNotEmpty(message.getRecipients(Message.RecipientType.BCC))) {
                replyModel.setCci(StringUtils.join(message.getRecipients(Message.RecipientType.BCC), ";"));
            }

            if (message.isMimeType("multipart/*")) {
                handler.decomposeMultipartEmail(message, replyModel);

            } else {
                String content = FaxToMailServiceUtils.getTextFromMessage(message);
                replyModel.setMessage(content);
            }
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        frame = getHandler().openModalFrame(frameContent,
                                            t("faxtomail.reply.title", getModel().getTitle()),
                                            new Dimension(800, 600));

        getContext().addPropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (!getContext().isBusy()) {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            if (frame != null) {
                                frame.toFront();
                                frame = null;
                            }
                        }
                    });
                    getContext().removePropertyChangeListener(FaxToMailUIContext.PROPERTY_BUSY, this);
                }
            }
        });
    }

}
