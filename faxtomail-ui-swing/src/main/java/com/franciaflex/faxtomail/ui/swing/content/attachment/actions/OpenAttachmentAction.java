package com.franciaflex.faxtomail.ui.swing.content.attachment.actions;

/*
 * #%L
 * FaxToMail :: UI
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.ui.swing.actions.AbstractFaxToMailAction;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentEditorUI;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentEditorUIHandler;
import com.franciaflex.faxtomail.ui.swing.content.attachment.AttachmentEditorUIModel;
import com.franciaflex.faxtomail.ui.swing.util.FaxToMailUIUtil;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class OpenAttachmentAction extends AbstractFaxToMailAction<AttachmentEditorUIModel, AttachmentEditorUI, AttachmentEditorUIHandler> {

    protected Attachment attachment;
    protected boolean original;

    public OpenAttachmentAction(AttachmentEditorUIHandler handler) {
        super(handler, false);
    }

    public void setAttachment(Attachment attachment, boolean original) {
        this.attachment = attachment;
        this.original = original;
        String attachmentName = original ? attachment.getOriginalFileName() : attachment.getEditedFileName();
        setActionDescription(t("faxtomail.action.attachment.open.tip", attachmentName));
    }

    @Override
    public void doAction() throws Exception {
        getModel().fireAttachmentOpened(attachment, original);
        FaxToMailUIUtil.forceAttachmentFileLoading(getContext(), attachment);
        AttachmentFile attachmentFile = original ? attachment.getOriginalFile() : attachment.getEditedFile();
        FaxToMailUIUtil.openFile(getContext(), attachmentFile);
    }
}
