#!/bin/bash

###
# #%L
# FaxToMail :: UI
# %%
# Copyright (C) 2014 Mac-Groupe, Code Lutin
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###

MEMORY="-Xmx1024M -XX:MaxPermSize=128M"
#FAXTOMAIL_JVM_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=8000"

REP=$(dirname $0)

cd $REP

java $MEMORY $FAXTOMAIL_JVM_OPTS -Dfaxtomail.log.file=$FAXTOMAIL_LOG_FILE -jar ${project.build.finalName}.${project.packaging} $*
exitcode=$?
echo "Stop FaxToMail with exitcode: $exitcode"
exit $exitcode
