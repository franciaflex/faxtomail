@echo off

set OLDDIR=%CD%
cd /d %~dp0%

java -Xmx1024M -XX:MaxPermSize=128M -Dfaxtomail.log.file=%FAXTOMAIL_LOG_FILE% -jar ${project.build.finalName}.${project.packaging} %1 %2 %3 %4 %5 %6 %7 %8 %9
set exitcode=%ERRORLEVEL%
echo Stop FaxToMail with exitcode: %exitcode%
cd %OLDDIR%
exit /b %exitcode%
