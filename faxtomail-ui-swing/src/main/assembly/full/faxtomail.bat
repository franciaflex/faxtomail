@echo off

set OLDDIR=%CD%
cd /d %~dp0%

set FAXTOMAIL_BASEDIR="%CD%"
set FAXTOMAIL_HOME=%FAXTOMAIL_BASEDIR%\faxtomail
set JAVA_HOME=%FAXTOMAIL_BASEDIR%\jre
set JAVA_COMMAND=%JAVA_HOME%\bin\java
set FAXTOMAIL_LOG_FILE=%FAXTOMAIL_BASEDIR%\data\faxtomail-${project.version}.log

echo FaxToMail basedir:  %FAXTOMAIL_BASEDIR%
echo FaxToMail app home: %FAXTOMAIL_HOME%
echo FaxToMail jre home: %JAVA_HOME%
echo FaxToMail log file: %FAXTOMAIL_LOG_FILE%

:start

if exist "faxtomail.config" copy faxtomail.config faxtomail
echo FaxToMail ${project.version} is starting...
call faxtomail\launch.bat --option faxtomail.launch.mode full --option faxtomail.basedir %FAXTOMAIL_BASEDIR%
if errorlevel 88 goto start

goto quit

:quit
cd %OLDDIR%
