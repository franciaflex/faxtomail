#!/bin/bash

###
# #%L
# FaxToMail :: UI
# %%
# Copyright (C) 2014 Mac-Groupe, Code Lutin
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###

export FAXTOMAIL_BASEDIR=$(pwd)
export FAXTOMAIL_HOME=$FAXTOMAIL_BASEDIR/faxtomail
export JAVA_HOME=$FAXTOMAIL_BASEDIR/jre
export JAVA_COMMAND=$JAVA_HOME/bin/java
export FAXTOMAIL_LOG_FILE=$FAXTOMAIL_BASEDIR/data/faxtomail-${project.version}.log

cd $FAXTOMAIL_BASEDIR

echo "FaxToMail basedir:  $FAXTOMAIL_BASEDIR"
echo "FaxToMail app home: $FAXTOMAIL_HOME"
echo "FaxToMail jre home: $JAVA_HOME"
echo "FaxToMail log file: $FAXTOMAIL_LOG_FILE"

while true; do

  if [ -f $FAXTOMAIL_BASEDIR/faxtomail.config ]; then
    cp -rfv $FAXTOMAIL_BASEDIR/faxtomail.config $FAXTOMAIL_HOME
  fi
  echo "FaxToMail ${project.version} is starting..."
  ./faxtomail/launch.sh --option faxtomail.launch.mode full --option faxtomail.basedir $FAXTOMAIL_BASEDIR
  exitcode=$?

  if [ ! "$exitcode" -eq  "88" ]; then
    # quit now!
    exit $exitcode
  fi
done
