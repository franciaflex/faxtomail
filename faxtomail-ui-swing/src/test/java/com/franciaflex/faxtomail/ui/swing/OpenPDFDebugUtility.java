package com.franciaflex.faxtomail.ui.swing;

/*-
 * #%L
 * FaxToMail :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2018 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.junit.Test;


import java.io.File;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import java.awt.image.BufferedImage;

public class OpenPDFDebugUtility {

    /*@Test
    public void test9623(){

        testOpenPDF("test9623");
    }*/

    /** Méthode permettant de tester les ouvertures de PDF (en l'occurence la première page)
     *
     * @param pdfId nom du pdf à tester (sans extension). Le PDF doit être présent dans les ressources
     *              (src/test/resources/pdf/pdfId.pdf)
     */
    protected void testOpenPDF(String pdfId) {
        try {
            File file = new File("src/test/resources/pdf/" + pdfId + ".pdf");

            PDDocument pdDocument = PDDocument.load(file);
            PDFRenderer renderer = new PDFRenderer(pdDocument);

            BufferedImage image = renderer.renderImage(0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
