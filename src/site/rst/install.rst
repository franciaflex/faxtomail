.. -
.. * #%L
.. * FaxToMail
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2014 Mac-Groupe, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Installation du serveur FaxToMail (Windows)
===========================================

Téléchargement
--------------

* Installer Java JRE 7 (minimum) : http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html
* Télécharger Tomcat 7 : http://apache.crihan.fr/dist/tomcat/tomcat-7/v7.0.53/bin/apache-tomcat-7.0.53.exe
* Télécharger Apache 2.4 : http://www.apachelounge.com/download/VC11/binaries/httpd-2.4.9-win64-VC11.zip

* Installer Java
* Installer tomcat. Tomcat s'installe automatiquement en tant que service windows.
* Décompresser httpd-2.4.9-win64-VC11.zip dans ''C:\Apache24''
* Installer apache en tant que service windows : ''httpd.exe -k install'':

  * Si un message à propos de msvcr110.dll s'affiche, c'est que "Microsoft Visual C++ Redistributable" n'est pas installé
  * http://answers.microsoft.com/en-us/windows/forum/windows_8-winapps/the-program-cant-start-because-msvcr110dll-is/f052d325-3af9-4ae5-990b-b080799724db
  * http://www.microsoft.com/en-us/download/details.aspx?id=30679
* Démarrer les services "Apache tomcat 7.0 Tomcat7" et "Apache2.4"

À ce stade, les adresses http://localhost/ et http://localhost:8080 doivent répondre.

Apache
------

Étapes:

* Dans le fichier 'C:\Apache24\conf\httpd.conf', décommenter les lignes

::

  LoadModule proxy_module modules/mod_proxy.so
  LoadModule proxy_ajp_module modules/mod_proxy_ajp.so

* Ajouter à la fin du fichier

::

  Include conf/extra/httpd-faxtomail.conf

* Créer le fichier ''conf/extra/httpd-faxtomail.conf'' avec en contenu

::

  <VirtualHost *:80>
    ServerAdmin webmaster@franciaflex.com
    ServerName 192.168.100.247
    #ServerAlias faxtomailsrv
    ErrorLog "logs/faxtomail-error.log" 
    CustomLog "logs/faxtomail-access.log" common
  
    # lien vers tomcat
    ProxyRequests On
    ProxyVia On
    ProxyPass / ajp://localhost:8009/
    ProxyPassReverse / ajp://localhost:8009/
  </VirtualHost>

À ce stade, l'adresse http://192.168.100.247/ devrait répondre par tomcat.

Tomcat
------

* Télécharger le war depuis l'adresse : http://forge.codelutin.com/projects/faxtomail/files
* Enregistrer le dans le dossier ''C:\Program Files\Apache Software Foundation\Tomcat 7.0\webapps\'' avec le nom ''ROOT.war'' (supprimer le dossier ROOT)
* Créer le fichier ''C:\Windows\System32\faxToMail.properties'' avec en contenu

::

  faxtomail.storage=C:\\faxtomail\\data

* Relancer le service ''Apache Tomcat 7.0 Tomcat7''

L'application doit maintenant être installée et répondre sur l'adresse : http://192.168.100.247/
