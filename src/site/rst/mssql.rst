.. -
.. * #%L
.. * FaxToMail
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2014 Mac-Groupe, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Déploiement sur MS-SQL Server
=============================

Le serveur testé a été ::

  Microsoft SQL Server 2008 (SP3) - 10.0.5500.0 (X64) 
  Sep 21 2011 22:45:45 
  Copyright (c) 1988-2008 Microsoft Corporation
  Standard Edition (64-bit) on Windows NT 6.1 <X64> (Build 7601: Service Pack 1) (VM)

La configuration à ajouter dans le fichier de configuration ``C:\Windows\System32\faxToMail.properties``::

  hibernate.dialect=org.hibernate.dialect.SQLServer2008Dialect
  hibernate.connection.driver_class=net.sourceforge.jtds.jdbc.Driver
  hibernate.connection.url=jdbc:jtds:sqlserver://192.168.100.247:1433/faxtomail
  hibernate.connection.username=user
  hibernate.connection.password=pass
