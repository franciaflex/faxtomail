.. -
.. * #%L
.. * FaxToMail
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2014 Mac-Groupe, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Import par fichiers CSV
======================

Format
------

Les fichiers d'import doivent être formatés suivant le standard CSV (Comma-separated values) et
utiliser le séparateur ``;``.

Exemple avec le fichier d'import des États d'attente::

  etatattente
  etat 1
  etat 2
  etat 3

Ce fichier ne comporte qu'une seule colonne ``etatattente``, les séparateurs ``;`` ne sont donc pas requis.

Voici un exemple plus complet avec le fichier client::

  Id_Correspondance;Societe;Nom;Caracteristique1;Caracteristique2;Caracteristique3;Numero_Fax;Adresse_Mail;Code_Client
  253042;FX;John Doe;A;71 Allo;NULL;03 12 34 56 78;john@doe.fr;99153
  253142;FX;Gerard Menvussa;A+;40 Nabi;NULL;05 12 23 34 45;gerard.menvussa@code.com;99154
  253242;FX;Ella Elli;NULL;38 LA;NULL;04 98 87 76 65;NULL;99184

Ce fichier comporte 9 colonnes séparées par le séparateur ``;``. Il comporte une ligne d'entête et
3 lignes de données.


Encodage
--------

Il est très difficile de déterminer le format d'encodage du fichier texte (CSV). L'application FaxToMail
impose donc l'utilisation du format d'encodage standardisé unicode (UTF-8).

Cependant, certains systèmes d'exploitation, comme Windows, n'utilisent pas ce standard.

Il faut donc spécifier lors de l'enregistrement du fichier:
  * l'encodage des caractères à utiliser
  * le format du fichier (séparateur ``;``, délimiteur...)

notepad++
~~~~~~~~~

Pour éditer un fichier au format texte, le logiciel `notepad++`_ est plus évolué que les outils
basiques mis à la disposition par défaut par Windows. Il dispose, de plus, d'une option pour spécifier
le format d'encodage lors de la sauvegarde.

.. _notepad++: http://notepad-plus-plus.org/

.. image:: img/notepad.png

Openoffice/Libreoffice
~~~~~~~~~~~~~~~~~~~~~~

Openoffice et Libreoffice sont deux equivalents libres de la suite bureautique Microsoft Office. Ce
logiciel propose plus facilement les options d'encodage lors de l'enregistrement du fichier CSV.

Il s'agit d'une simple option à sélection dans la fenêtre de sauvegarde.

Excel
~~~~~

Pour spécifier l'encodage sous Microsoft Excel, c'est un peu compliqué car l'option n'est pas
facilement accessible.

Pour pouvoir changer cette option, il faut:
  * ouvrir la boite de dialogue "Enregistrer sous..."
  * Choisir de sauver le document en type "Tous les fichiers (*.*)
  * modifier le nom du fichier en xxx.csv

comme montré dans cette image:

.. image:: http://www.ablebits.com/_img-blog/excel-csv/txt-to-csv.png
