<%--
  #%L
  FaxToMail :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@ taglib prefix="s"         uri="/struts-tags" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<html>
    <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>FaxToMail : <decorator:title default="FaxToMail"/></title>
            <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/faxtomail.css' />" />
            <script type="text/javascript" src="<s:url value='/nuiton-js/faxtomail.js' />"></script>
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
              <script type="text/javascript" src="<s:url value='/nuiton-js/faxtomail-ie8.js' />"></script>
            <![endif]-->
            <decorator:head/>
    </head>

    <body>
        <div id="wrap-global">
            <div id="wrap-main" class="<decorator:getProperty property="page.wide-display"/>">
                <div id="faxtomail-body">

                    <!-- navbar -->
                    <header class="navbar navbar-inverse navbar-static-top" role="banner">
                      <div class="container-fluid">
                        <div class="navbar-header">
                          <a href="<s:url value='/' />" class="navbar-brand">FaxToMail</a>
                        </div>
                        <nav class="collapse navbar-collapse" role="navigation">
                          <ul class="nav navbar-nav">
                            <s:if test="authenticated">
                              <s:if test="admin">
                                <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-wrench"></span> Administration <span class="caret"></span></a>
                                  <ul class="dropdown-menu" role="menu">
                                    <li><a href="<s:url action='ldap-input' namespace="/admin" />">
                                      <span class="fa fa-database"></span> Active Directory</a></li>
                                    <li><a href="<s:url action='configuration-input' namespace="/admin" />">
                                      <span class="fa fa-cogs"></span> Configuration</a></li>
                                    <li><a href="<s:url action='import-input' namespace="/admin" />">
                                      <span class="fa fa-file-excel-o"></span> Import</a></li>
                                    <li><a href="<s:url action='lock-input' namespace="/admin" />">
                                      <span class="fa fa-unlock-alt"></span> Verrouillages</a></li>
                                    <li><a href="<s:url action='import-archive-input' namespace="/admin" />">
                                      <span class="fa fa-archive"></span> Reprise des archives</a></li>
                                  </ul>
                                </li>
                              </s:if>

                              <li><a href="<s:url action='user-folder-input' namespace="/admin" />">
                                <span class="fa fa-folder-open"></span> Dossiers utilisateur</a></li>

                            </s:if>

                            <li><a href="<s:url action='search-input' namespace="/" />">
                              <span class="fa fa-search"></span> Recherche archive</a></li>

                          </ul>

                          <ul class="nav navbar-nav navbar-right">
                            <s:if test="authenticated">
                              <li><a><span class="fa fa-user"></span>
                                <s:property value="authenticatedUser.firstName" /> <s:property value="authenticatedUser.lastName" /></a></li>
                              <li><a href="<s:url action='logout' namespace="/" />">
                                <span class="fa fa-power-off"></span> Déconnexion</a></li>
                             </s:if>
                          </ul>
                        </nav>
                      </div>
                    </header>

                    <decorator:body/>
                </div>
            </div>
        </div>
    </body>
</html>
