<%--
  #%L
  FaxToMail :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
  <head>
    <title>Import</title>
  </head>

  <body>

    <div id="main-container" class="container">

      <h1 class="page-header">Import</h1>

      <s:form id="main_form" action="import" method="post" enctype="multipart/form-data">

        <s:actionmessage/>
        <s:actionerror/>

        <div class="form-group">
          <label for="clientField" class="control-label">Fichier client&nbsp;:</label>
          <input type="file" id="clientField" name="clientFile" class="form-control">
          <p class="help-block">Format du fichier csv : <code>Societe;Marque;Nom;Caracteristique1;Caracteristique2;Caracteristique3;Numero_Fax;Adresse_Mail;Code_Client</code><br />
          Encodage du fichier csv : <code>${applicationConfig.importFileEncoding}</code></p>
        </div>
        <button type="submit" class="btn btn-primary navbar-btn">Valider</button>
        <hr />

        <div class="form-group">
          <label for="waitingStateField" class="control-label">Fichier état d'attente&nbsp;:</label>
          <input type="file" id="waitingStateField" name="waitingStateFile" class="form-control">
          <p class="help-block">Format du fichier csv : <code>etatattente</code><br />
          Encodage du fichier csv : <code>${applicationConfig.importFileEncoding}</code></p>
        </div>
        <button type="submit" class="btn btn-primary navbar-btn">Valider</button>
        <hr />

        <div class="form-group">
          <label for="demandTypeField" class="control-label">Fichier types de demande&nbsp;:</label>
          <input type="file" id="demandTypeField" name="demandTypeFile" class="form-control">
          <p class="help-block">Format du fichier csv : <code>demandetype</code><br />
          Encodage du fichier csv : <code>${applicationConfig.importFileEncoding}</code></p>
        </div>
        <button type="submit" class="btn btn-primary navbar-btn">Valider</button>
        <hr />

        <div class="form-group">
          <label for="rangeField" class="control-label">Fichier gamme&nbsp;:</label>
          <input type="file" id="rangeField" name="rangeFile" class="form-control">
          <p class="help-block">Format du fichier csv : <code>gamme</code><br />
          Encodage du fichier csv : <code>${applicationConfig.importFileEncoding}</code></p>
        </div>
        <button type="submit" class="btn btn-primary navbar-btn">Valider</button>
        <hr />

        <div class="form-group">
          <label for="priorityField" class="control-label">Fichier priorite&nbsp;:</label>
          <input type="file" id="priorityField" name="priorityFile" class="form-control">
          <p class="help-block">Format du fichier csv : <code>priorite</code><br />
          Encodage du fichier csv : <code>${applicationConfig.importFileEncoding}</code></p>
        </div>
        <button type="submit" class="btn btn-primary navbar-btn">Valider</button>

      </s:form>
    </div>
  </body>
</html>
