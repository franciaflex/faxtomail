<%--
  #%L
  FaxToMail :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
  <head>
    <title>Reprise des archives</title>
  </head>

  <body>

    <div id="main-container" class="container">

      <h1 class="page-header">Reprise des archives</h1>

      <s:form id="main_form" action="import-archive" method="post" enctype="multipart/form-data">

        <s:actionmessage/>

        <s:if test="importResult != null">
          <div class="alert alert-success">
            <s:property value="importResult.nbImportedArchives"/> archives ont été importées.
          </div>

          <s:if test="importResult.getNbImportErrors() > 0">
            <s:url var="downloadErrorFile" value="download-import-archive-errorfile.action">
              <s:param name="fileName" value="%{importResult.errorFile.name}" />
            </s:url>
            <div class="alert alert-danger">
              <s:property value="importResult.nbImportErrors"/> archives n'ont pas pu été importées.
              Pour télécharger le fichier contenant les archives non importées,
              <s:a href="%{downloadErrorFile}" title="Télécharger le fichier contenant les archives non importées">
                cliquez ici
              </s:a>.
            </div>
          </s:if>
        </s:if>

        <s:actionerror/>

        <div class="form-group">
          <label for="archiveFile" class="control-label">Fichier d'archive&nbsp;:</label>
          <input type="file" id="archiveFile" name="archiveFile" class="form-control" required>
          <p class="help-block">Format du fichier csv : <code>receptionDate;projectReference;sender;fax;recipient;object;archiveDate;companyReference;originalEmail;comment;etatAttente;demandType;priority;mailFolder;client-code;client-brand;attachments</code><br />
          Encodage du fichier csv : <code>${applicationConfig.importFileEncoding}</code></p>
        </div>
        <div class="form-group">
          <label for="attachmentBase" class="control-label">Répertoire de base des pièces jointes&nbsp;:</label>
          <input type="text" id="attachmentBase" name="attachmentBase" class="form-control">
          <p class="help-block">Dossier contenant les pièces jointes référencées dans le fichier d'archive</p>
        </div>
        <button type="submit" class="btn btn-primary navbar-btn">Valider</button>
        <hr />

      </s:form>
    </div>
  </body>
</html>
