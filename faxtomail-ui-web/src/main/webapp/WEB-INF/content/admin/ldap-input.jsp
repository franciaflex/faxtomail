<%--
  #%L
  FaxToMail :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
  <head>
    <title>Ldap</title>
  </head>

  <body>

    <div id="main-container" class="container">

      <h1 class="page-header">Active Directory</h1>

      La base de données contient actuellement :
      <ul>
        <li><strong>${userCount}</strong> utilisateurs
        <li><strong>${groupCount}</strong> groupes
      </ul>

      <hr />

      <s:form id="main_form" action="ldap" method="post">

        Cette action permet d'importer tout les utilisateurs et les groupes du serveur Active Directory dans l'application FaxToMail. Cette action peut prendre quelques minutes.

        <button id="submit_button" type="submit" class="btn btn-primary navbar-btn">Mettre à jour les groupes et utilisateurs</button>

      </s:form>
    </div>
  </body>
</html>
