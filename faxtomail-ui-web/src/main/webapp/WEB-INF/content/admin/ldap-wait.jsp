<%--
  #%L
  FaxToMail :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
  <head>
    <title>Ldap</title>
    <meta http-equiv="refresh" content="5;url=<s:url />"/>
  </head>

  <body>

    <div id="main-container" class="container">

      <h1 class="page-header">Active Directory</h1>

      <em>L'import des utilisateurs et groupes Active Directory est en cours...</em>

    </div>
  </body>
</html>
