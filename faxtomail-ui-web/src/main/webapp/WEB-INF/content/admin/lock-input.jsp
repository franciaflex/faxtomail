<%--
  #%L
  FaxToMail :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
  <head>
    <title>Verrouillages</title>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/faxtomail-lock.css' />" />
  </head>

  <body>

    <div id="main-container" class="container">

      <h1 class="page-header">Liste des verrous actifs sur les demandes</h1>

      <s:form id="main_form" action="lock" method="post">

        <table id='table-snapshot' class="table table-bordered">
          <thead>
            <tr>
              <th></th>
              <th>Objet</th>
              <th>Notre référence</th>
              <th>Émetteur</th>
              <th>Verrouillé par</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator value="activeLocks">
              <tr>
                <td><input type="checkbox" name="mailLockIds" value="<s:property value="topiaId" />" />
                <td><s:property value="lockOn.object" /></td>
                <td><s:property value="lockOn.reference" /></td>
                <td><s:property value="lockOn.sender" /></td>
                <td>
                  <s:property value="lockBy.firstName" /> <s:property value="lockBy.lastName" />
                </td>
              </tr>
            </s:iterator>
            <s:if test="activeLocks == null || activeLocks.empty">
              <tr class="emptyTable">
                <td colspan="5">Aucune demande n'est actuellement verrouillée.</td>
              </tr>
            </s:if>
          </tbody>
        </table>

        <nav class="navbar navbar-default navbar-fixed-bottom">
          <div class="container">
            <button type="submit" class="btn btn-primary navbar-btn pull-right">Supprimer les verrous</button>
          </div>
        </nav>
      </s:form>
    </div>
  </body>
</html>
