<%--
  #%L
  FaxToMail :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
  <head>
    <title>Dossiers mis en avant par utilisateur</title>
    
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/faxtomail-user-folder.css' />" />
    <script type="text/javascript" src="<s:url value='/nuiton-js/faxtomail-user-folder.js' />"></script>

    <script type="text/javascript">
      UserFolderModule.value('UserFolderData', {
          'mailFolders': <s:property value="toJson(mailFolders)" escapeHtml="false"/>,
          'users': <s:property value="toJson(users)" escapeHtml="false"/>,
          'userFolders': <s:property value="toJson(userFolders)" escapeHtml="false"/>
        });
    </script>

    <!--
    Current user groups : 
    <s:iterator value="authenticatedUser.userGroups">
      - <s:property value="completeName" />
    </s:iterator>
    -->
  </head>

  <body>

    <div id="main-container" class="container" ng-app="UserFolderModule">

      <h1 class="page-header">Dossiers mis en avant par utilisateur</h1>

      <s:form id="main_form" action="user-folder" method="post" ng-controller="UserFolderController">
        <s:hidden name="userFoldersJson" value="{{userFolders}}" />

        Voici la liste des utilisateurs dont vous avez la gestion car vous faites partie d'un groupe
        capable de gérer les utilisateurs suivants&nbsp;:

        <table id='table-snapshot' class="table table-bordered">
          <thead>
            <tr>
              <th>Utilisateur</th>
              <th>Dossiers</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="user in users">
              <th>{{user.firstName}} {{user.lastName}}</th>
              <td>
                <div class="controls">
                  <input type="hidden" ui-select2-sortable ng-model="userFolders[user.topiaId]" simple-query="getObjectsData" multiple sortable>
                </div>
              </td>
            </tr>
            <tr ng-if="!users || users.length == 0">
              <td class="emptyTable" colspan="2">Aucun utilisateur trouvé</td>
            </tr>
          </tbody>
        </table>

        <nav class="navbar navbar-default navbar-fixed-bottom">
          <div class="container">
            <button type="submit" class="btn btn-primary navbar-btn pull-right">Valider</button>
          </div>
        </nav>
      </s:form>
    </div>
  </body>
</html>
