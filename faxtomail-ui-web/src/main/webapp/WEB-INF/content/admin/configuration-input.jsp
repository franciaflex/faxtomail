<%--
  #%L
  FaxToMail :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
  <head>
    <title>Configuration</title>

    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/faxtomail-configuration.css' />" />
    <script type="text/javascript" src="<s:url value='/nuiton-js/faxtomail-configuration.js' />"></script>

    <script type="text/javascript">
      ConfigurationModule.value('ConfigurationData', {
          // enums
          'mailActions': <s:property value="toJson(mailActions)" escapeHtml="false"/>,
          'canBeRequiredMailFields': <s:property value="toJson(canBeRequiredMailFields)" escapeHtml="false"/>,
          'tableMailFields': <s:property value="toJson(tableMailFields)" escapeHtml="false"/>,
          'emailProtocolPorts': <s:property value="toJson(emailProtocolPorts)" escapeHtml="false"/>,
          // datas
          'configuration': <s:property value="toJson(configuration)" escapeHtml="false"/>,
          'waitingStates': <s:property value="toJson(waitingStates)" escapeHtml="false"/>,
          'waitingStatesUsage': <s:property value="toJson(waitingStatesUsage)" escapeHtml="false"/>,
          'demandTypes': <s:property value="toJson(demandTypes)" escapeHtml="false"/>,
          'stamps': <s:property value="toJson(stamps)" escapeHtml="false"/>,
          'ranges': <s:property value="toJson(ranges)" escapeHtml="false"/>,
          'mailFolders': <s:property value="toJson(mailFolders)" escapeHtml="false"/>,
          'mailFoldersUsage': <s:property value="toJson(mailFoldersUsage)" escapeHtml="false"/>,
          'searchDisplayColumns': <s:property value="toJson(configuration.searchDisplayColumns)" escapeHtml="false"/>,
          'mailFilters': <s:property value="toJson(mailFilters)" escapeHtml="false"/>,
          'emailAccounts': <s:property value="toJson(emailAccounts)" escapeHtml="false"/>,
          'brandsForDomains': <s:property value="toJson(brandsForDomains)" escapeHtml="false"/>,
          'signings': <s:property value="toJson(signingForDomains)" escapeHtml="false"/>,
          // referentiels
          'users': <s:property value="toJson(users)" escapeHtml="false"/>,
          'groups': <s:property value="toJson(groups)" escapeHtml="false"/>,
          // remote service urls
          'remoteCheckFolder': "<s:url action="configuration-check-directory-json" />",
          'remoteCheckEmailAccount': "<s:url action="configuration-check-mailaccount-json" />",
          'remoteSearchClients': "<s:url action="search-clients-json" />"
      });
    </script>
  </head>

  <body>

    <div id="main-container" class="container" ng-app="ConfigurationModule">

      <h1 class="page-header">Configuration</h1>

      <s:form id="main_form" action="configuration" method="post" ng-controller="ConfigurationController"  enctype="multipart/form-data">

        <!--<s:hidden name="activeTab"/>-->

        <ul id="tabs" class="nav nav-tabs">
          <li class="active"><a href="#tabs-general" data-toggle="tab">Général</a></li>
          <li><a href="#tabs-waitingState" data-toggle="tab">États d'attente</a></li>
          <li><a href="#tabs-demandType" data-toggle="tab">Types de demandes</a></li>
          <li><a href="#tabs-stamps" data-toggle="tab">Tampons</a></li>
          <li><a href="#tabs-tree" data-toggle="tab">Arborescence</a></li>
          <li><a href="#tabs-filters" data-toggle="tab">Filtres de mail</a></li>
          <li><a href="#tabs-search" data-toggle="tab">Recherche</a></li>
          <li><a href="#tabs-mailaccounts" data-toggle="tab">Comptes mail</a></li>
          <li><a href="#tabs-brandsForDomain" data-toggle="tab">Noms de domaine / Marques</a></li>
          <li><a href="#tabs-signings" data-toggle="tab">Signatures</a></li>
          <li><a href="#tabs-chefgroup" data-toggle="tab">Chef de groupe</a></li>
        </ul>

        <div class="tab-content active" ng-controller="ConfigurationMiscController">
          <input type="hidden" name="configurationJson" value="{{configuration}}" />

          <div id="tabs-general" class="tab-pane active">
            <!-- Général -->
            <div class="form-group">
              <label class="control-label" for="configurationEmailMaxSizeField">Taille maximale des réponses (en octets)</label>
              <input type="text" id="configurationEmailMaxSizeField" class="form-control" ng-model="configuration.emailMaxSize" fm-integer />
            </div>

            <table class="table table-bordered">
              <caption>Commande de traitement des extensions de pièces jointes</caption>
              <thead>
                <tr>
                  <th>Extension</th>
                  <th>Commandes</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="extensionCommand in configuration.extensionCommands">
                  <td class="col-xs-2">
                    {{extensionCommand.extension}}
                    <a class="pull-right btn btn-danger btn-xs " ng-click="deleteExtensionRow($index)" title="Supprimer l'extension">
                        <span class="glyphicon glyphicon-remove"></span></a>
                  </td>
                  <td>
                    <div class="form-group">
                      <label for="convertToToPdfField">Conversion en pdf :</label>
                      <input id="convertToToPdfField" type="text" class="form-control" ng-model="extensionCommand.convertToPdfCommand" placeholder="" />
                      <p class="help-block">Utilisez <code>%f</code> et <code>%o</code> pour representer respectivement le fichier en entrée et en sortie de la commande</p>
                    </div>
                    <div class="form-group">
                      <label for="openAttachmentCommandField">Ouverture :</label>
                      <input id="openAttachmentCommandField" type="text" class="form-control" ng-model="extensionCommand.openAttachmentCommand" placeholder="" />
                      <p class="help-block">Utilisez <code>%f</code> pour representer le fichier en entrée de la commande</p>
                    </div>
                  </td>
                </tr>
                <tr ng-if="!configuration.extensionCommands || configuration.extensionCommands.length == 0">
                  <td colspan="2" class="emptyTable">Aucune extension définie</td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2">
                    <a class="pull-right btn btn-success btn-xs" ng-click="addExtensionRow()" >
                      <span class="glyphicon glyphicon-plus"></span> Ajouter une ligne</a>
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>

          <div id="tabs-waitingState" class="tab-pane" ng-controller="ConfigurationWaitingStateController">
            <input type="hidden" name="waitingStatesJson" value="{{waitingStates}}" />

            <div class="row">
              <div class="col-md-4">
                <h3>États d'attente <input type="button" class="btn btn-success btn-xs pull-right" value="Nouveau" ng-click="newWaitingState()" /></h3>
                <table class="table table-hover">
                  <tbody>
                    <tr ng-repeat="waitingState in waitingStates"
                        ng-class="{'info' : waitingState == selectedWaitingState}"
                        ng-click="editWaitingState(waitingState)">
                      <td>{{waitingState.label}}
                      
                        <a class="pull-right btn btn-danger btn-xs" data-nodrag ng-click="deleteWaitingState(waitingState, $index)"
                          ng-disabled="waitingStatesUsage[waitingState.topiaId] > 0"
                          tooltip="{{waitingStatesUsage[waitingState.topiaId] > 0 && 'Cet état d\'attente est utilisé et ne peut pas être supprimé'|| 'Supprimer cet état d\'attente'}}">
                          <span class="glyphicon glyphicon-remove"></span></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="col-md-8" ng-if="selectedWaitingState">
                <h3>Édition de l'état d'attente {{selectedWaitingState.label}}</h3>
                Actions autorisées lorsque la demande est <strong>valide</strong> :
                <div class="checkbox" ng-repeat="(mailAction,label) in mailActions">
                  <label>
                    <input type="checkbox" ng-checked="selectedWaitingState.validFormDisabledActions.indexOf(mailAction) == -1"
                      ng-click="changeWaitingStateValidAction(mailAction)"> {{label}}
                  </label>
                </div>
                
                Actions autorisées lorsque la demande est <strong>invalide</strong> :
                <div class="checkbox" ng-repeat="(mailAction,label) in mailActions">
                  <label>
                    <input type="checkbox" ng-checked="selectedWaitingState.invalidFormDisabledActions.indexOf(mailAction) == -1"
                      ng-click="changeWaitingStateInvalidAction(mailAction)"> {{label}}
                  </label>
                </div>
              </div>
              <div class="col-md-8" ng-if="!selectedWaitingState">
                <em>Sélectionnez un état d'attente.</em>
              </div>
            </div>

          </div>

          <div id="tabs-demandType" class="tab-pane" ng-controller="ConfigurationDemandTypeController">
            <input type="hidden" name="demandTypesJson" value="{{demandTypes}}" />

            <div class="row">
              <div class="col-md-4">
                <h3>Type de demande <input type="button" class="btn btn-success btn-xs pull-right" value="Nouveau" ng-click="newDemandType()" /></h3>
                <table class="table table-hover">
                  <tbody>
                    <tr ng-repeat="demandType in demandTypes"
                        ng-class="{'info' : demandType == selectedDemandType}"
                        ng-click="editDemandType(demandType)">
                      <td>{{demandType.label}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="col-md-8" ng-if="selectedDemandType">
                <h3>Édition du type de demande {{selectedDemandType.label}}</h3>
                Champs obligatoires pour le type de demande :
                <div class="checkbox" ng-repeat="(mailField,label) in canBeRequiredMailFields">
                  <label>
                    <input type="checkbox" ng-checked="selectedDemandType.requiredFields.indexOf(mailField) != -1"
                      ng-click="changeDemandTypeField(mailField)"> {{label}}
                  </label>
                  <span ng-show="mailField == 'RANGE_ROW'">(<label>
                    <input type="checkbox" ng-model="selectedDemandType.ediTransfer"
                        ng-disabled="selectedDemandType.requiredFields.indexOf(mailField) == -1"/> Autoriser le transfert EDI
                        <span class="fa fa-info-circle" tooltip="Si cette case est cochée, les demandes valides seront transférées à l'EDI pour le traitement des numéros de commande des gammes, sinon les demandes passerons directement dans le statut 'en cours'. Cette configuration n'est utilisée que si la société autorise également le transfert EDI."></span>
                  </label>)</span>
                </div>
              </div>
              <div class="col-md-8" ng-if="!selectedDemandType">
                <em>Sélectionnez un type de demande.</em>
              </div>
            </div>

          </div>

          <div id="tabs-stamps" class="tab-pane" ng-controller="ConfigurationStampsController">
            <input type="hidden" name="stampsJson" value="{{stamps}}" />

            <div class="row">
              <div class="col-md-4">
                <h3>Tampons <input type="button" class="btn btn-success btn-xs pull-right" value="Nouveau" ng-click="newStamp()" /></h3>
                <table class="table table-hover">
                  <tbody>
                    <tr ng-repeat="stamp in stamps"
                        ng-class="{'info' : stamp == selectedStamp}"
                        ng-click="editStamp(stamp)">
                      <td>
                        {{stamp.label}}
                        <a class="pull-right btn btn-danger btn-xs" data-nodrag ng-click="deleteStamp(stamp, $index)"
                            tooltip="Supprimer ce tampon">
                            <span class="glyphicon glyphicon-remove"></span></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="col-md-8" ng-show="selectedStamp">

                <h3>Édition du tampon {{selectedStamp.label}}</h3>

                <div class="form-group">
                  <label>Description :</label>
                  <textarea name="description" class="form-control" ng-model="selectedStamp.description"></textarea>
                </div>

                <div class="form-group">
                  <label><input type="radio" ng-model="selectedStamp.isImageType" ng-value="true"/>
                  Image :</label>

                  <div ng-show="selectedStamp.isImageType">

                    <input type="file" accept="image/*" id="stampImage"
                           class="form-control"
                           onchange="angular.element(this).scope().imageChanged(this)"/>

                    <output>
                      <img id="stampPreview" alt="Stamp preview"/>
                    </output>

                  </div>

                </div>

                <div class="form-group">
                  <label><input type="radio" ng-model="selectedStamp.isImageType" ng-value="false" /> Texte :</label>
                  <textarea name="text" class="form-control"
                            ng-model="selectedStamp.text" ng-show="!selectedStamp.isImageType">
                  </textarea>
                </div>

                <div class="form-group">
                  <label>Droits :</label>

                  <table id="rights" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Utilisateur ou groupe</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="group in selectedStamp.groups">
                        <td>
                          <span class="fa fa-users"></span> {{group.completeName}}
                          <a class="pull-right btn btn-danger btn-xs pull-right" ng-click="removeStampGroup($index, group)">
                            <span class="glyphicon glyphicon-remove"></span>
                          </a>
                        </td>
                      </tr>
                      <tr ng-repeat="user in selectedStamp.users">
                        <td>
                          <span class="fa fa-user"></span> {{user.firstName}} {{user.lastName}}
                          <a class="pull-right btn btn-danger btn-xs pull-right" ng-click="removeStampUser($index, user)">
                            <span class="glyphicon glyphicon-remove"></span>
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>

                  <div class="form">
                    <label for="newStampUserField" class="control-label">Nouvel utilisateur :</label>
                    <div class="form-group">
                      <a class="btn btn-success btn-xs pull-right" ng-click="addStampUser()" ng-disabled="!newStampUser">
                        <span class="glyphicon glyphicon-plus"></span>
                      </a>
                      <ui-select id="newStampUserField" ng-model="newStampUser" theme="bootstrap"
                                       on-select="onSelectStampUserCallback($item, $model)">
                        <ui-select-match>{{$select.selected.firstName}} {{$select.selected.lastName}}</ui-select-match>
                        <ui-select-choices repeat="user in users | filter:filterByAlreadyInCollection(selectedStamp.users) | filter: $select.search">
                          <div ng-bind-html="user.firstName + ' ' + user.lastName | highlight: $select.search"></div>
                        </ui-select-choices>
                      </ui-select>
                    </div>
                  </div>

                  <div class="form">
                    <label for="newStampGroupField" class="control-label">Nouveau groupe :</label>
                    <div class="form-group">
                      <a class="btn btn-success btn-xs pull-right" ng-click="addStampGroup()" ng-disabled="!newStampGroup">
                        <span class="glyphicon glyphicon-plus"></span>
                      </a>
                      <ui-select id="newStampGroupField" ng-model="newStampGroup" theme="bootstrap"
                                       on-select="onSelectStampGroupCallback($item, $model)">
                        <ui-select-match>{{$select.selected.completeName}}</ui-select-match>
                        <ui-select-choices repeat="group in groups | filter:filterByAlreadyInCollection(selectedStamp.groups) | filter: $select.search">
                          <div ng-bind-html="group.completeName | highlight: $select.search"></div>
                        </ui-select-choices>
                      </ui-select>
                    </div>
                  </div>

                </div>
              </div>

              <div class="col-md-8" ng-show="!selectedStamp">
                <em>Sélectionnez un tampon.</em>
              </div>
            </div>

          </div>
          
          <div id="tabs-tree" class="tab-pane" ng-controller="ConfigurationTreeController as ctrl">
            <input type="hidden" name="mailFoldersJson" value="{{mailFolders}}" />

            <div class="row">
              <div class="col-md-4">
                <h3>Dossiers <a class="btn btn-xs btn-success pull-right" ng-click="newRoot()">Nouveau</a></h3>
                <script type="text/ng-template" id="nodes_renderer.html">
                    <div ui-tree-handle ng-class="{'bg-success' : mailFolder == selectedMailFolder}">
                      <a class="btn btn-success btn-xs" data-nodrag ng-click="toggle(this)" ng-disabled="!mailFolder.children || mailFolder.children.length == 0">
                        <span class="glyphicon" ng-class="{'glyphicon-chevron-right': !collapsed, 'glyphicon-chevron-down': collapsed}"></span>
                      </a>
                      <a ng-click="editMailFolder(mailFolder)">{{mailFolder.name}}</a>
                      <a class="pull-right btn btn-info btn-xs" data-nodrag ng-click="newArchiveSubFolder(this)" style="margin-left: 8px;" ng-show="!mailFolder.hasArchiveFolder && !mailFolder.archiveFolder" tooltip="Créer un dossier d'archive"><span class="glyphicon glyphicon-plus"></span></a>
                      <a class="pull-right btn btn-primary btn-xs" data-nodrag ng-click="newSubFolder(this)" style="margin-left: 8px;" ng-show="!mailFolder.archiveFolder" tooltip="Créer un sous-dossier"><span class="glyphicon glyphicon-plus"></span></a>
                      <span class="pull-right" tooltip="{{mailFolder.$cumulativeCount > 0 && 'Ce dossier contient ' + mailFolder.$cumulativeCount + ' demandes et ne peut pas être supprimé !'|| 'Supprimer ce dossier'}}">
                        <a class="btn btn-danger btn-xs" data-nodrag ng-click="deleteFolder(this)" ng-disabled="mailFolder.$cumulativeCount > 0">
                          <span class="glyphicon glyphicon-remove"></span>
                        </a>
                      </span>
                    </div>
                    <ol ui-tree-nodes="options" ng-model="mailFolder.children" ng-class="{hidden: !collapsed}">
                      <li ng-repeat="mailFolder in mailFolder.children" ui-tree-node ng-include="'nodes_renderer.html'">
                      </li>
                    </ol>
                  </script>
                <div ui-tree="options" data-drag-enabled="false">
                  <ol ui-tree-nodes="" ng-model="mailFolders" id="tree-root">
                    <li ng-repeat="mailFolder in mailFolders" ui-tree-node ng-include="'nodes_renderer.html'"></li>
                  </ol>
                </div>
              </div>
            
              <div class="col-md-8" ng-show="selectedMailFolder && !selectedMailFolder.archiveFolder">
                <h3>Édition du dossier {{selectedMailFolder.name}}</h3>
                <div class="panel-group" id="accordion">

                  <!-- Général -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse0">
                          Généralité
                        </a>
                      </h4>
                    </div>
                    <div id="collapse0" class="panel-collapse collapse" ng-form="generalForm">
                      <div class="panel-body">
                        <div class="form-group" ng-if="selectedMailFolder.$parent">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelCompany" ng-value="false" />
                            Hériter de la société :</label>
                          <input type="text" class="form-control" disabled
                            ng-model="parentScopeValues.company" ng-if="!selectedMailFolder.useCurrentLevelCompany">
                        </div>
                        <div class="form-group">
                          <label>
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelCompany" ng-value="true" ng-if="selectedMailFolder.$parent" />
                            Définir la société :</label>
                          <input name="company" class="form-control" ng-model="selectedMailFolder.company" ng-if="selectedMailFolder.useCurrentLevelCompany || !selectedMailFolder.$parent">
                        </div>

                        <div class="form-group">
                          <label class="control-label">Autoriser le déplacement de demandes vers ce dossier ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.allowMoveDemandIntoFolder" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.allowMoveDemandIntoFolder" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.allowMoveDemandIntoFolder" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.allowMoveDemandIntoFolder ? 'Oui' : 'Non'}})
                          </label>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Autoriser la création de demandes dans ce dossier ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.allowCreateDemandIntoFolder" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.allowCreateDemandIntoFolder" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.allowCreateDemandIntoFolder" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.allowCreateDemandIntoFolder ? 'Oui' : 'Non'}})
                          </label>
                        </div>
                        <div class="form-group">
                          <label class="control-label">L'action imprimer est-elle équivalente à "prendre" ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.printActionEqualTakeAction" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.printActionEqualTakeAction" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.printActionEqualTakeAction" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.printActionEqualTakeAction ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group" ng-if="selectedMailFolder.printActionEqualTakeAction || selectedMailFolder.printActionEqualTakeAction == null && parentScopeValues.printActionEqualTakeAction">
                          <label class="control-label">Seulement si l'élément n'est pas pris ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.printActionEqualTakeOnlyIfNotTaken" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.printActionEqualTakeOnlyIfNotTaken" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.printActionEqualTakeOnlyIfNotTaken" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.printActionEqualTakeOnlyIfNotTaken ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group">
                          <label class="control-label">Par défaut, imprimer aussi les images incluses dans le mail séparément ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.printInlineAttachments" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.printInlineAttachments" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.printInlineAttachments" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.printInlineAttachments ? 'Oui' : 'Non'}})
                          </label>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label">Colorer les demandes invalides dans la liste ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.colorizeInvalidDemands" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.colorizeInvalidDemands" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.colorizeInvalidDemands" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.colorizeInvalidDemands ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group">
                          <label class="control-label">Ouvrir une pièce jointe sans prendre, met-il la ligne en orange ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.openAttachmentReportNoTaken" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.openAttachmentReportNoTaken" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.openAttachmentReportNoTaken" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.openAttachmentReportNoTaken ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group">
                          <label class="control-label">Permettre l'ouverture en lecture seule des demandes bloquées par un autre utilisateur ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.lockedDemandsOpenableInReadOnly" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.lockedDemandsOpenableInReadOnly" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.lockedDemandsOpenableInReadOnly" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.lockedDemandsOpenableInReadOnly ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group">
                          <label class="control-label">Forcer à prendre l'élément pour l'éditer ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.mustTakeToEditDemand" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.mustTakeToEditDemand" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.mustTakeToEditDemand" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.mustTakeToEditDemand ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group">
                          <label class="control-label">Permettre l'ouverture des versions non modifiées des pièces-jointes ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.canViewOriginalAttachments" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.canViewOriginalAttachments" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.canViewOriginalAttachments" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.canViewOriginalAttachments ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group">
                          <label class="control-label">Afficher les aperçus des pièces jointes à la suite du mail ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.showAttachmentPreview" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.showAttachmentPreview" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.showAttachmentPreview" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.showAttachmentPreview ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group">
                          <label class="control-label">Regrouper la pièce-jointe de contenu du mail dans la partie "Divers" ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.mailContentWithInlineAttachments" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.mailContentWithInlineAttachments" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.mailContentWithInlineAttachments" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.mailContentWithInlineAttachments ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group">
                          <label class="control-label">Afficher l'aide sur le message à la fermeture d'un élément non enregistré ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.displayHelpOnMessages" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.displayHelpOnMessages" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.displayHelpOnMessages" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.displayHelpOnMessages ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group">
                          <label class="control-label">Afficher les sous-totaux par état dans la fenêtre de calcul des quantités par gamme ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.computeQuantitiesSubtotalsByState" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.computeQuantitiesSubtotalsByState" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.computeQuantitiesSubtotalsByState" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.computeQuantitiesSubtotalsByState ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <hr />

                        <div class="form-group">
                          <label class="control-label">Afficher la fonction "Répondre" sur les éléments ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.showReplyAction" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.showReplyAction" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.showReplyAction" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.showReplyAction ? 'Oui' : 'Non'}})
                          </label>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Afficher la fonction "Transférer" sur les éléments ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.showForwardAction" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.showForwardAction" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.showForwardAction" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.showForwardAction ? 'Oui' : 'Non'}})
                          </label>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Afficher la fonction "Pris par" sur les éléments ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.showTakenByAction" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.showTakenByAction" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.showTakenByAction" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.showTakenByAction ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <hr />

                        <div class="form-group" ng-if="selectedMailFolder.$parent">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelNbElementToDisplay" ng-value="false" />
                            Hériter du nombre d'éléments à afficher à l'utilisateur :</label>
                          <select class="form-control" disabled
                                  ng-model="parentScopeValues.nbElementToDisplay"
                                  ng-if="!selectedMailFolder.useCurrentLevelNbElementToDisplay">
                            <option value="">Laisser le choix à l'utilisateur</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="150">150</option>
                            <option value="200">200</option>
                            <option value="250">250</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelNbElementToDisplay" ng-value="true" ng-if="selectedMailFolder.$parent" />
                            Définir le nombre d'éléments à afficher à l'utilisateur :</label>
                          <select class="form-control"
                                  ng-model="selectedMailFolder.nbElementToDisplay"
                                  ng-if="selectedMailFolder.useCurrentLevelNbElementToDisplay || !selectedMailFolder.$parent">
                            <option value="">Laisser le choix à l'utilisateur</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="150">150</option>
                            <option value="200">200</option>
                            <option value="250">250</option>
                          </select>
                        </div>

                        <hr />

                        <div class="form-group">
                          <label class="control-label">Autoriser le transfert EDI ?</label> <span class="fa fa-info-circle" tooltip="Autorise le transfert des demandes à l'EDI si les demandes utilisent un type de demande qui autorise également le transfert EDI"></span><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.ediTransfer" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.ediTransfer" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.ediTransfer" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.ediTransfer ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group" ng-if="selectedMailFolder.$parent">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelEdiFolder" ng-value="false" />
                            Hériter du dossier de dépôt des demandes EDI sur le serveur :</label>
                          <input type="text" class="form-control" disabled
                              ng-model="parentScopeValues.ediFolder" ng-if="!selectedMailFolder.useCurrentLevelEdiFolder" />
                        </div>
                        <div class="form-group">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelEdiFolder" ng-value="true" ng-if="selectedMailFolder.$parent" />
                            Définir le dossier de dépôt des demandes EDI sur le serveur :</label>
                          <div class="input-group" ng-if="selectedMailFolder.useCurrentLevelEdiFolder || !selectedMailFolder.$parent">
                            <input type="text" class="form-control" ng-model="selectedMailFolder.ediFolder">
                            <span class="input-group-addon btn btn-info" ng-disabled="!selectedMailFolder.ediFolder" ng-click="checkRemotePath()">
                              <span class="fa fa-cogs"></span> Test
                            </span>
                          </div>
                        </div>

                        <hr />

                        <div class="form-group" ng-if="selectedMailFolder.$parent">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelFaxDomain" ng-value="false" />
                            Hériter du domaine des fax :</label>
                          <input type="text" class="form-control" disabled
                            ng-model="parentScopeValues.faxDomain" ng-if="!selectedMailFolder.useCurrentLevelFaxDomain">
                        </div>
                        <div class="form-group">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelFaxDomain" ng-value="true" ng-if="selectedMailFolder.$parent" />
                            Définir le domaine des fax :</label>
                          <input type="text" class="form-control" ng-model="selectedMailFolder.faxDomain" ng-if="selectedMailFolder.useCurrentLevelFaxDomain || !selectedMailFolder.$parent">
                        </div>

                        <div class="form-group" ng-if="selectedMailFolder.$parent">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelFaxFromNumber" ng-value="false" />
                            Hériter du numéro d'envoi sur réponse à un fax :</label>
                          <input type="text" class="form-control" disabled
                            ng-model="parentScopeValues.faxFromNumber" ng-if="!selectedMailFolder.useCurrentLevelFaxFromNumber">
                        </div>
                        <div class="form-group">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelFaxFromNumber" ng-value="true" ng-if="selectedMailFolder.$parent" />
                            Définir le numéro d'envoi sur réponse à un fax:</label>
                          <input type="text" class="form-control" ng-model="selectedMailFolder.faxFromNumber" ng-if="selectedMailFolder.useCurrentLevelFaxFromNumber || !selectedMailFolder.$parent">
                        </div>
                        
                      </div>
                    </div>
                  </div>

                  <!-- Réception d'un mail -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                          À la réception d'un email
                        </a>
                      </h4>
                    </div>

                    <div id="collapse1" class="panel-collapse collapse" ng-form="emailReception">
                      <div class="panel-body">

                        <div class="form-group" ng-if="!selectedMailFolder.$parent">
                          <label>
                            <input type="checkbox" ng-model="selectedMailFolder.sendAcknowledgementToSender">
                            Envoyer un accusé de réception à la réception d'un email dans ce dossier
                          </label>
                        </div>

                        <div class="form-group" ng-if="!selectedMailFolder.$parent"
                              ng-class="{'has-error': emailReception.acknowledgementMailAddress.$dirty && emailReception.acknowledgementMailAddress.$invalid,
                              'has-success': emailReception.acknowledgementMailAddress.$dirty && emailReception.acknowledgementMailAddress.$valid}">
                          <label>Définir l'adresse expéditeur pour les accusés de réception :</label>
                          <input name="acknowledgementMailAddress" type="email" class="form-control"
                                  ng-model="selectedMailFolder.acknowledgementMailAddress">
                        </div>
                        <div class="form-group" ng-if="!selectedMailFolder.$parent">
                          <label>Définir le message des accusés de réception :</label>
                          <trix-editor angular-trix class="trix-content"
                                       ng-model="selectedMailFolder.acknowledgementMessage"
                                       ng-model-options="{ updateOn: 'change' }"
                                       name="acknowledgementMessage"
                                       trix-change="trixChange(e, editor);"
                                       trix-blur="trixBlur(e, editor);" ></trix-editor>
                         </div>
                         <div class="form-group" ng-if="!selectedMailFolder.$parent">
                          <label>Signature de l'accusé de réception':</label>
                          <input type="file" accept="image/*" id="acknowledgementSigning"
                                 class="form-control"
                                 onchange="angular.element(this).scope().imageChanged(this, 'acknowledgementSigning')"/>
                          <output>
                            <img id="acknowledgementSigningPreview" alt="Aknowledgment Signing Preview" ng-src="{{selectedMailFolder.acknowledgementSigning}}"/>
                          </output>
                        </div>

                        <div class="form" ng-if="!selectedMailFolder.$parent">
                          <div class="form-group">
                            <table id="rights" class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>Exception pour les clients suivants :</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr ng-repeat="client in selectedMailFolder.acknowledgementException">
                                  <td>
                                    <div class="pull-right">
                                      <a class="btn btn-danger btn-xs" ng-click="removeAcknowledgementException($index, client)">
                                        <span class="glyphicon glyphicon-remove"></span>
                                      </a>
                                    </div>
                                    <div>
                                      <strong>{{client.name}}</strong>,
                                      société : {{client.company}}
                                      <span ng-if="client.brand">, marque : {{client.brand}}</span>
                                      , code : {{client.code}}
                                    </div>
                                    <div>
                                      <span ng-if="client.emailAddresses">emails : {{client.emailAddresses}}</span>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>

                            <a class="btn btn-success btn-xs pull-right"
                                ng-click="addAcknowledgementException(newAcknowledgementException)"
                                ng-disabled="!newAcknowledgementException">
                              <span class="glyphicon glyphicon-plus"></span>
                            </a>
                            <ui-select ng-model="newAcknowledgementException" theme="bootstrap"
                                       name="newAcknowledgementException"
                                       on-select="onSelectCallback($item, $model)">
                              <ui-select-match placeholder="Tapez au moins deux caractères pour obtenir la liste des clients">
                                {{$select.selected.name}},
                                société : {{$select.selected.company}},
                                marque : {{$select.selected.brand ? $select.selected.brand : "Non renseignée"}},
                                code : {{$select.selected.code}},
                                emails : {{$select.selected.emailAddresses ? $select.selected.emailAddresses : "Non renseigné"}}
                              </ui-select-match>
                              <ui-select-choices repeat="client in matchingClients | filter:filterByAlreadyInCollection(selectedMailFolder.acknowledgementException) | orderBy:'name'"
                                                 refresh="loadClients($select)"
                                                 refresh-delay="500">
                                <div>
                                  {{client.name}},
                                  société : {{client.company}}
                                  <span ng-if="client.brand">, marque : {{client.brand}}</span>
                                  <span ng-bind-html="', code : ' + client.code | highlight: $select.search"></span>
                                </div>
                                <div>
                                  <span ng-if="client.emailAddresses" ng-bind-html="'emails : ' + client.emailAddresses | highlight: $select.search"></span>
                                </div>
                              </ui-select-choices>
                            </ui-select>
                          </div>
                        </div>

                        <hr ng-if="!selectedMailFolder.$parent"/>

                        <div class="form-group" ng-if="selectedMailFolder.$parent">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelRejectResponseMailAddress" ng-value="false" />
                            Hériter de l'adresse expéditeur de réponse pour les rejets :</label>
                          <input type="text" class="form-control" disabled
                            ng-model="parentScopeValues.rejectResponseMailAddress" ng-if="!selectedMailFolder.useCurrentLevelRejectResponseMailAddress">
                        </div>
                        <div class="form-group" ng-class="{'has-error': generalForm.rejectResponseMailAddress.$dirty && generalForm.rejectResponseMailAddress.$invalid,
                              'has-success': generalForm.rejectResponseMailAddress.$dirty && generalForm.rejectResponseMailAddress.$valid}">
                          <label>
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelRejectResponseMailAddress" ng-value="true" ng-if="selectedMailFolder.$parent" />
                            Définir l'adresse de réponse pour les rejets :</label>
                          <input name="rejectResponseMailAddress" type="email" class="form-control" ng-model="selectedMailFolder.rejectResponseMailAddress" ng-if="selectedMailFolder.useCurrentLevelRejectResponseMailAddress || !selectedMailFolder.$parent">
                        </div>
                        <div class="form-group" ng-if="selectedMailFolder.$parent">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelRejectResponseMessage" ng-value="false" />
                            Hériter du message de réponse pour les rejets :</label>
                          <textarea type="text" class="form-control" disabled
                            ng-model="parentScopeValues.rejectResponseMessage" ng-if="!selectedMailFolder.useCurrentLevelRejectResponseMessage"></textarea>
                        </div>
                        <div class="form-group">
                          <label><input type="radio" ng-model="selectedMailFolder.useCurrentLevelRejectResponseMessage" ng-value="true" ng-if="selectedMailFolder.$parent" />
                            Définir le message de réponse pour les rejets :</label>
                          <textarea class="form-control" ng-model="selectedMailFolder.rejectResponseMessage" ng-if="selectedMailFolder.useCurrentLevelRejectResponseMessage || !selectedMailFolder.$parent"></textarea>
                          <label>Signature :</label>
                          <input type="file" accept="image/*" id="rejectResponseSigning"
                                 class="form-control"
                                 onchange="angular.element(this).scope().imageChanged(this, 'rejectResponseSigning')"/>
                          <output>
                            <img id="rejectResponseSigningPreview" alt="Reject response signing preview" ng-src="{{selectedMailFolder.rejectResponseSigning}}"/>
                          </output>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- Liste des chargés de clientèle -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                          Liste des chargés de clientèle
                        </a>
                      </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                      <div class="panel-body">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Utilisateur</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="customerResponsible in selectedMailFolder.customerResponsibles">
                              <td>
                                <span class="fa fa-user"></span>
                                {{customerResponsible.firstName}} {{customerResponsible.lastName}}</td>
                              <td>
                                <a class="pull-right btn btn-danger btn-xs" ng-click="removeCustomerResponsible($index)">
                                  <span class="glyphicon glyphicon-remove"></span>
                                </a>
                              </td>
                            </tr>
                            <tr ng-if="!selectedMailFolder.customerResponsibles || selectedMailFolder.customerResponsibles.length == 0">
                              <td colspan="2" class="emptyTable">Aucun chargé de clientèle défini</td>
                            </tr>
                          </tbody>
                        </table>

                        <div class="form">
                          <label for="newCustomerResponsibleField" class="control-label">Nouveau :</label>
                          <div class="form-group">
                            <a class="btn btn-success btn-xs pull-right" ng-click="addCustomerResponsible()" ng-disabled="!newCustomerResponsible">
                              <span class="glyphicon glyphicon-plus"></span>
                            </a>
                            <ui-select id="newCustomerResponsibleField" ng-model="newCustomerResponsible"
                                       name="newCustomerResponsible" theme="bootstrap"
                                       on-select="onSelectCustomerResponsibleCallback($item, $model)">
                              <ui-select-match>{{$select.selected.firstName}} {{$select.selected.lastName}}</ui-select-match>
                              <ui-select-choices repeat="user in users | filter:filterByAlreadyInCollection(selectedMailFolder.customerResponsibles) | filter: $select.search">
                                <div ng-bind-html="user.firstName + ' ' + user.lastName | highlight: $select.search"></div>
                              </ui-select-choices>
                            </ui-select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- Liste des emails expéditeur -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                          Liste des adresses email expéditeur (pour les réponses)
                        </a>
                      </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                      <div class="panel-body">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Adresse email</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="replyAddress in parentScopeValues.replyAddresses" class="parentInfos">
                              <td>
                                <span class="glyphicon glyphicon-user"></span>
                                {{replyAddress}}</td>
                              <td>

                              </td>
                            </tr>
                            <tr ng-repeat="replyAddress in selectedMailFolder.replyAddresses">
                              <td>
                                <span class="glyphicon glyphicon-user"></span>
                                {{replyAddress}}</td>
                              <td>
                                <a class="pull-right btn btn-danger btn-xs" ng-click="removeReplyAddress($index)">
                                  <span class="glyphicon glyphicon-remove"></span>
                                </a>
                              </td>
                            </tr>
                            <tr ng-if="!selectedMailFolder.replyAddresses || selectedMailFolder.replyAddresses.length == 0">
                              <td colspan="2" class="emptyTable">Aucune adresse définie</td>
                            </tr>
                          </tbody>
                        </table>

                        <div class="form-inline" ng-form="addAddressForm" role="form">
                          <div class="form-group" 
                              ng-class="{'has-error': addAddressForm.address.$dirty && addAddressForm.address.$invalid,
                              'has-success': addAddressForm.address.$dirty && addAddressForm.address.$valid}">
                            <label for="newAddressField" class="control-label">Nouvelle adresse : </label>
                            <input id="newAddressField" class="form-control" name="address" type="email" ng-model="newReplyAddress" placeholder="nom.prenom@franciaflex.fr" />
                          </div>
                          <a class="btn btn-success btn-xs" ng-click="addReplyAddress()" ng-disabled="!newReplyAddress">
                            <span class="glyphicon glyphicon-plus"></span>
                          </a>
                        </div>

                        <%-- <br />

                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Nom de domaine</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="replyDomain in parentScopeValues.replyDomains" class="parentInfos">
                              <td>
                                <span class="glyphicon glyphicon-globe"></span>
                                {{replyDomain}}</td>
                              <td>

                              </td>
                            </tr>
                            <tr ng-repeat="replyDomain in selectedMailFolder.replyDomains">
                              <td>
                                <span class="glyphicon glyphicon-globe"></span>
                                {{replyDomain}}</td>
                              <td>
                                <a class="pull-right btn btn-danger btn-xs" ng-click="removeReplyDomain($index)">
                                  <span class="glyphicon glyphicon-remove"></span>
                                </a>
                              </td>
                            </tr>
                            <tr ng-if="!selectedMailFolder.replyDomains || selectedMailFolder.replyDomains.length == 0">
                              <td colspan="2" class="emptyTable">Aucune domaine défini</td>
                            </tr>
                          </tbody>
                        </table>
                         
                        <div class="form-inline" ng-form="addDomainForm" role="form">
                          <div class="form-group" 
                              ng-class="{'has-error': addDomainForm.domain.$dirty && addDomainForm.domain.$invalid,
                              'has-success': addDomainForm.domain.$dirty && addDomainForm.domain.$valid}">
                            <label for="newDomainField" class="control-label">Nouveau domaine : </label>
                            <input id="newDomainField" class="form-control" type="text" name="domain" ng-model="newReplyDomain" placeholder="franciaflex.fr" />
                          </div>
                          <a class="btn btn-success btn-xs" ng-click="addReplyDomain()" ng-disabled="!newReplyDomain">
                              <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div> --%>
                      </div>
                    </div>
                  </div>

                  <!-- Sélection des états d'attente possibles pour ce dossier -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                          Sélection des états d'attente possibles pour ce dossier
                        </a>
                      </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse">
                      <div class="panel-body">
                        <div>
                          <label ng-if="selectedMailFolder.$parent">
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelWaitingState"
                              ng-value="false" ng-change="initFolderWaitingState()" />
                            Hériter des états d'attente déclarés sur les dossiers parent :
                          </label>
                          <div class="checkbox" ng-repeat="waitingState in waitingStates" ng-if="selectedMailFolder.$parent && !selectedMailFolder.useCurrentLevelWaitingState">
                            <label>
                              <input type="checkbox" ng-checked="parentScopeValues.waitingStates.containsByTopiaId(waitingState)"
                                  disabled> {{waitingState.label}}
                            </label>
                          </div>
                        </div>
                        <div>
                          <label>
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelWaitingState" ng-value="true"
                              ng-change="initFolderWaitingState()" ng-if="selectedMailFolder.$parent" />
                            Définir les états d'attente pour ce dossier :
                          </label>
                          <div class="checkbox" ng-repeat="waitingState in waitingStates" ng-if="selectedMailFolder.useCurrentLevelWaitingState || !selectedMailFolder.$parent">
                            <label>
                              <input type="checkbox" ng-checked="selectedMailFolder.waitingStates.containsByTopiaId(waitingState)"
                                  ng-click="changeFolderWaitingState(waitingState)"> {{waitingState.label}}
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <!-- Sélection des types de demandes possibles pour ce dossier -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                          Sélection des types de demandes possibles pour ce dossier
                        </a>
                      </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                      <div class="panel-body">
                        <div>
                          <label ng-if="selectedMailFolder.$parent">
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelDemandType"
                              ng-value="false" ng-change="initFolderDemandTypes()" />
                            Hériter des types de demande déclarés sur les dossiers parent :
                          </label>
                          <div class="checkbox" ng-repeat="demandType in demandTypes" ng-if="selectedMailFolder.$parent && !selectedMailFolder.useCurrentLevelDemandType">
                            <label>
                              <input type="checkbox" ng-checked="parentScopeValues.demandTypes.containsByTopiaId(demandType)"
                                  disabled> {{demandType.label}}
                            </label>
                          </div>
                        </div>
                        <div>
                          <label>
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelDemandType" ng-value="true"
                              ng-change="initFolderDemandTypes()" ng-if="selectedMailFolder.$parent" />
                            Définir les types de demande pour ce dossier :
                          </label>
                          <div class="checkbox" ng-repeat="demandType in demandTypes" ng-if="selectedMailFolder.useCurrentLevelDemandType || !selectedMailFolder.$parent">
                            <label>
                              <input type="checkbox" ng-checked="selectedMailFolder.demandTypes.containsByTopiaId(demandType)"
                                  ng-click="changeFolderDemandType(demandType)"> {{demandType.label}}
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- Sélection des gammes possibles pour ce dossier -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                          Sélection des gammes possibles pour ce dossier
                        </a>
                      </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse">
                      <div class="panel-body">
                        <div>
                          <label ng-if="selectedMailFolder.$parent">
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelRange"
                              ng-value="false" ng-change="initFolderRanges()" />
                            Hériter des gammes déclarées sur les dossiers parent :
                          </label>
                          <div class="checkbox" ng-repeat="range in ranges" ng-if="selectedMailFolder.$parent && !selectedMailFolder.useCurrentLevelRange">
                            <label>
                              <input type="checkbox" ng-checked="parentScopeValues.ranges.containsByTopiaId(range)"
                                  disabled> {{range.label}}
                            </label>
                          </div>
                        </div>
                        <div>
                          <label>
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelRange" ng-value="true"
                              ng-change="initFolderRanges()" ng-if="selectedMailFolder.$parent" />
                            Définir les gammes pour ce dossier :
                          </label>
                          <div class="checkbox" ng-repeat="range in ranges" ng-if="selectedMailFolder.useCurrentLevelRange || !selectedMailFolder.$parent">
                            <label>
                              <input type="checkbox" ng-checked="selectedMailFolder.ranges.containsByTopiaId(range)"
                                  ng-click="changeFolderRange(range)"> {{range.label}}
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- Sélection des actions possibles pour ce dossier si une demande est invalide -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                          Sélection des actions autorisées lorsque la demande n'a pas d'état d'attente et est <strong>invalide</strong>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse7" class="panel-collapse collapse">
                      <div class="panel-body">
                        <div>
                          <label ng-if="selectedMailFolder.$parent">
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelInvalidFormDisabledActions"
                              ng-value="false" ng-change="initFolderInvalidFormDisabledActions()" />
                            Hériter des actions déclarées sur les dossiers parent :
                          </label>
                          <div class="checkbox" ng-repeat="(mailAction,label) in mailActions" ng-if="selectedMailFolder.$parent && !selectedMailFolder.useCurrentLevelInvalidFormDisabledActions">
                            <label>
                              <input type="checkbox" ng-checked="parentScopeValues.invalidFormDisabledActions.indexOf(mailAction) == -1"
                                  disabled> {{label}}
                            </label>
                          </div>
                        </div>
                        <div>
                          <label>
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelInvalidFormDisabledActions" ng-value="true"
                              ng-change="initFolderInvalidFormDisabledActions()" ng-if="selectedMailFolder.$parent" />
                            Définir les actions autorisées pour ce dossier :
                          </label>
                          <div class="checkbox" ng-repeat="(mailAction,label) in mailActions" ng-if="selectedMailFolder.useCurrentLevelInvalidFormDisabledActions || !selectedMailFolder.$parent">
                            <label>
                              <input type="checkbox" ng-checked="selectedMailFolder.invalidFormDisabledActions.indexOf(mailAction) == -1"
                                  ng-click="changeInvalidAction(mailAction)"> {{label}}
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- configuration des colonnes a afficher -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
                          Configuration des colonnes à afficher
                        </a>
                      </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                      <div class="panel-body">
                        <div class="form-group">
                          <label ng-if="selectedMailFolder.$parent">
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelTableColumns"
                              ng-value="false" ng-change="initFolderColumns()" />
                            Hériter des colonnes déclarées sur les dossiers parent :
                          </label>

                          <div class="control-group" ng-show="selectedMailFolder.$parent && !selectedMailFolder.useCurrentLevelTableColumns">
                            <div class="controls">
                              <input type="hidden" ui-select2-sortable ng-model="parentFolderTableColumns"
                                  simple-query="getObjectsData" multiple sortable disabled>
                            </div>
                          </div>

                          <div ng-if="selectedMailFolder.$parent && !selectedMailFolder.useCurrentLevelTableColumns">
                            <table id='table-snapshot' class="table table-bordered">
                              <caption>Aperçu</caption>
                              <thead>
                                <tr>
                                  <th ng-repeat="mailField in parentFolderTableColumns">{{tableMailFields[mailField.id]}}</th>
                                </tr>
                              </thead>
                            </table>
                          </div>

                          <label>
                            <input type="radio" ng-model="selectedMailFolder.useCurrentLevelTableColumns" ng-value="true"
                              ng-change="initFolderColumns()" ng-if="selectedMailFolder.$parent" />
                            Définir les colonnes à afficher pour ce dossier :
                          </label>

                          <div class="control-group" ng-show="!selectedMailFolder.$parent || selectedMailFolder.useCurrentLevelTableColumns">
                            <div class="controls">
                              <input type="hidden" ui-select2-sortable ng-model="folderTableColumns"
                                  simple-query="getObjectsData" multiple sortable>
                              <p class="help-block">(l'ordre peut être changé en faisant un glisser/déposer sur les champs)</p>
                            </div>
                          </div>

                          <div ng-if="!selectedMailFolder.$parent || selectedMailFolder.useCurrentLevelTableColumns">
                            <table id='table-snapshot' class="table table-bordered">
                              <caption>Aperçu</caption>
                              <thead>
                                <tr>
                                  <th ng-repeat="mailField in folderTableColumns">{{tableMailFields[mailField.id]}}</th>
                                </tr>
                              </thead>
                            </table>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label">N'afficher que les trigrammes dans les colonnes utilisateurs ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.displayOnlyUserTrigraphInTables" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.displayOnlyUserTrigraphInTables" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.displayOnlyUserTrigraphInTables" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.displayOnlyUserTrigraphInTables ? 'Oui' : 'Non'}})
                          </label>
                        </div>

                        <div class="form-group">
                          <label class="control-label">Permettre à l'utilisateur de trier selon les différentes colonnes ?</label><br />
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.canChangeOrderInTable" ng-value="true"> oui
                          </label>
                          <label class="radio-inline">
                            <input type="radio"
                                ng-model="selectedMailFolder.canChangeOrderInTable" ng-value="false"> non
                          </label>
                          <label class="radio-inline" ng-if="selectedMailFolder.$parent">
                            <input type="radio"
                                ng-model="selectedMailFolder.canChangeOrderInTable" ng-value="undefined"> hériter du dossier parent
                                ({{parentScopeValues.canChangeOrderInTable ? 'Oui' : 'Non'}})
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <!-- Droits -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
                          Droits
                        </a>
                      </h4>
                    </div>
                    <div id="collapse9" class="panel-collapse collapse">
                      <div class="panel-body">
                        <!-- Droits d'accès -->
                        <table id="rights" class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Utilisateur ou groupe</th>
                              <th>Droits</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="group in parentScopeValues.rightGroups" class="parentInfos">
                              <td>
                                <span class="fa fa-users"></span> {{group.completeName}}
                              </td>
                              <td>
                               <label><input type="checkbox" ng-checked="parentScopeValues.readRightGroups.containsByTopiaId(group)" disabled> Lecture</label>
                               <label><input type="checkbox" ng-checked="parentScopeValues.writeRightGroups.containsByTopiaId(group)" disabled> Écriture</label>
                               <label><input type="checkbox" ng-checked="parentScopeValues.moveRightGroups.containsByTopiaId(group)" disabled> Déplacement</label>
                              </td>
                            </tr>
                            <tr ng-repeat="user in parentScopeValues.rightUsers" class="parentInfos">
                              <td>
                                <span class="fa fa-user"></span> {{user.firstName}} {{user.lastName}}
                              </td>
                              <td>
                               <label><input type="checkbox" ng-checked="parentScopeValues.readRightUsers.containsByTopiaId(user)" disabled> Lecture</label>
                               <label><input type="checkbox" ng-checked="parentScopeValues.writeRightUsers.containsByTopiaId(user)" disabled> Écriture</label>
                               <label><input type="checkbox" ng-checked="parentScopeValues.moveRightUsers.containsByTopiaId(user)" disabled> Déplacement</label>
                              </td>
                            </tr>
                          </tbody>
                          <tbody>
                            <tr ng-repeat="group in selectedMailFolder.rightGroups">
                              <td>
                                <span class="fa fa-users"></span> {{group.completeName}}
                                <a class="pull-right btn btn-danger btn-xs" ng-click="removeRightGroup($index, group)">
                                  <span class="glyphicon glyphicon-remove"></span>
                                </a>
                              </td>
                              <td>
                               <label><input type="checkbox"
                                    ng-model="readRightGroup"
                                    ng-checked="selectedMailFolder.readRightGroups.containsByTopiaId(group)"
                                    ng-disabled="parentScopeValues.readRightGroups.containsByTopiaId(group) || selectedMailFolder.writeRightGroups.containsByTopiaId(group)"
                                    ng-change="changeReadRightGroup(group)"> Lecture</label>
                               <label><input type="checkbox"
                                    ng-model="writeRightGroup"
                                    ng-checked="selectedMailFolder.writeRightGroups.containsByTopiaId(group)"
                                    ng-disabled="parentScopeValues.writeRightGroups.containsByTopiaId(group)"
                                    ng-change="changeWriteRightGroup(group)"> Écriture</label>
                               <label><input type="checkbox"
                                    ng-model="moveRightGroup"
                                    ng-checked="selectedMailFolder.moveRightGroups.containsByTopiaId(group)"
                                    ng-disabled="parentScopeValues.moveRightGroups.containsByTopiaId(group)"
                                    ng-change="changeMoveRightGroup(group)"> Déplacement</label>
                              </td>
                            </tr>
                            <tr ng-repeat="user in selectedMailFolder.rightUsers">
                              <td>
                                <span class="fa fa-user"></span> {{user.firstName}} {{user.lastName}}
                                <a class="pull-right btn btn-danger btn-xs" ng-click="removeRightUser($index, user)">
                                  <span class="glyphicon glyphicon-remove"></span>
                                </a>
                              </td>
                              <td>
                               <label><input type="checkbox"
                                    ng-model="readRightUser"
                                    ng-checked="selectedMailFolder.readRightUsers.containsByTopiaId(user)"
                                    ng-disabled="parentScopeValues.readRightUsers.containsByTopiaId(user) || selectedMailFolder.writeRightUsers.containsByTopiaId(user)"
                                    ng-change="changeReadRightUser(user)"> Lecture</label>
                               <label><input type="checkbox"
                                    ng-model="writeRightUser"
                                    ng-checked="selectedMailFolder.writeRightUsers.containsByTopiaId(user)"
                                    ng-disabled="parentScopeValues.writeRightUsers.containsByTopiaId(user)"
                                    ng-change="changeWriteRightUser(user)"> Écriture</label>
                               <label><input type="checkbox"
                                    ng-model="moveRightUser"
                                    ng-checked="selectedMailFolder.moveRightUsers.containsByTopiaId(user)"
                                    ng-disabled="parentScopeValues.moveRightUsers.containsByTopiaId(user)"
                                    ng-change="changeMoveRightUser(user)"> Déplacement</label>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <div class="form">
                          <label for="newRightUserField" class="control-label">Nouvel utilisateur :</label>
                          <div class="form-group">
                            <a class="btn btn-success btn-xs pull-right" ng-click="addRightUser()" ng-disabled="!newRightUser">
                              <span class="glyphicon glyphicon-plus"></span>
                            </a>
                            <ui-select id="newRightUserField" ng-model="newRightUser" theme="bootstrap"
                                       on-select="onSelectRightUserCallback($item, $model)">
                              <ui-select-match>{{$select.selected.firstName}} {{$select.selected.lastName}}</ui-select-match>
                              <ui-select-choices repeat="user in users | filter:filterByAlreadyInCollection(selectedMailFolder.rightUsers) | filter: $select.search">
                                <div ng-bind-html="user.firstName + ' ' + user.lastName | highlight: $select.search"></div>
                              </ui-select-choices>
                            </ui-select>
                          </div>
                        </div>
                        
                        <div class="form">
                          <label for="newRightGroupField" class="control-label">Nouveau groupe :</label>
                          <div class="form-group">
                            <a class="btn btn-success btn-xs pull-right" ng-click="addRightGroup()" ng-disabled="!newRightGroup">
                              <span class="glyphicon glyphicon-plus"></span>
                            </a>
                            <ui-select id="newRightGroupField" ng-model="newRightGroup" theme="bootstrap"
                                       on-select="onSelectRightGroupCallback($item, $model)">
                              <ui-select-match>{{$select.selected.completeName}}</ui-select-match>
                              <ui-select-choices repeat="group in groups | filter:filterByAlreadyInCollection(selectedMailFolder.rightGroups) | filter: $select.search">
                                <div ng-bind-html="group.completeName | highlight: $select.search"></div>
                              </ui-select-choices>
                            </ui-select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-md-8" ng-show="selectedMailFolder.archiveFolder">
              <h3>Édition du dossier {{selectedMailFolder.name}}</h3>

              <!-- Droits -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                      Droits
                  </h4>
                </div>
                <div class="panel-body">
                  <!-- Droits d'accès -->
                  <table id="rights" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Utilisateur ou groupe</th>
                        <th>Droits</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="group in parentScopeValues.rightGroups" class="parentInfos">
                        <td>
                          <span class="fa fa-users"></span> {{group.completeName}}
                        </td>
                        <td>
                         <label><input type="checkbox" ng-checked="parentScopeValues.readRightGroups.containsByTopiaId(group)" disabled> Lecture</label>
                        </td>
                      </tr>
                      <tr ng-repeat="user in parentScopeValues.rightUsers" class="parentInfos">
                        <td>
                          <span class="fa fa-user"></span> {{user.firstName}} {{user.lastName}}
                        </td>
                        <td>
                         <label><input type="checkbox" ng-checked="parentScopeValues.readRightUsers.containsByTopiaId(user)" disabled> Lecture</label>
                        </td>
                      </tr>
                    </tbody>
                    <tbody>
                      <tr ng-repeat="group in selectedMailFolder.rightGroups">
                        <td>
                          <span class="fa fa-users"></span> {{group.completeName}}
                          <a class="pull-right btn btn-danger btn-xs" ng-click="removeRightGroup($index, group)">
                            <span class="glyphicon glyphicon-remove"></span>
                          </a>
                        </td>
                        <td>
                         <label><input type="checkbox"
                              ng-model="readRightGroup"
                              ng-checked="selectedMailFolder.readRightGroups.containsByTopiaId(group)"
                              ng-disabled="parentScopeValues.readRightGroups.containsByTopiaId(group)"
                              ng-change="changeReadRightGroup(group)"> Lecture</label>
                        </td>
                      </tr>
                      <tr ng-repeat="user in selectedMailFolder.rightUsers">
                        <td>
                          <span class="fa fa-user"></span> {{user.firstName}} {{user.lastName}}
                          <a class="pull-right btn btn-danger btn-xs" ng-click="removeRightUser($index, user)">
                            <span class="glyphicon glyphicon-remove"></span>
                          </a>
                        </td>
                        <td>
                         <label><input type="checkbox"
                              ng-model="readRightUser"
                              ng-checked="selectedMailFolder.readRightUsers.containsByTopiaId(user)"
                              ng-disabled="parentScopeValues.readRightUsers.containsByTopiaId(user) || selectedMailFolder.writeRightUsers.containsByTopiaId(user)"
                              ng-change="changeReadRightUser(user)"> Lecture</label>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="form">
                    <label for="newRightUserField" class="control-label">Nouvel utilisateur :</label>
                    <div class="form-group">
                      <a class="btn btn-success btn-xs pull-right" ng-click="addRightUser()" ng-disabled="!newRightUser">
                        <span class="glyphicon glyphicon-plus"></span>
                      </a>
                      <ui-select id="newRightUserField" ng-model="newRightUser" theme="bootstrap"
                                 on-select="onSelectRightUserCallback($item, $model)">
                        <ui-select-match>{{$select.selected.firstName}} {{$select.selected.lastName}}</ui-select-match>
                        <ui-select-choices repeat="user in users | filter:filterByAlreadyInCollection(selectedMailFolder.rightUsers) | filter: $select.search">
                          <div ng-bind-html="user.firstName + ' ' + user.lastName | highlight: $select.search"></div>
                        </ui-select-choices>
                      </ui-select>
                    </div>
                  </div>

                  <div class="form">
                    <label for="newRightGroupField" class="control-label">Nouveau groupe :</label>
                    <div class="form-group">
                      <a class="btn btn-success btn-xs pull-right" ng-click="addRightGroup()" ng-disabled="!newRightGroup">
                        <span class="glyphicon glyphicon-plus"></span>
                      </a>
                      <ui-select id="newRightGroupField" ng-model="newRightGroup" theme="bootstrap"
                                 on-select="onSelectRightGroupCallback($item, $model)">
                        <ui-select-match>{{$select.selected.completeName}}</ui-select-match>
                        <ui-select-choices repeat="group in groups | filter:filterByAlreadyInCollection(selectedMailFolder.rightGroups) | filter: $select.search">
                          <div ng-bind-html="group.completeName | highlight: $select.search"></div>
                        </ui-select-choices>
                      </ui-select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="tabs-filters" class="tab-pane" ng-controller="ConfigurationFilterController">
          <input type="hidden" name="mailFiltersJson" value="{{mailFilters}}" />
          
          <div class="panel-group" id="accordionFilters">

            <div class="panel panel-default" ng-repeat="mailFolder in mailFolders">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordionFilters" href="#mailFilerAccordion{{$index}}">
                    Filtre du dossier {{mailFolder.name}}
                  </a>
                </h4>
              </div>
              <div id="mailFilerAccordion{{$index}}" class="panel-collapse collapse">
                <div class="panel-body">
                  <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>Filtre</th>
                        <th>Dossier</th>
                        <th>Priorité au dossier de la règle</th>
                        <th/>
                      </tr>
                    </thead>
                    <tbody ui-sortable="sortableOptions" ng-model="rootFolderMailFilters[mailFolder.topiaId]">
                      <tr ng-repeat="mailFilter in rootFolderMailFilters[mailFolder.topiaId]">
                        <td>{{mailFilter.expression}}</td>
                        <td>{{mailFilter.mailFolder.$fullPath}}</td>
                        <td>{{mailFilter.filterFolderPriority ? 'Oui' : 'Non'}}</td>
                        <td><a class="btn btn-danger btn-xs" ng-click="removeFilter(mailFolder, mailFilter)">
                            <span class="glyphicon glyphicon-remove"></span>
                          </a></td>
                      </tr>
                    </tbody>
                  </table>
                  
                  <div role="form">
                    <div class="form-group">
                      <label for="filterField">Filtre</label>
                      <input type="text" class="form-control" id="folderField" ng-model="newFilter.expression">
                      <p class="help-block">Le fitre s'applique à l'adresse de reception de l'email.
                      Il peut commencer ou se terminer par <code>%</code> pour respectivement appliquer le filtre à la fin ou au début de l'adresse de réception</p>  
                    </div>
                    <div class="form-group">
                      <label for="folderField">Dossier de destination des email</label>
                      <select class="form-control" ng-model="newFilter.mailFolder" ng-options="mailFolder.$fullPath for mailFolder in flatMailFolders[mailFolder.topiaId]"></select>
                    </div>
                    <div class="form-group">
                      <label for="filterFolderPriorityField">Utiliser le dossier de la règle en priorité : </label>
                      <label class="radio-inline">
                        <input type="radio"
                            ng-model="newFilter.filterFolderPriority" ng-value="false"> non
                      </label>
                      <label class="radio-inline">
                        <input type="radio"
                            ng-model="newFilter.filterFolderPriority" ng-value="true"> oui
                      </label>
                    </div>
                  </div>
                  <button type="button" class="btn btn-success" ng-disabled="!newFilter.expression || !newFilter.mailFolder" ng-click="addNewFilter(mailFolder)">
                    <span class="glyphicon glyphicon-plus"></span> Ajouter
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="tabs-search" class="tab-pane" ng-controller="ConfigurationSearchController">
          <!-- <input type="hidden" name="configuration.searchDisplayColumns" value="{{searchDisplayColumn.id}}" ng-repeat="searchDisplayColumn in searchDisplayColumns"/> -->

          <div class="control-group">
              <label class="control-label" for="tableColumns">Champs à afficher dans le tableau (l'ordre peut être changé en faisant un glisser/déposer sur les champs)</label>

              <div class="controls">
                <input type="hidden" ui-select2-sortable ng-model="searchDisplayColumns" simple-query="getObjectsData" multiple sortable>
              </div>

              <div>
                <table id='table-snapshot' class="table table-bordered">
                  <caption>Aperçu</caption>
                  <thead>
                    <tr>
                      <th ng-repeat="mailField in searchDisplayColumns">{{tableMailFields[mailField.id]}}</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
        </div>
        
        <div id="tabs-mailaccounts" class="tab-pane" ng-controller="ConfigurationEmailAccountsController">
          <input type="hidden" name="emailAccountsJson" value="{{emailAccounts}}" />

          <table id='table-snapshot' class="table table-bordered">
            <thead>
              <tr>
                <th>Protocole</th>
                <th>Serveur</th>
                <th>Port</th>
                <th>Utilisateur</th>
                <th>Options</th>
                <th/>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="emailAccount in emailAccounts">
                <td>{{emailAccount.protocol}}</td>
                <td>{{emailAccount.host}}</td>
                <td>{{emailAccount.port}}</td>
                <td>{{emailAccount.login}}</td>
                <td>
                  <span class="fa fa-fax" tooltip="Compte email ne recoit que des fax" ng-if="emailAccount.faxAccountType"></span>
                  <span class="fa fa-reply" tooltip="Le rejet des email est autorisé pour ce compte" ng-if="emailAccount.rejectAllowed"></span>
                </td>
                <td>
                  <a class="btn btn-danger btn-xs" ng-click="removeEmailAccount($index)">
                    <span class="glyphicon glyphicon-remove"></span>
                  </a></td>
              </tr>
              <tr ng-if="!emailAccounts || emailAccounts.length == 0">
                <td class="emptyTable" colspan="6">Aucun compte email trouvé</td>
              </tr>
            </tbody>
          </table>
          
          <div role="form" ng-form="addEmailAccountForm">
            <fieldset>
              <legend>Nouveau compte email</legend>
              <div class="form row">
                <div class="form-group col-xs-1">
                  <label for="emailAccountProtocol" class="control-label">Protocole:</label>
                  <!-- <input type="text" class="form-control" ng-model="newAccount.protocol" /> -->
                  <select id="emailAccountProtocol" ng-model="newAccount.protocol" ng-options="protocol as protocol for (protocol, port) in emailProtocolPorts"></select>
                </div>
                <div class="form-group col-xs-4">
                  <label for="emailAccountHost" class="control-label">Serveur:</label>
                  <input id="emailAccountHost" name="newAccountHost" type="text" class="form-control" ng-model="newAccount.host" ng-minlength="1" />
                </div>
                <div class="form-group col-xs-1"
                    ng-class="{'has-error': addEmailAccountForm.newAccountPort.$dirty && addEmailAccountForm.newAccountPort.$invalid,
                               'has-success': addEmailAccountForm.newAccountPort.$dirty && addEmailAccountForm.newAccountPort.$valid}">
                  <label for="emailAccountPort" class="control-label">Port:</label>
                  <input id="emailAccountPort" name="newAccountPort" type="text" class="form-control" ng-model="newAccount.port" ng-pattern="/^\d+$/" />
                </div>
                <div class="form-group col-xs-3">
                  <label for="emailAccountUser" class="control-label">Utilisateur:</label>
                  <input id="emailAccountUser" type="text" class="form-control" ng-model="newAccount.login"/>
                </div>
                <div class="form-group col-xs-3">
                  <label for="emailAccountPassword" class="control-label">Mot de passe:</label>
                  <input id="emailAccountPassword" type="password" class="form-control" ng-model="newAccount.password">
                </div> 
              </div>
              <div class="form row">
                <div class="form-group col-xs-5">
                  <label class="control-label">Réception de fax:</label>
                  <div class="checkbox">
                    <label>
                      <input id="emailRejectAllowed" type="checkbox" ng-value="true" ng-model="newAccount.faxAccountType">
                      ce compte email recoit seulement des fax
                    </label>
                  </div>
                </div>
                <div class="form-group col-xs-5">
                  <label class="control-label">Rejet:</label>
                  <div class="checkbox">
                    <label>
                      <input id="emailRejectAllowed" type="checkbox" ng-value="true"
                        ng-model="newAccount.rejectAllowed">
                      autoriser les mails de ce compte à pouvoir être rejeté
                    </label>
                  </div>
                </div>
                <div class="form-group col-xs-2">
                  <br />
                  <button type="button" class="btn btn-info" ng-disabled="!newAccount.protocol || !newAccount.host || !newAccount.port || !newAccount.login || addEmailAccountForm.$invalid"
                    ng-click="checkEmailAccount()">
                    <span class="fa fa-cogs"></span> Test
                  </button>
                  <button type="button" class="btn btn-success" ng-disabled="!newAccount.protocol || !newAccount.host || !newAccount.port || !newAccount.login || addEmailAccountForm.$invalid" ng-click="addEmailAccount()">
                    <span class="fa fa-plus"></span> Ajouter
                  </button>
                </div>
              </div>
            </fieldset>
          </div>
        </div>

        <div id="tabs-brandsForDomain" class="tab-pane" ng-controller="ConfigurationBrandsForDomainController">
          <input type="hidden" name="brandsForDomainsJson" value="{{brandsForDomains}}" />

          <table id='table-snapshot' class="table table-bordered">
            <thead>
              <tr>
                <th>Nom de domaine</th>
                <th>Marques</th>
                <th/>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="brandsForDomain in brandsForDomains">
                <td>{{brandsForDomain.domainName}}</td>
                <td>{{brandsForDomain.brandsJson}}</td>
                <td>
                  <a class="btn btn-danger btn-xs" ng-click="removeBrandsForDomain($index)">
                    <span class="glyphicon glyphicon-remove"></span>
                  </a>
                </td>
              </tr>
              <tr ng-if="!brandsForDomains || brandsForDomains.length == 0">
                <td class="emptyTable" colspan="6">Aucun nom de domaine configuré</td>
              </tr>
            </tbody>
          </table>

          <div role="form" ng-form="addBrandsForDomainForm">
            <fieldset>
              <legend>Nouveau nom de domaine</legend>
              <div class="form row">
                <div class="form-group col-xs-5">
                  <label for="brandsForDomainDomain" class="control-label">Nom de domaine :</label>
                  <!-- <input type="text" class="form-control" ng-model="newAccount.protocol" /> -->
                  <input id="brandsForDomainDomain" name="newBrandsForDomainDomain" type="text" class="form-control" ng-model="newBrandsForDomain.domainName" ng-minlength="1" />
                </div>
                <div class="form-group col-xs-5">
                  <label for="brandsForDomainBrands" class="control-label">Marques :</label>
                  <input id="brandsForDomainBrands" name="newBrandsForDomainBrands" type="text" class="form-control" ng-model="newBrandsForDomain.brands" ng-list ng-minlength="1" />
                </div>
                <div class="form-group col-xs-2">
                  <br/>
                  <button type="button" class="btn btn-success" ng-disabled="!newBrandsForDomain.domainName || !newBrandsForDomain.brands || newBrandsForDomain.$invalid" ng-click="addBrandsForDomain()">
                    <span class="fa fa-plus"></span> Ajouter
                  </button>
                </div>
              </div>
            </fieldset>
          </div>
        </div>

        <div id="tabs-signings" class="tab-pane" ng-controller="ConfigurationSigningsController">
          <input type="hidden" name="signingForDomainsJson" value="{{signings}}" />

          <div class="row">
            <div class="col-md-4">
              <h3>Signatures <input type="button" class="btn btn-success btn-xs pull-right" value="Nouveau" ng-click="newSigning()" /></h3>
              <table class="table table-hover">
                <tbody>
                  <tr ng-repeat="signing in signings"
                      ng-class="{'info' : signing == selectedSigning}"
                      ng-click="editSigning(signing)">
                    <td>
                      {{signing.domainName}}
                      <a class="pull-right btn btn-danger btn-xs" data-nodrag ng-click="deleteSigning(signing, $index)"
                          tooltip="Supprimer cette signature">
                          <span class="glyphicon glyphicon-remove"></span></a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div class="col-md-8" ng-show="selectedSigning">

              <h3>Édition de la signature du domaine {{selectedSigning.domainName}}</h3>

              <div class="form-group">
                <label><input type="radio" ng-model="selectedSigning.isImageType" ng-value="true"/>
                Image :</label>

                <div ng-show="selectedSigning.isImageType">

                  <input type="file" accept="image/*" id="signingImage"
                         class="form-control"
                         onchange="angular.element(this).scope().imageChanged(this)"/>

                  <output>
                    <img id="signingPreview" alt="Signing preview"/>
                  </output>

                 </div>

              </div>

              <div class="form-group">
                <label><input type="radio" ng-model="selectedSigning.isImageType" ng-value="false" /> Texte :</label>
                <textarea name="text" class="form-control"
                          ng-model="selectedSigning.text" ng-show="!selectedSigning.isImageType">
                </textarea>
              </div>

            </div>

            <div class="col-md-8" ng-show="!selectedSigning">
              <em>Sélectionnez une signature.</em>
            </div>
          </div>

        </div>

        <div id="tabs-chefgroup" class="tab-pane" ng-controller="ConfigurationChefGroupController">

          <div class="row">
            <div class="col-md-4">
              <h3>Chef de groupe <input type="button" class="btn btn-success btn-xs pull-right" value="Nouveau" ng-click="newGroupChef()" /></h3>
              <table class="table table-hover">
                <tbody>
                  <tr ng-repeat="groupChef in configuration.chefs"
                      ng-class="{'info' : groupChef == selectedGroupChef}"
                      ng-click="editGroupChef(groupChef)">
                    <td>{{groupChef.userGroup ? groupChef.userGroup.completeName : '(aucun groupe)'}}  
                      <a class="btn btn-danger btn-xs pull-right" ng-click="removeGroupChef(groupChef, $index)">
                        <span class="glyphicon glyphicon-remove"></span>
                      </a>
                    </td>
                  </tr>
                  <tr ng-if="!configuration.chefs || configuration.chefs.length == 0">
                    <td class="emptyTable">Aucun chef de groupe</td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div class="col-md-8" ng-show="selectedGroupChef">
              <h3>Édition du chef de groupe</h3>
              
              <div class="form">
                <div class="form-group required">
                  <label for="groupChefUserGroupField" class="control-label">Chef du groupe : </label>
                  <ui-select id="groupChefUserGroupField" ng-model="selectedGroupChef.userGroup"
                             ng-required="true" theme="bootstrap">
                    <ui-select-match>{{$select.selected.completeName}}</ui-select-match>
                    <ui-select-choices repeat="group in groups | filter:availableNewGroupChef | filter: $select.search">
                      <div ng-bind-html="group.completeName | highlight: $select.search"></div>
                    </ui-select-choices>
                  </ui-select>
                </div>
              </div>

              <hr />
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Groupes gérés par le chef de groupe</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="group in selectedGroupChef.managedGroups">
                    <td>
                    <span class="fa fa-users"></span>
                    {{group.completeName}}
                    <a class="btn btn-danger btn-xs pull-right" ng-click="removeManagedGroup($index)">
                      <span class="glyphicon glyphicon-remove"></span>
                    </a>
                    </td>
                  </tr>
                  <tr ng-if="!selectedGroupChef.managedGroups || selectedGroupChef.managedGroups.length == 0">
                    <td class="emptyTable">Aucun groupe gérés</td>
                  </tr>
                </tbody>
              </table>
              <div class="form">
                <label for="newManagedGroupField" class="control-label">Nouveau groupe :</label>
                <div class="form-group">
                  <a class="btn btn-success btn-xs pull-right" ng-click="addManagedGroup()" ng-disabled="!newManagedGroup">
                    <span class="glyphicon glyphicon-plus"></span>
                  </a>
                  <ui-select id="newManagedGroupField" ng-model="newManagedGroup" theme="bootstrap"
                             on-select="onSelectManagedGroupCallback($item, $model)">
                    <ui-select-match>{{$select.selected.completeName}}</ui-select-match>
                    <ui-select-choices repeat="group in groups | filter:filterByAlreadyInCollection(selectedGroupChef.managedGroups) | filter: $select.search">
                      <div ng-bind-html="group.completeName | highlight: $select.search"></div>
                    </ui-select-choices>
                  </ui-select>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <nav class="navbar navbar-default navbar-fixed-bottom">
        <div class="container">
          <button type="submit" class="btn btn-primary navbar-btn pull-right">Valider</button>
        </div>
      </nav>
    </s:form>
  </div>
  </body>
</html>
