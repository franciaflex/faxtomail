<%--
  #%L
  FaxToMail :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html>
    <head>
       <title>Accueil</title>
    </head>
    <body>
      <div id="main-container" class="container">
        <h1 class="page-header">Administration FaxToMail</h1>
        
        <ul>
          <s:if test="admin">
            <li><a href="<s:url action='ldap-input' namespace="/admin" />">
              <span class="fa fa-database"></span> Active Directory</a></li>
            <li><a href="<s:url action='configuration-input' namespace="/admin" />">
              <span class="fa fa-cog"></span> Configuration</a></li>
            <li><a href="<s:url action='import-input' namespace="/admin" />">
              <span class="fa fa-file-excel-o"></span> Import</a></li>
            <li><a href="<s:url action='lock-input' namespace="/admin" />">
              <span class="fa fa-unlock-alt"></span> Verrouillages</a></li>
          </s:if>
          <li><a href="<s:url action='user-folder-input' namespace="/admin" />">
            <span class="fa fa-folder-open"></span> Dossiers utilisateur</a></li>
        </ul>
      </div>
    </body>
</html>
