<%--
  #%L
  FaxToMail :: Web
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
  <head>
    <title>Recherche d'archives</title>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/faxtomail-archive-search.css' />" />
  </head>

  <body>

    <div id="main-container" class="container">

      <h1 class="page-header">Recherche d'archives</h1>

      <s:form id="main_form" action="search" method="get" theme="simple">
        <s:actionmessage/>
        <s:actionerror/>

        <label for="elementField" class="control-label">Numéro de commande / devis&nbsp;:</label>
        <s:textfield type="text" id="elementField" name="element" required="true" class="form-control"/>
        <label for="companyField" class="control-label">Société&nbsp;:</label>
        <s:textfield type="text" id="companyField" name="company" required="true" class="form-control"/>
        <s:submit cssClass="btn btn-primary" value="Valider"/>

      </s:form>

      <s:if test="results != null">

        <div style="overflow-x: auto; margin-top: 20px">

          <s:if test="results.isEmpty()">
            <em>Aucun résultat pour cette recherche.</em>
          </s:if>
          <s:else>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th></th>
                  <s:iterator value="tableMailFields">
                    <th><s:property value="value"/></th>
                  </s:iterator>
                </tr>
              </thead>
              <tbody>
                <s:iterator value="results" var="result">
                  <tr>
                    <td>
                      <s:url var="detailUrlId" value="demand-detail.action">
                          <s:param name="id" value="%{topiaId}" />
                      </s:url>
                      <s:a href="%{detailUrlId}"><span class="fa fa-eye"></span></s:a>
                    </td>
                    <s:iterator value="tableMailFields">
                      <td class="cut-eol" title="<s:text name="%{getTooltip(key, #result)}"/>"><s:text name="%{getAttr(key, #result)}"/></td>
                    </s:iterator>
                  </tr>
                </s:iterator>
              </tbody>
            </table>
          </s:else>

        </div>

      </s:if>

    </div>
  </body>
</html>