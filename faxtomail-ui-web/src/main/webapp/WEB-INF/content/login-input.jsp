<%--
  #%L
  FaxToMail :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html>
    <head>
       <title>Connexion</title>
    </head>
    <body>

      <div id="main-container" class="container">
        <h1 class="page-header">Connexion</h1>

        <s:form id="main_form" action="login" method="post" role="form" class="form-horizontal">

          <s:actionerror/>

          <div class="form-group">
            <label for="loginField" class="col-sm-2 control-label">Identifiant :</label>
            <div class="col-sm-10">
              <input type="text" name="login" class="form-control" id="loginField" placeholder="ex: dpt" required>
            </div>
          </div>
          <div class="form-group">
            <label for="passwordField" class="col-sm-2 control-label">Mot de passe :</label>
            <div class="col-sm-10">
              <input type="password" name="password" class="form-control" id="passwordField" placeholder="ex: password" required>
            </div>
          </div>
          <!-- <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <div class="checkbox">
                <label>
                  <input type="checkbox"> Remember me
                </label>
              </div>
            </div>
          </div> -->
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary">Connexion</button>
            </div>
          </div>
        </s:form>
      </div>
    </body>
</html>
