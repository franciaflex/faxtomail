<%--
  #%L
  FaxToMail :: Web
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
  <head>
    <title>Détail de l'élément <s:property value="demand.title"/></title>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/faxtomail-detail-demand.css' />" />
    <script type="text/javascript" src="<s:url value='/nuiton-js/faxtomail-detail-demand.js' />"></script>
    <script type="text/javascript">
      DemandDetailModule.value('DemandDetailData', {
        'replies': <s:property value="toJson(replies)" escapeHtml="false"/>
      });
    </script>
  </head>

  <body>

    <div id="main-container" class="container-fluid" ng-app="DemandDetailModule" ng-controller="DemandDetailController">
      <s:if test="demand != null">

        <!-- groupe | réponses | historique | pj -->
        <nav class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <span class="navbar-brand"><s:property value="demand.title"/></span>
            </div>
            <ul class="nav navbar-nav navbar-right">

              <!-- replies -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" title="Réponses envoyées depuis l'application">
                  <span class="fa fa-envelope-o"></span> (<s:property value="demand.sizeReplies()"/>)
                  <s:if test="demand.sizeReplies() > 0">
                    <span class="caret"></span>
                  </s:if>
                </a>
                <s:if test="demand.sizeReplies() > 0">
                  <ul class="dropdown-menu" role="menu">
                    <s:iterator value="demand.replies" var="reply" status="status">
                      <li><span>
                        <a ng-click='showReplyModal(<s:property value="#status.index"/>)'
                            class="fa fa-eye float-right" title="Voir la réponse"></a>
                        <span class="margin-right-1-icon"><s:text name="%{decorate(#reply)}" /></span>
                      </span></li>
                    </s:iterator>
                  </ul>
                </s:if>
              </li>

              <!-- history -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" title="Historique de l'élément">
                  <span class="fa fa-calendar-o"></span> (<s:property value="demand.sizeHistory()"/>)
                  <s:if test="demand.sizeHistory() > 0">
                    <span class="caret"></span>
                  </s:if>
                </a>
                <s:if test="demand.sizeHistory() > 0">
                  <ul class="dropdown-menu" role="menu">
                    <s:iterator value="histories" var="history">
                      <li><span>
                        <div>
                          le <s:text name="%{decorate(#history.modificationDate)}"/>
                          <s:text name="%{decorate(#history.type)}"/>
                          par <s:text name="%{decorateUser(#history.faxToMailUser)}"/>
                        </div>
                      </span></li>
                    </s:iterator>
                  </ul>
                </s:if>
              </li>

              <!-- attachments -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" title="Pièces jointes de l'élément">
                  <span class="fa fa-paperclip"></span> (<s:property value="demand.sizeAttachment()"/>)
                  <s:if test="demand.sizeAttachment() > 0">
                    <span class="caret"></span>
                  </s:if>
                </a>
                <s:if test="demand.sizeAttachment() > 0">
                  <ul class="dropdown-menu" role="menu">
                    <s:iterator value="demand.attachment" var="attachment">
                      <li><span>
                        <s:if test="#attachment.editedFileName != null">
                          <!-- edited attachment -->
                          <s:url var="editedAttachmentUrlId" value="attachment-download.action">
                              <s:param name="id" value="%{#attachment.topiaId}" />
                              <s:param name="original" value="false" />
                          </s:url>
                          <s:a href="%{editedAttachmentUrlId}" cssClass="fa fa-pencil float-right" title="Ouvrir la version éditée"></s:a>
                        </s:if>
                        <s:else>
                          <span class="fa fa-pencil float-right" title="Pas de version éditée"></span>
                        </s:else>

                        <!-- original attachment -->
                        <s:url var="originalAttachmentUrlId" value="attachment-download.action">
                            <s:param name="id" value="%{#attachment.topiaId}" />
                            <s:param name="original" value="true" />
                        </s:url>
                        <s:a href="%{originalAttachmentUrlId}" cssClass="fa fa-folder-open-o float-right margin-right-10" title="Ouvrir la version originale"></s:a>

                        <span class="margin-right-2-icons"><s:text name="%{decorate(#attachment)}" /></span>
                      </span></li>
                    </s:iterator>
                  </ul>
                </s:if>
              </li>

              <!-- group -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" title="Éléments groupés">
                  <span class="fa fa-link"></span> (<s:property value="demand.emailGroup != null ? demand.emailGroup.sizeEmail() : 1"/>) <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <s:if test="demand.emailGroup != null">
                    <s:iterator value="demand.emailGroup.email" var="email">
                      <li><span>
                        <s:if test="demand.equals(#email)">
                          <span class="fa fa-eye float-right" title="Élément courant"></span>
                          <span class="margin-right-1-icon"><s:property value="#email.title"/></span>
                        </s:if>
                        <s:else>
                          <s:url var="detailUrlId" value="demand-detail.action">
                              <s:param name="id" value="%{#email.topiaId}" />
                          </s:url>
                          <s:a href="%{detailUrlId}" cssClass="fa fa-eye float-right" title="Ouvrir l'élément groupé"></s:a>
                          <span class="margin-right-1-icon"><s:property value="#email.title"/></span>
                        </s:else>
                      </span></li>
                    </s:iterator>
                  </s:if>
                  <s:else>
                    <li><span>
                      <span class="fa fa-eye float-right" title="Élément courant"></span>
                      <span class="margin-right-1-icon"><s:property value="demand.title"/></span>
                    </span></li>
                  </s:else>
                </ul>
              </li>
            </ul>
          </div><!-- /.container-fluid -->
        </nav>

        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 col-lg-6">
              <dl class="dl-horizontal large-dl">
                <dt>Reçu le</dt>
                <dd><s:text name="%{decorate(demand.receptionDate)}" /></dd>
                <dt>Émetteur</dt>
                <dd><s:property value="demand.sender" /></dd>
                <dt>Objet</dt>
                <dd><s:property value="demand.object" /></dd>
                <dt>Client (marque)</dt>
                <dd><s:if test="demand.client != null"><s:property value="demand.client.code" /> (<s:property value="demand.client.brand" />)</s:if></dd>
                <dt>Type</dt>
                <dd><s:text name="%{decorate(demand.demandType)}" /></dd>
                <dt>Priorité</dt>
                <dd><s:text name="%{decorate(demand.priority)}" /></dd>
                <dt>Référence chantier</dt>
                <dd><s:property value="demand.projectReference" /></dd>
                <dt>Notre référence</dt>
                <dd><s:property value="demand.companyReference" /></dd>
                <dt>État d'attente</dt>
                <dd><s:text name="%{decorate(demand.waitingState)}" /></dd>
                <dt>Status</dt>
                <dd><s:text name="%{decorate(demand.demandStatus)}" /></dd>
                <dt>Pris par</dt>
                <dd><s:text name="%{decorate(demand.takenBy)}" /></dd>
              </dl>

              <dl class="dl-horizontal large-dl">
                <s:if test="demand.firstOpeningDate != null">
                  <dt>Première ouverture</dt>
                  <dd>par <s:text name="%{decorateUser(demand.firstOpeningUser)}" /> le <s:text name="%{decorate(demand.firstOpeningDate)}" /></dd>
                </s:if>
                <s:if test="demand.lastModificationDate != null">
                  <dt>Dernière modification</dt>
                  <dd>par <s:text name="%{decorateUser(demand.lastModificationUser)}" /> le <s:text name="%{decorate(demand.lastModificationDate)}" /></dd>
                </s:if>
                <s:if test="demand.lastAttachmentOpeningInFolderDate != null">
                  <dt>Dernière ouverture de pièce jointe</dt>
                  <dd>par <s:text name="%{decorateUser(demand.lastAttachmentOpeningInFolderUser)}" /> le <s:text name="%{decorate(demand.lastAttachmentOpeningInFolderDate)}" /></dd>
                </s:if>
              </dl>

              <s:if test="demand.sizeRangeRow() > 0">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th title="Gamme">Gamme</th>
                      <th title="N° commande / devis">N° commande / devis</th>
                      <th title="Quantité de devis">Qté devis</th>
                      <th title="Quantité de produits">Qté produits</th>
                      <th title="Quantité de SAV">Qté SAV</th>
                    </tr>
                  </thead>
                  <tbody>
                    <s:iterator value="demand.rangeRow" var="row">
                      <tr>
                        <td title="<s:text name="%{decorate(#row.range)}" />"><s:text name="%{decorate(#row.range)}" /></td>
                        <td title="<s:property value="#row.commandNumber"/>"><s:property value="#row.commandNumber"/></td>
                        <td title="<s:property value="#row.quotationQuantity"/>"><s:property value="#row.quotationQuantity"/></td>
                        <td title="<s:property value="#row.productQuantity"/>"><s:property value="#row.productQuantity"/></td>
                        <td title="<s:property value="#row.savQuantity"/>"><s:property value="#row.savQuantity"/></td>
                      </tr>
                    </s:iterator>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Total</th>
                      <th></th>
                      <th><s:property value="quotationTotal"/></th>
                      <th><s:property value="productTotal"/></th>
                      <th><s:property value="savTotal"/></th>
                    </tr>
                  </tfoot>
                </table>
              </s:if>
            </div>

            <div class="col-md-6 col-lg-6">
              <dl class="dl-horizontal">
                <dt>À</dt>
                <dd><s:property value="emailUIModel.toRecipients" /></dd>
                <dt>Copie à</dt>
                <dd><s:property value="emailUIModel.ccRecipients" /></dd>
                <dt>Sujet</dt>
                <dd><s:property value="emailUIModel.subject" /></dd>
                <dt>Contenu de l'email</dt>
                <dd class="well well-sm"><s:property value="emailUIModel.content" escapeHtml="false"/></dd>
                <dt>Message</dt>
                <dd class="well well-sm"><s:text name="%{decorate(demand.comment)}" />
              </dl>
            </div>
          </div>
        </div>

        <div class="modal fade" id="replyModal"
             tabindex="-1" role="dialog"
             aria-labelledby="replyModalLabel" aria-hidden="true">

          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="replyModalLabel">{{currentReply.subject}}</h4>
              </div>
              <div class="modal-body">
                <dl class="dl-horizontal">
                  <dt>De</dt>
                  <dd>{{currentReply.sender}}</dd>
                  <dt>À</dt>
                  <dd>{{currentReply.toRecipients}}</dd>
                  <dt>Copie à</dt>
                  <dd>{{currentReply.ccRecipients}}</dd>
                  <dt>Copie cachée à</dt>
                  <dd>{{currentReply.bccRecipients}}</dd>
                  <dt>Sujet</dt>
                  <dd>{{currentReply.subject}}</dd>
                </dl>
                <p class="well" ng-bind-html="currentReply.content"></p>
                <div>
                  Pièces jointes
                  <ul>
                    <li ng-repeat="attachment in currentReply.attachments">
                      <a href="reply-attachment-download.action?id={{currentReply.id}}&index={{$index}}" title="Ouvrir la pive jointe">{{attachment.filename}}</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>

          <script>
              $('#replyModal').on('hide.bs.modal', function (e) {
                  $('#main-container').removeClass('not-printable');
                  $('#printable-reply').addClass('not-printable');
              });
          </script>

        </div>

        <div id="printable-reply" class="not-printable">
          <dl class="dl-horizontal">
            <dt>De</dt>
            <dd>{{currentReply.sender}}</dd>
            <dt>À</dt>
            <dd>{{currentReply.toRecipients}}</dd>
            <dt>Copie à</dt>
            <dd>{{currentReply.ccRecipients}}</dd>
            <dt>Copie cachée à</dt>
            <dd>{{currentReply.bccRecipients}}</dd>
            <dt>Sujet</dt>
            <dd>{{currentReply.subject}}</dd>
          </dl>
          <p class="well" ng-bind-html="currentReply.content"></p>
          <div>
            Pièces jointes
            <ul>
              <li ng-repeat="attachment in currentReply.attachments">
                <a href="reply-attachment-download.action?id={{currentReply.id}}&index={{$index}}" title="Ouvrir la pive jointe">{{attachment.filename}}</a>
              </li>
            </ul>
          </div>
        </div>
      </s:if>
    </div>

  </body>
</html>