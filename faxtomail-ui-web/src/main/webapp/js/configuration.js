/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

var ConfigurationModule = angular.module('ConfigurationModule', ['FaxToMail', 'ngSanitize', 'ui.tree', 'ui.select', 'ui.sortable', 'ui.select2.sortable', 'angularTrix']);

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs an AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform an OR.
 */
ConfigurationModule.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);

      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

/**
 * Global configuration controller.
 */
ConfigurationModule.controller('ConfigurationController', ['$scope', 'ConfigurationData',
  function($scope, ConfigurationData) {
    //{Object} L'object configuration
    $scope.configuration = ConfigurationData.configuration;
    //{Map} les actions possibles pour les etats d'attentes
    $scope.mailActions = ConfigurationData.mailActions;
    //{Map} les champs obligatoires possibles
    $scope.canBeRequiredMailFields = ConfigurationData.canBeRequiredMailFields;
    //{Map} les champs possibles du tableau
    $scope.tableMailFields = ConfigurationData.tableMailFields;
    //{Array} les etats d'attentes disponibles
    $scope.waitingStates = ConfigurationData.waitingStates;
    //{Array} les gammes disponibles
    $scope.ranges = ConfigurationData.ranges;
    //{Array} les types de documents
    $scope.demandTypes = ConfigurationData.demandTypes;
    //{Array} les tampons
    $scope.stamps = ConfigurationData.stamps;
    //{Array} les signatures
    $scope.signings = ConfigurationData.signings;
    //{Array} mail folders
    $scope.mailFolders = ConfigurationData.mailFolders;
    // {Map} Mail folder usage
    $scope.mailFoldersUsage = ConfigurationData.mailFoldersUsage;
    //{Map<MailFolder id, Array<MailFolder>>} liste des dossier (a plat, par dossier racine)
    $scope.flatMailFolders;
    //{Array} Current mail filter list
    $scope.mailFilters = ConfigurationData.mailFilters;
    //{Map} Folder mail fiters
    $scope.rootFolderMailFilters = {};
    //{Array} All application users
    $scope.users = ConfigurationData.users;
    // {Array} All application groups
    $scope.groups = ConfigurationData.groups;

    // method privée recursive pour retourner l'ensemble des dossiers en les modifiant pour ajouter
    // des metadata (full path from root, and root node instance)
    var recursiveAddMailFolder = function(result, mailFolders, rootFolder, prefix, parent) {
      var cumulativeCount = 0;
      if (mailFolders) {
        angular.forEach(mailFolders, function(mailFolder) {

          // warning modify input object :(
          // use $ from this, even not recommended because angular exlude those fields when jsonify
          mailFolder.$fullPath = prefix + "/" + mailFolder.name;
          // ha la la, modify input object again :(
          // use $ from this, even not recommended because angular exlude those fields when jsonify
          mailFolder.$rootFolder = rootFolder;
          // encore pour le parent
          mailFolder.$parent = parent;

          var folderCount = 0;
          if ($scope.mailFoldersUsage.hasOwnProperty(mailFolder.topiaId)) {
            folderCount += $scope.mailFoldersUsage[mailFolder.topiaId];
          }

          if (mailFolder.archiveFolder) {
            parent.hasArchiveFolder = true;

          } else {
            result.push(mailFolder);
          }

          folderCount += recursiveAddMailFolder(result, mailFolder.children, rootFolder, mailFolder.$fullPath, mailFolder);
          
          // et on modifie encore (tant qu'on a commencé !!!)
          mailFolder.$cumulativeCount = folderCount;
          cumulativeCount += folderCount;
        });
      }
      return cumulativeCount;
    };

    // update $scope.flatMailFolders when $scope.mailFolders changes
    $scope._updateFlatMailFolders = function() {
      $scope.flatMailFolders = {};
      angular.forEach($scope.mailFolders, function(mailFolder) {
        $scope.flatMailFolders[mailFolder.topiaId] = [];
        var cumulativeCount = recursiveAddMailFolder($scope.flatMailFolders[mailFolder.topiaId], mailFolder.children, mailFolder, mailFolder.name, mailFolder);
        mailFolder.$cumulativeCount = cumulativeCount;
        mailFolder.$fullPath = mailFolder.name;
      });
    };
    $scope._updateFlatMailFolders();


    // replace filter instance with mailFolder filter instance to use same objects (filled with fullPathInfo)
    var _updateMetaFilter = function() {
      angular.forEach($scope.mailFilters, function(filter) {
        // root folder
        angular.forEach($scope.mailFolders, function(mailFolder) {
          if (!$scope.rootFolderMailFilters.hasOwnProperty(mailFolder.topiaId)) {
            $scope.rootFolderMailFilters[mailFolder.topiaId] = [];
          }
          angular.forEach($scope.flatMailFolders[mailFolder.topiaId], function(subFolder) {
            if (subFolder.topiaId == filter.mailFolder.topiaId) {
              filter.mailFolder = subFolder;
  
              $scope.rootFolderMailFilters[subFolder.$rootFolder.topiaId].push(filter);
            }
          });
        });
      });
    };
    
    $scope.updateMetaFilter = function(news, olds) {
      $scope.rootFolderMailFilters = {};
      _updateMetaFilter();
    };

    // update when root folder collection changes (and for init)
    $scope.$watchCollection("mailFolders", $scope.updateMetaFilter);
    
    // filtre qui supprime les items déjà present dans une collection donnée
    $scope.filterByAlreadyInCollection = function(coll) {
      return function(item) {
        return coll && !coll.containsByTopiaId(item);
      }
    };
}]);

/**
 * Misc tab controller.
 */
ConfigurationModule.controller('ConfigurationMiscController', ['$scope', '$window', 'ConfigurationData',
  function($scope, $window, ConfigurationData) {

    // Ajout d'une nouvelle ligne vide d'extension
    $scope.addExtensionRow = function() {
      if (!$scope.configuration.extensionCommands) {
        $scope.configuration.extensionCommands = [];
      }
      var extensionName = $window.prompt("Nouvelle extension");
      
      if (extensionName) {
        extensionName = extensionName.toLowerCase();
        var extension = {extension : extensionName};

        // check if already exists
        if ($scope.configuration.extensionCommands.indexOfBy('extension', extension) != -1) {
          $window.alert("Cette extension existe déjà !");
        } else {
          $scope.configuration.extensionCommands.push(extension);
        }
      }
    };
    
    // suppression d'un ligne
    $scope.deleteExtensionRow = function(index) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer cette extension ?")) {
        $scope.configuration.extensionCommands.splice(index, 1);
      }
    };
}]);

/**
 * Etat attente tab controller.
 */
ConfigurationModule.controller('ConfigurationWaitingStateController', ['$scope', '$window', 'ConfigurationData',
  function($scope, $window, ConfigurationData) {
    //{Map} Usage des états d'attente
    $scope.waitingStatesUsage = ConfigurationData.waitingStatesUsage;
    //{Object} etat d'attente selectionné
    $scope.selectedWaitingState;

    // edition de l'etat d'attent cliqué
    $scope.editWaitingState = function(waitingState) {
      $scope.selectedWaitingState = waitingState;

      // initialize le tableau d'action si vide
      if (!$scope.selectedWaitingState.invalidFormDisabledActions) {
        $scope.selectedWaitingState.invalidFormDisabledActions = [];
      }
      if (!$scope.selectedWaitingState.validFormDisabledActions) {
        $scope.selectedWaitingState.validFormDisabledActions = [];
      }
    };

    // ajout d'un nouvel etat d'attente
    $scope.newWaitingState = function() {
      var label = $window.prompt("Nom du nouvel état d'attente ?");
      if (label) {
        // auto select new etat
        var newWaitingState = {
            topiaId : "new_" + guid(),
            label: label
        };
        
        // check if already exists
        if ($scope.waitingStates.indexOfBy('label', newWaitingState) != -1) {
          $window.alert("Cet état d'attente existe déjà !");
        } else {
          $scope.waitingStates.push(newWaitingState);
          
          // auto select
          $scope.editWaitingState(newWaitingState);
        }
      }
    };

    // suppression de cet etat d'attente dans les mail folder (sinon, ca fait
    // un resauvegarde d'un etat d'attente supprimé et donc un bug)
    var removeWaitingStateFromMailFolder = function(waitingState, mailFolders) {
      if (mailFolders) {
        angular.forEach(mailFolders, function(mailFolder) {
          var index = mailFolder.waitingStates.indexOfByTopiaId(waitingState);
          if (index != -1) {
            mailFolder.waitingStates.splice(index, 1);
          }
          removeWaitingStateFromMailFolder(waitingState, mailFolder.children);
        });
      }
    };

    // suppression d'un etat d'attente non utilisé
    $scope.deleteWaitingState = function(waitingState, index) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer cet état d'attente ?")) {
        $scope.waitingStates.splice(index, 1);
        
        // suppression de cet etat d'attente dans les mail folder (sinon, ca fait
        // un resauvegarde d'un etat d'attente supprimé et donc un bug)
        removeWaitingStateFromMailFolder(waitingState, $scope.mailFolders);
      }
    };

    // selection/deselection d'une action
    $scope.changeWaitingStateValidAction = function(action) {
      var index = $scope.selectedWaitingState.validFormDisabledActions.indexOf(action);
      if (index != -1) {
        $scope.selectedWaitingState.validFormDisabledActions.splice(index, 1);
      } else {
        $scope.selectedWaitingState.validFormDisabledActions.push(action);
      }
    };
    
    // selection/deselection d'une action
    $scope.changeWaitingStateInvalidAction = function(action) {
      var index = $scope.selectedWaitingState.invalidFormDisabledActions.indexOf(action);
      if (index != -1) {
        $scope.selectedWaitingState.invalidFormDisabledActions.splice(index, 1);
      } else {
        $scope.selectedWaitingState.invalidFormDisabledActions.push(action);
      }
    };
}]);


/**
 * Demand type tab controller.
 */
ConfigurationModule.controller('ConfigurationDemandTypeController', ['$scope', '$window', 'ConfigurationData',
  function($scope, $window, ConfigurationData) {
    //{Object} demand type selectionné
    $scope.selectedDemandType;

    // edition d'un type de demande
    $scope.editDemandType = function(demandType) {
      $scope.selectedDemandType = demandType;

      // initialize le tableau d'action si vide
      if (!$scope.selectedDemandType.requiredFields) {
        $scope.selectedDemandType.requiredFields = [];
      }
    };

    // ajout d'un nouveau type de demande
    $scope.newDemandType = function() {
      var label = $window.prompt("Nom du nouveau type de document ?");
      if (label) {
        // auto select new etat
        var newDemandType = {
            topiaId : "new_" + guid(),
            label: label
        };

        // check if already exists
        if ($scope.demandTypes.indexOfBy('label', newDemandType) != -1) {
          $window.alert("Ce type de demande existe déjà !");
        } else {
          $scope.demandTypes.push(newDemandType);
          
          // auto select
          $scope.editDemandType(newDemandType);
        }
      }
    };

    // selection/deselection d'un champ
    $scope.changeDemandTypeField = function(mailField) {

      var index = $scope.selectedDemandType.requiredFields.indexOf(mailField);
      if (index != -1) {
        $scope.selectedDemandType.requiredFields.splice(index, 1);
      } else {
        $scope.selectedDemandType.requiredFields.push(mailField);
      }
    };
}]);

/**
 * Stamps tab controller.
 */
ConfigurationModule.controller('ConfigurationStampsController', ['$scope', '$window', 'ConfigurationData',
  function($scope, $window, ConfigurationData) {
    //{Object} tampon selectionné
    $scope.selectedStamp;

    for (var i = 0 ; i < $scope.stamps.length ; i++) {
      var stamp = $scope.stamps[i];
      stamp.isImageType = stamp.image != null;
      stamp.oldImage = stamp.image;
      stamp.oldText = stamp.text;
    }

    $scope.$watch("selectedStamp.isImageType", function(newValue) {
      if ($scope.selectedStamp) {
        if (newValue) {
          $scope.selectedStamp.text = null;
          $scope.selectedStamp.image = $scope.selectedStamp.oldImage;

        } else {
          $scope.selectedStamp.image = null;
          $scope.selectedStamp.text = $scope.selectedStamp.oldText;
        }
      }
    });

    // edition d'un tampon
    $scope.editStamp = function(stamp) {

      $scope.selectedStamp = stamp;

      if (!$scope.selectedStamp.users) {
        $scope.selectedStamp.users = [];
      }
      if (!$scope.selectedStamp.groups) {
        $scope.selectedStamp.groups = [];
      }

      $('#stampImage').val(null);
      $('#stampPreview').attr('src', stamp.image ? stamp.image : null);

    };

    // ajout d'un nouveau tampon
    $scope.newStamp = function() {
      var label = $window.prompt("Nom du nouveau tampon ?");
      if (label) {
        // auto select new etat
        var newStamp = {
            topiaId : "new_" + guid(),
            label: label,
            users: [],
            groups: []
        };

        // check if already exists
        if ($scope.stamps.indexOfBy('label', newStamp) != -1) {
          $window.alert("Ce tampon existe déjà !");

        } else {
          $scope.stamps.push(newStamp);

          // auto select
          $scope.editStamp(newStamp);
        }
      }
    };

    // suppression d'un tampon
    $scope.deleteStamp = function(stamp, index) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer ce tampon ?")) {
        $scope.stamps.splice(index, 1);
        $scope.selectedStamp = null;
      }
    };

    $scope.imageChanged = function(input) {
      var f = input.files[0]; // FileList object

      // Only process image files.
      if (!f.type.match('image.*')) {
        $window.alert("Erreur lors du chargement de l'image");
        $('#stampPreview').attr('src', null);
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onloadend = function() {

        $scope.$apply(function() {
          $scope.selectedStamp.image = reader.result;
          $scope.selectedStamp.oldImage = reader.result;
        });
        $('#stampPreview').attr('src', reader.result);
      };

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }

    // add user stamps right
    $scope.addStampUser = function() {
      $scope.selectedStamp.users.push($scope.newStampUser);
      delete $scope.newStampUser;
    };

    // remove user stamps right
    $scope.removeStampUser = function(index, user) {
      if ($window.confirm("Êtes-vous sur de vouloir supprimer les droits de cet utilisateur ?")) {
        $scope.selectedStamp.users.splice(index, 1);
      }
    };

    // add group stamps right
    $scope.addStampGroup = function() {
      $scope.selectedStamp.groups.push($scope.newStampGroup);
      delete $scope.newStampGroup;
    };

    // remove group stamps right
    $scope.removeStampGroup = function(index, group) {
      if ($window.confirm("Êtes-vous sur de vouloir supprimer les droits de ce groupe ?")) {
        $scope.selectedStamp.groups.splice(index, 1);
      }
    };

    $scope.onSelectStampUserCallback = function (item, model) {
      $scope.newStampUser = item;
    };

    $scope.onSelectStampGroupCallback = function (item, model) {
      $scope.newStampGroup = item;
    };

}]);


/**
 * Mail folder tab controller.
 */
ConfigurationModule.controller('ConfigurationTreeController', ['$scope', '$window', '$http', 'ConfigurationData',
  function($scope, $window, $http, ConfigurationData) {
    // {Object} selected mail folder
    $scope.selectedMailFolder;
    // {String} add new customer responsible form value
    $scope.newCustomerResponsible;
    // {String} add new customer responsible form value
    $scope.newReplyAddress;
    // {String} add new domain form value
    $scope.newReplyDomain;
    // {Array} Colonnes selectionnées pour le dossier courant
    $scope.folderTableColumns;
    // {Array} Colonnes selectionnées pour les dossiers parents
    $scope.parentFolderTableColumns;
    // {Object} Global parent wide configuration and values
    $scope.parentScopeValues;

    // creation d'un nouveau noeud racine
    $scope.newRoot = function() {
      var name = $window.prompt("Nom du nouveau dossier racine ?");
      if (name) {
        var newRoot = {
          topiaId: "new_" + guid(),
          name: name,
          children: [],
          useCurrentLevelWaitingState: true,
          useCurrentLevelTableColumns: true,
          useCurrentLevelFaxDomain: true,
          useCurrentLevelEdiFolder: true,
          useCurrentLevelRejectResponseMessage: true,
          useCurrentLevelRejectResponseMailAddress: true,
          useCurrentLevelCompany: true,
          useCurrentLevelNbElementToDisplay: true,
          archiveFolder: false
        };
        
        // check if already exists
        if ($scope.mailFolders.indexOfBy('name', newRoot) != -1) {
          $window.alert("Un noeud de même nom existe déjà à ce niveau !");
        } else {
          // add node
          $scope.mailFolders.push(newRoot);
          // update flat map
          $scope._updateFlatMailFolders();
          // edition automatique
          $scope.editMailFolder(newRoot);
        }
      };
    };

    // new sub folder
    $scope.newSubFolder = function(scope) {
      var nodeData = scope.$modelValue;
      var name = $window.prompt("Nom du nouveau noeud ? ");
      if (name) {
        var newNode = {
            topiaId: "new_" + guid(),
            name: name,
            children: [],
            useCurrentLevelWaitingState: false,
            useCurrentLevelTableColumns: false,
            useCurrentLevelFaxDomain: false,
            useCurrentLevelEdiFolder: false,
            useCurrentLevelRejectResponseMessage: false,
            useCurrentLevelRejectResponseMailAddress: false,
            useCurrentLevelCompany: false,
            useCurrentLevelNbElementToDisplay: false,
            archiveFolder: false
          };
        
        // check if already exists
        if (nodeData.children.indexOfBy('name', newNode) != -1) {
          $window.alert("Un noeud de même nom existe déjà à ce niveau !");
        } else {
          // extend node (in faxtomail collapsed = extended)
          scope.collapse();
          // append new node
          nodeData.children.push(newNode);
          // update flat map
          $scope._updateFlatMailFolders();
          // edition automatique
          $scope.editMailFolder(newNode);
        }
      }
    };

    // new sub folder
    $scope.newArchiveSubFolder = function(scope) {
      var nodeData = scope.$modelValue;
      if (!nodeData.hasArchiveFolder) {
        var name = "Archives du dossier " + nodeData.name;
        if (name) {
          var newNode = {
              topiaId: "new_" + guid(),
              name: name,
              children: [],
              useCurrentLevelWaitingState: false,
              useCurrentLevelTableColumns: false,
              useCurrentLevelFaxDomain: false,
              useCurrentLevelEdiFolder: false,
              useCurrentLevelRejectResponseMessage: false,
              useCurrentLevelRejectResponseMailAddress: false,
              useCurrentLevelCompany: false,
              useCurrentLevelNbElementToDisplay: false,
              useCurrentLevelDemandType: false,
              useCurrentLevelRange: false,
              archiveFolder: true
            };
          
          // check if already exists
          if (nodeData.children.indexOfBy('name', newNode) != -1) {
            $window.alert("Un noeud de même nom existe déjà à ce niveau !");
          } else {
            // extend node (in faxtomail collapsed = extended)
            scope.collapse();
            // append new node
            nodeData.hasArchiveFolder = true;
            nodeData.children.push(newNode);
            // update flat map
            $scope._updateFlatMailFolders();
            // edition automatique
            $scope.editMailFolder(newNode);
          }
        }
      }
    };

    // suppression d'un dossier
    $scope.deleteFolder = function(scope) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer ce dossier ?")) {
        
        var nodeData = scope.$modelValue;

        // suppression des filtres utilisant ce dossier comme cible
        var index = -1;
        var current = 0;
        angular.forEach($scope.mailFilters, function(filter) {
          if (filter.mailFolder.topiaId == nodeData.topiaId) {
            index = current;
          }
          current++;
        });
        if (index != -1) {
          $scope.mailFilters.splice(index, 1);
        }

        // suppression de la zone d'edition (si necessaire)
        if ($scope.selectedMailFolder == nodeData) {
          delete $scope.selectedMailFolder;
        }

        // remove current folder
        if (nodeData.archiveFolder) {
          nodeData.$parent.hasArchiveFolder = false;
        }
        scope.remove(scope);
        // update filter meta map
        $scope.updateMetaFilter();
      }
    };

    // edit mail folder
    $scope.editMailFolder = function(mailFolder) {
      $('#acknowledgementSigning').val(null);
      $('#acknowledgementSigningPreview').attr('src', null);
      $('#rejectResponseSigning').val(null);
      $('#rejectResponseSigningPreview').attr('src', null);

      $scope.selectedMailFolder = mailFolder;
      $scope.editingAcknowledgementMessage = mailFolder.acknowledgementMessage;

      // edit undefined collections
      if (!$scope.selectedMailFolder.customerResponsibles) {
        $scope.selectedMailFolder.customerResponsibles = [];
      }
      if (!$scope.selectedMailFolder.replyAddresses) {
        $scope.selectedMailFolder.replyAddresses = [];
      }
      if (!$scope.selectedMailFolder.replyDomains) {
        $scope.selectedMailFolder.replyDomains = [];
      }
      if (!$scope.selectedMailFolder.folderTableColumns) {
        $scope.selectedMailFolder.folderTableColumns = [];
      }
      if (!$scope.selectedMailFolder.waitingStates) {
        $scope.selectedMailFolder.waitingStates = [];
      }
      if (!$scope.selectedMailFolder.demandTypes) {
        $scope.selectedMailFolder.demandTypes = [];
      }
      if (!$scope.selectedMailFolder.ranges) {
        $scope.selectedMailFolder.ranges = [];
      }
      if (!$scope.selectedMailFolder.readRightUsers) {
        $scope.selectedMailFolder.readRightUsers = [];
      }
      if (!$scope.selectedMailFolder.writeRightUsers) {
        $scope.selectedMailFolder.writeRightUsers = [];
      }
      if (!$scope.selectedMailFolder.moveRightUsers) {
        $scope.selectedMailFolder.moveRightUsers = [];
      }
      if (!$scope.selectedMailFolder.readRightGroups) {
        $scope.selectedMailFolder.readRightGroups = [];
      }
      if (!$scope.selectedMailFolder.writeRightGroups) {
        $scope.selectedMailFolder.writeRightGroups = [];
      }
      if (!$scope.selectedMailFolder.moveRightGroups) {
        $scope.selectedMailFolder.moveRightGroups = [];
      }

      // parse acknowledgement exception emailaddresses
      angular.forEach($scope.selectedMailFolder.acknowledgementException, function(acknowledgementException) {
        acknowledgementException.emailAddresses = JSON.parse(acknowledgementException.emailAddressesJson).toString();
      });

      // initialise la liste des colonnes pour le dossier courant
      $scope.folderTableColumns = [];
      angular.forEach($scope.selectedMailFolder.folderTableColumns, function(folderTableColumn) {
        $scope.folderTableColumns.push({
          id: folderTableColumn,
          label: $scope.tableMailFields[folderTableColumn]
        });
      });
      
      // right
      $scope.selectedMailFolder.rightUsers = [];
      $scope.selectedMailFolder.rightGroups = [];
      angular.forEach($scope.selectedMailFolder.readRightUsers, function(user) {
        if ($scope.selectedMailFolder.rightUsers.indexOfByTopiaId(user) == -1) {
          $scope.selectedMailFolder.rightUsers.push(user);
        }
      });
      angular.forEach($scope.selectedMailFolder.writeRightUsers, function(user) {
        if ($scope.selectedMailFolder.rightUsers.indexOfByTopiaId(user) == -1) {
          $scope.selectedMailFolder.rightUsers.push(user);
        }
      });
      angular.forEach($scope.selectedMailFolder.moveRightUsers, function(user) {
        if ($scope.selectedMailFolder.rightUsers.indexOfByTopiaId(user) == -1) {
          $scope.selectedMailFolder.rightUsers.push(user);
        }
      });
      angular.forEach($scope.selectedMailFolder.readRightGroups, function(group) {
        if ($scope.selectedMailFolder.rightGroups.indexOfByTopiaId(group) == -1) {
          $scope.selectedMailFolder.rightGroups.push(group);
        }
      });
      angular.forEach($scope.selectedMailFolder.writeRightGroups, function(group) {
        if ($scope.selectedMailFolder.rightGroups.indexOfByTopiaId(group) == -1) {
          $scope.selectedMailFolder.rightGroups.push(group);
        }
      });
      angular.forEach($scope.selectedMailFolder.moveRightGroups, function(group) {
        if ($scope.selectedMailFolder.rightGroups.indexOfByTopiaId(group) == -1) {
          $scope.selectedMailFolder.rightGroups.push(group);
        }
      });
    };

    // on doit faire ca par l'ordre des watch angular et ne pas l'appeler dans le edit
    // sinon, les champs $ ne sont pas valués
    $scope.$watch("selectedMailFolder", function(newValue, oldValue) {
      if (newValue) {
        // initialise default values pour le noeud racines
        if (!$scope.selectedMailFolder.$parent) {
          $scope.selectedMailFolder.allowCreateDemandIntoFolder = $scope.selectedMailFolder.allowCreateDemandIntoFolder || false;
          $scope.selectedMailFolder.allowMoveDemandIntoFolder = $scope.selectedMailFolder.allowMoveDemandIntoFolder || false;
          $scope.selectedMailFolder.colorizeInvalidDemands = $scope.selectedMailFolder.colorizeInvalidDemands || false;
          $scope.selectedMailFolder.openAttachmentReportNoTaken = $scope.selectedMailFolder.openAttachmentReportNoTaken || false;
          $scope.selectedMailFolder.printActionEqualTakeAction = $scope.selectedMailFolder.printActionEqualTakeAction || false;
          $scope.selectedMailFolder.printActionEqualTakeOnlyIfNotTaken = $scope.selectedMailFolder.printActionEqualTakeOnlyIfNotTaken || false;
          $scope.selectedMailFolder.lockedDemandsOpenableInReadOnly = $scope.selectedMailFolder.lockedDemandsOpenableInReadOnly || false;
          $scope.selectedMailFolder.canViewOriginalAttachments = $scope.selectedMailFolder.canViewOriginalAttachments || false;
          $scope.selectedMailFolder.mustTakeToEditDemand = $scope.selectedMailFolder.mustTakeToEditDemand || false;
          $scope.selectedMailFolder.displayHelpOnMessages = $scope.selectedMailFolder.displayHelpOnMessages || false;
          $scope.selectedMailFolder.computeQuantitiesSubtotalsByState = $scope.selectedMailFolder.computeQuantitiesSubtotalsByState || false;
          $scope.selectedMailFolder.showReplyAction = $scope.selectedMailFolder.showReplyAction || false;
          $scope.selectedMailFolder.showForwardAction = $scope.selectedMailFolder.showForwardAction || false;
          $scope.selectedMailFolder.showTakenByAction = $scope.selectedMailFolder.showTakenByAction || false;
          $scope.selectedMailFolder.ediTransfer = $scope.selectedMailFolder.ediTransfer || false;
          $scope.selectedMailFolder.displayOnlyUserTrigraphInTables = $scope.selectedMailFolder.displayOnlyUserTrigraphInTables || false;
          $scope.selectedMailFolder.canChangeOrderInTable = $scope.selectedMailFolder.canChangeOrderInTable || false;
        }
        
        // initialisation des valeurs du parent
        $scope.parentScopeValues = {
          replyAddresses: [],
          replyDomains: [],
          folderTableColumns: [],
          waitingStates: [],
          demandTypes: [],
          stamps: [],
          ranges: [],
          //rigths
          readRightUsers: [],
          readRightGroups: [],
          writeRightUsers: [],
          writeRightGroups: [],
          moveRightUsers: [],
          moveRightGroups: []
        };
        updateParentScopeValues($scope.selectedMailFolder.$parent);
        
        // init select 2 data for parent
        if ($scope.parentScopeValues.folderTableColumns) {
          $scope.parentFolderTableColumns = [];
          angular.forEach($scope.parentScopeValues.folderTableColumns, function(folderTableColumn) {
            $scope.parentFolderTableColumns.push({
              id: folderTableColumn,
              label: $scope.tableMailFields[folderTableColumn]
            });
          });
        }

        // parent rigths
        $scope.parentScopeValues.rightUsers = [];
        $scope.parentScopeValues.rightGroups = [];
        angular.forEach($scope.parentScopeValues.readRightUsers, function(user) {
          if ($scope.parentScopeValues.rightUsers.indexOfByTopiaId(user) == -1) {
            $scope.parentScopeValues.rightUsers.push(user);
          }
        });
        angular.forEach($scope.parentScopeValues.writeRightUsers, function(user) {
          if ($scope.parentScopeValues.rightUsers.indexOfByTopiaId(user) == -1) {
            $scope.parentScopeValues.rightUsers.push(user);
          }
        });
        angular.forEach($scope.parentScopeValues.moveRightUsers, function(user) {
          if ($scope.parentScopeValues.rightUsers.indexOfByTopiaId(user) == -1) {
            $scope.parentScopeValues.rightUsers.push(user);
          }
        });
        angular.forEach($scope.parentScopeValues.readRightGroups, function(group) {
          if ($scope.parentScopeValues.rightGroups.indexOfByTopiaId(group) == -1) {
            $scope.parentScopeValues.rightGroups.push(group);
          }
        });
        angular.forEach($scope.parentScopeValues.writeRightGroups, function(group) {
          if ($scope.parentScopeValues.rightGroups.indexOfByTopiaId(group) == -1) {
            $scope.parentScopeValues.rightGroups.push(group);
          }
        });
        angular.forEach($scope.parentScopeValues.moveRightGroups, function(group) {
          if ($scope.parentScopeValues.rightGroups.indexOfByTopiaId(group) == -1) {
            $scope.parentScopeValues.rightGroups.push(group);
          }
        });
      }
    });

    var updateParentScopeValues = function(folder) {
      if (folder) {

        // move to parent first to keed parent order for collection
        if (folder.$parent) {
          updateParentScopeValues(folder.$parent);
        };

        // take current folder first before parent values (warn about 'undefined' and 'false')
        if (angular.isUndefined($scope.parentScopeValues.allowCreateDemandIntoFolder)) {
          $scope.parentScopeValues.allowCreateDemandIntoFolder = folder.allowCreateDemandIntoFolder;
        }
        if (angular.isUndefined($scope.parentScopeValues.allowMoveDemandIntoFolder)) {
          $scope.parentScopeValues.allowMoveDemandIntoFolder = folder.allowMoveDemandIntoFolder;
        }
        if (angular.isUndefined($scope.parentScopeValues.colorizeInvalidDemands)) {
          $scope.parentScopeValues.colorizeInvalidDemands = folder.colorizeInvalidDemands;
        }
        if (angular.isUndefined($scope.parentScopeValues.openAttachmentReportNoTaken)) {
          $scope.parentScopeValues.openAttachmentReportNoTaken = folder.openAttachmentReportNoTaken;
        }
        if (angular.isUndefined($scope.parentScopeValues.printActionEqualTakeAction)) {
          $scope.parentScopeValues.printActionEqualTakeAction = folder.printActionEqualTakeAction;
        }
        if (angular.isUndefined($scope.parentScopeValues.printActionEqualTakeOnlyIfNotTaken)) {
          $scope.parentScopeValues.printActionEqualTakeOnlyIfNotTaken = folder.printActionEqualTakeOnlyIfNotTaken;
        }
        if (angular.isUndefined($scope.parentScopeValues.printInlineAttachments)) {
          $scope.parentScopeValues.printInlineAttachments = folder.printInlineAttachments;
        }
        if (angular.isUndefined($scope.parentScopeValues.lockedDemandsOpenableInReadOnly)) {
          $scope.parentScopeValues.lockedDemandsOpenableInReadOnly = folder.lockedDemandsOpenableInReadOnly;
        }
        if (angular.isUndefined($scope.parentScopeValues.canViewOriginalAttachments)) {
          $scope.parentScopeValues.canViewOriginalAttachments = folder.canViewOriginalAttachments;
        }
        if (angular.isUndefined($scope.parentScopeValues.mailContentWithInlineAttachments)) {
          $scope.parentScopeValues.mailContentWithInlineAttachments = folder.mailContentWithInlineAttachments;
        }
        if (angular.isUndefined($scope.parentScopeValues.showAttachmentPreview)) {
          $scope.parentScopeValues.showAttachmentPreview = folder.showAttachmentPreview;
        }
        if (angular.isUndefined($scope.parentScopeValues.mustTakeToEditDemand)) {
          $scope.parentScopeValues.mustTakeToEditDemand = folder.mustTakeToEditDemand;
        }
        if (angular.isUndefined($scope.parentScopeValues.displayHelpOnMessages)) {
          $scope.parentScopeValues.displayHelpOnMessages = folder.displayHelpOnMessages;
        }
        if (angular.isUndefined($scope.parentScopeValues.computeQuantitiesSubtotalsByState)) {
          $scope.parentScopeValues.computeQuantitiesSubtotalsByState = folder.computeQuantitiesSubtotalsByState;
        }
        if (angular.isUndefined($scope.parentScopeValues.showReplyAction)) {
          $scope.parentScopeValues.showReplyAction = folder.showReplyAction;
        }
        if (angular.isUndefined($scope.parentScopeValues.showForwardAction)) {
          $scope.parentScopeValues.showForwardAction = folder.showForwardAction;
        }
        if (angular.isUndefined($scope.parentScopeValues.showTakenByAction)) {
          $scope.parentScopeValues.showTakenByAction = folder.showTakenByAction;
        }
        if (angular.isUndefined($scope.parentScopeValues.ediTransfer)) {
          $scope.parentScopeValues.ediTransfer = folder.ediTransfer;
        }
        if (!$scope.parentScopeValues.faxDomain) {
          $scope.parentScopeValues.faxDomain = folder.faxDomain;
        }
        if (!$scope.parentScopeValues.ediFolder) {
          $scope.parentScopeValues.ediFolder = folder.ediFolder;
        }
        if (angular.isUndefined($scope.parentScopeValues.rejectResponseMailAddress)) {
          $scope.parentScopeValues.rejectResponseMailAddress = folder.rejectResponseMailAddress;
        }
        if (angular.isUndefined($scope.parentScopeValues.rejectResponseMessage)) {
          $scope.parentScopeValues.rejectResponseMessage= folder.rejectResponseMessage;
        }
        if (angular.isUndefined($scope.parentScopeValues.nbElementToDisplay)) {
          $scope.parentScopeValues.nbElementToDisplay= folder.nbElementToDisplay;
        }
        if (angular.isUndefined($scope.parentScopeValues.company)) {
          $scope.parentScopeValues.company = folder.company;
        }
        if ((!$scope.parentScopeValues.folderTableColumns || $scope.parentScopeValues.folderTableColumns.length == 0) && folder.folderTableColumns) {
          $scope.parentScopeValues.folderTableColumns = folder.folderTableColumns;
        }
        if (angular.isUndefined($scope.parentScopeValues.displayOnlyUserTrigraphInTables)) {
          $scope.parentScopeValues.displayOnlyUserTrigraphInTables = folder.displayOnlyUserTrigraphInTables;
        }
        if (angular.isUndefined($scope.parentScopeValues.canChangeOrderInTable)) {
          $scope.parentScopeValues.canChangeOrderInTable = folder.canChangeOrderInTable;
        }
        if ((!$scope.parentScopeValues.waitingStates || $scope.parentScopeValues.waitingStates.length == 0) && folder.waitingStates) {
          $scope.parentScopeValues.waitingStates = folder.waitingStates;
        }
        if ((!$scope.parentScopeValues.demandTypes || $scope.parentScopeValues.demandTypes.length == 0) && folder.demandTypes) {
          $scope.parentScopeValues.demandTypes = folder.demandTypes;
        }
        if ((!$scope.parentScopeValues.ranges || $scope.parentScopeValues.ranges.length == 0) && folder.ranges) {
          $scope.parentScopeValues.ranges = folder.ranges;
        }
        if ((!$scope.parentScopeValues.invalidFormDisabledActions || $scope.parentScopeValues.invalidFormDisabledActions.length == 0) && folder.invalidFormDisabledActions) {
          $scope.parentScopeValues.invalidFormDisabledActions = folder.invalidFormDisabledActions;
        }

        // parents values
        if (folder.replyAddresses) {
          $scope.parentScopeValues.replyAddresses = $scope.parentScopeValues.replyAddresses.concat(folder.replyAddresses);
        }
        if (folder.replyDomains) {
          $scope.parentScopeValues.replyDomains = $scope.parentScopeValues.replyDomains.concat(folder.replyDomains);
        }
        // manage rigths
        if (folder.readRightUsers) {
          $scope.parentScopeValues.readRightUsers = $scope.parentScopeValues.readRightUsers.concat(folder.readRightUsers);
        }
        if (folder.readRightGroups) {
          $scope.parentScopeValues.readRightGroups = $scope.parentScopeValues.readRightGroups.concat(folder.readRightGroups);
        }
        if (folder.writeRightUsers) {
          $scope.parentScopeValues.writeRightUsers = $scope.parentScopeValues.writeRightUsers.concat(folder.writeRightUsers);
        }
        if (folder.writeRightGroups) {
          $scope.parentScopeValues.writeRightGroups = $scope.parentScopeValues.writeRightGroups.concat(folder.writeRightGroups);
        }
        if (folder.moveRightUsers) {
          $scope.parentScopeValues.moveRightUsers = $scope.parentScopeValues.moveRightUsers.concat(folder.moveRightUsers);
        }
        if (folder.moveRightGroups) {
          $scope.parentScopeValues.moveRightGroups = $scope.parentScopeValues.moveRightGroups.concat(folder.moveRightGroups);
        }
      }
    };

    // acknowledgement message
    // force apply on blur of the rich editor, otherwise the data do not update
    $scope.trixBlur = function() {
      $scope.$apply();
    };

    // acknowledgement exceptions
    $scope.newAcknowledgementException = null;
    $scope.matchingClients = [];

    $scope.loadClients = function(select) {
      var search = select.search;
      if (search && search.length > 1) {
        $http.post(ConfigurationData.remoteSearchClients, "clientQuery=" + search, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
              .success(function(data, status, headers, config) {
                  for (var i = 0 ; i < data.length ; i++) {
                    var client = data[i];
                    if (client.emailAddressesJson) {
                      client.emailAddresses = JSON.parse(client.emailAddressesJson).toString();
                    }
                  }
                  $scope.matchingClients = data;
              });
       }
    };

    $scope.onSelectCallback = function (item, model) {
      $scope.newAcknowledgementException = item;
    };

    // vérifie sur le serveur que le répertoire existe et a les droits d'écriture
    $scope.checkRemotePath = function() {
      $http.get(ConfigurationData.remoteCheckFolder +
          "?path=" + encodeURIComponent($scope.selectedMailFolder.ediFolder), {cache:true})
          .success(function(data, status, headers, config) {
            if (data.isDirectory) {
              if (data.canWrite) {
                $window.alert("Le dossier existe et dispose des droits d'écriture.");
              } else {
                $window.alert("Le dossier existe, mais ne dispose pas des droits d'écriture !");
              }
            } else {
              $window.alert("Le dossier n'existe pas !");
            }
          });
    };

    // utilisé pour mettre à jour $scope.selectedMailFolder.folderTableColumns si $scope.folderTableColumns change
    $scope.$watch("folderTableColumns", function(newValue, oldValue) {
      if (newValue != oldValue) {
        $scope.selectedMailFolder.folderTableColumns = [];
        angular.forEach(newValue, function(elem) {
          $scope.selectedMailFolder.folderTableColumns.push(elem.id);
        });
      }
    }, true);

    // fonction retournant l'ensemble des options disponibles
    $scope.getObjectsData = function(term, result) {
      var resultArray = [];
      angular.forEach($scope.tableMailFields, function(label, field) {
        resultArray.push({
          id: field,
          label: label
        });
      });
      result(resultArray);
    };

    // recursive find folder where user is already responsible
    var checkUserResponsible = function(folders, user) {
      var result;
      angular.forEach(folders, function(folder) {
        if (folder.customerResponsibles) {
          angular.forEach(folder.customerResponsibles, function(responsible) {
            if (responsible.topiaId == user.topiaId) {
              result = folder;
            }
          });
        }
        if (!result && folder.children) {
          result = checkUserResponsible(folder.children, user);
        }
      });
      return result;
    };

    // add customer responsible
    $scope.addCustomerResponsible = function() {
      
      // check it user is already responsible of another folder
      var otherFolder = checkUserResponsible($scope.mailFolders, $scope.newCustomerResponsible);
      if (angular.isDefined(otherFolder)) {
        if ($window.confirm("Cet utilisateur est déjà reponsable du dossier '" + otherFolder.$fullPath +
            "'. Voulez-vous continuer et changer son dossier de responsabilité ?")) {

          // remove from previous
          var index = otherFolder.customerResponsibles.indexOf($scope.newCustomerResponsible);
          otherFolder.customerResponsibles.splice(index, 1);

          // add to current
          $scope.selectedMailFolder.customerResponsibles.push($scope.newCustomerResponsible);
          delete $scope.newCustomerResponsible;
        }
      } else {
        $scope.selectedMailFolder.customerResponsibles.push($scope.newCustomerResponsible);
        delete $scope.newCustomerResponsible;
      }
    };

    $scope.onSelectCustomerResponsibleCallback = function (item, model) {
      $scope.newCustomerResponsible = item;
    };

    // remove customer responsible
    $scope.removeCustomerResponsible = function(index) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer ce chargé de clientèle ?")) {
        $scope.selectedMailFolder.customerResponsibles.splice(index, 1);
      }
    };
    
    // add reply address
    $scope.addReplyAddress = function() {
      // check if already exists
      if ($scope.selectedMailFolder.replyAddresses.indexOf($scope.newReplyAddress) != -1) {
        $window.alert("Cette adresse existe déjà !");
      } else {
        $scope.selectedMailFolder.replyAddresses.push($scope.newReplyAddress);
        delete $scope.newReplyAddress;
        $scope.addAddressForm.$setPristine();
      }
    };

    // remove customer responsible
    $scope.removeReplyAddress = function(index) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer cette adresse ?")) {
        $scope.selectedMailFolder.replyAddresses.splice(index, 1);
      }
    };
    
    // add reply domain
    $scope.addReplyDomain = function() {
      // check if already exists
      if ($scope.selectedMailFolder.replyDomains.indexOf($scope.newReplyDomain) != -1) {
        $window.alert("Ce domaine existe déjà !");
      } else if ($scope.newReplyDomain.indexOf("@") != -1) {
        $window.alert("Le nom de domaine ne doit pas contenir le symbole @ !");
      } else {
        $scope.selectedMailFolder.replyDomains.push($scope.newReplyDomain);
        delete $scope.newReplyDomain;
        $scope.addDomainForm.$setPristine();
      }
    };

    // remove customer responsible
    $scope.removeReplyDomain = function(index) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer ce domaine ?")) {
        $scope.selectedMailFolder.replyDomains.splice(index, 1);
      }
    };

    // initialise la liste des etats d'attente avec celle des niveaux supérieurs
    $scope.initFolderWaitingState = function() {
      if ($scope.selectedMailFolder.useCurrentLevelWaitingState) {
        if ($scope.parentScopeValues.waitingStates) {
          $scope.selectedMailFolder.waitingStates = $scope.parentScopeValues.waitingStates.slice(); // soft copy
        }
      } else {
        delete $scope.selectedMailFolder.waitingStates;
      }
    };

    // selection/deselection d'un état d'attente possible pour ce dossier
    $scope.changeFolderWaitingState = function(waitingState) {
      var index = $scope.selectedMailFolder.waitingStates.indexOfByTopiaId(waitingState);
      if (index != -1) {
        $scope.selectedMailFolder.waitingStates.splice(index, 1);
      } else {
        $scope.selectedMailFolder.waitingStates.push(waitingState);
      }
    };
    
    // initialise la liste des types de demandes avec celle des niveaux supérieurs
    $scope.initFolderDemandTypes = function() {
      if ($scope.selectedMailFolder.useCurrentLevelDemandType) {
        if ($scope.parentScopeValues.demandTypes) {
          $scope.selectedMailFolder.demandTypes = $scope.parentScopeValues.demandTypes.slice(); // soft copy
        }
      } else {
        delete $scope.selectedMailFolder.demandTypes;
      }
    };

    // selection/deselection d'un type de demande possible pour ce dossier
    $scope.changeFolderDemandType = function(demandType) {
      var index = $scope.selectedMailFolder.demandTypes.indexOfByTopiaId(demandType);
      if (index != -1) {
        $scope.selectedMailFolder.demandTypes.splice(index, 1);
      } else {
        $scope.selectedMailFolder.demandTypes.push(demandType);
      }
    };

    // initialise la liste des gammes avec celle des niveaux supérieurs
    $scope.initFolderRanges = function() {
      if ($scope.selectedMailFolder.useCurrentLevelRange) {
        if ($scope.parentScopeValues.ranges) {
          $scope.selectedMailFolder.ranges = $scope.parentScopeValues.ranges.slice(); // soft copy
        }
      } else {
        delete $scope.selectedMailFolder.ranges;
      }
    };

    // selection/deselection d'une gamme possible pour ce dossier
    $scope.changeFolderRange = function(range) {
      var index = $scope.selectedMailFolder.ranges.indexOfByTopiaId(range);
      if (index != -1) {
        $scope.selectedMailFolder.ranges.splice(index, 1);
      } else {
        $scope.selectedMailFolder.ranges.push(range);
      }
    };

    // initialise la liste des actions avec celle des niveaux supérieurs
    $scope.initFolderInvalidFormDisabledActions = function() {
      if ($scope.selectedMailFolder.useCurrentLevelInvalidFormDisabledActions) {
        if ($scope.parentScopeValues.invalidFormDisabledActions) {
          $scope.selectedMailFolder.invalidFormDisabledActions = $scope.parentScopeValues.invalidFormDisabledActions.slice(); // soft copy
        }
      } else {
        delete $scope.selectedMailFolder.invalidFormDisabledActions;
      }
    };

    // selection/deselection d'une action
    $scope.changeInvalidAction = function(action) {
      var index = $scope.selectedMailFolder.invalidFormDisabledActions.indexOf(action);
      if (index != -1) {
        $scope.selectedMailFolder.invalidFormDisabledActions.splice(index, 1);
      } else {
        $scope.selectedMailFolder.invalidFormDisabledActions.push(action);
      }
    };

    // initialise la liste des colonnes sélectionnées avec celle des niveaux supérieurs
    $scope.initFolderColumns = function() {
      if ($scope.selectedMailFolder.useCurrentLevelTableColumns) {
        if ($scope.parentScopeValues.folderTableColumns) {
          $scope.selectedMailFolder.folderTableColumns = $scope.parentScopeValues.folderTableColumns.slice(); // soft copy
          
          // init select2 data
          $scope.folderTableColumns = [];
          angular.forEach($scope.selectedMailFolder.folderTableColumns, function(folderTableColumn) {
            $scope.folderTableColumns.push({
              id: folderTableColumn,
              label: $scope.tableMailFields[folderTableColumn]
            });
          });
        }
      } else {
        delete $scope.selectedMailFolder.folderTableColumns;
      }
    };

    // add user write right
    $scope.addRightUser = function() {
      $scope.selectedMailFolder.rightUsers.push($scope.newRightUser);
      delete $scope.newRightUser;
    };

    // remove user write right
    $scope.removeRightUser = function(index, user) {
      if ($window.confirm("Êtes-vous sur de vouloir supprimer les droits de cet utilisateur ?")) {
        $scope.selectedMailFolder.rightUsers.splice(index, 1);
        
        // suppression des droits réels
        var index = $scope.selectedMailFolder.readRightUsers.indexOfByTopiaId(user);
        if (index != -1) {
          $scope.selectedMailFolder.readRightUsers.splice(index, 1);
        }
        index = $scope.selectedMailFolder.writeRightUsers.indexOfByTopiaId(user);
        if (index != -1) {
          $scope.selectedMailFolder.writeRightUsers.splice(index, 1);
        }
        index = $scope.selectedMailFolder.moveRightUsers.indexOfByTopiaId(user);
        if (index != -1) {
          $scope.selectedMailFolder.moveRightUsers.splice(index, 1);
        }
      }
    };
    
    // add group write right
    $scope.addRightGroup = function() {
      $scope.selectedMailFolder.rightGroups.push($scope.newRightGroup);
      delete $scope.newRightGroup;
    };

    // remove group write right
    $scope.removeRightGroup = function(index, group) {
      if ($window.confirm("Êtes-vous sur de vouloir supprimer les droits de ce groupe ?")) {
        $scope.selectedMailFolder.rightGroups.splice(index, 1);
        
        // suppression des droits réels
        var index = $scope.selectedMailFolder.readRightGroups.indexOfByTopiaId(group);
        if (index != -1) {
          $scope.selectedMailFolder.readRightGroups.splice(index, 1);
        }
        index = $scope.selectedMailFolder.writeRightGroups.indexOfByTopiaId(group);
        if (index != -1) {
          $scope.selectedMailFolder.writeRightGroups.splice(index, 1);
        }
        index = $scope.selectedMailFolder.moveRightGroups.indexOfByTopiaId(group);
        if (index != -1) {
          $scope.selectedMailFolder.moveRightGroups.splice(index, 1);
        }
      }
    };

    $scope.onSelectRightUserCallback = function (item, model) {
      $scope.newRightUser = item;
    };

    $scope.onSelectRightGroupCallback = function (item, model) {
      $scope.newRightGroup = item;
    };

    // change le droit de lecture du groupe
    $scope.changeReadRightGroup = function(group) {
      var index = $scope.selectedMailFolder.readRightGroups.indexOfByTopiaId(group);
      if (index != -1) {
        $scope.selectedMailFolder.readRightGroups.splice(index, 1);
      } else {
        $scope.selectedMailFolder.readRightGroups.push(group);
      }
    };
    // change le droit d'écriture du groupe
    $scope.changeWriteRightGroup = function(group) {
      var index = $scope.selectedMailFolder.writeRightGroups.indexOfByTopiaId(group);
      if (index != -1) {
        $scope.selectedMailFolder.writeRightGroups.splice(index, 1);
      } else {
        // ajout automatique du droit de lecture pour un droit d'ecriture
        if ($scope.selectedMailFolder.readRightGroups.indexOfByTopiaId(group) == -1) {
          $scope.selectedMailFolder.readRightGroups.push(group);
        }
        $scope.selectedMailFolder.writeRightGroups.push(group);
      }
    };
    // change le droit de déplacement du groupe
    $scope.changeMoveRightGroup = function(group) {
      var index = $scope.selectedMailFolder.moveRightGroups.indexOfByTopiaId(group);
      if (index != -1) {
        $scope.selectedMailFolder.moveRightGroups.splice(index, 1);
      } else {
        $scope.selectedMailFolder.moveRightGroups.push(group);
      }
    };
    // change le droit de lecture d'un utilisateur
    $scope.changeReadRightUser = function(user) {
      var index = $scope.selectedMailFolder.readRightUsers.indexOfByTopiaId(user);
      if (index != -1) {
        $scope.selectedMailFolder.readRightUsers.splice(index, 1);
      } else {
        $scope.selectedMailFolder.readRightUsers.push(user);
      }
    };
    // change le droit d'écriture d'un utilisateur
    $scope.changeWriteRightUser = function(user) {
      var index = $scope.selectedMailFolder.writeRightUsers.indexOfByTopiaId(user);
      if (index != -1) {
        $scope.selectedMailFolder.writeRightUsers.splice(index, 1);
      } else {
        // ajout automatique du droit de lecture pour un droit d'ecriture
        if ($scope.selectedMailFolder.readRightUsers.indexOfByTopiaId(user) == -1) {
          $scope.selectedMailFolder.readRightUsers.push(user);
        }
        $scope.selectedMailFolder.writeRightUsers.push(user);
      }
    };
    // change le droit de déplacement d'un utilisateur
    $scope.changeMoveRightUser = function(user) {
      var index = $scope.selectedMailFolder.moveRightUsers.indexOfByTopiaId(user);
      if (index != -1) {
        $scope.selectedMailFolder.moveRightUsers.splice(index, 1);
      } else {
        $scope.selectedMailFolder.moveRightUsers.push(user);
      }
    };

    // when the signingImageChange
    $scope.imageChanged = function(input, attribute, preview) {
      var f = input.files[0]; // FileList object

      // Only process image files.
      if (!f.type.match('image.*')) {
        $window.alert("Erreur lors du chargement de l'image");
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onloadend = function() {

        $scope.$apply(function() {
          $scope.selectedMailFolder[attribute] = reader.result;
        });
      };

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    };

    $scope.addAcknowledgementException = function(newAcknowledgementException) {
      $scope.selectedMailFolder.acknowledgementException.push(newAcknowledgementException);
      delete $scope.newAcknowledgementException;
    };

    $scope.removeAcknowledgementException = function(index, client) {
      if ($window.confirm("Êtes-vous sur de vouloir supprimer cette exception ?")) {
        $scope.selectedMailFolder.acknowledgementException.splice(index, 1);
      }
    };
}]);


/**
 * Mail filter tab controller.
 */
ConfigurationModule.controller('ConfigurationFilterController', ['$scope', '$window', 'ConfigurationData',
  function($scope, $window, ConfigurationData) {
    //{Object} New accout empty filter
    $scope.newFilter = { filterFolderPriority: false };

    // option de la configuration 'sortable'
    $scope.sortableOptions = {
      stop: function(e, ui) {
        $scope.mailFilters = [];
        // update global $scope.mailFilters list
        angular.forEach($scope.rootFolderMailFilters, function(folderFilters, rootFolderId) {
          $scope.mailFilters = $scope.mailFilters.concat(folderFilters);
        });
      }
    };

    // add new filter action
    $scope.addNewFilter = function(rootMailFolder) {
      // check if already exists
      if ($scope.mailFilters.indexOfBy('expression', $scope.newFilter) != -1) {
        $window.alert("Ce filtre existe déjà !");
      } else {
        // add the filter in the end of the root folder filters
        var indexToInsert = null;
        do {
          var indexOfNextRootMailFolder = $scope.mailFolders.indexOf(rootMailFolder) + 1;
          if (indexOfNextRootMailFolder < $scope.mailFolders.length) {
            var nextFolderId = $scope.mailFolders[indexOfNextRootMailFolder].topiaId;
            if ($scope.rootFolderMailFilters.hasOwnProperty(nextFolderId)) {
              indexToInsert = $scope.mailFilters.indexOf($scope.rootFolderMailFilters[nextFolderId][0]);
            }
          }
        } while (indexToInsert == null && indexOfNextRootMailFolder < $scope.mailFolders.length);

        if (indexToInsert != null) {
          $scope.mailFilters.splice(indexToInsert, 0, $scope.newFilter);
        } else {
          $scope.mailFilters.push($scope.newFilter);
        }

        if (!$scope.rootFolderMailFilters.hasOwnProperty(rootMailFolder.topiaId)) {
          $scope.rootFolderMailFilters[rootMailFolder.topiaId] = [];
        }
        $scope.rootFolderMailFilters[rootMailFolder.topiaId].push($scope.newFilter);
        // clear form
        $scope.newFilter = { filterFolderPriority: false };
      }
    };

    // remove filter
    $scope.removeFilter = function(rootMailFolder, filter) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer ce filtre ?")) {
        var index = $scope.mailFilters.indexOf(filter);
        $scope.mailFilters.splice(index, 1);
        var index2 = $scope.rootFolderMailFilters[rootMailFolder.topiaId].indexOf(filter);
        $scope.rootFolderMailFilters[rootMailFolder.topiaId].splice(index2, 1);
      }
    };
}]);


/**
 * Search tab controller.
 */
ConfigurationModule.controller('ConfigurationSearchController', ['$scope', 'ConfigurationData',
  function($scope, ConfigurationData) {
    //{Array} Options actuelle
    $scope.searchDisplayColumns = [];
    if ($scope.configuration.searchDisplayColumns) {
      angular.forEach($scope.configuration.searchDisplayColumns, function(searchDisplayColumn) {
        $scope.searchDisplayColumns.push({
          id: searchDisplayColumn,
          label: $scope.tableMailFields[searchDisplayColumn]
        });
      });
    }

    $scope.$watch("searchDisplayColumns", function(newValue, oldValue) {
      if (newValue) {
        $scope.configuration.searchDisplayColumns = [];
        angular.forEach(newValue, function(elem) {
          $scope.configuration.searchDisplayColumns.push(elem.id);
        });
      }
    });

    // fonction retournant l'ensemble des options disponibles
    $scope.getObjectsData = function(term, result) {
      var resultArray = [];
      angular.forEach($scope.tableMailFields, function(label, mailField) {
        resultArray.push({
          id: mailField,
          label: label
        });
      });
      result(resultArray);
    };
}]);


/**
 * Email accounts controller.
 */
ConfigurationModule.controller('ConfigurationEmailAccountsController', ['$scope', '$window', '$http', 'ConfigurationData',
  function($scope, $window, $http, ConfigurationData) {
    //{Array} Email accounts
    $scope.emailAccounts = ConfigurationData.emailAccounts;
    //{Array} Port par défaut des protocols
    $scope.emailProtocolPorts = ConfigurationData.emailProtocolPorts;
    //{Object} New accout empty object
    $scope.newAccount = {};
    
    // ajout d'un nouveau compte
    $scope.addEmailAccount = function() {
      $scope.emailAccounts.push($scope.newAccount);
      $scope.newAccount = {};
      $scope.addEmailAccountForm.$setPristine();
    };

    // suppression d'un compte
    $scope.removeEmailAccount = function(index) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer ce compte email ?")) {
        $scope.emailAccounts.splice(index, 1);
      }
    };

    // test de connexion au serveur de mail
    $scope.checkEmailAccount = function() {
      $http.post(ConfigurationData.remoteCheckEmailAccount, "emailAccount=" + encodeURIComponent(angular.toJson($scope.newAccount)),
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
          .success(function(data, status, headers, config) {
            if (data.ok) {
              $window.alert("Le test de connexion est passé avec succès.");
            } else {
              $window.alert("Impossible de se connecter au serveur : " + data.error);
            }
          });
    };

    // selection d'un protocol
    $scope.$watch("newAccount.protocol", function(newValue, oldValue) {
      // auto remplissage du port par defaut
      if (newValue && ((oldValue && $scope.newAccount.port == $scope.emailProtocolPorts[oldValue]) || !$scope.newAccount.port)) {
        $scope.newAccount.port = $scope.emailProtocolPorts[newValue];
      }
    });
}]);

/**
 * Brands for domain controller.
 */
ConfigurationModule.controller('ConfigurationBrandsForDomainController', ['$scope', '$window', '$http', 'ConfigurationData',
  function($scope, $window, $http, ConfigurationData) {
    //{Array} Brands for domain
    $scope.brandsForDomains = ConfigurationData.brandsForDomains;
    //{Object} New accout empty object
    $scope.newBrandsForDomain = {};

    // ajout d'un nouveau domaine
    $scope.addBrandsForDomain = function() {
      if ($scope.brandsForDomains.indexOfBy('domainName', $scope.newBrandsForDomain) != -1) {
        $window.alert("Ce nom de domaine est déjà configuré !");

      } else if ($scope.newBrandsForDomain.domainName.indexOf("@") != -1) {
        $window.alert("Le nom de domaine ne doit pas contenir le symbole @ !");

      } else {
        var newBrandsForDomain = $scope.newBrandsForDomain;
        newBrandsForDomain.brandsJson = newBrandsForDomain.brands.toString();
        $scope.brandsForDomains.push(newBrandsForDomain);
        $scope.newBrandsForDomain = {};
        $scope.addBrandsForDomainForm.$setPristine();
      }
    };

    // suppression d'un domaine
    $scope.removeBrandsForDomain = function(index) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer ce nom de domaine ?")) {
        $scope.brandsForDomains.splice(index, 1);
      }
    };

}]);

/**
 * Signings tab controller.
 */
ConfigurationModule.controller('ConfigurationSigningsController', ['$scope', '$window', 'ConfigurationData',
  function($scope, $window, ConfigurationData) {
    //{Object} signature selectionné
    $scope.selectedSigning;

    for (var i = 0 ; i < $scope.signings.length ; i++) {
      var signing = $scope.signings[i];
      signing.isImageType = signing.image != null;
      signing.oldImage = signing.image;
      signing.oldText = signing.text;
    }

    $scope.$watch("selectedSigning.isImageType", function(newValue) {
      if ($scope.selectedSigning) {
        if (newValue) {
          $scope.selectedSigning.text = null;
          $scope.selectedSigning.image = $scope.selectedSigning.oldImage;

        } else {
          $scope.selectedSigning.image = null;
          $scope.selectedSigning.text = $scope.selectedSigning.oldText;
        }
      }
    });

    // edition d'une signature
    $scope.editSigning = function(signing) {

      $scope.selectedSigning = signing;

      $('#signingImage').val(null);
      $('#signingPreview').attr('src', signing.image ? signing.image : null);

    };

    // ajout d'une nouvelle signature
    $scope.newSigning = function() {
      var domainName = $window.prompt("Nom de domaine pour la nouvelle signature");
      // check if correct domain
      if (domainName) {
        // auto select new etat
        var newSigning = {
            topiaId : "new_" + guid(),
            domainName: domainName
        };

        // check if already exists
        if ($scope.signings.indexOfBy('domainName', newSigning) != -1) {
          $window.alert("Ce nom de domaine est déjà configuré !");

        } else if (domainName.indexOf("@") != -1) {
          $window.alert("Le nom de domaine ne doit pas contenir le symbole @ !");

        } else {
          $scope.signings.push(newSigning);

          // auto select
          $scope.editSigning(newSigning);
        }
      }
    };

    // suppression d'un tampon
    $scope.deleteSigning = function(signing, index) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer cette signature ?")) {
        $scope.signings.splice(index, 1);
        $scope.selectedSigning = null;
      }
    };

    $scope.imageChanged = function(input) {
      var f = input.files[0]; // FileList object

      // Only process image files.
      if (!f.type.match('image.*')) {
        $window.alert("Erreur lors du chargement de l'image");
        $('#signingPreview').attr('src', null);
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onloadend = function() {

        $scope.$apply(function() {
          $scope.selectedSigning.image = reader.result;
          $scope.selectedSigning.oldImage = reader.result;
        });
        $('#signingPreview').attr('src', reader.result);
      };

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }

}]);

/**
 * Chef de groupe controller.
 */
ConfigurationModule.controller('ConfigurationChefGroupController', ['$scope', '$window', 'ConfigurationData',
  function($scope, $window, ConfigurationData) {
    //{Object} Chef de group en cours d'edition
    $scope.selectedGroupChef;
    //{Object} Nouveau group selectionné
    $scope.newManagedGroup;

    // change instances
    if (!$scope.configuration.chefs) {
      $scope.configuration.chefs = [];
    }
    angular.forEach($scope.groups, function(group) {
      angular.forEach($scope.configuration.chefs, function(chef) {
        if (chef.userGroup.topiaId == group.topiaId) {
          chef.userGroup = group;
        }
        // managedGroups is not necessary to change here
      });
    });

    // filtre pour retourner seulement les groupes pouvant être chef (pas déjà utilisé)
    $scope.availableNewGroupChef = function(item) {
      // si c'est le groupe sélectionné, on l'autorise
      var result = 
        $scope.selectedGroupChef && $scope.selectedGroupChef.userGroup &&
        $scope.selectedGroupChef.userGroup.topiaId == item.topiaId;
      // sinon on refuse les groupes utilisé par d'autre chef
      if (!result) {
        result = true;
        angular.forEach($scope.configuration.chefs, function(group) {
          if (group.userGroup && group.userGroup.topiaId == item.topiaId) {
            result = false;
          }
        });
      }
      return result;
    };

    // ajout d'un nouveau chef de groupe
    $scope.newGroupChef = function() {
      var groupChef = {
          managedGroups : []
      };
      $scope.configuration.chefs.push(groupChef);
      $scope.editGroupChef(groupChef);
    };

    // edition d'un chef de groupe
    $scope.editGroupChef = function(groupChef) {
      $scope.selectedGroupChef = groupChef;
    };

    // suppression d'un chef de groupe
    $scope.removeGroupChef = function(groupChef, index) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer ce chef de groupe ?")) {
        $scope.configuration.chefs.splice(index, 1);
        if (groupChef == $scope.selectedGroupChef) {
          delete $scope.selectedGroupChef;
        }
      }
    };

    // Ajout d'un nouveau group géré
    $scope.addManagedGroup = function() {
      $scope.selectedGroupChef.managedGroups.push($scope.newManagedGroup);
      delete $scope.newManagedGroup;
    };
    
    // Suppression d'un group gérés
    $scope.removeManagedGroup = function(index) {
      if ($window.confirm("Êtes-vous sûr de vouloir supprimer ce groupe gérés ?")) {
        $scope.selectedGroupChef.managedGroups.splice(index, 1);
      }
    };


    $scope.onSelectManagedGroupCallback = function (item, model) {
      $scope.newManagedGroup = item;
    };
}]);

function isLocalStorageAvailable() {
  var result;
  try {
    result = 'localStorage' in window && window['localStorage'] !== null;
  } catch (e) {
    // can happen with IE 11 in desktop mode
    result = false;
  }
  return result;
}

$(function() {
  if (isLocalStorageAvailable()) {
    
    // save tab hash in local storage
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var hash = e.target.hash;
      localStorage.setItem("configuration-tab", hash);
    });
    
    // restore tab if present
    var current = localStorage.getItem("configuration-tab");
    if (current) {
      $('#tabs a[href="' + current + '"]').tab('show');
    }
  }
});
