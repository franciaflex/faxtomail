/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Generates a GUID string, according to RFC4122 standards.
 * @returns {String} The generated GUID.
 * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
 * @author Slavik Meltser (slavik@meltser.info).
 * @link http://slavik.meltser.info/?p=142
 */
function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16)+"000000000").substr(2,8);
        return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

/**
 * Cherche un element par egalité de champ sur le champ spécifié.
 */
Array.prototype.indexOfBy = function(field, obj) {
  var i = this.length;
  while (i--) {
      if (this[i][field] == obj[field]) {
          return i;
      }
  }
  return -1;
};

/**
 * Même fonction que indexOf mais qui compare par topiaId au lieu des références.
 */
Array.prototype.indexOfByTopiaId = function(obj) {
  return this.indexOfBy('topiaId', obj);
};

/**
 * Method contains par topiaId.
 */
Array.prototype.containsByTopiaId = function(obj) {
  return this.indexOfByTopiaId(obj) != -1;
};


/** Mail faxtomail application module. */
var FaxToMailModule = angular.module('FaxToMail', ['ui.bootstrap']);

/**
 * Directive de validation d'un champ en entier.
 */
FaxToMailModule.directive('fmInteger', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {
                if (/^\d+$/.test(viewValue)) {
                    // it is valid
                    ctrl.$setValidity('integer', true);
                    return viewValue;
                } else if (viewValue.length == 0) {
                    ctrl.$setValidity('integer', true);
                    return undefined;
                } else {
                    // it is invalid, return undefined (no model update)
                    ctrl.$setValidity('integer', false);
                    return undefined;
                }
            });
        }
    };
});

var ngBindHtmlDirective = ['$sce', function($sce) {
   return function(scope, element, attr) {
     scope.$watch($sce.parseAsHtml(attr.ngBindHtml), function(value) {
       element.html(value || '');
     });
   };
   }];
