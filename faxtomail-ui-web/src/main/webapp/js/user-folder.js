/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

var UserFolderModule = angular.module('UserFolderModule', ['FaxToMail', 'ui.select2.sortable']);

/**
 * Global configuration controller.
 */
UserFolderModule.controller('UserFolderController', ['$scope', '$http', 'UserFolderData',
  function($scope, $http, UserFolderData) {
    // {Array} mail folders
    $scope.mailFolders = UserFolderData.mailFolders;
    // {Array} Liste des utilisateurs ldap
    $scope.users = UserFolderData.users;
    // {Array} Group list
    $scope.groups = UserFolderData.groups;
    // {Map<MailFolder id, Array<MailFolder>>} liste des dossiers classé par topia Id du noeud racine
    $scope.flatMailFolders;
    // {Map<MailFolder id, MailFolder>} dossier accessible par topia id
    $scope.flatMailFolders2;
    // {Map} user id : mailFolders
    $scope.userFolders = UserFolderData.userFolders;
    

    // method privée recursive pour retourner l'ensemble des dossiers en les modifiant pour ajouter
    // des metadata (full path from root, and root node instance)
    var recursiveAddMailFolder = function(result, mailFolders, rootFolder, prefix, parent) {
      if (mailFolders) {
        angular.forEach(mailFolders, function(mailFolder) {

          // warning modify input object :(
          // use $ from this, even not recommended because angular exclude those fields when jsonify
          mailFolder.$fullPath = prefix + "/" + mailFolder.name;
          // ha la la, modify input object again :(
          // use $ from this, even not recommended because angular exclude those fields when jsonify
          mailFolder.$rootFolder = rootFolder;
          // encore pour le parent
          mailFolder.$parent = parent;

          result.push(mailFolder);
          $scope.flatMailFolders2[mailFolder.topiaId] = mailFolder;
          recursiveAddMailFolder(result, mailFolder.children, rootFolder, mailFolder.$fullPath, mailFolder);
        });
      }
    };

    // update $scope.flatMailFolders when $scope.mailFolders changes
    $scope._updateFlatMailFolders = function() {
      console.log("Update flatMailFolders map");
      $scope.flatMailFolders = {};
      $scope.flatMailFolders2 = {};
      angular.forEach($scope.mailFolders, function(mailFolder) {
        $scope.flatMailFolders[mailFolder.topiaId] = [];
        recursiveAddMailFolder($scope.flatMailFolders[mailFolder.topiaId], mailFolder.children, mailFolder, mailFolder.name, mailFolder);
      });
    };
    $scope._updateFlatMailFolders();

    // mise à jour des configurations actuelle des dossiers pour inclure les metas info des dossiers
    $scope._updateUserFolders = function() {
      angular.forEach($scope.userFolders, function(value, key) {
        angular.forEach(value, function(folder) {
          var realFolder = $scope.flatMailFolders2[folder.topiaId];
          // update text with full path
          folder.text = realFolder.$fullPath;
        });
      });
    };
    $scope._updateUserFolders();

    // fonction retournant l'ensemble des options disponibles
    $scope.getObjectsData = function(term, result) {
      var resultArray = [];
      angular.forEach($scope.mailFolders, function(mailFolder) {
        // select 2 require an id field
        mailFolder.id = mailFolder.topiaId;
        // select 2 can use 'name' for display, so it's ok

        // push root
        resultArray.push(mailFolder);

        // build children map
        if ($scope.flatMailFolders.hasOwnProperty(mailFolder.topiaId)) {
          angular.forEach($scope.flatMailFolders[mailFolder.topiaId], function(childFolder) {
            childFolder.id = childFolder.topiaId;
            // defined text for display
            childFolder.text = childFolder.$fullPath;
            resultArray.push(childFolder);
          });
        }

      });
      result(resultArray);
    };
}]);
