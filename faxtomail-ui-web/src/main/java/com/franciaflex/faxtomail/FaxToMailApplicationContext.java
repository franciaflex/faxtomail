package com.franciaflex.faxtomail;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaApplicationContext;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaPersistenceContext;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.InitFaxToMailService;
import com.franciaflex.faxtomail.services.service.migration.FaxtomailFlywayMigrationService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.flyway.TopiaFlywayService;

import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.Map;

public class FaxToMailApplicationContext {

    private static Log log = LogFactory.getLog(FaxToMailApplicationContext.class);

    public static final String APPLICATION_CONTEXT_PARAMETER = "faxToMailApplicationContext";

    protected FaxToMailTopiaApplicationContext topiaApplicationContext;

    protected FaxToMailConfiguration applicationConfig;
    
    public FaxToMailConfiguration getApplicationConfig() {
        return applicationConfig;
    }

    public FaxToMailTopiaApplicationContext getTopiaApplicationContext() {
        return topiaApplicationContext;
    }

    public FaxToMailTopiaPersistenceContext newPersistenceContext() {

        FaxToMailTopiaPersistenceContext persistenceContext = getTopiaApplicationContext().newPersistenceContext();

        return persistenceContext;

    }

    public FaxToMailServiceContext newServiceContext(FaxToMailTopiaPersistenceContext persistenceContext) {

        // FIXME AThimel 05/08/14 Check if newDirectServiceContext or newServiceContext must be used
        FaxToMailServiceContext newServiceContext =
                FaxToMailServiceContext.newDirectServiceContext(persistenceContext);

        FaxToMailConfiguration applicationConfig = getApplicationConfig();

        newServiceContext.setApplicationConfig(applicationConfig);

        return newServiceContext;

    }

    public void close() {
        if (topiaApplicationContext != null) {
            topiaApplicationContext.close();
        }
    }

    public void init(ServletContext servletContext) {

        applicationConfig = new FaxToMailConfiguration("faxToMail.properties");

        // it's set here for only web application to migrate schema, not client
        Map<String, String> properties = new HashMap<>();
        properties.put("topia.service.migration", FaxtomailFlywayMigrationService.class.getName());
        properties.put("topia.service.migration." + TopiaFlywayService.USE_MODEL_VERSION, "false");

        // add configuration properties after to allow override
        properties.putAll(applicationConfig.getTopiaProperties());

        // create application
        topiaApplicationContext = new FaxToMailTopiaApplicationContext(properties);

        /*if (getApplicationConfig().isLogConfigurationProvided()) {

            File log4jConfigurationFile = getApplicationConfig().getLogConfigurationFile();

            String log4jConfigurationFileAbsolutePath = log4jConfigurationFile.getAbsolutePath();

            if (log4jConfigurationFile.exists()) {

                if (log.isInfoEnabled()) {
                    log.info("will use logging configuration " + log4jConfigurationFileAbsolutePath);
                }

                // reset logger configuration
                LogManager.resetConfiguration();

                // use generate log config file
                PropertyConfigurator.configure(log4jConfigurationFileAbsolutePath);

                log = LogFactory.getLog(FaxToMailApplicationContext.class);

            } else {
                if (log.isWarnEnabled()) {
                    log.warn("there is no file " + log4jConfigurationFileAbsolutePath + ". Default logging configuration will be used.");
                }
            }

        } else {
            log.info("will use default logging configuration");
        }*/

//        I18nInitializer initializer = new DefaultI18nInitializer("faxtomail");

//        I18n.init(initializer, Locale.FRANCE);

//        LocalizedTextUtil.addDefaultResourceBundle("i18n.faxtomail-web");

        FaxToMailTopiaApplicationContext topiaAppContext = getTopiaApplicationContext();
        if (topiaAppContext.isSchemaEmpty()) {
            if (log.isInfoEnabled()) {
                log.info("Create application schema");
            }
            topiaAppContext.createSchema();
        }
        
        FaxToMailTopiaPersistenceContext persistenceContext = newPersistenceContext();

        FaxToMailServiceContext serviceContext = newServiceContext(persistenceContext);

        InitFaxToMailService initFaxToMailService =
                serviceContext.getInitFaxToMailService();

        initFaxToMailService.init();

        persistenceContext.close();

    }

}
