package com.franciaflex.faxtomail.web.action.admin;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.EmailAccount;
import com.franciaflex.faxtomail.services.service.ConfigurationService;
import com.franciaflex.faxtomail.web.FaxToMailJsonAction;
import org.apache.struts2.convention.annotation.Action;

public class ConfigurationJsonAction extends FaxToMailJsonAction {

    protected ConfigurationService configurationService;

    /** Directory path to check existence and writability. */
    protected String path;

    protected EmailAccount emailAccount;

    protected Object jsonData;

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    public void setEmailAccount(String json) {
        this.emailAccount = getGson().fromJson(json, EmailAccount.class);
    }

    @Action("configuration-check-directory-json")
    public String checkDirectory() {
        jsonData = configurationService.checkDirectory(path);
        return SUCCESS;
    }

    @Action("configuration-check-mailaccount-json")
    public String checkMail() {
        jsonData = configurationService.checkMailaccount(emailAccount);
        return SUCCESS;
    }

    @Override
    public Object getJsonData() {
        return jsonData;
    }

}
