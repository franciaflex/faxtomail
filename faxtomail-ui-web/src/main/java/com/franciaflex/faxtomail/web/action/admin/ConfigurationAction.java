package com.franciaflex.faxtomail.web.action.admin;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.BrandsForDomain;
import com.franciaflex.faxtomail.persistence.entities.Configuration;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.EmailAccount;
import com.franciaflex.faxtomail.persistence.entities.EmailProtocol;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroup;
import com.franciaflex.faxtomail.persistence.entities.MailAction;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.MailFilter;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.persistence.entities.SigningForDomain;
import com.franciaflex.faxtomail.persistence.entities.Stamp;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.service.ConfigurationService;
import com.franciaflex.faxtomail.services.service.MailFolderService;
import com.franciaflex.faxtomail.services.service.ReferentielService;
import com.franciaflex.faxtomail.services.service.UserService;
import com.franciaflex.faxtomail.web.FaxToMailActionSupport;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.xwork2.Preparable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Result;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kmorin - kmorin@codelutin.com
 *
 */
@InterceptorRefs({
    @InterceptorRef("faxToMailInterceptor"),
    @InterceptorRef("loginInterceptor"),
    @InterceptorRef("paramsPrepareParamsStack")
})
public class ConfigurationAction extends FaxToMailActionSupport implements Preparable {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(ConfigurationAction.class);

    protected ConfigurationService configurationService;

    protected UserService userService;

    protected ReferentielService referentielService;

    protected MailFolderService mailFolderService;

    protected Configuration configuration;

    protected List<WaitingState> waitingStates;

    protected Map<String, Long> waitingStatesUsage;

    protected List<DemandType> demandTypes;

    protected List<Stamp> stamps;

    protected List<Range> ranges;

    protected List<MailFolder> mailFolders;

    protected Map<String, Long> mailFoldersUsage;

    protected List<MailFilter> mailFilters;

    protected List<EmailAccount> emailAccounts;

    protected List<FaxToMailUser> users;

    protected List<FaxToMailUserGroup> groups;

    protected List<BrandsForDomain> brandsForDomains;

    protected List<SigningForDomain> signingForDomains;

    @Override
    public void prepare() throws Exception {
        // check authorization
        if (!getSession().isAdmin()) {
            throw new RuntimeException("Not authorized");
        }
    }

    @Override
    @Action("configuration-input")
    public String input() throws Exception {
        waitingStates = referentielService.getAllWaitingState();
        waitingStatesUsage = referentielService.getWaitingStatesUsage();
        demandTypes = referentielService.getAllDemandType();
        stamps = referentielService.getAllStamps();
        ranges = referentielService.getAllRange();
        mailFolders = mailFolderService.getRootMailFolders();
        mailFoldersUsage = mailFolderService.getMailFoldersUsage();
        emailAccounts = configurationService.getEmailAccountsWithoutPasswords();
        mailFilters = configurationService.getMailFilters();
        configuration = configurationService.getConfiguration();
        users = userService.getAllActiveUsers();
        groups = userService.getAllActiveUserGroups();
        brandsForDomains = configurationService.getAllBrandsForDomains();
        signingForDomains = configurationService.getAllSigningsForDomains();

        return INPUT;
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "configuration-input"})})
    public String execute() throws Exception {
        String result = super.execute();

        configurationService.save(configuration,
                                  demandTypes,
                                  stamps,
                                  waitingStates,
                                  mailFolders,
                                  mailFilters,
                                  emailAccounts,
                                  brandsForDomains,
                                  signingForDomains);

        return result;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setReferentielService(ReferentielService referentielService) {
        this.referentielService = referentielService;
    }

    public void setMailFolderService(MailFolderService mailFolderService) {
        this.mailFolderService = mailFolderService;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfigurationJson(String json) {
        this.configuration = getGson().fromJson(json, Configuration.class);
    }

    public List<WaitingState> getWaitingStates() {
        return waitingStates;
    }

    public void setWaitingStatesJson(String json) {
        Type type = new TypeToken<List<WaitingState>>() {}.getType();
        this.waitingStates = getGson().fromJson(json, type);
    }

    public Map<String, Long> getWaitingStatesUsage() {
        return waitingStatesUsage;
    }

    public List<DemandType> getDemandTypes() {
        return demandTypes;
    }

    public void setDemandTypesJson(String json) {
        Type type = new TypeToken<List<DemandType>>() {}.getType();
        this.demandTypes = getGson().fromJson(json, type);
    }

    public List<Stamp> getStamps() {
        return stamps;
    }

    public void setStampsJson(String json) {
        Type type = new TypeToken<List<Stamp>>() {}.getType();
        this.stamps = getGson().fromJson(json, type);
    }

    public List<Range> getRanges() {
        return ranges;
    }

    public List<MailFolder> getMailFolders() {
        return mailFolders;
    }

    public void setMailFoldersJson(String json) {
        Type type = new TypeToken<List<MailFolder>>() {}.getType();
        this.mailFolders = getGson().fromJson(json, type);
    }

    public List<MailFilter> getMailFilters() {
        return mailFilters;
    }
    
    public void setMailFiltersJson(String json) {
        Type type = new TypeToken<List<MailFilter>>() {}.getType();
        this.mailFilters = getGson().fromJson(json, type);
    }

    public Map<String, Long> getMailFoldersUsage() {
        return mailFoldersUsage;
    }

    public List<EmailAccount> getEmailAccounts() {
        return emailAccounts;
    }
    
    public void setEmailAccountsJson(String json) {
        Type type = new TypeToken<List<EmailAccount>>() {}.getType();
        this.emailAccounts = getGson().fromJson(json, type);
    }

    public List<BrandsForDomain> getBrandsForDomains() {
        return brandsForDomains;
    }

    public void setBrandsForDomainsJson(String json) {
        Type type = new TypeToken<List<BrandsForDomain>>() {}.getType();
        this.brandsForDomains = getGson().fromJson(json, type);
    }

    public List<SigningForDomain> getSigningForDomains() {
        return signingForDomains;
    }

    public void setSigningForDomainsJson(String json) {
        Type type = new TypeToken<List<SigningForDomain>>() {}.getType();
        this.signingForDomains = getGson().fromJson(json, type);
    }

    public List<FaxToMailUser> getUsers() {
        return users;
    }

    public List<FaxToMailUserGroup> getGroups() {
        return groups;
    }

    public Map<MailAction, String> getMailActions() {
        return getEnumAsMap(MailAction.values());
    }
    
    public Map<MailField, String> getCanBeRequiredMailFields() {
        return getEnumAsMap(MailField.getCanBeRequiredMailFields());
    }

    public Map<MailField, String> getTableMailFields() {
        return getEnumAsMap(MailField.getTableFields());
    }
    
    public Map<EmailProtocol, Integer> getEmailProtocolPorts() {
        Map<EmailProtocol, Integer> result = new LinkedHashMap<>();
        for (EmailProtocol protocol : EmailProtocol.values()) {
            result.put(protocol, protocol.getDefaultPort());
        }
        return result;
    }

}
