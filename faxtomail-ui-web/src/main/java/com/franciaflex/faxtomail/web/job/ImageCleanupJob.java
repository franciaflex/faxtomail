package com.franciaflex.faxtomail.web.job;

/*-
 * #%L
 * FaxToMail :: Web
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2023 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailApplicationContext;
import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaPersistenceContext;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import org.apache.commons.io.IOUtils;
import org.nuiton.util.FileUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.List;

@DisallowConcurrentExecution
public class ImageCleanupJob  extends AbstractFaxToMailJob {

    private static final Log log = LogFactory.getLog(ImageCleanupJob.class);

    protected FaxToMailApplicationContext applicationContext;

    protected FaxToMailConfiguration config;

    protected EmailService emailService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        applicationContext = getApplicationContext(jobExecutionContext);

        //persistenceContext clos par le serviceContext
        FaxToMailTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext();

        try (FaxToMailServiceContext serviceContext = applicationContext.newServiceContext(persistenceContext)){

            config = serviceContext.getApplicationConfig();

            if (log.isInfoEnabled()) {
                log.info("Running ImageCleanupJob at " + serviceContext.getNow());
            }

            emailService = serviceContext.getEmailService();

            cleanImages();

            if (log.isDebugEnabled()) {
                log.debug("ImageCleanupJob ended at " + serviceContext.getNow());
            }
        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't run quartz job", ex);
            }
        }
    }

    protected void cleanImages() {

        int maxNumberToClean = config.getConvertMaxNumber();

        int cleanedEmail = 0;

        int cleanedEmailThisIteration = 1;

        while (cleanedEmail < maxNumberToClean && cleanedEmailThisIteration != 0) {

            List<Email> emailsATraiter = emailService.getArchivedMailNotProcessed(10);

            for (Email email : emailsATraiter) {

                Collection<Attachment> attachments = email.getAttachment();

                boolean imageProcessed = true;

                for (Attachment attachment : attachments) {
                    if (isImage(attachment.getOriginalFileName()) && attachment.getOriginalFile().getContent() != null && attachment.getOriginalFile().getContent().length != 0) {

                        Path src = null;
                        Path dest = null;

                        try {

                            //Create src temp file from attachment
                            src = Files.createTempFile("input", "." + FileUtil.extension(attachment.getOriginalFileName()));
                            Files.write(src, attachment.getOriginalFile().getContent(), StandardOpenOption.CREATE);

                            //Create dest temp file
                            dest = Files.createTempFile("output", "." + FileUtil.extension(attachment.getOriginalFileName()));

                            //Convert using imageMagick
                            //Paramètres récupérés de cet article : https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/
                            Process process = new ProcessBuilder(
                                    config.getConvertLocation(),
                                    src.toAbsolutePath().toString(),
                                    "-filter", "Triangle",
                                    "-define", "filter:support=2",
                                    "-thumbnail", config.getConvertMaxSize() + "x" + config.getConvertMaxSize() + ">",
                                    "-unsharp", "0.25x0.25+8+0.065",
                                    "-dither", "None",
                                    "-posterize", "136",
                                    "-define", "jpeg:fancy-upsampling=off",
                                    "-define", "png:compression-filter=5",
                                    "-define", "png:compression-level=9",
                                    "-define", "png:compression-strategy=1",
                                    "-define", "png:exclude-chunk=all",
                                    "-interlace", "none",
                                    "-colorspace", "sRGB",
                                    "-quality", String.valueOf(config.getConvertQuality()),
                                    "-strip",
                                    dest.toAbsolutePath().toString()
                            ).start();

                            //Log imagemagick errors if any
                            try (InputStream is = process.getErrorStream();
                                 InputStreamReader isr = new InputStreamReader(is);
                                 BufferedReader br = new BufferedReader(isr)) {
                                String line;
                                while ((line = br.readLine()) != null) {
                                    if (log.isWarnEnabled()) {
                                        log.warn(line);
                                    }
                                }
                                //Wait for process to finish
                                int exitCode = process.waitFor();
                                if (exitCode != 0) {
                                    if (log.isWarnEnabled()) {
                                        log.warn("Could not clean image " + attachment.getOriginalFileName() + " from courriel " + email.getTopiaId() + " , error in imagemagick");
                                    }

                                    imageProcessed = false;
                                }
                            }

                            //Read destFile and put it back into attachment
                            attachment.getOriginalFile().setContent(IOUtils.toByteArray(dest.toUri()));

                            //Clean directory after process execution
                            Files.delete(src);
                            Files.delete(dest);
                        } catch (IOException e) {
                            if (log.isWarnEnabled()) {
                                log.warn("Could not clean image " + attachment.getOriginalFileName() + " from courriel " + email.getTopiaId(), e);
                            }
                            imageProcessed = false;
                        } catch (InterruptedException e) {
                            if (log.isErrorEnabled()) {
                                log.error("Cannot run convert command for image " + attachment.getOriginalFileName() + " from courriel " + email.getTopiaId(), e);
                            }
                            imageProcessed = false;
                            Thread.currentThread().interrupt();
                        } finally {
                            try {
                                //Clean directory after process execution
                                if (src != null) {
                                    Files.delete(src);
                                }
                                if (dest != null) {
                                    Files.delete(dest);
                                }
                            } catch (IOException e) {
                                log.debug("Could not delete files, they probably do not exist", e);
                            }
                        }
                    }
                }

                email.setImageCleaned(imageProcessed);

                emailService.saveEmail(email, attachments, null, null, Email.PROPERTY_ATTACHMENT);

                attachments = null;

            }

            cleanedEmailThisIteration = emailsATraiter.size();
            cleanedEmail += cleanedEmailThisIteration;
        }

        if (log.isInfoEnabled()) {
            log.info("Cleaned up " + cleanedEmail + " emails");
        }

    }

    protected boolean isImage(String fileName) {
        return FileUtil.extension(fileName).equalsIgnoreCase("PNG") ||
                FileUtil.extension(fileName).equalsIgnoreCase("JPG") ||
                FileUtil.extension(fileName).equalsIgnoreCase("JPEG") ||
                FileUtil.extension(fileName).equalsIgnoreCase("HEIC") ||
                FileUtil.extension(fileName).equalsIgnoreCase("TIFF");
    }

}
