package com.franciaflex.faxtomail.web;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailEntityEnum;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserAbstract;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.MailFolderAbstract;
import com.franciaflex.faxtomail.web.json.HibernateProxyTypeAdapter;
import com.franciaflex.faxtomail.web.json.TopiaEntityAdapter;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Map;

@Results({
        @Result(name="login", type="redirectAction", params = { "actionName", "login-input", "namespace", "/authentication"})
})
public class FaxToMailActionSupport extends ActionSupport {

    private static final Log logger = LogFactory.getLog(FaxToMailActionSupport.class);

    public static final String SAVE = "save";

    public static final String NEXT = "next";

    protected static final Multimap<Class<?>, String> GSON_EXCLUSIONS = HashMultimap.create();

    static {
        // Fill exclusion map
        GSON_EXCLUSIONS.put(FaxToMailUserAbstract.class, FaxToMailUser.PROPERTY_AFFECTED_FOLDERS);
        GSON_EXCLUSIONS.put(MailFolderAbstract.class, MailFolder.PROPERTY_PARENT);
    }

    protected static final ExclusionStrategy EXCLUSION_STRATEGY = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes f) {
            Class<?> declaringClass = f.getDeclaringClass();
            String attributeName = f.getName();
            boolean result = GSON_EXCLUSIONS.containsEntry(declaringClass, attributeName);
            return result;
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    };

    protected FaxToMailConfiguration applicationConfig;

    protected FaxToMailSession session;

    protected static Gson gson;

    public void setApplicationConfig(FaxToMailConfiguration applicationConfig) {
        this.applicationConfig = applicationConfig;
    }

    public FaxToMailConfiguration getApplicationConfig() {
        return applicationConfig;
    }

    protected FaxToMailSession getSession() {
        return session;
    }

    public void setSession(FaxToMailSession session) {
        this.session = session;
    }

    public boolean isAuthenticated() {
        return session.getAuthenticatedFaxToMailUser() != null;
    }

    public boolean isAdmin() {
        return session.isAdmin();
    }

    public FaxToMailUser getAuthenticatedUser() {
        return session.getAuthenticatedFaxToMailUser();
    }

    public String toJson(Object element) {
        String result = null;
        try {
            result = getGson().toJson(element);
        } catch (Exception e) {
            logger.warn("Can't convert object to json", e);
        }
        return result;
    }

    public static Gson getGson() {
        if (gson == null) {
            GsonBuilder builder = new GsonBuilder();
            // exclusion
            builder.addSerializationExclusionStrategy(EXCLUSION_STRATEGY);

            // Renseigne a gson comment instancier les Impl à partir des interfaces Topia
            FaxToMailEntityEnum[] classes = FaxToMailEntityEnum.getContracts();
            for (FaxToMailEntityEnum clazz : classes) {
                Class<? extends TopiaEntity> type = clazz.getContract();
                Class<? extends TopiaEntity> implementationClass = clazz.getImplementation();
                TopiaEntityAdapter adapter = new TopiaEntityAdapter(implementationClass);
                builder.registerTypeAdapter(type, adapter);
            }

            // Type adapters : Hibernate proxies
            builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);

            gson = builder.create();
        }
        return gson;
    }

    /**
     * Transform enumeration values into map with i18n value for each enum value.
     * 
     * i18n key is fqn.NAME
     *
     * @param values values to transform
     * @return map (enum value &gt; i18n text)
     */
    protected <T> Map<T, String> getEnumAsMap(T... values) {
        Map<T, String> valuesMap = Maps.newLinkedHashMap();
        for (T value : values) {
            String i18n = value.getClass().getName() + "." + value.toString();
            String trans = getText(i18n);
            valuesMap.put(value, trans);
        }
        return valuesMap;
    }
}
