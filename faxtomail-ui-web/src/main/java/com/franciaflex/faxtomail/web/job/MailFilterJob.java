package com.franciaflex.faxtomail.web.job;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailApplicationContext;
import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.persistence.entities.*;
import com.franciaflex.faxtomail.services.DecoratorService;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.franciaflex.faxtomail.services.service.ClientService;
import com.franciaflex.faxtomail.services.service.ConfigurationService;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.services.service.MailFolderService;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.io.Files;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.nuiton.decorator.Decorator;
import org.nuiton.util.FileUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMessage;
import javax.net.ssl.SSLContext;
import java.awt.image.BufferedImage;

import java.io.*;

import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 */
@DisallowConcurrentExecution
public class MailFilterJob extends AbstractFaxToMailJob {

    private static final Log log = LogFactory.getLog(MailFilterJob.class);

    protected FaxToMailApplicationContext applicationContext;

    protected FaxToMailConfiguration config;

    protected ConfigurationService configurationService;

    protected EmailService emailService;

    protected MailFolderService mailFolderService;

    protected ClientService clientService;

    protected DecoratorService decoratorService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        applicationContext = getApplicationContext(jobExecutionContext);

        //persistenceContext clos par le serviceContext
        FaxToMailTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext();

        try (FaxToMailServiceContext serviceContext = applicationContext.newServiceContext(persistenceContext)){

            config = serviceContext.getApplicationConfig();
        
            if (log.isInfoEnabled()) {
                log.info("Running MailFilterJob at " + serviceContext.getNow());
            }

            emailService = serviceContext.getEmailService();
            mailFolderService = serviceContext.getMailFolderService();
            configurationService = serviceContext.getConfigurationService();
            clientService = serviceContext.getClientService();
            decoratorService = serviceContext.getDecoratorService();

            Collection<EmailAccount> emailAccounts = configurationService.getEmailAccounts();
    
            for (EmailAccount account : emailAccounts) {
                checkEmails(account);
            }
            
            if (log.isDebugEnabled()) {
                log.debug("MailFilterJob ended at " + serviceContext.getNow());
            }
        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't run quartz job", ex);
            }
        }
    }

    /**
     * Checks the emails of the account
     * @param account
     */
    public void checkEmails(EmailAccount account) {
        Properties properties = new Properties();
        // set the mail.mime.address.strict to false to avoid
        // javax.mail.internet.AddressException: Domain contains illegal character errors when recipients contains []
        properties.setProperty("mail.mime.address.strict", "false");
        
        switch (account.getProtocol()) {
        case IMAPS:
            properties.setProperty("mail.imap.ssl.enable", "true");
        case IMAP:
            properties.setProperty("mail.store.protocol", "imap");
            properties.setProperty("mail.imap.host", account.getHost());
            properties.setProperty("mail.imap.port", String.valueOf(account.getPort()));
            properties.setProperty("mail.imap.connectiontimeout", "2000");
            properties.setProperty("mail.imap.starttls.enable", "true");
            try {
                String protocols = String.join(" ",
                        SSLContext
                                .getDefault()
                                .getSupportedSSLParameters()
                                .getProtocols()
                );

                log.debug("Set tls version to " + protocols);
                System.setProperty("mail.imap.ssl.protocols", protocols);
            } catch (NoSuchAlgorithmException e) {
                log.error("Could not set tls version. Force to v1.2");
                System.setProperty("mail.imap.ssl.protocols", "TLSv1.2");
            }
            break;
        case POP3S:
            properties.setProperty("mail.pop3.ssl.enable", "true");
        case POP3:
            properties.setProperty("mail.store.protocol", "pop3");
            properties.setProperty("mail.pop3.host", account.getHost());
            properties.setProperty("mail.pop3.port", String.valueOf(account.getPort()));
            properties.setProperty("mail.pop3.connectiontimeout", "2000");
            try {
                String protocols = String.join(" ",
                        SSLContext
                                .getDefault()
                                .getSupportedSSLParameters()
                                .getProtocols()
                );

                log.debug("Set tls version to " + protocols);
                System.setProperty("mail.pop3.ssl.protocols", protocols);
            } catch (NoSuchAlgorithmException e) {
                log.error("Could not set tls version. Force to v1.2");
                System.setProperty("mail.pop3.ssl.protocols", "TLSv1.2");
            }
            break;
        }

        Session session = Session.getInstance(properties);
        Store store = null;
        Folder defaultFolder = null;
        Folder inbox = null;

        try {
            store = session.getStore();
            store.connect(account.getLogin(), account.getPassword());
            defaultFolder = store.getDefaultFolder();
            inbox = defaultFolder.getFolder("INBOX");
            checkEmailsOfFolder(account, inbox);

        } catch (Exception | Error e) {
            log.error("Error while getting emails from the mailbox " + account.getLogin() + "@" + account.getHost(), e);

        } finally {
            close(inbox);
            close(defaultFolder);
            try {
                if (store != null && store.isConnected()) {
                    store.close();
                }
            } catch (MessagingException e) {
                log.error("Error while closing the store", e);
            }
        }

    }

    protected void close(Folder folder) {
        if (folder != null && folder.isOpen()) {
            try {
                boolean expunge = config.isMailExpunge();
                folder.close(expunge);
            } catch (Exception e) {
                log.error("Error while closing the folder", e);
            }
        }
    }

    /**
     * Check the emails of the folder, create the emails in the database and delete the email in the folder.
     * 
     * @param emailAccount email account currently checked
     * @param folder the folder to check
     */
    protected void checkEmailsOfFolder(EmailAccount emailAccount, Folder folder) {
        
        int importedCount = 0;

        int importCount = 1;

        try {
            folder.open(Folder.READ_WRITE);

            int count = folder.getMessageCount();
            int unread = folder.getUnreadMessageCount();

            if (log.isDebugEnabled()) {
                log.debug(emailAccount.getLogin() + "@" +  emailAccount.getHost() + " : " + count + " messages, " + unread + " unread");
            }

            for (Message message:folder.getMessages()){
                try {
                    Email email = new EmailImpl();
                    email.setFax(emailAccount.isFaxAccountType());

                    Charset charset = FaxToMailServiceUtils.getCharset(message);

                    Set<String> modifiedProperties = new HashSet<>();

                    if (log.isDebugEnabled()) {
                        log.debug(String.format("Message %d/%d : %s", importCount, count,  message.getSubject()));
                    }

                    importCount++;

                    List<Address> recipientAddresses = new ArrayList<>();
                    Address[] allRecipients = message.getAllRecipients();
                    if (allRecipients != null) {
                        recipientAddresses.addAll(Arrays.asList(allRecipients));
                    }
                    Set<String> recipients = recipientAddresses.stream()
                            .distinct()
                            .map(a -> FaxToMailServiceUtils.getDecodedFrom(a.toString()))
                            .collect(Collectors.toCollection(LinkedHashSet::new));

                    // try to find the real recipient, in case it is in the bcc -> Check Received headers
                    Enumeration<Header> allHeaders = message.getAllHeaders();
                    String regex = "^.*for<(.*)>.*$";

                    while (allHeaders.hasMoreElements()) {
                        Header header = allHeaders.nextElement();
                        if ("Received".equals(header.getName())) {
                            String forRecipient = StringUtils.removePattern(header.getValue(), "\\s");

                            if (forRecipient != null && forRecipient.matches(regex)) {
                                forRecipient = forRecipient.replaceFirst(regex, "$1");

                                if (StringUtils.isNotBlank(forRecipient) && recipients.add(forRecipient)) {
                                    if (log.isDebugEnabled()) {
                                        log.debug("recipient found in \"Received\" header: " + forRecipient);
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    // try to find the real recipient, in case it is in the bcc -> Check Delivered-To headers
                    String[] bcc = message.getHeader("Delivered-To");
                    if (bcc != null) {
                        recipients.addAll(Arrays.asList(bcc));
                    }

                    MailFilter filter = null;
                    for (String recipient : recipients) {
                        List<MailFilter> filters = mailFolderService.getFiltersForRecipient(recipient);

                        if (CollectionUtils.isNotEmpty(filters)) {
                            MailFilter mailFilter = filters.get(0);
                            // see #6161
                            if (filter == null || mailFilter.getPosition() < filter.getPosition()) {
                                filter = mailFilter;
                                email.setRecipient(recipient);
                                modifiedProperties.add(Email.PROPERTY_RECIPIENT);
                            }
                        }
                    }

                    if (filter == null) {
                        if (log.isDebugEnabled()) {
                            log.debug(" ==> No filter found for this message");
                            if (log.isTraceEnabled()) {
                                for (String recipient : recipients) {
                                    log.trace(" - for recipient " + recipient);
                                }
                            }
                        }

                        //  on garde le mail sur le serveur pour le traiter suite à l'ajout d'un futur filtre
                        continue;
                    }

                    // find company of the filter folder
                    final MailFolder filterFolder = filter.getMailFolder();
                    MailFolder withCompanyFolder = filterFolder;
                    while (!withCompanyFolder.isUseCurrentLevelCompany() && withCompanyFolder.getParent() != null) {
                        withCompanyFolder = withCompanyFolder.getParent();
                    }
                    String company = withCompanyFolder.getCompany();

                    List<String> brands = configurationService.getBrandsForEmailAddress(email.getRecipient());

                    Address[] addresses = message.getFrom();
                    if (addresses != null && addresses.length > 0) {
                        String sender = FaxToMailServiceUtils.getDecodedFrom(addresses[0].toString());

                        // Identification du client en fonction du numéro de fax appelant ou de l'adresse e-mail émettrice
                        List<Client> clients = clientService.getClientForEmailAddress(sender, email, company, brands);
                        modifiedProperties.add(Email.PROPERTY_SENDER);

                        String object;
                        Client client = null;
                        if (CollectionUtils.isNotEmpty(clients)) {
                            //TODO echatellier : a valider que l'on affiche bien le premier mail du client

                            client = clients.get(0);

                            String contact = sender;
                            if (email.isFax()) {
                                String faxNumber = contact.substring(0, contact.indexOf('@')).replaceAll(" ", "");
                                // NumberUtils.isNumber peut echouer (notation octal)
                                if (StringUtils.isNumeric(faxNumber)) {
                                    contact = StringUtils.leftPad(faxNumber, 10, '0');
                                }
                            }

                            List<String> objectItems = new ArrayList<>();
                            if (StringUtils.isNotEmpty(client.getCaracteristic1())) {
                                objectItems.add(client.getCaracteristic1());
                            }
                            if (StringUtils.isNotEmpty(client.getCaracteristic2())) {
                                objectItems.add(client.getCaracteristic2());
                            }
                            if (StringUtils.isNotEmpty(client.getCaracteristic3())) {
                                objectItems.add(client.getCaracteristic3());
                            }
                            if (StringUtils.isNotEmpty(client.getCode())) {
                                objectItems.add(client.getCode());
                            }
                            if (StringUtils.isNotEmpty(client.getName())) {
                                objectItems.add(client.getName());
                            }
                            objectItems.add(contact);
                            objectItems.add(DateFormat.getDateInstance(DateFormat.MEDIUM).format(new Date()));

                            object = StringUtils.join(objectItems, " / ");

                            modifiedProperties.add(Email.PROPERTY_CLIENT);

                        } else {
                            object = t("faxtomail.email.object.noClient");
                        }
                        email.setObject(object);
                        email.setClient(client);
                        email.setMatchingClients(clients);
                        modifiedProperties.add(Email.PROPERTY_OBJECT);

                        MailFolder mailFolder = getMailFolderForFilter(filter, client);
                        email.setMailFolder(mailFolder);
                        modifiedProperties.add(Email.PROPERTY_MAIL_FOLDER);

                        // if client is null and folder
                        if (client == null && emailAccount.isRejectAllowed()) {
                            String rejectMessage = null;
                            String rejectSiging = null;
                            String senderEmail = null;

                            // find reject conf
                            MailFolder rejectMailFolder = mailFolder;
                            while (rejectMailFolder != null
                                    && (!rejectMailFolder.isUseCurrentLevelRejectResponseMessage()
                                            || !rejectMailFolder.isUseCurrentLevelRejectResponseMailAddress())) {

                                if (rejectMessage == null && rejectMailFolder.isUseCurrentLevelRejectResponseMessage()) {
                                    rejectMessage = rejectMailFolder.getRejectResponseMessage();
                                    rejectSiging = rejectMailFolder.getRejectResponseSigning();
                                }
                                if (senderEmail == null && rejectMailFolder.isUseCurrentLevelRejectResponseMailAddress()) {
                                    senderEmail = rejectMailFolder.getRejectResponseMailAddress();
                                }
                                rejectMailFolder = rejectMailFolder.getParent();
                            }

                            if (StringUtils.isNoneBlank(senderEmail, rejectMessage)) {
                                // unknown client -> message rejected
                                String recipient = email.getSender();
                                if (email.isFax()) {
                                    recipient = FaxToMailServiceUtils.addFaxDomainToFaxNumber(recipient, mailFolder);
                                }
                                emailService.sendHtmlEmail(senderEmail,
                                                           recipient,
                                                           t("faxtomail.email.subject.re", message.getSubject()),
                                                           rejectMessage,
                                                           rejectSiging);

                                // important, delete mail
                                deleteMail(message);
                            }

                            continue;
                        }

                    } else {
                        email.setMailFolder(filterFolder);
                    }


                    //JC180423 : Ne pas utiliser message.getReceivedDate() -> Date souvent vide/nulle
                    Date receivedDate = new Date();
                    email.setReceptionDate(receivedDate);
                    modifiedProperties.add(Email.PROPERTY_RECEPTION_DATE);

                    Date now = new Date();

                    Decorator<Date> dateDecorator = decoratorService.getDecoratorByType(Date.class, DecoratorService.DATE);
                    String projectRef = t("faxtomail.email.projectReference.default", dateDecorator.toString(now));
                    email.setProjectReference(projectRef);
                    modifiedProperties.add(Email.PROPERTY_PROJECT_REFERENCE);

                    email.setDemandStatus(DemandStatus.UNTREATED);
                    modifiedProperties.add(Email.PROPERTY_DEMAND_STATUS);

                    OriginalEmail originalEmail = emailService.originalEmailFromMessage((MimeMessage) message, charset);
                    email.setOriginalEmail(originalEmail);

                    if (message.getSubject() != null) {
                        email.setSubject(FaxToMailServiceUtils.getDecodedSubject(message.getSubject()));
                    }

                    List<Attachment> attachments = emailService.extractAttachmentsFromMessage((MimeMessage) message);

                    for (Attachment attachment:attachments) {
                        if (isFileAPDF(attachment.getOriginalFile())) {
                            //Pre-generate PDF rendering
                            //Open file once to get page number
                            int pageNumber = 0;
                            try (PDDocument pdDocument = PDDocument.load(attachment.getOriginalFile().getContent())) {
                                pageNumber = pdDocument.getNumberOfPages();
                            } catch (IOException eee) {
                                log.error("Could not pre-generate PDF rendering from : " + email.getSender() + " on mailbox : " + email.getRecipient());
                            }
                            //Open file once per page to limit memory overhead
                            for (int i = 0; i < pageNumber; i++) {
                                try (PDDocument pdDocument = PDDocument.load(attachment.getOriginalFile().getContent())) {
                                    PDFRenderer renderer = new PDFRenderer(pdDocument);
                                    BufferedImage pageImage = renderer.renderImage(i);
                                    emailService.createGeneratedPDFPage(attachment, pageImage);
                                }
                            }
                        } else if (isFileAHeic(attachment.getOriginalFile())) {

                            //Create src temp file from attachment
                            File srcFile = File.createTempFile("input",".heic");
                            srcFile.createNewFile();
                            Files.write(attachment.getOriginalFile().getContent(), srcFile);

                            //Create dest temp file
                            File destFile = File.createTempFile("output",".png");

                            //Convert using imageMagick
                            Process process = new ProcessBuilder(config.getImageMagickLocation(),"convert", srcFile.getAbsolutePath(),destFile.getAbsolutePath()).start();

                            //Log imagemagick errors if any
                            InputStream is = process.getErrorStream();
                            InputStreamReader isr = new InputStreamReader(is);
                            BufferedReader br = new BufferedReader(isr);
                            String line;
                            while ((line = br.readLine()) != null) {
                                if (log.isWarnEnabled()) {
                                    log.warn(line);
                                }
                            }
                            //Wait for process to finish
                            int exitCode = process.waitFor();
                            if (exitCode != 0) {
                                if (log.isWarnEnabled()) {
                                    log.warn("Could not convert heic file");
                                }
                            }

                            //Read destFile and put it back into attachment
                            attachment.getOriginalFile().setContent(IOUtils.toByteArray(destFile.toURI()));
                            attachment.getOriginalFile().setFilename(destFile.getName());
                        }
                    }

                    emailService.saveEmail(email,
                            attachments,
                            null,
                            null,
                            modifiedProperties.toArray(new String[modifiedProperties.size()]));
                    importedCount++;

                    if (log.isDebugEnabled()) {
                        log.debug(" ==> Message placé dans le dossier " + email.getMailFolder().getName());
                    }

                    // send acknowledgement if needed
                    MailFolder rootFolder = email.getMailFolder();
                    while (rootFolder.getParent() != null) {
                        rootFolder = rootFolder.getParent();
                    }
                    boolean sendAcknowledgementToSender = BooleanUtils.isTrue(rootFolder.getSendAcknowledgementToSender())
                                                          && !rootFolder.containsAcknowledgementException(email.getClient())
                            || BooleanUtils.isNotTrue(rootFolder.getSendAcknowledgementToSender())
                                && rootFolder.containsAcknowledgementException(email.getClient());
                    if (log.isDebugEnabled()) {
                        log.debug(email.getClient());
                        log.debug(rootFolder.containsAcknowledgementException(email.getClient()));
                        log.debug("Send acknowledgement ? " + sendAcknowledgementToSender);
                    }

                    if (sendAcknowledgementToSender) {

                        String acknowledgementMailAddress = rootFolder.getAcknowledgementMailAddress();
                        String acknowledgementMessage = rootFolder.getAcknowledgementMessage();

                        if (StringUtils.isAnyBlank(acknowledgementMailAddress, acknowledgementMessage)) {
                            if (log.isErrorEnabled()) {
                                log.error("Can't send acknowledgement message due to invalid configuration");
                            }
                        } else {
                            String recipient = email.getSender();
                            if (email.isFax()) {
                                recipient = FaxToMailServiceUtils.addFaxDomainToFaxNumber(recipient, email.getMailFolder());
                            }
                            if (log.isDebugEnabled()) {
                                log.debug("Send acknowledgement to " + recipient);
                            }

                            try {
                                emailService.sendHtmlEmail(acknowledgementMailAddress,
                                        recipient,
                                        t("faxtomail.email.subject.re", message.getSubject()),
                                        acknowledgementMessage,
                                        rootFolder.getAcknowledgementSigning());
                            } catch (EmailException eee) {
                                log.warn("Error sending acknowledgement to " + recipient + "(" + eee.getMessage()+")");
                            }
                        }

                    }

                    // important, delete mail
                    deleteMail(message);

                } catch (Exception | StackOverflowError | OutOfMemoryError e) {
                    log.error("Error while reading the email from " +
                            emailAccount.getLogin() + "@" +  emailAccount.getHost(), e);
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("End of emails");
            }

        } catch (Exception e) {
            log.error("Error while reading the emails from " +
                    emailAccount.getLogin() + "@" +  emailAccount.getHost(), e);
        }

        // usefull log info (do not remove)
        if (importedCount > 0 && log.isInfoEnabled()) {
            log.info(String.format("Imported %d mail for account %s@%s", importedCount, emailAccount.getLogin(), emailAccount.getHost()));
        }
    }

    private MailFolder getMailFolderForFilter(final MailFilter filter, final Client client) {
        MailFolder mailFolder = null;
        if (!filter.isFilterFolderPriority() && client != null) {
            FaxToMailUser personInCharge = client.getPersonInCharge();
            if (personInCharge != null) {
                mailFolder = mailFolderService.getFolderForFaxToMailUser(personInCharge);
            }
        }

        if (mailFolder == null) {
            // to default folder
            mailFolder = filter.getMailFolder();
        }
        return mailFolder;
    }

    /**
     * Supprime le mail sur le serveur (si nécessaire).
     * 
     * @param message message to delete
     * @throws MessagingException 
     */
    protected void deleteMail(Message message) throws MessagingException {
        // suppression des mails sur le serveur distant (non actif par default)
        if (config.isMailDelete()) {
            message.setFlag(Flags.Flag.DELETED, true);
        }
    }

    public static boolean isFileAPDF(AttachmentFile file) {
        return FileUtil.extension(file.getFilename()).toUpperCase().equals("PDF");
    }

    public static boolean isFileAHeic(AttachmentFile file) {
        return FileUtil.extension(file.getFilename()).toUpperCase().equals("HEIC");
    }
}
