package com.franciaflex.faxtomail.web.action;

/*
 * #%L
 * FaxToMail :: Web
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFileImpl;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.History;
import com.franciaflex.faxtomail.persistence.entities.OriginalEmail;
import com.franciaflex.faxtomail.persistence.entities.RangeRow;
import com.franciaflex.faxtomail.persistence.entities.Reply;
import com.franciaflex.faxtomail.persistence.entities.ReplyContent;
import com.franciaflex.faxtomail.services.DecoratorService;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.web.FaxToMailActionSupport;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Result;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.nuiton.decorator.Decorator;
import org.nuiton.topia.persistence.TopiaNoResultException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 1.1
 */

@InterceptorRefs({
     @InterceptorRef("faxToMailInterceptor"),
     @InterceptorRef("paramsPrepareParamsStack")
})
public class DemandDetailAction extends FaxToMailActionSupport {

    private static final Log log = LogFactory.getLog(DemandDetailAction.class);

    protected EmailService emailService;
    protected DecoratorService decoratorService;

    protected String id;
    protected boolean original;
    protected int index;

    protected Email demand;

    protected int quotationTotal;
    protected int productTotal;
    protected int savTotal;

    protected EmailUIModel emailUIModel;

    protected InputStream attachmentFileInputStream;
    protected String fileName;
    protected String contentType;

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void setDecoratorService(DecoratorService decoratorService) {
        this.decoratorService = decoratorService;
    }

    @Override
    @Action("demand-detail")
    public String execute() throws Exception {
        String result = SUCCESS;
        try {
            demand = emailService.getEmailById(id);
            computeTotals();
            decomposeEmail();

        } catch (TopiaNoResultException e) {
            result = ERROR;
            addActionError(t("faxtomail.demandDetail.noDemandForId", id));
        }
        return result;
    }

    @Action(value = "attachment-download",
            results = {@Result(name = "success",
                               type = "stream",
                               params = {
                                       "contentType", "${contentType}",
                                       "inputName", "attachmentFileInputStream",
                                       "contentDisposition", "attachment;filename=\"${fileName}\"",
                                       "bufferSize", "1024"
                                })
                        })
    public String dlAttachment() throws Exception {
        AttachmentFile attachmentFile = emailService.getAttachmentFile(id, original);
        File file = attachmentFile.getFile();
        fileName = attachmentFile.getFilename();
        contentType = Files.probeContentType(file.toPath());
        attachmentFileInputStream = new FileInputStream(file);
        return SUCCESS;
    }

    @Action(value = "reply-attachment-download",
            results = {@Result(name = "success",
                               type = "stream",
                               params = {
                                       "contentType", "${contentType}",
                                       "inputName", "attachmentFileInputStream",
                                       "contentDisposition", "attachment;filename=\"${fileName}\"",
                                       "bufferSize", "1024"
                               })
            })
    public String dlReplyAttachment() throws Exception {
        ReplyContent replyContent = emailService.getReplyContent(id);

        MimeMessage message = new MimeMessage(null, new ByteArrayInputStream(replyContent.getSource()));
        EmailUIModel replyModel = new EmailUIModel();
        if (message.isMimeType("multipart/*")) {
            decomposeMultipartEmail(message, replyModel);
        } else {
            String content = FaxToMailServiceUtils.getTextFromMessage(message);
            replyModel.setPlainContent(content);
        }

        AttachmentFile attachmentFile = replyModel.getAttachments().get(index);
        File file = attachmentFile.getFile();
        fileName = attachmentFile.getFilename();
        contentType = Files.probeContentType(file.toPath());
        attachmentFileInputStream = new FileInputStream(file);
        return SUCCESS;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOriginal(boolean original) {
        this.original = original;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Email getDemand() {
        return demand;
    }

    public int getQuotationTotal() {
        return quotationTotal;
    }

    public int getProductTotal() {
        return productTotal;
    }

    public int getSavTotal() {
        return savTotal;
    }

    public EmailUIModel getEmailUIModel() {
        return emailUIModel;
    }

    public InputStream getAttachmentFileInputStream() {
        return attachmentFileInputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public List<EmailUIModel> getReplies() throws Exception {
        Preconditions.checkNotNull(demand);
        List<EmailUIModel> result = new ArrayList<>();

        for (Reply reply : demand.getReplies()) {
            ReplyContent replyContent = reply.getReplyContent();
            MimeMessage message = new MimeMessage(null, new ByteArrayInputStream(replyContent.getSource()));

            EmailUIModel replyModel = new EmailUIModel();
            replyModel.setId(reply.getTopiaId());
            replyModel.setSubject(message.getSubject());

            String toRecipient = message.getRecipients(Message.RecipientType.TO)[0].toString();
            replyModel.setToRecipients(Lists.newArrayList(toRecipient));

            replyModel.setSender(message.getFrom()[0].toString());

            if (ArrayUtils.isNotEmpty(message.getRecipients(Message.RecipientType.CC))) {
                String ccRecipient = message.getRecipients(Message.RecipientType.CC)[0].toString();
                replyModel.setCcRecipients(Lists.newArrayList(ccRecipient));
            }
            if (ArrayUtils.isNotEmpty(message.getRecipients(Message.RecipientType.BCC))) {
                String bccRecipient = message.getRecipients(Message.RecipientType.BCC)[0].toString();
                replyModel.setBccRecipients(Lists.newArrayList(bccRecipient));
            }

            if (message.isMimeType("multipart/*")) {
                decomposeMultipartEmail(message, replyModel);

            } else {
                String content = FaxToMailServiceUtils.getTextFromMessage(message);
                replyModel.setPlainContent(content);
            }

//            replyModel.setPlainContent("test<mailto:test2");
            result.add(replyModel);
        }

        return result;
    }

    public List<History> getHistories() {
        Preconditions.checkNotNull(demand);
        List<History> result = new ArrayList<>(demand.getHistory());

        Collections.sort(result, Ordering.natural().onResultOf(new Function<History, Comparable>() {
            @Override
            public Comparable apply(History history) {
                return history.getModificationDate();
            }
        }));

        return result;
    }

    public String decorate(Object o) {
        return decorate(o, "&nbsp;");
    }

    public String decorateUser(FaxToMailUser user) {
        return decorate(user, t("faxtomail.systemUser"));
    }

    protected String decorate(Object o, String defaultValue) {
        String result = null;
        if (o != null) {
            Decorator decorator = decoratorService.getDecoratorByType(o.getClass());
            if (decorator != null) {
                result = decorator.toString(o);
            } else {
                result = o.toString();
            }
        }
        if (StringUtils.isBlank(result)) {
            result = defaultValue;
        } else {
            result = result.replaceAll("(\r\n|\n)", "<br />");
        }
        return result;
    }

    protected void computeTotals() {
        quotationTotal = 0;
        productTotal = 0;
        savTotal = 0;

        if (demand != null && demand.getRangeRow() != null) {
            for (RangeRow row : demand.getRangeRow()) {
                if (row.getQuotationQuantity() != null) {
                    quotationTotal += row.getQuotationQuantity();
                }
                if (row.getProductQuantity() != null) {
                    productTotal += row.getProductQuantity();
                }
                if (row.getSavQuantity() != null) {
                    savTotal += row.getSavQuantity();
                }
            }
        }
    }

    protected void decomposeEmail() {
        emailUIModel = new EmailUIModel();
        try {
            // ce code peut provoquer une NPE avec les données de test
            OriginalEmail originalEmail = demand.getOriginalEmail();
            String originalEmailContent = originalEmail.getContent();
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(originalEmailContent.getBytes(StandardCharsets.UTF_8));
            MimeMessage message = new MimeMessage(null, byteArrayInputStream);
            emailUIModel.setSubject(message.getSubject());

            List<String> toRecipients = FaxToMailServiceUtils.convertAddressesToStrings(message.getRecipients(Message.RecipientType.TO));
            emailUIModel.setToRecipients(toRecipients);

            List<String> ccRecipients = FaxToMailServiceUtils.convertAddressesToStrings(message.getRecipients(Message.RecipientType.CC));
            emailUIModel.setCcRecipients(ccRecipients);

            if (message.isMimeType("multipart/*")) {
                decomposeMultipartEmail(message, emailUIModel);

            } else if (message.isMimeType("text/*")) {
                String content = FaxToMailServiceUtils.getTextFromMessage(message);
                emailUIModel.setPlainContent(content);
            }

        } catch (Exception e) {
            //TODO kmorin 20140516 do something when we use the real data
            if (log.isErrorEnabled()) {
                log.error("", e);
            }
        }
    }

    /**
     * Decompose a multipart part.
     * - sets the email content if the part contains a text bodypart
     * - adds attachments to the email
     *
     * @param part the part to decompose
     * @throws Exception
     */
    protected void decomposeMultipartEmail(Part part, EmailUIModel emailUIModel) throws Exception {
        DataSource dataSource = part.getDataHandler().getDataSource();
        MimeMultipart mimeMultipart = new MimeMultipart(dataSource);
        int multiPartCount = mimeMultipart.getCount();

        for (int j = 0; j < multiPartCount; j++) {
            BodyPart bp = mimeMultipart.getBodyPart(j);

            // if it is a text part, the,n this is the email content
            String disposition = bp.getDisposition();
            if (bp.isMimeType("text/*") && !Part.ATTACHMENT.equals(disposition)) {
                String content = FaxToMailServiceUtils.getTextFromPart(bp);
                if (bp.isMimeType("text/plain")) {
                    emailUIModel.setPlainContent(content);
                } else {
                    emailUIModel.setHtmlContent(content);
                }

                // if it is multipart part, decompose it
            } else if (bp.isMimeType("multipart/*")) {
                decomposeMultipartEmail(bp, emailUIModel);

                // else, this is an attachment
            } else {
                String fileName = bp.getFileName();
                if (fileName == null) {
                    String[] header = bp.getHeader("Content-ID");
                    if (header != null && header.length > 0) {
                        fileName = header[0];
                        // remove the guillemets between the id
                        fileName = fileName.replaceFirst("^<(.*)>$", "$1");
                    }
                }
                if (fileName == null) {
                    fileName = t("faxtomail.email.content.attachment.unnamed", j);
                }

                ByteArrayOutputStream fos = new ByteArrayOutputStream();

                DataHandler dh = bp.getDataHandler();
                dh.writeTo(fos);

                // copy content into an empty attachment
                AttachmentFile attachmentFile = new AttachmentFileImpl();
                attachmentFile.setContent(fos.toByteArray());
                attachmentFile.setFilename(fileName);

                emailUIModel.addAttachmentFile(attachmentFile);
            }
        }
    }

    public static class EmailUIModel {

        protected String id;
        protected String subject;
        protected String sender;
        protected String toRecipients;
        protected String ccRecipients;
        protected String bccRecipients;
        protected String content;
        protected List<AttachmentFile> attachments = new ArrayList<>();

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getSender() {
            return sender;
        }

        public void setSender(String sender) {
            this.sender = sender;
        }

        public String getToRecipients() {
            return toRecipients;
        }

        public void setToRecipients(List<String> toRecipients) {
            this.toRecipients = StringUtils.join(toRecipients, ", ");
        }

        public String getCcRecipients() {
            return ccRecipients;
        }

        public void setCcRecipients(List<String> ccRecipients) {
            this.ccRecipients = StringUtils.join(ccRecipients, ", ");
        }

        public String getBccRecipients() {
            return bccRecipients;
        }

        public void setBccRecipients(List<String> bccRecipients) {
            this.bccRecipients = StringUtils.join(bccRecipients, ", ");
        }

        public void setPlainContent(String plainContent) {
            if (content == null) {
                this.content = plainContent == null ? null : plainContent.replaceAll("(\r\n|\n)", "<br />")
                                                                        // <mailto:...> throws an error in the ng-bind-html primitive in the UI
                                                                         .replaceAll("<mailto:([^\\>]*)>", "[mailto:$1]");
            }
        }

        public void setHtmlContent(String htmlContent) {
            Document document = Jsoup.parse(htmlContent);
            Element body = document.body();
            Element head = document.head();
            this.content = head.html() + body.outerHtml().replaceAll("<body", "<div").replaceAll("</body>", "</div>");
        }

        public String getContent() {
            return content;
        }

        public List<AttachmentFile> getAttachments() {
            return attachments;
        }

        public void addAttachmentFile(AttachmentFile attachmentFile) {
            attachments.add(attachmentFile);
        }
    }
}