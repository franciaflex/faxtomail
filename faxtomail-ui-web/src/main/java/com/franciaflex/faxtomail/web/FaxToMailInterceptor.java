package com.franciaflex.faxtomail.web;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailApplicationContext;
import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaPersistenceContext;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroup;
import com.franciaflex.faxtomail.services.FaxToMailService;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.LdapService;
import com.google.common.base.Preconditions;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionProxy;
import com.opensymphony.xwork2.interceptor.Interceptor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.dispatcher.HttpParameters;
import org.apache.struts2.dispatcher.Parameter;
import org.nuiton.util.beans.BeanUtil;

import java.beans.PropertyDescriptor;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FaxToMailInterceptor implements Interceptor {

    private static final Log log = LogFactory.getLog(FaxToMailInterceptor.class);

    public static final String SESSION_LAST_LOCATION = "lastLocation";

    @Override
    public void init() {

        if (log.isDebugEnabled()) {
            log.debug("init " + this);
        }

    }

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        Object action = invocation.getAction();

        if (action instanceof FaxToMailActionSupport) {

            FaxToMailServiceContext serviceContext = null;
            try {
                FaxToMailActionSupport faxToMailAction = (FaxToMailActionSupport) action;
    
                serviceContext = newServiceContext(invocation);
    
                FaxToMailSession faxToMailSession = getFaxToMailSession(invocation);
                populateUser(faxToMailSession, serviceContext);
    
                if (CollectionUtils.isNotEmpty(faxToMailSession.getMessages())) {
                    for (String message : faxToMailSession.getMessages()) {
                        log.info(message);
                        faxToMailAction.addActionMessage(message);
                    }
                    faxToMailSession.getMessages().clear();
                }
    
                if (CollectionUtils.isNotEmpty(faxToMailSession.getErrorMessages())) {
                    for (String message : faxToMailSession.getErrorMessages()) {
                        log.info(message);
                        faxToMailAction.addActionError(message);
                    }
                    faxToMailSession.getErrorMessages().clear();
                }
    
                Set<PropertyDescriptor> descriptors =
                        BeanUtil.getDescriptors(
                                action.getClass(),
                                BeanUtil.IS_WRITE_DESCRIPTOR);
    
                for (PropertyDescriptor propertyDescriptor : descriptors) {
    
                    Class<?> propertyType = propertyDescriptor.getPropertyType();
                    Object toInject = null;
    
                    if (FaxToMailService.class.isAssignableFrom(propertyType)) {
                        Class<? extends FaxToMailService> serviceClass =
                                (Class<? extends FaxToMailService>) propertyType;
                        toInject = serviceContext.newService(serviceClass);
    
                    } else if (FaxToMailSession.class.isAssignableFrom(propertyType)) {
                        toInject = faxToMailSession;
    
                    } else if (FaxToMailConfiguration.class.isAssignableFrom(propertyType)) {
                        toInject = getFaxToMailApplicationContext(invocation).getApplicationConfig();

                    } else if (FaxToMailApplicationContext.class.isAssignableFrom(propertyType)) {
                        toInject = getFaxToMailApplicationContext(invocation);
                    }
    
                    if (toInject != null) {
                        if (log.isTraceEnabled()) {
                            log.trace("injecting " + toInject + " in action " + action);
                        }
                        propertyDescriptor.getWriteMethod().invoke(action, toInject);
                    }
                }

                return invocation.invoke();

            } finally {
                if (serviceContext != null) {
                    serviceContext.getPersistenceContext().close();
                }
            }

        } else {

            // not an action, just process
            saveLastAction(invocation);
            return invocation.invoke();

        }

    }

    protected void populateUser(FaxToMailSession faxToMailSession, FaxToMailServiceContext serviceContext) {
        if (StringUtils.isNotBlank(faxToMailSession.getAuthenticatedUserId())) {
            LdapService ldapService = serviceContext.getLdapService();
            // return can be null
            FaxToMailUser user = ldapService.getUserBean(faxToMailSession.getAuthenticatedUserId());
            faxToMailSession.setAuthenticatedFaxToMailUser(user);
            
            // test si l'utilisateur est admin
            List<String> adminGroups = serviceContext.getApplicationConfig().getLdapAdminGroups();
            if (user != null && CollectionUtils.isNotEmpty(adminGroups)  && user.getUserGroups() != null) {
                for (FaxToMailUserGroup group : user.getUserGroups()) {
                    if (adminGroups.contains(group.getCompleteName())) {
                        faxToMailSession.setAdmin(true);
                    }
                }
            }
        }
    }

    protected void saveLastAction(ActionInvocation invocation) {
        Map<String, Object> session = invocation.getInvocationContext().getSession();
        ActionProxy proxy = invocation.getProxy();
        StringBuilder lastLocation = new StringBuilder(proxy.getNamespace() + "/" + proxy.getActionName() + "!" + proxy.getMethod() + "?");
        HttpParameters parameters = invocation.getInvocationContext().getParameters();
        for (Map.Entry<String, Parameter> entry : parameters.entrySet()) {
            lastLocation.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        session.put(SESSION_LAST_LOCATION, lastLocation);
    }

    protected FaxToMailSession getFaxToMailSession(ActionInvocation invocation) {

        FaxToMailSession session = (FaxToMailSession) invocation.getInvocationContext().getSession().get(FaxToMailSession.SESSION_PARAMETER);

        if (session == null) {
            session = new FaxToMailSession();
            invocation.getInvocationContext().getSession().put(FaxToMailSession.SESSION_PARAMETER, session);
        }

        return session;

    }

    protected FaxToMailApplicationContext getFaxToMailApplicationContext(ActionInvocation invocation) {

        FaxToMailApplicationContext applicationContext =
                (FaxToMailApplicationContext) invocation
                        .getInvocationContext()
                        .getApplication()
                        .get(FaxToMailApplicationContext.APPLICATION_CONTEXT_PARAMETER);

        Preconditions.checkNotNull(applicationContext,
                "application context must be initialized before calling an action");

        return applicationContext;

    }

    protected FaxToMailServiceContext newServiceContext(ActionInvocation invocation) {

        FaxToMailApplicationContext extranetEncAhiApplicationContext = getFaxToMailApplicationContext(invocation);
        FaxToMailTopiaPersistenceContext persistenceContext = extranetEncAhiApplicationContext.newPersistenceContext();
        FaxToMailServiceContext serviceContext = extranetEncAhiApplicationContext.newServiceContext(persistenceContext);

        return serviceContext;
    }

    @Override
    public void destroy() {
        if (log.isDebugEnabled()) {
            log.debug("destroy " + this);
        }
    }
}
