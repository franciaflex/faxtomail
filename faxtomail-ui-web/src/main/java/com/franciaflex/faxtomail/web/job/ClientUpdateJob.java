package com.franciaflex.faxtomail.web.job;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailApplicationContext;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaPersistenceContext;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.ClientService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.IOException;

/**
 * @author Kevin Morin (Code Lutin)
 */
@DisallowConcurrentExecution
public class ClientUpdateJob extends AbstractFaxToMailJob {

    private static final Log log = LogFactory.getLog(ClientUpdateJob.class);

    protected FaxToMailApplicationContext applicationContext;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        applicationContext = getApplicationContext(jobExecutionContext);
        //persistenceContext clos par le serviceContext
        FaxToMailTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext();

        try (FaxToMailServiceContext serviceContext = applicationContext.newServiceContext(persistenceContext)){

            if (log.isInfoEnabled()) {
                log.info("Running ClientUpdateJob at " + serviceContext.getNow());
            }

            ClientService clientService = serviceContext.getClientService();
            clientService.updateNewClients();

            if (log.isDebugEnabled()) {
                log.debug("ClientUpdateJob ended at " + serviceContext.getNow());
            }
        } catch (IOException eee) {
            log.error("Error executing client update job",eee);
        }
    }
}
