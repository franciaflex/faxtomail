package com.franciaflex.faxtomail.web.action.admin;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.services.service.ReferentielService;
import com.franciaflex.faxtomail.web.FaxToMailActionSupport;
import com.opensymphony.xwork2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Result;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@InterceptorRefs({
    @InterceptorRef("faxToMailInterceptor"),
    @InterceptorRef("loginInterceptor"),
    @InterceptorRef("paramsPrepareParamsStack")
})
public class ImportAction extends FaxToMailActionSupport implements Preparable {

    protected ReferentielService referentielService;

    protected File clientFile;
    protected File demandTypeFile;
    protected File rangeFile;
    protected File priorityFile;
    protected File waitingStateFile;

    public void setReferentielService(ReferentielService referentielService) {
        this.referentielService = referentielService;
    }

    public void setClientFile(File clientFile) {
        this.clientFile = clientFile;
    }

    public void setDemandTypeFile(File demandTypeFile) {
        this.demandTypeFile = demandTypeFile;
    }

    public void setRangeFile(File rangeFile) {
        this.rangeFile = rangeFile;
    }

    public void setPriorityFile(File priorityFile) {
        this.priorityFile = priorityFile;
    }

    public void setWaitingStateFile(File waitingStateFile) {
        this.waitingStateFile = waitingStateFile;
    }

    @Override
    public void prepare() throws Exception {
        // check authorization
        if (!getSession().isAdmin()) {
            throw new RuntimeException("Not authorized");
        }
    }

    @Override
    @Action("import-input")
    public String input() throws Exception {
        return INPUT;
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "import-input"})})
    public String execute() throws Exception { 

        // clientFile
        if (clientFile != null) {
            try (InputStream is = new FileInputStream(clientFile)) {
                referentielService.importClients(is);
                getSession().addMessage("Fichier client importé avec succes");
            } catch (Exception ex) {
                addActionError(ex.getMessage());
            }
        }

        // demandTypeFile
        if (demandTypeFile != null) {
            try (InputStream is = new FileInputStream(demandTypeFile)) {
                referentielService.importDemandTypes(is);
                getSession().addMessage("Fichier type de demande importé avec succes");
            } catch (Exception ex) {
                addActionError(ex.getMessage());
            }
        }

        // rangeFile
        if (rangeFile != null) {
            try (InputStream is = new FileInputStream(rangeFile)) {
                referentielService.importRanges(is);
                getSession().addMessage("Fichier gamme importé avec succes");
            } catch (Exception ex) {
                addActionError(ex.getMessage());
            }
        }

        // priorityFile
        if (priorityFile != null) {
            try (InputStream is = new FileInputStream(priorityFile)) {
                referentielService.importPriorities(is);
                getSession().addMessage("Fichier priorité importé avec succes");
            } catch (Exception ex) {
                addActionError(ex.getMessage());
            }
        }

        // waitingStateFile
        if (waitingStateFile != null) {
            try (InputStream is = new FileInputStream(waitingStateFile)) {
                referentielService.importWaitingStates(is);
                getSession().addMessage("Fichier état attente importé avec succes");
            } catch (Exception ex) {
                addActionError(ex.getMessage());
            }
        }

        return hasErrors() ? super.input() : super.execute();
    }
}
