package com.franciaflex.faxtomail.web;

/*
 * #%L
 * FaxToMail :: Web
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.ActionInvocation;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.interceptor.BackgroundProcess;
import org.apache.struts2.interceptor.ExecuteAndWaitInterceptor;

import java.io.File;

/**
 * Interceptor to copy the temp file to avoid it is deleted before the execAndWait thread has time to read it
 * cf http://stackoverflow.com/questions/22382779/file-not-saved-in-temp-path-using-struts2-with-execandwait-interceptor
 * @author Kevin Morin (Code Lutin)
 * @since 1.1
 */
public class FaxToMailExecuteAndWaitInterceptor extends ExecuteAndWaitInterceptor {

    @Override
    protected BackgroundProcess getNewBackgroundProcess(String name,
                                                        ActionInvocation actionInvocation,
                                                        int threadPriority) {

        BackgroundProcess bgProcess;
        if (actionInvocation.getAction() instanceof HasUploadedFile) {
            HasUploadedFile uploadAction = (HasUploadedFile) actionInvocation.getAction();
            try {
                File origFile = uploadAction.getUploadedFile();
                if (origFile != null) {
                    File altFile = new File(origFile.getParentFile(), origFile.getName() + "-alt.tmp");
                    FileUtils.copyFile(origFile, altFile);
                    altFile.deleteOnExit();
                    uploadAction.setUploadedFile(altFile);
                }
            } catch (Exception ex) {
                throw new RuntimeException("Error copying uploaded file", ex);
            }
            bgProcess = new UploadBackgroundProcess(name + "BackgroundThread", actionInvocation, threadPriority);
        } else {
            bgProcess = super.getNewBackgroundProcess(name, actionInvocation, threadPriority);
        }
        return bgProcess;
    }

    /**
     * Wraps the standard {@link BackgroundProcess} to clean up alternate file created above.
     */
    private class UploadBackgroundProcess extends BackgroundProcess {

        public UploadBackgroundProcess(String threadName, ActionInvocation invocation, int threadPriority) {
            super(threadName, invocation, threadPriority);
        }

        @Override
        protected void afterInvocation() throws Exception {
            super.afterInvocation();
            FileUtils.deleteQuietly(((HasUploadedFile)getAction()).getUploadedFile());
        }
    }
}
