package com.franciaflex.faxtomail.web;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;


/**
 * Abstract action used to render custom objects as json string using gson directly in response output stream.
 *
 * @author Eric Chatellier
 */
@Results({
        @Result(type = "faxtomail-json", name = Action.SUCCESS),
        @Result(type = "faxtomail-json", name = Action.ERROR)
})
public abstract class FaxToMailJsonAction extends FaxToMailActionSupport {

    /**
     * Method to override to get object data to render as json. Method HAS to be public because result support will use
     * this method.
     *
     * @return object to render as json
     */
    public abstract Object getJsonData();

}
