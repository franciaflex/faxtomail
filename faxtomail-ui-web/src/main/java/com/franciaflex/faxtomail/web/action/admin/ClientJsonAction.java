package com.franciaflex.faxtomail.web.action.admin;

/*-
 * #%L
 * FaxToMail :: Web
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.services.service.ClientService;
import com.franciaflex.faxtomail.web.FaxToMailJsonAction;
import org.apache.struts2.convention.annotation.Action;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class ClientJsonAction extends FaxToMailJsonAction {

    protected ClientService clientService;

    /** Start of the client name to search */
    protected String clientQuery;

    protected Object jsonData;

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    public void setClientQuery(String clientQuery) {
        this.clientQuery = clientQuery;
    }

    @Action("search-clients-json")
    public String searchClients() {
        jsonData = clientService.getAllClientsForEmailOrFax(clientQuery);
        return SUCCESS;
    }

    @Override
    public Object getJsonData() {
        return jsonData;
    }

}
