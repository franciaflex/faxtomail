package com.franciaflex.faxtomail.web.action.admin;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailLock;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.web.FaxToMailActionSupport;
import com.opensymphony.xwork2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Result;

import java.util.List;

@InterceptorRefs({
    @InterceptorRef("faxToMailInterceptor"),
    @InterceptorRef("loginInterceptor"),
    @InterceptorRef("paramsPrepareParamsStack")
})
public class LockAction extends FaxToMailActionSupport implements Preparable {

    protected EmailService emailService;

    protected List<MailLock> activeLocks;

    protected List<String> mailLockIds;

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Override
    public void prepare() throws Exception {
        // check authorization
        if (!getSession().isAdmin()) {
            throw new RuntimeException("Not authorized");
        }
    }

    @Override
    @Action("lock-input")
    public String input() throws Exception {
        activeLocks = emailService.getAllMailLocks();
        return INPUT;
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "lock-input"})})
    public String execute() throws Exception {
        if (mailLockIds != null) {
            emailService.unlockMails(mailLockIds);
        }
        return SUCCESS;
    }

    public List<MailLock> getActiveLocks() {
        return activeLocks;
    }
    
    public void setMailLockIds(List<String> mailLockIds) {
        this.mailLockIds = mailLockIds;
    }
}
