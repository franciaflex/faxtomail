package com.franciaflex.faxtomail.web.action.admin;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailApplicationContext;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaPersistenceContext;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.services.service.imports.ArchiveImportResult;
import com.franciaflex.faxtomail.web.FaxToMailActionSupport;
import com.franciaflex.faxtomail.web.HasUploadedFile;
import com.opensymphony.xwork2.Preparable;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Result;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * Action de reprise des archives.
 * 
 * @author Eric Chatellier
 */
@InterceptorRefs({
    @InterceptorRef("faxToMailInterceptor"),
    @InterceptorRef("loginInterceptor"),
    @InterceptorRef("paramsPrepareParamsStack"),
    @InterceptorRef(value="faxToMailExecAndWait",
                    params={"excludeMethods", "input,downloadImportErrorFile", "delay", "1000"})
})
public class ImportArchiveAction extends FaxToMailActionSupport implements Preparable, HasUploadedFile {

    private static final Log log = LogFactory.getLog(ImportArchiveAction.class);

    protected String attachmentBase;

    protected File archiveFile;
    protected String archiveFileFileName;
    protected String archiveFileContentType;

    protected FaxToMailApplicationContext applicationContext;

    protected ArchiveImportResult importResult;

    protected InputStream inputStream;
    protected String fileName;
    protected String contentType;

    public void setApplicationContext(FaxToMailApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void prepare() throws Exception {
        // check authorization
        if (!getSession().isAdmin()) {
            throw new RuntimeException("Not authorized");
        }
    }
    
    @Override
    @Action("import-archive-input")
    public String input() throws Exception {
        return super.input();
    }

    @Override
    @Action(results = {
            @Result(name="wait", location="/WEB-INF/content/admin/import-archive-wait.jsp"),
            @Result(name="*", type = "chain", params = {"actionName", "import-archive-input"})})
    public String execute() throws Exception {
        String result = SUCCESS;

        if (StringUtils.isBlank(fileName)) {
            if (StringUtils.isNotBlank(attachmentBase) && !new File(attachmentBase).isDirectory()) {
                getSession().addErrorMessages("Le répertoire de base des pièces jointes n'est pas un répertoire lisible !");
                return ERROR;
            }
            if (archiveFile == null) {
                getSession().addErrorMessages("Le fichier d'archive est obligatoire !");
                return ERROR;
            }
        }

        // cette methode doit ouvrir sa propre connexion sinon l'interceptor va la fermer
        // and cause du execAndWait
        try (FaxToMailTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext();
             FaxToMailServiceContext serviceContext = applicationContext.newServiceContext(persistenceContext)){

            EmailService emailService = serviceContext.getEmailService();

            try (InputStream is = new FileInputStream(archiveFile)) {
                File file = StringUtils.isNotBlank(attachmentBase) ? new File(attachmentBase) : null;

                importResult = emailService.importArchive(is, file);
                if (log.isInfoEnabled()) {
                    log.info(importResult.getNbImportedArchives() + " imported archives");
                }

            } catch (Exception ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't import archive file", ex);
                }
                getSession().addErrorMessages("Erreur lors de l'import : " + ex.getMessage());

                result = ERROR;
            }

        }

        return result;
    }

    @Action(value = "download-import-archive-errorfile",
            results = {@Result(name = "success",
                               type = "stream",
                               params = {
                                       "contentType", "${contentType}",
                                       "inputName", "inputStream",
                                       "contentDisposition", "attachment;filename=\"${fileName}\"",
                                       "bufferSize", "1024"
                               })
            })
    public String downloadImportErrorFile() throws Exception {
        File file = new File(getApplicationConfig().getDataDirectory(), fileName);
        if (log.isDebugEnabled()) {
            log.debug(fileName + " exists : " + file.exists());
        }
        contentType = Files.probeContentType(file.toPath());
        inputStream = new FileInputStream(file);
        return SUCCESS;
    }


    public void setArchiveFile(File archiveFile) {
        this.archiveFile = archiveFile;
    }

    public void setAttachmentBase(String attachmentBase) {
        this.attachmentBase = attachmentBase;
    }

    public String getArchiveFileFileName() {
        return archiveFileFileName;
    }

    public void setArchiveFileFileName(String archiveFileFileName) {
        this.archiveFileFileName = archiveFileFileName;
    }

    public String getArchiveFileContentType() {
        return archiveFileContentType;
    }

    public void setArchiveFileContentType(String archiveFileContentType) {
        this.archiveFileContentType = archiveFileContentType;
    }

    public ArchiveImportResult getImportResult() {
        return importResult;
    }

    public void setImportResult(ArchiveImportResult importResult) {
        this.importResult = importResult;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    @Override
    public File getUploadedFile() {
        return archiveFile;
    }

    @Override
    public void setUploadedFile(File file) {
        archiveFile = file;
    }
}
