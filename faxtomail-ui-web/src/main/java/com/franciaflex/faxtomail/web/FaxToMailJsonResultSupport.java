package com.franciaflex.faxtomail.web;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.result.StrutsResultSupport;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FaxToMailJsonResultSupport extends StrutsResultSupport {

    private static final Log log = LogFactory.getLog(FaxToMailJsonResultSupport.class);

    @Override
    protected void doExecute(String finalLocation, ActionInvocation invocation) throws Exception {
        Object jsonData = invocation.getStack().findValue("jsonData");

        String json = FaxToMailActionSupport.getGson().toJson(jsonData);

        // Work-arround for IE to not display download dialog for json result
        // see https://github.com/blueimp/jQuery-File-Upload/issues/1795
        HttpServletRequest servletRequest = (HttpServletRequest) invocation.getInvocationContext().get(HTTP_REQUEST);
        HttpServletResponse servletResponse = (HttpServletResponse) invocation.getInvocationContext().get(HTTP_RESPONSE);
        servletResponse.setCharacterEncoding(Charsets.UTF_8.name());
        if (servletRequest.getHeader("accept").indexOf("application/json") != -1) {
            servletResponse.setContentType("application/json");
        } else {
            // IE workaround
            servletResponse.setContentType("text/plain");
        }
        
        // manage error code
        if (!Action.SUCCESS.equals(invocation.getResultCode())) {
            servletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

        try {
            ServletOutputStream outputStream = servletResponse.getOutputStream();
            byte[] jsonBytes = json.getBytes(Charsets.UTF_8); // On transforme en bytes pour assurer l'encodage
            outputStream.write(jsonBytes);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to write JSON output into Servlet Response");
            }
        }

    }

}
