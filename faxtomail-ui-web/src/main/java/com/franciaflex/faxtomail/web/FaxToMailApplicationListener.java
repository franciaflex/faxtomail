package com.franciaflex.faxtomail.web;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailApplicationContext;
import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.web.job.AbstractFaxToMailJob;
import com.franciaflex.faxtomail.web.job.ClientUpdateJob;
import com.franciaflex.faxtomail.web.job.EDIManagementJob;
import com.franciaflex.faxtomail.web.job.ImageCleanupJob;
import com.franciaflex.faxtomail.web.job.MailFilterJob;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class FaxToMailApplicationListener implements ServletContextListener {

    private static final Log log = LogFactory.getLog(FaxToMailApplicationListener.class);

    protected FaxToMailApplicationContext applicationContext;

    protected Scheduler scheduler;

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        if (log.isInfoEnabled()) {
            log.info("init FaxToMail web ui");
        }

        log.debug("scheduler launched");

        ServletContext sc = sce.getServletContext();
        applicationContext = new FaxToMailApplicationContext();
        applicationContext.init(sc);

        sce.getServletContext().setAttribute(
                FaxToMailApplicationContext.APPLICATION_CONTEXT_PARAMETER,
                applicationContext);
        
        FaxToMailConfiguration config = applicationContext.getApplicationConfig();

        JobDataMap data = new JobDataMap();
        
        data.put(AbstractFaxToMailJob.APPLICATION_CONTEXT, applicationContext);

        JobDetail mailFilteringJob = JobBuilder.newJob(MailFilterJob.class)
                                      .usingJobData(data)
                                      .withIdentity("faxToMailJobs", "mailFilteringJob")
                                      .build();

        JobDetail ediManagementJob = JobBuilder.newJob(EDIManagementJob.class)
                                    .usingJobData(data)
                                    .withIdentity("faxToMailJobs", "ediManagementJob")
                                    .build();

        JobDetail clientUpdateJob = JobBuilder.newJob(ClientUpdateJob.class)
                .usingJobData(data)
                .withIdentity("faxToMailJobs", "clientUpdateJob")
                .build();

        JobDetail imageCleanupJob = JobBuilder.newJob(ImageCleanupJob.class)
                .usingJobData(data)
                .withIdentity("faxToMailJobs", "imageCleanupJob")
                .build();

        try {

            scheduler = new StdSchedulerFactory().getScheduler();

            // schedule mail job (toutes les 5 minutes)
            Trigger trigger = TriggerBuilder
                            .newTrigger()
                            .withIdentity("mailFiltering", "mailFilteringTrigger")
                            .withSchedule(CronScheduleBuilder.cronSchedule(config.getJobMailExpression()))
                            .build();

            scheduler.scheduleJob(mailFilteringJob, trigger);

            // schedule EDI job (toutes les 5 minutes)
            trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("ediManagement", "ediManagementTrigger")
                    .withSchedule(CronScheduleBuilder.cronSchedule(config.getJobEdiExpression()))
                    .build();
            
            scheduler.scheduleJob(ediManagementJob, trigger);

            // schedule client updates (toutes les 15 minutes)
            trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("clientUpdates", "clientUpdatesTrigger")
                    .withSchedule(CronScheduleBuilder.cronSchedule(config.getJobCientExpression()))
                    .build();

            scheduler.scheduleJob(clientUpdateJob, trigger);

            // schedule image cleanup job (cron en config)
            trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("imageCleanup", "imageCleanupTrigger")
                    .withSchedule(CronScheduleBuilder.cronSchedule(config.getImageCleanupExpression()))
                    .build();

            scheduler.scheduleJob(imageCleanupJob, trigger);

            scheduler.start();
            log.debug("schedulers launched");

        } catch (SchedulerException e) {
            log.error("Error while launching the mail filter job", e);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

        if (scheduler != null) {
            if (log.isInfoEnabled()) {
                if (log.isInfoEnabled()) {
                    log.info("Stopping quartz sheduler");
                }
            }

            try {
                // wait for thread to complete
                scheduler.shutdown();
            } catch (SchedulerException e) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't stop quartz", e);
                }
            }
        }

        if (log.isInfoEnabled()) {
            log.info("Closing web ui application context");
        }

        applicationContext.close();

    }

}
