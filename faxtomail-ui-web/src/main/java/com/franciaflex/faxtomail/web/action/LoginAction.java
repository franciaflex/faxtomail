package com.franciaflex.faxtomail.web.action;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.services.service.LdapService;
import com.franciaflex.faxtomail.services.service.ldap.AuthenticationException;
import com.franciaflex.faxtomail.web.FaxToMailActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

public class LoginAction extends FaxToMailActionSupport {

    protected LdapService ldapService;

    protected String login;

    protected String password;

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    @Action("login-input")
    public String input() throws Exception {
        return super.input();
    }

    @Override
    @Action(results = {
            @Result(type = "redirectAction", params = {"actionName", "index", "namespace", "/"})})
    public String execute() {

        String result = SUCCESS;
        try {
            FaxToMailUser user = ldapService.authenticateUser(login, password);
            getSession().setAuthenticatedUserId(user.getTopiaId());
        } catch (AuthenticationException ex) {
            result = INPUT;
            addActionError("Impossible de se connecter (Le serveur à répondu : " + ex.getMessage() + ")");
        }
        return result;
    }
}
