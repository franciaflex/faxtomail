package com.franciaflex.faxtomail.web.action.admin;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.services.service.ConfigurationService;
import com.franciaflex.faxtomail.services.service.MailFolderService;
import com.franciaflex.faxtomail.web.FaxToMailActionSupport;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Result;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@InterceptorRefs({
    @InterceptorRef("faxToMailInterceptor"),
    @InterceptorRef("loginInterceptor"),
    @InterceptorRef("paramsPrepareParamsStack")
})
public class UserFolderAction extends FaxToMailActionSupport {

    protected ConfigurationService configurationService;

    protected MailFolderService mailFolderService;

    protected List<MailFolder> mailFolders;

    protected Set<FaxToMailUser> users;

    protected Map<String, Collection<MailFolder>> userFolders;

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public void setMailFolderService(MailFolderService mailFolderService) {
        this.mailFolderService = mailFolderService;
    }

    @Override
    @Action("user-folder-input")
    public String input() throws Exception {
        mailFolders = mailFolderService.getRootMailFolders();
        users = configurationService.getUserManagedUsers(getSession().getAuthenticatedFaxToMailUser());
        
        // build display map
        userFolders = new HashMap<>();
        for (FaxToMailUser user : users) {
            userFolders.put(user.getTopiaId(), CollectionUtils.emptyIfNull(user.getAffectedFolders()));
        }
        return INPUT;
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "user-folder-input"})})
    public String execute() throws Exception {

        configurationService.saveUserFolders(userFolders);
        return SUCCESS;
    }

    public List<MailFolder> getMailFolders() {
        return mailFolders;
    }

    public Set<FaxToMailUser> getUsers() {
        return users;
    }

    public Map<String, Collection<MailFolder>> getUserFolders() {
        return userFolders;
    }
    
    public void setUserFoldersJson(String json) {
        Type type = new TypeToken<Map<String, Collection<MailFolder>>>() {}.getType();
        this.userFolders = getGson().fromJson(json, type);
    }
}
