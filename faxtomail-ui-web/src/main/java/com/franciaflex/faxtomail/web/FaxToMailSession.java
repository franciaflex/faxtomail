package com.franciaflex.faxtomail.web;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Collection;

public class FaxToMailSession implements Serializable {

    public static final String SESSION_PARAMETER = "faxToMailSession";

    protected Collection<String> messages;

    protected Collection<String> errorMessages;

    protected String authenticatedUserId;

    protected transient FaxToMailUser authenticatedFaxToMailUser;
    
    protected transient boolean admin;

    public Collection<String> getMessages() {
        if (messages == null) {
            messages = Lists.newLinkedList();
        }
        return messages;
    }

    public void addMessage(String message) {
        getMessages().add(message);
    }

    public Collection<String> getErrorMessages() {
        if (errorMessages == null) {
            errorMessages = Lists.newLinkedList();
        }
        return errorMessages;
    }

    public void addErrorMessages(String errorMessage) {
        getErrorMessages().add(errorMessage);
    }

    public String getAuthenticatedUserId() {
        return authenticatedUserId;
    }

    public void setAuthenticatedUserId(String authenticatedUserId) {
        this.authenticatedUserId = authenticatedUserId;
    }

    public FaxToMailUser getAuthenticatedFaxToMailUser() {
        return authenticatedFaxToMailUser;
    }

    public void setAuthenticatedFaxToMailUser(FaxToMailUser authenticatedFaxToMailUser) {
        this.authenticatedFaxToMailUser = authenticatedFaxToMailUser;
    }

    public void logout() {
        setAuthenticatedUserId(null);
        setAuthenticatedFaxToMailUser(null);
    }

    public boolean isAdmin() {
        return admin;
    }
    
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
