package com.franciaflex.faxtomail.web.json;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Type;

/**
 * Gson topia entity adaptor that use a specific implementationClass to deserialize a topia entity interface.
 *
 * @author Eric Chatellier
 */
public class TopiaEntityAdapter implements JsonSerializer<TopiaEntity>, JsonDeserializer<TopiaEntity> {

    protected Class<? extends TopiaEntity> implementationClass;

    public TopiaEntityAdapter(Class<? extends TopiaEntity> implementationClass) {
        this.implementationClass = implementationClass;
    }

    @Override
    public TopiaEntity deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        TopiaEntity result = jsonDeserializationContext.deserialize(jsonElement, implementationClass);
        return result;
    }

    @Override
    public JsonElement serialize(TopiaEntity object, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonElement result = jsonSerializationContext.serialize(object, object.getClass());
        return result;
    }
}
