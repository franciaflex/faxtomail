package com.franciaflex.faxtomail.web.action.admin;

/*
 * #%L
 * FaxToMail :: Web
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailApplicationContext;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaPersistenceContext;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import com.franciaflex.faxtomail.services.service.LdapService;
import com.franciaflex.faxtomail.services.service.UserService;
import com.franciaflex.faxtomail.web.FaxToMailActionSupport;
import com.opensymphony.xwork2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Result;

@InterceptorRefs({
    @InterceptorRef("faxToMailInterceptor"),
    @InterceptorRef("loginInterceptor"),
    @InterceptorRef("paramsPrepareParamsStack"),
    @InterceptorRef(value="execAndWait",
        params={"excludeMethods","input"})
})
public class LdapAction extends FaxToMailActionSupport implements Preparable {

    protected UserService userService;

    protected long userCount;

    protected long groupCount;

    protected FaxToMailApplicationContext applicationContext;

    public void setApplicationContext(FaxToMailApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public long getUserCount() {
        return userCount;
    }

    public long getGroupCount() {
        return groupCount;
    }

    @Override
    public void prepare() throws Exception {
        // check authorization
        if (!getSession().isAdmin()) {
            throw new RuntimeException("Not authorized");
        }
    }

    @Override
    @Action("ldap-input")
    public String input() throws Exception {
        userCount = userService.getActiveUserCount();
        groupCount = userService.getActiveGroupCount();
        return INPUT;
    }

    @Override
    @Action(results = {
            @Result(name="wait", location="/WEB-INF/content/admin/ldap-wait.jsp"),
            @Result(type = "redirectAction", params = {"actionName", "ldap-input"})})
    public String execute() throws Exception {
        
        // cette methode doit ouvrir sa propre connexion sinon l'interceptor va la fermer
        // and cause du execAndWait
        FaxToMailTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext();
        try {
            persistenceContext = applicationContext.newPersistenceContext();
            FaxToMailServiceContext serviceContext = applicationContext.newServiceContext(persistenceContext);
            LdapService ldapService2 = serviceContext.getLdapService();
            ldapService2.updateLdapData();
        } finally {
            persistenceContext.close();
        }

        return SUCCESS;
    }
}
