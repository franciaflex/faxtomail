package com.franciaflex.faxtomail.web.action;

/*
 * #%L
 * FaxToMail :: Web
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.EmailGroup;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.RangeRow;
import com.franciaflex.faxtomail.persistence.entities.Reply;
import com.franciaflex.faxtomail.services.DecoratorService;
import com.franciaflex.faxtomail.services.service.ConfigurationService;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.web.FaxToMailActionSupport;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Result;
import org.nuiton.decorator.Decorator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 1.1
 */

@InterceptorRefs({
     @InterceptorRef("faxToMailInterceptor"),
     @InterceptorRef("paramsPrepareParamsStack")
})
public class SearchAction extends FaxToMailActionSupport {

    private static final Log log = LogFactory.getLog(SearchAction.class);

    protected EmailService emailService;
    protected ConfigurationService configurationService;
    protected DecoratorService decoratorService;

    protected String element;
    protected String company;
    protected List<Email> results;
    protected String singleResultId;
    protected Map<MailField, String> tableMailFields;

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public void setDecoratorService(DecoratorService decoratorService) {
        this.decoratorService = decoratorService;
    }

    @Override
    @Action("search-input")
    public String input() throws Exception {
        return INPUT;
    }

    @Override
    @Action(value = "search",
            results = {
                    @Result(name="detail",
                            type="redirectAction",
                            params = {"actionName", "demand-detail", "id", "%{singleResultId}"})
            })
    public String execute() throws Exception {
        String result = INPUT;
        if (element != null && company != null) {
            results = new ArrayList<>(emailService.searchArchives(element, company));
            if (results.size() == 1) {
                singleResultId = results.get(0).getTopiaId();
                result = "detail";

            } else {
                List<MailField> mailFields = configurationService.getSearchDisplayColumns();
                if (CollectionUtils.isEmpty(mailFields)) {
                    mailFields = Lists.newArrayList(MailField.getTableFields());
                }
                tableMailFields = getEnumAsMap(mailFields.toArray(new MailField[mailFields.size()]));
            }
        }
        return result;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public List<Email> getResults() {
        return results;
    }

    public void setResults(List<Email> results) {
        this.results = results;
    }

    public String getSingleResultId() {
        return singleResultId;
    }

    public Map<MailField, String> getTableMailFields() {
        return tableMailFields;
    }

    public String getAttr(MailField field, Email email) {
        Object attrValue = null;
        switch (field) {
            case PRIORITY:
                attrValue = email.getPriority();
                break;
            case DEMAND_TYPE:
                attrValue = email.getDemandType();
                break;
            case OBJECT:
                attrValue = email.getObject();
                break;
            case CLIENT_CODE:
                Client client = email.getClient();
                attrValue = client == null ? null : client.getCode();
                break;
            case CLIENT_NAME:
                client = email.getClient();
                attrValue = client == null ? null : client.getName();
                break;
            case CLIENT_BRAND:
                client = email.getClient();
                attrValue = client == null ? null : client.getBrand();
                break;
            case SENDER:
                attrValue = email.getSender();
                break;
            case PROJECT_REFERENCE:
                attrValue = email.getProjectReference();
                break;
            case COMPANY_REFERENCE:
                attrValue = email.getCompanyReference();
                break;
            case REFERENCE:
                attrValue = email.getReference();
                break;
            case RECEPTION_DATE:
                attrValue = email.getReceptionDate();
                break;
            case RECIPIENT:
                attrValue = email.getRecipient();
                break;
            case DEMAND_STATUS:
                attrValue = email.getDemandStatus();
                break;
            case WAITING_STATE:
                attrValue = email.getWaitingState();
                break;
            case PF_NB:
                int intAttrValue = 0;
                for (RangeRow rr : email.getRangeRow()) {
                    Integer productQuantity = rr.getProductQuantity();
                    if (productQuantity != null) {
                        intAttrValue += productQuantity;
                    }
                }
                attrValue = intAttrValue;
                break;
            case SAV_NB:
                intAttrValue = 0;
                for (RangeRow rr : email.getRangeRow()) {
                    Integer savQuantity = rr.getSavQuantity();
                    if (savQuantity != null) {
                        intAttrValue += savQuantity;
                    }
                }
                attrValue = intAttrValue;
                break;
            case QUOTATION_NB:
                intAttrValue = 0;
                for (RangeRow rr : email.getRangeRow()) {
                    Integer quotationQuantity = rr.getQuotationQuantity();
                    if (quotationQuantity != null) {
                        intAttrValue += quotationQuantity;
                    }
                }
                attrValue = intAttrValue;
                break;
            case TAKEN_BY:
                attrValue = email.getTakenBy();
                break;
            case COMMENT:
                attrValue = email.getComment();
                break;
            case EDI_RETURN:
                attrValue = email.getEdiError();
                break;
            case LAST_ATTACHMENT_OPENING_IN_THIS_FOLDER_USER:
                attrValue = email.getLastAttachmentOpener();
                break;
            case REPLIES:
                attrValue = "<span class='fa fa-envelope-o'></span>&nbsp;" + email.sizeReplies();
                break;
            case ATTACHMENT:
                attrValue = "<span class='fa fa-paperclip'></span>&nbsp;" + email.sizeAttachment();
                break;
            case GROUP:
                EmailGroup emailGroup = email.getEmailGroup();
                attrValue = "<span class='fa fa-link'></span>&nbsp;" + (emailGroup == null ? 1 : emailGroup.sizeEmail());
                break;
            case LAST_PRINTING_USER:
                attrValue = email.getLastPrintingUser();
                break;
            case LAST_PRINTING_DATE:
                attrValue = email.getLastPrintingDate();
                break;
        }

        String result;
        if (attrValue == null || StringUtils.isBlank(attrValue.toString())) {
            result = "&nbsp;";

        } else {
            Decorator decorator = decoratorService.getDecorator(attrValue);
            if (decorator != null) {
                result = decorator.toString(attrValue);
            } else {
                result = attrValue.toString();
            }
        }
        return result;
    }

    public String getTooltip(MailField field, Email email) {
        String tooltipText = null;
        switch (field) {
            case PF_NB:
                List<String> ranges = new ArrayList<String>();
                Collection<RangeRow> rangeRows = email.getRangeRow();
                if (rangeRows != null) {
                    for (RangeRow rangeRow : rangeRows) {
                        Integer qty = rangeRow.getProductQuantity();
                        if (qty != null && qty > 0) {
                            ranges.add(rangeRow.getRange().getLabel() + " : " + qty);
                        }
                    }
                }

                if (!ranges.isEmpty()) {
                    tooltipText = StringUtils.join(ranges, "\n");
                }
                break;

            case SAV_NB:
                ranges = new ArrayList<String>();
                rangeRows = email.getRangeRow();
                if (rangeRows != null) {
                    for (RangeRow rangeRow : rangeRows) {
                        Integer qty = rangeRow.getSavQuantity();
                        if (qty != null && qty > 0) {
                            ranges.add(rangeRow.getRange().getLabel() + " : " + qty);
                        }
                    }
                }

                if (!ranges.isEmpty()) {
                    tooltipText = StringUtils.join(ranges, "\n");
                }
                break;

            case QUOTATION_NB:
                ranges = new ArrayList<String>();
                rangeRows = email.getRangeRow();
                if (rangeRows != null) {
                    for (RangeRow rangeRow : rangeRows) {
                        Integer qty = rangeRow.getQuotationQuantity();
                        if (qty != null && qty > 0) {
                            ranges.add(rangeRow.getRange().getLabel() + " : " + qty);
                        }
                    }
                }

                if (!ranges.isEmpty()) {
                    tooltipText = StringUtils.join(ranges, "\n");
                }
                break;

            case REPLIES:
                List<Reply> replies = email.getReplies();
                if (CollectionUtils.isNotEmpty(replies)) {
                    final Decorator<Reply> decorator = decoratorService.getDecoratorByType(Reply.class);

                    List<String> replyNames = Lists.transform(replies, new Function<Reply, String>() {
                        @Override
                        public String apply(Reply attachment) {
                            return decorator.toString(attachment);
                        }
                    });
                    tooltipText = StringUtils.join(replyNames, "\n");
                }
                break;

            case ATTACHMENT:
                Collection<Attachment> attachments = email.getAttachment();
                if (CollectionUtils.isNotEmpty(attachments)) {
                    final Decorator<Attachment> decorator = decoratorService.getDecoratorByType(Attachment.class);

                    Collection<String> attachmentNames = Collections2.transform(attachments, new Function<Attachment, String>() {
                        @Override
                        public String apply(Attachment attachment) {
                            String name = decorator.toString(attachment);
                            if (attachment.getEditedFileName() != null) {
                                name += " (+ édité)";
                            }
                            return name;
                        }
                    });
                    tooltipText = StringUtils.join(attachmentNames, "\n");
                }
                break;

            case GROUP:

                EmailGroup emailGroup = email.getEmailGroup();
                Collection<Email> emails = new ArrayList<>();
                if (emailGroup != null) {
                    Collection<Email> groupEmails = emailGroup.getEmail();
                    if (CollectionUtils.isNotEmpty(groupEmails)) {
                        emails.addAll(groupEmails);
                    }
                }
                if (emails.isEmpty()) {
                    emails.add(email);
                }
                Collection<String> emailTitles = Collections2.transform(emails, new Function<Email, String>() {
                    @Override
                    public String apply(Email email) {
                        String result = email.getObject();
                        String ref = email.getReference();
                        if (!ref.isEmpty()) {
                            result = ref + " - " + result;
                        }
                        return result;
                    }
                });
                tooltipText = StringUtils.join(emailTitles, "\n");
                break;

            default:
                tooltipText = getAttr(field, email);
        }

        if (tooltipText == null) {
            tooltipText = "&nbsp;";
        }

        return tooltipText;
    }
}
