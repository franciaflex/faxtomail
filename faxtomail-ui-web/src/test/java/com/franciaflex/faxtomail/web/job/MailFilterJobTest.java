package com.franciaflex.faxtomail.web.job;

/*-
 * #%L
 * FaxToMail :: Web
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.EmailAccount;
import com.franciaflex.faxtomail.persistence.entities.EmailAccountImpl;
import com.franciaflex.faxtomail.persistence.entities.EmailProtocol;
import com.franciaflex.faxtomail.services.service.ConfigurationService;
import com.franciaflex.faxtomail.services.service.MailFolderService;
import com.franciaflex.faxtomail.services.service.UserService;
//import com.franciaflex.faxtomail.services.service.AbstractFaxToMailServiceTest;
import com.icegreen.greenmail.junit.GreenMailRule;
import com.icegreen.greenmail.user.GreenMailUser;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetupTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.mail.MessagingException;
import java.io.IOException;

/**
 * @author Jean Couteau (Code Lutin)
 * @since 2.4.27
 */
public class MailFilterJobTest {

    @Rule
    public final GreenMailRule greenMail = new GreenMailRule(ServerSetupTest.SMTP_IMAP);

    @Before
    public void setUp() throws IOException {
//        service = newService(MailFolderService.class);
//        newService(InitTestData.class).initTestData();
//        userService = newService(UserService.class);
    }

    @Test
    public void testMailReceptionNoFilter() throws MessagingException {
        GreenMailUser user = greenMail.setUser("to@localhost.com", "test", "test");

        //user.deliver(createMimeMessage()); // You can either create a more complex message...
        GreenMailUtil.sendTextEmailTest("to@localhost.com", "from@localhost.com",
                "subject", "body"); // ...or use the default messages

        //Prepare test Data
        EmailAccount emailAccount = getTestAccount();
        //confService.getMailFilters();
        //confService save mailAccount();


        //Create a conf to put to@localhost.com -> Cyril



        MailFilterJob job = new MailFilterJob();
        job.checkEmails(emailAccount);
        //job.execute();

        //Check that mail is still on server
        Assert.assertEquals(1, greenMail.getReceivedMessages().length);
        //Check that database contains no email
        // ??????
    }

    protected EmailAccount getTestAccount(){
        EmailAccount emailAccount = new EmailAccountImpl();
        emailAccount.setLogin("test");
        emailAccount.setPassword("test");
        emailAccount.setHost("127.0.0.1");
        emailAccount.setPort(greenMail.getImap().getPort());
        emailAccount.setProtocol(EmailProtocol.IMAP);
        return emailAccount;
    }

}
