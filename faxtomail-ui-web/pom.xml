<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  FaxToMail :: Web
  %%
  Copyright (C) 2014 Mac-Groupe, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>com.franciaflex</groupId>
    <artifactId>faxtomail</artifactId>
    <version>2.5.17-SNAPSHOT</version>
  </parent>

  <groupId>com.franciaflex.faxtomail</groupId>
  <artifactId>faxtomail-ui-web</artifactId>
  <packaging>war</packaging>

  <name>FaxToMail :: Web</name>
  <description>faxToMail Web Module</description>

  <properties>
    <redmine.releaseFiles>
      target/${project.build.finalName}.war
    </redmine.releaseFiles>

    <deployFiles>target/${project.build.finalName}.war</deployFiles>

    <defaultWebContextPath>faxtomail</defaultWebContextPath>
    <defaultSiteUrl>http://localhost:8080/${defaultWebContextPath}</defaultSiteUrl>

    <devMode>true</devMode>
    <jsMinimize>false</jsMinimize>
    <jsCachePeriod>1</jsCachePeriod>
    <jsPreProcessors>preProcessors=forceCssDataUri,cssUrlRewriting,cssImport,semicolonAppender</jsPreProcessors>
    <jsPostProcessors>postProcessors=cssVariables</jsPostProcessors>

  </properties>

  <dependencies>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>faxtomail-persistence</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>faxtomail-service</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>org.nuiton.topia</groupId>
      <artifactId>topia-persistence</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton.topia</groupId>
      <artifactId>topia-service-flyway</artifactId>
    </dependency>
    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-utils</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-validator</artifactId>
      <scope>runtime</scope>
    </dependency>

    <dependency>
      <groupId>commons-logging</groupId>
      <artifactId>commons-logging</artifactId>
    </dependency>

    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton.i18n</groupId>
      <artifactId>nuiton-i18n</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-decorator</artifactId>
    </dependency>

    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.struts</groupId>
      <artifactId>struts2-core</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-collections4</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>
    
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-email</artifactId>
      <exclusions>
        <exclusion>
          <groupId>com.sun.mail</groupId>
          <artifactId>javax.mail</artifactId>
        </exclusion>
      </exclusions>
    </dependency>

    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
    </dependency>

    <dependency>
      <groupId>com.jgeppert.struts2.jquery</groupId>
      <artifactId>struts2-jquery-plugin</artifactId>
    </dependency>

    <dependency>
      <groupId>com.jgeppert.struts2.bootstrap</groupId>
      <artifactId>struts2-bootstrap-plugin</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.struts</groupId>
      <artifactId>struts2-convention-plugin</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.struts</groupId>
      <artifactId>struts2-sitemesh-plugin</artifactId>
    </dependency>

    <dependency>
      <groupId>com.google.code.gson</groupId>
      <artifactId>gson</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-jcl</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-slf4j-impl</artifactId>
    </dependency>

    <dependency>
      <groupId>com.h2database</groupId>
      <artifactId>h2</artifactId>
      <scope>runtime</scope>
    </dependency>
    
    <dependency>
      <groupId>org.hibernate</groupId>
      <artifactId>hibernate-core</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton.web</groupId>
      <artifactId>nuiton-struts2</artifactId>
    </dependency>

    <dependency>
      <groupId>org.quartz-scheduler</groupId>
      <artifactId>quartz</artifactId>
    </dependency>

    <dependency>
      <groupId>com.sun.mail</groupId>
      <artifactId>jakarta.mail</artifactId>
    </dependency>

    <dependency>
      <groupId>com.sun.activation</groupId>
      <artifactId>jakarta.activation</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton.js</groupId>
      <artifactId>nuiton-js-wro</artifactId>
    </dependency>

    <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>jquery</artifactId>
    </dependency>
    
    <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>jquery-ui</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton.js</groupId>
      <artifactId>nuiton-js-bootstrap</artifactId>
    </dependency>

    <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>select2</artifactId>
      <exclusions>
        <exclusion>
          <groupId>org.webjars</groupId>
          <artifactId>jquery</artifactId>
        </exclusion>
      </exclusions>
    </dependency>

    <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>ui-select</artifactId>
    </dependency>

    <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>angular-sanitize</artifactId>
    </dependency>

    <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>angularjs</artifactId>
    </dependency>

    <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>angular-ui-tree</artifactId>
    </dependency>
    
    <dependency>
      <groupId>org.nuiton.js</groupId>
      <artifactId>nuiton-js-font-awesome</artifactId>
    </dependency>
    
    <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>angular-ui-bootstrap</artifactId>
      <exclusions>
        <exclusion>
          <groupId>org.webjars</groupId>
          <artifactId>bootstrap</artifactId>
        </exclusion>
      </exclusions>
    </dependency>

    <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>angular-ui-sortable</artifactId>
      <exclusions>
        <exclusion>
          <groupId>org.webjars</groupId>
          <artifactId>jquery-ui</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    
    <dependency>
      <groupId>org.nuiton.js</groupId>
      <artifactId>nuiton-js-html5shiv</artifactId>
    </dependency>
    
    <dependency>
      <groupId>org.webjars</groupId>
      <artifactId>respond</artifactId>
    </dependency>

    <dependency>
      <groupId>org.webjars.bower</groupId>
      <artifactId>trix</artifactId>
    </dependency>

    <dependency>
      <groupId>org.webjars.bower</groupId>
      <artifactId>angular-trix</artifactId>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
    </dependency>

    <dependency>
      <groupId>org.jsoup</groupId>
      <artifactId>jsoup</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.pdfbox</groupId>
      <artifactId>pdfbox</artifactId>
    </dependency>

    <dependency>
      <groupId>com.icegreen</groupId>
      <artifactId>greenmail</artifactId>
      <version>1.5.7</version>
      <scope>test</scope>
    </dependency>

  </dependencies>

  <build>

    <finalName>faxtomail-${project.version}</finalName>
    
    <resources>
      <resource>
        <directory>src/main/resources</directory>
        <includes>
          <include>*.properties</include>
        </includes>
        <filtering>true</filtering>
      </resource>
      <resource>
        <directory>src/main/resources</directory>
        <excludes>
          <exclude>*.properties</exclude>
        </excludes>
        <filtering>false</filtering>
      </resource>
    </resources>

    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.tomcat.maven</groupId>
          <artifactId>tomcat7-maven-plugin</artifactId>
          <configuration>
            <path>/${defaultWebContextPath}</path>
            <systemProperties>
              <siteUrl>${defaultSiteUrl}</siteUrl>
            </systemProperties>
            <uriEncoding>UTF-8</uriEncoding>
          </configuration>
        </plugin>

      </plugins>
    </pluginManagement>

    <plugins>
      <plugin>
        <groupId>org.nuiton.i18n</groupId>
        <artifactId>i18n-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>parseJsp</id>
            <goals>
              <goal>parserStruts2</goal>
            </goals>
            <configuration>
              <acceptKeyFormat>^faxtomail\..*$</acceptKeyFormat>
            </configuration>
          </execution>
          <execution>
            <id>parseOthersAndGen</id>
            <configuration>
              <entries>
                <entry>
                  <specificGoal>parserValidation</specificGoal>
                  <basedir>${project.basedir}/src/main/java/</basedir>
                  <includes>
                    <param>**/**-validation.xml</param>
                  </includes>
                </entry>
              </entries>
            </configuration>
            <goals>
              <goal>parserJava</goal>
              <goal>gen</goal>
              <goal>bundle</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>

  <profiles>

    <profile>
      <id>disable-devMode-release-profile</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>
      <properties>
        <devMode>false</devMode>
        <jsMinimize>true</jsMinimize>
        <jsCachePeriod>0</jsCachePeriod>
        <jsPreProcessors> </jsPreProcessors> <!-- Vide pour ne pas inclure la propriété en release -->
        <jsPostProcessors> </jsPostProcessors> <!-- Vide pour ne pas inclure la propriété en release -->
      </properties>
    </profile>

  </profiles>
</project>
