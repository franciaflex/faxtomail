
-- add mail folder level configuration
alter table mailfolder add useCurrentLevelCompany boolean;
alter table mailfolder add company text;
update mailfolder set useCurrentLevelCompany = 'f';