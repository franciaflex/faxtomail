
-- remove topia from newclient
drop table newclient;
create table newClient (
    name text,
    emailAddress text,
    faxNumber text,
    caracteristic1 text,
    caracteristic2 text,
    caracteristic3 text,
    code text not null,
    company text not null,
    brand text,
    personInCharge text,
    unique (code, company, emailAddress, faxNumber)
);

-- remove topia from edireturn
drop table edireturn;
create table ediReturn (
    rangeRowTopiaId text not null,
    commandNumber text,
    error text,
    unique (rangeRowTopiaId)
);

-- clear client table
update email set client = null;
delete from client;

-- add company in client
--drop constraint UK_b4ck8pelycojqmbrd8n36mdfw;

alter table client drop emailAddress;
alter table client drop faxNumber;
alter table client drop id;

alter table client add company text not NULL;
alter table client add emailAddressesJson text;
alter table client add faxNumbersJson text;
alter table client alter brand drop not NULL;

alter table client add constraint UK_kqpy7y2f4onck5julevr0hfg1 unique (code, company);
