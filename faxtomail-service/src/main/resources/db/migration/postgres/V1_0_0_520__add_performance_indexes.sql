CREATE INDEX email_receptionDate_idx ON email(receptionDate ASC);
CREATE INDEX email_archiveDate_idx ON email(archiveDate NULLS FIRST);
CREATE INDEX email_mailFolder_idx ON email(mailFolder);
CREATE INDEX email_demandStatus_idx ON email(demandStatus);

CREATE INDEX email_takenBy_idx ON email(takenBy);
CREATE INDEX email_client_idx ON email(client);
CREATE INDEX email_etatAttente_idx ON email(etatAttente);
CREATE INDEX email_priority_idx ON email(priority);
CREATE INDEX email_demandType_idx ON email(demandType);

CREATE INDEX demandType_fields_OWNER_idx ON demandType_fields(OWNER);
CREATE INDEX rangeRow_range_idx ON rangeRow(range);
CREATE INDEX history_faxToMailUser_idx ON history(faxToMailUser);
CREATE INDEX history_fields_OWNER_idx ON history_fields(OWNER);

CREATE INDEX mailfolder_readrightusers_readRightUsers_idx ON mailfolder_readrightusers(readRightUsers);
CREATE INDEX mailfolder_readrightgroups_readRightGroups_idx ON mailfolder_readrightgroups(readRightGroups);
