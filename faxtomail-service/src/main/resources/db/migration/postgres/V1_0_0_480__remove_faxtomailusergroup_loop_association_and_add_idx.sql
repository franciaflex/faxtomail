
-- remove unused association
ALTER TABLE faxtomailusergroup DROP COLUMN faxtomailusergroup;

-- add column faxAccountType on emailAccount
ALTER TABLE emailAccount add faxAccountType boolean;
update emailAccount set faxAccountType = 'f';

-- add email_idx on attachment
ALTER TABLE attachment ADD email_idx int4;

UPDATE attachment SET email_idx = (
    SELECT COUNT(topiaid) FROM attachment sub
    WHERE sub.email = attachment.email AND (sub.topiacreatedate || sub.topiaid) < (attachment.topiacreatedate || attachment.topiaid)
);

-- add email_idx on history
ALTER TABLE history ADD COLUMN email_idx int4;

UPDATE history SET email_idx = (
    SELECT COUNT(topiaid) FROM history sub
    WHERE sub.email = history.email AND (sub.topiacreatedate || sub.topiaid) < (history.topiacreatedate || history.topiaid)
);

-- add email_idx on rangeRow
ALTER TABLE rangeRow ADD email_idx int4;

UPDATE rangeRow SET email_idx = (
    SELECT COUNT(topiaid) FROM rangeRow sub
    WHERE sub.email = rangeRow.email AND (sub.topiacreatedate || sub.topiaid) < (rangeRow.topiacreatedate || rangeRow.topiaid)
);

-- add email_idx on reply
ALTER TABLE reply ADD email_idx int4;

UPDATE reply SET email_idx = (
    SELECT COUNT(topiaid) FROM reply sub
    WHERE sub.email = reply.email AND (sub.topiacreatedate || sub.topiaid) < (reply.topiacreatedate || reply.topiaid)
);
