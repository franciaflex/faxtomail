
-- add user and group hidden configuration
alter table FaxToMailUserGroup add hidden boolean;
alter table FaxToMailUser add hidden boolean;
update FaxToMailUserGroup set hidden = 'f';
update FaxToMailUser set hidden = 'f';
