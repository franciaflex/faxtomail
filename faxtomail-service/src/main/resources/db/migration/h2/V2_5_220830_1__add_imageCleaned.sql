-- add imageCleaned

alter table email add column if not exists imageCleaned boolean default 'f';
update email set imageCleaned = 'f';
