-- add generatedPDFPages

drop table if exists GENERATEDPDFPAGE;

create table GENERATEDPDFPAGE (
    page blob NOT NULL,
    topiaId VARCHAR(255) NOT NULL,
    topiaVersion BIGINT  NOT NULL,
    topiaCreateDate  timestamp,
    attachment VARCHAR(255),
    PRIMARY KEY (topiaId)
);

ALTER TABLE GENERATEDPDFPAGE
    ADD CONSTRAINT FK_9CRCVUPPG70VCFW7334VAH7PJ
    FOREIGN KEY(ATTACHMENT)
    REFERENCES ATTACHMENT;