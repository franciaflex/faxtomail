-- add displayOnlyUserTrigraphInTables

alter table mailfolder add displayOnlyUserTrigraphInTables boolean;
update mailfolder set displayOnlyUserTrigraphInTables = 'f' where parent is null;