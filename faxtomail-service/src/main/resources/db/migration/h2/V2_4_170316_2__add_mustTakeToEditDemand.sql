-- add mustTakeToEditDemand

alter table mailfolder add mustTakeToEditDemand boolean;
update mailfolder set mustTakeToEditDemand = 't' where parent is null;