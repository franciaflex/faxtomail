
-- add mail folder level configuration
alter table mailfolder add useCurrentLevelCompany boolean;
alter table mailfolder add company longvarchar;
update mailfolder set useCurrentLevelCompany = 'f';