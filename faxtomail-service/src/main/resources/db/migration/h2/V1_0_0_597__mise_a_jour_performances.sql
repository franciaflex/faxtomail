-- remove contraints
alter table email drop constraint FK_4q69gdpfr96aqig9ephikqxxr;
alter table etatAttente drop constraint UK_e1mte18x0rh5akvjfbi5ljse9;
alter table etatattentes_mailfolder drop constraint FK_3gi1yi1o932oe5t8ikcnsd3ef;
alter table etatattentes_mailfolder drop constraint FK_hsynjnyke7heuntkxkks9voxy;
alter table history_fields drop constraint FK_qoup2jo9smmju8hgou9sq5108;
DROP INDEX idx_MailFolder_etatAttentes;
DROP INDEX email_etatAttente_idx;

-- renommage etat attente
ALTER TABLE email ALTER COLUMN etatAttente RENAME TO waitingState;
ALTER TABLE etatAttente RENAME TO waitingState;
ALTER TABLE etatattentes_mailfolder RENAME TO mailfolder_waitingstates;
ALTER TABLE mailfolder_waitingstates ALTER COLUMN etatAttentes RENAME TO waitingStates;
ALTER TABLE mailFolder ALTER COLUMN useCurrentLevelEtatAttente RENAME TO useCurrentLevelWaitingState;

-- migration des topia id
update waitingState set topiaId = replace(topiaId, 'EtatAttente', 'WaitingState');
update email set waitingState = replace(waitingState, 'EtatAttente', 'WaitingState');
update mailfolder_waitingstates set waitingStates = replace(waitingStates, 'EtatAttente', 'WaitingState') where waitingStates is not null;

-- migration des champs obligatoire (enum)
update configuration set searchDisplayColumns = replace(searchDisplayColumns, 'ETAT_ATTENTE', 'WAITING_STATE') where searchDisplayColumns is not null;
update mailfolder set folderTableColumns = replace(folderTableColumns, 'ETAT_ATTENTE', 'WAITING_STATE') where folderTableColumns is not null;
update DemandType set requiredFields = replace(requiredFields, 'ETAT_ATTENTE', 'WAITING_STATE') where requiredFields is not null;

-- restore constraints
alter table waitingState add constraint UK_ccllv66h9tle79mgklh1kpicn unique (label);
alter table email 
        add constraint FK_nfplqi5mxyfuxgkpe3rb83put 
        foreign key (waitingState) 
        references waitingState;
alter table mailfolder_waitingstates 
        add constraint FK_tb0qx61jwbtopp4xjv089syt2 
        foreign key (waitingStates) 
        references waitingState;
alter table mailfolder_waitingstates 
        add constraint FK_3ucrlum12s7ivskvvdgykxq7d 
        foreign key (mailFolder) 
        references mailFolder;
CREATE INDEX idx_MailFolder_waitingStates ON mailfolder_waitingstates(mailFolder);
CREATE INDEX email_waitingState_idx ON email(waitingState);

-- Reply
delete from reply;
ALTER TABLE reply ALTER sentDate SET NOT NULL;
ALTER TABLE reply ALTER subject SET NOT NULL;
ALTER TABLE reply ADD sender longvarchar not null;
ALTER TABLE reply ADD recipient longvarchar not null;
ALTER TABLE reply ADD replyContent varchar(255) not null;
ALTER TABLE reply ADD sentBy varchar(255) not null;

create table replyContent (
        topiaId varchar(255) not null,
        topiaVersion bigint not null,
        topiaCreateDate timestamp,
        source binary(9999999) not null,
        primary key (topiaId)
);

alter table reply add constraint UK_7xjte6c6h2lltfpu6ot1bel8k  unique (replyContent);
alter table reply 
        add constraint FK_7xjte6c6h2lltfpu6ot1bel8k 
        foreign key (replyContent) 
        references replyContent;

alter table reply 
        add constraint FK_9m87oooqhsbf1wbad9h4jcwm3 
        foreign key (sentBy) 
        references faxToMailUser;

-- Last opener
ALTER TABLE email ADD lastAttachmentOpener varchar(255);
alter table email 
        add constraint FK_pu5j5vh83354jhoikyvb22jru 
        foreign key (lastAttachmentOpener) 
        references faxToMailUser;

-- Champs hitory fields en json
DROP TABLE history_fields;
ALTER TABLE history ADD fieldsJson longvarchar;
