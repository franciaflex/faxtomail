-- add last printing user

alter table email add lastPrintingUser varchar(255);

alter table email
    add constraint FK_I8WXUJ77ETK21ME0JC939HBEO_INDEX_3
    foreign key (lastPrintingUser)
    references faxtomailuser;

update email set email.lastPrintingUser = (select h.faxtomailuser
  from history h
  where h.email = email.topiaId
  and h.type = 'PRINTING'
  and h.email not in (
    select h2.email from history h2
    where (h2.type='TRANSMISSION' OR h2.type='PRINTING' )and h2.modificationDate > h.modificationDate
  )
) where archiveDate is null;