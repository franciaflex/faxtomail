-- add printActionEqualTakeOnlyIfNotTaken

alter table mailfolder add printActionEqualTakeOnlyIfNotTaken boolean;
update mailfolder set printActionEqualTakeOnlyIfNotTaken = 'f' where parent is null;