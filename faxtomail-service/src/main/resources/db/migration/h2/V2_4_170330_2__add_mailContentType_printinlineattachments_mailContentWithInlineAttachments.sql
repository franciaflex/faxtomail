-- add mailContentType

alter table attachment add mailContent boolean;
update attachment set mailContent = 'f';
update attachment set mailContent = 't' where originalFileName like 'contenu % du mail%.pdf';

-- add printinlineattachments and mailContentWithInlineAttachments

alter table mailfolder add printinlineattachments boolean;
alter table mailfolder add mailContentWithInlineAttachments boolean;
update mailfolder set printinlineattachments = 't', mailContentWithInlineAttachments = 'f' where parent is null;