-- add showReplyAction and showForwardAction

alter table mailfolder add showReplyAction boolean;
alter table mailfolder add showForwardAction boolean;
update mailfolder set showReplyAction = 't', showForwardAction = 'f' where parent is null;