alter table attachment add inlineAttachment boolean;
alter table attachment add lessImportant boolean;

update attachment set inlineAttachment = 'f';
update attachment set lessImportant = 'f';