-- add canChangeOrderInTable

alter table mailfolder add canChangeOrderInTable boolean;
update mailfolder set canChangeOrderInTable = 'f' where parent is null;