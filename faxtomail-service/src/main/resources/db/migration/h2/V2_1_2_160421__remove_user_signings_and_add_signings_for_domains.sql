alter table faxtomailuser drop constraint FK_CDWL91T1FPNO1PW07532VPN21_INDEX_7;
alter table faxtomailuser drop defaultsigning;

delete from stamp where signing = true;

alter table stamp drop constraint FK_KPY7QI4OX3LIY20GRSP7D449M_INDEX_4;
alter table stamp drop signing;
alter table stamp drop faxtomailuser;

create table signingForDomain (
    topiaId varchar(255) not null,
    topiaVersion bigint not null,
    topiaCreateDate timestamp,
    domainName varchar(255) unique,
    image longvarchar,
    text longvarchar,
    primary key (topiaId)
);