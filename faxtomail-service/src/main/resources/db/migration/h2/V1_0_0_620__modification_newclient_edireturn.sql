
-- remove topia from newclient
drop table newclient;
create table newClient (
    name longvarchar,
    emailAddress longvarchar,
    faxNumber longvarchar,
    caracteristic1 longvarchar,
    caracteristic2 longvarchar,
    caracteristic3 longvarchar,
    code longvarchar not null,
    company longvarchar not null,
    brand longvarchar,
    personInCharge longvarchar,
    unique (code, company, emailAddress, faxNumber)
);

-- remove topia from edireturn
drop table edireturn;
create table ediReturn (
    rangeRowTopiaId longvarchar not null,
    commandNumber longvarchar,
    error longvarchar,
    unique (rangeRowTopiaId)
);

-- clear client table
update email set client = null;
delete from client;

-- add company in client
--drop constraint UK_b4ck8pelycojqmbrd8n36mdfw;

alter table client drop emailAddress;
alter table client drop faxNumber;
alter table client drop id;

alter table client add company longvarchar not NULL;
alter table client add emailAddressesJson longvarchar;
alter table client add faxNumbersJson longvarchar;
alter table client alter brand SET NULL;

alter table client add constraint UK_kqpy7y2f4onck5julevr0hfg1 unique (code, company);
