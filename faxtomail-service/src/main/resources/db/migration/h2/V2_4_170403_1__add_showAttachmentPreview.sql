-- add showAttachmentPreview

alter table mailfolder add showAttachmentPreview boolean;
update mailfolder set showAttachmentPreview = 'f' where parent is null;