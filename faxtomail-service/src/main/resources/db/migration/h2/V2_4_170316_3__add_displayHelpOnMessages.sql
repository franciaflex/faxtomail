-- add displayHelpOnMessages

alter table mailfolder add displayHelpOnMessages boolean;
update mailfolder set displayHelpOnMessages = 't' where parent is null;