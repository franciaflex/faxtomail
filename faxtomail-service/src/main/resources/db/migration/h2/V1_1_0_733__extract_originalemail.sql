
-- extract original email from email
create table originalEmail (
    topiaId varchar(255) not null,
    topiaVersion bigint not null,
    topiaCreateDate timestamp,
    content longvarchar not null,
    email varchar(255) not null,
    primary key (topiaId)
);

insert into originalEmail (
    select concat('com.franciaflex.faxtomail.persistence.entities.OriginalEmail_', RANDOM_UUID()), 0, topiaCreateDate, originalEmail, topiaId from email
);

merge into email(topiaId, originalEmail) key (topiaId) select email, topiaId from originalEmail;

alter table email alter column originalEmail varchar(255);

alter table email
        add constraint UK_P4YIV4FRJSIKCMLMFOX8CF6RE
        unique (originalEmail);

alter table originalEmail drop column email;
