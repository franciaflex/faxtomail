-- add canViewOriginalAttachments

alter table mailfolder add canViewOriginalAttachments boolean;
update mailfolder set canViewOriginalAttachments = 't' where parent is null;