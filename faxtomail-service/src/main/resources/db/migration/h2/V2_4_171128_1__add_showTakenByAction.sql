-- add showTakenByAction

alter table mailfolder add showTakenByAction boolean;
update mailfolder set showTakenByAction = 'f' where parent is null;