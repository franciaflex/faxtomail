
-- add mail folder level configuration
alter table mailfolder add useCurrentLevelNbElementToDisplay boolean;
alter table mailfolder add nbElementToDisplay integer;
update mailfolder set useCurrentLevelNbElementToDisplay = 'f';