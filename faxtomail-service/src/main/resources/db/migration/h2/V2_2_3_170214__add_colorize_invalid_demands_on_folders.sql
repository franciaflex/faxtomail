-- add mail folder COLORIZEINVALIDDEMANDS
alter table mailfolder add COLORIZEINVALIDDEMANDS boolean;
update mailfolder set COLORIZEINVALIDDEMANDS = 't' where parent is null;

-- add mail folder lockedDemandsOpenableInReadOnly
alter table mailfolder add lockedDemandsOpenableInReadOnly boolean;
update mailfolder set lockedDemandsOpenableInReadOnly = 'f' where parent is null;