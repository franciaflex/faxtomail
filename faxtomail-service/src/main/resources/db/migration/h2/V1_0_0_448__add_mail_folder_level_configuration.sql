
-- add mail folder level configuration
alter table mailfolder add useCurrentLevelDemandType boolean;
alter table mailfolder add useCurrentLevelRange boolean;
update mailfolder set useCurrentLevelDemandType = 'f';
update mailfolder set useCurrentLevelRange = 'f';
