-- add brands for domain

CREATE TABLE brandsForDomain (
  topiaId VARCHAR(255) NOT NULL,
  topiaVersion BIGINT  NOT NULL,
  topiaCreateDate  TIMESTAMP,
  domainName longvarchar NOT NULL,
  brandsJson longvarchar,
  PRIMARY KEY (topiaId)
);

-- add matching clients in email

create table email_matchingClients (
    email varchar(255) not null,
    matchingClients varchar(255) not null
);

alter table email_matchingClients
    add constraint FK_IICRYCV6TYE2EFQU4JTUM9CS1
    foreign key (matchingClients)
    references client;

alter table email_matchingClients
    add constraint FK_CDHGKIXMOOF90L83HNWB83A35
    foreign key (email)
    references email;

CREATE INDEX idx_Email_matchingClients ON email_matchingClients(email);