alter table mailfolder add useCurrentLevelInvalidFormDisabledActions boolean;
alter table mailfolder add invalidFormDisabledActions varchar(MAX);

update mailfolder set useCurrentLevelInvalidFormDisabledActions = 'f';
update mailfolder
set useCurrentLevelInvalidFormDisabledActions = 't',
    invalidFormDisabledActions = (select invalidFormDisabledActions from configuration)
where parent is null;

alter table configuration drop invalidFormDisabledActions;