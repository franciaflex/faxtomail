-- add mail folder acknowledgement
alter table mailfolder add SENDACKNOWLEDGEMENTTOSENDER boolean;
alter table mailfolder add ACKNOWLEDGEMENTMAILADDRESS varchar(255);
alter table mailfolder add ACKNOWLEDGEMENTMESSAGE varchar(255);
alter table mailfolder add ACKNOWLEDGEMENTSIGNING varchar(MAX);
alter table mailfolder add rejectResponseSigning varchar(MAX);
alter table mailfolder drop REJECTUNKNOWNSENDER;
update mailfolder set SENDACKNOWLEDGEMENTTOSENDER = 'f';

-- add matching clients in email

create table ACKNOWLEDGEMENTEXCEPTION_MAILFOLDER (
    MAILFOLDER varchar(255) not null,
    ACKNOWLEDGEMENTEXCEPTION varchar(255) not null
);

alter table ACKNOWLEDGEMENTEXCEPTION_MAILFOLDER
    add constraint FK_NNT5VNK8P09DIU5YP1V9FD1LS
    foreign key (ACKNOWLEDGEMENTEXCEPTION)
    references client;

alter table ACKNOWLEDGEMENTEXCEPTION_MAILFOLDER
    add constraint FK_MSSQC9QI74V9XH1CJIIDJWYU8
    foreign key (MAILFOLDER)
    references mailfolder;

CREATE INDEX idx_ACKNOWLEDGEMENTEXCEPTION_MAILFOLDER ON ACKNOWLEDGEMENTEXCEPTION_MAILFOLDER(MAILFOLDER);