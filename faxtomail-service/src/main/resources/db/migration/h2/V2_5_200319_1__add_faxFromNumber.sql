-- add faxFromNumber

alter table mailfolder add column if not exists faxFromNumber varchar(255);

alter table mailfolder add column if not exists useCurrentLevelFaxFromNumber boolean;
update mailfolder set useCurrentLevelFaxFromNumber = 'f';
