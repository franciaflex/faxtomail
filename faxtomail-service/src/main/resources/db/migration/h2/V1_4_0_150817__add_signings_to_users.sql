-- add signings

alter table stamp add signing boolean;
alter table stamp add faxtomailuser varchar(255);

alter table stamp
    add constraint FK_KPY7QI4OX3LIY20GRSP7D449M_INDEX_4
    foreign key (faxtomailuser)
    references faxtomailuser;

update stamp set signing = 'f';

-- add default signing

alter table faxtomailuser add defaultsigning varchar(255);
alter table faxtomailuser
    add constraint FK_CDWL91T1FPNO1PW07532VPN21_INDEX_7
    foreign key (defaultsigning)
    references stamp;
