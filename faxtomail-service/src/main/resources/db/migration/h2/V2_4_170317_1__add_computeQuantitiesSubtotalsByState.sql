-- add computeQuantitiesSubtotalsByState

alter table mailfolder add computeQuantitiesSubtotalsByState boolean;
update mailfolder set computeQuantitiesSubtotalsByState = 'f' where parent is null;