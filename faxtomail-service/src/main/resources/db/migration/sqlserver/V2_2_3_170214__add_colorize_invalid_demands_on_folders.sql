-- add mail folder COLORIZEINVALIDDEMANDS
alter table mailfolder add COLORIZEINVALIDDEMANDS bit;
GO
update mailfolder set COLORIZEINVALIDDEMANDS = 1 where parent is null;
GO

-- add mail folder lockedDemandsOpenableInReadOnly
alter table mailfolder add lockedDemandsOpenableInReadOnly bit;
GO
update mailfolder set lockedDemandsOpenableInReadOnly = 0 where parent is null;
GO

