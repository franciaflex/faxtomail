-- remove contraints
alter table email drop constraint FK_4q69gdpfr96aqig9ephikqxxr;
--sql serveur n'utilise pas le nom donne :(
--alter table etatAttente drop constraint UK_e1mte18x0rh5akvjfbi5ljse9;
alter table etatattentes_mailfolder drop constraint FK_3gi1yi1o932oe5t8ikcnsd3ef;
alter table etatattentes_mailfolder drop constraint FK_hsynjnyke7heuntkxkks9voxy;
alter table history_fields drop constraint FK_qoup2jo9smmju8hgou9sq5108;
DROP INDEX etatattentes_mailfolder.idx_MailFolder_etatAttentes;
DROP INDEX email.email_etatAttente_idx;
GO

-- renommage etat attente
EXEC sp_rename 'etatAttente', 'waitingState';
EXEC sp_rename 'etatattentes_mailfolder', 'mailfolder_waitingstates';
GO
EXEC sp_rename 'email.etatAttente', 'waitingState', 'COLUMN';
EXEC sp_rename 'mailfolder_waitingstates.etatAttentes', 'waitingStates', 'COLUMN';
EXEC sp_rename 'mailFolder.useCurrentLevelEtatAttente', 'useCurrentLevelWaitingState', 'COLUMN';
GO

-- migration des topia id
update waitingState set topiaId = replace(topiaId, 'EtatAttente', 'WaitingState');
update email set waitingState = replace(waitingState, 'EtatAttente', 'WaitingState');
update mailfolder_waitingstates set waitingStates = replace(waitingStates, 'EtatAttente', 'WaitingState') where waitingStates is not null;

-- migration des champs obligatoire (enum)
update configuration set searchDisplayColumns = replace(searchDisplayColumns, 'ETAT_ATTENTE', 'WAITING_STATE') where searchDisplayColumns is not null;
update mailfolder set folderTableColumns = replace(folderTableColumns, 'ETAT_ATTENTE', 'WAITING_STATE') where folderTableColumns is not null;
update DemandType set requiredFields = replace(requiredFields, 'ETAT_ATTENTE', 'WAITING_STATE') where requiredFields is not null;

-- restore constraints
--sql serveur n'utilise pas le nom donne :(
--alter table waitingState add constraint UK_ccllv66h9tle79mgklh1kpicn unique (label);
alter table email 
        add constraint FK_nfplqi5mxyfuxgkpe3rb83put 
        foreign key (waitingState) 
        references waitingState;
alter table mailfolder_waitingstates 
        add constraint FK_tb0qx61jwbtopp4xjv089syt2 
        foreign key (waitingStates) 
        references waitingState;
alter table mailfolder_waitingstates 
        add constraint FK_3ucrlum12s7ivskvvdgykxq7d 
        foreign key (mailFolder) 
        references mailFolder;
CREATE INDEX idx_MailFolder_waitingStates ON mailfolder_waitingstates(mailFolder);
CREATE INDEX email_waitingState_idx ON email(waitingState);
GO

-- Reply
delete from reply;
ALTER TABLE reply ALTER COLUMN sentDate datetime2 NOT NULL;
ALTER TABLE reply ALTER COLUMN subject varchar(MAX) NOT NULL;
ALTER TABLE reply ADD sender varchar(MAX) not null;
ALTER TABLE reply ADD recipient varchar(MAX) not null;
ALTER TABLE reply ADD replyContent varchar(255) not null;
ALTER TABLE reply ADD sentBy varchar(255) not null;

create table replyContent (
        topiaId varchar(255) not null,
        topiaVersion bigint not null,
        topiaCreateDate datetime2,
        source varbinary(MAX) not null,
        primary key (topiaId)
);

alter table reply add constraint UK_7xjte6c6h2lltfpu6ot1bel8k  unique (replyContent);
alter table reply 
        add constraint FK_7xjte6c6h2lltfpu6ot1bel8k 
        foreign key (replyContent) 
        references replyContent;

alter table reply 
        add constraint FK_9m87oooqhsbf1wbad9h4jcwm3 
        foreign key (sentBy) 
        references faxToMailUser;
GO

-- Last opener
ALTER TABLE email ADD lastAttachmentOpener varchar(255);
alter table email 
        add constraint FK_pu5j5vh83354jhoikyvb22jru 
        foreign key (lastAttachmentOpener) 
        references faxToMailUser;
GO

-- Champs hitory fields en json
DROP TABLE history_fields;
ALTER TABLE history ADD fieldsJson varchar(MAX);
GO
