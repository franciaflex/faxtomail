-- add last printing user

alter table email add lastPrintingDate datetime2;
GO

update email set email.lastPrintingDate = (select h.modificationDate
  from history h
  where h.email = email.topiaId
  and h.type = 'PRINTING'
  and h.email not in (
    select h2.email from history h2
    where (h2.type='TRANSMISSION' OR h2.type='PRINTING') and h2.modificationDate > h.modificationDate
  )
) where archiveDate is null;
