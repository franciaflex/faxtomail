-- add canViewOriginalAttachments

alter table mailfolder add canViewOriginalAttachments bit;
GO
update mailfolder set canViewOriginalAttachments = 1 where parent is null;
GO