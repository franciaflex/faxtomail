-- add showReplyAction and showForwardAction

alter table mailfolder add showReplyAction bit;
alter table mailfolder add showForwardAction bit;
GO
update mailfolder set showReplyAction = 1, showForwardAction = 0 where parent is null;
GO