
-- add mail folder level configuration
alter table mailfolder add useCurrentLevelDemandType bit;
alter table mailfolder add useCurrentLevelRange bit;
GO
update mailfolder set useCurrentLevelDemandType = 0;
update mailfolder set useCurrentLevelRange = 0;
