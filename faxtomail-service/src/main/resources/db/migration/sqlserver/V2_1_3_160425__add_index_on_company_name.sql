alter table client alter column company varchar(255);
GO
CREATE INDEX client_company_idx ON client(company);