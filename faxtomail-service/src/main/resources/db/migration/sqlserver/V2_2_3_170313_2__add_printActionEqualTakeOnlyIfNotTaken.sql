-- add printActionEqualTakeOnlyIfNotTaken

alter table mailfolder add printActionEqualTakeOnlyIfNotTaken bit;
GO
update mailfolder set printActionEqualTakeOnlyIfNotTaken = 1 where parent is null;
GO