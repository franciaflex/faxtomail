
-- move lock out of business model and to his own table
alter table email drop constraint FK_970tl3l0qssnsrvvb3m1n6ivp;
GO
alter table email drop column lockedby;
GO
create table mailLock (
    topiaId varchar(255) not null,
    topiaVersion bigint not null,
    topiaCreateDate datetime2,
    lockOn varchar(255) not null,
    lockBy varchar(255) not null,
    primary key (topiaId)
);
alter table mailLock 
    add constraint UK_cebqxr5mtsd4wmm0x0nwaa5km  unique (lockOn);
alter table mailLock 
    add constraint FK_cebqxr5mtsd4wmm0x0nwaa5km 
    foreign key (lockOn) 
    references email;
alter table mailLock 
    add constraint FK_gwxpc59s0wjg60djbk5xa4d5d 
    foreign key (lockBy) 
    references faxToMailUser;

-- edi transfer for each folder
alter table demandtype drop column editransfer;
alter table mailfolder add editransfer bit;
GO
update mailfolder set editransfer = 0 where parent is null;

-- reject allowed for email account
alter table EmailAccount add rejectAllowed bit;
GO
update EmailAccount set rejectAllowed = 0;

-- demandtype per folder
create table demandtypes_mailfolder (
    mailFolder varchar(255) not null,
    demandTypes varchar(255) not null
);
alter table demandtypes_mailfolder 
    add constraint FK_apld4ycj71ouug7vmg5wtr1y9 
    foreign key (demandTypes) 
    references demandType;
alter table demandtypes_mailfolder 
    add constraint FK_pmybd6fsyapv8ygtn7pjw258k 
    foreign key (mailFolder) 
    references mailFolder;
CREATE INDEX idx_MailFolder_demandTypes ON demandtypes_mailfolder(mailFolder);

-- range per folder
create table mailfolder_ranges (
    mailFolder varchar(255) not null,
    ranges varchar(255) not null
);
alter table mailfolder_ranges 
    add constraint FK_kiolyiaeicw5he7xlima0ugbb 
    foreign key (ranges) 
    references range;
alter table mailfolder_ranges 
    add constraint FK_h78fwd9gc92wh7vw612q48xrr 
    foreign key (mailFolder) 
    references mailFolder;
CREATE INDEX idx_MailFolder_ranges ON mailfolder_ranges(mailFolder);
GO