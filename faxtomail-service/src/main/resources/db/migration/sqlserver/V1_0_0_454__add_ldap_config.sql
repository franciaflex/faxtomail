
-- add user and group hidden configuration
alter table FaxToMailUserGroup add hidden bit;
alter table FaxToMailUser add hidden bit;
GO
update FaxToMailUserGroup set hidden = 0;
update FaxToMailUser set hidden = 0;
