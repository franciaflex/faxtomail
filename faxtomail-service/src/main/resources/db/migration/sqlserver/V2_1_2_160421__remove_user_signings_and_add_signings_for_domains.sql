alter table faxtomailuser drop constraint FK_CDWL91T1FPNO1PW07532VPN21_INDEX_7;
GO
alter table faxtomailuser drop column defaultsigning;
GO

delete from stamp where signing = 1;
GO

alter table stamp drop constraint FK_KPY7QI4OX3LIY20GRSP7D449M_INDEX_4;
GO
alter table stamp drop column signing;
alter table stamp drop column faxtomailuser;
GO

create table signingForDomain (
    topiaId varchar(255) not null,
    topiaVersion bigint not null,
    topiaCreateDate datetime2,
    domainName varchar(255) unique,
    image varchar(MAX),
    text varchar(MAX),
    primary key (topiaId)
);
