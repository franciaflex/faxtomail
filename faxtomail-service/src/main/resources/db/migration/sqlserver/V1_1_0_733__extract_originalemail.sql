
-- extract original email from email
create table originalEmail (
    topiaId varchar(255) not null,
    topiaVersion bigint not null,
    topiaCreateDate datetime2,
    content varchar(MAX) not null,
    email varchar(255) not null,
    primary key (topiaId)
);
GO

insert into originalEmail
select 'com.franciaflex.faxtomail.persistence.entities.OriginalEmail_' + cast(newid() as varchar(255)), 0, topiaCreateDate, originalEmail, topiaId from email;

merge into email as e
using originalEmail as o
on (e.topiaId = o.email)
when matched then update set e.originalEmail = o.topiaId;
GO

alter table email alter column originalEmail varchar(255) not NULL;
alter table email
        add constraint UK_P4YIV4FRJSIKCMLMFOX8CF6RE
        unique (originalEmail);
GO

alter table originalEmail drop column email;
GO