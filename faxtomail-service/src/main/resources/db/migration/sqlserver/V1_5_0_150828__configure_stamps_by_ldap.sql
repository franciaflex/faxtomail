-- remove configuration by mail folder

alter table mailfolder drop column usecurrentlevelpdfeditorstamps;

GO

drop table MAILFOLDER_PDFEDITORSTAMPS;

GO

create table PDFEDITORSTAMPS_USERS (
    USERS varchar(255) not null,
    PDFEDITORSTAMPS varchar(255) not null
);

GO

alter table PDFEDITORSTAMPS_USERS
    add constraint FK_9EMRBYUL9EJBXBXDRISLN1W56_INDEX_F
    foreign key (USERS)
    references FAXTOMAILUSER;

GO

alter table PDFEDITORSTAMPS_USERS
    add constraint FK_RC8JM7WI0GR4O05CL6BKRCC9T_INDEX_F
    foreign key (PDFEDITORSTAMPS)
    references STAMP;

GO

CREATE INDEX idx_PDFEDITORSTAMPS_USERS ON PDFEDITORSTAMPS_USERS(USERS);

GO

create table GROUPS_PDFEDITORSTAMPS  (
    GROUPS varchar(255) not null,
    PDFEDITORSTAMPS varchar(255) not null
);

GO

alter table GROUPS_PDFEDITORSTAMPS
    add constraint FK_47041IYC9CUBBX7OB6E1B32QD
    foreign key (GROUPS)
    references FAXTOMAILUSERGROUP;

GO

alter table GROUPS_PDFEDITORSTAMPS
    add constraint FK_6I724UYTC1U81R3LXHKMWVF55_INDEX_F
    foreign key (PDFEDITORSTAMPS)
    references STAMP;

GO

CREATE INDEX idx_GROUPS_PDFEDITORSTAMPS  ON GROUPS_PDFEDITORSTAMPS (GROUPS);

GO