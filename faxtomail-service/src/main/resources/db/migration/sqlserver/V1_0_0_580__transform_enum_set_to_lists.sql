-- configuration
alter table configuration_invalidFormDisabledActions drop constraint FK_fj3yycjetl6acpiybfsg4v0yq;
alter table configuration_searchDisplayColumns drop constraint FK_9ffmpp0utfad26oxe70efo1da;
GO
drop table configuration_invalidFormDisabledActions;
drop table configuration_searchDisplayColumns;
alter table configuration add searchDisplayColumns varchar(MAX);
alter table configuration add invalidFormDisabledActions varchar(MAX);
GO

-- demande type
alter table demandType_fields drop constraint FK_c44wglsh0xpua19f1ps9vn24i;
GO
drop table demandType_fields;
alter table demandType add requiredFields varchar(MAX);
GO

-- etat attente
alter table etatAttente_invalidFormDisabledActions drop constraint FK_221gjt0dyjtbndk8v7tr85ta6 ;
alter table etatAttente_validFormDisabledActions drop constraint FK_86hqc0dxhqyca7rbx7b3bfydr;
GO
drop table etatAttente_invalidFormDisabledActions;
drop table etatAttente_validFormDisabledActions;
alter table etatAttente add invalidFormDisabledActions varchar(MAX);
alter table etatAttente add validFormDisabledActions varchar(MAX);
GO

-- mail folder
alter table mailFolder_folderTableColumns drop constraint FK_ap7lwii8to9wlxefnu3pn0goe;
GO
drop table mailFolder_folderTableColumns;
alter table mailFolder add folderTableColumns varchar(MAX);
GO