-- add showAttachmentPreview
alter table mailfolder add showAttachmentPreview bit;
GO
update mailfolder set showAttachmentPreview = 0 where parent is null;
GO