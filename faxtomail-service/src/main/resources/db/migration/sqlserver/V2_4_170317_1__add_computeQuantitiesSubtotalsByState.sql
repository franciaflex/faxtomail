-- add computeQuantitiesSubtotalsByState

alter table mailfolder add computeQuantitiesSubtotalsByState bit;
GO
update mailfolder set computeQuantitiesSubtotalsByState = 0 where parent is null;
GO