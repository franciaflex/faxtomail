-- add generatedPDFPages
IF (NOT EXISTS (SELECT *
                 FROM INFORMATION_SCHEMA.TABLES
                 WHERE TABLE_NAME = 'generatedPDFPage') )
BEGIN
    create table generatedPDFPage (
        page VARBINARY(MAX) not null,
        topiaId VARCHAR(255) NOT NULL,
        topiaVersion BIGINT  NOT NULL,
        topiaCreateDate  datetime2,
        attachment VARCHAR(255),
        PRIMARY KEY (topiaId))
    ALTER TABLE GENERATEDPDFPAGE
        ADD CONSTRAINT FK_9CRCVUPPG70VCFW7334VAH7PJ
        FOREIGN KEY(ATTACHMENT)
        REFERENCES ATTACHMENT
END
GO