-- add mustTakeToEditDemand

alter table mailfolder add displayHelpOnMessages bit;
GO
update mailfolder set displayHelpOnMessages = 1 where parent is null;
GO