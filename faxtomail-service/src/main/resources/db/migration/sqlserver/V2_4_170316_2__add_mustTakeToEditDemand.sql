-- add mustTakeToEditDemand

alter table mailfolder add mustTakeToEditDemand bit;
GO
update mailfolder set mustTakeToEditDemand = 1 where parent is null;
GO