alter table mailfolder add useCurrentLevelInvalidFormDisabledActions bit;
GO
alter table mailfolder add invalidFormDisabledActions varchar(MAX);
GO

update mailfolder set useCurrentLevelInvalidFormDisabledActions = 0;
GO
update mailfolder
set useCurrentLevelInvalidFormDisabledActions = 1,
    invalidFormDisabledActions = (select invalidFormDisabledActions from configuration)
where parent is null;
GO

alter table configuration drop column invalidFormDisabledActions;
GO