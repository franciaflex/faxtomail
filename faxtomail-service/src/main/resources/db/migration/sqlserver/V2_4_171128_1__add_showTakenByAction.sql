-- add showTakenByAction

alter table mailfolder add showTakenByAction bit;
GO
update mailfolder set showTakenByAction = 0 where parent is null;
GO