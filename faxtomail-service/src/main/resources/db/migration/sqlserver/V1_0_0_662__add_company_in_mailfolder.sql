
-- add mail folder level configuration
alter table mailfolder add useCurrentLevelCompany bit;
alter table mailfolder add company varchar(MAX);
GO
update mailfolder set useCurrentLevelCompany = 0;