-- add canChangeOrderInTable

alter table mailfolder add canChangeOrderInTable bit;
GO
update mailfolder set canChangeOrderInTable = 0 where parent is null;
GO