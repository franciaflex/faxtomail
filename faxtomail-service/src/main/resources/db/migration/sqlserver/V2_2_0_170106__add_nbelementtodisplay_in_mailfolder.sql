
-- add mail folder level configuration
alter table mailfolder add useCurrentLevelNbElementToDisplay bit;
alter table mailfolder add nbElementToDisplay integer;
GO
update mailfolder set useCurrentLevelNbElementToDisplay = 0;