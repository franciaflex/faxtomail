-- add imageCleaned
IF (NOT EXISTS (SELECT *
                 FROM INFORMATION_SCHEMA.COLUMNS
                 WHERE COLUMN_NAME = 'imageCleaned') )
BEGIN
	alter table email add imageCleaned bit default 0
	update email set imageCleaned = 0
END
GO