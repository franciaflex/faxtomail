
-- remove unused association
ALTER TABLE faxtomailusergroup DROP constraint FK_8efs0kgau1bt0iifrn8bdbecy;
ALTER TABLE faxtomailusergroup DROP COLUMN faxtomailusergroup;

-- add column faxAccountType on emailAccount
ALTER TABLE emailAccount add faxAccountType bit;
GO
update emailAccount set faxAccountType = 0;

-- see http://www.sql-server-helper.com/tips/date-formats.aspx for convert

-- add email_idx on attachment
ALTER TABLE attachment ADD email_idx integer;
GO
UPDATE attachment SET email_idx = (
    SELECT COUNT(topiaid) FROM attachment sub
    WHERE sub.email = attachment.email
    AND (CONVERT(VARCHAR(23), sub.topiacreatedate, 126) + sub.topiaid) < (CONVERT(VARCHAR(23), attachment.topiacreatedate, 126) + attachment.topiaid)
);

-- add email_idx on history
ALTER TABLE history ADD email_idx integer;
GO
UPDATE history SET email_idx = (
    SELECT COUNT(topiaid) FROM history sub
    WHERE sub.email = history.email
    AND (CONVERT(VARCHAR(23), sub.topiacreatedate, 126) + sub.topiaid) < (CONVERT(VARCHAR(23), history.topiacreatedate, 126) + history.topiaid)
);

-- add email_idx on rangeRow
ALTER TABLE rangeRow ADD email_idx integer;
GO
UPDATE rangeRow SET email_idx = (
    SELECT COUNT(topiaid) FROM rangeRow sub
    WHERE sub.email = rangeRow.email
    AND (CONVERT(VARCHAR(23), sub.topiacreatedate, 126) + sub.topiaid) < (CONVERT(VARCHAR(23), rangeRow.topiacreatedate, 126) + rangeRow.topiaid)
);

-- add email_idx on reply
ALTER TABLE reply ADD email_idx integer;
GO
UPDATE reply SET email_idx = (
    SELECT COUNT(topiaid) FROM reply sub
    WHERE sub.email = reply.email
    AND (CONVERT(VARCHAR(23), sub.topiacreatedate, 126) + sub.topiaid) < (CONVERT(VARCHAR(23), reply.topiacreatedate, 126) + reply.topiaid)
);
