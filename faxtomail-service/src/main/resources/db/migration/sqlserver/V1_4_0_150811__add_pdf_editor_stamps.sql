-- add pdf editor stamps

create table STAMP (
    topiaId varchar(255) not null,
    topiaVersion bigint not null,
    topiaCreateDate datetime2,
    label varchar(MAX),
    description varchar(MAX),
    image varchar(MAX),
    text varchar(MAX),
    primary key (topiaId)
);

-- add association between mail folder and stamp

create table MAILFOLDER_PDFEDITORSTAMPS (
    MAILFOLDER varchar(255) not null,
    PDFEDITORSTAMPS varchar(255) not null
);

alter table MAILFOLDER_PDFEDITORSTAMPS
    add constraint FK_47041IYC9CUBBX7OB6E1B32QD
    foreign key (MAILFOLDER)
    references MAILFOLDER;

alter table MAILFOLDER_PDFEDITORSTAMPS
    add constraint FK_IL3874S78O4MA6YG1EONJ213D
    foreign key (PDFEDITORSTAMPS)
    references STAMP;

CREATE INDEX idx_MAILFOLDER_PDFEDITORSTAMPS ON MAILFOLDER_PDFEDITORSTAMPS(MAILFOLDER);

GO

-- add useCurrentLevelPdfEditorStamps in mail folder

alter table mailfolder add useCurrentLevelPdfEditorStamps bit;
GO
update mailfolder set useCurrentLevelPdfEditorStamps = 0;