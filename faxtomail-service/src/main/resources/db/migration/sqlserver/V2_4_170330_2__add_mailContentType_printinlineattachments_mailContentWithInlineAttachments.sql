-- add mailContentType

alter table attachment add mailContent bit;
GO
update attachment set mailContent = 0;
update attachment set mailContent = 1 where originalFileName like 'contenu % du mail%.pdf';
GO

-- add printinlineattachments and mailContentWithInlineAttachments

alter table mailfolder add printinlineattachments bit;
alter table mailfolder add mailContentWithInlineAttachments bit;
GO
update mailfolder set printinlineattachments = 1, mailContentWithInlineAttachments = 0 where parent is null;
GO