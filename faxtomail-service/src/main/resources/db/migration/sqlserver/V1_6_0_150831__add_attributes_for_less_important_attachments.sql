alter table attachment add inlineAttachment bit;
alter table attachment add lessImportant bit;
GO

update attachment set inlineAttachment = 0;
update attachment set lessImportant = 0;