-- add faxFromNumber
IF (NOT EXISTS (SELECT *
                 FROM INFORMATION_SCHEMA.COLUMNS
                 WHERE COLUMN_NAME = 'faxFromNumber') )
BEGIN
	alter table mailfolder add faxFromNumber varchar(255)
	alter table mailfolder add useCurrentLevelFaxFromNumber bit
	update mailfolder set useCurrentLevelFaxFromNumber = 0
END
GO