
-- remove topia from newclient
drop table newclient;
GO
create table newClient (
    name varchar(MAX),
    emailAddress varchar(255),
    faxNumber varchar(255),
    caracteristic1 varchar(MAX),
    caracteristic2 varchar(MAX),
    caracteristic3 varchar(MAX),
    code varchar(255) not null,
    company varchar(255) not null,
    brand varchar(MAX),
    personInCharge varchar(MAX),
    unique (code, company, emailAddress, faxNumber)
);
GO

-- remove topia from edireturn
drop table edireturn;
GO
create table ediReturn (
    rangeRowTopiaId varchar(255) not null,
    commandNumber varchar(MAX),
    error varchar(MAX),
    unique (rangeRowTopiaId)
);
GO

-- clear client table
update email set client = null;
delete from client;

-- add company in client
--drop constraint UK_b4ck8pelycojqmbrd8n36mdfw;

alter table client drop column emailAddress;
alter table client drop column faxNumber;
alter table client drop column id;

alter table client add company varchar(255) not NULL;
alter table client add emailAddressesJson varchar(MAX);
alter table client add faxNumbersJson varchar(MAX);

alter table client alter column code varchar(255) not NULL;
alter table client alter column brand varchar(MAX) not NULL;

alter table client add constraint UK_kqpy7y2f4onck5julevr0hfg1 unique (code, company);
GO
