-- add displayOnlyUserTrigraphInTables

alter table mailfolder add displayOnlyUserTrigraphInTables bit;
GO
update mailfolder set displayOnlyUserTrigraphInTables = 1 where parent is null;
GO