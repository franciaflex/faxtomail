package db.migration.h2;

/*
 * #%L
 * FaxToMail :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import org.apache.commons.mail.util.MimeMessageUtils;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import org.flywaydb.core.internal.util.logging.Log;
import org.flywaydb.core.internal.util.logging.LogFactory;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 1.1.10
 */
public class V1_1_10_735__subject_migration implements JdbcMigration {

    /** Logger. */
    private static final Log log = LogFactory.getLog(V1_1_10_735__subject_migration.class);

    @Override
    public void migrate(Connection connection) throws Exception {

        PreparedStatement updateStatement = null;
        ResultSet resultSet = null;

        try {
            connection.setAutoCommit(false);

            updateStatement =
                    connection.prepareStatement("UPDATE email SET subject = ? WHERE topiaid = ?");

            int emailWoSubjectRemaining = 100;

            while (emailWoSubjectRemaining > 0) {

                emailWoSubjectRemaining = 0;

                resultSet = connection.createStatement()
                        .executeQuery("SELECT e.topiaId, oe.content " +
                                      "FROM email e LEFT OUTER JOIN originalemail oe " +
                                      "ON e.originalemail = oe.topiaid " +
                                      "WHERE e.archivedate IS NULL AND e.subject IS NULL " +
                                      "LIMIT 100");

                while (resultSet.next()) {
                    emailWoSubjectRemaining++;

                    String emailId = resultSet.getString(1);
                    String emailContent = resultSet.getString(2);

                    Properties properties = new Properties();
                    // set the mail.mime.address.strict to false to avoid
                    // javax.mail.internet.AddressException: Domain contains illegal character errors when recipients contains []
                    properties.setProperty("mail.mime.address.strict", "false");

                    Session session = Session.getInstance(properties);
                    String subject;
                    try {
                        MimeMessage message = MimeMessageUtils.createMimeMessage(session, emailContent);
                        subject = FaxToMailServiceUtils.getDecodedSubject(message.getSubject());

                    } catch (Exception e) {
                        subject = "";
                    }

                    updateStatement.setString(1, subject);
                    updateStatement.setString(2, emailId);
                    updateStatement.addBatch();
                }

                log.info("update " + emailWoSubjectRemaining + " emails");

                updateStatement.executeBatch();
                resultSet.close();
            }

            log.info("commit");
            connection.commit();

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (updateStatement != null) {
                updateStatement.close();
            }
        }

        log.info("done !");
    }
}
