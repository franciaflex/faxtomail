package com.franciaflex.faxtomail.services.service.imports;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.EmailAccount;
import com.franciaflex.faxtomail.persistence.entities.EmailAccountImpl;
import com.franciaflex.faxtomail.persistence.entities.EmailProtocol;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ext.AbstractImportModel;

import java.text.ParseException;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class EmailAccountImportModel extends AbstractImportModel<EmailAccount> {

    public EmailAccountImportModel(char separator) {
        super(separator);

        newMandatoryColumn("protocol", EmailAccount.PROPERTY_PROTOCOL, new ValueParser<EmailProtocol>() {

            @Override
            public EmailProtocol parse(String value) throws ParseException {
                EmailProtocol protocol = EmailProtocol.valueOf(value.toUpperCase());
                return protocol;
            }
            
        });
        newMandatoryColumn("host", EmailAccount.PROPERTY_HOST);
        newMandatoryColumn("port", EmailAccount.PROPERTY_PORT, new ValueParser<Integer>() {

            @Override
            public Integer parse(String value) throws ParseException {
                Integer result = Integer.valueOf(value);
                return result;
            }
            
        });
        newMandatoryColumn("user", EmailAccount.PROPERTY_LOGIN);
        newMandatoryColumn("password", EmailAccount.PROPERTY_PASSWORD);
    }

    @Override
    public EmailAccount newEmptyInstance() {
        return new EmailAccountImpl();
    }
}
