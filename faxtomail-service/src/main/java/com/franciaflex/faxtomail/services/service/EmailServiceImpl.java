package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.beans.QuantitiesByRange;
import com.franciaflex.faxtomail.persistence.entities.*;
import com.franciaflex.faxtomail.services.FaxToMailServiceSupport;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.franciaflex.faxtomail.services.service.exceptions.AlreadyLockedMailException;
import com.franciaflex.faxtomail.services.service.exceptions.FolderNotReadableException;
import com.franciaflex.faxtomail.services.service.exceptions.InvalidClientException;
import com.franciaflex.faxtomail.services.service.imports.ArchiveImportExportBean;
import com.franciaflex.faxtomail.services.service.imports.ArchiveImportExportModel;
import com.franciaflex.faxtomail.services.service.imports.ArchiveImportResult;
import com.franciaflex.faxtomail.services.service.imports.InvalidArchiveImportBeanException;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailConstants;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaQueryException;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.persistence.support.TopiaSqlWork;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import static org.nuiton.i18n.I18n.t;

/**
 * @author kmorin - kmorin@codelutin.com
 */
public class EmailServiceImpl extends FaxToMailServiceSupport implements EmailService {

    private static final Log log = LogFactory.getLog(EmailServiceImpl.class);
    public static final String SEMICOLON_SPLIT_PATTERN = "\\s*;\\s*";
    public static final String DD_MM_YYYY_HH_MM = "dd/MM/yyyy HH:mm";

    protected Binder<Attachment, Attachment> attachmentBinder =
            BinderFactory.newBinder(Attachment.class, Attachment.class);

    public static final Ordering<GeneratedPDFPage> GENERATED_PDF_PAGE_NATURAL_ORDERING = Ordering.natural().onResultOf(new Function<GeneratedPDFPage, Date>() {
        @Override
        public Date apply(GeneratedPDFPage generatedPDFPage) {
            return generatedPDFPage.getTopiaCreateDate();
        }
    });

    @Override
    public Email getEmailById(String id) {
        Email email = getPersistenceContext().getEmailDao().forTopiaIdEquals(id).findUnique();
        return email;
    }

    protected Email getEmailById(String id, String fetch, String... otherFetches) {
        Email email = getPersistenceContext().getEmailDao().forTopiaIdEquals(id).addAllFetches(fetch, otherFetches).findUnique();
        return email;
    }

    @Override
    public Email getFullEmailById(String id) {

        Email email = getPersistenceContext().getEmailDao()
                .forTopiaIdEquals(id)
                .addAllFetches(Email.PROPERTY_DEMAND_TYPE,
                               Email.PROPERTY_TAKEN_BY,
                               Email.PROPERTY_LAST_ATTACHMENT_OPENER,
                               Email.PROPERTY_LAST_PRINTING_USER,
                               Email.PROPERTY_CLIENT,
                               Email.PROPERTY_WAITING_STATE,
                               Email.PROPERTY_ORIGINAL_EMAIL,
                               Email.PROPERTY_PRIORITY).findUnique();

        // manual fetch
        List<RangeRow> rangeRows = email.getRangeRow();
        if (rangeRows != null) {
            for (RangeRow rangeRow : rangeRows) {
                Hibernate.initialize(rangeRow.getRange());
            }
        }
        Hibernate.initialize(email.getMatchingClients());
        Hibernate.initialize(email.getReplies());
        Hibernate.initialize(email.getAttachment());
        Hibernate.initialize(email.getMailFolder());
        //should also initialize parents
        MailFolder folder = email.getMailFolder();
        while (folder.getParent() != null){
            folder = folder.getParent();
            Hibernate.initialize(folder);
        }
        Collection<History> histories = email.getHistory();
        if (histories != null) {
            for (History history : histories) {
                Hibernate.initialize(history.getFaxToMailUser());
            }
        }
        EmailGroup emailGroup = email.getEmailGroup();
        if (emailGroup != null) {
            Collection<Email> emails = emailGroup.getEmail();
            for (Email email2 : emails) {
                Hibernate.initialize(email2.getMailFolder());
                Hibernate.initialize(email2.getAttachment());
            }
        }

        return email;
    }

    @Override
    public Email getFullEmailById(String id, FaxToMailUser user) {
        Email email = getFullEmailById(id);

        MailFolderService mailFolderService = serviceContext.getMailFolderService();
        MailFolder folder = email.getMailFolder();
        browseFolderParent(folder, user, mailFolderService);

        return email;
    }

    protected void browseFolderParent(MailFolder folder, FaxToMailUser user, MailFolderService mailFolderService) {
        Hibernate.initialize(folder);
        mailFolderService.fetchFolderAttributes(folder);

        MailFolder parent = folder.getParent();
        if (parent != null) {
            browseFolderParent(parent, user, mailFolderService);
        }

        boolean writable = parent != null && parent.isFolderWritable();
        if (folder.getWriteRightGroups() != null && user.getUserGroups() != null) {
            writable |= CollectionUtils.containsAny(folder.getWriteRightGroups(), user.getUserGroups());
        }
        writable |= folder.containsWriteRightUsers(user);

        boolean readable = parent != null && parent.isFolderReadable();
        if (folder.getReadRightGroups() != null && user.getUserGroups() != null) {
            readable |= CollectionUtils.containsAny(folder.getReadRightGroups(), user.getUserGroups());
        }
        readable |= folder.containsReadRightUsers(user);

        folder.setFolderWritable(writable);
        folder.setFolderReadable(readable);
    }

    /**
     * Save email with default email client code.
     * 
     * @param email email
     * @param user user
     * @param modifiedFields modified fields
     * @return updated email instance
     * @throws InvalidClientException if client code is not valid
     */
    @Override
    public Email saveEmail(Email email, FaxToMailUser user, String... modifiedFields) throws InvalidClientException {
        return saveEmail(email, null, email.getReplies(), user, modifiedFields);
    }

    @Override
    public Email saveEmail(Email email, Collection<Attachment> attachments, Collection<Reply> replies,
            FaxToMailUser user, String... modifiedFields) throws InvalidClientException {
        Date now = getNow();
        EmailTopiaDao dao = getPersistenceContext().getEmailDao();
        HistoryTopiaDao historyDao = getPersistenceContext().getHistoryDao();

        MailFolder folder = email.getMailFolder();
        while (!folder.isUseCurrentLevelCompany() && folder.getParent() != null) {
            folder = folder.getParent();
        }

        if (attachments != null) {
            updateAttachments(email, attachments, user);
        }

        if (replies != null) {
            updateReplies(email);
        }

        if (email.getRangeRow() != null) {
            RangeRowTopiaDao rangeRowDao = getPersistenceContext().getRangeRowDao();
            for (RangeRow rangeRow : email.getRangeRow()) {
                if (!rangeRow.isPersisted()) {
                    rangeRowDao.create(rangeRow);
                } else {
                    rangeRowDao.update(rangeRow);
                }
            }
        }

        OriginalEmail originalEmail = email.getOriginalEmail();
        if (!originalEmail.isPersisted()) {
            getPersistenceContext().getOriginalEmailDao().create(originalEmail);
        }

        Collection<History> histories;
        if (email.isPersisted()) {
            histories = historyDao.forEquals("email.topiaId", email.getTopiaId()).findAll();
        } else {
            histories = email.getHistory();
            if (histories != null) {
                historyDao.createAll(histories);
            }
            email = dao.create(email);
        }

        Set<String> fieldSet = Sets.newHashSet(modifiedFields);


        History transmissionToEdi = CollectionUtils.find(histories, new Predicate<History>() {
            @Override
            public boolean evaluate(History object) {
                return object.getType() == HistoryType.TRANSMISSION_TO_EDI;
            }
        });

        // we transmit to EDI if:
        // - it has never been transmitted before
        // - all the required fields are filled
        if (transmissionToEdi == null
                && email.getClient() != null
                && email.getDemandType() != null
                && StringUtils.isNotBlank(email.getProjectReference())) {

            handleEdiTransmission(email, historyDao, fieldSet);
        }

        if (histories != null) {
            historyDao.createAll(Collections2.filter(histories, new com.google.common.base.Predicate<History>() {
                @Override
                public boolean apply(History history) {
                    return !history.isPersisted();
                }
            }));
        }

        History history = null;
        if (fieldSet.contains(Email.PROPERTY_ARCHIVE_DATE)) {
            history = historyDao.create(History.PROPERTY_TYPE, HistoryType.ARCHIVED,
                                        History.PROPERTY_FAX_TO_MAIL_USER, user,
                                        History.PROPERTY_MODIFICATION_DATE, now,
                                        History.PROPERTY_EMAIL, email);

        } else if (fieldSet.contains(Email.PROPERTY_MAIL_FOLDER)) {
            history = historyDao.create(History.PROPERTY_TYPE, HistoryType.TRANSMISSION,
                                        History.PROPERTY_FAX_TO_MAIL_USER, user,
                                        History.PROPERTY_MODIFICATION_DATE, now,
                                        History.PROPERTY_EMAIL, email);

        } else {
            if (email.getTakenBy() == null &&
                    !fieldSet.isEmpty() && !fieldSet.contains(Email.PROPERTY_TAKEN_BY)) {
                email.setTakenBy(user);
                fieldSet.add(Email.PROPERTY_TAKEN_BY);
            }

            boolean historyEmpty = CollectionUtils.isEmpty(histories);
            if (historyEmpty || !fieldSet.isEmpty()) {
                history = historyDao.create(History.PROPERTY_TYPE, historyEmpty ? HistoryType.CREATION : HistoryType.MODIFICATION,
                                            History.PROPERTY_FAX_TO_MAIL_USER, user,
                                            History.PROPERTY_MODIFICATION_DATE, now,
                                            History.PROPERTY_EMAIL, email,
                                            History.PROPERTY_FIELDS_JSON, AbstractFaxToMailTopiaDao.GSON_INSTANCE.toJson(fieldSet));
            }
        }
        if (history != null) {
            email.addHistory(history);
        }

        Email result = dao.update(email);
        getPersistenceContext().commit();

        return result;
    }


    @Override
    public List<Email> takeBy(final List<Email> emails, final FaxToMailUser user) {

        TopiaSqlSupport sqlSupport = getPersistenceContext().getSqlSupport();

        sqlSupport.doSqlWork(new TopiaSqlWork() {
            @Override
            public void execute(Connection connection) throws SQLException {
                Date now = getNow();

                HistoryTopiaDao historyDao = getPersistenceContext().getHistoryDao();

                for (Email email:emails) {

                    Set<String> fieldSet = Sets.newHashSet(Email.PROPERTY_TAKEN_BY);
                    final History history = historyDao.create(History.PROPERTY_TYPE, HistoryType.MODIFICATION,
                            History.PROPERTY_FAX_TO_MAIL_USER, user,
                            History.PROPERTY_MODIFICATION_DATE, now,
                            History.PROPERTY_EMAIL, email,
                            History.PROPERTY_FIELDS_JSON, AbstractFaxToMailTopiaDao.GSON_INSTANCE.toJson(fieldSet));

                    getPersistenceContext().getHibernateSupport().getHibernateSession().flush();

                    try (PreparedStatement updateStatement = connection.prepareStatement("UPDATE email SET takenby = ? WHERE topiaid = ?");
                         PreparedStatement historyStatement = connection.prepareStatement("UPDATE history SET email = ? WHERE topiaid = ?")) {

                        if (user != null) {
                            updateStatement.setString(1, user.getTopiaId());
                        } else {
                            updateStatement.setString(1, null);
                        }
                        updateStatement.setString(2, email.getTopiaId());
                        int result = updateStatement.executeUpdate();

                        historyStatement.setString(1, email.getTopiaId());
                        historyStatement.setString(2, history.getTopiaId());
                        int result2 = historyStatement.executeUpdate();

                        if (result != 1 || result2 != 1) {
                            if (user != null) {
                                log.error(String.format("Could not take email (%s) by user (%s)", email.getTopiaId(), user.getTopiaId()));
                            } else {
                                log.error(String.format("Could not take email (%s) by null user", email.getTopiaId()));
                            }
                        }
                    }
                }


                connection.commit();
            }
        });

        getPersistenceContext().commit();

        getPersistenceContext().getHibernateSupport().getHibernateSession().clear();

        List<Email> returnValues= new ArrayList<>();

        for (Email email:emails) {

            Email returnValue = getFullEmailById(email.getTopiaId());

            returnValues.add(returnValue);
        }

        return returnValues;
    }

    /**
     * Les réponses sont modifiées par l'ui pour ne pas charger le contenu binaire, pour la sauvegarde,
     * il faut bien rétablir les replyContent avec ceux en base.
     * 
     * @param email
     */
    protected void updateReplies(Email email) {
        ReplyTopiaDao replyTopiaDao = getPersistenceContext().getReplyDao();

        List<Reply> currentReplies = new ArrayList<Reply>(email.getReplies());
        email.clearReplies();
        for (Reply reply : currentReplies) {
            // normalement elle sont toutes persistée et non modifiées par l'ui
            // donc reprendre la version en base avec un replyContent non null est suffisant dans ce cas
            Reply newReply = replyTopiaDao.forTopiaIdEquals(reply.getTopiaId()).findUnique();
            email.addReplies(newReply);
        }
    }

    /**
     * Les attachment sont copiés dans l'UI sans la valorisation des champs "originalFile" et "editedFile".
     * Ici ont revalorise ces champs pour la sauvegarde.
     * 
     * @param email
     * @param attachments
     * @param user
     */
    protected void updateAttachments(Email email, Collection<Attachment> attachments, FaxToMailUser user) {
        AttachmentTopiaDao attachmentTopiaDao = getPersistenceContext().getAttachmentDao();
        HistoryTopiaDao historyTopiaDao = getPersistenceContext().getHistoryDao();

        List<Attachment> currentAttachments = attachmentTopiaDao.forTopiaIdIn(email.getAttachmentTopiaIds()).findAll();
        if (currentAttachments == null) {
            currentAttachments = new ArrayList<>();
        }
        Map<String, Attachment> currentAttachmentIndex = new HashMap<>(Maps.uniqueIndex(currentAttachments,
                                                                                        TopiaEntities.getTopiaIdFunction()));

        Set<String> newFiles = new HashSet<>();

        AttachmentFileTopiaDao attachementFileTopiaDao = getPersistenceContext().getAttachmentFileDao();
        for (Attachment attachment : attachments) {

            // get session attachment from id -> currentAttachment = attachment stored in db
            Attachment currentAttachment;
            if (StringUtils.isNotBlank(attachment.getTopiaId())) {
                currentAttachment = currentAttachmentIndex.remove(attachment.getTopiaId());
            } else {
                currentAttachment = new AttachmentImpl();
            }

            AttachmentFile originalFile = null;
            AttachmentFile editedFile = null;
            Collection<GeneratedPDFPage> pages = null;
            // dans tout les cas, les pieces jointes courantes de l'attachment prévalent
            // sur celles en base
            if (attachment.getOriginalFile() != null) {
                originalFile = attachment.getOriginalFile();
            } else {
                originalFile = currentAttachment.getOriginalFile();
            }
            if (attachment.getEditedFile() != null) {
                editedFile = attachment.getEditedFile();
            } else {
                editedFile = currentAttachment.getEditedFile();
            }

            if (attachment.getGeneratedPDFPages() != null) {
                if (currentAttachment.getGeneratedPDFPages() == null) {
                    pages = attachment.getGeneratedPDFPages();
                } else {
                    pages = currentAttachment.getGeneratedPDFPages();
                    for (GeneratedPDFPage page:attachment.getGeneratedPDFPages()){
                        if (!pages.contains(page)){
                            pages.add(page);
                        }
                    }
                }
            }

            // copy new data
            attachmentBinder.copyExcluding(attachment, currentAttachment,
                    Attachment.PROPERTY_EMAIL,
                    Attachment.PROPERTY_EDITED_FILE,
                    Attachment.PROPERTY_EDITED_FILE_NAME,
                    Attachment.PROPERTY_ORIGINAL_FILE,
                    Attachment.PROPERTY_ORIGINAL_FILE_NAME,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    Attachment.PROPERTY_GENERATED_PDFPAGES);

            currentAttachment.setEmail(email);
            currentAttachment.setEditedFile(editedFile);
            currentAttachment.setOriginalFile(originalFile);

            // ici les pieces jointes peuvent être sauvegardées sans que les pièces jointes
            // soit présentes dans les entités car l'ui ne les a pas copiées
            // pour ne pas les charger inutilement
            // donc on les remet manuellement

            if (!originalFile.isPersisted()) {
                attachementFileTopiaDao.create(originalFile);
                newFiles.add(originalFile.getFilename());

            } else {
                attachementFileTopiaDao.update(originalFile);
            }

            if (editedFile != null) {
                if (!editedFile.isPersisted()) {
                    attachementFileTopiaDao.create(editedFile);

                } else {
                    attachementFileTopiaDao.update(editedFile);
                }
            }

            if (!currentAttachment.isPersisted()) {
                //Deal here with new generated pdf pages (Mail import case)
                currentAttachment.setGeneratedPDFPages(pages);

                // persist using cascade
                currentAttachment = attachmentTopiaDao.create(currentAttachment);
                currentAttachments.add(currentAttachment);

            } else {
                attachmentTopiaDao.update(currentAttachment);
            }
        }

        if (!newFiles.isEmpty()) {
            History history = historyTopiaDao.create(History.PROPERTY_TYPE, HistoryType.ATTACHMENT_ADDITION,
                                                     History.PROPERTY_FAX_TO_MAIL_USER, user,
                                                     History.PROPERTY_MODIFICATION_DATE, new Date(),
                                                     History.PROPERTY_EMAIL, email,
                                                     History.PROPERTY_FIELDS_JSON, AbstractFaxToMailTopiaDao.GSON_INSTANCE.toJson(newFiles));
            email.addHistory(history);
        }

        // delete not found attachments
        for (Attachment attachment : currentAttachmentIndex.values()) {
            currentAttachments.remove(attachment);
        }

        email.setAttachment(currentAttachments);
    }

    protected void handleEdiTransmission(Email email, HistoryTopiaDao historyDao, Set<String> fieldSet) {
        History transmissionToEdi;

        // le transfert EDI est configuré par type de demande ET par société
        if (FaxToMailServiceUtils.contains(email.getDemandType().getRequiredFields(), MailField.RANGE_ROW)
                && email.getDemandType().isEdiTransfer()) {

            // si les gammes sont vides, la demande n'est pas valide, on ne fait rien dans ce cas
            // sinon on transfer à edi
            if (email.isRangeRowNotEmpty()) {

                // recherche parmis les dossiers parent, si la configuration ediTranfer à été demandée
                Boolean ediTranfer = null;
                MailFolder loopFolder = email.getMailFolder();
                do {
                    ediTranfer = loopFolder.getEdiTransfer();
                    loopFolder = loopFolder.getParent();
                } while (ediTranfer == null && loopFolder != null);

                if (BooleanUtils.isTrue(ediTranfer)) {
                    // ajout d'un historique
                    transmissionToEdi = historyDao.create(History.PROPERTY_TYPE, HistoryType.TRANSMISSION_TO_EDI,
                                                          History.PROPERTY_MODIFICATION_DATE, new Date(),
                                                          History.PROPERTY_EMAIL, email);
                    email.addHistory(transmissionToEdi);

                    // changement du status
                    email.setDemandStatus(DemandStatus.TRANSMISSION_TO_EDI);
                    fieldSet.add(Email.PROPERTY_DEMAND_STATUS);

                } else {
                    // TODO echatellier : pas sur qu'il faille l'ajouter tout le temps, mais sinon, le transfer se reproduira
//                    transmissionToEdi = historyDao.create(History.PROPERTY_TYPE, HistoryType.TRANSMISSION_TO_EDI,
//                            History.PROPERTY_MODIFICATION_DATE, new Date(),
//                            History.PROPERTY_EMAIL, email);
//                    email.addHistory(transmissionToEdi);

                    // passage en status
                    if (email.getDemandStatus() == DemandStatus.UNTREATED) {
                        email.setDemandStatus(DemandStatus.IN_PROGRESS);
                    }
                    fieldSet.add(Email.PROPERTY_DEMAND_STATUS);
                }
            }

        } else {

            // TODO echatellier : pas sur qu'il faille l'ajouter tout le temps, mais sinon, le transfer se reproduira
//            transmissionToEdi = historyDao.create(History.PROPERTY_TYPE, HistoryType.TRANSMISSION_TO_EDI,
//                    History.PROPERTY_MODIFICATION_DATE, new Date(),
//                    History.PROPERTY_EMAIL, email);
//            email.addHistory(transmissionToEdi);

            // passage en status

            if (email.getDemandStatus() == DemandStatus.UNTREATED) {
                email.setDemandStatus(DemandStatus.IN_PROGRESS);
            }
            fieldSet.add(Email.PROPERTY_DEMAND_STATUS);
        }
    }

    @Override
    public void transmitPendingDemandsToEdi() {
        EmailTopiaDao dao = getPersistenceContext().getEmailDao();

        List<Email> toTransmitToEdi = dao.forDemandStatusEquals(DemandStatus.TRANSMISSION_TO_EDI).findAll();
        if (log.isDebugEnabled()) {
            log.debug(toTransmitToEdi.size() + " demands to transmit to edi");
        }
        for (Email email : toTransmitToEdi) {
            transmitDemandToEdi(email);
        }
    }

    /**
     * Generate txt file to send demand to EDI system.
     * 
     * @param email email to send
     */
    protected void transmitDemandToEdi(Email email) {
        Preconditions.checkArgument(email.getDemandStatus() == DemandStatus.TRANSMISSION_TO_EDI);
        Preconditions.checkArgument(!email.getRangeRow().isEmpty());
       
        // recherche du dossier ou deposer les demandes EDI
        MailFolder folder = email.getMailFolder();
        while (!folder.isUseCurrentLevelEdiFolder() && folder.getParent() != null) {
            folder = folder.getParent();
        }
        String ediFolder = folder.getEdiFolder();
        if (StringUtils.isBlank(ediFolder)) {
            if (log.isFatalEnabled()) {
                log.fatal("Aucun dossier de depot des demandes EDI défini pour le dossier " + folder.getName());
            }
            return;
        }
        File ediFolderDirectory = new File(ediFolder);
        if (!ediFolderDirectory.canWrite()) {
            if (log.isFatalEnabled()) {
                log.fatal("Le dossier " + ediFolderDirectory.getAbsolutePath() + " ne dispose pas des droits d'écriture !");
            }
            return;
        }

        //TODO kmorin 20140521 maybe check if the file is not being read

        // get output file with pattern name depending on date
        Date now = serviceContext.getNow();
        String fileDate = DateFormatUtils.format(now, "yyMMddHHmmss");

        // create a file for each commande
        for (RangeRow rangeRow : email.getRangeRow()) {

            String fileName = "ORD_FAX_" + fileDate + "_" + rangeRow.getTopiaId() + ".txt";
            
            File ediFile = new File(ediFolderDirectory, fileName);
    
            // generate output content
            String separator = ";";
            try (Writer ediWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ediFile), StandardCharsets.UTF_8))) {
                
                ediWriter.write("%BEGIN_ENTETE_QUOTE\n");
                
                //N° Champ/Champ/Type/Longueur/Observation
                //01/Id Enregistrement/Alpha/1/Cst : E (En-tête)
                ediWriter.write("E" + separator);
                //02/N° d’ordre achat/Numérique/6/Cst : OARFAX
                ediWriter.write("OARFAX" + separator);
                //03/Date traitement/Date/6/JJMMAA dte système
                ediWriter.write(DateFormatUtils.format(now, "ddMMyy") + separator);
                //04/Date livraison prévue/Date/6/JJMMAA dte de réception
                ediWriter.write(DateFormatUtils.format(email.getReceptionDate(), "ddMMyy HHmmss") + separator);
                //05/Commentaire/Alpha/70/Vide
                ediWriter.write(separator);
                //06/Commentaire/Alpha/70/Vide
                ediWriter.write(separator);
                //07/Commentaire/Alpha/70/Vide
                ediWriter.write(separator);
                //08/Commentaire/Alpha/70/Vide
                ediWriter.write(separator);
                //09//Référence client FX/Alpha/36/Référence Chantier
                ediWriter.write(Strings.nullToEmpty(email.getProjectReference()) + separator);
                //10/Nom du contact/Alpha/35/Vide
                ediWriter.write(separator);
                //11/Téléphone contact/Alpha/15/Vide
                ediWriter.write(separator);
                //12/Fax contact/Alpha/15/Vide
                ediWriter.write(separator);
                //13/Devise/Alpha/3/Cst : EUR
                ediWriter.write("EUR" + separator);
                //14/Condition de livraison/Alpha/3/Vide
                ediWriter.write(separator);
                //15/Type de commande/Alpha/Type de document
                ediWriter.write(email.getDemandType().getLabel() + separator);
                //16/Adresse livr : Nom/Alpha/35/Vide
                ediWriter.write(separator);
                //18/Adresse livr : adr1/Alpha/30/Vide
                ediWriter.write(separator);
                //19/Adresse livr : adr2/Alpha/30/Vide
                ediWriter.write(separator);
                //20/Adresse livr : cp ville/Alpha/30/Vide
                ediWriter.write(separator);
                //21/Adresse livr : compl./Alpha/30/Vide
                ediWriter.write(separator);
                //22/Adresse livr : Code postal/Alpha/10/Vide
                ediWriter.write(separator);
                //23/Adresse livr : Code Pays/Alpha/3/Vide
                ediWriter.write(separator);
                //24/Code Ean acheteur/Alpha/13/Code client
                ediWriter.write(email.getClient().getCode() + separator);
                //25/Code Ean facturé/Alpha/13/Code client
                ediWriter.write(email.getClient().getCode() + separator);
                //26/Code Ean fournisseur/Alpha/13/Gamme
                ediWriter.write(rangeRow.getRange().getLabel() + separator);
                //27/Identifiant Unique
                ediWriter.write(rangeRow.getTopiaId() + "\n");

                ediWriter.write("%END_ENTETE_QUOTE\n");

            } catch (IOException ex) {
                //delete file if error, to avoid having unusable files
                FileUtils.deleteQuietly(ediFile);
                throw new RuntimeException("Can't generate EDI file");
            }

        }

        // changement du status
        EmailTopiaDao dao = getPersistenceContext().getEmailDao();
        HistoryTopiaDao historyDao = getPersistenceContext().getHistoryDao();
        email.setDemandStatus(DemandStatus.TRANSMITTED_TO_EDI);
        History history = historyDao.create(History.PROPERTY_TYPE, HistoryType.TRANSMITTED_TO_EDI,
                                            History.PROPERTY_MODIFICATION_DATE, new Date(),
                                            History.PROPERTY_EMAIL, email);
        email.addHistory(history);
        dao.update(email);
    }

    @Override
    public Set<Object> getDistinctValues(MailFolder folder, String[] properties, boolean sum) {
        Set<Object> result = null;
        if (folder.isFolderReadable()) {
            EmailTopiaDao dao = getPersistenceContext().getEmailDao();
            result = dao.getDistinctValues(folder, properties, sum);
        }
        return result;
    }

    /**
     * Recupère les demandes d'un dossier visible par un utilisateur.
     * 
     * La methode et les résultats sont paginés.
     * 
     * @param folder folder to get demande
     * @param currentUser user to check rigth
     * @param page pagination
     * @return paginated results
     */
    @Override
    public PaginationResult<Email> getEmailForFolder(final MailFolder folder,
                                                     FaxToMailUser currentUser,
                                                     EmailFilter filter,
                                                     final PaginationParameter page) {

        // perform request or not depending on rigths
        PaginationResult<Email> result;
        if (folder.isFolderReadable()) {
            EmailTopiaDao dao = getPersistenceContext().getEmailDao();
            result = dao.getEmailForFolder(filter, folder, page);

        } else {
            List<Email> elements = Collections.emptyList();
            result = PaginationResult.of(elements, 0, page);
        }
        return result;
    }

    @Override
    public List<MailFolder> getChildrenRecursively(MailFolder folder) {
        List<MailFolder> folders = new ArrayList<>();
        folders.add(folder);
        Collection<MailFolder> children = folder.getChildren();
        if (children != null) {
            for (MailFolder child : children) {
                folders.addAll(getChildrenRecursively(child));
            }
        }
        return folders;
    }

    @Override
    public QuantitiesByRange computeQuantitiesByRange(MailFolder rootFolder) {

        MailFolder folderWithComputeQuantitiesByState = rootFolder;
        while (folderWithComputeQuantitiesByState.getParent() != null
               && folderWithComputeQuantitiesByState.getComputeQuantitiesSubtotalsByState() == null) {
            folderWithComputeQuantitiesByState = folderWithComputeQuantitiesByState.getParent();
        }
        boolean computeQuantitiesSubtotalsByState =
                Boolean.TRUE.equals(folderWithComputeQuantitiesByState.getComputeQuantitiesSubtotalsByState());

        List<MailFolder> folders = getChildrenRecursively(rootFolder);
        EmailTopiaDao emailDao = getPersistenceContext().getEmailDao();
        return emailDao.computeQuantitiesByRange(folders, computeQuantitiesSubtotalsByState);
    }

    @Override
    public Email addToHistory(String emailId, HistoryType type, FaxToMailUser user, Date date, String... fields) {
        EmailTopiaDao emailDao = getPersistenceContext().getEmailDao();
        Email email = getEmailById(emailId,
                                   Email.PROPERTY_HISTORY + "." + History.PROPERTY_FAX_TO_MAIL_USER);

        HistoryTopiaDao historyDao = getPersistenceContext().getHistoryDao();
        History history = historyDao.create(History.PROPERTY_TYPE, type,
                                            History.PROPERTY_FAX_TO_MAIL_USER, user,
                                            History.PROPERTY_MODIFICATION_DATE, date,
                                            History.PROPERTY_EMAIL, email,
                                            History.PROPERTY_FIELDS_JSON, AbstractFaxToMailTopiaDao.GSON_INSTANCE.toJson(fields));

        email.addHistory(history);

        // pour eviter de charger tout l'historique dans la liste, on duplique l'information de
        // dernier ouvreur de PJ directement dans l'email
        if (type == HistoryType.ATTACHMENT_OPENING) {
            email.setLastAttachmentOpener(user);
        }
        // pour eviter de charger tout l'historique dans la liste, on duplique l'information de
        // dernier imprimeur directement dans l'email
        if (type == HistoryType.PRINTING) {
            email.setLastPrintingUser(user);
            email.setLastPrintingDate(date);
        }

        email = emailDao.update(email);
        getPersistenceContext().commit();

        return email;
    }

    @Override
    public Email openEmail(String emailId, FaxToMailUser user, boolean takeEmail) throws FolderNotReadableException {
        EmailTopiaDao emailDao = getPersistenceContext().getEmailDao();
        Email email = getFullEmailById(emailId, user);

        MailFolder mailFolder = email.getMailFolder();
        if (!mailFolder.isFolderReadable()) {
            throw new FolderNotReadableException(String.format("Mail folder %s not readable by %s",
                                                               mailFolder.getName(),
                                                               user.getLogin()),
                                                 mailFolder);
        }

        HistoryTopiaDao historyDao = getPersistenceContext().getHistoryDao();
        History history = historyDao.create(History.PROPERTY_TYPE, HistoryType.OPENING,
                                            History.PROPERTY_FAX_TO_MAIL_USER, user,
                                            History.PROPERTY_MODIFICATION_DATE, new Date(),
                                            History.PROPERTY_EMAIL, email);
        email.addHistory(history);
        if (takeEmail) {

            String jsonFields = AbstractFaxToMailTopiaDao.GSON_INSTANCE.toJson(Sets.newHashSet(Email.PROPERTY_TAKEN_BY));
            history = historyDao.create(History.PROPERTY_TYPE, HistoryType.MODIFICATION,
                                        History.PROPERTY_FAX_TO_MAIL_USER, user,
                                        History.PROPERTY_MODIFICATION_DATE, new Date(),
                                        History.PROPERTY_EMAIL, email,
                                        History.PROPERTY_FIELDS_JSON, jsonFields);
            email.addHistory(history);
            email.setTakenBy(user);
        }

        email = emailDao.update(email);
        getPersistenceContext().commit();
        return email;
    }

    /**
     * Vérrouille une demande par un utilisateur empechant les autres de l'ouvrir.
     * 
     * @param emailId topiaId de la demande à vérouiller
     * @param currentUser user
     * @return email
     * @throws AlreadyLockedMailException if email is already locked by another user
     */
    @Override
    public Email lockEmail(String emailId, FaxToMailUser currentUser) throws AlreadyLockedMailException,
                                                                                FolderNotReadableException {

        // get current lock on mail if any
        EmailTopiaDao emailDao = getPersistenceContext().getEmailDao();
        MailLockTopiaDao mailLockDao = getPersistenceContext().getMailLockDao();

        Email email = emailDao.forTopiaIdEquals(emailId).findUnique();

        MailFolderService mailFolderService = serviceContext.getMailFolderService();
        MailFolder folder = email.getMailFolder();
        browseFolderParent(folder, currentUser, mailFolderService);

        if (!folder.isFolderReadable()) {
            throw new FolderNotReadableException(String.format("Mail folder %s not readable by %s",
                                                               folder.getName(),
                                                               currentUser.getLogin()),
                                                 folder);
        }

        Hibernate.initialize(email.getTakenBy());

        MailLock mailLock = mailLockDao.forLockOnEquals(email).findUniqueOrNull();

        // if no lock found, create new one
        if (mailLock == null) {

            // déverrouillage automatique des mails qui ne font pas partie du même group que celui du mail
            // qui vient d'être locké
            // Ce comportement est supprimé par le #10747 : On peut locker plusieurs emails en même temps.
            /*List<MailLock> mailLocksToRemove;
            if (email.getEmailGroup() != null) {
                // si le mail courant a un group, on déverrouille tous les mails
                // qui ne portent pas sur le même groupe
                mailLocksToRemove = mailLockDao.forLockByEquals(currentUser)
                        .addNotEquals(MailLock.PROPERTY_LOCK_ON + "." + Email.PROPERTY_EMAIL_GROUP, email.getEmailGroup())
                        .findAll();
            } else {
                mailLocksToRemove = mailLockDao.forLockByEquals(currentUser).findAll();
            }
            if (log.isDebugEnabled()) {
                for (MailLock mailLockToRemove : mailLocksToRemove) {
                    log.debug("[UNLOCK] " + mailLockToRemove.getLockOn().getTopiaId() + " unlocked (automatic)");
                }
            }
            mailLockDao.deleteAll(mailLocksToRemove);*/

            // ajout d'un nouveau lock
            mailLock = new MailLockImpl();
            mailLock.setLockBy(currentUser);
            mailLock.setLockOn(email);
            mailLockDao.create(mailLock);

            if (log.isDebugEnabled()) {
                log.debug("[LOCK] " + emailId + " locked by " + currentUser.getLogin());
            }

            getPersistenceContext().commit();

        } else if (!mailLock.getLockBy().equals(currentUser)) {

            // throw exception if already locked by another user
            throw new AlreadyLockedMailException(String.format("Mail %s already locked by %s", emailId, mailLock.getLockBy().getTopiaId()),
                                                 mailLock.getLockBy(),
                                                 mailLock.getLockOn());
        }

        return email;
    }

    /**
     * Dévérrouille une demande.
     * 
     * @param emailId topiaId de la demande à devérouiller
     */
    @Override
    public void unlockEmail(String emailId) {
        MailLockTopiaDao mailLockDao = getPersistenceContext().getMailLockDao();
        MailLock mailLock = mailLockDao.forAll().addEquals(MailLock.PROPERTY_LOCK_ON + "." + TopiaEntity.PROPERTY_TOPIA_ID, emailId).findUniqueOrNull();
        // ca peut être null si c'est un emailId qui est valorisé suite à une creation et donc
        // qui n'a pas été vérrouillé avant
        if (mailLock != null) {
            mailLockDao.delete(mailLock);
            if (log.isDebugEnabled()) {
                log.debug("[UNLOCK] " + emailId + " unlocked");
            }
            getPersistenceContext().commit();
        }
    }

    /**
     * Calcule recursivement l'ensemble des répertoires lisible par un utilisateur récursivement.
     * 
     * @param readMailFolders result mail folders
     * @param mailFolders mail folder to allow
     */
    protected void computeUserReadableFolder(Set<MailFolder> readMailFolders, Iterable<MailFolder> mailFolders) {
        if (mailFolders != null) {
            for (MailFolder mailFolder : mailFolders) {
                readMailFolders.add(mailFolder);
                computeUserReadableFolder(readMailFolders, mailFolder.getChildren());
            }
        }
    }

    @Override
    public PaginationResult<Email> search(SearchFilter emailFilter, FaxToMailUser user, PaginationParameter pagination) {

        // compute rigths
        MailFolderTopiaDao mailFolderDao = getPersistenceContext().getMailFolderDao();
        Set<MailFolder> readMailFolders = new HashSet<MailFolder>();
        // read rights for user
        Iterable<MailFolder> mailFolders = mailFolderDao.forReadRightUsersContains(user).findAll();
        computeUserReadableFolder(readMailFolders, mailFolders);
        // read rigths for groups
        if (user.getUserGroups() != null) {
            for (FaxToMailUserGroup group : user.getUserGroups()) {
                mailFolders = mailFolderDao.forReadRightGroupsContains(group).findAll();
                computeUserReadableFolder(readMailFolders, mailFolders);
            }
        }

        // compute search
        EmailTopiaDao emailDao = getPersistenceContext().getEmailDao();
        PaginationResult<Email> result = emailDao.search(emailFilter, readMailFolders, pagination);
        return result;
    }

    @Override
    public Collection<Email> searchArchives(String commandQuotationNumber, String company) {
        Preconditions.checkArgument(StringUtils.isNotBlank(commandQuotationNumber));
        Preconditions.checkArgument(StringUtils.isNotBlank(company));

        EmailTopiaDao emailDao = getPersistenceContext().getEmailDao();
        return emailDao.findArchivedEmails(commandQuotationNumber, company);
    }

    @Override
    public Email groupEmails(String email1Id, String email2Id, FaxToMailUser user) {
        EmailGroupTopiaDao groupDao = getPersistenceContext().getEmailGroupDao();
        EmailTopiaDao emailDao = getPersistenceContext().getEmailDao();

        Email email1 = getFullEmailById(email1Id, user);
        Email email2 = getFullEmailById(email2Id, user);

        EmailGroup group1 = email1.getEmailGroup();
        EmailGroup group2 = email2.getEmailGroup();

        // if both groups are null
        if (group1 == null && group2 == null) {
            EmailGroup group = groupDao.create(EmailGroup.PROPERTY_EMAIL, Sets.newHashSet(email1, email2));
            email1.setEmailGroup(group);
            email2.setEmailGroup(group);

        // if only group 1 is null
        } else if (group1 == null) {
            email1.setEmailGroup(group2);
            group2.addEmail(email1);
            groupDao.update(group2);

        // if only group 2 is null
        } else if (group2 == null) {
            email2.setEmailGroup(group1);
            group1.addEmail(email2);
            groupDao.update(group1);

        // if the groups are equals, do nothing
        } else if (group1.equals(group2)) {
           return email1;

        // if both groups exist, merge them
        } else {
            Set<Email> group2Emails = new HashSet<>(group2.getEmail());

            // on désassocie les emails du groupe 2
            group2.clearEmail();

            // on les associe au groupe 1
            group1.addAllEmail(group2Emails);

            groupDao.delete(group2);
            groupDao.update(group1);

        }

        HistoryTopiaDao historyDao = getPersistenceContext().getHistoryDao();
        Date now = new Date();

        String jsonFields = AbstractFaxToMailTopiaDao.GSON_INSTANCE.toJson(Sets.newHashSet(email2.getObject()));
        History history = historyDao.create(History.PROPERTY_TYPE, HistoryType.GROUP,
                History.PROPERTY_FAX_TO_MAIL_USER, user,
                History.PROPERTY_MODIFICATION_DATE, now,
                History.PROPERTY_EMAIL, email1,
                History.PROPERTY_FIELDS_JSON, jsonFields);
        email1.addHistory(history);
        Email result = emailDao.update(email1);

        jsonFields = AbstractFaxToMailTopiaDao.GSON_INSTANCE.toJson(Sets.newHashSet(email1.getObject()));
        History history2 = historyDao.create(History.PROPERTY_TYPE, HistoryType.GROUP,
                History.PROPERTY_FAX_TO_MAIL_USER, user,
                History.PROPERTY_MODIFICATION_DATE, now,
                History.PROPERTY_EMAIL, email2,
                History.PROPERTY_FIELDS_JSON, jsonFields);
        email2.addHistory(history2);
        emailDao.update(email2);

        getPersistenceContext().commit();

        return result;
    }

    /**
     * Envoi une réponse et retourne l'email.
     * 
     * @param from from
     * @param to to
     * @param cc cc
     * @param bcc bcc
     * @param subject subject
     * @param content content
     * @param attachments attachement
     * @param originalEmailId mail topia id
     * @param user user to add new history entry for user
     * @return modified email
     * @throws EmailException if message can't be sent
     * @throws MessagingException if message can't be sent
     * @throws IOException if message can't be sent
     */
    @Override
    public Email reply(String from, String to, String cc, String bcc, String subject,
                       String content, Collection<AttachmentFile> attachments,
                       String originalEmailId, FaxToMailUser user) throws EmailException, MessagingException, IOException {

        Date begin = new Date();

        Email email = getEmailById(originalEmailId,
                                   Email.PROPERTY_HISTORY + "." + History.PROPERTY_FAX_TO_MAIL_USER,
                                   Email.PROPERTY_REPLIES);

        final String smtpUser = getApplicationConfig().getSmtpUser();
        final String password = getApplicationConfig().getSmtpPassword();
        final boolean useSsl = getApplicationConfig().isSmtpUseSsl();

        HtmlEmail message = new HtmlEmail();
        message.setHostName(getApplicationConfig().getSmtpHost());
        message.setSmtpPort(getApplicationConfig().getSmtpPort());
        if (StringUtils.isNotBlank(smtpUser) && password != null) {
            message.setAuthenticator(new DefaultAuthenticator(smtpUser, password));
        }
        message.setSSLOnConnect(useSsl);

        message.setCharset(EmailConstants.UTF_8);
        message.setFrom(from);

        String[] tos = to.split(SEMICOLON_SPLIT_PATTERN);
        message.addTo(tos);
        if (StringUtils.isNotBlank(cc)) {
            String[] ccs = cc.split(SEMICOLON_SPLIT_PATTERN);
            message.addCc(ccs);
        }
        if (StringUtils.isNotBlank(bcc)) {
            String[] bccs = bcc.split(SEMICOLON_SPLIT_PATTERN);
            message.addBcc(bccs);
        }
        message.setSubject(subject);

        StringBuilder toSendContent = new StringBuilder();
        if (StringUtils.isNotEmpty(content)) {
            toSendContent.append(content);
        }

        message.setHtmlMsg(toSendContent.toString());
        message.setTextMsg(toSendContent.toString());

        for (AttachmentFile attachmentFile : attachments) {
            // Create the attachment
            File file = attachmentFile.getFile();
            DataSource source = new FileDataSource(file);
            message.attach(source, attachmentFile.getFilename(), null);
        }

        Date beforeSendingMessage = new Date();

        try {
            message.send();
        } catch (EmailException e) {
            throw new RuntimeException("Impossible d'envoyer l'email, vérifiez la taille des pièces jointes ou la connexion au serveur. Si le problème persiste, contactez la hotline", e);
        }

        Date afterSendingMessage = new Date();

        ReplyTopiaDao replyTopiaDao = getPersistenceContext().getReplyDao();
        ReplyContentTopiaDao replyContentTopiaDao = getPersistenceContext().getReplyContentDao();
        Date now = new Date();

        // build mail header part
        StringBuilder emailHeaders = new StringBuilder();
        Enumeration<String> headerLines = message.getMimeMessage().getAllHeaderLines();
        while (headerLines.hasMoreElements()) {
            String headerLine = headerLines.nextElement();
            emailHeaders.append(headerLine).append("\n");
        }
        emailHeaders.append("\n"); // blank line (IMPORTANT)
        
        // build mail serialization part
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        IOUtils.write(emailHeaders, byteOut);
        IOUtils.copy(message.getMimeMessage().getInputStream(), byteOut);

        ReplyContent replyContent = replyContentTopiaDao.createByNotNull(byteOut.toByteArray());

        Reply reply = replyTopiaDao.create(Reply.PROPERTY_REPLY_CONTENT, replyContent,
                                           Reply.PROPERTY_SENT_DATE, now,
                                           Reply.PROPERTY_SUBJECT, subject,
                                           Reply.PROPERTY_SENDER, from,
                                           Reply.PROPERTY_RECIPIENT, to,
                                           Reply.PROPERTY_SENT_BY, user);

        email.addReplies(reply);

        HistoryTopiaDao historyDao = getPersistenceContext().getHistoryDao();
        History history = historyDao.create(History.PROPERTY_TYPE, HistoryType.REPLY,
                                            History.PROPERTY_FAX_TO_MAIL_USER, user,
                                            History.PROPERTY_MODIFICATION_DATE, now,
                                            History.PROPERTY_EMAIL, email);
        email.addHistory(history);

        email = saveEmail(email, null, null, user);

        Date postSendingOperations = new Date();

        if (log.isDebugEnabled()) {
            log.debug("Message preparation took : " + (beforeSendingMessage.getTime() - begin.getTime()) + " ms");
            log.debug("Message sending (outside FTM) took : " + (afterSendingMessage.getTime() - beforeSendingMessage.getTime()) + " ms");
            log.debug("Post sending operations took : " + (postSendingOperations.getTime() - afterSendingMessage.getTime()) + " ms");
        }


        return email;
    }

    @Override
    public void transmit(Collection<String> emailIds, MailFolder newFolder, FaxToMailUser currentUser) {
        EmailTopiaDao dao = getPersistenceContext().getEmailDao();
        HistoryTopiaDao historyDao = getPersistenceContext().getHistoryDao();

        List<Email> emails = dao.forTopiaIdIn(emailIds).findAll();
        for (Email email : emails) {
            if (email.getDemandStatus() != DemandStatus.TRANSMISSION_TO_EDI
                    && email.getDemandStatus() != DemandStatus.TRANSMITTED_TO_EDI) {
                email.setDemandStatus(DemandStatus.QUALIFIED);
            }
            email.setMailFolder(newFolder);
            email.setTakenBy(null);
            email.setLastAttachmentOpener(null);
            email.setLastPrintingUser(null);
            email.setLastPrintingDate(null);

            unlockEmail(email.getTopiaId());

            History history = historyDao.create(History.PROPERTY_TYPE, HistoryType.TRANSMISSION,
                                                History.PROPERTY_FAX_TO_MAIL_USER, currentUser,
                                                History.PROPERTY_MODIFICATION_DATE, new Date(),
                                                History.PROPERTY_EMAIL, email);

            email.addHistory(history);
        }
        dao.updateAll(emails);
        getPersistenceContext().commit();
    }

    /**
     * Save content stream into attachment file content.
     * 
     * @param contentStream content
     * @return attachmentFile filled by content
     */
    @Override
    public AttachmentFile getAttachmentFileFromStream(InputStream contentStream) {
        AttachmentFile attachmentFile = new AttachmentFileImpl();
        try {
            //Session hibernateSession = getPersistenceContext().getHibernateSupport().getHibernateSession();
            //Blob contentBlob = Hibernate.getLobCreator(hibernateSession).createBlob(contentStream, contentStream.available());
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            IOUtils.copy(contentStream, output);
            attachmentFile.setContent(output.toByteArray());
        } catch (Exception ex) {
            throw new RuntimeException("Can't save content", ex);
        }
        return attachmentFile;
    }

    @Override
    public AttachmentFile getAttachmentFile(String attachmentId, boolean original) {
        AttachmentTopiaDao dao = getPersistenceContext().getAttachmentDao();
        Attachment attachment = dao.forTopiaIdEquals(attachmentId).findUnique();
        AttachmentFile result = original ? attachment.getOriginalFile() : attachment.getEditedFile();
        if (result != null) {
            // force lazy initialize
            Hibernate.initialize(result);
        }
        return result;
    }

    @Override
    public Collection<GeneratedPDFPage> getGeneratedPDFPage(String attachmentId) {
        AttachmentTopiaDao dao = getPersistenceContext().getAttachmentDao();
        Attachment attachment = dao.forTopiaIdEquals(attachmentId).findUnique();
        Collection<GeneratedPDFPage> result = attachment.getGeneratedPDFPages();
        if (result != null) {
            // force lazy initialize
            Hibernate.initialize(result);
            result = GENERATED_PDF_PAGE_NATURAL_ORDERING.sortedCopy(result);
        }

        return result;
    }

    @Override
    public ReplyContent getReplyContent(String replyId) {
        ReplyTopiaDao dao = getPersistenceContext().getReplyDao();
        Reply reply = dao.forTopiaIdEquals(replyId).findUnique();
        ReplyContent result = reply.getReplyContent();
        // force lazy initialize
        Hibernate.initialize(result);
        return result;
    }

    /**
     * Retourne la liste des vérrouillages actifs.
     * 
     * @return email list
     */
    @Override
    public List<MailLock> getAllMailLocks() {
        MailLockTopiaDao mailLockDao = getPersistenceContext().getMailLockDao();
        List<MailLock> result = mailLockDao.findAll();
        return result;
    }

    /**
     * Dévérrouille les mails specifiés.
     * 
     * @param mailLockIds mail lock ids to unlock
     */
    @Override
    public void unlockMails(List<String> mailLockIds) {
        MailLockTopiaDao mailLockDao = getPersistenceContext().getMailLockDao();
        Collection<MailLock> mailLocks = mailLockDao.forTopiaIdIn(mailLockIds).findAll();
        mailLockDao.deleteAll(mailLocks);

        getPersistenceContext().commit();
    }

    @Override
    public void sendHtmlEmail(String from, String to, String subject, String content, String signing)
            throws EmailException {

        final String smtpUser = getApplicationConfig().getSmtpUser();
        final String password = getApplicationConfig().getSmtpPassword();
        final boolean useSsl = getApplicationConfig().isSmtpUseSsl();

        HtmlEmail message = new HtmlEmail();
        message.setHostName(getApplicationConfig().getSmtpHost());
        message.setSmtpPort(getApplicationConfig().getSmtpPort());
        if (StringUtils.isNotBlank(smtpUser) && password != null) {
            message.setAuthenticator(new DefaultAuthenticator(smtpUser, password));
        }
        message.setSSLOnConnect(useSsl);

        message.setCharset(EmailConstants.UTF_8);
        message.setFrom(from);

        String[] tos = to.split(SEMICOLON_SPLIT_PATTERN);
        message.addTo(tos);
        message.setSubject(subject);

        StringBuilder toSendContent = new StringBuilder("<html><body>");
        if (StringUtils.isNotEmpty(content)) {
            toSendContent.append(content);
        }
        if (StringUtils.isNotEmpty(signing)) {
            toSendContent.append("<p><img src='").append(signing).append("'/></p>");
        }
        toSendContent.append("</body></html>");

        message.setHtmlMsg(toSendContent.toString());
        if (StringUtils.isNotEmpty(content)) {
            message.setTextMsg(content);
        } else {
            message.setTextMsg(toSendContent.toString());
        }

        message.send();
    }

    @Override
    public void updateRangeRowsWithEdiReturns() {
        final RangeRowTopiaDao rangeRowTopiaDao = getPersistenceContext().getRangeRowDao();
        final EmailTopiaDao emailTopiaDao = getPersistenceContext().getEmailDao();
        final HistoryTopiaDao historyDao = getPersistenceContext().getHistoryDao();

        final Set<Email> emailsToUpdate = new HashSet<>();

        TopiaSqlSupport sqlSupport = getPersistenceContext().getSqlSupport();
        sqlSupport.doSqlWork(new TopiaSqlWork() {

            @Override
            public void execute(Connection connection) throws SQLException {

                int importedCount = 0;
                List<String> rangeRowTopiaIdsInEdiReturnToDelete = new ArrayList<String>();

                // attention, le sql à pour cible postgresql, mssql, h2, il doit rester simple
                String query = String.format("SELECT %s, %s, %s FROM %s",
                        EdiReturn.PROPERTY_RANGE_ROW_TOPIA_ID,
                        EdiReturn.PROPERTY_COMMAND_NUMBER,
                        EdiReturn.PROPERTY_ERROR,
                        EdiReturn.class.getSimpleName());

                try(Statement stat = connection.createStatement();
                    ResultSet resultSet = stat.executeQuery(query)) {

                    while (resultSet.next()) {

                        String rangeRowTopiaId = resultSet.getString(EdiReturn.PROPERTY_RANGE_ROW_TOPIA_ID);
                        String commandNumber = resultSet.getString(EdiReturn.PROPERTY_COMMAND_NUMBER);
                        String error = resultSet.getString(EdiReturn.PROPERTY_ERROR);

                        // manage email and range row
                        RangeRow rangeRow = rangeRowTopiaDao.forTopiaIdEquals(rangeRowTopiaId).findUniqueOrNull();
                        if (rangeRow == null) {
                            if (log.isErrorEnabled()) {
                                log.error("Can't find rangeRow " + rangeRowTopiaId + " to update");
                            }
                            continue;
                        }
                        Email email = emailTopiaDao.forRangeRowContains(rangeRow).findUniqueOrNull();
                        if (email == null) {
                            if (log.isErrorEnabled()) {
                                log.error("Can't find email for " + rangeRowTopiaId + " to update");
                            }
                            continue;
                        }

                        rangeRowTopiaIdsInEdiReturnToDelete.add(rangeRowTopiaId);

                        Set<String> modifiedFields = new HashSet<>();

                        if (StringUtils.isNotBlank(error)) {
                            //error
                            String oldError = email.getEdiError();
                            if (StringUtils.isNotBlank(oldError)) {
                                error = oldError + ", " + error;
                            }
                            email.setEdiError(error);
                            modifiedFields.add(Email.PROPERTY_EDI_ERROR);

                        } else {
                            rangeRow.setCommandNumber(commandNumber);
                            rangeRowTopiaDao.update(rangeRow);
                            modifiedFields.add(Email.PROPERTY_RANGE_ROW);
                        }

                        History transmission = CollectionUtils.find(email.getHistory(), new Predicate<History>() {
                            @Override
                            public boolean evaluate(History object) {
                                return object.getType() == HistoryType.TRANSMISSION;
                            }
                        });

                        if (transmission != null) {
                            email.setDemandStatus(DemandStatus.QUALIFIED);

                        } else {
                            email.setDemandStatus(DemandStatus.IN_PROGRESS);
                        }
                        modifiedFields.add(Email.PROPERTY_DEMAND_STATUS);

                        History history = historyDao.create(History.PROPERTY_TYPE, HistoryType.MODIFICATION,
                                History.PROPERTY_MODIFICATION_DATE, new Date(),
                                History.PROPERTY_EMAIL, email,
                                History.PROPERTY_FIELDS_JSON, AbstractFaxToMailTopiaDao.GSON_INSTANCE.toJson(modifiedFields));

                        email.addHistory(history);

                        history = historyDao.create(History.PROPERTY_TYPE, HistoryType.EDI_RETURN,
                                History.PROPERTY_MODIFICATION_DATE, new Date(),
                                History.PROPERTY_EMAIL, email);

                        email.addHistory(history);

                        emailsToUpdate.add(email);

                        importedCount++;
                    }
                }


                // delete all rows
                try (Statement stat = connection.createStatement()){
                    stat.execute("DELETE FROM " + EdiReturn.class.getSimpleName() +
                            " WHERE " + EdiReturn.PROPERTY_RANGE_ROW_TOPIA_ID +
                            " IN ('" + StringUtils.join(rangeRowTopiaIdsInEdiReturnToDelete, "','") + "')");

                }

                // useful log info (do not remove)
                if (importedCount > 0 && log.isInfoEnabled()) {
                    log.info(String.format("Imported %d ediReturn rows", importedCount));
                }
            }
        });

        emailTopiaDao.updateAll(emailsToUpdate);
        getPersistenceContext().commit();
    }

	public OriginalEmail originalEmailFromMessage(MimeMessage message, Charset charset) throws MessagingException, IOException {
		ByteArrayOutputStream baos;
        int messageSize = message.getSize();
        if (messageSize > 0) {
            baos = new ByteArrayOutputStream(messageSize);
        } else {
            baos = new ByteArrayOutputStream();
        }
		message.writeTo(baos);
		OriginalEmail originalEmail = getPersistenceContext().getOriginalEmailDao().newInstance();
		originalEmail.setContent(baos.toString());
		return originalEmail;
	}

    /**
     * Generate email details as PDF and return it as an printable attachment.
     * 
     * @param email email
     * @return attachment filled with pdf content
     */
    @Override
    public AttachmentFile getEmailDetailAsAttachment(Email email) {

        AttachmentFile result = null;

        try {

            // get html content
            String content = getEmailDetailAsHtml(email);

            // generate pdf
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(content);
            renderer.layout();
            renderer.createPDF(out);

            // create attachent
            result = new AttachmentFileImpl();
            result.setContent(out.toByteArray());
            result.setFilename(t("faxtomail.attachment.demand.filename") + ".pdf");
            result.getFile();

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("", e);
            }
        }
        return result;
    }
    
    protected String getEmailDetailAsHtml(Email email) {
        Decorator<FaxToMailUser> userDecorator = getDecoratorService().getDecoratorByType(FaxToMailUser.class);

        Map<String, Object> scopes = new HashMap<String, Object>();

        scopes.put("title", email.getTitle());
        scopes.put("receivedDate", DateFormatUtils.format(email.getReceptionDate(), DD_MM_YYYY_HH_MM));
        scopes.put("sender", email.getSender());
        scopes.put("object", email.getObject());
        scopes.put("client", email.getClient());
        scopes.put("demandType", email.getDemandType());
        scopes.put("priority", email.getPriority());
        scopes.put("projectReference", email.getProjectReference());
        scopes.put("companyReference", email.getCompanyReference());
        scopes.put("waitingState", email.getWaitingState());
        scopes.put("status", email.getDemandStatus());
        scopes.put("takenBy", email.getTakenBy() == null ? "" : userDecorator.toString(email.getTakenBy()));
        scopes.put("message", email.getComment());
        scopes.put("date", DateFormatUtils.format(serviceContext.getNow(), DD_MM_YYYY_HH_MM));

        scopes.put("firstOpeningUser", email.getFirstOpeningUser() == null ?
                "" : userDecorator.toString(email.getFirstOpeningUser()));
        scopes.put("firstOpeningDate", email.getFirstOpeningDate() == null ?
                "" : DateFormatUtils.format(email.getFirstOpeningDate(), DD_MM_YYYY_HH_MM));

        scopes.put("lastModificationUser", email.getLastModificationUser() == null ?
                "" : userDecorator.toString(email.getLastModificationUser()));
        scopes.put("lastModificationDate", email.getLastModificationDate() == null ?
                "" : DateFormatUtils.format(email.getLastModificationDate(), DD_MM_YYYY_HH_MM));

        scopes.put("lastAttachmentOpeningInFolderUser", email.getLastAttachmentOpeningInFolderUser() == null ?
                "" : userDecorator.toString(email.getLastAttachmentOpeningInFolderUser()));
        scopes.put("lastAttachmentOpeningInFolderDate", email.getLastAttachmentOpeningInFolderDate() == null ?
                "" : DateFormatUtils.format(email.getLastAttachmentOpeningInFolderDate(), DD_MM_YYYY_HH_MM));

        scopes.put("hasRangeRows", email.sizeRangeRow() > 0);
        scopes.put("rangeRows", email.getRangeRow());

        // find template
        InputStream is = EmailServiceImpl.class.getResourceAsStream("/pdf/demande.mustache");
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache mustache = mf.compile(new InputStreamReader(is, StandardCharsets.UTF_8), "demande");
        StringWriter writer = new StringWriter();
        mustache.execute(writer, scopes);
        writer.flush();
        
        return writer.toString();
    }

    @Override
    public ArchiveImportResult importArchive(InputStream inputStream, File attachmentBase) {

        // all daos involved
        FaxToMailTopiaPersistenceContext persistenceContext = getPersistenceContext();
        TopiaHibernateSupport hibernateSupport = persistenceContext.getHibernateSupport();
//        Session hibernateSession = hibernateSupport.getHibernateSession();
        StatelessSession statelessSession = hibernateSupport.getHibernateFactory().openStatelessSession();
        Transaction transaction = statelessSession.beginTransaction();

        EmailTopiaDao emailDao = persistenceContext.getEmailDao();
        DemandTypeTopiaDao demandTypedao = persistenceContext.getDemandTypeDao();
        PriorityTopiaDao priorityDao = persistenceContext.getPriorityDao();
        WaitingStateTopiaDao waitingStateDao = persistenceContext.getWaitingStateDao();
        MailFolderTopiaDao mailFolderDao = persistenceContext.getMailFolderDao();
        AttachmentTopiaDao attachmentDao = persistenceContext.getAttachmentDao();
        AttachmentFileTopiaDao attachmentFileDao = persistenceContext.getAttachmentFileDao();
        HistoryTopiaDao historyDao = persistenceContext.getHistoryDao();
        OriginalEmailTopiaDao originalEmailTopiaDao = getPersistenceContext().getOriginalEmailDao();

        int commitThreshold = getApplicationConfig().getArchiveImportCommitTreshold();
        if (log.isInfoEnabled()) {
            log.info("commit every " + commitThreshold + " archives imported");
        }

        // get referentiel map
        Map<String, DemandType> allDemandTypes = Maps.uniqueIndex(demandTypedao, new Function<DemandType, String>() {
            @Override
            public String apply(DemandType input) {
                return input.getLabel();
            }
        });
        Map<String, Priority> allPriority = Maps.uniqueIndex(priorityDao, new Function<Priority, String>() {
            @Override
            public String apply(Priority input) {
                return input.getLabel();
            }
        });
        Map<String, WaitingState> allWaitingStates = Maps.uniqueIndex(waitingStateDao, new Function<WaitingState, String>() {
            @Override
            public String apply(WaitingState input) {
                return input.getLabel();
            }
        });
        
        // build folder map
        Map<String, MailFolder> mailFolderMap = Maps.uniqueIndex(mailFolderDao, new Function<MailFolder, String>() {
            @Override
            public String apply(MailFolder input) {
                return FaxToMailServiceUtils.getFullMailFolderPath(input);
            }
        });

        // run import
        ArchiveImportExportModel archiveImportExportModel = new ArchiveImportExportModel(';', allWaitingStates, allDemandTypes, allPriority);
        Binder<ArchiveImportExportBean, Email> emailBinder = BinderFactory.newBinder(ArchiveImportExportBean.class, Email.class);
        Import<ArchiveImportExportBean> importer = null;

        // cache of archive folders
        Map<MailFolder, MailFolder> archiveFoldersByFolder = new HashMap<>();
        Map<MailFolder, String> companyByFolder = new HashMap<>();
        ArchiveImportResult result = new ArchiveImportResult();

        Set<ArchiveImportExportBean> notImportedBeans = new HashSet<>();
        Set<ArchiveImportExportBean> toCommitBeans = new HashSet<>();

        try {
            importer = Import.newImport(archiveImportExportModel, new InputStreamReader(inputStream, getApplicationConfig().getImportFileEncoding()));
            Iterator<ArchiveImportExportBean> iterator = importer.iterator();
            int index = 0;

            while (iterator.hasNext()) {
                try {
                    ArchiveImportExportBean archiveBean = iterator.next();
                    toCommitBeans.add(archiveBean);
                    index++;

                    if (archiveBean.getProjectReference() == null) {
                        log.error("null project ref for " + archiveBean.getReceptionDate());
                    }

                    String mailFolderPath = archiveBean.getMailFolderPath();
                    MailFolder folder = mailFolderMap.get(mailFolderPath);
                    archiveBean.setMailFolder(folder);

                    if (archiveBean.getMailFolder() == null) {
                        log.error("null mail folder for " + archiveBean.getReceptionDate());
                        String errorMessage = t("faxtomail.archives.import.error.invalidFolder", mailFolderPath);
                        throw new InvalidArchiveImportBeanException(errorMessage, archiveBean);
                    }
                    // create new email to persist
                    Email email = emailDao.newInstance();
                    emailBinder.copy(archiveBean, email);

                    email.setDemandStatus(DemandStatus.ARCHIVED);

                    MailFolder archiveChild = null;
                    String company = null;
                    boolean companyFound = false;

                    if (!archiveFoldersByFolder.containsKey(folder)) {
                        do {
                            if (archiveChild == null && folder.getChildren() != null) {
                                Optional<MailFolder> optArchiveChild = Iterables.tryFind(folder.getChildren(), new com.google.common.base.Predicate<MailFolder>() {
                                    @Override
                                    public boolean apply(MailFolder mailFolder) {
                                        return mailFolder.isArchiveFolder();
                                    }
                                });
                                if (optArchiveChild.isPresent()) {
                                    archiveChild = optArchiveChild.get();
                                }
                            }
                            if (!companyFound && folder.isUseCurrentLevelCompany()) {
                                companyFound = true;
                                company = folder.getCompany();
                            }
                            folder = folder.getParent();

                        } while ((archiveChild == null || !companyFound) && folder != null);

                        archiveFoldersByFolder.put(folder, archiveChild);
                        companyByFolder.put(folder, company);

                    } else {
                        archiveChild = archiveFoldersByFolder.get(folder);
                        company = companyByFolder.get(folder);
                    }

                    // le dossier d'archive peut ne pas exister. Dans ce cas on laisse le mail dans le dossier
                    // d'origine
                    if (archiveChild != null) {
                        email.setMailFolder(archiveChild);
                    }

                    Client client = getClientService().getClientForCode(archiveBean.getClientCode(), company);
                    email.setClient(client);

                    OriginalEmail originalEmail = originalEmailTopiaDao.newInstance();
                    String originalEmailContent = archiveBean.getOriginalEmailContent();
                    originalEmail.setContent(originalEmailContent != null ? originalEmailContent : "");
                    originalEmail.setTopiaId(getTopiaId(OriginalEmail.class, originalEmail));
                    statelessSession.insert(originalEmail);

                    email.setOriginalEmail(originalEmail);

                    // persist it
                    email.setTopiaId(getTopiaId(Email.class, email));
                    statelessSession.insert(email);

                    History history = historyDao.newInstance();
                    history.setType(HistoryType.ARCHIVED);
                    history.setModificationDate(new Date());
                    history.setEmail(email);
                    history.setTopiaId(getTopiaId(History.class, history));
                    statelessSession.insert(history);

//                    email.addHistory(history);

                    if (StringUtils.isNotBlank(originalEmailContent)) {
                        try {
                            Attachment contentAttachment = convertTextToPdf(originalEmailContent,
                                                                            t("faxtomail.email.content.attachment.plainFileName"));

                            AttachmentFile contentAttachmentFile = contentAttachment.getOriginalFile();
                            contentAttachmentFile.setTopiaId(getTopiaId(AttachmentFile.class, contentAttachmentFile));
                            statelessSession.insert(contentAttachmentFile);

                            contentAttachment.setEmail(email);
                            contentAttachment.setTopiaId(getTopiaId(Attachment.class, contentAttachment));
                            statelessSession.insert(contentAttachment);

                        } catch (IOException e) {
                            if (log.isErrorEnabled()) {
                                log.error("error while creating email content attachment", e);
                            }
                        }
                    }


                    // manage attachments
                    if (archiveBean.getAttachmentPaths() != null) {
                        Iterable<String> itAttachmentPaths = Splitter.on(',').omitEmptyStrings().trimResults().split(archiveBean.getAttachmentPaths());
                        for (String attachmentPath : itAttachmentPaths) {
                            File attFile;
                            if (attachmentBase != null) {
                                attFile = new File(attachmentBase, attachmentPath);
                            } else {
                                attFile = new File(attachmentPath);
                            }
                            if (!attFile.isFile()) {
                                String errorMessage = t("faxtomail.archives.import.error.invalidAttachmentPath", attachmentPath);
                                throw new InvalidArchiveImportBeanException(errorMessage, archiveBean);
                            }

                            Attachment attachment = attachmentDao.newInstance();
                            attachment.setEmail(email);
                            attachment.setTopiaId(getTopiaId(Attachment.class, attachment));

                            AttachmentFile attachmentFile = attachmentFileDao.newInstance();
                            attachmentFile.setFilename(attFile.getName());

                            try {
                                attachmentFile.setContent(FileUtils.readFileToByteArray(attFile));
                            } catch (IOException e) {
                                String errorMessage = t("faxtomail.archives.import.error.readingAttachmentFile", attachmentPath);
                                throw new InvalidArchiveImportBeanException(errorMessage, archiveBean);
                            }
                            attachmentFile.setTopiaId(getTopiaId(AttachmentFile.class, attachmentFile));
                            statelessSession.insert(attachmentFile);

                            attachment.setOriginalFile(attachmentFile);
                            try {
                                convertIfNecessary(attachment);

                            } catch (IOException e) {
                                if (log.isErrorEnabled()) {
                                    log.error("error while converting attachment", e);
                                }
                            }

                            statelessSession.insert(attachment);

//                            email.addAttachment(attachment);
                        }

                        statelessSession.update(email);
                        if (log.isDebugEnabled()) {
                            log.debug("add archive " + index);
                        }

                    }

                    if (index % commitThreshold == 0 || !iterator.hasNext()) {
                        transaction.commit();
                        transaction = statelessSession.beginTransaction();

                        result.addNbImportedArchives(toCommitBeans.size());
                        toCommitBeans.clear();
                    }

                } catch (InvalidArchiveImportBeanException e) {
                    log.error(e.getMessage(), e);
                    notImportedBeans.add(e.getBean());

                } catch (HibernateException | TopiaQueryException e) {
                    String message;
                    if (e.getCause() != null) {
                        message = e.getCause().getMessage();
                    } else {
                        message = e.getMessage();
                    }
                    log.error(message, e);

                    String errorMessage = t("faxtomail.archives.import.error.persistence");
                    for (ArchiveImportExportBean bean : toCommitBeans) {
                        bean.setError(errorMessage);
                        notImportedBeans.add(bean);
                    }
                    toCommitBeans.clear();

                    while (iterator.hasNext()) {
                        ArchiveImportExportBean next = iterator.next();
                        next.setError(errorMessage);
                        notImportedBeans.add(next);
                    }
                }
            }

        } catch (UnsupportedEncodingException e) {
            String message;
            if (e.getCause() != null) {
                message = e.getCause().getMessage();
            } else {
                message = e.getMessage();
            }
            throw new ApplicationTechnicalException(message, e);

        } finally {
            IOUtils.closeQuietly(importer);
            IOUtils.closeQuietly(inputStream);
            transaction.rollback();
            statelessSession.close();
        }

        if (!notImportedBeans.isEmpty()) {
            try {
                result.addNbImportErrors(notImportedBeans.size());
                Export<ArchiveImportExportBean> exporter = Export.newExport(archiveImportExportModel, notImportedBeans);

                String fileName = DateFormatUtils.format(new Date(), "yyyyMMddhhmmss") + "-" + UUID.randomUUID() + ".csv";
                File errorFile = new File(getApplicationConfig().getDataDirectory(), fileName);
                exporter.write(errorFile);
                result.setErrorFile(errorFile);

            } catch (Exception e) {
                log.error(e);
            }
        }

        return result;
    }

    protected String getTopiaId(Class clazz, TopiaEntity entity) {
        return getPersistenceContext().getTopiaIdFactory().newTopiaId(clazz, entity);
    }

    @Override
    public long getArchivedMailCount() {
        EmailTopiaDao emailDao = getPersistenceContext().getEmailDao();
        long result = emailDao.forDemandStatusEquals(DemandStatus.ARCHIVED).count();
        return result;
    }


    @Override
    public List<Email> getArchivedMailNotProcessed(int limit) {
        EmailTopiaDao emailDao = getPersistenceContext().getEmailDao();
        return emailDao.forDemandStatusEquals(DemandStatus.ARCHIVED).addEquals(Email.PROPERTY_IMAGE_CLEANED, false).find(0, limit);
    }

    @Override
    public List<String> decomposeMultipartEmail(List<Attachment> attachments, Part part) throws Exception {
        return decomposeMultipartEmail(attachments, part, 0);
    }

    protected List<String> decomposeMultipartEmail(List<Attachment> attachments, Part part, int decomposingForwardedEmail) throws Exception {
        List<String> result = null;

        if (part.getContent() instanceof MimeMessage) {

            List<Attachment> forwardeEmailAttachments = extractAttachmentsFromMessage((MimeMessage) part.getContent(),
                                                                                      decomposingForwardedEmail + 1);
            attachments.addAll(forwardeEmailAttachments);

        } else {

            DataSource dataSource = part.getDataHandler().getDataSource();
            MimeMultipart mimeMultipart = new MimeMultipart(dataSource);
            int multiPartCount = mimeMultipart.getCount();

            for (int j = 0; j < multiPartCount; j++) {
                MimeBodyPart bp = (MimeBodyPart) mimeMultipart.getBodyPart(j);

                // if it is a text part, then this is the email content
                String disposition = bp.getDisposition();
                if (bp.isMimeType("text/*") && !Part.ATTACHMENT.equals(disposition)) {

                    String content = FaxToMailServiceUtils.getTextFromPart(bp);

                    if (bp.isMimeType("text/plain")) {
                        if (StringUtils.isNotBlank(content)) {
                            String name;
                            if (decomposingForwardedEmail > 0) {
                                name = t("faxtomail.email.content.attachment.forwardedFileName", decomposingForwardedEmail);
                            } else {
                                name = t("faxtomail.email.content.attachment.plainFileName");
                            }
                            Attachment attachment = convertTextToPdf(content, name);
                            attachment.setMailContent(decomposingForwardedEmail == 0);
                            attachments.add(attachment);
                        }

                    } else {
                        if (result == null) {
                            result = new ArrayList<>();
                        }
                        result.add(content);
                    }

                    // if it is multipart part, decompose it
                } else if (bp.isMimeType("multipart/*")) {
                    List<String> htmlContent = decomposeMultipartEmail(attachments, bp, decomposingForwardedEmail);
                    if (htmlContent != null) {
                        result = htmlContent;
                    }

                } else if (bp.isMimeType("message/*")) {
                    decomposingForwardedEmail++;
                    Attachment attachment = null;
                    String fileName = t("faxtomail.email.content.attachment.forwardedFileName", decomposingForwardedEmail);
                    try {
                        List<String> content = decomposeMultipartEmail(attachments, bp, decomposingForwardedEmail);
                        if (content != null) {
                            attachment = convertHTMLToPdf(attachments, content, fileName);
                        }

                    } catch (Exception e) {
                        String content = FaxToMailServiceUtils.getTextFromPart(bp);
                        if (StringUtils.isNotBlank(content)) {
                            attachment = convertTextToPdf(content, FileUtil.basename(fileName, ".pdf", ".PDF"));
                        }
                    }
                    if (attachment != null) {
                        attachment.setInlineAttachment(Part.INLINE.equals(disposition));
                        attachments.add(attachment);
                    }


                    // else, this is an attachment
                } else {
                    String fileName = bp.getFileName();

                    // parse Content-ID (content identifier in html mail content)
                    String[] headers = bp.getHeader("Content-ID");
                    String contentID = null;
                    if (headers != null) {
                        contentID = headers[0];
                        contentID = contentID.replaceFirst("^<(.*)>$", "$1");
                    }

                    // remove the guillemets between the id

                    if (fileName == null && contentID == null) {
                        fileName = t("faxtomail.email.content.attachment.unnamed", attachments.size());

                    } else if (fileName == null) {
                        fileName = contentID;
                    }

                    try {
                        fileName = MimeUtility.decodeText(fileName);
                        if (log.isDebugEnabled()) {
                            log.debug("FileName : " + fileName + ", Content-ID : " + contentID);
                        }
                    } catch (UnsupportedEncodingException ex) {
                        // don't care, use filename raw value
                        if (log.isWarnEnabled()) {
                            log.warn("Can't debug email file name", ex);
                        }
                    }

                    DataHandler dh = bp.getDataHandler();

                    // create new attachment
                    Attachment attachment = new AttachmentImpl();
                    attachment.setAddedByUser(false);
                    AttachmentFile attachmentFile = getAttachmentFileFromStream(dh.getInputStream());
                    attachmentFile.setFilename(fileName);
                    attachment.setOriginalFile(attachmentFile);
                    attachment.setContentId(contentID);
                    attachment.setInlineAttachment(Part.INLINE.equals(disposition));

                    // convert attachment if defined by admin
                    convertIfNecessary(attachment);

                    // save attachment
                    attachments.add(attachment);
                }
            }
        }
        return result;
    }

    @Override
    public List<Attachment> extractAttachmentsFromMessage(MimeMessage message) throws Exception {
        List<Attachment> attachments = extractAttachmentsFromMessage(message, 0);
        return attachments;
    }

    protected List<Attachment> extractAttachmentsFromMessage(MimeMessage message, int decomposingForwardedEmail) throws Exception {

        String plainTextFileName = t("faxtomail.email.content.attachment.plainFileName");
        if (decomposingForwardedEmail > 0) {
            plainTextFileName += decomposingForwardedEmail;
        }
        String htmlFileName = t("faxtomail.email.content.attachment.htmlFileName");
        if (decomposingForwardedEmail > 0) {
            htmlFileName += decomposingForwardedEmail;
        }
        
        List<Attachment> attachments = new ArrayList<>();
        if (message.isMimeType("multipart/*")) {

            // manage boundary id
            List<String> htmlContent = decomposeMultipartEmail(attachments, message, decomposingForwardedEmail);
            if (htmlContent != null)  {
                if (log.isDebugEnabled()) {
                    log.debug("Converting html content to pdf : " + message.getSubject());
                }

                Attachment attachment = convertHTMLToPdf(attachments, htmlContent, htmlFileName);
                if (attachment != null) {
                    attachment.setAddedByUser(false);
                    attachment.setMailContent(true);

                    //remove text plain attachement if exists, to avoid having twice the mail content in the attachments
                    for (Attachment a : attachments) {
                        if (plainTextFileName.equals(FileUtil.basename(a.getOriginalFileName(), ".pdf"))) {
                            attachments.remove(a);
                            break;
                        }
                    }
                    attachments.add(attachment);
                }
            }

            // html email
        } else if (message.isMimeType("text/html")) {
            String htmlContent = FaxToMailServiceUtils.getTextFromMessage(message);

            if (StringUtils.isNotBlank(htmlContent)) {
                if (log.isDebugEnabled()) {
                    log.debug("Converting html content to pdf : " + message.getSubject());
                }

                Attachment attachment = convertHTMLToPdf(attachments, Collections.singletonList(htmlContent), htmlFileName);
                if (attachment != null) {
                    attachment.setAddedByUser(false);
                    attachment.setMailContent(true);

                    //remove text plain attachement if exists, to avoid having twice the mail content in the attachments
                    for (Attachment a : attachments) {
                        if (plainTextFileName.equals(FileUtil.basename(a.getOriginalFileName(), ".pdf"))) {
                            attachments.remove(a);
                            break;
                        }
                    }
                    attachments.add(attachment);
                }
            }

            // text email
        } else if (message.isMimeType("text/*")) {
            String content = FaxToMailServiceUtils.getTextFromMessage(message);

            if (StringUtils.isNotBlank(content)) {
                boolean alreadyHtmlContent = false;
                //remove html attachement if exists, to avoid having twice the mail content in the attachments
                for (Attachment a : attachments) {
                    if (htmlFileName.equals(FileUtil.basename(a.getOriginalFileName(), ".pdf"))) {
                        alreadyHtmlContent = true;
                        break;
                    }
                }
                if (!alreadyHtmlContent) {
                    Attachment attachment = convertTextToPdf(content, plainTextFileName);
                    attachment.setAddedByUser(false);
                    attachment.setMailContent(true);
                    attachments.add(0, attachment);
                }
            }

            //directly an attachment
        } else {
            String fileName = message.getFileName();

            try {
                fileName = MimeUtility.decodeText(fileName);

            } catch (UnsupportedEncodingException ex) {
                // don't care, use filename raw value
            }

            DataHandler dh = message.getDataHandler();

            // create new attachment
            Attachment attachment = new AttachmentImpl();
            attachment.setAddedByUser(false);
            attachment.setMailContent(false);
            AttachmentFile attachmentFile = getAttachmentFileFromStream(dh.getInputStream());
            attachmentFile.setFilename(fileName);
            attachment.setOriginalFile(attachmentFile);

            // convert attachment if defined by admin
            convertIfNecessary(attachment);

            // save attachment
            attachments.add(attachment);
        }

        return attachments;
    }

    @Override
    public Attachment convertIfNecessary(Attachment attachment) throws IOException {

        // get file extension
        String filename = attachment.getOriginalFileName();
        String extension = FilenameUtils.getExtension(filename);

        if (StringUtils.isNotBlank(extension)) {
            ExtensionCommand command = getConfigurationService().getExtensionCommand(extension);

            // si une extension est configurée avec une commande non vide
            if (command != null && StringUtils.isNotBlank(command.getConvertToPdfCommand())) {
                attachment = convertToPdf(attachment, command);
            }
        }
        return attachment;
    }

    @Override
    public Attachment convertToPdf(Attachment attachment, ExtensionCommand extensionCommand) throws IOException {

        // get file extension
        String filename = attachment.getOriginalFileName();
        String basename = FilenameUtils.getBaseName(filename);
        String extension = FilenameUtils.getExtension(filename);

        // copy file to temp file
        File file = File.createTempFile("faxtomail-" + basename, "." + extension);
        file.deleteOnExit();
        // on creer un nouveau nom de fichier ou seulement le nom de l'extension change
        // cela permet de fonctionner avec openoffice par exemple où il n'est pas possible
        // de specifier le nom du fichier de sortie
        String fullname = StringUtils.removeEnd(file.getAbsolutePath(), "." + extension);
        File outfile = new File(fullname + ".pdf");
        outfile.deleteOnExit();
        FileUtils.writeByteArrayToFile(file, attachment.getOriginalFile().getContent());

        // get process command
        String command = extensionCommand.getConvertToPdfCommand();
        String[] args = StringUtil.split(command, " ");
        List<String> comArgs = new ArrayList<String>();
        for (String arg : args) {
            String localArg = arg;
            localArg = localArg.replace("%f", file.getAbsolutePath());
            localArg = localArg.replace("%o", outfile.getAbsolutePath());
            comArgs.add(localArg);
        }
        ProcessBuilder pb = new ProcessBuilder(comArgs);
        // run process
        if (log.isDebugEnabled()) {
            log.debug("Convert attachment with command : " + comArgs);
        }
        try {
            Process process = pb.start();
            process.waitFor();

            // read output to save into attachment
            byte[] outContent = FileUtils.readFileToByteArray(outfile);

            // on va dire que normalement un pdf ca fait plus de 1 octet
            if (outContent.length >= 1) {
                AttachmentFile editedFile = new AttachmentFileImpl();
                editedFile.setContent(outContent);
                editedFile.setFilename(basename + "-converted.pdf");
                attachment.setEditedFile(editedFile);
            } else if (log.isWarnEnabled()) {
                log.warn("Can't convert extension to pdf (content is empty)");
            }

        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Cannot run convert command", e);
            }
        } catch (InterruptedException e) {
            if (log.isErrorEnabled()) {
                log.error("Cannot run convert command", e);
            }
            Thread.currentThread().interrupt();
        }

        // cleanup
        file.delete();
        outfile.delete();

        return attachment;
    }

    @Override
    public Attachment convertTextToPdf(String content, String name) throws IOException {
        Preconditions.checkArgument(StringUtils.isNotBlank(content));

        Attachment result = null;

        Document document = Jsoup.parse(content, "UTF-8");
        document.outputSettings().syntax(Document.OutputSettings.Syntax.xml);

        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            PdfRendererBuilder builder = new PdfRendererBuilder();
            //builder.withUri(outputPdf);
            builder.toStream(os);
            builder.withW3cDocument(new W3CDom().fromJsoup(document), "/");
            builder.run();

            // convert content to blob
            AttachmentFile attachmentFileNew = new AttachmentFileImpl();
            attachmentFileNew.setContent(os.toByteArray());
            attachmentFileNew.setFilename(name + ".pdf");

            result = new AttachmentImpl();
            result.setOriginalFile(attachmentFileNew);
            result.setOriginalFileName(name + ".pdf");
            result.setAddedByUser(false);
        } catch (OutOfMemoryError | StackOverflowError er) {
            // certains mails (spam) très compliqués surchargent la mémoire
            if (log.isWarnEnabled()) {
                log.warn("Can't convert html content to pdf", er);
            }
        }

        return result;
    }

    @Override
    public Attachment convertHTMLToPdf(List<Attachment> attachments, List<String> contentList, String name) throws IOException {
        Attachment result = null;

        String content = extractHtmlContent(attachments, contentList);

        Document document = Jsoup.parse(content, "UTF-8");
        document.outputSettings().syntax(Document.OutputSettings.Syntax.xml);

        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            PdfRendererBuilder builder = new PdfRendererBuilder();
            //builder.withUri(outputPdf);
            builder.toStream(os);
            builder.withW3cDocument(new W3CDom().fromJsoup(document), "/");
            builder.run();

            deleteTempAttachmentFiles(attachments);

            // convert content to blob
            AttachmentFile attachmentFileNew = new AttachmentFileImpl();
            attachmentFileNew.setContent(os.toByteArray());
            attachmentFileNew.setFilename(name + ".pdf");

            result = new AttachmentImpl();
            result.setOriginalFile(attachmentFileNew);
            result.setOriginalFileName(name + ".pdf");
            result.setAddedByUser(false);
        } catch (OutOfMemoryError | StackOverflowError er) {
            // certains mails (spam) très compliqués surchargent la mémoire
            if (log.isWarnEnabled()) {
                log.warn("Can't convert html content to pdf", er);
            }
        }

        return result;
    }

    /**
     *
     * WARNING, must call deleteTempAttachmentFiles afterwards so temp directory does not grow indefinitely !
     * Could not be integrated in this method as on transfer, it deletes attachment before really using it
     *
     * @param attachments list of attachments that might be present in c
     * @param contentList email html content
     * @return email html content with attachments and inline attachments curated
     * @throws URIException
     */
    public String extractHtmlContent(List<Attachment> attachments, List<String> contentList) throws URIException {


        List<String> contents = new ArrayList<>();

        if (contentList != null) {
            for (String content : contentList) {
                content = content.replaceAll("<meta (.*?)>(</meta>)?", "");
                // remove the images whose sources are on the filesystem of the sender (yes, it happens...)
                // cf #6996
                content = content.replaceAll("(\\w+)=([\"'])file://.*?([\"'])", "");

                for (Attachment attachment : attachments) {
                    String key = attachment.getContentId();
                    if (key == null) {
                        key = attachment.getOriginalFileName();
                    }

                    // get file content
                    AttachmentFile attachmentFile = attachment.getOriginalFile();
                    if (attachmentFile != null) {
                        File file = attachmentFile.getFile();

                        // replace the inline attachments with the extracted attachment file url
                        // match les patterns:
                        // <td background="cid:bg.gif" height="52">
                        // <img border=0 src="cid:bg.gif" />
                        // <img src='cid:5e9ef859-ea65-4f9b-a9fa-30d4a2c5837c'
                        content = content.replaceAll("(\\w+)=([\"'])cid:" + Pattern.quote(key) + "([\"'])", "$1=$2" + file.toURI() + "$3");

                        if (log.isDebugEnabled()) {
                            log.debug("Mapping attachment id " + key + " to file " + file.toURI());
                        }
                    }
                }

                // remove the remaining cids whose attachment is not in the email (yes, it happens)
                // cf #6996
                content = content.replaceAll("(\\w+)=([\"'])cid:.*?([\"'])", "");

                // on reformate les urls pour supprimer les caractères qui vont pas (ex espaces)
                // cf #7740
                String defaultImageIfMalformedUrl = serviceContext.getApplicationConfig().getDefaultImageIfMalformedUrl();
                content = FaxToMailServiceUtils.encodeImageSourcesInEmail(content, defaultImageIfMalformedUrl);

                //On remplace les font-size:0 pour éviter les font-size too small
                //cf #10123
                //content = content.replace("font-size:0", "font-size:1");

                //content = content.replaceAll("(.+) width=([\"])(.+)([\"])", "$1");
                //content = content.replaceAll("(.+) width=(['])(.+)(['])", "$1");
                //content = content.replaceAll("(.+) height=([\"])(.+)([\"])", "$1");
                //content = content.replaceAll("(.+) height=(['])(.+)(['])", "$1");
                //content = content.replaceAll("(style=\\\")([a-zA-Z0-9:;\\.\\s\\(\\)\\-\\,&#]*)(\\\")", "$1$3");
                //content = content.replaceAll("(style=\\\')([a-zA-Z0-9:;\\.\\s\\(\\)\\-\\,&#]*)(\\\')", "$1$3");
                //content = content.replace("&nbsp;", "");

                contents.add(content);
            }
        }

        return StringUtils.join(contents, "<hr/>");
    }

    public void deleteTempAttachmentFiles(List<Attachment> attachments) {
        for (Attachment attachment : attachments) {
            AttachmentFile attachmentFile = attachment.getOriginalFile();
            if (attachmentFile != null) {
                File file = attachmentFile.getFile();
                file.delete();
            }
        }
    }

    public GeneratedPDFPage createGeneratedPDFPage(Attachment attachment, BufferedImage image) {

        GeneratedPDFPageTopiaDao dao = getPersistenceContext().getGeneratedPDFPageDao();
        GeneratedPDFPage generatedPDFPage = dao.newInstance();


        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

            ImageIO.write(image, "png", baos);
            baos.flush();
            generatedPDFPage.setPage(baos.toByteArray());

            generatedPDFPage = dao.create(generatedPDFPage);
            attachment.addGeneratedPDFPages(generatedPDFPage);

            //force flush to limit RAM overhead
            getPersistenceContext().getHibernateSupport().getHibernateSession().flush();

        } catch (IOException e) {
            log.error("Error while writing image", e);
        }


        return generatedPDFPage;

    }
}
