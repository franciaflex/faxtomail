package com.franciaflex.faxtomail.services;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.HasLabel;
import com.franciaflex.faxtomail.persistence.entities.Reply;
import com.franciaflex.faxtomail.services.service.ldap.Contact;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorProvider;
import org.nuiton.decorator.MultiJXPathDecorator;

import java.text.DateFormat;
import java.util.Date;

/**
 * FaxToMail decorator service.
 *
 * @author Kevin Morin - morin@codelutin.com
 */
public class DecoratorServiceImpl extends FaxToMailServiceSupport implements DecoratorService {

    /** Delegate decorator provider. */
    protected DecoratorProvider decoratorProvider;

    @Override
    public <O> Decorator<O> getDecorator(O object) {
        return decoratorProvider.getDecorator(object);
    }

    @Override
    public <O> Decorator<O> getDecorator(O object, String name) {
        return decoratorProvider.getDecorator(object, name);
    }

    @Override
    public <O> Decorator<O> getDecoratorByType(Class<O> type) {
        return decoratorProvider.getDecoratorByType(type);
    }

    @Override
    public <O> Decorator<O> getDecoratorByType(Class<O> type, String name) {
        return decoratorProvider.getDecoratorByType(type, name);
    }

    @Override
    public void setServiceContext(FaxToMailServiceContext context) {
        super.setServiceContext(context);

        decoratorProvider = new DecoratorProvider() {
            @Override
            protected void loadDecorators() {

                registerDecorator(new Decorator<Float>(Float.class) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public String toString(Object bean) {
                        return bean == null ? "" : String.valueOf(bean);
                    }
                });
                registerDecorator(new Decorator<Number>(Number.class) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public String toString(Object bean) {
                        return bean == null ? "" : String.valueOf(bean);
                    }
                });
                registerDecorator(new Decorator<Date>(Date.class) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public String toString(Object bean) {
                        if (bean == null) {
                            return "";
                        }
                        return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(bean);
                    }
                });
                registerDecorator(DATE, new Decorator<Date>(Date.class) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public String toString(Object bean) {
                        if (bean == null) {
                            return "";
                        }
                        return DateFormat.getDateInstance(DateFormat.MEDIUM).format(bean);
                    }
                });
                registerDecorator(new StringMultiJXPathDecorator());
                registerMultiJXPathDecorator(Contact.class, "${name}$s <${email}$s>", SEPARATOR, " - ");
                registerMultiJXPathDecorator(HasLabel.class, "${label}$s", SEPARATOR, " - ");
                registerDecorator(new Decorator<Attachment>(Attachment.class) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public String toString(Object bean) {
                        if (bean == null) {
                            return "";
                        }
                        return ((Attachment) bean).getOriginalFileName();
                    }
                });
                registerDecorator(new Decorator<AttachmentFile>(AttachmentFile.class) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public String toString(Object bean) {
                        if (bean == null) {
                            return "";
                        }
                        return ((AttachmentFile) bean).getFilename();
                    }
                });
                registerMultiJXPathDecorator(Reply.class, "${sentDate}$s#${subject}$s", SEPARATOR, " - ");
                registerMultiJXPathDecorator(FaxToMailUser.class, "${lastName}$s#${firstName}$s#${trigraph}$s", SEPARATOR, " ");
                registerMultiJXPathDecorator(FaxToMailUser.class, SHORT, "${trigraph}$s", SEPARATOR, " ");
                registerMultiJXPathDecorator(Client.class, "${code}$s#${name}$s#${brand}$s", SEPARATOR, " - ");
            }
        };
    }

    public static class StringMultiJXPathDecorator extends MultiJXPathDecorator<String> implements Cloneable {

        public StringMultiJXPathDecorator() throws IllegalArgumentException, NullPointerException {
            super(String.class, "", "#", null);
        }

        @Override
        public Object clone() {
            return new StringMultiJXPathDecorator();
        }

        @Override
        public String toString(Object bean) {
            return String.valueOf(bean);
        }
    }
}
