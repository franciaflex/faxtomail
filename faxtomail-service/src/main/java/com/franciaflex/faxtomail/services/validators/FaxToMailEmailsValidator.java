package com.franciaflex.faxtomail.services.validators;

/*-
 * #%L
 * FaxToMail :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.util.StringUtil;
import org.nuiton.validator.xwork2.field.NuitonFieldValidatorSupport;

/**
 * Validateur pour autoriser plusieurs emails séparés par des points-virgule
 */
public class FaxToMailEmailsValidator extends NuitonFieldValidatorSupport {


    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {

        String fieldName = getFieldName();
        Object allValues = getFieldValue(fieldName, object);

        if (allValues == null) {
            // no value defined
            return;
        }
        if (allValues instanceof String) {

            String[] values = StringUtils.split((String)allValues,';');
            for (String value:values) {
                if (StringUtils.isEmpty(value)) {
                    // no value defined
                    return;
                }
                if (!StringUtil.isEmail(value)) {
                    addFieldError(fieldName, object);
                }
            }
        } else {
            addFieldError(fieldName, object);
        }
    }

    @Override
    public String getValidatorType() {
        return "emails";
    }
}
