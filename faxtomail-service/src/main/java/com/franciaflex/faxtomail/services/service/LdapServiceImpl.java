package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroup;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroupTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserImpl;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserTopiaDao;
import com.franciaflex.faxtomail.services.FaxToMailServiceSupport;
import com.franciaflex.faxtomail.services.service.ldap.AuthenticationException;
import com.franciaflex.faxtomail.services.service.ldap.Contact;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LdapServiceImpl extends FaxToMailServiceSupport implements LdapService {

    private static final Log log = LogFactory.getLog(LdapServiceImpl.class);

    /**
     * Update all user and group from ldap.
     */
    @Override
    public void updateLdapData() {

        LDAPConnection connection = null;
        try {
            connection = new LDAPConnection(getApplicationConfig().getLdapHost(), 
                    getApplicationConfig().getLdapPort(),
                    getApplicationConfig().getLdapUser(),
                    getApplicationConfig().getLdapPassword());
            if (connection.isConnected()) {

                FaxToMailUserTopiaDao faxtomailUserDao = getPersistenceContext().getFaxToMailUserDao();
                FaxToMailUserGroupTopiaDao faxtomailUserGroupDao = getPersistenceContext().getFaxToMailUserGroupDao();

                // cache group DN > faxtomail user group instance
                Map<String, FaxToMailUserGroup> groupCache = new HashMap<>();
                // cache user DN > faxtomail user instance
                Map<String, FaxToMailUser> userCache = new HashMap<>();

                // get all groups
                String groupsBaseDN = "OU=Mac-Groupe,DC=mac-groupe,DC=net";
                String groupsFilter = "(objectCategory=CN=Group,CN=Schema,CN=Configuration,DC=mac-groupe,DC=net)";
                SearchResult groupsResult = connection.search(groupsBaseDN, SearchScope.SUB, groupsFilter);
                List<SearchResultEntry> groupEntries = groupsResult.getSearchEntries();
                for (SearchResultEntry groupEntry : groupEntries) {
                    String groupDN = groupEntry.getDN();
                    String groupPath = getGroupCompleteName(groupDN);
                    String groupName = StringUtils.substringAfterLast(groupPath, "/");
                    FaxToMailUserGroup userGroup = faxtomailUserGroupDao.forNameEquals(groupName).findUniqueOrNull();
                    if (userGroup == null) {
                        userGroup = faxtomailUserGroupDao.create(
                            FaxToMailUserGroup.PROPERTY_NAME, groupName,
                            FaxToMailUserGroup.PROPERTY_COMPLETE_NAME, groupPath);
                    }
                    groupCache.put(groupDN, userGroup);
                }

                // get all users (
                String usersBaseDN = "OU=Mac-Groupe,DC=mac-groupe,DC=net";
                String usersFilter = "(objectCategory=CN=Person,CN=Schema,CN=Configuration,DC=mac-groupe,DC=net)";
                // on recupere d'abord les organisationUnit pour fractionner les requettes
                // sinon au dela de 1000 resultats ca veux plus
                SearchResult usersResult = connection.search(usersBaseDN, SearchScope.SUB, "(objectClass=organizationalUnit)");
                List<SearchResultEntry> unitEntries = usersResult.getSearchEntries();
                for (SearchResultEntry unitEntry : unitEntries) {
                    if (log.isDebugEnabled()) {
                        log.debug("Search for unit " + unitEntry.getDN());
                    }
                    SearchResult userResult = connection.search(unitEntry.getDN(), SearchScope.ONE, usersFilter);

                    List<SearchResultEntry> userEntries = userResult.getSearchEntries();
                    for (SearchResultEntry userEntry : userEntries) {

                        String login = userEntry.getAttributeValue("sAMAccountName");
                        String userDN = userEntry.getDN();
                        if (login == null) {
                            if (log.isWarnEnabled()) {
                                log.warn("Null sAMAccountName for DN " + userDN);
                            }
                        } else {
                            login = login.toLowerCase();
                            FaxToMailUser user = updateUserFormLdap(connection, userEntry, login, groupCache);
                            userCache.put(userDN, user);
                        }
                    }
                }

                // make remaining user and groups in database as 'hidden'
                List<List<FaxToMailUser>> partitionedUserCache = partition(new ArrayList<>(userCache.values()), 1990);
                Collection<FaxToMailUser> usersToHide = new ArrayList<>();
                for (List<FaxToMailUser> userCacheValues:partitionedUserCache){
                    usersToHide.addAll(faxtomailUserDao.forNotIn(userCacheValues));
                }
                for (FaxToMailUser userToHide : usersToHide) {
                    userToHide.setHidden(true);
                    faxtomailUserDao.update(userToHide);
                }
                List<List<FaxToMailUserGroup>> partitionedGroupCache = partition(new ArrayList<>(groupCache.values()), 1990);
                Collection<FaxToMailUserGroup> userGroupsToHide = new ArrayList<>();
                for (List<FaxToMailUserGroup> userGroupCacheValues:partitionedGroupCache){
                    userGroupsToHide.addAll(faxtomailUserGroupDao.forNotIn(userGroupCacheValues));
                }
                for (FaxToMailUserGroup userGroupToHide : userGroupsToHide) {
                    userGroupToHide.setHidden(true);
                    faxtomailUserGroupDao.update(userGroupToHide);
                }
                getPersistenceContext().commit();
            }

        } catch (LDAPException ex) {
            throw new RuntimeException("Can't connect to ldap", ex);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    /**
     * Transform group CN to group path.
     * 
     * Example:
     * CN=Tout Franciaflex,OU=Listes de distribution,OU=Comptes Spéciaux,OU=Mac-Groupe,DC=mac-groupe,DC=net
     * net/mac-groupe/Mac-Groupe/Comptes Spéciaux/Listes de distribution/Tout Franciaflex
     * 
     * @param groupCN
     * @return
     */
    protected String getGroupCompleteName(String groupCN) {
        String[] parts = groupCN.split(",");
        ArrayUtils.reverse(parts);
        
        // join tab
        String separator = "";
        StringBuilder result = new StringBuilder();
        for (String part : parts) {
            result.append(separator);
            result.append(StringUtils.substringAfter(part, "="));
            separator = "/";
        }
        return result.toString();
    }

    /**
     * Return user for given user id.
     * 
     * @param userTopiaId userTopiaId
     * @return user bean (without password)
     */
    @Override
    public FaxToMailUser getUserBean(String userTopiaId) {
        FaxToMailUser result = null;
        
        FaxToMailUserTopiaDao faxtomailUserDao = getPersistenceContext().getFaxToMailUserDao();
        FaxToMailUser user = faxtomailUserDao.forTopiaIdEquals(userTopiaId).findUniqueOrNull();
        if (user != null) {
            Binder<FaxToMailUser, FaxToMailUser> faxToMailUserBinder = BinderFactory.newBinder(FaxToMailUser.class);
            result = new FaxToMailUserImpl();
            faxToMailUserBinder.copyExcluding(user, result);
        }
        return result;
    }

    /**
     * Authenticate and update ldap user.
     * 
     * @param login login
     * @param password password
     * @return authenticated user
     * @throws AuthenticationException if authentication fails
     */
    @Override
    public FaxToMailUser authenticateUser(String login, String password) throws AuthenticationException {

        FaxToMailUser result;

        login = login.toLowerCase();
        try {
            // first connexion to get full user login
            LDAPConnection adminConnect = new LDAPConnection();
            adminConnect.connect(getApplicationConfig().getLdapHost(), getApplicationConfig().getLdapPort());
            adminConnect.bind(getApplicationConfig().getLdapUser(), getApplicationConfig().getLdapPassword());

            // search user in ldap
            String userDN = null;
            SearchResultEntry searchEntry = null;
            if (adminConnect.isConnected()) {
                // sn est le login interne à franciaflex
                String filter = String.format("(sAMAccountName=%s)", login);
                SearchResult searchResult = adminConnect.search(getApplicationConfig().getLdapBaseDn(), SearchScope.SUB, filter);
                if (!searchResult.getSearchEntries().isEmpty()) {
                    searchEntry = searchResult.getSearchEntries().get(0);
                    userDN = searchEntry.getDN();
                }
            }

            if (searchEntry != null) {
                // ouvre une connexion avec l'identification de l'utilisateur qui essaye de se connecter
                // à l'application (ca permet de vérifier l'authentification)
                LDAPConnection userConnect = new LDAPConnection();
                userConnect.connect(getApplicationConfig().getLdapHost(), getApplicationConfig().getLdapPort());
                userConnect.bind(userDN, password);
    
                if (userConnect.isConnected()) {
                    // update in database
                    result = updateUserFormLdap(adminConnect, searchEntry, login, null);
                    getPersistenceContext().commit();

                    userConnect.close();
                } else {
                    throw new AuthenticationException("Not connected");
                }
            } else {
                throw new AuthenticationException("Utilisateur inconnu : " + login);
            }
            adminConnect.close();

        } catch (LDAPException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't login to ldap", ex);
            }
            throw new AuthenticationException(ex.getResultCode().getName(), ex);
        }
        return result;
    }

    /**
     * Retreive user from principal and updated user database instance.
     * 
     * This method doesn't require any password.
     * 
     * @param login user principal (without domain)
     * @return user instance
     * @throws AuthenticationException is user can't be found in ldap
     */
    @Override
    public FaxToMailUser getUserFromPrincipal(String login) throws AuthenticationException {

        FaxToMailUser result;

        login = login.toLowerCase();

        try {
            // first connexion to get full user login
            LDAPConnection adminConnect = new LDAPConnection();
            adminConnect.connect(getApplicationConfig().getLdapHost(), getApplicationConfig().getLdapPort());
            adminConnect.bind(getApplicationConfig().getLdapUser(), getApplicationConfig().getLdapPassword());

            // search user in ldap
            SearchResultEntry searchEntry = null;
            if (adminConnect.isConnected()) {
                // sAMAccountName est l'identifiant kerberos
                String filter = String.format("(sAMAccountName=%s)", login);
                SearchResult searchResult = adminConnect.search(getApplicationConfig().getLdapBaseDn(), SearchScope.SUB, filter);
                if (!searchResult.getSearchEntries().isEmpty()) {
                    searchEntry = searchResult.getSearchEntries().get(0);
                }
                
            } else {
                throw new AuthenticationException("Utilisateur inconnu : " + login);
            }

            if( searchEntry != null ) {
                // update in database
                result = updateUserFormLdap(adminConnect, searchEntry, login, null);
                getPersistenceContext().commit();
            } else {
                throw new AuthenticationException("Utilisateur inconnu : " + login);
            }
            
            adminConnect.close();
        } catch (LDAPException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't login to ldap", ex);
            }
            throw new AuthenticationException(ex.getResultCode().getName(), ex);
        }
        return result;
    }

    /**
     * Update user (or create) in database from ldap search result entry.
     * 
     * @param adminConnect admin connection used to load groups content
     * @param searchEntry entry containing data
     * @param login login
     * @param groupCache group cache when mass update (can be null)
     * @return updated user
     * @throws LDAPException 
     */
    protected FaxToMailUser updateUserFormLdap(LDAPConnection adminConnect, SearchResultEntry searchEntry, String login, Map<String, FaxToMailUserGroup> groupCache) throws LDAPException {

        FaxToMailUser user;

        // create or 
        FaxToMailUserTopiaDao faxtomailUserDao = getPersistenceContext().getFaxToMailUserDao();
        FaxToMailUserGroupTopiaDao faxtomailUserGroupDao = getPersistenceContext().getFaxToMailUserGroupDao();
        user = faxtomailUserDao.forLoginEquals(login).findUniqueOrNull();
        
        if (user == null) {
            user = new FaxToMailUserImpl();
            user.setLogin(login);
        }
        user.setHidden(false);
        
        // update other ldap fields
        String fullName = searchEntry.getAttributeValue("name");
        if (fullName.indexOf(' ') != -1) {
            String lastName = fullName.substring(0, fullName.indexOf(' '));
            String firstName = fullName.substring(fullName.indexOf(' ') + 1);
            user.setFirstName(firstName);
            user.setLastName(lastName);
        } else {
            user.setFirstName("");
            user.setLastName(fullName);
        }

        String trigraph = searchEntry.getAttributeValue("sAMAccountName");
        user.setTrigraph(StringUtils.upperCase(trigraph));

        // manage user groups
        user.clearUserGroups();
        String[] groups = searchEntry.getAttributeValues("memberOf");
        if (ArrayUtils.isNotEmpty(groups)) {
            for (String group : groups) {

                // en mass-update, on a le cache
                if (groupCache != null) {
                    FaxToMailUserGroup userGroup = groupCache.get(group);
                    if (userGroup != null) {
                        user.addUserGroups(userGroup);
                    }
                } else {
                    // parmis tous les groupes, on n'utilise que ceux qui sont de la catégories
                    // CN=Group,CN=Schema,CN=Configuration,DC=mac-groupe,DC=net
                    SearchResultEntry groupSearchEntry = adminConnect.getEntry(group);
                    String objectCategory = groupSearchEntry.getAttributeValue("objectCategory");
                    if ("CN=Group,CN=Schema,CN=Configuration,DC=mac-groupe,DC=net".equals(objectCategory)) {
                        String groupPath = getGroupCompleteName(group);
                        String groupName = StringUtils.substringAfterLast(groupPath, "/");
                        FaxToMailUserGroup userGroup = faxtomailUserGroupDao.forNameEquals(groupName).findUniqueOrNull();
                        if (userGroup == null) {
                            userGroup = faxtomailUserGroupDao.create(
                                FaxToMailUserGroup.PROPERTY_NAME, groupName,
                                FaxToMailUserGroup.PROPERTY_COMPLETE_NAME, groupPath);
                        }
                        user.addUserGroups(userGroup);
                    }
                }
            }
        }

        if (user.isPersisted()) {
            user = faxtomailUserDao.update(user);
        } else {
            user = faxtomailUserDao.create(user);
        }
        
        // force collection loading to force non lazy collections
        user.isAffectedFoldersEmpty();
        user.isUserGroupsEmpty();

        return user;
    }

    public List<Contact> getUserAndEmails() {
        List<Contact> result = new ArrayList<>();
        LDAPConnection connection = null;
        try {
            connection = new LDAPConnection(getApplicationConfig().getLdapHost(),
                                            getApplicationConfig().getLdapPort(),
                                            getApplicationConfig().getLdapUser(),
                                            getApplicationConfig().getLdapPassword());
            if (connection.isConnected()) {

                // get all users (
                String usersBaseDN = getApplicationConfig().getLdapBaseDn();
                String usersFilter = "(objectCategory=CN=Person,CN=Schema,CN=Configuration,DC=mac-groupe,DC=net)";

                // on recupere d'abord les organisationUnit pour fractionner les requettes
                // sinon au dela de 1000 resultats ca veux plus
                SearchResult usersResult = connection.search(usersBaseDN, SearchScope.SUB, "(objectClass=organizationalUnit)");
                List<SearchResultEntry> unitEntries = usersResult.getSearchEntries();
                for (SearchResultEntry unitEntry : unitEntries) {
                    if (log.isDebugEnabled()) {
                        log.debug("Search for unit " + unitEntry.getDN());
                    }
                    SearchResult userResult = connection.search(unitEntry.getDN(), SearchScope.ONE, usersFilter);

                    List<SearchResultEntry> userEntries = userResult.getSearchEntries();
                    for (SearchResultEntry userEntry : userEntries) {

                        String name = userEntry.getAttributeValue("displayname");
                        String email = userEntry.getAttributeValue("mail");
                        if (StringUtils.isNotBlank(email)) {
                            result.add(new Contact(name, email));
                        }
                    }
                }
            }

        } catch (LDAPException ex) {
            throw new RuntimeException("Can't connect to ldap", ex);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    public List<FaxToMailUser> getUsersForGroup(FaxToMailUserGroup group){
        FaxToMailUserTopiaDao faxtomailUserDao = getPersistenceContext().getFaxToMailUserDao();

        List<FaxToMailUser> users = faxtomailUserDao.forUserGroupsContains(group).findAll();
        return users;

    }

    public static <T> List<List<T>> partition(List<T> list, int size) {

        if (list == null)
            throw new NullPointerException(
                    "'list' must not be null");
        if (!(size > 0))
            throw new IllegalArgumentException(
                    "'size' must be greater than 0");

        return new Partition<>(list, size);
    }

    private static class Partition<T> extends AbstractList<List<T>> {

        final List<T> list;
        final int size;

        Partition(List<T> list, int size) {
            this.list = list;
            this.size = size;
        }

        @Override
        public List<T> get(int index) {
            int listSize = size();
            if (listSize < 0)
                throw new IllegalArgumentException("negative size: " + listSize);
            if (index < 0)
                throw new IndexOutOfBoundsException(
                        "index " + index + " must not be negative");
            if (index >= listSize)
                throw new IndexOutOfBoundsException(
                        "index " + index + " must be less than size " + listSize);
            int start = index * size;
            int end = Math.min(start + size, list.size());
            return list.subList(start, end);
        }

        @Override
        public int size() {
            return (list.size() + size - 1) / size;
        }

        @Override
        public boolean isEmpty() {
            return list.isEmpty();
        }
    }
}


