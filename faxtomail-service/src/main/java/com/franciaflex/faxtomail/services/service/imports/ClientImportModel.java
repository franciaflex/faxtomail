package com.franciaflex.faxtomail.services.service.imports;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ext.AbstractImportModel;

import java.text.ParseException;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class ClientImportModel extends AbstractImportModel<ClientImportBean> {

    public ClientImportModel(char separator) {
        super(separator);

        ValueParser<String> nullParser = new ValueParser<String>() {
            @Override
            public String parse(String s) throws ParseException {
                if ("NULL".equals(s)) {
                    s = null;
                }
                return s;
            }
        };

        // parser qui retire les non nombres des numero de fax
        ValueParser<String> faxParser = new ValueParser<String>() {
            @Override
            public String parse(String s) throws ParseException {
                if ("NULL".equals(s)) {
                    s = null;
                }
                if (s != null) {
                    s = StringUtils.removePattern(s, "[^0-9]");
                    if ("0".equals(s) || s.isEmpty()) {
                        s = null;
                    }
                }

                return s;
            }
        };

        newIgnoredColumn("Id_Correspondance");
        newMandatoryColumn("Societe", Client.PROPERTY_COMPANY);
        newMandatoryColumn("Marque", Client.PROPERTY_BRAND, nullParser);
        newMandatoryColumn("Nom", Client.PROPERTY_NAME);
        newMandatoryColumn("Caracteristique1", Client.PROPERTY_CARACTERISTIC1, nullParser);
        newMandatoryColumn("Caracteristique2", Client.PROPERTY_CARACTERISTIC2, nullParser);
        newMandatoryColumn("Caracteristique3", Client.PROPERTY_CARACTERISTIC3, nullParser);
        newMandatoryColumn("Numero_Fax", ClientImportBean.PROPERTY_FAX_NUMBER, faxParser);
        newMandatoryColumn("Adresse_Mail", ClientImportBean.PROPERTY_EMAIL_ADDRESS, nullParser);
        newMandatoryColumn("Code_Client", Client.PROPERTY_CODE);

        newIgnoredColumn("Client_Top");
        newIgnoredColumn("Objet_Message");
        newIgnoredColumn("Destination");
    }

    @Override
    public ClientImportBean newEmptyInstance() {
        return new ClientImportBean();
    }
}
