package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.ClientImpl;
import com.franciaflex.faxtomail.persistence.entities.ClientTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.NewClient;
import com.franciaflex.faxtomail.services.FaxToMailServiceSupport;
import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.persistence.support.TopiaSqlWork;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Service de gestion des clients.
 * 
 * @author Kevin Morin (Code Lutin)
 */
public class ClientServiceImpl extends FaxToMailServiceSupport implements ClientService {

    private static final Log log = LogFactory.getLog(ClientServiceImpl.class);

    /**
     * Recherche le client qui correspond à l'adresse email de l'expediteur du mail.
     * 
     * This method is a getter, but modify input {@code email} !
     * 
     * @param emailAddress sender address
     * @param email email
     * @param company company of the client
     * @return client for emailAddress (can be {@code null})
     */
    @Override
    public List<Client> getClientForEmailAddress(String emailAddress, Email email, final String company, List<String> brands) {
        Preconditions.checkNotNull(email);
        Preconditions.checkNotNull(emailAddress);

        ClientTopiaDao clientDao = getPersistenceContext().getClientDao();

        List<Client> result = null;
        List<Client> clients;
        if (StringUtils.isNotBlank(emailAddress)) {
            clients = clientDao.forEmailAddressesJsonLike(emailAddress);

            // on essaye de voir si on peut gérer l'adresse de l'expediteur comme un fax
            if (CollectionUtils.isEmpty(clients)) {

                String faxNumber;
                // deal with buggy email adresses
                if (emailAddress.contains("@")) {
                    faxNumber = emailAddress.substring(0, emailAddress.indexOf('@')).replace(" ", "");
                } else {
                    faxNumber = emailAddress;
                }
                // NumberUtils.isNumber peut echouer (notation octal)
                if (StringUtils.isNumeric(faxNumber)) {
                    emailAddress = StringUtils.leftPad(faxNumber, 10, '0');
                    clients = clientDao.forFaxNumbersJsonLike(emailAddress);
                    email.setFax(true);
                }
            }

            if (CollectionUtils.isNotEmpty(clients)) {
                // transform the brand list into a list of lower case brands
                final List<String> lowerCaseBrands = brands == null ? new ArrayList<>() :
                        Lists.transform(brands, s -> StringUtils.lowerCase(s));

                result = new ArrayList<>(Collections2.filter(clients, client -> {
                    boolean equalsCompany = StringUtils.isBlank(company)
                            || Objects.equals(company, client.getCompany());
                    boolean inBrands = CollectionUtils.isEmpty(lowerCaseBrands)
                            || lowerCaseBrands.contains(StringUtils.lowerCase(client.getBrand()));
                    return equalsCompany && inBrands;
                }));
            }
        }
        
        // on doit faire l'affectation ici car emailAddress a pu etre remplacé par le numero de fax
        email.setSender(emailAddress);
        return result;
    }

    /**
     * Recherche un client par code et société.
     * 
     * @param code client code
     * @param company
     * @return le client pour la société
     */
    @Override
    public Client getClientForCode(String code, String company) {
        ClientTopiaDao clientDao = getPersistenceContext().getClientDao();
        Client client = null;
        if (StringUtils.isNotBlank(code)) {
            TopiaQueryBuilderAddCriteriaOrRunQueryStep<Client> ctqbacorqs = clientDao.forCodeEquals(code);
            if (StringUtils.isNotBlank(company)) {
                ctqbacorqs = ctqbacorqs.addEquals(Client.PROPERTY_COMPANY, company);
            }
            client = ctqbacorqs.findAnyOrNull();
        }
        return client;
    }

    @Override
    public List<Client> getClientsForFolder(MailFolder folder) {
        while (!folder.isUseCurrentLevelCompany() && folder.getParent() != null) {
            folder = folder.getParent();
        }

        String company = folder.getCompany();

        ClientTopiaDao clientDao = getPersistenceContext().getClientDao();
        List<Client> result = clientDao.forCompanyEquals(company).findAll();
        return result;
    }

    @Override
    public List<Client> getClientsForFolder(MailFolder folder, String filter) {
        while (!folder.isUseCurrentLevelCompany() && folder.getParent() != null) {
            folder = folder.getParent();
        }

        String company = folder.getCompany();

        ClientTopiaDao clientDao = getPersistenceContext().getClientDao();
        List<Client> result = clientDao.forCompanyFiltered(company,filter);
        return result;
    }

    /**
     * Récupère les information de la table NewClient pour mettre à jour la table Client.
     */
    @Override
    public void updateNewClients() {

        final ClientTopiaDao clientDao = getPersistenceContext().getClientDao();
        final FaxToMailUserTopiaDao faxToMailUserDao = getPersistenceContext().getFaxToMailUserDao();

        TopiaSqlSupport sqlSupport = getPersistenceContext().getSqlSupport();
        sqlSupport.doSqlWork(new TopiaSqlWork() {

            @Override
            public void execute(Connection connection) throws SQLException {

                int importedCount = 0;

                // attention, le sql à pour cible postgresql, mssql, h2, il doit rester simple
                String query = String.format("SELECT %s, %s, %s, %s, %s, %s, %s, %s, %s, %s FROM %s",
                        NewClient.PROPERTY_NAME,
                        NewClient.PROPERTY_EMAIL_ADDRESS,
                        NewClient.PROPERTY_FAX_NUMBER,
                        NewClient.PROPERTY_CARACTERISTIC1,
                        NewClient.PROPERTY_CARACTERISTIC2,
                        NewClient.PROPERTY_CARACTERISTIC3,
                        NewClient.PROPERTY_CODE,
                        NewClient.PROPERTY_BRAND,
                        NewClient.PROPERTY_COMPANY,
                        NewClient.PROPERTY_PERSON_IN_CHARGE,
                        NewClient.class.getSimpleName());
                Statement stat = connection.createStatement();

                // get query result set
                ResultSet resultSet = stat.executeQuery(query);
                // cache utilise pour mettre à jour les mêmes client dans le cas ou
                // il reviennt plusieurs fois
                MultiKeyMap<String, Client> clientCache = new MultiKeyMap<>();

                // parse result set
                while (resultSet.next()) {
                    String name = resultSet.getString(NewClient.PROPERTY_NAME);
                    String emailAddress = resultSet.getString(NewClient.PROPERTY_EMAIL_ADDRESS);
                    String faxNumber = resultSet.getString(NewClient.PROPERTY_FAX_NUMBER);
                    String caracteristic1 = resultSet.getString(NewClient.PROPERTY_CARACTERISTIC1);
                    String caracteristic2 = resultSet.getString(NewClient.PROPERTY_CARACTERISTIC2);
                    String caracteristic3 = resultSet.getString(NewClient.PROPERTY_CARACTERISTIC3);
                    String code = resultSet.getString(NewClient.PROPERTY_CODE);
                    String brand = resultSet.getString(NewClient.PROPERTY_BRAND);
                    String company = resultSet.getString(NewClient.PROPERTY_COMPANY);
                    String personInCharge = resultSet.getString(NewClient.PROPERTY_PERSON_IN_CHARGE);

                    // clear some data
                    if ("null".equalsIgnoreCase(caracteristic1)) {
                        caracteristic1 = null;
                    }
                    if ("null".equalsIgnoreCase(caracteristic2)) {
                        caracteristic2 = null;
                    }
                    if ("null".equalsIgnoreCase(caracteristic3)) {
                        caracteristic3 = null;
                    }
                    if ("null".equalsIgnoreCase(brand)) {
                        brand = null;
                    }
                    if ("null".equalsIgnoreCase(emailAddress)) {
                        emailAddress = null;
                    }
                    if (faxNumber != null) {
                        faxNumber = StringUtils.removePattern(faxNumber, "[^0-9]");
                        if (faxNumber.isEmpty() || faxNumber.equals("0")) {
                            faxNumber = null;
                        }
                    }

                    // save data
                    Client client = clientCache.get(code, company);
                    if (client == null) {
                        client = clientDao.forCodeEquals(code).addEquals(Client.PROPERTY_COMPANY, company).findUniqueOrNull();
                        if (client == null) {
                            client = new ClientImpl();
                            client.setCode(code);
                            client.setCompany(company);
                        }

                        client.setName(name);
                        client.setCaracteristic1(caracteristic1);
                        client.setCaracteristic2(caracteristic2);
                        client.setCaracteristic3(caracteristic3);
                        client.setBrand(brand);
                        
                        // clear current adresses and fax numbers
                        client.setEmailAddresses(null);
                        client.setFaxNumbers(null);

                        // look for personInCharge
                        FaxToMailUser faxToMailUserInCharge = null;
                        if (StringUtils.isNotBlank(personInCharge)) {
                            faxToMailUserInCharge = faxToMailUserDao.forLoginEquals(personInCharge).findUniqueOrNull();
                        }
                        client.setPersonInCharge(faxToMailUserInCharge);

                        if (client.isPersisted()) {
                            client = clientDao.update(client);
                        } else {
                            client = clientDao.create(client);
                        }
                        
                        // update cache
                        clientCache.put(code, company, client);
                    }

                    // only manage emailAdress and faxNumber in this case client is in cache
                    if (StringUtils.isNotBlank(emailAddress)) {
                        List<String> currentAddresses = client.getEmailAddresses();
                        if (currentAddresses == null) {
                            currentAddresses = new ArrayList<String>();
                        }
                        currentAddresses.add(emailAddress);
                        client.setEmailAddresses(currentAddresses);
                        client = clientDao.update(client);
                    }
                    // and............................ faxNumber
                    if (StringUtils.isNotBlank(faxNumber)) {
                        List<String> faxNumbers = client.getFaxNumbers();
                        if (faxNumbers == null) {
                            faxNumbers = new ArrayList<String>();
                        }
                        faxNumbers.add(faxNumber);
                        client.setFaxNumbers(faxNumbers);
                        client = clientDao.update(client);
                    }
                    
                    importedCount++;
                }

                //close resultSet when finished
                resultSet.close();
                
                // delete all rows
                stat = connection.createStatement();
                stat.execute("DELETE FROM " + NewClient.class.getSimpleName());
                
                //close statement when finished
                stat.close();

                // usefull log info (do not remove)
                if (importedCount > 0 && log.isInfoEnabled()) {
                    log.info(String.format("Imported %d new client rows", importedCount));
                }
            }
        });
        
        getPersistenceContext().commit();
    }

    @Override
    public List<Client> getAllClientsForUser(FaxToMailUser user) {
        List<MailFolder> folders = getMailFolderService().getRootMailFoldersWithReadingRights(user);
        Set<String> companies = new HashSet<>();

        for (MailFolder folder : folders) {

            MailFolder folderWithCompany = folder;
            while (!folderWithCompany.isUseCurrentLevelCompany() && folderWithCompany != null) {
                folderWithCompany = folderWithCompany.getParent();
            }

            if (folderWithCompany != null) {
                companies.add(folderWithCompany.getCompany());
            }
        }

        fetchCompaniesFromFolders(folders, companies);

        ClientTopiaDao clientDao = getPersistenceContext().getClientDao();
        List<Client> result = clientDao.forCompanyIn(companies).findAll();
        return result;
    }

    @Override
    public List<Client> getAllClientsForUserFilter(FaxToMailUser user, String filter) {
        List<MailFolder> folders = getMailFolderService().getRootMailFoldersWithReadingRights(user);
        Set<String> companies = new HashSet<>();

        for (MailFolder folder : folders) {

            MailFolder folderWithCompany = folder;
            while (!folderWithCompany.isUseCurrentLevelCompany()) {
                folderWithCompany = folderWithCompany.getParent();
            }
            companies.add(folderWithCompany.getCompany());
        }

        fetchCompaniesFromFolders(folders, companies);

        ClientTopiaDao clientDao = getPersistenceContext().getClientDao();
        List<Client> result = clientDao.forCompanyInFiltered(companies,filter);
        return result;
    }

    @Override
    public List<Client> getAllClientsForEmailOrFax(String query) {
        return getPersistenceContext().getClientDao().forEmailAddressOrCodeLike(query);
    }

    protected void fetchCompaniesFromFolders(Collection<MailFolder> folders, Set<String> companies) {
        for (MailFolder folder : folders) {
            if (folder.isUseCurrentLevelCompany()) {
                companies.add(folder.getCompany());
            }

            fetchCompaniesFromFolders(folder.getChildren(), companies);
        }

    }
}
