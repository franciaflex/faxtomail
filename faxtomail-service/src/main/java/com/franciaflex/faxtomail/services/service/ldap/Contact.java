package com.franciaflex.faxtomail.services.service.ldap;

/*
 * #%L
 * FaxToMail :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class Contact extends AbstractSerializableBean {

    public static final String PROPERTY_NAME = "name";
    public static final String PROPERTY_EMAIL = "email";

    protected String name;
    protected String email;

    public Contact(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        Object oldValue = getName();
        this.name = name;
        firePropertyChange(PROPERTY_NAME, oldValue, name);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        Object oldValue = getEmail();
        this.email = email;
        firePropertyChange(PROPERTY_EMAIL, oldValue, email);
    }
}
