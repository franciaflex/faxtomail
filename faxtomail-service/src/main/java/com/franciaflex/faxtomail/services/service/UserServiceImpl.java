package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroup;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroupTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.Stamp;
import com.franciaflex.faxtomail.persistence.entities.StampTopiaDao;
import com.franciaflex.faxtomail.services.FaxToMailServiceSupport;
import com.google.common.base.Preconditions;
import org.hibernate.Hibernate;

import java.util.Collection;
import java.util.List;

/**
 * @author kmorin - kmorin@codelutin.com
 */
public class UserServiceImpl extends FaxToMailServiceSupport implements UserService {

    /**
     * Retourne le nombre d'utilisateur en base.
     * 
     * @return user count
     */
    @Override
    public long getActiveUserCount() {
        FaxToMailUserTopiaDao faxtomailUserDao = getPersistenceContext().getFaxToMailUserDao();
        long result = faxtomailUserDao.forHiddenEquals(false).count();
        return result;
    }

    /**
     * Retourne le nombre de groupe en base.
     * 
     * @return group count
     */
    @Override
    public long getActiveGroupCount() {
        FaxToMailUserGroupTopiaDao faxtomailUserGroupDao = getPersistenceContext().getFaxToMailUserGroupDao();
        long result = faxtomailUserGroupDao.forHiddenEquals(false).count();
        return result;
    }

    @Override
    public List<FaxToMailUser> getAllActiveUsers() {
        FaxToMailUserTopiaDao dao = getPersistenceContext().getFaxToMailUserDao();
        return dao.forHiddenEquals(false).setOrderByArguments(FaxToMailUser.PROPERTY_FIRST_NAME, FaxToMailUser.PROPERTY_LAST_NAME).findAll();
    }

    @Override
    public FaxToMailUser getUserByLogin(String login) {
        FaxToMailUserTopiaDao dao = getPersistenceContext().getFaxToMailUserDao();
        FaxToMailUser user = dao.forLoginEquals(login)
                                    .findUnique();
        return user;
    }

    @Override
    public List<FaxToMailUserGroup> getAllActiveUserGroups() {
        FaxToMailUserGroupTopiaDao dao = getPersistenceContext().getFaxToMailUserGroupDao();
        return dao.forHiddenEquals(false).setOrderByArguments(FaxToMailUserGroup.PROPERTY_COMPLETE_NAME).findAll();
    }

    @Override
    public FaxToMailUser getUser(String topiaId) {
        FaxToMailUserTopiaDao dao = getPersistenceContext().getFaxToMailUserDao();
        FaxToMailUser user = dao.forTopiaIdEquals(topiaId)
                                /*.addAllFetches(FaxToMailUser.PROPERTY_AFFECTED_FOLDERS)*/
                                .findUnique();
        Hibernate.initialize(user.getAffectedFolders());
        return user;
    }

    @Override
    public Collection<Stamp> getPdfEditorStamps(FaxToMailUser user) {
        Preconditions.checkNotNull(user);

        StampTopiaDao stampDao = getPersistenceContext().getStampDao();
        Collection<Stamp> result = stampDao.findAllForUser(user);

        return result;
    }
}
