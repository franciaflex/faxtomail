package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.*;
import com.franciaflex.faxtomail.services.DecoratorService;
import com.franciaflex.faxtomail.services.FaxToMailServiceSupport;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.franciaflex.faxtomail.services.service.exceptions.InvalidClientException;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.nuiton.i18n.I18n.t;

public class InitFaxToMailServiceImpl extends FaxToMailServiceSupport implements InitFaxToMailService {

    private static final Log log = LogFactory.getLog(InitFaxToMailServiceImpl.class);

    @Override
    public void init() {

        if (log.isInfoEnabled()) {
            log.info("faxtomail initialization started");
        }

        boolean devMode = serviceContext.getApplicationConfig().isDevMode();

        if (devMode) {
            initSampleData();
        }

        // Reset the value stack used, if required, a new will be recreated using the struts ActionContext.
        // Need to do it here, if we want to be synched between the struts ActionContext and the value stack.
//        XWork2ValidatorUtil.setSharedValueStack(null);

        if (log.isInfoEnabled()) {
            log.info("faxtomail initialization done");
        }

    }

    protected void initSampleData() {
        log.debug("init Sample Data");

        DemandTypeTopiaDao demandTypeDao = getPersistenceContext().getDemandTypeDao();
        long count = demandTypeDao.count();

        if (count == 0) {

            List<DemandType> types = Lists.newArrayList(
                    demandTypeDao.create(DemandType.PROPERTY_LABEL, "Commande",
                            DemandType.PROPERTY_REQUIRED_FIELDS, EnumSet.of(MailField.RANGE_ROW)),
                    demandTypeDao.create(DemandType.PROPERTY_LABEL, "Commande Réponse"),
                    demandTypeDao.create(DemandType.PROPERTY_LABEL, "Commande Annulation/Modification"),
                    demandTypeDao.create(DemandType.PROPERTY_LABEL, "Devis Diffus"),
                    demandTypeDao.create(DemandType.PROPERTY_LABEL, "Devis Chantier"),
                    demandTypeDao.create(DemandType.PROPERTY_LABEL, "Devis Gros Chantier"),
                    demandTypeDao.create(DemandType.PROPERTY_LABEL, "Devis Réponse/Modification"),
                    demandTypeDao.create(DemandType.PROPERTY_LABEL, "Réclamation"),
                    demandTypeDao.create(DemandType.PROPERTY_LABEL, "Autres")
            );

            WaitingStateTopiaDao waitingStateDao = getPersistenceContext().getWaitingStateDao();
            List<WaitingState> waitingStates = Lists.newArrayList(
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "BAT attente retour BAT lettrage-logo"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "CLT attente réponse client"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "DAC attente pour assistance chantier"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "DAV attente analyse avoir"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "EXP attente expertise"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "GAB attente reception gabarit"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "GRA attente accord gratuit"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "ODC attente Ouverture de Compte"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "PPC attente chiffrage BEI PPC"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "PROFORMA attente paiement proforma"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "PROTO attente accord proto"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "REM Attente Remise-accord expo"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "RET attente retour materiel"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "SIGN attente réponse BEI signature"),
                    waitingStateDao.create(WaitingState.PROPERTY_LABEL, "SPAM")
            );

            PriorityTopiaDao priorityDao = getPersistenceContext().getPriorityDao();
            List<Priority> priorities = Lists.newArrayList(
                    priorityDao.create(Priority.PROPERTY_LABEL, "!"),
                    priorityDao.create(Priority.PROPERTY_LABEL, "F")
            );

            RangeTopiaDao rangeDao = getPersistenceContext().getRangeDao();
            List<Range> ranges = Lists.newArrayList(
                    rangeDao.create(Range.PROPERTY_LABEL, "FFE VR"),
                    rangeDao.create(Range.PROPERTY_LABEL, "FFE AUTRE"),
                    rangeDao.create(Range.PROPERTY_LABEL, "FME FX DROIT"),
                    rangeDao.create(Range.PROPERTY_LABEL, "FME FX CINTRE"),
                    rangeDao.create(Range.PROPERTY_LABEL, "FME NOEL"),
                    rangeDao.create(Range.PROPERTY_LABEL, "FSE BANNE"),
                    rangeDao.create(Range.PROPERTY_LABEL, "FSE ROCH AUTRE"),
                    rangeDao.create(Range.PROPERTY_LABEL, "FSE CHECY"),
                    rangeDao.create(Range.PROPERTY_LABEL, "FSI"),
                    rangeDao.create(Range.PROPERTY_LABEL, "FSI FMI")
            );

            // user groups
            FaxToMailUserGroupTopiaDao userGroupDao = getPersistenceContext().getFaxToMailUserGroupDao();
            userGroupDao.create(FaxToMailUserGroup.PROPERTY_NAME, "Chargés de clientèle",
                    FaxToMailUserGroup.PROPERTY_COMPLETE_NAME, "Franciaflex/Chargés de clientèle");
            FaxToMailUserGroup commerciauxGroup = userGroupDao.create(FaxToMailUserGroup.PROPERTY_NAME, "Commerciaux",
                    FaxToMailUserGroup.PROPERTY_COMPLETE_NAME, "Franciaflex/Commerciaux");
            FaxToMailUserGroup savGroup = userGroupDao.create(FaxToMailUserGroup.PROPERTY_NAME, "SAV",
                                                              FaxToMailUserGroup.PROPERTY_COMPLETE_NAME, "Franciaflex/SAV");
            userGroupDao.create(FaxToMailUserGroup.PROPERTY_NAME, "Administrateurs",
                    FaxToMailUserGroup.PROPERTY_COMPLETE_NAME, "Franciaflex/Administrateurs");

            FaxToMailUserTopiaDao userDao = getPersistenceContext().getFaxToMailUserDao();
            FaxToMailUser marc = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Lefèbvre",
                                                FaxToMailUser.PROPERTY_FIRST_NAME, "Marc",
                                                FaxToMailUser.PROPERTY_LOGIN, "mlefebvre",
                                                FaxToMailUser.PROPERTY_TRIGRAPH, "MLE",
                                                FaxToMailUser.PROPERTY_USER_GROUPS, Collections.singletonList(commerciauxGroup));
            FaxToMailUser cyril = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Baillet",
                                                 FaxToMailUser.PROPERTY_FIRST_NAME, "Cyril",
                                                 FaxToMailUser.PROPERTY_LOGIN, "cbaillet",
                                                 FaxToMailUser.PROPERTY_TRIGRAPH, "CBA");
            FaxToMailUser frederic = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Viala",
                                                    FaxToMailUser.PROPERTY_FIRST_NAME, "Frédéric",
                                                    FaxToMailUser.PROPERTY_LOGIN, "fviala",
                                                    FaxToMailUser.PROPERTY_TRIGRAPH, "FVI",
                                                    FaxToMailUser.PROPERTY_USER_GROUPS, Collections.singletonList(commerciauxGroup));
            FaxToMailUser claire = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Marquis",
                                                  FaxToMailUser.PROPERTY_FIRST_NAME, "Claire",
                                                  FaxToMailUser.PROPERTY_LOGIN, "cmarquis",
                                                  FaxToMailUser.PROPERTY_TRIGRAPH, "CMA");
            FaxToMailUser agathe = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Borde",
                                                  FaxToMailUser.PROPERTY_FIRST_NAME, "Agathe",
                                                  FaxToMailUser.PROPERTY_LOGIN, "aborde",
                                                  FaxToMailUser.PROPERTY_TRIGRAPH, "ABO",
                                                  FaxToMailUser.PROPERTY_USER_GROUPS, Collections.singletonList(savGroup));
            FaxToMailUser jeanne = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Bourgoin",
                                                  FaxToMailUser.PROPERTY_FIRST_NAME, "Jeanne",
                                                  FaxToMailUser.PROPERTY_LOGIN, "jbourgoin",
                                                  FaxToMailUser.PROPERTY_TRIGRAPH, "JBO",
                                                  FaxToMailUser.PROPERTY_USER_GROUPS, Collections.singletonList(commerciauxGroup));

            MailFolderTopiaDao folderDao = getPersistenceContext().getMailFolderDao();
            Map<String, MailFolder> folders = new HashMap<>();

            // fixme seed to be used in test
            Random random = new Random(1234);
            EmailTopiaDao emailDao = getPersistenceContext().getEmailDao();
            OriginalEmailTopiaDao originalEmailDao = getPersistenceContext().getOriginalEmailDao();
            HistoryTopiaDao historyDao = getPersistenceContext().getHistoryDao();
            RangeRowTopiaDao rangeRowDao = getPersistenceContext().getRangeRowDao();

            int waitingStateSize = waitingStates.size();

            // root folders (~companies)
            MailFolder franciaflex = folderDao.create(MailFolder.PROPERTY_NAME, "Franciaflex",
                                                      MailFolder.PROPERTY_USE_CURRENT_LEVEL_COMPANY, true,
                                                      MailFolder.PROPERTY_COMPANY, "FX",
                                                      MailFolder.PROPERTY_USE_CURRENT_LEVEL_REJECT_RESPONSE_MAIL_ADDRESS, true,
                                                      MailFolder.PROPERTY_USE_CURRENT_LEVEL_REJECT_RESPONSE_MESSAGE, true);
            //folders never used
            folderDao.create(MailFolder.PROPERTY_NAME, "Faber");
            folderDao.create(MailFolder.PROPERTY_NAME, "France Fermetures",
                                                          MailFolder.PROPERTY_USE_CURRENT_LEVEL_COMPANY, true,
                                                          MailFolder.PROPERTY_COMPANY, "FF");

            // categories
            MailFolder chargesClientelle = folderDao.create(MailFolder.PROPERTY_NAME, "Chargés de clientèle",
                                                            MailFolder.PROPERTY_PARENT, franciaflex,
                                                            MailFolder.PROPERTY_REPLY_ADDRESSES, Lists.newArrayList("no-reply@franciaflex.com"));
            franciaflex.addChildren(chargesClientelle);
            folders.put("Chargés de clientèle", chargesClientelle);

            Collections.shuffle(waitingStates);
            MailFolder sav = folderDao.create(MailFolder.PROPERTY_NAME, "SAV",
                                              MailFolder.PROPERTY_PARENT, franciaflex,
                                              MailFolder.PROPERTY_WAITING_STATES, waitingStates.subList(0, random.nextInt(waitingStateSize - 8) + 2),
                                              MailFolder.PROPERTY_USE_CURRENT_LEVEL_WAITING_STATE, true);
            franciaflex.addChildren(sav);
            folders.put("SAV", sav);

            // user folders
            Collections.shuffle(waitingStates);
            MailFolder cyrilFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Cyril",
                                                      MailFolder.PROPERTY_PARENT, chargesClientelle,
                                                      MailFolder.PROPERTY_WAITING_STATES, waitingStates.subList(0, random.nextInt(waitingStateSize - 8) + 2),
                                                      MailFolder.PROPERTY_USE_CURRENT_LEVEL_WAITING_STATE, true);
            chargesClientelle.addChildren(cyrilFolder);
            folders.put("Cyril", cyrilFolder);

            MailFolder claireFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Claire",
                                                       MailFolder.PROPERTY_PARENT, chargesClientelle);
            chargesClientelle.addChildren(claireFolder);
            folders.put("Claire", claireFolder);

            Collections.shuffle(waitingStates);
            MailFolder agatheFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Agathe",
                                                       MailFolder.PROPERTY_PARENT, chargesClientelle,
                                                       MailFolder.PROPERTY_WAITING_STATES, waitingStates.subList(0, random.nextInt(waitingStateSize - 8) + 2),
                                                       MailFolder.PROPERTY_USE_CURRENT_LEVEL_WAITING_STATE, true);
            chargesClientelle.addChildren(agatheFolder);
            folders.put("Agathe", agatheFolder);

            MailFolder marcFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Marc",
                                                     MailFolder.PROPERTY_PARENT, sav);
            sav.addChildren(marcFolder);
            folders.put("Marc", marcFolder);

            MailFolder fredericFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Frédéric",
                                                         MailFolder.PROPERTY_PARENT, sav);
            sav.addChildren(fredericFolder);
            folders.put("Frédéric", fredericFolder);

            MailFolder jeanneFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Jeanne",
                                                       MailFolder.PROPERTY_PARENT, sav);
            sav.addChildren(jeanneFolder);
            folders.put("Jeanne", jeanneFolder);

            // email accounts
            File demoDirectory = getApplicationConfig().getDemoDirectory();
            File emailAcountFile = new File(demoDirectory, "email_accounts.csv");
            try (InputStream emailAccountsPropertiesStream = new FileInputStream(emailAcountFile)){
                getReferentielService().importEmailAccounts(emailAccountsPropertiesStream);
            } catch(Exception e) {
                log.error("error getting the email accounts", e);
            }

            // email filters
            File emailFilterFile = new File(demoDirectory, "email_filters.csv");
            try (InputStream emailFiltersPropertiesStream = new FileInputStream(emailFilterFile)){
                getReferentielService().importEmailFilters(emailFiltersPropertiesStream, folders);
            } catch(Exception e) {
                log.error("error getting the email filters", e);
            }

            // clients and emails
            File clientFile = new File(demoDirectory, "fx_clients.csv");
            try (InputStream fxClientsStream = new FileInputStream(clientFile)){
                List<Client> clients = getReferentielService().importClients(fxClientsStream);

                createEmails(random, historyDao, rangeRowDao, emailDao, originalEmailDao, ranges, clients, types, priorities, cyrilFolder, cyril);
                createEmails(random, historyDao, rangeRowDao, emailDao, originalEmailDao, ranges, clients, types, priorities, claireFolder, claire);
                createEmails(random, historyDao, rangeRowDao, emailDao, originalEmailDao, ranges, clients, types, priorities, agatheFolder, agathe);
                createEmails(random, historyDao, rangeRowDao, emailDao, originalEmailDao, ranges, clients, types, priorities, marcFolder, marc);
                createEmails(random, historyDao, rangeRowDao, emailDao, originalEmailDao, ranges, clients, types, priorities, fredericFolder, frederic);
                createEmails(random, historyDao, rangeRowDao, emailDao, originalEmailDao, ranges, clients, types, priorities, jeanneFolder, jeanne);
            } catch(Exception e) {
                log.error("error getting the client file", e);
            }

            getPersistenceContext().commit();
        }
    }

    protected void createEmails(Random random, HistoryTopiaDao historyDao, RangeRowTopiaDao rangeRowDao,
                                EmailTopiaDao emailDao, OriginalEmailTopiaDao originalEmailDao,
                                List<Range> ranges, List<Client> clients, List<DemandType> types, List<Priority> priorities,
                                MailFolder folder, FaxToMailUser user) throws InvalidClientException {
        int r = random.nextInt(100) + 50;
        for (int i = 0 ; i < r ; i++) {
            createEmail(random, historyDao, rangeRowDao, emailDao, originalEmailDao, ranges, clients, types, priorities, folder, user);
        }
    }

    protected void createEmail(Random random,
                               HistoryTopiaDao historyDao,
                               RangeRowTopiaDao rangeRowDao,
                               EmailTopiaDao emailDao,
                               OriginalEmailTopiaDao originalEmailDao,
                               List<Range> ranges,
                               List<Client> clients,
                               List<DemandType> types,
                               List<Priority> priorities,
                               MailFolder folder,
                               FaxToMailUser user) throws InvalidClientException {

        List<RangeRow> rangeRows = new ArrayList<>();

        Client client = clients.get(random.nextInt(clients.size()));
        boolean fax = false;
        List<String> senders = client.getEmailAddresses();
        List<String> faxNumbers = client.getFaxNumbers();
        if (CollectionUtils.isEmpty(senders) && CollectionUtils.isNotEmpty(faxNumbers)) {
            senders = Collections.singletonList(client.getFaxNumbers().get(0));
            fax = true;
        }
        String sender = null;
        if (CollectionUtils.isNotEmpty(senders)) {
            sender = senders.get(0);
        }
        String faxNumber = null;
        if (CollectionUtils.isNotEmpty(faxNumbers)) {
            faxNumber = faxNumbers.get(0);
        }

        boolean opened = random.nextBoolean();

        Decorator<Date> dateDecorator = getDecoratorService().getDecoratorByType(Date.class, DecoratorService.DATE);

        Date now = new Date();
        History history = historyDao.create(History.PROPERTY_TYPE, HistoryType.CREATION,
                                            History.PROPERTY_MODIFICATION_DATE, now);
        history.setFields(Sets.newHashSet(Email.PROPERTY_SENDER,
                Email.PROPERTY_CLIENT,
                Email.PROPERTY_PROJECT_REFERENCE,
                Email.PROPERTY_OBJECT,
                Email.PROPERTY_RECEPTION_DATE,
                Email.PROPERTY_MAIL_FOLDER,
                Email.PROPERTY_DEMAND_STATUS));
        String projectRef = t("faxtomail.email.projectReference.default", dateDecorator.toString(now));

        Email email = emailDao.create(Email.PROPERTY_SENDER, sender,
                                      Email.PROPERTY_FAX, fax,
                                      Email.PROPERTY_CLIENT, client,
                                      Email.PROPERTY_PROJECT_REFERENCE, projectRef,
                                      Email.PROPERTY_OBJECT, client.getCaracteristic1() + " / " + client.getCode() + " / " + client.getName() + " / " + faxNumber + " / " + dateDecorator.toString(now),
                                      Email.PROPERTY_RECEPTION_DATE, now,
                                      Email.PROPERTY_MAIL_FOLDER, folder,
                                      Email.PROPERTY_DEMAND_STATUS, opened ? DemandStatus.IN_PROGRESS : DemandStatus.UNTREATED,
                                      Email.PROPERTY_HISTORY, Lists.newArrayList(history),
                                      Email.PROPERTY_ORIGINAL_EMAIL, originalEmailDao.create(OriginalEmail.PROPERTY_CONTENT, "Demo data"));


        if (opened) {

            getEmailService().addToHistory(email.getTopiaId(), HistoryType.OPENING, user, now);
            DemandType demandType = types.get(random.nextInt(types.size()));
            if (FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.RANGE_ROW)) {
                for (int j = 0; j < random.nextInt(4) + 1; j++) {
                    RangeRow rangeRow = rangeRowDao.create(RangeRow.PROPERTY_RANGE, ranges.get(random.nextInt(ranges.size())),
                                                           RangeRow.PROPERTY_COMMAND_NUMBER, RandomStringUtils.randomNumeric(6),
                                                           RangeRow.PROPERTY_PRODUCT_QUANTITY, random.nextInt(100),
                                                           RangeRow.PROPERTY_SAV_QUANTITY, random.nextInt(100));
                    rangeRows.add(rangeRow);
                }
                email.setRangeRow(rangeRows);
            }

            email.setDemandType(demandType);
            email.setPriority(priorities.get(random.nextInt(priorities.size())));
            email.setRangeRow(rangeRows);
            getEmailService().saveEmail(email, user,
                    Email.PROPERTY_DEMAND_TYPE, Email.PROPERTY_PRIORITY, Email.PROPERTY_RANGE_ROW);
        }

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date yesterday = cal.getTime();
        history = historyDao.create(History.PROPERTY_TYPE, HistoryType.CREATION,
                                    History.PROPERTY_MODIFICATION_DATE, yesterday);
        history.setFields(Sets.newHashSet(Email.PROPERTY_SENDER,
                Email.PROPERTY_CLIENT,
                Email.PROPERTY_PROJECT_REFERENCE,
                Email.PROPERTY_OBJECT,
                Email.PROPERTY_RECEPTION_DATE,
                Email.PROPERTY_MAIL_FOLDER,
                Email.PROPERTY_DEMAND_STATUS));
        projectRef = t("faxtomail.email.projectReference.default", dateDecorator.toString(yesterday));

        email = emailDao.create(Email.PROPERTY_SENDER, sender,
                                Email.PROPERTY_FAX, fax,
                                Email.PROPERTY_CLIENT, client,
                                Email.PROPERTY_PROJECT_REFERENCE, projectRef,
                                Email.PROPERTY_OBJECT, client.getCaracteristic1() + " / " + client.getCode() + " / " + client.getName() + " / " + faxNumber + " / " + dateDecorator.toString(yesterday),
                                Email.PROPERTY_RECEPTION_DATE, yesterday,
                                Email.PROPERTY_MAIL_FOLDER, folder,
                                Email.PROPERTY_DEMAND_STATUS, opened ? DemandStatus.IN_PROGRESS : DemandStatus.UNTREATED,
                                Email.PROPERTY_HISTORY, Lists.newArrayList(history),
                                Email.PROPERTY_ORIGINAL_EMAIL, originalEmailDao.create(OriginalEmail.PROPERTY_CONTENT, "Demo data"));


        if (opened) {
            getEmailService().addToHistory(email.getTopiaId(), HistoryType.OPENING, user, now);
            DemandType demandType = types.get(random.nextInt(types.size()));
            if (FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.RANGE_ROW)) {
                for (int j = 0; j < random.nextInt(4) + 1; j++) {
                    RangeRow rangeRow = rangeRowDao.create(RangeRow.PROPERTY_RANGE, ranges.get(random.nextInt(ranges.size())),
                                                           RangeRow.PROPERTY_COMMAND_NUMBER, RandomStringUtils.randomNumeric(6),
                                                           RangeRow.PROPERTY_PRODUCT_QUANTITY, random.nextInt(100),
                                                           RangeRow.PROPERTY_SAV_QUANTITY, random.nextInt(100));
                    rangeRows.add(rangeRow);
                }
                email.setRangeRow(rangeRows);

                if (random.nextBoolean()) {
                    email.setEdiError("error 42");
                }
            }
            email.setDemandType(demandType);
            email.setPriority(priorities.get(random.nextInt(priorities.size())));
            email.setRangeRow(rangeRows);
            getEmailService().saveEmail(email, null, null, user,
                    Email.PROPERTY_DEMAND_TYPE, Email.PROPERTY_PRIORITY, Email.PROPERTY_RANGE_ROW);
        }
    }

}
