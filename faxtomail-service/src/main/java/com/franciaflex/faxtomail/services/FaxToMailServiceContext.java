package com.franciaflex.faxtomail.services;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaApplicationContext;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaPersistenceContext;
import com.franciaflex.faxtomail.services.service.ClientService;
import com.franciaflex.faxtomail.services.service.ConfigurationService;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.services.service.InitFaxToMailService;
import com.franciaflex.faxtomail.services.service.LdapService;
import com.franciaflex.faxtomail.services.service.LdapServiceImpl;
import com.franciaflex.faxtomail.services.service.MailFolderService;
import com.franciaflex.faxtomail.services.service.ReferentielService;
import com.franciaflex.faxtomail.services.service.UserService;
import com.franciaflex.faxtomail.services.service.ValidationService;
import com.franciaflex.faxtomail.services.service.ldap.LdapServiceMock;
import com.google.common.base.Preconditions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.util.Date;

public class FaxToMailServiceContext implements Closeable {

    private static final Log log = LogFactory.getLog(FaxToMailServiceContext.class);

    protected FaxToMailServiceContext delegateServiceContext;

    protected FaxToMailConfiguration applicationConfig;

    protected FaxToMailTopiaApplicationContext topiaApplicationContext;

    protected FaxToMailTopiaPersistenceContext persistenceContext;

    protected FaxToMailServiceFactory serviceFactory;

    public static FaxToMailServiceContext newServiceContext(FaxToMailTopiaApplicationContext applicationContext) {
        return new FaxToMailServiceContext(applicationContext, new FaxToMailProxiedServiceFactory());
    }

    public static FaxToMailServiceContext newDirectServiceContext(FaxToMailTopiaApplicationContext applicationContext) {
        return new FaxToMailServiceContext(applicationContext, new FaxToMailDefaultServiceFactory());
    }
    
    public static FaxToMailServiceContext newDirectServiceContext(FaxToMailTopiaPersistenceContext persistenceContext) {
        return new FaxToMailServiceContext(persistenceContext, new FaxToMailDefaultServiceFactory());
    }

    private FaxToMailServiceContext(FaxToMailTopiaApplicationContext topiaApplicationContext, FaxToMailServiceFactory serviceFactory) {
        Preconditions.checkNotNull(topiaApplicationContext, "Cannot create an ServiceContext without an ApplicationContext");
        Preconditions.checkNotNull(serviceFactory, "Cannot create an ServiceContext without a ServiceFactory");
        this.topiaApplicationContext = topiaApplicationContext;
        this.serviceFactory = serviceFactory;
        this.serviceFactory.setServiceContext(this);
    }

    private FaxToMailServiceContext(FaxToMailTopiaPersistenceContext persistenceContext, FaxToMailServiceFactory serviceFactory) {
        this.persistenceContext = persistenceContext;
        this.serviceFactory = serviceFactory;
        this.serviceFactory.setServiceContext(this);
    }

    private FaxToMailServiceContext(FaxToMailServiceContext delegateServiceContext, FaxToMailServiceFactory serviceFactory) {
        this.delegateServiceContext = delegateServiceContext;
        this.serviceFactory = serviceFactory;
        this.serviceFactory.setServiceContext(this);
    }

    public FaxToMailConfiguration getApplicationConfig() {
        if (delegateServiceContext != null) {
            return delegateServiceContext.getApplicationConfig();
        } else {
            return applicationConfig;
        }
    }

    public FaxToMailTopiaPersistenceContext getPersistenceContext() {
        if (delegateServiceContext != null) {
            return delegateServiceContext.getPersistenceContext();
        } else {
            if (persistenceContext == null) {
                persistenceContext = topiaApplicationContext.newPersistenceContext();
            }
            return persistenceContext;
        }
    }

    public void setApplicationConfig(FaxToMailConfiguration applicationConfig) {
        if (delegateServiceContext != null) {
            delegateServiceContext.setApplicationConfig(applicationConfig);
        } else {
            this.applicationConfig = applicationConfig;
        }
    }

    public <E extends FaxToMailService> E newService(Class<E> serviceInterfaceClass) {
        return serviceFactory.newService(serviceInterfaceClass);
    }

    public static class FaxToMailDefaultServiceFactory implements FaxToMailServiceFactory {

        protected FaxToMailServiceContext serviceContext;

        @Override
        public void setServiceContext(FaxToMailServiceContext serviceContext) {
            this.serviceContext = serviceContext;
        }

        @Override
        public <E extends FaxToMailService> E newService(Class<E> serviceInterfaceClass) {

            E service;
            try {
                Class<E> serviceImplClass = (Class<E>) Class.forName(serviceInterfaceClass.getCanonicalName() + "Impl");

                // special case for mock services
                if (serviceImplClass.equals(LdapServiceImpl.class) && serviceContext.getApplicationConfig().isLdapMock()) {
                    serviceImplClass = (Class<E>) LdapServiceMock.class;
                }

                Constructor<E> constructor = serviceImplClass.getConstructor();
                service = constructor.newInstance();

            } catch (NoSuchMethodException e) {
                throw new ApplicationTechnicalException("all services must provide a non-argument constructor", e);
            } catch (InvocationTargetException|InstantiationException|IllegalAccessException e) {
                throw new ApplicationTechnicalException("unable to instantiate service", e);
            } catch (ClassNotFoundException e) {
                throw new ApplicationTechnicalException("unable to find the implementation of the service", e);
            }

            service.setServiceContext(serviceContext);

            return service;

        }
    }

    public static class FaxToMailProxiedServiceFactory implements FaxToMailServiceFactory {

        protected FaxToMailDefaultServiceFactory defaultServiceFactory;

        protected FaxToMailServiceContext subServiceContext;

        protected FaxToMailServiceContext serviceContext;

        @Override
        public void setServiceContext(FaxToMailServiceContext serviceContext) {
            this.serviceContext = serviceContext;
        }

        @Override
        public <E extends FaxToMailService> E newService(Class<E> serviceInterfaceClass) {

            if (defaultServiceFactory == null) {
                defaultServiceFactory = new FaxToMailDefaultServiceFactory();
                subServiceContext = new FaxToMailServiceContext(serviceContext, defaultServiceFactory);
                defaultServiceFactory.setServiceContext(subServiceContext);
            }

            E realService = defaultServiceFactory.newService(serviceInterfaceClass);

            E service = (E) Proxy.newProxyInstance(serviceInterfaceClass.getClassLoader(), new Class[]{serviceInterfaceClass},
                    new FaxToMailServiceInvocationHandler(realService, serviceContext));

            return service;

        }
    }

    public Date getNow() {
        Date now = new Date();
        return now;
    }

    public DecoratorService getDecoratorService() {
        return newService(DecoratorService.class);
    }

    public ValidationService getValidationService() {
        return newService(ValidationService.class);
    }

    public MailFolderService getMailFolderService() {
        return newService(MailFolderService.class);
    }

    public UserService getUserService() {
        return newService(UserService.class);
    }

    public LdapService getLdapService() {
        return newService(LdapService.class);
    }

    public EmailService getEmailService() {
        return newService(EmailService.class);
    }

    public ReferentielService getReferentielService() {
        return newService(ReferentielService.class);
    }

    public ConfigurationService getConfigurationService() {
        return newService(ConfigurationService.class);
    }
    
    public InitFaxToMailService getInitFaxToMailService() {
        return newService(InitFaxToMailService.class);
    }

    public ClientService getClientService() {
        return newService(ClientService.class);
    }
    
    @Override
    public void close() throws IOException {
        if (persistenceContext != null) {

            if (Boolean.getBoolean("hibernate.generate_statistics") && log.isInfoEnabled()) {
                log.info(persistenceContext.getHibernateSupport().getHibernateFactory().getStatistics());
            }

            persistenceContext.close();
            persistenceContext = null;
        }
    }
}
