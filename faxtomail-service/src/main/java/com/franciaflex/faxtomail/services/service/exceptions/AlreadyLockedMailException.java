package com.franciaflex.faxtomail.services.service.exceptions;

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Exception throw when trying to define a lock on en email already locked by another user.
 * 
 * @author Eric Chatellier
 */
public class AlreadyLockedMailException extends Exception {

    /** serialVersionUID. */
    private static final long serialVersionUID = 7090578827422040229L;

    protected FaxToMailUser lockedBy;

    protected Email email;

    public AlreadyLockedMailException(String message, FaxToMailUser lockedBy, Email email) {
        super(message);
        this.lockedBy = lockedBy;
        this.email = email;
    }

    public FaxToMailUser getLockedBy() {
        return lockedBy;
    }

    public Email getEmail() {
        return email;
    }
}
