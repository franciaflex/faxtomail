package com.franciaflex.faxtomail.services.service.migration;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.flywaydb.core.Flyway;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Environment;
import org.nuiton.topia.flyway.TopiaFlywayServiceImpl;
import org.nuiton.topia.persistence.TopiaApplicationContext;

/**
 * Surcharge du service de migration flayway par default pour pouvoir définir des scripts pour
 * plusieurs type de base de données.
 * 
 * @author Eric Chatellier
 */
public class FaxtomailFlywayMigrationService extends TopiaFlywayServiceImpl {

    @Override
    protected void setLocations(Flyway flyway, TopiaApplicationContext topiaApplicationContext) {

        String specificDirectory;
        String dialect = (String)topiaApplicationContext.getConfiguration().get(AvailableSettings.DIALECT);
        if (StringUtils.startsWith(dialect, "org.hibernate.dialect.SQLServer")) {
            specificDirectory = "db/migration/sqlserver";
        } else if (StringUtils.startsWith(dialect, "org.hibernate.dialect.PostgreSQL")) {
            specificDirectory = "db/migration/postgres";
        } else {
            specificDirectory = "db/migration/h2";
        }

        flyway.setLocations(specificDirectory);
    }

}
