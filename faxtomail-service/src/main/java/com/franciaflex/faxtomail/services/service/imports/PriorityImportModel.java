package com.franciaflex.faxtomail.services.service.imports;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Priority;
import com.franciaflex.faxtomail.persistence.entities.PriorityImpl;
import org.nuiton.csv.ext.AbstractImportModel;

/**
 * Modèle d'import des etat d'attente.
 */
public class PriorityImportModel extends AbstractImportModel<Priority> {

    public PriorityImportModel(char separator) {
        super(separator);

        newMandatoryColumn("priorite", Priority.PROPERTY_LABEL);
    }

    @Override
    public Priority newEmptyInstance() {
        return new PriorityImpl();
    }
}
