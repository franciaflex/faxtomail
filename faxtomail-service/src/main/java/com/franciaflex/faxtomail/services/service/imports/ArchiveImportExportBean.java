package com.franciaflex.faxtomail.services.service.imports;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.EmailImpl;

/**
 * Bean model d'import des archives. (très proche de l'entité
 * {@link com.franciaflex.faxtomail.persistence.entities.Email}).
 * 
 * @author Eric Chatellier
 */
public class ArchiveImportExportBean extends EmailImpl {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1498046445018851410L;

    protected static final String PROPERTY_MAIL_FOLDER_PATHS = "mailFolderPath";
    protected static final String PROPERTY_ATTACHMENT_PATHS = "attachmentPaths";
    protected static final String PROPERTY_CLIENT_CODE = "clientCode";
    protected static final String PROPERTY_CLIENT_BRAND = "clientBrand";
    protected static final String PROPERTY_ERROR = "error";
    protected static final String PROPERTY_ORIGINAL_EMAIL_CONTENT = "originalEmailContent";

    protected String mailFolderPath;

    protected String attachmentPaths;

    protected String clientCode;
    
    protected String clientBrand;

    protected String error;

    protected String originalEmailContent;

    public String getMailFolderPath() {
        return mailFolderPath;
    }

    public void setMailFolderPath(String mailFolderPath) {
        this.mailFolderPath = mailFolderPath;
    }

    public String getAttachmentPaths() {
        return attachmentPaths;
    }

    public void setAttachmentPaths(String attachmentPaths) {
        this.attachmentPaths = attachmentPaths;
    }
    
    public String getClientCode() {
        return clientCode;
    }
    
    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }
    
    public String getClientBrand() {
        return clientBrand;
    }
    
    public void setClientBrand(String clientBrand) {
        this.clientBrand = clientBrand;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getOriginalEmailContent() {
        return originalEmailContent;
    }

    public void setOriginalEmailContent(String originalEmailContent) {
        this.originalEmailContent = originalEmailContent;
    }
}
