package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.BrandsForDomain;
import com.franciaflex.faxtomail.persistence.entities.BrandsForDomainImpl;
import com.franciaflex.faxtomail.persistence.entities.BrandsForDomainTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.Configuration;
import com.franciaflex.faxtomail.persistence.entities.ConfigurationImpl;
import com.franciaflex.faxtomail.persistence.entities.ConfigurationTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.DemandTypeImpl;
import com.franciaflex.faxtomail.persistence.entities.DemandTypeTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.EmailAccount;
import com.franciaflex.faxtomail.persistence.entities.EmailAccountImpl;
import com.franciaflex.faxtomail.persistence.entities.EmailAccountTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.EmailProtocol;
import com.franciaflex.faxtomail.persistence.entities.ExtensionCommand;
import com.franciaflex.faxtomail.persistence.entities.ExtensionCommandTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroup;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.GroupChef;
import com.franciaflex.faxtomail.persistence.entities.GroupChefTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.MailFilter;
import com.franciaflex.faxtomail.persistence.entities.MailFilterTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.MailFolderImpl;
import com.franciaflex.faxtomail.persistence.entities.MailFolderTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.SigningForDomain;
import com.franciaflex.faxtomail.persistence.entities.SigningForDomainImpl;
import com.franciaflex.faxtomail.persistence.entities.SigningForDomainTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.Stamp;
import com.franciaflex.faxtomail.persistence.entities.StampImpl;
import com.franciaflex.faxtomail.persistence.entities.StampTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.persistence.entities.WaitingStateImpl;
import com.franciaflex.faxtomail.persistence.entities.WaitingStateTopiaDao;
import com.franciaflex.faxtomail.services.FaxToMailServiceSupport;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author kmorin - kmorin@codelutin.com
 */
public class ConfigurationServiceImpl extends FaxToMailServiceSupport implements ConfigurationService {

    private static final Log log = LogFactory.getLog(ConfigurationServiceImpl.class);


    protected static final Function<MailFilter, String> GET_EXPRESSION = new Function<MailFilter, String>() {
        @Override
        public String apply(MailFilter input) {
            return input == null ? null : input.getExpression();
        }
    };

    /**
     * Return unique database configuration.
     * 
     * @return configuration (not {@code null}, created on not found in database)
     */
    @Override
    public Configuration getConfiguration() {
        ConfigurationTopiaDao dao = getPersistenceContext().getConfigurationDao();
        Configuration config = dao.forAll().findUniqueOrNull();
        if (config == null) {
            config = new ConfigurationImpl();
        }
        return config;
    }

    /**
     * Get all mail filter ordered by position.
     * 
     * @return all mail filter
     */
    @Override
    public List<MailFilter> getMailFilters() {
        MailFilterTopiaDao mailFilterDao = getPersistenceContext().getMailFilterDao();
        List<MailFilter> result = mailFilterDao.forAll().setOrderByArguments(MailFilter.PROPERTY_POSITION + " asc").findAll();
        return result;
    }

    /**
     * Save configuration.
     * 
     * @param config configuration to save
     */
    protected void saveConfiguration(Configuration config) {
        ConfigurationTopiaDao dao = getPersistenceContext().getConfigurationDao();
        if (config.isPersisted()) {
            dao.update(config);
        } else {
            dao.create(config);
        }
    }

    /**
     * Sauve l'ensemble de la configuration.
     * 
     * @param configuration configuration
     * @param demandTypes types de demande
     * @param stamps tampons
     * @param waitingStates etat d'attentes
     * @param mailFolders mail folder
     * @param mailFilters mail filters
     * @param emailAccounts mail accounts
     */
    @Override
    public void save(Configuration configuration,
                     List<DemandType> demandTypes,
                     List<Stamp> stamps,
                     List<WaitingState> waitingStates,
                     List<MailFolder> mailFolders,
                     List<MailFilter> mailFilters,
                     List<EmailAccount> emailAccounts,
                     List<BrandsForDomain> brandsForDomains,
                     List<SigningForDomain> signingForDomains) {

        Collection<MailFolder> toDeleteMailFolder = new ArrayList<>();

        // sauvegarde
        saveConfiguration(configuration);
        saveDemandTypes(demandTypes);
        saveStamps(stamps);
        Map<String, WaitingState> waitingStateCache = saveWaitingStates(waitingStates);
        Map<String, MailFolder> mailFolderCache = saveMailFolders(waitingStateCache, mailFolders, toDeleteMailFolder);
        saveMailFilters(mailFolderCache, mailFilters);
        saveEmailAccounts(emailAccounts);
        saveBrandsForDomains(brandsForDomains);
        saveSigningsForDomains(signingForDomains);

        // delete after loop
        MailFolderTopiaDao mailFilterDao = getPersistenceContext().getMailFolderDao();
        mailFilterDao.deleteAll(toDeleteMailFolder);

        // unique commit pour toute la conf
        getPersistenceContext().commit();
    }

    protected void saveDemandTypes(List<DemandType> demandTypes) {
        Binder<DemandType, DemandType> binderDemandType = BinderFactory.newBinder(DemandType.class);
        DemandTypeTopiaDao demandTypeDAO = getPersistenceContext().getDemandTypeDao();

        List<DemandType> allDemandType = demandTypeDAO.findAll();
        Map<String, DemandType> allDemandTypeIndex = new HashMap<>(Maps.uniqueIndex(allDemandType, TopiaEntities.getTopiaIdFunction()));
        for (DemandType demandType : demandTypes) {
            // get current etat attente
            DemandType currentDemandType;
            if (StringUtils.isBlank(demandType.getTopiaId()) || demandType.getTopiaId().startsWith("new_")) {
                currentDemandType = new DemandTypeImpl();
            } else {
                currentDemandType = allDemandTypeIndex.remove(demandType.getTopiaId());
            }
            
            // copy
            binderDemandType.copyExcluding(demandType, currentDemandType,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION);

            // persist
            if (currentDemandType.isPersisted()) {
                demandTypeDAO.update(currentDemandType);
            } else {
                demandTypeDAO.create(currentDemandType);
            }
        }
    }

    protected void saveStamps(List<Stamp> stamps) {
        Binder<Stamp, Stamp> binderStamp = BinderFactory.newBinder(Stamp.class);
        StampTopiaDao stampDao = getPersistenceContext().getStampDao();

        List<Stamp> allStamps = stampDao.findAll();
        Map<String, Stamp> allStampsIndex = new HashMap<>(Maps.uniqueIndex(allStamps, TopiaEntities.getTopiaIdFunction()));
        for (Stamp stamp : stamps) {
            // get current etat attente
            Stamp currentStamp;
            if (StringUtils.isBlank(stamp.getTopiaId()) || stamp.getTopiaId().startsWith("new_")) {
                currentStamp = new StampImpl();

            } else {
                currentStamp = allStampsIndex.remove(stamp.getTopiaId());
            }

            // copy
            binderStamp.copyExcluding(stamp, currentStamp,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION);

            // persist
            if (currentStamp.isPersisted()) {
                stampDao.update(currentStamp);
            } else {
                stampDao.create(currentStamp);
            }
        }

        // delete remaining (not done here, done after reference cleaning)
        stampDao.deleteAll(allStampsIndex.values());
    }

    /**
     * 
     * @param waitingStates
     */
    protected Map<String, WaitingState> saveWaitingStates(Collection<WaitingState> waitingStates) {

        Map<String, WaitingState> cache = new HashMap<>();
        Binder<WaitingState, WaitingState> binderEtatAttente = BinderFactory.newBinder(WaitingState.class);
        WaitingStateTopiaDao waitingStateDAO = getPersistenceContext().getWaitingStateDao();

        List<WaitingState> allWaitingStates = waitingStateDAO.findAll();
        Map<String, WaitingState> allWaitingStateIndex = new HashMap<>(Maps.uniqueIndex(allWaitingStates, TopiaEntities.getTopiaIdFunction()));
        for (WaitingState waitingState : waitingStates) {
            // get current etat attente
            WaitingState currentWaitingState;
            if (StringUtils.isBlank(waitingState.getTopiaId()) || waitingState.getTopiaId().startsWith("new_")) {
                currentWaitingState = new WaitingStateImpl();
            } else {
                currentWaitingState = allWaitingStateIndex.remove(waitingState.getTopiaId());
            }
            
            // copy
            binderEtatAttente.copyExcluding(waitingState, currentWaitingState,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION);

            // persist
            if (currentWaitingState.isPersisted()) {
                waitingStateDAO.update(currentWaitingState);
            } else {
                waitingStateDAO.create(currentWaitingState);
            }
            
            // cache
            cache.put(waitingState.getTopiaId(), currentWaitingState);
        }

        // delete remaining (not done here, done after reference cleaning)
        waitingStateDAO.deleteAll(allWaitingStateIndex.values());

        return cache;
    }

    /**
     * Save mail folder without commit.
     * 
     * @param waitingStateCache cache etat attente
     * @param newMailFolders mail folders
     * @return mail folder cache with input ids
     */
    protected Map<String, MailFolder> saveMailFolders(Map<String, WaitingState> waitingStateCache, Collection<MailFolder> newMailFolders, Collection<MailFolder> toDeleteMailFolder) {
        // get current folders
        MailFolderTopiaDao dao = getPersistenceContext().getMailFolderDao();
        List<MailFolder> mailFolders = dao.findAll();
        Map<String, MailFolder> mailFolderMap = new HashMap<>(Maps.uniqueIndex(mailFolders, TopiaEntities.getTopiaIdFunction()));

        // recursive update
        Map<String, MailFolder> mailFolderCache = new HashMap<>();
        saveMailFolders(dao, waitingStateCache, mailFolderMap, null, newMailFolders, mailFolderCache);

        // if map is not empty after recursive iteration, remaining folder must be deleted
        toDeleteMailFolder.addAll(mailFolderMap.values());
        
        return mailFolderCache;
    }

    /**
     * Save mail folder without commit.
     * 
     * @param dao
     * @param waitingStateCache
     * @param mailFolderMap
     * @param parent
     * @param mailFolders
     * @param mailFolderCache
     * @return
     */
    protected Collection<MailFolder> saveMailFolders(MailFolderTopiaDao dao, Map<String, WaitingState> waitingStateCache,
            Map<String, MailFolder> mailFolderMap, MailFolder parent, Collection<MailFolder> mailFolders, Map<String, MailFolder> mailFolderCache) {
        
        Collection<MailFolder> result = Lists.newArrayList();
        if (mailFolders == null) {
            return result;
        }

        Binder<MailFolder, MailFolder> binderMailFolder = BinderFactory.newBinder(MailFolder.class);
        for (MailFolder mailFolder : mailFolders) {

            MailFolder currentMailFolder;
            if (StringUtils.isBlank(mailFolder.getTopiaId()) || mailFolder.getTopiaId().startsWith("new_")) {
                currentMailFolder = new MailFolderImpl();
            } else {
                currentMailFolder = mailFolderMap.remove(mailFolder.getTopiaId());
            }
            
            binderMailFolder.copyExcluding(mailFolder, currentMailFolder,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    MailFolder.PROPERTY_CHILDREN,
                    MailFolder.PROPERTY_WAITING_STATES,
                    MailFolder.PROPERTY_PARENT);

            // gestion des bonnes références des états d'attente (surtout le cas "new_")
            currentMailFolder.clearWaitingStates();
            if (mailFolder.getWaitingStates() != null) {
                for (WaitingState waitingState : mailFolder.getWaitingStates()) {
                    // etatAttente.getTopiaId() peut retourner un id qui commence par new_
                    currentMailFolder.addWaitingStates(waitingStateCache.get(waitingState.getTopiaId()));
                }
            }
            
            currentMailFolder.setParent(parent);

            // set the parent's company if it is inherited
            if (!currentMailFolder.isUseCurrentLevelCompany() && parent != null) {
                currentMailFolder.setCompany(parent.getCompany());
            }

            if (!currentMailFolder.isPersisted()) {
                currentMailFolder = dao.create(currentMailFolder);
            }

            Collection<MailFolder> children = saveMailFolders(dao, waitingStateCache, mailFolderMap,
                    currentMailFolder, mailFolder.getChildren(), mailFolderCache);
            currentMailFolder.setChildren(children);
            dao.update(currentMailFolder);

            result.add(currentMailFolder);
            
            // add in cache
            mailFolderCache.put(mailFolder.getTopiaId(), currentMailFolder);
        }

        return result;
    }

    /**
     * Save all mail filters without commit.
     * 
     * @param mailFolderCache mail folder cache (with corrects ids)
     * @param mailFilters mail filters to save
     */
    protected void saveMailFilters(Map<String, MailFolder> mailFolderCache, List<MailFilter> mailFilters) {
        MailFilterTopiaDao dao = getPersistenceContext().getMailFilterDao();

        int position = 0;
        List<MailFilter> filters = dao.findAll();
        // order by naturalId
        Map<String, MailFilter> filterByExpression = new HashMap<>(Maps.uniqueIndex(filters, GET_EXPRESSION));
        for (MailFilter mailFilter : mailFilters) {

            MailFilter filter = filterByExpression.remove(mailFilter.getExpression().toLowerCase());
            boolean create = filter == null;
            if (create) {
                filter = dao.newInstance();
            }

            filter.setExpression(mailFilter.getExpression().toLowerCase());
            filter.setFilterFolderPriority(mailFilter.isFilterFolderPriority());

            // mailFilter.getMailFolder().getTopiaId() can start with new_
            MailFolder mailFolder = mailFolderCache.get(mailFilter.getMailFolder().getTopiaId());
            filter.setMailFolder(mailFolder);
            filter.setPosition(position);

            if (create) {
                filter = dao.create(filter);
            } else {
                filter = dao.update(filter);
            }
            position++;
        }

        dao.deleteAll(filterByExpression.values());
    }

    /**
     * Return all email accounts (without password info).
     * 
     * @return all email account
     */
    @Override
    public List<EmailAccount> getEmailAccountsWithoutPasswords() {
        List<EmailAccount> accounts = getEmailAccounts();
        // remove password information from bean
        final Binder<EmailAccount, EmailAccount> binderEmailAccount = BinderFactory.newBinder(EmailAccount.class);
        List<EmailAccount> result = Lists.transform(accounts, new Function<EmailAccount, EmailAccount>() {
            @Override
            public EmailAccount apply(EmailAccount input) {
                EmailAccount account = new EmailAccountImpl();
                binderEmailAccount.copyExcluding(input, account, EmailAccount.PROPERTY_PASSWORD);
                return account;
            }
        });

        return result;
    }

    @Override
    public List<EmailAccount> getEmailAccounts() {
        EmailAccountTopiaDao emailAccountTopiaDao = getPersistenceContext().getEmailAccountDao();
        List<EmailAccount> result = emailAccountTopiaDao.findAll();
        return result;
    }

    /**
     * Save email account without commit.
     * 
     * @param newEmailAccounts new email account list to save
     */
    protected void saveEmailAccounts(List<EmailAccount> newEmailAccounts) {
        EmailAccountTopiaDao emailAccountTopiaDao = getPersistenceContext().getEmailAccountDao();
        List<EmailAccount> emailAccounts = emailAccountTopiaDao.findAll();
        Map<String, EmailAccount> emailAccountMap = new HashMap<>(Maps.uniqueIndex(emailAccounts, TopiaEntities.getTopiaIdFunction()));
        Binder<EmailAccount, EmailAccount> binder = BinderFactory.newBinder(EmailAccount.class);

        for (EmailAccount newEmailAccount : newEmailAccounts) {
            EmailAccount emailAccount;
            if (StringUtils.isNotBlank(newEmailAccount.getTopiaId())) {
                emailAccount = emailAccountMap.remove(newEmailAccount.getTopiaId());
            } else {
                emailAccount = new EmailAccountImpl();
            }

            binder.copyExcluding(newEmailAccount, emailAccount,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    EmailAccount.PROPERTY_PASSWORD);

            // password is only present for new account (not for edited ones)
            if (StringUtils.isNotEmpty(newEmailAccount.getPassword())) {
                emailAccount.setPassword(newEmailAccount.getPassword());
            }

            if (emailAccount.isPersisted()) {
                emailAccountTopiaDao.update(emailAccount);
            } else {
                emailAccountTopiaDao.create(emailAccount);
            }
        }

        emailAccountTopiaDao.deleteAll(emailAccountMap.values());
    }

    protected void saveBrandsForDomains(List<BrandsForDomain> newBrandsForDomains) {
        BrandsForDomainTopiaDao brandsForDomainTopiaDao = getPersistenceContext().getBrandsForDomainDao();
        List<BrandsForDomain> brandsForDomains = brandsForDomainTopiaDao.findAll();
        Map<String, BrandsForDomain> brandsForDomainMap = new HashMap<>(Maps.uniqueIndex(brandsForDomains, TopiaEntities.getTopiaIdFunction()));
        Binder<BrandsForDomain, BrandsForDomain> binder = BinderFactory.newBinder(BrandsForDomain.class);

        for (BrandsForDomain newBrandsForDomain : newBrandsForDomains) {
            BrandsForDomain brandsForDomain;
            if (StringUtils.isNotBlank(newBrandsForDomain.getTopiaId())) {
                brandsForDomain = brandsForDomainMap.remove(newBrandsForDomain.getTopiaId());
            } else {
                brandsForDomain = new BrandsForDomainImpl();
            }

            binder.copyExcluding(newBrandsForDomain, brandsForDomain,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION);


            if (brandsForDomain.isPersisted()) {
                brandsForDomainTopiaDao.update(brandsForDomain);
            } else {
                brandsForDomainTopiaDao.create(brandsForDomain);
            }
        }

        brandsForDomainTopiaDao.deleteAll(brandsForDomainMap.values());
    }

    protected void saveSigningsForDomains(List<SigningForDomain> newSigningForDomains) {
        SigningForDomainTopiaDao signingForDomainTopiaDao = getPersistenceContext().getSigningForDomainDao();
        List<SigningForDomain> signingForDomains = signingForDomainTopiaDao.findAll();

        Map<String, SigningForDomain> signingsForDomainMap = new HashMap<>(Maps.uniqueIndex(signingForDomains, TopiaEntities.getTopiaIdFunction()));
        Binder<SigningForDomain, SigningForDomain> binder = BinderFactory.newBinder(SigningForDomain.class);

        for (SigningForDomain newSigningForDomain : newSigningForDomains) {
            SigningForDomain signingForDomain;
            if (StringUtils.isBlank(newSigningForDomain.getTopiaId()) || newSigningForDomain.getTopiaId().startsWith("new_")) {
                signingForDomain = new SigningForDomainImpl();

            } else {
                signingForDomain = signingsForDomainMap.remove(newSigningForDomain.getTopiaId());
            }

            binder.copyExcluding(newSigningForDomain, signingForDomain,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION);


            if (signingForDomain.isPersisted()) {
                signingForDomainTopiaDao.update(signingForDomain);
            } else {
                signingForDomainTopiaDao.create(signingForDomain);
            }
        }

        signingForDomainTopiaDao.deleteAll(signingsForDomainMap.values());
    }

    @Override
    public void saveUserFolders(Map<String, Collection<MailFolder>> userFolders) {
        FaxToMailUserTopiaDao faxToMailUserTopiaDao = getPersistenceContext().getFaxToMailUserDao();

        for (Map.Entry<String, Collection<MailFolder>> entry : userFolders.entrySet()) {
            String userId = entry.getKey();
            Collection<MailFolder> mailFolders = entry.getValue();
            
            FaxToMailUser user = faxToMailUserTopiaDao.forTopiaIdEquals(userId).findUnique();
            user.clearAffectedFolders();
            for (MailFolder mailFolder : mailFolders) {
                user.addAffectedFolders(mailFolder);
            }
            faxToMailUserTopiaDao.update(user);
        }

        getPersistenceContext().commit();
    }

    @Override
    public ExtensionCommand getExtensionCommand(String extension) {
        ExtensionCommandTopiaDao extensionCommandDao = getPersistenceContext().getExtensionCommandDao();
        ExtensionCommand result = extensionCommandDao.findByExtension(extension);
        return result;
    }

    /**
     * Retourne l'ensemble des utilisateurs appartenant aux groupes gérés par les groupes chef.
     * 
     * @param user current user
     * @return all managed users
     */
    @Override
    public Set<FaxToMailUser> getUserManagedUsers(FaxToMailUser user) {

        Set<FaxToMailUser> result = new HashSet<>();

        // s'il n'y a pas de group, il ne peut pas y avoir de droits
        if (user.getUserGroups() != null) {

            // recuperation de tout les groupes chef dont l'utilsateur est membre
            GroupChefTopiaDao groupChefTopiaDao = getPersistenceContext().getGroupChefDao();
            List<GroupChef> groupChefs = groupChefTopiaDao.forUserGroupIn(user.getUserGroups()).findAll();

            // recuperation de tous les users des groups gérés
            FaxToMailUserTopiaDao faxToMailUserTopiaDao = getPersistenceContext().getFaxToMailUserDao();
            for (GroupChef groupChef : groupChefs) {
                // groups
                if (groupChef.getManagedGroups() != null) {
                    for (FaxToMailUserGroup group : groupChef.getManagedGroups()) {
                        List<FaxToMailUser> users = faxToMailUserTopiaDao.forUserGroupsContains(group).findAll();
                        result.addAll(users);
                    }
                }
                // users
                if (groupChef.getManagedUsers() != null) {
                    result.addAll(groupChef.getManagedUsers());
                }
            }
        }

        return result;
    }

    /**
     * Return file information on file path.
     * 
     * @param path path
     * @return file info
     */
    @Override
    public Map<String, Object> checkDirectory(String path) {
        Map<String, Object> result = new HashMap<>();
        File file = new File(path);
        result.put("path", path);
        result.put("exist", file.exists());
        result.put("isDirectory", file.isDirectory());
        result.put("canRead", file.canRead());
        result.put("canWrite", file.canWrite());
        result.put("canExecute", file.canExecute());
        return result;
    }

    /**
     * Verification de la connexion à un serveur de mail.
     * 
     * @param account email account to check
     * @return 
     */
    @Override
    public Map<String, Object> checkMailaccount(EmailAccount account) {
        Map<String, Object> result = new HashMap<>();

        long before = System.currentTimeMillis();
        if (log.isDebugEnabled()) {
            log.debug("Check account " + account.getProtocol() + " : " + account.getHost());
        }

        Properties properties = new Properties();
        
        if (account.getProtocol() == EmailProtocol.POP3S) {
            properties.setProperty("mail.pop3.ssl.enable", "true");
        }

        switch (account.getProtocol()) {
        case IMAPS:
            properties.setProperty("mail.imap.ssl.enable", "true");
        case IMAP:
            properties.setProperty("mail.store.protocol", "imap");
            properties.setProperty("mail.imap.host", account.getHost());
            properties.setProperty("mail.imap.port", String.valueOf(account.getPort()));
            properties.setProperty("mail.imap.connectiontimeout", "2000");
            properties.setProperty("mail.imap.starttls.enable", "true");
            break;
        case POP3S:
            properties.setProperty("mail.pop3.ssl.enable", "true");
        case POP3:
            properties.setProperty("mail.store.protocol", "pop3");
            properties.setProperty("mail.pop3.host", account.getHost());
            properties.setProperty("mail.pop3.port", String.valueOf(account.getPort()));
            properties.setProperty("mail.pop3.connectiontimeout", "2000");
            break;
        }

        Session session = Session.getInstance(properties);
        Store store = null;

        try {
            store = session.getStore();
            store.connect(account.getLogin(), account.getPassword());
            store.close();
            
            // set result map
            result.put("ok", true);
        } catch (MessagingException e) {
            log.error("Error while getting emails from the mailbox", e);
            
            // set result map
            result.put("ok", false);
            result.put("error", e.getMessage());
        } finally {
            if (store != null) {
                try {
                    store.close();
                } catch (MessagingException ex) {
                    // silent close
                }
            }
        }

        if (log.isDebugEnabled()) {
            long after = System.currentTimeMillis();
            log.debug("Checked in  " + (after - before) + " ms");
        }
        return result;
    }

    @Override
    public long getEmailMaxSize() {
        Configuration conf = getConfiguration();
        return conf.getEmailMaxSize();
    }

    @Override
    public List<MailField> getSearchDisplayColumns() {
        Configuration conf = getConfiguration();
        return conf.getSearchDisplayColumns();
    }

    @Override
    public List<BrandsForDomain> getAllBrandsForDomains() {
        return getPersistenceContext().getBrandsForDomainDao().findAll();
    }

    @Override
    public List<String> getBrandsForEmailAddress(String emailAddress) {
        String domain = FaxToMailServiceUtils.getDomainForEmailAddress(emailAddress);
        BrandsForDomainTopiaDao dao = getPersistenceContext().getBrandsForDomainDao();
        BrandsForDomain brandsForDomain = dao.forDomainNameEquals(domain).findUniqueOrNull();

        List<String> brands = null;

        if (brandsForDomain != null){
            brands = brandsForDomain.getBrands();
        }

        return brands;
    }

    @Override
    public List<SigningForDomain> getAllSigningsForDomains() {
        return getPersistenceContext().getSigningForDomainDao().findAll();
    }

    @Override
    public Optional<SigningForDomain> getSigningForEmailAddress(String emailAddress) {
        String domain = FaxToMailServiceUtils.getDomainForEmailAddress(emailAddress);
        return getPersistenceContext().getSigningForDomainDao().forDomainNameEquals(domain).tryFindUnique();
    }
}
