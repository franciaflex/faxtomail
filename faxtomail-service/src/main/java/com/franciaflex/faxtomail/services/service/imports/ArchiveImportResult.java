package com.franciaflex.faxtomail.services.service.imports;

/*
 * #%L
 * FaxToMail :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 1.1
 */
public class ArchiveImportResult {

    protected int nbImportedArchives = 0;
    protected int nbImportErrors = 0;
    protected File errorFile;

    public int getNbImportedArchives() {
        return nbImportedArchives;
    }

    public void addNbImportedArchives(int nbImportedArchives) {
        this.nbImportedArchives += nbImportedArchives;
    }

    public int getNbImportErrors() {
        return nbImportErrors;
    }

    public void addNbImportErrors(int nbImportErrors) {
        this.nbImportErrors += nbImportErrors;
    }

    public File getErrorFile() {
        return errorFile;
    }

    public void setErrorFile(File errorFile) {
        this.errorFile = errorFile;
    }
}
