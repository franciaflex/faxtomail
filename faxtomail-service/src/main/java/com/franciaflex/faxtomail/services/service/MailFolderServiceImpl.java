package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.EmailTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroup;
import com.franciaflex.faxtomail.persistence.entities.MailFilter;
import com.franciaflex.faxtomail.persistence.entities.MailFilterTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.MailFolderTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.FaxToMailServiceSupport;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Hibernate;
import org.nuiton.util.pagination.PaginationParameter;

import javax.mail.Address;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author kmorin - kmorin@codelutin.com
 */
public class MailFolderServiceImpl extends FaxToMailServiceSupport implements MailFolderService {

    @Override
    public MailFolder getFolderForRecipient(String recipient) {
        Collection<MailFilter> filters = getFiltersForRecipient(recipient);
        MailFolder result;
        if (CollectionUtils.isNotEmpty(filters)) {
            result = filters.iterator().next().getMailFolder();
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public List<MailFilter> getFiltersForRecipient(String recipient) {
        MailFilterTopiaDao filterTopiaDao = getPersistenceContext().getMailFilterDao();

        PaginationParameter tpb = PaginationParameter.of(0, -1, MailFilter.PROPERTY_POSITION, false);

        String query = "FROM " + MailFilter.class.getCanonicalName() + " WHERE :recipient LIKE " + MailFilter.PROPERTY_EXPRESSION;
        Map<String, Object> params = new HashMap<>();
        params.put("recipient", recipient);

        List<MailFilter> filters = filterTopiaDao.find(query, params, tpb);
        return filters;
    }

    @Override
    public List<MailFolder> getAllMailFolders() {
        MailFolderTopiaDao dao = getPersistenceContext().getMailFolderDao();
        return new ArrayList<>(dao.findAll());
    }
    
    @Override
    public List<MailFolder> getRootMailFolders() {
        MailFolderTopiaDao dao = getPersistenceContext().getMailFolderDao();
        List<MailFolder> result = dao.forParentEquals(null).findAll();
        return result;
    }

    @Override
    public List<MailFolder> getRootMailFoldersWithReadingRights(FaxToMailUser user) {
        FaxToMailUser upToDateUser = serviceContext.getUserService().getUser(user.getTopiaId());
        user.setAffectedFolders(upToDateUser.getAffectedFolders());

        MailFolderTopiaDao dao = getPersistenceContext().getMailFolderDao();

        List<MailFolder> folders = new ArrayList<>(dao.getReadableFolders(user));
        // copy the list to be able to remove the folders whose parent is already in the list
        List<MailFolder> result = new ArrayList<>(folders);

        for (MailFolder folder : folders) {
            MailFolder f = folder;
            while (f.getParent() != null) {
                f = f.getParent();

                // if the parent is already in the readable roots, remove the current folder from the roots
                if (result.contains(f)) {
                    result.remove(folder);
                    folder = null;
                    break;
                }

                Hibernate.initialize(f);
                f.setFolderReadable(false);
                f.setFolderWritable(false);

                fetchFolderAttributes(f);
            }

            if (folder != null) {
                browseReadableFolders(folder, result, user, false);
            }
        }
        return result;
    }

    protected void browseReadableFolders(MailFolder folder, List<MailFolder> readableRoots, FaxToMailUser user, boolean writable) {
        folder.setFolderReadable(true);

        if (!writable && folder.sizeWriteRightGroups() > 0) {
            writable = CollectionUtils.containsAny(folder.getWriteRightGroups(), user.getUserGroups());
        }
        if (!writable && folder.sizeWriteRightUsers() > 0) {
            writable = folder.containsWriteRightUsers(user);
        }
        folder.setFolderWritable(writable);

        if (folder.isChildrenNotEmpty()) {
            for (MailFolder child : folder.getChildren()) {
                browseReadableFolders(child, readableRoots, user, writable);
            }
        }

        fetchFolderAttributes(folder);

    }

    @Override
    public List<MailFolder> getRootMailFoldersWithMoveRights(FaxToMailUser user) {
        MailFolderTopiaDao dao = getPersistenceContext().getMailFolderDao();

        List<MailFolder> folders = new ArrayList<>(dao.getMoveableFolders(user));
        // copy the list to be able to remove the folders whose parent is already in the list
        List<MailFolder> result = new ArrayList<>(folders);

        for (MailFolder folder : folders) {
            MailFolder f = folder;
            Boolean allowMoveDemandIntoFolder = folder.getAllowMoveDemandIntoFolder();
            while (f.getParent() != null) {
                f = f.getParent();

                // if the parent is already in the moveable roots, remove the current folder from the roots
                if (result.contains(f)) {
                    result.remove(folder);
                    folder = null;
                    break;
                }

                fetchFolderAttributes(f);

                if (allowMoveDemandIntoFolder == null) {
                    allowMoveDemandIntoFolder = f.getAllowMoveDemandIntoFolder();
                }
            }

            if (folder != null) {
                browseMoveableFolders(folder, result, allowMoveDemandIntoFolder);
            }
        }
        return result;
    }

    protected void browseMoveableFolders(MailFolder folder, List<MailFolder> readableRoots, boolean inheritedAllowMoveDemandIntoFolder) {
        Boolean allowMoveDemandIntoFolder = folder.getAllowMoveDemandIntoFolder();
        if (allowMoveDemandIntoFolder == null) {
            allowMoveDemandIntoFolder = inheritedAllowMoveDemandIntoFolder;
        } else {
            inheritedAllowMoveDemandIntoFolder = allowMoveDemandIntoFolder;
        }
        folder.setFolderMoveable(allowMoveDemandIntoFolder);

        if (folder.isChildrenNotEmpty()) {
            for (MailFolder child : folder.getChildren()) {
                // if the child is in the moveable roots, remove it to merge the branches
                readableRoots.remove(child);
                browseMoveableFolders(child, readableRoots, inheritedAllowMoveDemandIntoFolder);
            }
        }

        fetchFolderAttributes(folder);
    }

    protected void findMoveFolders(MailFolder folder, FaxToMailUser user, List<MailFolder> folders) {
        if (folder.containsMoveRightUsers(user)
                || !CollectionUtils.intersection(folder.getMoveRightGroups(), user.getUserGroups()).isEmpty()) {
            folders.add(folder);

        } else if (folder.isChildrenNotEmpty()) {
            for (MailFolder child : folder.getChildren()) {
                findMoveFolders(child, user, folders);
            }
        }
    }

    @Override
    public void fetchFolderAttributes(MailFolder folder) {
        Hibernate.initialize(folder.getChildren());
        Hibernate.initialize(folder.getFolderTableColumns());
        Hibernate.initialize(folder.getRanges());
        Hibernate.initialize(folder.getReplyAddresses());
        Hibernate.initialize(folder.getReplyDomains());

        if (folder.sizeWaitingStates() > 0) {
            for (WaitingState waitingState : folder.getWaitingStates()) {
                Hibernate.initialize(waitingState.getInvalidFormDisabledActions());
                Hibernate.initialize(waitingState.getValidFormDisabledActions());
            }
        }

        Hibernate.initialize(folder.getDemandTypes());
    }

    @Override
    public List<MailFolder> getMailFolders(Collection<String> ids) {
        MailFolderTopiaDao dao = getPersistenceContext().getMailFolderDao();
        return new ArrayList<MailFolder>(dao.forTopiaIdIn(ids).findAll());
    }

    /**
     * Retourne les dossiers qui ont l'ensemble des etats d'attente compatible avec ceux en argument.
     */
    @Override
    public Set<MailFolder> getFoldersWithWaitingState(Set<WaitingState> requiredWaitingStates) {
        Set<MailFolder> result = new HashSet<>();

        MailFolderTopiaDao dao = getPersistenceContext().getMailFolderDao();
        List<MailFolder> roots = dao.forParentEquals(null).findAll();

        for (MailFolder root : roots) {
            Collection<MailFolder> mailFolders = getFoldersWithWaitingState(root, new HashSet<WaitingState>(), requiredWaitingStates);
            result.addAll(mailFolders);
        }

        return result;
    }

    protected Collection<MailFolder> getFoldersWithWaitingState(MailFolder folder,
                                                               Collection<WaitingState> parentWaitingState,
                                                               Set<WaitingState> requiredWaitingStates) {
        Collection<WaitingState> waitingStates = folder.getWaitingStates();
        if (CollectionUtils.isEmpty(waitingStates)) {
            waitingStates = parentWaitingState;
        } else {
            parentWaitingState = waitingStates;
        }

        // resursion
        Collection<MailFolder> result = new HashSet<>();
        Collection<MailFolder> children = folder.getChildren();
        if (children != null) {
            for (MailFolder child : children) {
                Collection<MailFolder> subMailFolders = getFoldersWithWaitingState(child,
                        parentWaitingState, requiredWaitingStates);
                result.addAll(subMailFolders);
            }
        }

        // pour qu'un dossier soit sélectionné il faut que les dossiers ait des etat d'attentes (ou ses parents)
        // que les etats d'attente requis ne soit pas vide
        if ((CollectionUtils.isEmpty(waitingStates) ||
                CollectionUtils.isEmpty(requiredWaitingStates) ||
                waitingStates.containsAll(requiredWaitingStates))) {
            result.add(folder);
        }

        return result;

    }

    /**
     * Retourne une map avec les identifiants des {@code mailFolder}s et le nombre de mails qu'ils
     * contiennent pour pouvoir empecher la suppression.
     * Seulement pour les dossiers qui ont au moins un email.
     * 
     * @return le nombre de mail par dossier
     */
    @Override
    public Map<String, Long> getMailFoldersUsage() {
        EmailTopiaDao dao = getPersistenceContext().getEmailDao();
        Map<String, Long> result = dao.getMailCountByFolder();
        return result;
    }

    @Override
    public MailFolder getFolderForFaxToMailUser(FaxToMailUser customerResponsible) {
        MailFolderTopiaDao dao = getPersistenceContext().getMailFolderDao();
        MailFolder result = dao.forCustomerResponsiblesContains(customerResponsible).findAnyOrNull();
        return result;
    }

    @Override
    public MailFolder getMailFolder(String topiaId) {
        MailFolderTopiaDao dao = getPersistenceContext().getMailFolderDao();
        MailFolder result = dao.forTopiaIdEquals(topiaId).findUnique();
        return result;
    }

    @Override
    public MailFilter findMailFilter(Email email, Set<String> modifiedProperties, List<Address> recipients) {
        MailFilter filter = null;
        for (Address address : recipients) {

            String recipient = address.toString();

            // some recipient are like "toto tutu<toto.tutu73@gmail.com>"
            // the regex is to extract email address from it
            recipient = recipient.replaceFirst("^.*<(.*)>$", "$1");
            recipient = recipient.toLowerCase();

            List<MailFilter> filters = getFiltersForRecipient(recipient);

            if (CollectionUtils.isNotEmpty(filters)) {
                MailFilter mailFilter = filters.get(0);
                // see #6161
                if (filter == null || mailFilter.getPosition() < filter.getPosition()) {
                    filter = mailFilter;
                    email.setRecipient(recipient);
                    modifiedProperties.add(Email.PROPERTY_RECIPIENT);
                }
            }
        }
        return filter;
    }

    /**
     * Get back all the users who have writing rights on a folder
     * @param folderId The folder topiaId
     * @return List of all the users with writing rights
     */
    @Override
    public Collection<FaxToMailUser> getUsersForFolder(String folderId) {
        Collection<FaxToMailUser> users = new ArrayList<>();
        MailFolder mailFolder = getMailFolder(folderId);
        LdapService ldapService = serviceContext.getLdapService();


        Hibernate.initialize(mailFolder.getWriteRightUsers());
        Hibernate.initialize(mailFolder.getWriteRightGroups());
        users.addAll(mailFolder.getWriteRightUsers());
        Collection<FaxToMailUserGroup> groups = mailFolder.getWriteRightGroups();
        for (FaxToMailUserGroup group:groups) {
            users.addAll(ldapService.getUsersForGroup(group));
        }

        if (mailFolder.getParent() != null) {
            users.addAll(getUsersForFolder(mailFolder.getParent().getTopiaId()));
        }
        return users;
    }

}
