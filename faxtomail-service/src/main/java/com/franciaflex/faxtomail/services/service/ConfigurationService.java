package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.BrandsForDomain;
import com.franciaflex.faxtomail.persistence.entities.Configuration;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.EmailAccount;
import com.franciaflex.faxtomail.persistence.entities.ExtensionCommand;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.MailFilter;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.SigningForDomain;
import com.franciaflex.faxtomail.persistence.entities.Stamp;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.FaxToMailService;
import com.google.common.base.Optional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Kevin Morin (Code Lutin)
 */
public interface ConfigurationService extends FaxToMailService {
    Configuration getConfiguration();

    List<MailFilter> getMailFilters();

    void save(Configuration configuration,
              List<DemandType> demandTypes,
              List<Stamp> stamps,
              List<WaitingState> waitingStates,
              List<MailFolder> mailFolders,
              List<MailFilter> mailFilters,
              List<EmailAccount> emailAccounts,
              List<BrandsForDomain> brandsForDomains,
              List<SigningForDomain> signingForDomains);

    List<EmailAccount> getEmailAccountsWithoutPasswords();

    List<EmailAccount> getEmailAccounts();

    void saveUserFolders(Map<String, Collection<MailFolder>> userFolders);

    ExtensionCommand getExtensionCommand(String extension);

    Set<FaxToMailUser> getUserManagedUsers(FaxToMailUser user);

    Map<String, Object> checkDirectory(String path);

    Map<String, Object> checkMailaccount(EmailAccount account);

    long getEmailMaxSize();

    List<MailField> getSearchDisplayColumns();

    List<BrandsForDomain> getAllBrandsForDomains();

    List<String> getBrandsForEmailAddress(String emailAddress);

    List<SigningForDomain> getAllSigningsForDomains();

    Optional<SigningForDomain> getSigningForEmailAddress(String emailAddress);



}
