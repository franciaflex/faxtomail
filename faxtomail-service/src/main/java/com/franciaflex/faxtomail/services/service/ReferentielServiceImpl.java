package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.ClientImpl;
import com.franciaflex.faxtomail.persistence.entities.ClientTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.DemandTypeTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.EmailAccount;
import com.franciaflex.faxtomail.persistence.entities.EmailAccountTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.MailFilter;
import com.franciaflex.faxtomail.persistence.entities.MailFilterTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.Priority;
import com.franciaflex.faxtomail.persistence.entities.PriorityTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.persistence.entities.RangeTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.Stamp;
import com.franciaflex.faxtomail.persistence.entities.StampTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.persistence.entities.WaitingStateTopiaDao;
import com.franciaflex.faxtomail.services.FaxToMailServiceSupport;
import com.franciaflex.faxtomail.services.service.imports.ClientImportBean;
import com.franciaflex.faxtomail.services.service.imports.ClientImportModel;
import com.franciaflex.faxtomail.services.service.imports.DemandTypeImportModel;
import com.franciaflex.faxtomail.services.service.imports.EmailAccountImportModel;
import com.franciaflex.faxtomail.services.service.imports.EmailFilterImportModel;
import com.franciaflex.faxtomail.services.service.imports.PriorityImportModel;
import com.franciaflex.faxtomail.services.service.imports.RangeImportModel;
import com.franciaflex.faxtomail.services.service.imports.WaitingStateImportModel;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author kmorin - kmorin@codelutin.com
 */
public class ReferentielServiceImpl extends FaxToMailServiceSupport implements ReferentielService {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(ReferentielServiceImpl.class);

    @Override
    public List<DemandType> getAllDemandType() {
        DemandTypeTopiaDao dao = getPersistenceContext().getDemandTypeDao();
        return dao.forAll().setOrderByArguments(DemandType.PROPERTY_LABEL).findAll();
    }

    @Override
    public List<Priority> getAllPriority() {
        PriorityTopiaDao dao = getPersistenceContext().getPriorityDao();
        return dao.forAll().setOrderByArguments(Priority.PROPERTY_LABEL).findAll();
    }

    @Override
    public List<Range> getAllRange() {
        RangeTopiaDao dao = getPersistenceContext().getRangeDao();
        return dao.forAll().setOrderByArguments(Range.PROPERTY_LABEL).findAll();
    }

    @Override
    public List<Stamp> getAllStamps() {
        StampTopiaDao dao = getPersistenceContext().getStampDao();
        return dao.forAll().setOrderByArguments(Stamp.PROPERTY_LABEL).findAll();
    }

    @Override
    public List<WaitingState> getAllWaitingState() {
        WaitingStateTopiaDao dao = getPersistenceContext().getWaitingStateDao();
        return dao.forAll().setOrderByArguments(WaitingState.PROPERTY_LABEL).findAll();
    }

    @Override
    public Map<String, Long> getWaitingStatesUsage() {
        WaitingStateTopiaDao dao = getPersistenceContext().getWaitingStateDao();
        Map<String, Long> result = dao.getWaitingStateCountByFolder();
        return result;
    }

    @Override
    public List<Client> importClients(InputStream inputStream) {
        List<Client> result = new ArrayList<>();
        ClientTopiaDao dao = getPersistenceContext().getClientDao();

        ClientImportModel clientImportModel = new ClientImportModel(';');
        Binder<ClientImportBean, Client> clientBinder = BinderFactory.newBinder(ClientImportBean.class, Client.class);
        Import<ClientImportBean> importer = null;
        try {
            importer = Import.newImport(clientImportModel, new InputStreamReader(inputStream, getApplicationConfig().getImportFileEncoding()));
            
            MultiKeyMap<String, Client> clientCache = new MultiKeyMap<>();
            for (ClientImportBean client : importer) {

                Client current = clientCache.get(client.getCode(), client.getCompany());
                if (current == null) {
                    current = dao.forCodeEquals(client.getCode()).addEquals(Client.PROPERTY_COMPANY, client.getCompany()).findAnyOrNull();
                    if (current == null) {
                        current = new ClientImpl();
                    }
    
                    clientBinder.copyExcluding(client, current,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION);
                    
                    // clear current adresses and fax numbers
                    current.setEmailAddresses(null);
                    current.setFaxNumbers(null);
                    
                    // update cache
                    clientCache.put(client.getCode(), client.getCompany(), current);
                }

                // manage special collections
                if (StringUtils.isNotBlank(client.getEmailAddress())) {
                    List<String> currentAddresses = current.getEmailAddresses();
                    if (currentAddresses == null) {
                        currentAddresses = new ArrayList<String>();
                    }
                    currentAddresses.add(client.getEmailAddress());
                    current.setEmailAddresses(currentAddresses);
                }
                if (StringUtils.isNotBlank(client.getFaxNumber())) {
                    List<String> faxNumbers = current.getFaxNumbers();
                    if (faxNumbers == null) {
                        faxNumbers = new ArrayList<String>();
                    }
                    faxNumbers.add(client.getFaxNumber());
                    current.setFaxNumbers(faxNumbers);
                }

                if (client.isPersisted()) {
                    current = dao.update(current);
                } else {
                    current = dao.create(current);
                }
                result.add(current);
            }
            getPersistenceContext().commit();

        } catch (ImportRuntimeException|UnsupportedEncodingException e) {
            String message;
            if (e.getCause() != null) {
                message = e.getCause().getMessage();
            } else {
                message = e.getMessage();
            }
            throw new ApplicationTechnicalException(message, e);
        } finally {
            IOUtils.closeQuietly(importer);
            IOUtils.closeQuietly(inputStream);
        }
        return result;
    }

    @Override
    public List<EmailAccount> importEmailAccounts(InputStream inputStream) {
        List<EmailAccount> result = new ArrayList<>();
        EmailAccountTopiaDao dao = getPersistenceContext().getEmailAccountDao();

        EmailAccountImportModel emailAccountImportModel = new EmailAccountImportModel(';');
        try (Import<EmailAccount> importer = Import.newImport(emailAccountImportModel, new InputStreamReader(inputStream, getApplicationConfig().getImportFileEncoding()))) {
            for (EmailAccount emailAccount : importer) {
                result.add(dao.create(emailAccount));
            }
            getPersistenceContext().commit();
        } catch (ImportRuntimeException|UnsupportedEncodingException e) {
            String message;
            if (e.getCause() != null) {
                message = e.getCause().getMessage();
            } else {
                message = e.getMessage();
            }
            throw new ApplicationTechnicalException(message, e);

        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        return result;
    }

    @Override
    public List<MailFilter> importEmailFilters(InputStream inputStream, Map<String, MailFolder> foldersByName) {
        List<MailFilter> result = new ArrayList<>();
        MailFilterTopiaDao dao = getPersistenceContext().getMailFilterDao();

        EmailFilterImportModel emailFilterImportModel = new EmailFilterImportModel(';', foldersByName);
        try (Import<MailFilter> importer = Import.newImport(emailFilterImportModel, new InputStreamReader(inputStream, getApplicationConfig().getImportFileEncoding()))) {
            for (MailFilter emailFilter : importer) {
                result.add(dao.create(emailFilter));
            }
            getPersistenceContext().commit();

        } catch (ImportRuntimeException|UnsupportedEncodingException e) {
            String message;
            if (e.getCause() != null) {
                message = e.getCause().getMessage();
            } else {
                message = e.getMessage();
            }
            throw new ApplicationTechnicalException(message, e);

        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        return result;
    }

    @Override
    public List<WaitingState> importWaitingStates(InputStream inputStream) {

        List<WaitingState> result = new ArrayList<>();
        WaitingStateTopiaDao waitingStateTopiaDao = getPersistenceContext().getWaitingStateDao();
        
        WaitingStateImportModel importModel = new WaitingStateImportModel(';');
        try (Import<WaitingState> importer = Import.newImport(importModel, new InputStreamReader(inputStream, getApplicationConfig().getImportFileEncoding()))) {
            for (WaitingState waitingState : importer) {
                
                WaitingState currentWaitingState = waitingStateTopiaDao.forLabelEquals(waitingState.getLabel()).findAnyOrNull();
                if (currentWaitingState == null) {
                    currentWaitingState = waitingStateTopiaDao.create(waitingState);
                }
                // else maybe update current ?
                
                result.add(currentWaitingState);
            }
            getPersistenceContext().commit();

        } catch (ImportRuntimeException|UnsupportedEncodingException e) {
            String message;
            if (e.getCause() != null) {
                message = e.getCause().getMessage();
            } else {
                message = e.getMessage();
            }
            throw new ApplicationTechnicalException(message, e);

        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        
        return result;
    }

    @Override
    public List<Priority> importPriorities(InputStream inputStream) {
        List<Priority> result = new ArrayList<>();
        PriorityTopiaDao priorityTopiaDao = getPersistenceContext().getPriorityDao();
        
        PriorityImportModel importModel = new PriorityImportModel(';');
        try (Import<Priority> importer = Import.newImport(importModel, new InputStreamReader(inputStream, getApplicationConfig().getImportFileEncoding()))) {
            for (Priority priority : importer) {
                
                Priority currentPriority = priorityTopiaDao.forLabelEquals(priority.getLabel()).findAnyOrNull();
                if (currentPriority == null) {
                    currentPriority = priorityTopiaDao.create(priority);
                }
                // else maybe update current ?
                
                result.add(currentPriority);
            }
            getPersistenceContext().commit();

        } catch (ImportRuntimeException|UnsupportedEncodingException e) {
            String message;
            if (e.getCause() != null) {
                message = e.getCause().getMessage();
            } else {
                message = e.getMessage();
            }
            throw new ApplicationTechnicalException(message, e);

        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        
        return result;
    }

    @Override
    public List<Range> importRanges(InputStream inputStream) {
        List<Range> result = new ArrayList<>();
        RangeTopiaDao rangeTopiaDao = getPersistenceContext().getRangeDao();
        
        RangeImportModel importModel = new RangeImportModel(';');
        try (Import<Range> importer = Import.newImport(importModel, new InputStreamReader(inputStream, getApplicationConfig().getImportFileEncoding()))) {
            for (Range range : importer) {
                
                Range currentRange = rangeTopiaDao.forLabelEquals(range.getLabel()).findAnyOrNull();
                if (currentRange == null) {
                    currentRange = rangeTopiaDao.create(range);
                }
                // else maybe update current ?
                
                result.add(currentRange);
            }
            getPersistenceContext().commit();

        } catch (ImportRuntimeException|UnsupportedEncodingException e) {
            String message;
            if (e.getCause() != null) {
                message = e.getCause().getMessage();
            } else {
                message = e.getMessage();
            }
            throw new ApplicationTechnicalException(message, e);

        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        return result;
    }

    @Override
    public List<DemandType> importDemandTypes(InputStream inputStream) {
        List<DemandType> result = new ArrayList<>();
        DemandTypeTopiaDao demandTypeTopiaDao = getPersistenceContext().getDemandTypeDao();
        
        DemandTypeImportModel importModel = new DemandTypeImportModel(';');
        try (Import<DemandType> importer = Import.newImport(importModel, new InputStreamReader(inputStream, getApplicationConfig().getImportFileEncoding()))) {
            for (DemandType demandType : importer) {
                
                DemandType currentDemandType = demandTypeTopiaDao.forLabelEquals(demandType.getLabel()).findAnyOrNull();
                if (currentDemandType == null) {
                    currentDemandType = demandTypeTopiaDao.create(demandType);
                }
                // else maybe update current ?
                
                result.add(currentDemandType);
            }
            getPersistenceContext().commit();

        } catch (ImportRuntimeException|UnsupportedEncodingException e) {
            String message;
            if (e.getCause() != null) {
                message = e.getCause().getMessage();
            } else {
                message = e.getMessage();
            }
            throw new ApplicationTechnicalException(message, e);

        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        return result;
    }
}
