package com.franciaflex.faxtomail.services;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.IOUtils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class FaxToMailServiceInvocationHandler implements InvocationHandler {

    protected Object wrappedService;
    protected FaxToMailServiceContext serviceContext;

    public FaxToMailServiceInvocationHandler(Object service, FaxToMailServiceContext serviceContext) {
        this.wrappedService = service;
        this.serviceContext = serviceContext;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            Object result = method.invoke(wrappedService, args);
            return result;
        } catch (InvocationTargetException ex) {
            throw ex.getCause();
        } finally {
            IOUtils.closeQuietly(serviceContext);
        }
    }
}
