package com.franciaflex.faxtomail.services.service.imports;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailFilter;
import com.franciaflex.faxtomail.persistence.entities.MailFilterImpl;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ext.AbstractImportModel;

import java.text.ParseException;
import java.util.Map;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class EmailFilterImportModel extends AbstractImportModel<MailFilter> {

    public EmailFilterImportModel(char separator, final Map<String, MailFolder> foldersByName) {
        super(separator);

        newMandatoryColumn("recipient", MailFilter.PROPERTY_EXPRESSION);
        newMandatoryColumn("folder", MailFilter.PROPERTY_MAIL_FOLDER, new ValueParser<Object>() {
            @Override
            public Object parse(String value) throws ParseException {
                return foldersByName.get(value);
            }
        });
        newMandatoryColumn("position", MailFilter.PROPERTY_POSITION, new ValueParser<Object>() {
            @Override
            public Object parse(String value) throws ParseException {
                return Integer.parseInt(value);
            }
        });
    }

    @Override
    public MailFilter newEmptyInstance() {
        return new MailFilterImpl();
    }
}
