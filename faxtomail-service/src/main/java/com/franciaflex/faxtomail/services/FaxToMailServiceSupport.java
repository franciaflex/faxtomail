package com.franciaflex.faxtomail.services;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaPersistenceContext;
import com.franciaflex.faxtomail.services.service.ClientService;
import com.franciaflex.faxtomail.services.service.ConfigurationService;
import com.franciaflex.faxtomail.services.service.EmailService;
import com.franciaflex.faxtomail.services.service.MailFolderService;
import com.franciaflex.faxtomail.services.service.ReferentielService;

import java.util.Date;

public class FaxToMailServiceSupport implements FaxToMailService {

    protected FaxToMailServiceContext serviceContext;

    @Override
    public void setServiceContext(FaxToMailServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    protected FaxToMailConfiguration getApplicationConfig() {
        return serviceContext.getApplicationConfig();
    }

    protected Date getNow() {
        return serviceContext.getNow();
    }

    protected FaxToMailTopiaPersistenceContext getPersistenceContext() {
        return serviceContext.getPersistenceContext();
    }

    public <E extends FaxToMailService> E newService(Class<E> serviceClass) {
        return serviceContext.newService(serviceClass);
    }

    protected DecoratorService getDecoratorService() {
        return newService(DecoratorService.class);
    }

    protected ConfigurationService getConfigurationService() {
        return newService(ConfigurationService.class);
    }

    protected MailFolderService getMailFolderService() {
        return newService(MailFolderService.class);
    }

    protected EmailService getEmailService() {
        return newService(EmailService.class);
    }

    protected ReferentielService getReferentielService() {
        return newService(ReferentielService.class);
    }

    protected ClientService getClientService() {
        return newService(ClientService.class);
    }

}
