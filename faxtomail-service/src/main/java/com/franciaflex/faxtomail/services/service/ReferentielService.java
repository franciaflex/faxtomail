package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.EmailAccount;
import com.franciaflex.faxtomail.persistence.entities.MailFilter;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.Priority;
import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.persistence.entities.Stamp;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.FaxToMailService;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * @author Kevin Morin (Code Lutin)
 */
public interface ReferentielService extends FaxToMailService {
    List<DemandType> getAllDemandType();

    List<Priority> getAllPriority();

    List<Range> getAllRange();

    List<Stamp> getAllStamps();

    List<WaitingState> getAllWaitingState();

    Map<String, Long> getWaitingStatesUsage();

    List<Client> importClients(InputStream inputStream);

    List<EmailAccount> importEmailAccounts(InputStream inputStream);

    List<MailFilter> importEmailFilters(InputStream inputStream, Map<String, MailFolder> foldersByName);

    List<WaitingState> importWaitingStates(InputStream inputStream);

    List<Priority> importPriorities(InputStream inputStream);

    List<Range> importRanges(InputStream inputStream);

    List<DemandType> importDemandTypes(InputStream inputStream);

}
