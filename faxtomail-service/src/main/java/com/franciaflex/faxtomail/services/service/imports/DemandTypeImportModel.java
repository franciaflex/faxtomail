package com.franciaflex.faxtomail.services.service.imports;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.DemandTypeImpl;
import org.nuiton.csv.ext.AbstractImportModel;

/**
 * Modèle d'import des etat d'attente.
 */
public class DemandTypeImportModel extends AbstractImportModel<DemandType> {

    public DemandTypeImportModel(char separator) {
        super(separator);

        newMandatoryColumn("demandetype", DemandType.PROPERTY_LABEL);
    }

    @Override
    public DemandType newEmptyInstance() {
        return new DemandTypeImpl();
    }
}
