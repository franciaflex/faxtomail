package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFilter;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.MailFolderTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.FaxToMailService;

import javax.mail.Address;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Kevin Morin (Code Lutin)
 */
public interface MailFolderService extends FaxToMailService {
    MailFolder getFolderForRecipient(String recipient);

    List<MailFilter> getFiltersForRecipient(String recipient);

    List<MailFolder> getAllMailFolders();

    List<MailFolder> getRootMailFolders();

    void fetchFolderAttributes(MailFolder folder);

    List<MailFolder> getRootMailFoldersWithReadingRights(FaxToMailUser user);

    List<MailFolder> getRootMailFoldersWithMoveRights(FaxToMailUser user);

    List<MailFolder> getMailFolders(Collection<String> ids);

    Collection<MailFolder> getFoldersWithWaitingState(Set<WaitingState> waitingStates);

    Map<String, Long> getMailFoldersUsage();

    MailFolder getFolderForFaxToMailUser(FaxToMailUser customerResponsible);

    MailFolder getMailFolder(String topiaId);

    MailFilter findMailFilter(Email email, Set<String> modifiedProperties, List<Address> recipients);

    Collection<FaxToMailUser> getUsersForFolder(String topiaId);
}
