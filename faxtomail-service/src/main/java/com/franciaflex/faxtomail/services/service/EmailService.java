package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.EmailFilter;
import com.franciaflex.faxtomail.persistence.entities.ExtensionCommand;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.GeneratedPDFPage;
import com.franciaflex.faxtomail.persistence.entities.HistoryType;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.MailLock;
import com.franciaflex.faxtomail.persistence.entities.OriginalEmail;
import com.franciaflex.faxtomail.persistence.entities.Reply;
import com.franciaflex.faxtomail.persistence.entities.ReplyContent;
import com.franciaflex.faxtomail.persistence.entities.SearchFilter;
import com.franciaflex.faxtomail.services.FaxToMailService;
import com.franciaflex.faxtomail.beans.QuantitiesByRange;
import com.franciaflex.faxtomail.services.service.exceptions.AlreadyLockedMailException;
import com.franciaflex.faxtomail.services.service.exceptions.FolderNotReadableException;
import com.franciaflex.faxtomail.services.service.exceptions.InvalidClientException;
import com.franciaflex.faxtomail.services.service.imports.ArchiveImportResult;
import com.itextpdf.text.DocumentException;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.mail.EmailException;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.sql.Blob;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Kevin Morin (Code Lutin)
 */
public interface EmailService extends FaxToMailService {
    Email getEmailById(String id);

    Email getFullEmailById(String id);

    Email getFullEmailById(String id, FaxToMailUser user);

    Email saveEmail(Email email, FaxToMailUser user, String... modifiedFields) throws InvalidClientException;

    Email saveEmail(Email email, Collection<Attachment> attachments, Collection<Reply> replies,
            FaxToMailUser user, String... modifiedFields) throws InvalidClientException;

    List<Email> takeBy(List<Email> email, FaxToMailUser user);

    void transmitPendingDemandsToEdi();

    Set<Object> getDistinctValues(MailFolder folder, String[] properties, boolean sum);

    PaginationResult<Email> getEmailForFolder(MailFolder folder, FaxToMailUser currentUser, EmailFilter filter, PaginationParameter page);

    List<MailFolder> getChildrenRecursively(MailFolder folder);

    QuantitiesByRange computeQuantitiesByRange(MailFolder rootFolder);

    Email addToHistory(String emailId, HistoryType type, FaxToMailUser user, Date date, String... fields);

    Email openEmail(String emailId, FaxToMailUser user, boolean takeEmail) throws FolderNotReadableException;

    Email lockEmail(String emailId, FaxToMailUser currentUser) throws AlreadyLockedMailException, FolderNotReadableException;

    void unlockEmail(String emailId);

    PaginationResult<Email> search(SearchFilter emailFilter, FaxToMailUser user, PaginationParameter pagination);

    Collection<Email> searchArchives(String commandQuotationNumber, String company);

    Email groupEmails(String email1Id, String email2Id, FaxToMailUser user);

    Email reply(String from, String to, String cc, String bcc, String subject,
                String content, Collection<AttachmentFile> attachments,
                String originalEmailId, FaxToMailUser user) throws EmailException, MessagingException, IOException;

    /**
     * Get reply content on demand for lazy loading.
     * 
     * @param replyId reply topia id
     * @return reply content for reply id.
     */
    ReplyContent getReplyContent(String replyId);

    void transmit(Collection<String> emailIds, MailFolder newFolder, FaxToMailUser currentUser);

    AttachmentFile getAttachmentFileFromStream(InputStream contentStream);

    AttachmentFile getAttachmentFile(String attachmentId, boolean original);

    Collection<GeneratedPDFPage> getGeneratedPDFPage(String attachmentId);

    List<MailLock> getAllMailLocks();

    void unlockMails(List<String> mailLockIds);

    void sendHtmlEmail(String from, String to, String subject, String content, String signing)
            throws EmailException;

    void updateRangeRowsWithEdiReturns();

    OriginalEmail originalEmailFromMessage(MimeMessage message, Charset charset) throws MessagingException, IOException;

    AttachmentFile getEmailDetailAsAttachment(Email email);

    /**
     * Retourne le nombre de mail archivé.
     * 
     * @return le nombre de mail archivé
     */
    long getArchivedMailCount();

    /**
     *
     * @return
     */
    List<Email> getArchivedMailNotProcessed(int limit);

    /**
     * Import archive from input stream.
     * 
     * @param is input stream of csv file
     * @param attachmentBase base folder containing attachment listed in csv file
     */
    ArchiveImportResult importArchive(InputStream is, File attachmentBase);

    /**
     * Decompose a multipart part.
     * - sets the email content if the part contains a text bodypart
     * - adds attachments to the email
     *
     * @param part the part to decompose
     * @throws Exception
     */
    List<String> decomposeMultipartEmail(List<Attachment> attachments, Part part) throws Exception;

    List<Attachment> extractAttachmentsFromMessage(MimeMessage message) throws Exception;

    /**
     * Retourne un input stream sur une piece jointe convertie ou pas.
     *
     * @param attachment to convert
     * @return attachment
     * @throws IOException
     */
    Attachment convertIfNecessary(Attachment attachment) throws IOException;

    /**
     * Convert attachment to pdf.
     *
     * @param attachment attachment to convert
     * @param extensionCommand command
     * @return
     * @throws IOException
     */
    Attachment convertToPdf(Attachment attachment, ExtensionCommand extensionCommand) throws IOException;

    Attachment convertTextToPdf(String content, String name) throws IOException, DocumentException;

    /**
     * Convert html content to image.
     *
     * @param attachments attachment content to link content to
     * @param content html content
     * @param name result attachment name
     * @return image attachment or {@code null} if content can't be converted
     * @throws IOException
     */
    Attachment convertHTMLToPdf(List<Attachment> attachments, List<String> content, String name) throws IOException;

    void deleteTempAttachmentFiles(List<Attachment> attachments);

    GeneratedPDFPage createGeneratedPDFPage(Attachment attachment, BufferedImage image);

    String extractHtmlContent(List<Attachment> attachments, List<String> contentList) throws URIException;
}
