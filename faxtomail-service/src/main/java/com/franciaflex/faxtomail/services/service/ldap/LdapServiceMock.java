package com.franciaflex.faxtomail.services.service.ldap;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroup;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroupImpl;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroupTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserImpl;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserTopiaDao;
import com.franciaflex.faxtomail.services.service.LdapServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.List;

/**
 * Surcharge les methodes qui utilisent le serveur ldap pour pouvoir fonctionner sans serveur ldap.
 * 
 * @author Eric Chatellier
 */
public class LdapServiceMock extends LdapServiceImpl {

    protected FaxToMailUser getFakeFaxToMailUser() {
        FaxToMailUserTopiaDao userDao = getPersistenceContext().getFaxToMailUserDao();
        FaxToMailUserGroupTopiaDao groupDao = getPersistenceContext().getFaxToMailUserGroupDao();

        FaxToMailUser fakeUser = userDao.forAll().setOrderByArguments(TopiaEntity.PROPERTY_TOPIA_ID).findFirstOrNull();

        // create user
        if (fakeUser == null) {
            fakeUser = new FaxToMailUserImpl();
            fakeUser.setTrigraph("fkr");
            fakeUser.setFirstName("Test ldap");
            fakeUser.setLastName("Test ldap");
            fakeUser.setLogin("test");
            fakeUser = userDao.create(fakeUser);
        }
        
        // add user to fake group
        FaxToMailUserGroup fakeGroup = null;
        List<String> adminGroups = getApplicationConfig().getLdapAdminGroups();
        if (CollectionUtils.isNotEmpty(adminGroups)) {
            fakeGroup = groupDao.forCompleteNameEquals(adminGroups.get(0)).findAnyOrNull();
            if (fakeGroup == null) {
                fakeGroup = new FaxToMailUserGroupImpl();
                fakeGroup.setName("fake group");
                fakeGroup.setCompleteName(adminGroups.get(0));
                fakeGroup = groupDao.create(fakeGroup);
            }
            
            if (!fakeUser.containsUserGroups(fakeGroup)) {
                fakeUser.addUserGroups(fakeGroup);
            }
        }

        getPersistenceContext().commit();
        return fakeUser;
    }

    @Override
    public void updateLdapData() {
        // do nothing here
    }

    @Override
    public FaxToMailUser getUserBean(String userTopiaId) {
        FaxToMailUserTopiaDao faxtomailUserDao = getPersistenceContext().getFaxToMailUserDao();
        FaxToMailUser result = faxtomailUserDao.forTopiaIdEquals(userTopiaId).findUnique();
        // force collection loading to force non lazy collections
        result.isAffectedFoldersEmpty();
        result.isUserGroupsEmpty();
        return result;
    }

    @Override
    public FaxToMailUser authenticateUser(String login, String password) throws AuthenticationException {
        return getUserFromPrincipal(login);
    }

    @Override
    public FaxToMailUser getUserFromPrincipal(String principal) throws AuthenticationException {
        FaxToMailUserTopiaDao faxtomailUserDao = getPersistenceContext().getFaxToMailUserDao();
        FaxToMailUser result = faxtomailUserDao.forLoginEquals(principal.toLowerCase()).findUniqueOrNull();
        if (result == null) {
            result = getFakeFaxToMailUser();
        }
        // force collection loading to force non lazy collections
        result.isAffectedFoldersEmpty();
        result.isUserGroupsEmpty();

        return result;
    }
}
