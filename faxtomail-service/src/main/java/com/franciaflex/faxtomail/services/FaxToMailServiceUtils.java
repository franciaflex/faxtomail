package com.franciaflex.faxtomail.services;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.google.common.base.Preconditions;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.ContentType;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class FaxToMailServiceUtils {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(FaxToMailServiceUtils.class);

    public static final Pattern IMG_SRC_PATTERN = Pattern.compile("src=([\"'])(.+?)([\"'])");

    private FaxToMailServiceUtils(){}

    public static String addFaxDomainToFaxNumber(String faxNumber, MailFolder folder) {
        while (!folder.isUseCurrentLevelFaxDomain()
               && folder.getParent() != null) {
            folder = folder.getParent();
        }
        int endIndex = faxNumber.indexOf('@');
        if (endIndex >= 0) {
            faxNumber = faxNumber.substring(0, endIndex);
        }
        faxNumber += "@" + folder.getFaxDomain();
        return faxNumber;
    }

    public static String getFaxFromNumber(MailFolder folder) {
        while (!folder.isUseCurrentLevelFaxFromNumber()
                && folder.getParent() != null) {
            folder = folder.getParent();
        }
        return folder.getFaxFromNumber();
    }

    public static Charset getCharset(Part part) throws MessagingException {
        ContentType contentType = new ContentType(part.getContentType());
        String charsetName = contentType.getParameter("charset");
        Charset charset;
        try {
            charset = Charsets.toCharset(charsetName);

        } catch (UnsupportedCharsetException e) {
            charset = Charset.defaultCharset();
        }
        return charset;
    }

    /**
     * Return true if given collection is not null and contains requested value.
     *
     * @param coll  collection
     * @param value value
     * @return true if set contains value
     */
    public static <T extends Enum<T>> boolean contains(Collection<T> coll, T value) {
        boolean result = false;
        if (coll != null) {
            result = coll.contains(value);
        }
        return result;
    }

    /**
     * Compute mail folder path (separated by /) from root to current.
     *
     * @param folder folder to get path
     * @return full mail folder path
     */
    public static String getFullMailFolderPath(MailFolder folder) {
        StringBuilder sb = new StringBuilder(folder.getName());
        MailFolder loopFolder = folder.getParent();
        while (loopFolder != null) {
            sb.insert(0, "/");
            sb.insert(0, loopFolder.getName());
            loopFolder = loopFolder.getParent();
        }
        return sb.toString();
    }

    /**
     * Sometimes, the urls of the images in an email contains spaces or an unknown protocol.
     * But URI.parse(uri) does not accept spaces as a valid character nor unknown protocols..
     * This method encode the url for them to be compatible with URI.parse
     * or replace the url with the one of the error image if the protocol is wrong.
     *
     * @param emailContent the content of the email
     * @return the email content with the url of the images encoded to be parsed by URI
     * @throws URIException
     */
    public static String encodeImageSourcesInEmail(String emailContent, String defaultImage) throws URIException {
        StringBuffer emailContentBuffer = new StringBuffer();
        Matcher imgSrcMatcher = IMG_SRC_PATTERN.matcher(emailContent);
        while (imgSrcMatcher.find()) {
            String imgSrcUrl = imgSrcMatcher.group(2);

            String correctImgUrl = URIUtil.encodeQuery(imgSrcUrl);
            try {
                // test if the url is correct (cf #7855)
                new URL(correctImgUrl);

            } catch (MalformedURLException e) {
                //if incorrect, replace it with the default image
                if (log.isErrorEnabled()) {
                    log.error("the url of the image " + imgSrcUrl + " is not correct, replace it with the default image");
                }

                GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
                correctImgUrl = ge.isHeadlessInstance() ? "" : defaultImage;
            }

            imgSrcMatcher.appendReplacement(emailContentBuffer, "src=" + imgSrcMatcher.group(1) + correctImgUrl + imgSrcMatcher.group(3));
        }
        imgSrcMatcher.appendTail(emailContentBuffer);
        return emailContentBuffer.toString();
    }

    public static String getDecodedSubject(String subject) throws UnsupportedEncodingException {
        Preconditions.checkNotNull(subject);

        Pattern pattern=Pattern.compile("=\\?([^?]+)\\?([^?]+)\\?([^?]+)\\?=");
        Matcher m=pattern.matcher(subject);

        ArrayList<String> encodedParts=new ArrayList<>();

        while(m.find()){
            encodedParts.add(m.group(0));
        }

        if(!encodedParts.isEmpty()){
            try {
                for(String encoded:encodedParts) {
                    String correctlyEncoded = encoded;
                    if (correctlyEncoded.startsWith("=?iso-")) {
                        correctlyEncoded = correctlyEncoded.replace(" ", "=20");
                    }
                    subject=subject.replace(encoded, MimeUtility.decodeText(correctlyEncoded));
                }
            } catch(Exception ex) {
                log.info("Error decoding subject : ", ex);
            }
        }

        return subject;
    }

    public static String getDecodedFrom(String from) {
        Preconditions.checkNotNull(from);

        // some sender are like "toto tutu<toto.tutu73@gmail.com>"
        // some sender are like "toto tutu@ <gmail.com toto.tutu73@gmail.com>"
        // the regex is to extract email address from it
        from = from.replaceFirst("^.*<(.*\\s)?([\\S]+@[\\S]+)(\\s.*)?>$", "$2");

        return from.toLowerCase();
    }

    public static String getDomainForEmailAddress(String emailAddress) {
        Preconditions.checkNotNull(emailAddress);
        return emailAddress.substring(emailAddress.lastIndexOf('@') + 1);
    }

    public static String getTextFromMessage(MimeMessage message) throws IOException, MessagingException {
        // convertit le contenu texte en PDF
        String text;

        try {
            text = (String) message.getContent();

        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("Error while getting the text from the message, reading the raw data", e);
            }
            InputStream inputStream = message.getRawInputStream();
            Charset charset = getCharset(message);
            text = getTextFromInputStream(inputStream, charset);
        }
        return text;
    }

    public static String getTextFromPart(Part part) throws IOException, MessagingException {
        // convertit le contenu texte en PDF
        Charset charset = getCharset(part);
        return getTextFromInputStream(part.getInputStream(), charset);
    }

    public static String getTextFromInputStream(InputStream inputStream, Charset charset) throws IOException {
        return IOUtils.toString(inputStream, charset);
    }

    public static List<String> convertAddressesToStrings(Address[] addresses) {
        List<String> result = new ArrayList<>();
        if (addresses != null) {
            for (Address address : addresses) {
                result.add(getDecodedFrom(address.toString()));
            }
        }
        return result;
    }
}
