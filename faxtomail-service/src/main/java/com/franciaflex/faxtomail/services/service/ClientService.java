package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.services.FaxToMailService;

import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 */
public interface ClientService extends FaxToMailService {

    List<Client> getClientForEmailAddress(String emailAddress, Email email, String company, List<String> brand);

    Client getClientForCode(String code, String company);

    List<Client> getClientsForFolder(MailFolder folder);

    List<Client> getClientsForFolder(MailFolder folder, String filter);

    void updateNewClients();

    List<Client> getAllClientsForUser(FaxToMailUser currentUser);

    List<Client> getAllClientsForUserFilter(FaxToMailUser user, String filter);

    List<Client> getAllClientsForEmailOrFax(String query);
}
