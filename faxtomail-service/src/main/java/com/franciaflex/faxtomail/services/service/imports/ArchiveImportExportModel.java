package com.franciaflex.faxtomail.services.service.imports;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.HasLabel;
import com.franciaflex.faxtomail.persistence.entities.Priority;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.persistence.entities.Email;
import org.nuiton.csv.Common;
import org.nuiton.csv.ValueParserFormatter;
import org.nuiton.csv.ext.AbstractImportExportModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Modele d'import des archives, défini comme suit:
 * 
 * receptionDate;projectReference;sender;fax;recipient;object;archiveDate;companyReference;
 * originalEmail;comment;etatAttente;demandType;priority;mailFolder;client-code;client-brand;attachments
 * 
 * @author Eric Chatellier
 */
public class ArchiveImportExportModel extends AbstractImportExportModel<ArchiveImportExportBean> {

    /**
     * Parse date pattern : dd/MM/yyyy hh:mm:ss.
     */
    protected static final ValueParserFormatter<Date> dateValueParserFormatter = new ValueParserFormatter<Date>() {
        protected DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        @Override
        public Date parse(String value) throws ParseException {
            return dateFormat.parse(value);
        }

        @Override
        public String format(Date value) {
            return dateFormat.format(value);
        }
    };

    public ArchiveImportExportModel(char separator,
            final Map<String, WaitingState> waitingStates,
            final Map<String, DemandType> demandTypes,
            final Map<String, Priority> priorities) {
        super(separator);

        newColumnForImportExport("receptionDate", Email.PROPERTY_RECEPTION_DATE, dateValueParserFormatter);
        newColumnForImportExport("projectReference", Email.PROPERTY_PROJECT_REFERENCE);
        newColumnForImportExport("sender", Email.PROPERTY_SENDER, new ValueParserFormatter<Object>() {
            @Override
            public Object parse(String value) throws ParseException {
                value = value.replaceFirst("^.*<(.*)>$", "$1");
                value = value.toLowerCase();
                return value;
            }

            @Override
            public String format(Object value) {
                return value != null ? value.toString() : "";
            }
        });
        newColumnForImportExport("fax", Email.PROPERTY_FAX, Common.PRIMITIVE_BOOLEAN);
        newColumnForImportExport("recipient", Email.PROPERTY_RECIPIENT);
        newColumnForImportExport("object", Email.PROPERTY_OBJECT);
        newColumnForImportExport("archiveDate", Email.PROPERTY_ARCHIVE_DATE, dateValueParserFormatter);
        newColumnForImportExport("companyReference", Email.PROPERTY_COMPANY_REFERENCE);
        newColumnForImportExport("originalEmail", ArchiveImportExportBean.PROPERTY_ORIGINAL_EMAIL_CONTENT);
        newColumnForImportExport("comment", Email.PROPERTY_COMMENT);
        newColumnForImportExport("etatAttente", Email.PROPERTY_WAITING_STATE, new HasLabelValueParserFormatter<WaitingState>() {
            @Override
            public WaitingState parse(String value) throws ParseException {
                return waitingStates.get(value);
            }
        });
        newColumnForImportExport("demandType", Email.PROPERTY_DEMAND_TYPE, new HasLabelValueParserFormatter<DemandType>() {
            @Override
            public DemandType parse(String value) throws ParseException {
                return demandTypes.get(value);
            }
        });
        newColumnForImportExport("priority", Email.PROPERTY_PRIORITY, new HasLabelValueParserFormatter<Priority>() {
            @Override
            public Priority parse(String value) throws ParseException {
                return priorities.get(value);
            }
        });
        newColumnForImportExport("mailFolder", ArchiveImportExportBean.PROPERTY_MAIL_FOLDER_PATHS);
        newColumnForImportExport("client-code", ArchiveImportExportBean.PROPERTY_CLIENT_CODE);
        newOptionalColumn("client-brand", ArchiveImportExportBean.PROPERTY_CLIENT_BRAND);
        newColumnForExport("client-brand", ArchiveImportExportBean.PROPERTY_CLIENT_BRAND);
        newColumnForImportExport("attachments", ArchiveImportExportBean.PROPERTY_ATTACHMENT_PATHS);

        newOptionalColumn("error", ArchiveImportExportBean.PROPERTY_ERROR);
        newColumnForExport("error", ArchiveImportExportBean.PROPERTY_ERROR);
    }

    @Override
    public ArchiveImportExportBean newEmptyInstance() {
        return new ArchiveImportExportBean();
    }

    protected abstract class HasLabelValueParserFormatter<E extends HasLabel> implements ValueParserFormatter<E> {

        @Override
        public String format(E value) {
            return value != null ? value.getLabel() : "";
        }
    }
}
