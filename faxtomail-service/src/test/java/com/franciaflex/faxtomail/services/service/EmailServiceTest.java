package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.beans.Quantities;
import com.franciaflex.faxtomail.beans.QuantitiesByRange;
import com.franciaflex.faxtomail.persistence.entities.Attachment;
import com.franciaflex.faxtomail.persistence.entities.AttachmentFile;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.EmailFilter;
import com.franciaflex.faxtomail.persistence.entities.EmailGroup;
import com.franciaflex.faxtomail.persistence.entities.EmailTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.MailFolderTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.persistence.entities.SearchFilter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.util.MimeMessageUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import javax.imageio.ImageIO;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

/**
 * Email service tests.
 * 
 * @author Eric Chatellier
 */
public class EmailServiceTest extends AbstractFaxToMailServiceTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EmailServiceTest.class);

    protected EmailService service;

    protected ReferentielService referentielService;

    protected UserService userService;

    protected ClientService clientService;

    @Before
    public void setUp() throws IOException {
        service = newService(EmailService.class);
        referentielService = newService(ReferentielService.class);
        userService = newService(UserService.class);
        clientService = newService(ClientService.class);
        newService(InitTestData.class).initTestData();
    }

    @Test
    public void testSearch() {

        PaginationParameter paginationParameter = PaginationParameter.of(0, 50);

        // empty filter
        SearchFilter filter = new SearchFilter();
        PaginationResult<Email> emails = service.search(filter, getCurrentUser(), paginationParameter);
        Assert.assertEquals(10, emails.getCount());

        // client code filter
        filter.setClient(clientService.getClientForCode("9915", null));
        emails = service.search(filter, getCurrentUser(), paginationParameter);
        Assert.assertEquals(10, emails.getCount());

        // client code filter + subject filter
        filter.setDemandSubject("A+");
        emails = service.search(filter, getCurrentUser(), paginationParameter);
        Assert.assertEquals(0, emails.getCount());

        // some other fields just to test query syntax
        filter.setMinReceptionDate(getServiceContext().getNow());
        filter.setMaxReceptionDate(getServiceContext().getNow());
        filter.setTakenBy(userService.getUserByLogin("cmarquis"));
        filter.setMinModificationDate(getServiceContext().getNow());
        filter.setMaxModificationDate(getServiceContext().getNow());
        filter.setModifiedBy(userService.getUserByLogin("cmarquis"));
        filter.setMinTransferDate(getServiceContext().getNow());
        filter.setMaxTransferDate(getServiceContext().getNow());
        filter.setTransferBy(userService.getUserByLogin("cmarquis"));
        filter.setMinArchivedDate(getServiceContext().getNow());
        filter.setMaxArchivedDate(getServiceContext().getNow());
        filter.setArchivedBy(userService.getUserByLogin("cmarquis"));
        filter.setMinPrintingDate(getServiceContext().getNow());
        filter.setMaxPrintingDate(getServiceContext().getNow());
        filter.setPrintingBy(userService.getUserByLogin("cmarquis"));

        filter.setSender("test");
        filter.setBody("test");
        filter.setDemandStatus(Collections.singletonList(DemandStatus.ARCHIVED));
        filter.setGamme(Collections.singletonList(referentielService.getAllRange().get(0)));
        filter.setPriority(referentielService.getAllPriority());
        filter.setWaitingStates(Collections.singletonList(referentielService.getAllWaitingState().get(0)));
        filter.setEdiCodeNumber("test");
        filter.setLocalReference("test");
        filter.setMessage("test");
        filter.setProjectReference("test");
        emails = service.search(filter, getCurrentUser(), paginationParameter);
        Assert.assertEquals(0, emails.getCount()); 
    }

    /**
     * Generate email detail as pdf test.
     */
    @Test
    public void testEmailDetailsPdf() {
        
        // select a random test email
        PaginationParameter paginationParameter = PaginationParameter.of(0, 1);
        SearchFilter filter = new SearchFilter();
        PaginationResult<Email> emails = service.search(filter, getCurrentUser(), paginationParameter);
        Email testEmail = emails.getElements().get(0);
        
        // test pdf generation
        AttachmentFile attachmentFile = service.getEmailDetailAsAttachment(testEmail);
        Assert.assertNotNull(attachmentFile);
    }

    /**
     * Test que le contenu textuel du pdf généré.
     */
    @Test
    public void testEmailDetails() {
        // select a random test email
        PaginationParameter paginationParameter = PaginationParameter.of(0, 1);
        SearchFilter filter = new SearchFilter();
        PaginationResult<Email> emails = service.search(filter, getCurrentUser(), paginationParameter);
        Email testEmail = emails.getElements().get(0);

        // get html content
        String content = ((EmailServiceImpl)service).getEmailDetailAsHtml(testEmail);
        Assert.assertTrue(content.contains(testEmail.getObject()));
    }

    @Test
    public void testGetEmailForFolder() {

        // tests fetch + pagination

        PaginationParameter page = PaginationParameter.of(0, 4, EmailTopiaDao.EMAIL_IDENTIFIER + "." + Email.PROPERTY_RECEPTION_DATE, false);
        MailFolderTopiaDao mailFolderDao = getServiceContext().getPersistenceContext().getMailFolderDao();
        MailFolder cyril = mailFolderDao.forNameEquals("Cyril").findUnique();
        
        // FIXME required for getEmailForFolder security check
        cyril.setFolderReadable(true);

        PaginationResult<Email> page1 = service.getEmailForFolder(cyril, getCurrentUser(), new EmailFilter(), page);
        Assert.assertEquals(5, page1.getCount()); // + 5 archived now
        Assert.assertEquals(4, page1.getElements().size());

        PaginationResult<Email> page2 = service.getEmailForFolder(cyril, getCurrentUser(), new EmailFilter(), page1.getNextPage());
        Assert.assertEquals(5, page2.getCount()); // + 5 archived now
        Assert.assertEquals(1, page2.getElements().size());

        Set<String> page1Ids = Sets.newHashSet(Iterables.transform(page1.getElements(), TopiaEntities.getTopiaIdFunction()));
        Set<String> page2Ids = Sets.newHashSet(Iterables.transform(page2.getElements(), TopiaEntities.getTopiaIdFunction()));
        Assert.assertTrue(Sets.intersection(page1Ids, page2Ids).isEmpty());

    }

    /**
     * Test de l'import de la reprise des archives.
     * 
     * @throws IOException 
     */
    @Test
    public void testImportArchive() throws IOException {
        try (InputStream is = EmailServiceTest.class.getResourceAsStream("/archives/archives.csv")) {
            File attachmentBase = new File("src/test/resources/archives");
            service.importArchive(is, attachmentBase);
        }
    }

    /**
     * Test le la fonction de calcul des quantités par gammes qui fesait une erreur de groupBy en
     * 1.0.0-rc-3.
     */
    @Test
    public void testComputeQuantititesByRange() {
        MailFolderTopiaDao mailFolderDao = getServiceContext().getPersistenceContext().getMailFolderDao();
        MailFolder cyril = mailFolderDao.forNameEquals("Cyril").findUnique();

        QuantitiesByRange quantitiesByRange = service.computeQuantitiesByRange(cyril);
        Assert.assertEquals(4, quantitiesByRange.getInProgressQuantitiesByRange().size());

        // recupere la premiere lignes
        Entry<Range, Quantities> entry = quantitiesByRange.getInProgressQuantitiesByRange().entrySet().iterator().next();
        Assert.assertEquals("Bas de gamme", entry.getKey().getLabel());
        Assert.assertEquals(250, entry.getValue().getProductQuantity().longValue());
        Assert.assertEquals(300, entry.getValue().getSavQuantity().longValue());
        // NPE : Assert.assertEquals(600, entry.getValue()[2].longValue());
    }

    /**
     * Teste le fonctionnement de search archives qui contennait une erreur de synthax en 1.1.
     */
    @Test
    public void testSearchArchives() {
        // il y a une compagnie de test FXCOMP
        // les range row sont préfixé par FXRR
        Collection<Email> emails = service.searchArchives("FXRR", "FXCOMP");
        Assert.assertFalse(emails.isEmpty());
    }

    /**
     * Teste le groupement d'emails
     */
    @Test
    public void testGroupEmails() {
        MailFolderTopiaDao mailFolderDao = getServiceContext().getPersistenceContext().getMailFolderDao();
        MailFolder cyrilFolder = mailFolderDao.forNameEquals("Cyril").findUnique();

        FaxToMailUser cyril = userService.getUserByLogin("cbaillet");

        EmailTopiaDao emailDao = getServiceContext().getPersistenceContext().getEmailDao();
        List<Email> emails = emailDao.forMailFolderEquals(cyrilFolder).addEquals(Email.PROPERTY_ARCHIVE_DATE, null).find(0, 6);

        // on groupe 2 éléments
        Email email0 = emails.get(0);
        Email email1 = emails.get(1);
        Email groupedEmail = service.groupEmails(email0.getTopiaId(), email1.getTopiaId(), cyril);
        Assert.assertEquals(2, groupedEmail.getEmailGroup().sizeEmail());

        // on groupe un élément non groupé à un élément groupé
        Email email2 = emails.get(2);
        groupedEmail = service.groupEmails(email2.getTopiaId(), email0.getTopiaId(), cyril);
        Assert.assertEquals(3, groupedEmail.getEmailGroup().sizeEmail());

        // on groupe un élément groupé à un élément non groupé
        Email email3 = emails.get(3);
        groupedEmail = service.groupEmails(email1.getTopiaId(), email3.getTopiaId(), cyril);
        Assert.assertEquals(4, groupedEmail.getEmailGroup().sizeEmail());

        Email email4 = emails.get(4);
        Email email5 = emails.get(5);
        groupedEmail = service.groupEmails(email4.getTopiaId(), email5.getTopiaId(), cyril);
        Assert.assertEquals(2, groupedEmail.getEmailGroup().sizeEmail());

        // on groupe 2 éléments qui sont déjà groupés
        groupedEmail = service.groupEmails(email0.getTopiaId(), email5.getTopiaId(), cyril);
        Assert.assertEquals(6, groupedEmail.getEmailGroup().sizeEmail());

        // on vérifie que l'email dont le groupe a été mergé a bien le nouveau groupe
        Email email4Reloaded = service.getEmailById(email4.getTopiaId());
        EmailGroup email4ReloadedEmailGroup = email4Reloaded.getEmailGroup();
        Assert.assertEquals(groupedEmail.getEmailGroup(), email4ReloadedEmailGroup);


    }

    @Test
    public void testSetTakenBy(){
        MailFolderTopiaDao mailFolderDao = getServiceContext().getPersistenceContext().getMailFolderDao();
        MailFolder cyrilFolder = mailFolderDao.forNameEquals("Cyril").findUnique();

        FaxToMailUser cmarquis = userService.getUserByLogin("cmarquis");

        EmailTopiaDao emailDao = getServiceContext().getPersistenceContext().getEmailDao();

        List<Email> emails = emailDao.forMailFolderEquals(cyrilFolder).addEquals(Email.PROPERTY_ARCHIVE_DATE, null).find(0, 1);

        Email email = emails.get(0);

        //Check we can rightfully test data
        Assert.assertNotEquals(cmarquis,email.getTakenBy());

        service.takeBy(emails, cmarquis);

        Email emailToTest = service.getEmailById(email.getTopiaId());

        Assert.assertEquals(emailToTest.getSubject(), email.getSubject());

        Assert.assertEquals(cmarquis.getTrigraph(),emailToTest.getTakenBy().getTrigraph());

        //put back taken by to null and test it works
        service.takeBy(emails, null);

        emailToTest = service.getEmailById(email.getTopiaId());

        Assert.assertNull(emailToTest.getTakenBy());


        //TODO should test history to
    }

    @Test
    public void test6863() {
        testConvertHTMLToPdf("test6863");
    }

    @Test
    public void test7716() {
        testConvertHTMLToPdf("test7716");
    }

    @Test
    public void test7740() {
        testConvertHTMLToPdf("test7740");
    }

    @Test
    public void test7855() {
        testConvertHTMLToPdf("test7855");
    }

    /**
     * Test qui ne fail jamais, mais permet de vérifier la génération de PDFs très très larges
     */
    @Test
    public void test10015() {
        testConvertHTMLToPdf("test10015");

    }

    /**
     * Test qui ne fail jamais, mais permet de vérifier la génération de PDFs très larges
     */
    @Test
    public void test10015_2() {
        testConvertHTMLToPdf("test10015-2");

    }

    /**
     * Test qui permet de vérifier la génération de sPDF pour les mails contenant des font-size:0
     */
    @Test
    public void test10123() {
        testConvertHTMLToPdf("test10123");

    }

    protected void testConvertHTMLToPdf(String emailId) {

        File tempDirectory = FileUtils.getTempDirectory();

        try {
            File email = new File("src/test/resources/emails/" + emailId + ".eml");
            String emailContent = FileUtils.readFileToString(email, Charset.defaultCharset());

            Properties properties = new Properties();
            // set the mail.mime.address.strict to false to avoid
            // javax.mail.internet.AddressException: Domain contains illegal character errors when recipients contains []
            properties.setProperty("mail.mime.address.strict", "false");
            Session session = Session.getInstance(properties);

            MimeMessage message = MimeMessageUtils.createMimeMessage(session, emailContent);

            List<Attachment> attachments = service.extractAttachmentsFromMessage(message);

            for (Attachment attachment : attachments) {
                Files.copy(attachment.getOriginalFile().getFile(), new File(tempDirectory, attachment.getOriginalFileName()));
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Error while handling the email " + emailId, e);
            }
            Assert.fail(e.getMessage());
        }

    }

    /**
     * Test that jbig2 dependency is present so that pdf generation is correct
     */
    @Test
    public void test10004jbig2dependency() throws IOException {
        File pdf = new File("src/test/resources/pdf/10004.pdf");
        File image = new File("src/test/resources/pdf/10004-page1.png");


        //Pre-generate PDF rendering
        PDDocument pdDocument = PDDocument.load(pdf);
        PDFRenderer renderer = new PDFRenderer(pdDocument);
        BufferedImage pageImage = renderer.renderImage(0, 1.5f);

        //Get properly generated image
        BufferedImage in = ImageIO.read(image);

        Assert.assertTrue(compareImagePixels(in, pageImage));
    }

    /**
     * Test that generatedPdfPages are always in order
     */
//    public void testGeneratedPDFPageOrder() throws IOException {
//        File pdf = new File("src/test/resources/pdf/10004.pdf");
//        File image = new File("src/test/resources/pdf/10004-page1.png");
//
//
//        //Pre-generate PDF rendering
//        PDDocument pdDocument = PDDocument.load(pdf);
//        PDFRenderer renderer = new PDFRenderer(pdDocument);
//        BufferedImage pageImage = renderer.renderImage(0, 1.5f);
//
//        //Get properly generated image
//        BufferedImage in = ImageIO.read(image);
//
//        Assert.assertTrue(compareImagePixels(in, pageImage));
//    }

    /**
     * Test that Imaging (JAI) Image I/O Tools is present so that pdf generation is correct
     */
    @Test
    public void test10075() throws IOException {
        File pdf = new File("src/test/resources/pdf/10075.pdf");

        try {
            //Pre-generate PDF rendering
            PDDocument pdDocument = PDDocument.load(pdf);
            PDFRenderer renderer = new PDFRenderer(pdDocument);
            renderer.renderImage(0, 1.5f);
        } catch (Error eee) {
            Assert.fail(eee.getMessage());
        }
    }




    private final int ALPHA = 24;
    private final int RED = 16;
    private final int GREEN = 8;
    private final int BLUE = 0;

    protected int[] getRGBA(BufferedImage img, int x, int y)
    {
        int[] color = new int[4];
        color[0]=getColor(img, x,y,RED);
        color[1]=getColor(img, x,y,GREEN);
        color[2]=getColor(img, x,y,BLUE);
        color[3]=getColor(img, x,y,ALPHA);
        return color;
    }

    protected static int getColor(BufferedImage img, int x, int y, int color)
    {
        int value=img.getRGB(x, y) >> color & 0xff;
        return value;
    }

    protected boolean compareImagePixels(BufferedImage biA, BufferedImage biB) {
        try {
            if (biA.getHeight() != biB.getHeight()){
                return false;
            }

            if (biA.getWidth() != biB.getWidth()){
                return false;
            }

            //loop on pixels to get their values
            for (int i=0;i<biA.getWidth();i++) {
                for (int j=0;j<biA.getHeight();j++){
                    int[] colorA =  getRGBA(biA,i,j);
                    int[] colorB =  getRGBA(biB,i,j);

                    for (int k=0; k<4; k++) {
                        if (colorA[k] != colorB[k]){
                            log.debug("Difference at pixel " + i+ ","+j+" channel " + k);
                            return false;
                        }
                    }
                }
            }
            return true;
        } catch (Exception e) {
            log.debug("Failed to compare image files ...");
            return false;
        }
    }


//    @Test
//    public void testSendPdfBlanc() {
//        testSendEmail("");
//    }
//
//    protected void testSendEmail(String emailId) {
//        try {
//            final String smtpUser = getApplicationConfig().getSmtpUser();
//            final String password = getApplicationConfig().getSmtpPassword();
//
//            Properties properties = new Properties();
//            // set the mail.mime.address.strict to false to avoid
//            // javax.mail.internet.AddressException: Domain contains illegal character errors when recipients contains []
//            properties.setProperty("mail.mime.address.strict", "false");
//
//            Session session = Session.getInstance(properties);
//            File email = new File("src/test/resources/emails/" + emailId + ".eml");
//            String emailContent = FileUtils.readFileToString(email);
//            MimeMessage message = MimeMessageUtils.createMimeMessage(session, emailContent);
//
//            Transport tr = session.getTransport("smtps");
//            tr.connect(getApplicationConfig().getSmtpHost(), getApplicationConfig().getSmtpPort(), smtpUser, password);
//            message.saveChanges();      // don't forget this
//
//            tr.sendMessage(message, new Address[]{new InternetAddress("test@test.com") });
//            tr.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * Helper method to generate image for each page of a pdf file
     *
     * @throws IOException
     */
    public void generateImageForEachPage() throws IOException {
        File pdf = new File("src/test/resources/pdf/10094.pdf");

        //Pre-generate PDF rendering
        try (PDDocument pdDocument = PDDocument.load(pdf)) {
            PDFRenderer renderer = new PDFRenderer(pdDocument);

            for (int i = 0; i < pdDocument.getNumberOfPages(); i++) {
                System.out.println("Generate page " + i);
                BufferedImage pageImage = renderer.renderImage(i, 1.5f);
                File tempFile = File.createTempFile("10094-" + i + "-", ".png");
                tempFile.createNewFile();
                System.out.println(tempFile.getName());

                try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                    ImageIO.write(pageImage, "png", baos);
                    baos.flush();
                    byte[] imageInByte = baos.toByteArray();
                    baos.close();

                    Files.write(imageInByte, tempFile);
                }
            }
        }

    }
}
