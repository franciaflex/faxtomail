package com.franciaflex.faxtomail.services.service.migration;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaApplicationContext;
import com.franciaflex.faxtomail.services.service.AbstractFaxToMailServiceTest;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.hibernate.cfg.Environment;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.flyway.TopiaFlywayService;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Restore la base de test dans src/test/resources/db et test que les migrations flyway fonctionnent bien.
 */
public class FlywayMigrationTest extends AbstractFaxToMailServiceTest {

    @Override
    protected Map<String, String> getDatabaseConfiguration(String dataBase) {
        Map<String, String> hibernateH2Config = new HashMap<String, String>();

        // copy database to test it outside source directory
        File tempDirFile = SystemUtils.getJavaIoTmpDir();
        databaseFile = new File(tempDirFile, dataBase);
        try {
            File db100rc2File = new File("src/test/resources/db/h2data-1_0_0_rc2.h2.db");
            FileUtils.copyFileToDirectory(db100rc2File, databaseFile);
        } catch (IOException ex) {
            Assert.fail("Can't copy db file to test dir");
            
        }

        // sqlserver config
        hibernateH2Config.put(Environment.DIALECT, "org.hibernate.dialect.H2Dialect");
        hibernateH2Config.put(Environment.DRIVER, "org.h2.Driver");
        hibernateH2Config.put(Environment.USER, "sa");
        hibernateH2Config.put(Environment.PASS, "");
        hibernateH2Config.put(Environment.HBM2DDL_AUTO, "");
        hibernateH2Config.put(Environment.URL, "jdbc:h2:file:" + databaseFile.getAbsolutePath() + "/h2data-1_0_0_rc2;MODE=MSSQLServer");
        
        // flyway config
        hibernateH2Config.put("topia.service.migration", FaxtomailFlywayMigrationService.class.getName());
        hibernateH2Config.put("topia.service.migration." + TopiaFlywayService.USE_MODEL_VERSION, "false");
        hibernateH2Config.put("topia.service.migration." + TopiaFlywayService.FLYWAY_INIT_VERSION, "1.0.0.438");

        return hibernateH2Config;
    }
    
    @Override
    protected void addMissingTable(FaxToMailTopiaApplicationContext applicationContext) {
        // on desactive la creation des tables dans ce cas car la base est existante
    }

    /**
     * Vérifie la version de la base de données après les migrations qui sont sensées être passée.
     */
    @Test
    public void testFlywayMigrations() {
        
        // recupere le nombre de fichier de migration
        List<File> migrationFiles = FileUtil.find(new File("src/main/resources/db/migration/h2"), ".*\\.sql", false);

        // recupere dans la base le nombre de migration qui sont passée
        Integer migrationCount = getServiceContext().getPersistenceContext().getSqlSupport().findSingleResult(new TopiaSqlQuery<Integer>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("select count(*) from \"schema_version\"");
            }

            @Override
            public Integer prepareResult(ResultSet set) throws SQLException {
                return set.getInt(1);
            }
        });

        // il y en a au moins 4 depuis la version 1.0.0-rc-3
        Assert.assertTrue(migrationCount >= 4);
        // +2 car il y a l'init en plus et les fichiers java
        Assert.assertEquals(migrationFiles.size() + 2, migrationCount.intValue());
    }
}
