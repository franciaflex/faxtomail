package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.DemandStatus;
import com.franciaflex.faxtomail.persistence.entities.DemandType;
import com.franciaflex.faxtomail.persistence.entities.Email;
import com.franciaflex.faxtomail.persistence.entities.EmailTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroup;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserGroupTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.History;
import com.franciaflex.faxtomail.persistence.entities.HistoryTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.HistoryType;
import com.franciaflex.faxtomail.persistence.entities.MailField;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.franciaflex.faxtomail.persistence.entities.MailFolderTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.OriginalEmail;
import com.franciaflex.faxtomail.persistence.entities.OriginalEmailTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.Priority;
import com.franciaflex.faxtomail.persistence.entities.Range;
import com.franciaflex.faxtomail.persistence.entities.RangeRow;
import com.franciaflex.faxtomail.persistence.entities.RangeRowTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.WaitingState;
import com.franciaflex.faxtomail.services.DecoratorService;
import com.franciaflex.faxtomail.services.FaxToMailServiceSupport;
import com.franciaflex.faxtomail.services.FaxToMailServiceUtils;
import com.franciaflex.faxtomail.services.service.exceptions.InvalidClientException;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Test service to manage test data.
 * 
 * @author Eric Chatellier
 */
public class InitTestDataImpl extends FaxToMailServiceSupport implements InitTestData {

    private static final Log log = LogFactory.getLog(InitTestDataImpl.class);

    /**
     * Import all test data.
     *
     * @throws IOException
     */
    @Override
    public void initTestData() throws IOException {
        if (log.isDebugEnabled()) {
            log.debug("Init test data");
        }
        ReferentielService referentielService = newService(ReferentielService.class);

        // import etat attentes
        List<WaitingState> waitingStates = null;
        try (InputStream is = InitTestDataImpl.class.getResourceAsStream("/csv/etatattentes.csv")) {
            waitingStates = referentielService.importWaitingStates(is);
        }

        // create test groups and users
        FaxToMailUserGroupTopiaDao faxToMailUserGroupDao = getPersistenceContext().getFaxToMailUserGroupDao();
        FaxToMailUserGroup group1 = faxToMailUserGroupDao.create(FaxToMailUserGroup.PROPERTY_NAME, "group1");
        FaxToMailUserGroup group2 = faxToMailUserGroupDao.create(FaxToMailUserGroup.PROPERTY_NAME, "group2");
        FaxToMailUserGroup group3 = faxToMailUserGroupDao.create(FaxToMailUserGroup.PROPERTY_NAME, "group3");
        Collection<FaxToMailUserGroup> groups12 = Lists.newArrayList(group1, group2);
        Collection<FaxToMailUserGroup> groups23 = Lists.newArrayList(group3, group2);

        FaxToMailUserTopiaDao userDao = getPersistenceContext().getFaxToMailUserDao();
        FaxToMailUser marc = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Lefèbvre",
                                            FaxToMailUser.PROPERTY_FIRST_NAME, "Marc",
                                            FaxToMailUser.PROPERTY_LOGIN, "mlefebvre",
                                            FaxToMailUser.PROPERTY_TRIGRAPH, "MLE",
                                            FaxToMailUser.PROPERTY_USER_GROUPS, groups12);
        FaxToMailUser cyril = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Baillet",
                                             FaxToMailUser.PROPERTY_FIRST_NAME, "Cyril",
                                             FaxToMailUser.PROPERTY_LOGIN, "cbaillet",
                                             FaxToMailUser.PROPERTY_TRIGRAPH, "CBA",
                                             FaxToMailUser.PROPERTY_USER_GROUPS, groups12);
        FaxToMailUser frederic = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Viala",
                                                FaxToMailUser.PROPERTY_FIRST_NAME, "Frédéric",
                                                FaxToMailUser.PROPERTY_LOGIN, "fviala",
                                                FaxToMailUser.PROPERTY_TRIGRAPH, "FVI",
                                                FaxToMailUser.PROPERTY_USER_GROUPS, groups12);
        FaxToMailUser claire = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Marquis",
                                              FaxToMailUser.PROPERTY_FIRST_NAME, "Claire",
                                              FaxToMailUser.PROPERTY_LOGIN, "cmarquis",
                                              FaxToMailUser.PROPERTY_TRIGRAPH, "CMA",
                                              FaxToMailUser.PROPERTY_USER_GROUPS, groups12);
        FaxToMailUser agathe = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Borde",
                                              FaxToMailUser.PROPERTY_FIRST_NAME, "Agathe",
                                              FaxToMailUser.PROPERTY_LOGIN, "aborde",
                                              FaxToMailUser.PROPERTY_TRIGRAPH, "ABO",
                                              FaxToMailUser.PROPERTY_USER_GROUPS, groups12);
        FaxToMailUser jeanne = userDao.create(FaxToMailUser.PROPERTY_LAST_NAME, "Bourgoin",
                                              FaxToMailUser.PROPERTY_FIRST_NAME, "Jeanne",
                                              FaxToMailUser.PROPERTY_LOGIN, "jbourgoin",
                                              FaxToMailUser.PROPERTY_TRIGRAPH, "JBO",
                                              FaxToMailUser.PROPERTY_USER_GROUPS, groups12);

        // create folders
        MailFolderTopiaDao folderDao = getPersistenceContext().getMailFolderDao();
        Map<String, MailFolder> foldersByName = new HashMap<>();
        MailFolder franciaflex = folderDao.create(MailFolder.PROPERTY_NAME, "Franciaflex",
                MailFolder.PROPERTY_COMPANY, "FXCOMP");

        // categories
        MailFolder chargesClientelle = folderDao.create(MailFolder.PROPERTY_NAME, "Chargés de clientèle",
                                                        MailFolder.PROPERTY_PARENT, franciaflex,
                                                        MailFolder.PROPERTY_REPLY_ADDRESSES, Lists.newArrayList("no-reply@franciaflex.com"),
                                                        MailFolder.PROPERTY_COMPANY, "FXCOMP");
        franciaflex.addChildren(chargesClientelle);
        foldersByName.put(chargesClientelle.getName(), chargesClientelle);

        MailFolder sav = folderDao.create(MailFolder.PROPERTY_NAME, "SAV",
                                          MailFolder.PROPERTY_PARENT, franciaflex,
                                          MailFolder.PROPERTY_WAITING_STATES, waitingStates.subList(0, 1),
                                          MailFolder.PROPERTY_COMPANY, "FXCOMP",
                                          MailFolder.PROPERTY_READ_RIGHT_GROUPS, groups23);
        franciaflex.addChildren(sav);
        foldersByName.put(sav.getName(), sav);

        // user folders
        MailFolder cyrilFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Cyril",
                                                  MailFolder.PROPERTY_PARENT, chargesClientelle,
                                                  MailFolder.PROPERTY_WAITING_STATES, waitingStates.subList(1, 2),
                                                  MailFolder.PROPERTY_COMPANY, "FXCOMP",
                                                  MailFolder.PROPERTY_READ_RIGHT_USERS, Lists.newArrayList(cyril));
        chargesClientelle.addChildren(cyrilFolder);
        foldersByName.put(cyrilFolder.getName(), cyrilFolder);

        MailFolder claireFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Claire",
                                                   MailFolder.PROPERTY_PARENT, chargesClientelle,
                                                   MailFolder.PROPERTY_COMPANY, "FXCOMP");
        chargesClientelle.addChildren(claireFolder);
        foldersByName.put(claireFolder.getName(), claireFolder);

        MailFolder agatheFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Agathe",
                                                   MailFolder.PROPERTY_PARENT, chargesClientelle,
                                                   MailFolder.PROPERTY_WAITING_STATES, waitingStates.subList(2, 3),
                                                   MailFolder.PROPERTY_COMPANY, "FXCOMP");
        chargesClientelle.addChildren(agatheFolder);
        foldersByName.put(agatheFolder.getName(), agatheFolder);

        MailFolder marcFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Marc",
                                                 MailFolder.PROPERTY_PARENT, sav,
                                                 MailFolder.PROPERTY_COMPANY, "FXCOMP");
        sav.addChildren(marcFolder);
        foldersByName.put(marcFolder.getName(), marcFolder);

        MailFolder fredericFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Frédéric",
                                                     MailFolder.PROPERTY_PARENT, sav,
                                                     MailFolder.PROPERTY_COMPANY, "FXCOMP");
        sav.addChildren(fredericFolder);
        foldersByName.put(fredericFolder.getName(), fredericFolder);

        MailFolder jeanneFolder = folderDao.create(MailFolder.PROPERTY_NAME, "Jeanne",
                                                   MailFolder.PROPERTY_PARENT, sav,
                                                   MailFolder.PROPERTY_COMPANY, "FXCOMP");
        sav.addChildren(jeanneFolder);
        foldersByName.put(jeanneFolder.getName(), jeanneFolder);

        // droits global à marc pour tout
        franciaflex.addReadRightUsers(marc);
        franciaflex = folderDao.update(franciaflex);

        // import test csv
        List<Range> ranges = null;
        try (InputStream is = InitTestDataImpl.class.getResourceAsStream("/csv/ranges.csv")) {
            ranges = referentielService.importRanges(is);
        }
        List<Priority> priorities = null;
        try (InputStream is = InitTestDataImpl.class.getResourceAsStream("/csv/priorities.csv")) {
            priorities = referentielService.importPriorities(is);
        }
        try (InputStream is = InitTestDataImpl.class.getResourceAsStream("/csv/email_accounts.csv")) {
            referentielService.importEmailAccounts(is);
        }
        List<Client> clients = null;
        try (InputStream is = InitTestDataImpl.class.getResourceAsStream("/csv/clients.csv")) {
            clients = referentielService.importClients(is);
        }
        List<DemandType> types = null;
        try (InputStream is = InitTestDataImpl.class.getResourceAsStream("/csv/demandtypes.csv")) {
            types = referentielService.importDemandTypes(is);
            
            // ajoute le champ GAMME en champ obligatoire pour tester les quantités par gammes
            types.get(0).setRequiredFields(EnumSet.of(MailField.CLIENT, MailField.RANGE_ROW));
        }
        try (InputStream is = InitTestDataImpl.class.getResourceAsStream("/csv/email_filters.csv")) {
            referentielService.importEmailFilters(is, foldersByName);
        }

        // create test mail
        HistoryTopiaDao historyDao = getPersistenceContext().getHistoryDao();
        RangeRowTopiaDao rangeRowDao = getPersistenceContext().getRangeRowDao();
        EmailTopiaDao emailDao = getPersistenceContext().getEmailDao();
        OriginalEmailTopiaDao originalEmailDao = getPersistenceContext().getOriginalEmailDao();
        createEmails(historyDao, rangeRowDao, emailDao, originalEmailDao, ranges, clients, types, priorities, cyrilFolder, cyril);
    }

    protected void createEmails(HistoryTopiaDao historyDao, RangeRowTopiaDao rangeRowDao, EmailTopiaDao emailDao, OriginalEmailTopiaDao originalEmailDao,
            List<Range> ranges, List<Client> clients, List<DemandType> types, List<Priority> priorities, MailFolder folder, FaxToMailUser user)
            throws InvalidClientException {
        for (int i = 0; i < 5; i++) {
            createEmail(historyDao, rangeRowDao, emailDao, originalEmailDao, ranges, clients, types, priorities, folder, user);
        }
    }

    protected void createEmail(HistoryTopiaDao historyDao, RangeRowTopiaDao rangeRowDao, EmailTopiaDao emailDao, OriginalEmailTopiaDao originalEmailDao,
            List<Range> ranges, List<Client> clients, List<DemandType> types, List<Priority> priorities, MailFolder folder, FaxToMailUser user)
            throws InvalidClientException {

        Client client = clients.get(0);
        boolean fax = false;
        List<String> senders = client.getEmailAddresses();
        List<String> faxNumbers = client.getFaxNumbers();
        if (CollectionUtils.isEmpty(senders) && CollectionUtils.isNotEmpty(faxNumbers)) {
            senders = Collections.singletonList(client.getFaxNumbers().get(0));
            fax = true;
        }
        String sender = null;
        if (CollectionUtils.isNotEmpty(senders)) {
            sender = senders.get(0);
        }
        String faxNumber = null;
        if (CollectionUtils.isNotEmpty(faxNumbers)) {
            faxNumber = faxNumbers.get(0);
        }

        boolean opened = true; //random.nextBoolean();

        Decorator<Date> dateDecorator = getDecoratorService().getDecoratorByType(Date.class, DecoratorService.DATE);

        Date now = new Date();
        History history = historyDao.create(
                History.PROPERTY_TYPE, HistoryType.CREATION,
                History.PROPERTY_MODIFICATION_DATE, now);
        history.setFields(Sets.newHashSet(
                        Email.PROPERTY_SENDER, Email.PROPERTY_CLIENT,
                        Email.PROPERTY_PROJECT_REFERENCE, Email.PROPERTY_OBJECT, Email.PROPERTY_RECEPTION_DATE,
                        Email.PROPERTY_MAIL_FOLDER, Email.PROPERTY_DEMAND_STATUS));

        String projectRef = t("faxtomail.email.projectReference.default", dateDecorator.toString(now));

        OriginalEmail originalEmail = originalEmailDao.create(OriginalEmail.PROPERTY_CONTENT, "test");

        Email email = emailDao.create(
                Email.PROPERTY_SENDER, sender,
                Email.PROPERTY_FAX, fax,
                Email.PROPERTY_CLIENT, client,
                Email.PROPERTY_PROJECT_REFERENCE, projectRef,
                Email.PROPERTY_OBJECT, client.getCaracteristic1() + " / " + client.getCode() + " / "
                        + client.getName() + " / " + faxNumber + " / " + dateDecorator.toString(now),
                Email.PROPERTY_RECEPTION_DATE, now,
                Email.PROPERTY_MAIL_FOLDER, folder,
                Email.PROPERTY_DEMAND_STATUS, opened ? DemandStatus.IN_PROGRESS : DemandStatus.UNTREATED,
                Email.PROPERTY_HISTORY, Lists.newArrayList(history),
                Email.PROPERTY_ORIGINAL_EMAIL, originalEmail);

        if (opened) {

            List<RangeRow> rangeRows = new ArrayList<>();
            getEmailService().addToHistory(email.getTopiaId(), HistoryType.OPENING, user, now);
            DemandType demandType = types.get(0); // types.get(0) à des gammes obligatoires
            if (FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.RANGE_ROW)) {
                for (int j = 0; j < 4; j++) {
                    RangeRow rangeRow = rangeRowDao.create(
                            RangeRow.PROPERTY_RANGE, ranges.get(j),
                            RangeRow.PROPERTY_COMMAND_NUMBER, "FX-" + RandomStringUtils.randomNumeric(4),
                            RangeRow.PROPERTY_PRODUCT_QUANTITY, 50,
                            RangeRow.PROPERTY_SAV_QUANTITY, 60);
                    rangeRows.add(rangeRow);
                }
                email.setRangeRow(rangeRows);
            }

            email.setDemandType(demandType);
            email.setPriority(priorities.get(0));
            email.setRangeRow(rangeRows);
            getEmailService().saveEmail(email, user, Email.PROPERTY_DEMAND_TYPE, Email.PROPERTY_PRIORITY, Email.PROPERTY_RANGE_ROW);
        }

        // create another test mail one month ago in archived state
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date yesterday = cal.getTime();
        history = historyDao.create(History.PROPERTY_TYPE, HistoryType.CREATION,
                History.PROPERTY_MODIFICATION_DATE, yesterday);
        history.setFields(Sets.newHashSet(Email.PROPERTY_SENDER,
                Email.PROPERTY_CLIENT, Email.PROPERTY_PROJECT_REFERENCE, Email.PROPERTY_OBJECT, Email.PROPERTY_RECEPTION_DATE,
                Email.PROPERTY_MAIL_FOLDER, Email.PROPERTY_DEMAND_STATUS));
        projectRef = t("faxtomail.email.projectReference.default", dateDecorator.toString(yesterday));

        originalEmail = originalEmailDao.create(OriginalEmail.PROPERTY_CONTENT, "test");

        email = emailDao.create(
                Email.PROPERTY_SENDER, sender,
                Email.PROPERTY_FAX, fax,
                Email.PROPERTY_CLIENT,client,
                Email.PROPERTY_PROJECT_REFERENCE, projectRef, Email.PROPERTY_OBJECT, client.getCaracteristic1() + " / " + client.getCode() + " / "
                        + client.getName() + " / " + faxNumber + " / " + dateDecorator.toString(yesterday),
                Email.PROPERTY_RECEPTION_DATE, yesterday,
                Email.PROPERTY_MAIL_FOLDER, folder,
                Email.PROPERTY_DEMAND_STATUS, DemandStatus.ARCHIVED,
                Email.PROPERTY_HISTORY, Lists.newArrayList(history),
                Email.PROPERTY_ORIGINAL_EMAIL, originalEmail);

        if (opened) {
            List<RangeRow> rangeRows = new ArrayList<>();
            getEmailService().addToHistory(email.getTopiaId(), HistoryType.OPENING, user, yesterday);
            DemandType demandType = types.get(0); // types.get(0) à des gammes obligatoires
            if (FaxToMailServiceUtils.contains(demandType.getRequiredFields(), MailField.RANGE_ROW)) {
                for (int j = 0; j < 4; j++) {
                    RangeRow rangeRow = rangeRowDao.create(RangeRow.PROPERTY_RANGE, ranges.get(j),
                            RangeRow.PROPERTY_COMMAND_NUMBER, "FXRR-" + RandomStringUtils.randomNumeric(4),
                            RangeRow.PROPERTY_PRODUCT_QUANTITY, 50,
                            RangeRow.PROPERTY_SAV_QUANTITY, 60);
                    rangeRows.add(rangeRow);
                }
                email.setRangeRow(rangeRows);
            }
            email.setDemandType(demandType);
            email.setPriority(priorities.get(0));
            email.setRangeRow(rangeRows);
            getEmailService().saveEmail(email, null, null, user, Email.PROPERTY_DEMAND_TYPE, Email.PROPERTY_PRIORITY,
                    Email.PROPERTY_RANGE_ROW);
        }
    }
}
