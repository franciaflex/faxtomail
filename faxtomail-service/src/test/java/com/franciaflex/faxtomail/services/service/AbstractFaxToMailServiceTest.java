package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.FaxToMailConfiguration;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaApplicationContext;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaPersistenceContext;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUserTopiaDao;
import com.franciaflex.faxtomail.services.FaxToMailService;
import com.franciaflex.faxtomail.services.FaxToMailServiceContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.H2Dialect;
import org.junit.After;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.persistence.support.TopiaSqlWork;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class AbstractFaxToMailServiceTest {

    private static final Log log = LogFactory.getLog(AbstractFaxToMailServiceTest.class);

    protected static final String DATABASE_REF = "DataBaseRef";

    protected static FaxToMailConfiguration applicationConfig;

    protected FaxToMailServiceContext serviceContext;

    protected FaxToMailTopiaApplicationContext applicationContext;

    protected List<FaxToMailTopiaPersistenceContext> openedTransactions = new LinkedList<>();

    protected File databaseFile;
    
    protected FaxToMailUser currentUser;

    protected Map<String, String> getDatabaseConfiguration(String dataBase) {
        Map<String, String> hibernateH2Config = new HashMap<String, String>();

        hibernateH2Config.putAll(getApplicationConfig().getTopiaProperties());

        hibernateH2Config.put(Environment.DIALECT, H2Dialect.class.getName());
        hibernateH2Config.put(Environment.DRIVER, org.h2.Driver.class.getName());
        hibernateH2Config.put(Environment.USER, "sa");
        hibernateH2Config.put(Environment.PASS, "");
        hibernateH2Config.put(Environment.HBM2DDL_AUTO, ""); // default validate won't work with h2

        File tempDirFile = SystemUtils.getJavaIoTmpDir();

        databaseFile = new File(tempDirFile, dataBase);

        // Set test data in MSSQLServer mode (like production database)
        String h2dataPath = databaseFile.getAbsolutePath() + File.separator + "h2data-test;MODE=MSSQLServer";

        String jdbcUrl = "jdbc:h2:file:" + h2dataPath;

        hibernateH2Config.put(Environment.URL, jdbcUrl);

        if (log.isTraceEnabled()) {
            log.trace("will store H2 data in " + h2dataPath);
            log.trace("allJpaParameters = " + hibernateH2Config);
        }

        if (log.isDebugEnabled()) {
            log.debug("jdbc url is\n" + jdbcUrl);
        }
        
        return hibernateH2Config;
    }

    protected FaxToMailTopiaApplicationContext getApplicationContext() {

        if (applicationContext == null) {
            String databaseName = UUID.randomUUID().toString();
            Map<String, String> hibernateH2Config = getDatabaseConfiguration(databaseName);

            applicationContext = new FaxToMailTopiaApplicationContext(hibernateH2Config);
            addMissingTable(applicationContext);
    
            if (log.isTraceEnabled()) {
                log.trace("created root context " + applicationContext);
            }
        }

        return applicationContext;
    }

    /**
     * Ajoute manuellement les tables qui ne sont pas gérées par Topia.
     * 
     * @param applicationContext application context
     */
    protected void addMissingTable(FaxToMailTopiaApplicationContext applicationContext) {
        FaxToMailTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext();
        TopiaSqlSupport sqlSupport = persistenceContext.getSqlSupport();
        sqlSupport.doSqlWork(new TopiaSqlWork() {
            @Override
            public void execute(Connection connection) throws SQLException {
                
                Statement stat = connection.createStatement();

                stat.executeUpdate("create table newClient (       " +
                                   "name longvarchar,              " +
                                   "emailAddress longvarchar,    " +
                                   "faxNumber longvarchar,         " +
                                   "caracteristic1 longvarchar,    " +
                                   "caracteristic2 longvarchar,    " +
                                   "caracteristic3 longvarchar,    " +
                                   "code longvarchar not null,     " +
                                   "company longvarchar not null,  " +
                                   "brand longvarchar,             " +
                                   "personInCharge longvarchar,    " +
                                   "unique (code, company, emailAddress, faxnumber)" +
                                   ");                             ");
                
                stat.executeUpdate("create table ediReturn (             " +
                                   "rangeRowTopiaId longvarchar not null," +
                                   "commandNumber longvarchar,           " +
                                   "error longvarchar,                   " +
                                   "unique (rangeRowTopiaId)             " +
                                   ")");
                
                connection.commit();

            }
        });

        persistenceContext.close();
    }

    protected FaxToMailTopiaPersistenceContext newPersistenceContext() {

        FaxToMailTopiaPersistenceContext persistenceContext;

        persistenceContext = getApplicationContext().newPersistenceContext();

        if (log.isTraceEnabled()) {
            log.trace("opened transaction " + persistenceContext);
        }

        openedTransactions.add(persistenceContext);

        return persistenceContext;

    }

    @After
    public void tearDown() throws IOException {

        for (FaxToMailTopiaPersistenceContext openedTransaction : openedTransactions) {

            if (log.isTraceEnabled()) {
                log.trace("closing transaction " + openedTransaction);
            }

            openedTransaction.close();

        }

        if (applicationContext != null) {

            if (log.isTraceEnabled()) {
                log.trace("closing transaction " + applicationContext);
            }

            applicationContext.close();

        }

        // clear full directory
        if (databaseFile != null) {
            FileUtils.deleteDirectory(databaseFile);
        }
    }

    protected static FaxToMailConfiguration getApplicationConfig() {

        if (applicationConfig == null) {

            applicationConfig = new FaxToMailConfiguration("faxToMail.properties");

        }

        return applicationConfig;

    }

    protected FaxToMailServiceContext getServiceContext() {

        if (serviceContext == null) {

            FaxToMailServiceContext serviceContext = FaxToMailServiceContext.newDirectServiceContext(getApplicationContext());

            serviceContext.setApplicationConfig(getApplicationConfig());

            this.serviceContext = serviceContext;
        }

        return serviceContext;

    }

    protected <E extends FaxToMailService> E newService(Class<E> serviceClass) {

        return getServiceContext().newService(serviceClass);

    }

    protected FaxToMailUser getCurrentUser() {
        if (currentUser == null) {
            FaxToMailUserTopiaDao faxToMailUserTopiaDao = serviceContext.getPersistenceContext().getFaxToMailUserDao();
            currentUser = faxToMailUserTopiaDao.forLoginEquals("mlefebvre").findUnique();
        }
        return currentUser;
    }
}
