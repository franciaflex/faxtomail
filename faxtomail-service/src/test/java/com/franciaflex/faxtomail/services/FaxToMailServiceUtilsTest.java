package com.franciaflex.faxtomail.services;

/*
 * #%L
 * FaxToMail :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 2.0.4
 */
public class FaxToMailServiceUtilsTest {

    @Test
    public void testGetDomainForEmailAddress() {
        String email = "test@test.com";
        String domain = FaxToMailServiceUtils.getDomainForEmailAddress(email);
        Assert.assertEquals("test.com", domain);

        email = "test.com";
        domain = FaxToMailServiceUtils.getDomainForEmailAddress(email);
        Assert.assertEquals("test.com", domain);

        try {
            FaxToMailServiceUtils.getDomainForEmailAddress(null);
            Assert.fail("null email address should throw an excpetion");
        } catch(NullPointerException e) {
            // a NPE should be thrown
        }
    }

    @Test
    public void testDecodeSubject() {
        try {
            String subject = "=?iso-8859-1?Q?Un fax de 1 page(s) a =E9t=E9 re=E7u?=";
            Assert.assertEquals("Un fax de 1 page(s) a été reçu", FaxToMailServiceUtils.getDecodedSubject(subject));

            subject = "Un fax de 1 page(s) a été reçu";
            Assert.assertEquals("Un fax de 1 page(s) a été reçu", FaxToMailServiceUtils.getDecodedSubject(subject));

            subject = "Auto: Accusé de réception de votre=?Cp1252?Q?_commande_=E0_France_Fermetures?=";
            Assert.assertEquals("Auto: Accusé de réception de votre commande à France Fermetures", FaxToMailServiceUtils.getDecodedSubject(subject));

        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testDecodeFrom() {
        String from = "=?iso-8859-1?b?qkfjrsbpvuvtvcbo?= =?iso-8859-1?b?qu5urvmgqkftu0ug?=   =?iso-8859-1?b?r09vtefjtku=?= <agence.nantes@baie-ouest.fr>";
        Assert.assertEquals("agence.nantes@baie-ouest.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "=?iso-8859-1?b?QkFJRSBPVUVTVCBC?= =?iso-8859-1?b?QVNSBHT1VMQULO?= =?iso-8859-1?b?RQ==?= <agence.nantes@baie-ouest.fr>";
        Assert.assertEquals("agence.nantes@baie-ouest.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "=?iso-8859-1?b?u2nhbm7pigrlchvp?= =?iso-8859-1?b?c1rvc2hpymegrxhw?= =?iso-8859-1?b?zxj0ie1lbnvpc2vy?= =?iso-8859-1?b?awu=?= <a.bonnaud@expertmenuiserie.fr>";
        Assert.assertEquals("a.bonnaud@expertmenuiserie.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "=?iso-8859-1?q?henri_seguin__mail_=3a_henri=2eseguin=40homkia=2efr_-_t=e9?=   =?iso-8859-1?q?l_=3a_09_84_07_18_47?= <henri.seguin@homkia.fr>";
        Assert.assertEquals("henri.seguin@homkia.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "=?windows-1252?q?henri_seguin__mail_=3a_henri=2eseguin=40homkia=2efr_-_t?= =?windows-1252?q?=e9l_=3a_09_84_07_18_47?= <henri.seguin@homkia.fr>";
        Assert.assertEquals("henri.seguin@homkia.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "=?utf-8?q?secr=c3=a9tariat_-_menuiseries_de_lum?= =?utf-8?q?i=c3=a8re_technal?= <mgivaja@mdl83.fr>";
        Assert.assertEquals("mgivaja@mdl83.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "=?utf-8?q?6_jrs_=7c_r=c3=a8gles_juridiques_des?= =?utf-8?q?_travaux_de_btp?= <actu@ofib.eu>";
        Assert.assertEquals("actu@ofib.eu", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "=?utf-8?q?entreprise_on_demand_=2d_comptablilit=c3=a9_fournisseu?= =?utf-8?q?rs?= <comptabilite.fournisseurs@vipplus.com>";
        Assert.assertEquals("comptabilite.fournisseurs@vipplus.com", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "Jean Couteau <couteau@codelutin.com>";
        Assert.assertEquals("couteau@codelutin.com", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "";
        Assert.assertEquals("", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "agence.nantes@baie-ouest.fr";
        Assert.assertEquals("agence.nantes@baie-ouest.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "<agence.nantes@baie-ouest.fr>";
        Assert.assertEquals("agence.nantes@baie-ouest.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "BAIE OUEST NANTES BASSE GOULAINE <agence.nantes@baie-ouest.fr>";
        Assert.assertEquals("agence.nantes@baie-ouest.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "\"mpo.cae.sec@\" <free.fr mpo.cae.sec@free.fr>";
        Assert.assertEquals("mpo.cae.sec@free.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "\"mpo.cae.sec@\" <mpo.cae.sec@free.fr free.fr>";
        Assert.assertEquals("mpo.cae.sec@free.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "\"mpo.cae.sec@\" <mpo.cae.sec@free.fr>";
        Assert.assertEquals("mpo.cae.sec@free.fr", FaxToMailServiceUtils.getDecodedFrom(from));

        from = "<mpo.cae.sec@free.fr>";
        Assert.assertEquals("mpo.cae.sec@free.fr", FaxToMailServiceUtils.getDecodedFrom(from));
    }
}