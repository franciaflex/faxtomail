package com.franciaflex.faxtomail.services.service;

/*-
 * #%L
 * FaxToMail :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2018 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

//class used as utility to test/debug image related problems
public class ImageDebugUtility extends AbstractFaxToMailServiceTest {

    private static final Log log = LogFactory.getLog(ImageDebugUtility.class);

    //@Test
    public void testReadImage() throws IOException {
        File file = new File("src/test/resources/image/image001.png");
        BufferedImage image = ImageIO.read(file);
        PDDocument document = new PDDocument();
        LosslessFactory.createFromImage(document, image);
    }

}
