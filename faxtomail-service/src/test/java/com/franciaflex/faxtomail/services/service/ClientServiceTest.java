package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.BrandsForDomain;
import com.franciaflex.faxtomail.persistence.entities.BrandsForDomainTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.Client;
import com.franciaflex.faxtomail.persistence.entities.ClientTopiaDao;
import com.franciaflex.faxtomail.persistence.entities.EmailImpl;
import com.franciaflex.faxtomail.persistence.entities.NewClient;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.persistence.support.TopiaSqlWork;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Test de client service.
 * 
 * @author Eric Chatellier
 */
public class ClientServiceTest extends AbstractFaxToMailServiceTest {

    @Before
    public void setUp() throws IOException {
        newService(InitTestData.class).initTestData();
    }

    /**
     * Test le nombre de client importés.
     */
    @Test
    public void testImportClientCount() {
        ClientTopiaDao clientDao = getServiceContext().getPersistenceContext().getClientDao();
        
        // asserts
        Assert.assertEquals(4, clientDao.count());
        // les emails et les fax ont une gestion particuliere
        Assert.assertFalse(clientDao.forEmailAddressesJsonLike("gerard.menvussa@code.com").isEmpty());
        Assert.assertFalse(clientDao.forFaxNumbersJsonLike("0498877665").isEmpty());
    }

    /**
     * Ajout des clients dans la table temporaire et appel le service de traitement de cette table.
     */
    @Test
    public void testAddNewClientInTmpTable() {

        ClientService clientService = newService(ClientService.class);
        ClientTopiaDao clientDao = getServiceContext().getPersistenceContext().getClientDao();

        // asserts
        Assert.assertTrue(clientDao.forEmailAddressesJsonLike("gerard.menvussa@lutin.com").isEmpty());

        // add new clients
        TopiaSqlSupport sqlSupport = getServiceContext().getPersistenceContext().getSqlSupport();
        sqlSupport.doSqlWork(new TopiaSqlWork() {
            @Override
            public void execute(Connection connection) throws SQLException {

                String query = String.format("INSERT INTO %s(%s,%s,%s,%s,%s) VALUES(?, ?, ?, ?, ?)",
                        NewClient.class.getSimpleName(),
                        NewClient.PROPERTY_CODE,
                        NewClient.PROPERTY_COMPANY,
                        NewClient.PROPERTY_BRAND,
                        NewClient.PROPERTY_EMAIL_ADDRESS,
                        NewClient.PROPERTY_NAME);

                PreparedStatement stat = connection.prepareStatement(query);
                stat.setString(1, "99154");
                stat.setString(2, "FX");
                stat.setString(3, "FX");
                stat.setString(4, "gerard.menvussa@lutin.com");
                stat.setString(5, "Gerard");
                int result = stat.executeUpdate();
                Assert.assertEquals(1, result);

                stat.setString(1, "99154");
                stat.setString(2, "FX");
                stat.setString(3, "FX");
                stat.setString(4, "gerard.menvussa+test@lutin.com");
                stat.setString(5, "Gerard");
                result = stat.executeUpdate();
                Assert.assertEquals(1, result);
            }
        });

        // precedure de mise à jour
        clientService.updateNewClients();

        // asserts
        List<Client> clients = clientDao.forEmailAddressesJsonLike("gerard.menvussa@lutin.com");
        Assert.assertEquals(1, clients.size());
        Assert.assertEquals(2, clients.get(0).getEmailAddresses().size());

    }

    @Test
    public void testGetClientForEmailAddress() {
        BrandsForDomainTopiaDao brandsForDomainTopiaDao = getServiceContext().getPersistenceContext().getBrandsForDomainDao();

        BrandsForDomain brandsForDomain = brandsForDomainTopiaDao.newInstance();
        brandsForDomain.setDomainName("groupecreal.com");
        brandsForDomain.setBrands(Lists.newArrayList("CREAL"));
        brandsForDomain = brandsForDomainTopiaDao.create(brandsForDomain);

        ClientTopiaDao clientTopiaDao = getServiceContext().getPersistenceContext().getClientDao();

        Client client = clientTopiaDao.newInstance();
        client.setBrand("FX");
        client.setCompany("FX");

        String emailAddress = "test@groupecreal.com";
        String domain = emailAddress.substring(emailAddress.lastIndexOf("@") + 1);
        Assert.assertEquals("groupecreal.com", domain);

        client.setEmailAddressesJson("[\"" + emailAddress + "\"]");
        client.setCode("987654");
        client.setName("Test Test");
        client = clientTopiaDao.create(client);

        List<Client> foundClients =
                getServiceContext().getClientService().getClientForEmailAddress(emailAddress,
                                                                                new EmailImpl(),
                                                                                "FX", null);

        Assert.assertEquals(foundClients.size(), 1);
        Assert.assertEquals(client, foundClients.get(0));

        client = clientTopiaDao.newInstance();
        client.setBrand("CREAL");
        client.setCompany("FX");
        client.setEmailAddressesJson("[\"" + emailAddress + "\"]");
        client.setCode("123456");
        client.setName("Test Test");
        client = clientTopiaDao.create(client);

        List<String> brands = getServiceContext().getConfigurationService().getBrandsForEmailAddress(domain);
        foundClients = getServiceContext().getClientService().getClientForEmailAddress(emailAddress,
                                                                                        new EmailImpl(),
                                                                                        "FX", brands);

        Assert.assertEquals(1, foundClients.size());
        Assert.assertEquals(client, foundClients.get(0));

        brandsForDomain.setBrands(Lists.newArrayList("CREAL", "FX"));
        brandsForDomain = brandsForDomainTopiaDao.update(brandsForDomain);

        brands = getServiceContext().getConfigurationService().getBrandsForEmailAddress(domain);
        foundClients = getServiceContext().getClientService().getClientForEmailAddress(emailAddress,
                                                                                       new EmailImpl(),
                                                                                       "FX", brands);
        Assert.assertEquals(2, foundClients.size());
    }
}
