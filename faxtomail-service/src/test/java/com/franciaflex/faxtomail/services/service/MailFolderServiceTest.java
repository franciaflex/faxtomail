package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.EmailImpl;
import com.franciaflex.faxtomail.persistence.entities.FaxToMailUser;
import com.franciaflex.faxtomail.persistence.entities.MailFilter;
import com.franciaflex.faxtomail.persistence.entities.MailFolder;
import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 *
 */
public class MailFolderServiceTest extends AbstractFaxToMailServiceTest {

    private static final Log log = LogFactory.getLog(MailFolderServiceTest.class);

    protected MailFolderService service;
    protected UserService userService;

    @Before
    public void setUp() throws IOException {
        service = newService(MailFolderService.class);
        newService(InitTestData.class).initTestData();
        userService = newService(UserService.class);
    }

    @Test
    public void testFindFilter() {
        MailFolder folder = service.getFolderForRecipient("fx.savtest@franciaflex.com");
        Assert.assertNotNull(folder);

        try {
            Address a1 = new InternetAddress("  etyhjsd  sqhdksd sdlksqdj - sqjdsq <test@franciaflex.com>");
            Address a2 = new InternetAddress("dfsdf  fsf- sdfs<fx.adv%test@franciaflex.com>");
            Address a3 = new InternetAddress("dsfdsf qd qsdsq d <fx.01test@franciaflex.com>");

            EmailImpl email = new EmailImpl();
            HashSet<String> modifiedProperties = new HashSet<String>();

            MailFilter filter = service.findMailFilter(email, modifiedProperties, Lists.newArrayList(a1, a2, a3));
            Assert.assertNotNull(filter);
            Assert.assertEquals(filter.getMailFolder().getName(), "Cyril");

            filter = service.findMailFilter(email, modifiedProperties, Lists.newArrayList(a1, a3, a2));
            Assert.assertNotNull(filter);
            Assert.assertEquals(filter.getMailFolder().getName(), "Cyril");

            filter = service.findMailFilter(email, modifiedProperties, Lists.newArrayList(a3, a1, a2));
            Assert.assertNotNull(filter);
            Assert.assertEquals(filter.getMailFolder().getName(), "Cyril");


        } catch (AddressException e) {
            if (log.isErrorEnabled()) {
                log.error("error on email creation", e);
            }
        }
    }

    @Test
    public void testRootFolders() {
        FaxToMailUser cyril = userService.getUserByLogin("cbaillet");
        List<MailFolder> mailFolders = service.getRootMailFoldersWithReadingRights(cyril);
        Assert.assertEquals(2, mailFolders.size());
        log.info(mailFolders.size() + " readable folders ");
        for (MailFolder folder : mailFolders) {
            printTree(folder, " ");
        }
    }

    protected void printTree(MailFolder folder, String prefix) {
        log.info(prefix + folder.getName());
        if (folder.getChildren() != null) {
            for (MailFolder f : folder.getChildren()) {
                printTree(f, "-" + prefix);
            }
        }
    }

}
