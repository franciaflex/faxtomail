package com.franciaflex.faxtomail.services.service;

/*
 * #%L
 * FaxToMail :: Service
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Mac-Groupe, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.franciaflex.faxtomail.persistence.entities.FaxToMailTopiaApplicationContext;
import org.hibernate.cfg.Environment;
import org.junit.Ignore;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Class utilitaire permettant de générer facilement les schemas pour différentes base de données.
 * 
 * @author Eric Chatellier
 */
@Ignore
public class MiscTest extends AbstractFaxToMailServiceTest {

    @Override
    protected Map<String, String> getDatabaseConfiguration(String dataBase) {
        Map<String, String> hibernateH2Config = new HashMap<>();

        // h2 config
        hibernateH2Config.put(Environment.DIALECT, "org.hibernate.dialect.H2Dialect");
        hibernateH2Config.put(Environment.DRIVER, "org.h2.Driver");
        hibernateH2Config.put(Environment.USER, "sa");
        hibernateH2Config.put(Environment.PASS, "");
        hibernateH2Config.put(Environment.HBM2DDL_AUTO, "");
        hibernateH2Config.put(Environment.URL, "jdbc:h2:file:/tmp/faxtomail/h2data");

        // sqlserver config
        /*hibernateH2Config.put(Environment.DIALECT, "org.hibernate.dialect.SQLServer2008Dialect");
        hibernateH2Config.put(Environment.DRIVER, "org.h2.Driver");
        hibernateH2Config.put(Environment.USER, "sa");
        hibernateH2Config.put(Environment.PASS, "");
        hibernateH2Config.put(Environment.HBM2DDL_AUTO, "");
        hibernateH2Config.put(Environment.URL, "jdbc:h2:file:/tmp/faxtomail/h2data;MODE=MSSQLServer");*/

        // postgres configuration
        /*hibernateH2Config.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQL9Dialect");
        hibernateH2Config.put(Environment.DRIVER, "org.postgresql.Driver");
        hibernateH2Config.put(Environment.USER, "postgres");
        hibernateH2Config.put(Environment.PASS, "postgres");
        hibernateH2Config.put(Environment.HBM2DDL_AUTO, "");
        hibernateH2Config.put(Environment.URL, "jdbc:postgresql:faxtomail");*/
        return hibernateH2Config;
    }
    
    @Override
    protected void addMissingTable(FaxToMailTopiaApplicationContext applicationContext) {
        // not needed here (and not working)
    }

    @Test
    public void getSqlServerSchema() {
        getApplicationContext().showCreateSchema();
    }
}
